      /*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package rra;

import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.PrintJob;
import java.awt.Toolkit;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.Printable;
import java.awt.print.PrinterJob;
import java.io.File;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Date;
import java.util.LinkedList;
import java.util.Properties;
import javax.imageio.ImageIO;
import javax.swing.JFrame;  
import pos.Db;
import pos.Main;
import pos.Product;
/**
 *
 * @author Kimenyi
 */
public class RRA_PRINT2 extends JFrame {

    public String Trade_Name;
    public String Address, City;
    public String TIN;//: 000000000
    public String separateur = "---------------------------------------------------------------------";
    public String welcome = " ";
    public String Client_ID;//: 000000000
    public double TOTAL;// 6340.00
    public double TOTAL_A_EX;//1000.00
    public double TOTAL_B_18;//.00% 5340.00
    public double TOTAL_TAX_B;// 814.58
    public double TOTAL_TAX;// 814.58 
    //public double arr = 0.005;
    public double CASH;// 6340.00
    public int ITEMS_NUMBER;// 3
    public String SDC_INFORMATION = "SDC INFORMATION ";
    public String SDC_ID;//: SDC001000001
    public String RECEIPT_NUMBER;//: 168/258 NS
    public String Internal_Data;//:TE68-SLA2-34J5-EAV3-N569-88LJ-Q7
    public String Receipt_Signature;//:V249-J39C-FJ48-HE2W
    public String typeSales;
    public String transaction;
    public int RECEIPT_NUMBER2;//: 152
    public String date_time_sdc, date_time_mrc;//DATE: 25/5/2012 TIME: 11:09:32
    public String MRC;//: AAACC123456
    public String VERSION;
    public String tks = "";
    public String comeback = "COME BACK AGAIN";
    public String best = "YOUR BEST STORE IN TOWN";
    public LinkedList<Product> getProductListDB;
    public LinkedList<String> infoClient;
    public int margin = 5, boldEpson = 8, merciBold = 15, title = 10;
    public int size = 50;
    public int west = 200;
    String marge = "   ";
    private Properties properties = new Properties();
    public boolean byempurimwe = false;
    int REFUND_REF = 0;
    LinkedList<String> ejList;
    public String NUM_AFF = "";
    public String NOM_CLIENT = "";
    public String PRENOM_CLIENT = "";
    public String ASSURANCE = "";
    public double PERCENTAGE = 0;
    public double PERCENTAGE_PART=0;
    public String EMPLOYEUR = "";
    public String UMUSERVEUR = "";
    public String QUITANCE = "";
    public String IZINA_CLIENT2 = "";
    public double MONEY=0;
    public double CHANGE=0;
    Statement stat;
    int NEG=1;
    PageFormat c;
    
    
    public RRA_PRINT2(String Trade_Name, String Address, String City, String TIN, String Client_ID,
            double TOTAL, double TOTAL_A_EX, double TOTAL_B_18, double TOTAL_TAX_B, double TOTAL_TAX,
            double CASH, String SDC_ID,String RECEIPT_NUMBER, String Internal_Data, String Receipt_Signature,
            int RECEIPT_NUMBER2,String MRC, LinkedList<Product> getProductListDB, String typeSales,
            String transaction, String VERSION, int NEG, int REFUND_REF, LinkedList<String> infoClient,
            String date_time_mrc, String date_time_sdc, String Employe, String Quittance,Statement stat,
            String nom_client,int margin,double money,double change)
    
       {//55
           
         System.out.println("nom_client  "+nom_client); 
         this.margin=margin;
           System.out.println("margin:"+margin);
        this.stat=stat;
        this.Trade_Name = Trade_Name;
        this.Address = Address;
        this.City = City;
        this.TIN = "TIN: " + TIN;
        this.Client_ID = Client_ID;
        this.NEG=NEG;
        if (Client_ID != null && Client_ID.length() > 2) {
            this.Client_ID = "CLIENT TIN:" + Client_ID;
        }
        this.TOTAL = TOTAL;
        this.TOTAL_A_EX = TOTAL_A_EX;
        this.TOTAL_B_18 = TOTAL_B_18;
        this.TOTAL_TAX_B = TOTAL_TAX_B;
        this.TOTAL_TAX = TOTAL_TAX;
        this.CASH = CASH;
        this.date_time_sdc = date_time_sdc;
        this.SDC_ID = SDC_ID;
        this.RECEIPT_NUMBER = RECEIPT_NUMBER;
        this.Internal_Data = Internal_Data;
        this.Receipt_Signature = Receipt_Signature;
        this.RECEIPT_NUMBER2 = RECEIPT_NUMBER2;
        this.date_time_mrc = date_time_mrc;
        this.MRC = MRC;
        this.getProductListDB = getProductListDB;
        this.typeSales = typeSales;
        this.transaction = transaction;
        this.REFUND_REF = REFUND_REF;
        this.VERSION = VERSION;
        this.UMUSERVEUR = Employe;
        this.QUITANCE = Quittance;
        if (infoClient != null) {
            System.out.println("percentage_part:"+infoClient.size());
            for (int i = 0; i < infoClient.size(); i++) {
                this.NUM_AFF = infoClient.get(i);
                this.NOM_CLIENT = infoClient.get(i + 1);
                this.PRENOM_CLIENT = infoClient.get(i + 2);
                this.ASSURANCE = infoClient.get(i + 3);
                this.PERCENTAGE = Integer.parseInt(infoClient.get(i + 4));
                this.EMPLOYEUR = infoClient.get(i + 5);
                if(infoClient.size()>6) {
                    this.PERCENTAGE_PART=Double.parseDouble(infoClient.get(i+6));
                }
                System.out.println("percentage_part:"+PERCENTAGE_PART);
                break;
            }
        }
        this.infoClient = infoClient;
        this.welcome=l(Main.SDCLANG, "V_WELCOME");
        this.SDC_INFORMATION=l(Main.SDCLANG, "V_SDC_INFORMATION");
        this.MONEY=money;
        this.CHANGE=change;
        
            if(nom_client!=null) {
                    this.IZINA_CLIENT2=nom_client;
                }
          System.out.println(IZINA_CLIENT2+"   IZINA_CLIENT2  "+nom_client);
        this.tks=l(Main.SDCLANG, "V_MERCI");
        
        String printmode=getString(typeSales+"printmode"); 
        System.out.println("printmode:"+printmode); 
        int numberOfPrint=1; 
        try 
        {  numberOfPrint=Integer.parseInt(printmode.split("_")[1]);  }
        catch (Throwable t ) 
        { System.err.println(printmode+"  "+t);   } 
        if(printmode== null  || printmode.contains("SELECTED")) 
        { 
            for(int i=0;i<numberOfPrint;i++) 
            { this.byempurimwe = PrintSelectedPrinter(); 
            } 
        }
        else {  for(int i=0;i<numberOfPrint;i++) { this.byempurimwe = PrintCanvas(); } }   
        
    }

    public RRA_PRINT2(String Trade_Name, String Address, String City, String TIN, String Client_ID,
            double TOTAL, double TOTAL_A_EX, double TOTAL_B_18, double TOTAL_TAX_B, double TOTAL_TAX,
            double CASH, String SDC_ID,String RECEIPT_NUMBER, String Internal_Data, String Receipt_Signature,
            int RECEIPT_NUMBER2,String MRC, String typeSales, String transaction, String VERSION, 
            String date_time_mrc, String date_time_sdc, String employe,Statement stat,int margin)
    
       {//55
            
         this.margin=margin;
        this.stat=stat;
        this.Trade_Name = Trade_Name;
        this.Address = Address;
        this.City = City;
        this.TIN = "TIN: " + TIN;
        this.Client_ID = Client_ID;
        if (Client_ID != null && Client_ID.length() > 2) {
            this.Client_ID = "CLIENT TIN:" + Client_ID;
        }
        System.out.println("FIMBO");
        this.TOTAL = TOTAL;
        this.TOTAL_A_EX = TOTAL_A_EX;
        this.TOTAL_B_18 = TOTAL_B_18;
        this.TOTAL_TAX_B = TOTAL_TAX_B;
        this.TOTAL_TAX = TOTAL_TAX;
        this.CASH = CASH;
        this.date_time_sdc = date_time_sdc;
        this.SDC_ID = SDC_ID;
        this.RECEIPT_NUMBER = RECEIPT_NUMBER;
        this.Internal_Data = Internal_Data;
        this.Receipt_Signature = Receipt_Signature;
        this.RECEIPT_NUMBER2 = RECEIPT_NUMBER2;
        this.date_time_mrc = date_time_mrc;
        this.MRC = MRC;
        this.typeSales = typeSales;
        this.transaction = transaction;
        this.VERSION = VERSION;
        this.UMUSERVEUR = employe;
        this.welcome=l(Main.SDCLANG, "V_WELCOME");
        this.SDC_INFORMATION=l(Main.SDCLANG, "V_SDC_INFORMATION");
        this.tks=l(Main.SDCLANG, "V_MERCI");
        System.out.println("FIMBO");
    }

    public RRA_PRINT2(LinkedList<RRA_PRINT2_LINE> toPrint) {
        PrintJob pjob = getToolkit().getPrintJob(this, "Ishyiga ", properties);
        if (pjob != null) {
            Graphics pg = pjob.getGraphics();
            if (pg != null) {
                int curHeight = margin + 10;

                int height = pjob.getPageDimension().height;
                for (int i = 0; i < toPrint.size(); i++) {
                    RRA_PRINT2_LINE line = toPrint.get(i);
                    printInt(pg, line.data, west, curHeight, line.font);
                    curHeight = printS(pg, line.fonction, margin, curHeight, line.font, "LEFT");
                    
                    if (curHeight + 20 > height) {
                        curHeight = 0;
                        pg.dispose();
                        pg = pjob.getGraphics();
                    }
                }
            }
            pjob.end();
        }
    }

    public RRA_PRINT2(String Trade_Name, String Address, String City, String TIN, String MRC,double tvaRateA,
            double tvaRateB, double tvaRateC, double tvaRateD,String Rtype, String user, String SDC_ID) 
    {
        PrintJob pjob = getToolkit().getPrintJob(this, "Ishyiga ", properties);
        if (pjob != null) {
            Graphics pg = pjob.getGraphics();
            if (pg != null) {
                int W = pjob.getPageDimension().width / 2;
                int curHeight = margin + 10;

                Font f = new Font("Arial", Font.BOLD, title);
                curHeight = printS(pg, Trade_Name, margin - 3, curHeight, f, "CENTER");
                f = new Font("Arial", Font.PLAIN, boldEpson);
                curHeight = printS(pg, "   " + Address, margin, curHeight, f, "CENTER");
                curHeight = printS(pg, "   " + City, margin, curHeight, f, "CENTER");
                curHeight = printS(pg, " " + TIN, margin, curHeight, f, "CENTER");
                curHeight = printS(pg, separateur, margin, curHeight, f, "CENTER");
                f = new Font("Arial", Font.BOLD, boldEpson);
                curHeight = printS(pg, "TAXE RATE A: ", margin, curHeight, f, "LEFT");
                printInt(pg, "" + setVirgule(tvaRateA, 1, ","), west, curHeight, f);
                curHeight = printS(pg, "TAXE RATE B: ", margin, curHeight, f, "LEFT");
                printInt(pg, "" + setVirgule(tvaRateB, 1, ","), west, curHeight, f);
                curHeight = printS(pg, "TAXE RATE C: ", margin, curHeight, f, "LEFT");
                printInt(pg, "" + setVirgule(tvaRateC, 1, ","), west, curHeight, f);
                curHeight = printS(pg, "TAXE RATE D: ", margin, curHeight, f, "LEFT");
                printInt(pg, "" + setVirgule(tvaRateD, 1, ","), west, curHeight, f);
                f = new Font("Arial", Font.PLAIN, boldEpson);
                curHeight = printS(pg, "ISHYIGA POS", margin, curHeight, f, "CENTER");
                curHeight = printS(pg, " " + VERSION, margin, curHeight, f, "LEFT");
                curHeight = printS(pg, "WWW.ISHYIGA.NET", margin, curHeight, f, "CENTER");
                f = new Font("Arial", Font.BOLD, boldEpson);
                curHeight = printS(pg, separateur, margin, curHeight, f, "CENTER");
                curHeight = printS(pg, SDC_INFORMATION, margin, curHeight, f, "CENTER");
                curHeight = printS(pg, "TIME: " + new Date().toLocaleString(), margin, curHeight, f, "LEFT");
                curHeight = printS(pg, "SDC ID: " + SDC_ID, margin, curHeight, f, "LEFT");
                curHeight = printS(pg, "PRINTED BY: " + user, margin, curHeight, f, "LEFT");

                pg.drawRect(margin - 5, margin - 5, W - 25, curHeight);
                pg.dispose();
            }
            pjob.end();
        }
    }

    public RRA_PRINT2(LinkedList<RRA_PRINT2_LINE> toPrint,
            LinkedList<RRA_PRODUCT> prod, int NEG) {
        PrintJob pjob = getToolkit().getPrintJob(this, "Ishyiga ", properties);
        if (pjob != null) {
            Graphics pg = pjob.getGraphics();
            if (pg != null) {
                int curHeight = margin + 10;

                west = 580;
                size = 40;
                int height = pjob.getPageDimension().height;
                for (int i = 0; i < toPrint.size(); i++) {
                    RRA_PRINT2_LINE line = toPrint.get(i);
                    printInt(pg, line.data, west, curHeight, line.font);
                    curHeight = printS(pg, line.fonction, margin, curHeight, line.font, "LEFT");
                }

                System.out.println("west:" + west);

                Font f = new Font("Arial", Font.BOLD, boldEpson);
                printS(pg, " TAX RATE ", west - 300, curHeight, f, "LEFT");
                printS(pg, " SOLD QTY", west - 240, curHeight, f, "LEFT");
                printS(pg, " STOCK QTY", west - 180, curHeight, f, "LEFT");
                printS(pg, " UNIT PRICE", west - 115, curHeight, f, "LEFT");
                printS(pg, " TOTAL PRICE", west - 50, curHeight, f, "LEFT");
                printS(pg, " CODE ", margin, curHeight, f, "LEFT");
                curHeight = printS(pg, " ITEM ", margin + 60, curHeight, f, "LEFT");
                f = new Font("Arial", Font.PLAIN, boldEpson);
                int pagenumber = 1;

                for (int i = 0; i < prod.size(); i++) {

                    RRA_PRODUCT p = (RRA_PRODUCT) prod.get(i);

                    printInt(pg, p.CATEGORIE, west - 270, curHeight, f);
                    printInt(pg, "" + p.qty, west - 210, curHeight, f);
                    printInt(pg, "" + p.onStock, west - 140, curHeight, f);
                    printInt(pg, "" + setVirgule(p.currentPrice, NEG,","), west-70, curHeight, f);
                    printInt(pg, "" + setVirgule((p.currentPrice*p.qty), NEG,","), west, curHeight, f);
                    printS(pg, p.productCode, margin, curHeight, f, "LEFT");
                    curHeight = printS(pg, p.name, margin + 60, curHeight, f, "LEFT");

//System.err.println(curHeight+"   "+height);

                    if (curHeight + 20 > height) {
//System.err.println(curHeight+"   "+height);
                        curHeight = printS(pg, "Page: " + pagenumber, margin + 60, curHeight, f, "CENTER");
                        curHeight = 20;
                        pg.dispose();
                        pg = pjob.getGraphics();
                        pagenumber++;
                        for (int j = 0; j < 4; j++) {
                            RRA_PRINT2_LINE line = toPrint.get(j);
                            printInt(pg, line.data, west, curHeight, line.font);
                            curHeight = printS(pg, line.fonction, margin, curHeight, line.font, "LEFT");
                        }
                    }
                }

            }
            pjob.end();
        }
    }

    private boolean PrintSelectedPrinter() 
    {
        System.out.println(" selected printer ");
        PrintJob pjob = getToolkit().getPrintJob(this, "Ishyiga ", properties);
        if (pjob != null) {
            Graphics pg = pjob.getGraphics();
            if (pg != null) { 
                
                int curHeight = margin + 10;
                int dim=0;
                
                if(Main.tax.rraClient.isRRA) {
                    dim=55;
                    pg.drawImage(Db.rraLogo, margin, curHeight-10,dim,45, rootPane);
                }

                Font f = new Font("Arial", Font.BOLD, title);
                curHeight = printS(pg, Trade_Name, margin+dim, curHeight, f, "LEFT");
                f = new Font("Arial", Font.PLAIN, boldEpson);
                curHeight = printS(pg, Address, margin+dim, curHeight, f, "LEFT");
                curHeight = printS(pg, City, margin+dim, curHeight, f, "LEFT");
                curHeight = printS(pg, TIN, margin+dim, curHeight, f, "LEFT");
                curHeight = printS(pg, separateur, margin, curHeight, f, "CENTER");
                
                System.out.println("hapa1  "+margin);

                if (transaction.contains("R")) {
                    curHeight = printS(pg, "REFUND", margin, curHeight, f, "CENTER");
                    curHeight = printS(pg, "REF.NORMAL RECEIPT#: " + REFUND_REF, margin, curHeight, f, "CENTER");
                    curHeight = printS(pg, separateur, margin, curHeight, f, "CENTER");
                    curHeight = printS(pg, "REFUND IS APPROVED ONLY FOR", margin, curHeight, f, "CENTER");
                    curHeight = printS(pg, "ORIGINAL SALES RECEIPT", margin, curHeight, f, "CENTER");
                    curHeight = printS(pg, Client_ID, margin, curHeight, f, "CENTER");
                    curHeight = printS(pg, separateur, margin, curHeight, f, "CENTER");
                } else if (typeSales.contains("P")) {
                    curHeight = printS(pg, welcome, margin, curHeight, f, "CENTER");
                    curHeight = printS(pg, "Client Name: " + NOM_CLIENT + " ", margin, curHeight, f, "CENTER");
                    curHeight = printS(pg, "Reference: " + ASSURANCE + ":" + PRENOM_CLIENT, margin, curHeight, f, "CENTER");
                    curHeight = printS(pg, Client_ID, margin, curHeight, f, "CENTER");
                      if(IZINA_CLIENT2 !=null)
                        {
                            curHeight = printS(pg, " "+IZINA_CLIENT2, margin, curHeight, f, "CENTER");
                        }
                    curHeight = printS(pg, l(Main.SDCLANG, "V_SERVED")+"" + UMUSERVEUR, margin, curHeight, f, "CENTER");
                    curHeight = printS(pg, separateur, margin, curHeight, f, "CENTER");

                } else {
                    if (infoClient != null && !ASSURANCE.equals("CASHNORM") && NOM_CLIENT.length() > 2) {
                        
                        curHeight = printS(pg, welcome, margin, curHeight, f, "CENTER");
                        curHeight = printS(pg, "Affiliate N°: " + NUM_AFF, margin, curHeight, f, "LEFT");
                        curHeight = printS(pg, "Client Name: " + NOM_CLIENT + " " + PRENOM_CLIENT, margin, curHeight, f, "LEFT");
                        curHeight = printS(pg, "Departement: " + EMPLOYEUR, margin, curHeight, f, "LEFT");
                        curHeight = printS(pg, "Prescription N°: " + QUITANCE, margin, curHeight, f, "LEFT");
                        curHeight = printS(pg, Client_ID, margin, curHeight, f, "LEFT");
                          if(IZINA_CLIENT2 !=null)
                        {
                            curHeight = printS(pg, " "+IZINA_CLIENT2, margin, curHeight, f, "CENTER");
                        }
                        curHeight = printS(pg, l(Main.SDCLANG, "V_SERVED")+"" + UMUSERVEUR, margin, curHeight, f, "CENTER");
                        curHeight = printS(pg, separateur, margin, curHeight, f, "CENTER");
                        
                    } else {
                        curHeight = printS(pg, welcome, margin, curHeight, f, "CENTER");
                        if(IZINA_CLIENT2 !=null)
                        {
                            curHeight = printS(pg, " "+IZINA_CLIENT2, margin, curHeight, f, "CENTER");
                        }
                        
                      
                            curHeight = printS(pg, Client_ID, margin, curHeight, f, "CENTER");
                         
                        
                        curHeight = printS(pg, l(Main.SDCLANG, "V_SERVED")+"" + UMUSERVEUR, margin, curHeight, f, "CENTER");
                        curHeight = printS(pg, separateur, margin, curHeight, f, "CENTER");
                    }
                }
                
                System.out.println("IZINA_CLIENT  "+IZINA_CLIENT2);
                System.out.println("hapa2");
//////////////////////////////////////////////////////////////////////////////////////////////////////
                f = new Font("Arial", Font.PLAIN, 8);
                FontMetrics fm = pg.getFontMetrics(f);
                int fontHeight = fm.getHeight();
                for (int i = 0; i < getProductListDB.size(); i++) {
                    Product p = (Product) getProductListDB.get(i);

                    String name = p.productName;
                    System.out.println("name::"+name);
                    if (name != null && name.contains("#")) {

                        String[] sp = name.split("#");

                        String newProd = sp[0].substring(0, Math.min(sp[0].length(), size));
                        int l = size - newProd.length();
                        name = newProd + "" + getSpaces(l) + setVirgule(p.currentPrice, NEG,",") + "X ";
                        curHeight = printS(pg, name, margin, curHeight, f, "LEFT");
                        curHeight -= fontHeight;
                        f = new Font("Arial", Font.BOLD, 8);
                        printInt(pg, "" + p.qty, west - 110, curHeight, f);
                        f = new Font("Arial", Font.PLAIN, 8);
                        printInt(pg, "" + setVirgule((p.originalPrice()), NEG,",") + "" + p.classe, west, curHeight, f);
                        curHeight += fontHeight;
                        printInt(pg, "" + setVirgule((p.salePrice()), NEG,",") + "" + p.classe, west, curHeight, f);
                        curHeight = printS(pg, sp[1], margin, curHeight, f, "LEFT");

                    } else {
                        String newProd = name.substring(0, Math.min(name.length(), size));
                        int l = size - newProd.length();
                        name = newProd + "" + getSpaces(l) + setVirgule(p.currentPrice, NEG,",") + "X ";
                        curHeight = printS(pg, name, margin, curHeight, f, "LEFT");
                        curHeight -= fontHeight;
                        f = new Font("Arial", Font.BOLD, 8);
                        printInt(pg, "" + p.qty, west - 110, curHeight, f);
                        f = new Font("Arial", Font.PLAIN, 8);
                        printInt(pg, "" + setVirgule((p.salePrice()  ), NEG,",") + "" + p.classe, west, curHeight, f);
                        curHeight += fontHeight;
                    }
                }
                System.out.println("hapa3");
/////////////////////////////////////////////////////////////////////////////////////////////////////////
                f = new Font("Arial", Font.PLAIN, boldEpson);

                if (typeSales.contains("P") || typeSales.contains("C") || typeSales.contains("T")) {
                    curHeight = printS(pg, separateur, margin, curHeight, f, "CENTER");
                    f = new Font("Arial", Font.BOLD, title);
                    curHeight = printS(pg, "THIS IS NOT AN OFFICIAL RECEIPT ", margin, curHeight, f, "CENTER");
                }
                f = new Font("Arial", Font.PLAIN, boldEpson);
                curHeight = printS(pg, separateur, margin, curHeight, f, "CENTER");
                f = new Font("Arial", Font.BOLD, boldEpson);

                System.out.println(TOTAL);
                
                String running=l(Main.SDCLANG, "V_DEVISE_LOCAL");
                if(running==null)
                {
                    running="";
                }
                printInt(pg, "" + setVirgule(TOTAL, NEG, ","), west, curHeight, f);
                curHeight = printS(pg, l(Main.SDCLANG, "V_TOTAL")+" "+running, margin, curHeight, f, "LEFT");

                String indiDevise=l(Main.SDCLANG, "V_CONGO");
                String exchangeRate=l(Main.SDCLANG, "V_CONGO_USD_RATE");
                
                
                boolean isDouble=true;
                double rate=1;
                try
                {
                    rate=Double.parseDouble(exchangeRate);
                }
                catch (Exception NumberFormatException)
                {isDouble=false;
                  System.out.println(NumberFormatException+" NumberFormatException "+exchangeRate);
                }
                
                if(indiDevise!=null && indiDevise.length()>2 && isDouble)
                {
                    double localTotal=TOTAL*rate;
                System.out.println(localTotal);
                printInt(pg, "" + setVirgule(localTotal, NEG, ","), west, curHeight, f);
                curHeight = printS(pg, l(Main.SDCLANG, "V_TOTAL")+" "+indiDevise, margin, curHeight, f, "LEFT");
  
                }
                

                f = new Font("Arial", Font.PLAIN, boldEpson);
                printInt(pg, "" + setVirgule(TOTAL_A_EX, NEG, ","), west, curHeight, f);
                curHeight = printS(pg,l(Main.SDCLANG, "V_TOTAL_A-EX")+" ", margin, curHeight, f, "LEFT");
                printInt(pg, "" + setVirgule(TOTAL_B_18, NEG, ","), west, curHeight, f);
                curHeight = printS(pg, l(Main.SDCLANG, "V_TOTAL_B")+" ", margin, curHeight, f, "LEFT");
                printInt(pg, "" + setVirgule(TOTAL_TAX_B, NEG, ","), west, curHeight, f);
                curHeight = printS(pg, l(Main.SDCLANG, "V_TOTAL_TAX_B")+" ", margin, curHeight, f, "LEFT");
                printInt(pg, "" + setVirgule(TOTAL_TAX, NEG, ","), west, curHeight, f);
                curHeight = printS(pg,l(Main.SDCLANG, "V_TOTAL_TAX")+" ", margin, curHeight, f, "LEFT");
System.out.println("hapa4");
                curHeight = printS(pg, separateur, margin, curHeight, f, "CENTER");
                if (typeSales.contains("P")) {
                    curHeight = printS(pg, "PROFORMA", margin, curHeight, f, "CENTER");
                } else if (typeSales.contains("C")) {
                    f = new Font("Arial", Font.BOLD, boldEpson);
                    curHeight = printS(pg, "COPY", margin, curHeight, f, "CENTER");
                    curHeight = printS(pg, separateur, margin, curHeight, f, "CENTER");
                } else if (typeSales.contains("T")) {
                    f = new Font("Arial", Font.BOLD, boldEpson);
                    curHeight = printS(pg, "TRAINING MODE", margin, curHeight, f, "CENTER");
                    curHeight = printS(pg, separateur, margin, curHeight, f, "CENTER");
                }

                if (typeSales.contains("N") || typeSales.contains("C") || typeSales.contains("T")) {
                    f = new Font("Arial", Font.BOLD, boldEpson);
                    if (infoClient != null && !ASSURANCE.equals("CASHNORM") && NOM_CLIENT.length() > 2) {
                        double p = (((CASH * PERCENTAGE) * 0.01));
                        
                        if(PERCENTAGE_PART>0)
                        {
                            p = (((CASH * PERCENTAGE_PART) * 0.01));
                            PERCENTAGE=PERCENTAGE_PART;
                            p=CASH-p;
                        }
                        
                        curHeight = printS(pg, l(Main.SDCLANG, "V_CASH")+" " + setVirgule((100-PERCENTAGE),NEG, ",") + "%: " + setVirgule(p, NEG, ","), margin, curHeight, f, "CENTER");
                        curHeight = printS(pg, ASSURANCE + " " + setVirgule((PERCENTAGE),NEG, ",") + "% : " + setVirgule(CASH - p, NEG, ","), margin, curHeight, f, "CENTER");
                        curHeight = printS(pg, l(Main.SDCLANG, "V_ITEMS_NUMBER")+" " + getProductListDB.size(), margin, curHeight, f, "CENTER");
                    } else {
                        curHeight = printS(pg, l(Main.SDCLANG, "V_CASH")+" " + setVirgule(CASH, NEG, ","), margin, curHeight, f, "CENTER");
                        curHeight = printS(pg, l(Main.SDCLANG, "V_ITEMS_NUMBER")+" " + getProductListDB.size(), margin, curHeight, f, "CENTER");

                    }
                }
                
                if(MONEY>0)
                {
                    curHeight = printS(pg, separateur, margin, curHeight, f, "CENTER");
                    curHeight = printS(pg, l(Main.SDCLANG, "V_CASH")+": " + setVirgule(MONEY, NEG, ","), margin, curHeight, f, "LEFT");
                    curHeight = printS(pg, l(Main.SDCLANG, "V_SUBIZA")+": " + setVirgule(CHANGE, NEG, ","), margin, curHeight, f, "LEFT");
                }
                
                
                
                System.out.println("hapa5");
                f = new Font("Arial", Font.PLAIN, boldEpson);
                if(Main.tax.rraClient.isRRA)
                {
                curHeight = printS(pg, separateur, margin, curHeight, f, "CENTER");
                curHeight = printS(pg, SDC_INFORMATION, margin, curHeight, f, "CENTER");
                curHeight = printS(pg,  l(Main.SDCLANG, "V_TIME")+"" + date_time_sdc, margin, curHeight, f, "LEFT");
                curHeight = printS(pg,  l(Main.SDCLANG, "V_SDC_ID")+"" + SDC_ID, margin, curHeight, f, "LEFT");
                curHeight = printS(pg,  l(Main.SDCLANG, "V_RECEIPT_NUMBER")+"" + RECEIPT_NUMBER, margin, curHeight, f, "LEFT");

                if (typeSales.contains("N") || typeSales.contains("C")) {
                    curHeight = printS(pg,  l(Main.SDCLANG, "V_INTERNAL_DATA")+"", margin, curHeight, f, "CENTER");
                    curHeight = printS(pg, set4Virgule(Internal_Data), margin, curHeight, f, "CENTER");
                    curHeight = printS(pg,  l(Main.SDCLANG, "V_RECEIPT_SIGNATURE")+" ", margin, curHeight, f, "CENTER");
                    curHeight = printS(pg, set4Virgule(Receipt_Signature), margin, curHeight, f, "CENTER");
                }
                }

                curHeight = printS(pg, separateur, margin, curHeight, f, "CENTER");
                curHeight = printS(pg,  l(Main.SDCLANG, "V_INVOICE_NUMBER")+" " + RECEIPT_NUMBER2, margin, curHeight, f, "LEFT");
                curHeight = printS(pg,  l(Main.SDCLANG, "V_MRC_TIME")+" " + date_time_mrc, margin, curHeight, f, "LEFT");
                curHeight = printS(pg,  l(Main.SDCLANG, "V_MRC") + MRC, margin, curHeight, f, "LEFT");

                curHeight = printS(pg, separateur, margin, curHeight, f, "CENTER");

//                curHeight = printS(pg, ""+getString("unsatisfied"), margin, curHeight,new Font("Arial", Font.BOLD, boldEpson), "CENTER");
                curHeight = printS(pg, tks, margin, curHeight, new Font("Arial", Font.BOLD, 8), "CENTER");                
                curHeight = printS(pg,  l(Main.SDCLANG, "V_PRINTED")+"", margin, curHeight, f, "CENTER"); 
                
                

//pg.drawRect(margin-5,margin-5, W-25,curHeight);
                pg.dispose();
            }
            pjob.end();
            return true;
        } else {
            return false;
        }
    }

    String setString(String toSet, String side) {

        if (side.equals("CENTER")) {
            int leCentre = (40 / 2);
            int size = toSet.length();
            if (size > 2) {
                int hagati = (size) / 2;
                return printBackwardBlanc2(toSet, leCentre - hagati) + toSet;
            } else {
                return marge + toSet;
            }
        } else {
            return marge + toSet;
        }
    }

    public void makeEjRRA() {
        ejList = new LinkedList<String>();
        ejList.add(setString(Trade_Name, "CENTER"));
        ejList.add(setString(Address, "CENTER"));
        ejList.add(setString(City, "CENTER"));
        ejList.add(setString(TIN, "CENTER"));
        ejList.add(setString(separateur, "CENTER"));
        int NEG = 1;
        if (typeSales.contains("R")) {
            ejList.add(setString("REFUND", "CENTER"));
            ejList.add(setString("REF.NORMAL RECEIPT#:" + RECEIPT_NUMBER2, "CENTER"));
            ejList.add(setString(separateur, "CENTER"));
            ejList.add(setString("REFUND IS APPROVED ONLY FOR", "CENTER"));
            ejList.add(setString("ORIGINAL SALES RECEIPT", "CENTER"));
            ejList.add(setString(Client_ID, "CENTER"));
            ejList.add(setString(separateur, "CENTER"));
            NEG = -1;
        } else {
            ejList.add(setString(welcome, "CENTER"));
            ejList.add(setString(Client_ID, "CENTER"));
            ejList.add(setString(separateur, "CENTER"));
        }

        for (int i = 0; i < getProductListDB.size(); i++) {
            Product p = (Product) getProductListDB.get(i);
            int west_qty = 21;
            int west_Tot = 21;
            int west_Tot_Disc = 42;
            String name = p.productName;
            if (name != null && name.contains("#")) {
                String[] sp = name.split("#");
                String newProd = sp[0].substring(0, Math.min(sp[0].length(), size));
                int l = size - newProd.length();
                ejList.add(setString(sp[0], "LEFT"));
                String UnityPrice = setVirgule(p.currentPrice, NEG,",") + "X";
                String qty = "" + p.qty;
                int space_qty = west_qty - UnityPrice.length() - qty.length();
                String TotalPrice = setVirgule(p.originalPrice(), NEG,",") + p.classe;
                int space_tot = west_Tot - TotalPrice.length();

                ejList.add(setString(
                        UnityPrice
                        + getSpaces(space_qty)
                        + qty
                        + getSpaces(space_tot)
                        + TotalPrice, "LEFT"));

                String originalPrice = setVirgule((p.salePrice()), NEG,",") + "" + p.classe;
                int space_ori = west_Tot_Disc - sp[1].length() - originalPrice.length();

                ejList.add(setString(sp[1]
                        + getSpaces(space_ori)
                        + originalPrice, "LEFT"));
            } else {
                ejList.add(setString(name, "LEFT"));

                String UnityPrice = setVirgule(p.currentPrice, NEG,",") + "X";
                String qty = "" + p.qty;
                int space_qty = west_qty - UnityPrice.length() - qty.length();
                String TotalPrice = setVirgule(p.salePrice(), NEG,",") + p.classe;
                int space_tot = west_Tot - TotalPrice.length();

                ejList.add(setString(
                        UnityPrice
                        + getSpaces(space_qty)
                        + qty
                        + getSpaces(space_tot)
                        + TotalPrice, "LEFT"));

            }

        }

        if (typeSales.contains("P") || typeSales.contains("C") || typeSales.contains("T")) {
            ejList.add(setString(separateur, "CENTER"));
            ejList.add(setString("THIS IS NOT AN OFFICIAL RECEIPT ", "CENTER"));
        }
        ejList.add(setString(separateur, "CENTER"));

        int west_Tot = 42;
        String appelation = "TOTAL";
        String tot = setVirgule(TOTAL, NEG, ",");
        int space_tot = west_Tot - appelation.length() - tot.length();
        ejList.add(setString(appelation + getSpaces(space_tot) + tot, "LEFT"));

        appelation = "TOTAL A-EX";
        tot = setVirgule(TOTAL_A_EX, NEG, ",");
        space_tot = west_Tot - appelation.length() - tot.length();
        ejList.add(setString(appelation + getSpaces(space_tot) + tot, "LEFT"));

        appelation = l(Main.SDCLANG, "V_TOTAL_B");
        tot = setVirgule(TOTAL_B_18, NEG, ",");
        space_tot = west_Tot - appelation.length() - tot.length();
        ejList.add(setString(appelation + getSpaces(space_tot) + tot, "LEFT"));

        appelation = "TOTAL B";
        tot = setVirgule(TOTAL_TAX_B, NEG, ",");
        space_tot = west_Tot - appelation.length() - tot.length();
        ejList.add(setString(appelation + getSpaces(space_tot) + tot, "LEFT"));

        appelation = "TOTAL TAX";
        tot = setVirgule(TOTAL_TAX, NEG, ",");
        space_tot = west_Tot - appelation.length() - tot.length();
        ejList.add(setString(appelation + getSpaces(space_tot) + tot, "LEFT"));

        ejList.add(setString(separateur, "CENTER"));

        if (typeSales.contains("P")) {
            ejList.add(setString("PROFORMA", "CENTER"));
        } else if (typeSales.contains("C")) {
            ejList.add(setString("COPY", "CENTER"));

        } else if (typeSales.contains("T")) {
            ejList.add(setString("TRAINING MODE", "CENTER"));
        } else {
            ejList.add(setString("CASH: " + setVirgule(CASH, NEG, ","), "CENTER"));
            ejList.add(setString("ITEMS NUMBER: " + getProductListDB.size(), "CENTER"));
        }

        ejList.add(setString(separateur, "CENTER"));
        ejList.add(setString(SDC_INFORMATION, "CENTER"));
        ejList.add(setString("TIME SDC: " + date_time_sdc, "LEFT"));
        ejList.add(setString("SDC ID: " + SDC_ID, "LEFT"));
        ejList.add(setString("RECEIPT NUMBER: " + RECEIPT_NUMBER, "LEFT"));

        if (typeSales.contains("N") || typeSales.contains("C")) {
            ejList.add(setString("Internal Data:", "CENTER"));
            ejList.add(setString(Internal_Data, "CENTER"));
            ejList.add(setString("Receipt Signature:", "CENTER"));
            ejList.add(setString(set4Virgule(Receipt_Signature), "CENTER"));
        }

        ejList.add(setString(separateur, "CENTER"));
        ejList.add(setString("INVOICE NUMBER: " + RECEIPT_NUMBER2, "LEFT"));
        ejList.add(setString("TIME MRC: " + date_time_mrc, "LEFT"));
        ejList.add(setString("MRC: " + MRC, "LEFT"));

        ejList.add(setString(separateur, "CENTER"));
        ejList.add(setString(tks, "CENTER"));
        ejList.add(setString(comeback, "CENTER"));
        ejList.add(setString(best, "CENTER"));

    }

    public static String getSpaces(int l) {
        String s = "";
        for (int i = 0; i < l; i++) {
            s += " ";
        }
        return s;
    }

    public static void printBackward(Graphics g, Font f, String s, int w, int h) {
        int back = 0;
        for (int i = s.length() - 1; i >= 0; i--) {
            char c = s.charAt(i);
            FontMetrics fm = g.getFontMetrics(f);
            back += fm.charWidth(c);
            g.drawString("" + c, w - back, h);
        }
    }

    public static String printBackwardBlanc2(String s, int withKuzuza) {
        String res = "";
        char space = ' ';
        int back = 0;
        while (back < withKuzuza) {
            res = res + space;
            back++;
        }
        return res;
    }

    public static String printBackwardBlanc(FontMetrics fm, String s, int withKuzuza) {
        String res = "";
        int back = 0;
        char space = ' ';
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            back += fm.charWidth(c);
        }
        while (back < withKuzuza) {
            res = res + space;
            back += fm.charWidth(space);
        }
        return res;
    }

    int printS(Graphics pg, String s, int w, int curHeight, Font f, String location) {
        if (s == null) {
            return curHeight;
        } else {
            pg.setFont(f);
            FontMetrics fm = pg.getFontMetrics(f);
            int fontHeight = fm.getHeight();

            if (location.equals("CENTER")) {
                int leCentre = (west / 2);
                int size = s.length();
                if (size > 2) {
                    int hagati = (size) / 2;
                    printBackward(pg, f, s.substring(0, hagati), leCentre, curHeight);
                    pg.drawString(s.substring(hagati, s.length()), leCentre, curHeight);
                } else {
                    pg.drawString(s, margin, curHeight);
                }
                curHeight += fontHeight;
            } else {
                if (s.length() > size) {
                    //nextLineE = nextLineE.substring(0, 34) ;
                    pg.drawString(s.substring(0, (size - 1)), w, curHeight);
                    curHeight += fontHeight;
                    s = s.substring((size - 1), s.length());
                }
                pg.drawString(s, w, curHeight);
                curHeight += fontHeight;
            }
            return curHeight;
        }
    }
 
    public void printInt(Graphics pg, String s, int w, int h, Font f) {
        pg.setFont(f);
        int back = 0;

        for (int i = s.length() - 1; i >= 0; i--) {
            if (s.charAt(i) == ',' || s.charAt(i) == '.') {
                back += 2;
            } else {
                back += 5;
            }
            pg.drawString("" + s.charAt(i), w - back, h);
        }
    }

    public static String setSansVirgule(double frw) {
        return setVirgule(frw, 1, ",").replaceAll(",", "");

    }

    public static String setVirgule(double frw, int NEG, String separ) {

        String sep2=".";
        if(separ.equals("."))
            sep2=",";
//        System.out.println("frw:"+frw);
//        if (arr == 0.005) {
            frw = frw + Main.ARRONDI;
//        }
//        System.out.println("frw arre:"+frw);
        BigDecimal big = new BigDecimal(frw);
//        big = big.setScale(2, BigDecimal.ROUND_UP);
        String getBigString = big.toString();
//        System.out.println("big:"+getBigString);
        getBigString = getBigString.replace('.', ';');
//  System.out.println("after;"+getBigString);
        String[] sp = getBigString.split(";");

        String decimal = "";
        if (sp.length > 1) {
            decimal = sp[1];
        }

        if (decimal.equals("") || (decimal.length() == 1 && decimal.equals("0"))) {
            decimal = sep2+"00";
        } else if (decimal.length() == 1 && !decimal.equals("0")) {
            decimal = sep2+"" + decimal + "0";
        } else {
            decimal = sep2+"" + decimal.substring(0, 2);
        }


        String toret = setVirgule(sp[0],separ) + decimal;

        if (NEG == -1 && frw > 0) {
            toret = "-" + toret;
        }

        return toret;
    }

    public static String setVirgule(String frw,String separateur) {
        String setString = "";
        int k = 0;
        for (int i = (frw.length() - 1); i >= 0; i--) {
            k++;
            if ((k - 1) % 3 == 0 && (k - 1) != 0) {
                setString += ""+separateur;
            }
            setString += frw.charAt(i);
        }
        return inverse(setString);
    }

    public static String set4Virgule(String frw) {
        String setString = "";
        int k = 0;
        for (int i = (frw.length() - 1); i >= 0; i--) {
            k++;
            if ((k - 1) % 4 == 0 && (k - 1) != 0) {
                setString += "-";
            }
            setString += frw.charAt(i);
        }
        return inverse(setString);
    }

    static String inverse(String frw) {
        String l = "";
        for (int i = (frw.length() - 1); i >= 0; i--) {
            l += frw.charAt(i);
        }
        return l;
    }

 private boolean PrintCanvas() {  
        System.setProperty("java.awt.headless", "true"); 
        Toolkit tk = Toolkit.getDefaultToolkit(); 
        tk.beep(); 
        GraphicsEnvironment ge = 
        GraphicsEnvironment.getLocalGraphicsEnvironment();
        System.out.println("Headless mode: " + ge.isHeadless()); 
        Image i = null;
        try
        { File f = new File("rraLogo.gif");  i = ImageIO.read(f); }
        catch (Exception z)
        { z.printStackTrace(System.err);  }
        final Image im = i; 
        PrinterJob pj = PrinterJob.getPrinterJob(); 
        PageFormat pageFormat = pj.defaultPage();  
        Paper paper = pageFormat.getPaper();  
        paper.setImageableArea(0, 0, paper.getWidth(), paper.getHeight());  
        pageFormat.setPaper(paper);
        pj.validatePage(pageFormat); 
        
        pj.setPrintable(new MyPrintable(), pageFormat);  
        try { pj.print(); }
        catch (Exception z)
        { z.printStackTrace(System.err); return false;  } 

        return true;
}
 public String l(String lang,String var)
{
    String value=""; 
    try
    {
       // System.out.println(" select  * from APP.LANGUE where variable= '"+var+"'");
      ResultSet  outResult = stat.executeQuery(" select  * from APP.LANGUE where variable= '"+var+"'");
        //System.out.println("  select  * from APP.LANGUE where variable= '"+var+"'");
        while (outResult.next())
        { 
            value =outResult.getString(lang);
            // System.out.println(value);
        } 
    }
    catch (Throwable e)
    {
      System.err.println(e+"langue");  //insertError(e+"","langue");
    }
    
    return  value;
}
 public String getString(String s)
{ 
 try
{
 ResultSet outResult = stat.executeQuery("select  * from APP.VARIABLES where "
        + "APP.variables.nom_variable= '"+s+"MAIN"+"'  and APP.variables.famille_variable= 'RUN' ");
//System.out.println("select  * from APP.VARIABLES where APP.variables.nom_variable= '"+nom+"'  and APP.variables.famille_variable= '"+famille+"' ");

while (outResult.next())
{
  //  System.out.println(outResult.getString(2));
        String num =outResult.getString(1);
        return outResult.getString(2); 
}}
catch (Throwable e){
      System.out.println(e+" getparm autoout");
 }
return     "";
} 
  
 
  
 
class MyPrintable implements Printable {   
    
        public int print(Graphics pg, PageFormat pf, int pageIndex) {  
            if (pageIndex != 0)  
              return 1;
            
                int curHeight = margin + 10;
                int dim=55;
               // if(Main.tax.rraClient.isRRA) {
                    pg.drawImage(Db.rraLogo, margin, curHeight-10,dim,45, rootPane);
               // }

                Font f = new Font("Arial", Font.BOLD, title);
                curHeight = printS(pg, Trade_Name, margin+dim, curHeight, f, "LEFT");
                f = new Font("Arial", Font.PLAIN, boldEpson);
                curHeight = printS(pg, Address, margin+dim, curHeight, f, "LEFT");
                curHeight = printS(pg, City, margin+dim, curHeight, f, "LEFT");
                curHeight = printS(pg, TIN, margin+dim, curHeight, f, "LEFT");
                curHeight = printS(pg, separateur, margin, curHeight, f, "CENTER");
                
                System.out.println("hapa1");

                if (transaction.contains("R")) {
                    curHeight = printS(pg, "REFUND", margin, curHeight, f, "CENTER");
                    curHeight = printS(pg, "REF.NORMAL RECEIPT#: " + REFUND_REF, margin, curHeight, f, "CENTER");
                    curHeight = printS(pg, separateur, margin, curHeight, f, "CENTER");
                    curHeight = printS(pg, "REFUND IS APPROVED ONLY FOR", margin, curHeight, f, "CENTER");
                    curHeight = printS(pg, "ORIGINAL SALES RECEIPT", margin, curHeight, f, "CENTER");
                    curHeight = printS(pg, Client_ID, margin, curHeight, f, "CENTER");
                    curHeight = printS(pg, separateur, margin, curHeight, f, "CENTER");
                } else if (typeSales.contains("P")) {
                    curHeight = printS(pg, welcome, margin, curHeight, f, "CENTER");
                    curHeight = printS(pg, "Client Name: " + NOM_CLIENT + " ", margin, curHeight, f, "CENTER");
                    curHeight = printS(pg, "Reference: " + ASSURANCE + ":" + PRENOM_CLIENT, margin, curHeight, f, "CENTER");
                    curHeight = printS(pg, Client_ID, margin, curHeight, f, "CENTER");
                      if(IZINA_CLIENT2 !=null)
                        {
                            curHeight = printS(pg, " "+IZINA_CLIENT2, margin, curHeight, f, "CENTER");
                        }
                    curHeight = printS(pg, l(Main.SDCLANG, "V_SERVED")+"" + UMUSERVEUR, margin, curHeight, f, "CENTER");
                    curHeight = printS(pg, separateur, margin, curHeight, f, "CENTER");

                } else {
                    if (infoClient != null && !ASSURANCE.equals("CASHNORM") && NOM_CLIENT.length() > 2) {
                        curHeight = printS(pg, welcome, margin, curHeight, f, "CENTER");
                        curHeight = printS(pg, "Affiliate N°: " + NUM_AFF, margin, curHeight, f, "LEFT");
                        curHeight = printS(pg, "Client Name: " + NOM_CLIENT + " " + PRENOM_CLIENT, margin, curHeight, f, "LEFT");
                        curHeight = printS(pg, "Departement: " + EMPLOYEUR, margin, curHeight, f, "LEFT");
                        curHeight = printS(pg, "Prescription N°: " + QUITANCE, margin, curHeight, f, "LEFT");
                        curHeight = printS(pg, Client_ID, margin, curHeight, f, "LEFT");
                          if(IZINA_CLIENT2 !=null)
                        {
                            curHeight = printS(pg, " "+IZINA_CLIENT2, margin, curHeight, f, "CENTER");
                        }
                        curHeight = printS(pg, l(Main.SDCLANG, "V_SERVED")+"" + UMUSERVEUR, margin, curHeight, f, "CENTER");
                        curHeight = printS(pg, separateur, margin, curHeight, f, "CENTER");
                    } else {
                        curHeight = printS(pg, welcome, margin, curHeight, f, "CENTER");
                         if(IZINA_CLIENT2 !=null)
                        {
                            curHeight = printS(pg, " "+IZINA_CLIENT2, margin, curHeight, f, "CENTER");
                        }
                        
                            curHeight = printS(pg, Client_ID, margin, curHeight, f, "CENTER");
                       
                        curHeight = printS(pg, l(Main.SDCLANG, "V_SERVED")+"" + UMUSERVEUR, margin, curHeight, f, "CENTER");
                        curHeight = printS(pg, separateur, margin, curHeight, f, "CENTER");
                    }
                }
                System.out.println("hapa2");
//////////////////////////////////////////////////////////////////////////////////////////////////////
                f = new Font("Arial", Font.PLAIN, 8);
                FontMetrics fm = pg.getFontMetrics(f);
                int fontHeight = fm.getHeight();
                for (int i = 0; i < getProductListDB.size(); i++) {
                    Product p = (Product) getProductListDB.get(i);

                    String name = p.productName;
                    System.out.println("name::"+name);
                    if (name != null && name.contains("#")) {

                        String[] sp = name.split("#");

                        String newProd = sp[0].substring(0, Math.min(sp[0].length(), size));
                        int l = size - newProd.length();
                        name = newProd + "" + getSpaces(l) + setVirgule(p.currentPrice, NEG,",") + "X ";
                        curHeight = printS(pg, name, margin, curHeight, f, "LEFT");
                        curHeight -= fontHeight;
                        f = new Font("Arial", Font.BOLD, 8);
                        printInt(pg, "" + p.qty, west - 110, curHeight, f);
                        f = new Font("Arial", Font.PLAIN, 8);
                        printInt(pg, "" + setVirgule((p.originalPrice()), NEG,",") + "" + p.classe, west, curHeight, f);
                        curHeight += fontHeight;
                        printInt(pg, "" + setVirgule((p.salePrice()  ), NEG,",") + "" + p.classe, west, curHeight, f);
                        curHeight = printS(pg, sp[1], margin, curHeight, f, "LEFT");

                    } else {
                        String newProd = name.substring(0, Math.min(name.length(), size));
                        int l = size - newProd.length();
                        name = newProd + "" + getSpaces(l) + setVirgule(p.currentPrice, NEG,",") + "X ";
                        curHeight = printS(pg, name, margin, curHeight, f, "LEFT");
                        curHeight -= fontHeight;
                        f = new Font("Arial", Font.BOLD, 8);
                        printInt(pg, "" + p.qty, west - 110, curHeight, f);
                        f = new Font("Arial", Font.PLAIN, 8);
                        printInt(pg, "" + setVirgule((p.salePrice()  ), NEG,",") + "" + p.classe, west, curHeight, f);
                        curHeight += fontHeight;
                    }
                }
                System.out.println("hapa3");
/////////////////////////////////////////////////////////////////////////////////////////////////////////
                f = new Font("Arial", Font.PLAIN, boldEpson);

                if (typeSales.contains("P") || typeSales.contains("C") || typeSales.contains("T")) {
                    curHeight = printS(pg, separateur, margin, curHeight, f, "CENTER");
                    f = new Font("Arial", Font.BOLD, title);
                    curHeight = printS(pg, "THIS IS NOT AN OFFICIAL RECEIPT ", margin, curHeight, f, "CENTER");
                }
                f = new Font("Arial", Font.PLAIN, boldEpson);
                curHeight = printS(pg, separateur, margin, curHeight, f, "CENTER");
                f = new Font("Arial", Font.BOLD, boldEpson);

                System.err.println(TOTAL);
                printInt(pg, "" + setVirgule(TOTAL, NEG, ","), west, curHeight, f);
                curHeight = printS(pg, l(Main.SDCLANG, "V_TOTAL")+" ", margin, curHeight, f, "LEFT");


                f = new Font("Arial", Font.PLAIN, boldEpson);
                printInt(pg, "" + setVirgule(TOTAL_A_EX, NEG, ","), west, curHeight, f);
                curHeight = printS(pg,l(Main.SDCLANG, "V_TOTAL_A-EX")+" ", margin, curHeight, f, "LEFT");
                printInt(pg, "" + setVirgule(TOTAL_B_18, NEG, ","), west, curHeight, f);
                curHeight = printS(pg, l(Main.SDCLANG, "V_TOTAL_B")+" ", margin, curHeight, f, "LEFT");
                printInt(pg, "" + setVirgule(TOTAL_TAX_B, NEG, ","), west, curHeight, f);
                curHeight = printS(pg, l(Main.SDCLANG, "V_TOTAL_TAX_B")+" ", margin, curHeight, f, "LEFT");
                printInt(pg, "" + setVirgule(TOTAL_TAX, NEG, ","), west, curHeight, f);
                curHeight = printS(pg,l(Main.SDCLANG, "V_TOTAL_TAX")+" ", margin, curHeight, f, "LEFT");
System.out.println("hapa4");
                curHeight = printS(pg, separateur, margin, curHeight, f, "CENTER");
                if (typeSales.contains("P")) {
                    curHeight = printS(pg, "PROFORMA", margin, curHeight, f, "CENTER");
                } else if (typeSales.contains("C")) {
                    f = new Font("Arial", Font.BOLD, boldEpson);
                    curHeight = printS(pg, "COPY", margin, curHeight, f, "CENTER");
                    curHeight = printS(pg, separateur, margin, curHeight, f, "CENTER");
                } else if (typeSales.contains("T")) {
                    f = new Font("Arial", Font.BOLD, boldEpson);
                    curHeight = printS(pg, "TRAINING MODE", margin, curHeight, f, "CENTER");
                    curHeight = printS(pg, separateur, margin, curHeight, f, "CENTER");
                }

                if (typeSales.contains("N") || typeSales.contains("C") || typeSales.contains("T")) {
                    f = new Font("Arial", Font.BOLD, boldEpson);
                    if (infoClient != null && !ASSURANCE.equals("CASHNORM") && NOM_CLIENT.length() > 2) {
                        double p = (((CASH * PERCENTAGE) * 0.01));
                        if(PERCENTAGE_PART>0)
                        {
                            p = (((CASH * PERCENTAGE_PART) * 0.01));
                            PERCENTAGE=PERCENTAGE_PART;
                            p=CASH-p;
                        }
                        
                        curHeight = printS(pg, l(Main.SDCLANG, "V_CASH")+" " + setVirgule((100-PERCENTAGE),NEG ,",") + "%: " + setVirgule(p, NEG, ","), margin, curHeight, f, "CENTER");
                        curHeight = printS(pg, ASSURANCE + " " + setVirgule((PERCENTAGE),NEG ,",") + "% : " + setVirgule(CASH - p, NEG, ","), margin, curHeight, f, "CENTER");
                        curHeight = printS(pg, l(Main.SDCLANG, "V_ITEMS_NUMBER")+" " + getProductListDB.size(), margin, curHeight, f, "CENTER");
                    } else {
                        curHeight = printS(pg, l(Main.SDCLANG, "V_CASH")+" " + setVirgule(CASH, NEG, ","), margin, curHeight, f, "CENTER");
                        curHeight = printS(pg, l(Main.SDCLANG, "V_ITEMS_NUMBER")+" " + getProductListDB.size(), margin, curHeight, f, "CENTER");

                    }
                }
                
                if(MONEY>0)
                {
                    curHeight = printS(pg, separateur, margin, curHeight, f, "CENTER");
                    curHeight = printS(pg, l(Main.SDCLANG, "V_CASH")+" " + setVirgule(MONEY, NEG, ","), margin, curHeight, f, "LEFT");
                    curHeight = printS(pg, l(Main.SDCLANG, "V_SUBIZA")+" " + setVirgule(CHANGE, NEG, ","), margin, curHeight, f, "LEFT");
                }
                
                
                System.out.println("hapa5");
                f = new Font("Arial", Font.PLAIN, boldEpson);
                if(Main.tax.rraClient.isRRA)
                {
                curHeight = printS(pg, separateur, margin, curHeight, f, "CENTER");
                curHeight = printS(pg, SDC_INFORMATION, margin, curHeight, f, "CENTER");
                curHeight = printS(pg,  l(Main.SDCLANG, "V_TIME")+"" + date_time_sdc, margin, curHeight, f, "LEFT");
                curHeight = printS(pg,  l(Main.SDCLANG, "V_SDC_ID")+"" + SDC_ID, margin, curHeight, f, "LEFT");
                curHeight = printS(pg,  l(Main.SDCLANG, "V_RECEIPT_NUMBER")+"" + RECEIPT_NUMBER, margin, curHeight, f, "LEFT");

                if (typeSales.contains("N") || typeSales.contains("C")) {
                    curHeight = printS(pg,  l(Main.SDCLANG, "V_INTERNAL_DATA")+"", margin, curHeight, f, "CENTER");
                    curHeight = printS(pg, Internal_Data, margin, curHeight, f, "CENTER");
                    curHeight = printS(pg,  l(Main.SDCLANG, "V_RECEIPT_SIGNATURE")+" ", margin, curHeight, f, "CENTER");
                    curHeight = printS(pg, set4Virgule(Receipt_Signature), margin, curHeight, f, "CENTER");
                }
                }

                curHeight = printS(pg, separateur, margin, curHeight, f, "CENTER");
                curHeight = printS(pg,  l(Main.SDCLANG, "V_INVOICE_NUMBER")+" " + RECEIPT_NUMBER2, margin, curHeight, f, "LEFT");
                curHeight = printS(pg,  l(Main.SDCLANG, "V_MRC_TIME")+" " + date_time_mrc, margin, curHeight, f, "LEFT");
                curHeight = printS(pg,  l(Main.SDCLANG, "V_MRC") + MRC, margin, curHeight, f, "LEFT");

                curHeight = printS(pg, separateur, margin, curHeight, f, "CENTER");

//                curHeight = printS(pg, ""+getString("unsatisfied"), margin, curHeight,new Font("Arial", Font.BOLD, boldEpson), "CENTER");
                curHeight = printS(pg, tks, margin, curHeight, new Font("Arial", Font.BOLD, 8), "CENTER");                
                curHeight = printS(pg,  l(Main.SDCLANG, "V_PRINTED")+"", margin, curHeight, f, "CENTER"); 
                 
 return PAGE_EXISTS; 
        }    
     
 }
 
}
