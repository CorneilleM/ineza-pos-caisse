/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package rra;
  
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.JOptionPane;

/**
 *
 * @author Kimenyi
 */
public class SDC_CONTROLLER   {

 String driver = "org.apache.derby.jdbc.NetWorkDriver"; 
String connectionURL ;
ResultSet outResult ;
Connection conn = null;
Statement s;
PreparedStatement psInsert; 

  
public SDC_CONTROLLER(Statement s,Connection conn) 
    { 
    this.s=s;  
     this.conn=conn;  
    }
  
String rebaSDC(String message)
{  
 RRA_COMMAND   rcmd=null;  
 
    String [] sp=message.split("#"); 
     boolean isOk=true;
 if(sp!=null && sp.length==4)
 {
       System.out.println(" message "+message);
System.out.println("44444444444444444444444            "+sp[0]);
if(sp[0].equals("SEND RECEIPT"))
{ 
    System.out.println(sp[1]);
    String [] spMSG=sp[3].split("@");
    System.out.println(spMSG.length);
    
    if(spMSG!=null && spMSG.length==6  )
 {
       rcmd=new RRA_COMMAND
 (sp[0],sp[1],sp[2], sp[3] ,null);
    
     rcmd.KEY_INVOICE=spMSG[0];
     rcmd.TIME=spMSG[1];
     rcmd.TIN=spMSG[2];
     rcmd.R_TYPE=spMSG[3];
     rcmd.T_TYPE=spMSG[4];
     System.out.println("555555555555555555555555            "+spMSG[5]);
     try
     {
     rcmd.ID_INVOICE=Integer.parseInt(spMSG[5]);
     }
     catch (NullPointerException ne)
     {
         System.err.println(ne);
     isOk=false; 
     }
 }
} 
System.out.println(isOk +" isOk            "+rcmd); 

    if( isOk  && rcmd!=null && rcmd.CMD!=null && rcmd.CMD.equals("SEND RECEIPT") )
      {
          RRA_INVOICE ri=null;
                String MRC=rcmd.DATA.substring(2, 13);
                System.out.println("ttttttttttttttt   "+rcmd.R_TYPE);
                if((rcmd.R_TYPE.equals("P") && rcmd.T_TYPE.equals("S")))
                    {
                       ri=   insertProforma3(rcmd.KEY_INVOICE);  
                    }  
                else if((rcmd.R_TYPE.equals("N") && rcmd.T_TYPE.equals("S")) || 
                        (rcmd.R_TYPE.equals("T") && rcmd.T_TYPE.equals("S"))    )
                    {
                        ri=   insertInvoice3(rcmd.KEY_INVOICE);  
                    }
                
                else if((rcmd.R_TYPE.equals("N") && rcmd.T_TYPE.equals("R")) || 
                        (rcmd.R_TYPE.equals("T") && rcmd.T_TYPE.equals("R"))    )
                    {
                         ri=   insertAnnule3(rcmd.KEY_INVOICE);   
                    } 
                return ""+ri.id_invoice+"#"+ri.key+"#";
}
      else {
      System.err.println("AN ERROR MSG");
        
        return "AN ERROR MSG"; }
 }
    return " # #";
 } 
public RRA_INVOICE insertInvoice3(String keyIdInvoice)
{    
try
     { 
         psInsert = conn.prepareStatement("insert into APP.INVOICE "
                 + "(KEY_INVOICE,EXTERNAL_DATA )  values (?,? )");
         
         psInsert.setString(1,keyIdInvoice); 
          psInsert.setString(2,",NS,");
         psInsert.executeUpdate();
         psInsert.close(); 
          System.out.println(" ns  in  " );
          
         return getIdFacture(keyIdInvoice);
}  
 catch (Throwable e)  
 { 
     insertError(e+"","doFinishVente");
 } 
 return null;
}
public RRA_INVOICE insertProforma3(String keyIdInvoice )
{    
try
     { 
         psInsert = conn.prepareStatement("insert into APP.proforma "
                 + "(KEY_INVOICE )  values (? )");
         
         psInsert.setString(1,keyIdInvoice); 
         psInsert.executeUpdate();
         psInsert.close();
         
         
          return  getIdProfo(keyIdInvoice);
 
}  
 catch (Throwable e)  
 { 
     insertError(e+"","insertProforma");
 } 
 return null;
} 

public RRA_INVOICE insertAnnule3(  String key_invoice)
{
    
try
     { 
  
         psInsert = conn.prepareStatement("insert into APP.REFUND "
                 + "(  numero_affilie,key_invoice,id_invoice,EXTERNAL_DATA)  values (  ?,?,?,?)");
          
         psInsert.setString(1," ");
         psInsert.setString(2,key_invoice);
         psInsert.setInt(3,-1);
          psInsert.setString(4,",NR,");
         psInsert.executeUpdate();
         psInsert.close();
         System.out.println("insert into APP.REFUND  (  numero_affilie,key_invoice)  values (  ?,?)"+key_invoice);
         return  getIdRefund(key_invoice);
}  
 catch (Throwable e)  
 { 
     insertError(e+"","doFinishVente");
 } 
 return null;
} 
 public RRA_INVOICE  getIdFacture(String keyInvoice)
  { 
      try
      {
          outResult = s.executeQuery("select  ID_INVOICE,heure from APP.INVOICE WHERE KEY_INVOICE='"+keyInvoice+"'");
          while (outResult.next())
          {System.out.println(" heureb  "+outResult.getString(2) );
            return(new RRA_INVOICE(outResult.getInt(1),outResult.getString(2),keyInvoice));
          }  outResult.close();
      }
      catch (Throwable e)       
      {
        insertError(e+"","getIdfacture");
      }
     return null; 
  }
  public RRA_INVOICE  getIdFactureID(int idInvoice)
  { 
try
{
    System.out.println("select  EXTERNAL_DATA,heure from APP.INVOICE WHERE ID_INVOICE= "+idInvoice+" ");
    outResult = s.executeQuery("select  KEY_INVOICE,heure,EXTERNAL_DATA from APP.INVOICE WHERE ID_INVOICE= "+idInvoice+" ");
    while (outResult.next())
    { RRA_INVOICE ri =new RRA_INVOICE(idInvoice,outResult.getString(2),outResult.getString(1));
    ri.data=outResult.getString(3);
      return  ri;
    }  outResult.close();
}
      catch (Throwable e)       
      {
        insertError(e+"","getIdfacture");
      }
     return null; 
  }
    public RRA_INVOICE  getIdREFUNDID(int idInvoice)
  { 
try
{
    System.out.println("select  EXTERNAL_DATA,heure from APP.REFUND WHERE ID_REFUND= "+idInvoice+" ");
    outResult = s.executeQuery("select  KEY_INVOICE,heure,EXTERNAL_DATA from APP.REFUND WHERE ID_REFUND= "+idInvoice+" ");
    while (outResult.next())
    { RRA_INVOICE ri =new RRA_INVOICE(idInvoice,outResult.getString(2),outResult.getString(1));
    ri.data=outResult.getString(3);
      return  ri;
    }  outResult.close();
}
      catch (Throwable e)       
      {
        insertError(e+"","getIdfacture");
      }
     return null; 
  }
 public RRA_INVOICE  getIdProfo(String keyInvoice)
  { 
      try
      {
          outResult = s.executeQuery("select  ID_PROFORMA,heure from APP.PROFORMA"
                  + " WHERE KEY_INVOICE='"+keyInvoice+"'");
          while (outResult.next())
          {
            return(new RRA_INVOICE(outResult.getInt(1),outResult.getString(2),keyInvoice));
          }  outResult.close();
      }
      catch (Throwable e)       
      {
        insertError(e+"","getIdfacture");
      }
     return null; 
  }
  public RRA_INVOICE  getIdRefund(String keyInvoice)
  { 
      try
      {
          outResult = s.executeQuery("select  ID_REFUND,heure,id_invoice from APP.REFUND WHERE KEY_INVOICE='"+keyInvoice+"'");
          while (outResult.next())
          {
            return(new RRA_INVOICE(outResult.getInt(1),outResult.getString(2),keyInvoice,outResult.getInt(3)));
          }  outResult.close();
      }
      catch (Throwable e)       
      {
        insertError(e+"","getIdfacture");
      }
     return null; 
  }
 public RRA_INVOICE  getIdheureInvoice (int ID_INVOICE)
  { 
      try
      {
          outResult = s.executeQuery("select  ID_INVOICE,heure_copy from APP.INVOICE WHERE ID_INVOICE= "
                  +ID_INVOICE+" ");
          while (outResult.next())
          {
            return(new RRA_INVOICE(outResult.getInt(1),outResult.getString(2),""));
          }  outResult.close();
      }
      catch (Throwable e)       
      {
        insertError(e+"","getIdfacture");
      }
     return null; 
  }
 public RRA_INVOICE  getIdheureRefund (int ID_REFUND)
  { 
      try
      {
          outResult = s.executeQuery("select  ID_REFUND,heure_copy from APP.REFUND WHERE ID_REFUND= "
                  +ID_REFUND+" ");
          while (outResult.next())
          {
            return(new RRA_INVOICE(outResult.getInt(1),outResult.getString(2),""));
          }  outResult.close();
      }
      catch (Throwable e)       
      {
        insertError(e+"","getIdfacture");
      }
     return null; 
  }
 void insertError(String desi,String ERR)
    { 
        try {
            psInsert = conn.prepareStatement("insert into APP.ERROR(DESI,USERNAME)  values (?,?)");

            if(desi.length()>160)
                desi=desi.substring(0, 155);
             if(ERR.length()>160)
                ERR=ERR.substring(0, 155);  
            
            psInsert.setString(1,desi );
            psInsert.setString(2, ERR);  
            psInsert.executeUpdate();
            psInsert.close();
            JOptionPane.showMessageDialog(null,"  "+desi," Ikibazo ",JOptionPane.PLAIN_MESSAGE);
    
            
            
        } catch (Throwable ex) {
         JOptionPane.showMessageDialog(null,"  "+ex," Ikibazo ",JOptionPane.PLAIN_MESSAGE);   ;
        }
            
                    
          }
}