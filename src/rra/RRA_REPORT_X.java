/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package rra;

import pos.Db;
import java.awt.Font;
import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.StringTokenizer;

/**
 *
 * @author Kimenyi
 */
public class RRA_REPORT_X {
    Db db;
    String EMPLOYE;
    String hera,geza,tradename,tin,CIS,MRC ;
    double   tvaRateA,   tvaRateB,   tvaRateC,   tvaRateD;
    
    public RRA_REPORT_X(Db db,String hera, String geza, String tradename, 
            String tin, String CIS, String MRC,double tvaRateA, double tvaRateB, double tvaRateC, double tvaRateD,String EMPLOYE) {
        this.db=db;
        this.hera = hera;
        this.geza = geza;
        this.tradename = tradename;
        this.tin = tin;
        this.CIS = CIS;
        this.MRC = MRC;
        this.tvaRateA = tvaRateA;
        this.tvaRateB = tvaRateB;
        this.tvaRateC = tvaRateC;
        this.tvaRateD = tvaRateD;
        this.EMPLOYE=EMPLOYE;
    }

    
double totalSalesNS(String sql) 
{
return db.sum("invoice", "total", "EXTERNAL_DATA like '%,NS,%'", "HEURE >'"+hera+"' AND HEURE <'"+geza+"' "+sql);
} 
double totalSalesNSGoupe(String groupe,String sql)
{
return db.sum("invoice", "total", "EXTERNAL_DATA like '%,NS,%' and num_client='"+groupe+"' ",
        "HEURE >'"+hera+"' AND HEURE <'"+geza+"' "+sql);
}
double totalRefundNRGoupe(String groupe,String sql)
{
return db.sum("refund", "total", "EXTERNAL_DATA like '%,NS,%' and num_client='"+groupe+"' ",
        "HEURE >'"+hera+"' AND HEURE <'"+geza+"' "+sql);
}
int numberSalesNS(String sql)
{return db.count("invoice", "ID_INVOICE", "EXTERNAL_DATA like '%,NS,%'", "HEURE >'"+hera+"' AND HEURE <'"+geza+"' "+sql);}
double totalRefundNR(String sql)// including tax;
{
return db.sum("REFUND", "total", "EXTERNAL_DATA like '%,NR,%'", "HEURE >'"+hera+"' AND HEURE <'"+geza+"' "+sql);
} 
int  numberRefundNR(String sql)
{return db.count("REFUND", "ID_REFUND", "EXTERNAL_DATA like '%,NR,%'", "HEURE >'"+hera+"' AND HEURE <'"+geza+"' "+sql);}
       
double taxableAmounts(String rate,String sql,String table)// per applicable tax rates divided between sales (NS) and refunds (NR);
{
return db.sum(table, rate, "EXTERNAL_DATA like '%,NS,%' ", "HEURE >'"+hera+"' AND HEURE <'"+geza+"' "+sql);
}
double taxAmounts(String rate,String sql,String table) //per applicable tax rates divided between sales (NS) and refunds(NR);
{
return db.sum(table, rate, "EXTERNAL_DATA like '%,NS,%' ", "HEURE >'"+hera+"' AND HEURE <'"+geza+"' "+sql);
}
double  login(String in)
{ 
return db.sum("LOGIN", "ARGENT", "IN_OUT= '"+in+"' ", "LOG >'"+hera+"' AND LOG <'"+geza+"'");
}
int  numberItemsSold(String sql)
{double [] taxes= new double []{1,   1,   1,   1};    
return db.getPLU(hera, geza,taxes,sql).size();
}
int numberCS(String sql) 
{return db.count("invoice", "ID_INVOICE", "COPY_DATA like '%,CS,%'", "HEURE >'"+hera+"' AND HEURE <'"+geza+"' "+sql);}

int numberCR(String sql)
{return db.count("REFUND", "ID_REFUND", "COPY_DATA like '%,CR,%'", "HEURE >'"+hera+"' AND HEURE <'"+geza+"' "+sql);}

double totalCS(String sql)
{
return db.sum("invoice", "total", "COPY_DATA like '%,CS,%'", "HEURE >'"+hera+"' AND HEURE <'"+geza+"' "+sql);
} 
double totalCR(String sql)
{
return db.sum("REFUND", "total", "COPY_DATA like '%,CR,%'", "HEURE >'"+hera+"' AND HEURE <'"+geza+"' "+sql);
}  
int numberTS (String sql)
{return db.count("invoice", "ID_invoice", "EXTERNAL_DATA like '%,TS,%'", "HEURE >'"+hera+"' AND HEURE <'"+geza+"' "+sql);}
 int numberTR (String sql)
{return db.count("REFUND", "ID_REFUND", "EXTERNAL_DATA like '%,TR,%'", "HEURE >'"+hera+"' AND HEURE <'"+geza+"' "+sql);}
double totalTS (String sql)
{
return db.sum("invoice", "total", "EXTERNAL_DATA like '%,TS,%'", "HEURE >'"+hera+"' AND HEURE <'"+geza+"' "+sql);
} 
double totalTR (String sql)
{
return db.sum("REFUND", "total", "EXTERNAL_DATA like '%,TR,%'", "HEURE >'"+hera+"' AND HEURE <'"+geza+"' "+sql);
} 
int numberPS (String sql)
{
return db.count("BON_PROFORMA", "ID_PROFORMA", "EXTERNAL_DATA like '%,PS,%'", "HEURE >'"+hera+"' AND HEURE <'"+geza+"' "+sql);
}
double totalPS (String sql)
{
return db.sum("BON_PROFORMA", "total", "EXTERNAL_DATA like '%,PS,%'", "HEURE >'"+hera+"' AND HEURE <'"+geza+"' "+sql);
}  
double totalSalesCash(String sql)// amount for all sales receipts labeled as NS, including tax;
{
return db.sum("invoice", "cash", "EXTERNAL_DATA like '%,NS,%' ",
  "HEURE >'"+hera+"' AND HEURE <'"+geza+"' "+sql);
} 
double totalSalesCredit(String sql)// amount for all sales receipts labeled as NS, including tax;
{
return db.sum("invoice", "CREDIT", "EXTERNAL_DATA like '%,NS,%' ",
  "HEURE >'"+hera+"' AND HEURE <'"+geza+"' "+sql);
}
double totalRefundNRCash(String sql)// amount for all sales receipts labeled as NS, including tax;
{
return db.sum("REFUND", "cash", "EXTERNAL_DATA like '%,NR,%' ",
  "HEURE >'"+hera+"' AND HEURE <'"+geza+"' "+sql);
} 
double totalRefundNRCredit( String sql)// amount for all sales receipts labeled as NS, including tax;
{
return db.sum("REFUND", "CREDIT", "EXTERNAL_DATA like '%,NR,%' ",
  "HEURE >'"+hera+"' AND HEURE <'"+geza+"' "+sql);
} 
double totaDiscounts(String sql)  
{
return db.sum("invoice", "REDUCTION", " HEURE >'"+hera+"' ",
  "  HEURE <'"+geza+"' "+sql);
}  
//xix. other registrations that have reduced the day's sales and their amount;
int numberIncompleteSales(String sql)
{return
db.count("invoice", "ID_INVOICE", "EXTERNAL_DATA = 'EMPTY'", "HEURE >'"+hera+"' AND HEURE <'"+geza+"' "+sql)
        ;
}
double totalBL()// including tax;
{
return db.sum("BON_LIVRAISON", "TOTAL", " HEURE >'"+hera+"'", " HEURE <'"+geza+"'");
}
int  numberBL()
{return db.count("BON_LIVRAISON", "ID_BON_LIVRAISON", " HEURE >'"+hera+"' ", " HEURE <'"+geza+"' ");}
double taxBL() //per applicable tax rates divided between sales (NS) and refunds(NR);
{
return db.sum("BON_LIVRAISON", "TVA", " HEURE >'"+hera+"'  ", " HEURE <'"+geza+"'");
}

 String setVirgule(double frw)
    {
        String setString="";
        if(frw>99999)
        {
            BigDecimal big=new BigDecimal(frw);
            int value=big.intValue();
            float dec=(float)(frw-value);
            
            StringTokenizer st1=new StringTokenizer (""+dec);
            st1.nextToken(".");
            
            String decimal=st1.nextToken("").replace(".", "");
            
            if(decimal.length()==1 && decimal.equals("0") )
                decimal=".00";
            else if (decimal.length()==1 && !decimal.equals("0") )
                decimal="."+decimal+"0";
            else
                decimal="."+decimal.substring(0, 2);
            
            setString = ""+value;  
            
            String toret=setVirgule(setString)+decimal;
            return toret;
        }
        else
        {
            StringTokenizer st1=new StringTokenizer (""+frw);
            String entier =st1.nextToken(".");
            //System.out.println(frw);
            String decimal=st1.nextToken("").replace(".", "");
            
            if(decimal.length()==1 && decimal.equals("0") )    
                decimal=".00";
                    
            else if (decimal.length()==1 && !decimal.equals("0") )
                decimal="."+decimal+"0";
            else
                decimal="."+decimal.substring(0, 2);
            String toret=setVirgule(""+entier)+decimal; 
            return toret;
        }
         
}

 String setVirgule(String frw)
   {
       String setString="";
       int k=0;
       for(int i=(frw.length()-1);i>=0;i--)
       {
           k++;
           if((k-1)%3==0 && (k-1)!=0)
           {
               setString+=",";
           }
           setString+=frw.charAt(i);
       }
       return inverse(setString);
   }
   
   String inverse(String frw)
   {
       String l="";
       for(int i=(frw.length()-1);i>=0;i--)
       {
           l+=frw.charAt(i);
       }
       return l;
   }


String setVirguleDouble1(double dou)
{
return " "+dou;
}
String setVirguleInt(int in)
{
return " "+in;
}
public LinkedList <RRA_PRINT2_LINE> toPrint(String sql)
{
  //  daily report hera,geza,tradename,tin,CIS,MRC 
    
LinkedList <RRA_PRINT2_LINE> toPrint = new LinkedList();
Font f = new Font("Arial", Font.BOLD, 12 );
 
toPrint.add(new RRA_PRINT2_LINE(" X DAILY REPORT ", "", f)); 
toPrint.add(new RRA_PRINT2_LINE(" DATE : "+hera.substring(0, 11), "", f));  
toPrint.add(new RRA_PRINT2_LINE(" TRADE NAME : "+tradename, "", f)); 
//toPrint.add(new RRA_PRINT2_LINE(" TRADE NAME :"+tradename, "", f)); 
//toPrint.add(new RRA_PRINT2_LINE(" TIN  : "+tin, "", f)); 
toPrint.add(new RRA_PRINT2_LINE(" TIN  : "+tin, "", f)); 
if(sql.contains("MRC="))
    toPrint.add(new RRA_PRINT2_LINE(" MRC : "+MRC, "", f));  
if(sql.contains("EMPLOYE="))
    toPrint.add(new RRA_PRINT2_LINE(" EMPLOYE:  "+EMPLOYE, "", f));
toPrint.add(new RRA_PRINT2_LINE(" CIS  : "+CIS, "", f));  
//toPrint.add(new RRA_PRINT2_LINE(" TAXE RATE A :"+RRA_PRINT2.setVirgule(tvaRateA,1 ), "", f)); 
//toPrint.add(new RRA_PRINT2_LINE(" TAXE RATE B :"+RRA_PRINT2.setVirgule(tvaRateB,1 ), "", f)); 
//toPrint.add(new RRA_PRINT2_LINE(" TAXE RATE C :"+RRA_PRINT2.setVirgule(tvaRateC,1 ), "", f)); 
//toPrint.add(new RRA_PRINT2_LINE(" TAXE RATE D :"+RRA_PRINT2.setVirgule(tvaRateD,1 ), "", f)); 
f = new Font("Arial", Font.PLAIN, 10 );
toPrint.add(new RRA_PRINT2_LINE(" -------- SALES -----------   ", "", f));
toPrint.add(new RRA_PRINT2_LINE("TOTAL SALES NS ", setVirgule(totalSalesNS(sql)), f)); 
LinkedList<String> clients=db.lesClients();
for(int i=0;i<clients.size();i++)
{
    String clie=clients.get(i);
    double tot=totalSalesNSGoupe(clie,sql);
    if(tot!=0)
    {toPrint.add(new RRA_PRINT2_LINE("TOTAL GROUPE NS "+clie,
            setVirgule(tot), f));}
}
 
toPrint.add(new RRA_PRINT2_LINE("NUMBER SALES NS ", setVirguleInt(numberSalesNS(sql)), f));
toPrint.add(new RRA_PRINT2_LINE("TOTAL COPY SALE ", setVirgule(totalCS(sql)), f));
toPrint.add(new RRA_PRINT2_LINE("NUMBER COPY SALE ", setVirguleInt(numberCS(sql)), f));
toPrint.add(new RRA_PRINT2_LINE("TOTAL SALES CASH ", setVirgule(totalSalesCash(sql)), f));
toPrint.add(new RRA_PRINT2_LINE("TOTAL SALES CREDIT ", setVirgule(totalSalesCredit(sql)), f));
toPrint.add(new RRA_PRINT2_LINE("TOTAL TRAINING SALES ", setVirgule(totalTS (sql)), f));
toPrint.add(new RRA_PRINT2_LINE("NUMBER TRAINING SALES ", setVirguleInt(numberTS (sql)), f)); 
toPrint.add(new RRA_PRINT2_LINE("TOTAL PROFORMA SALES", setVirgule(totalPS (sql)), f));
toPrint.add(new RRA_PRINT2_LINE("NUMBER PROFORMA SALES ", setVirguleInt(numberPS (sql)), f)); 
//toPrint.add(new RRA_PRINT2_LINE("--------- TAXE NORMAL SALES----------   ", "", f));
double taxB=taxAmounts("TAXE_B",sql,"INVOICE");
toPrint.add(new RRA_PRINT2_LINE("TOTAL TAXABLE  A ", setVirgule(taxableAmounts("TOTAL_A",sql,"INVOICE")), f));   
toPrint.add(new RRA_PRINT2_LINE("TOTAL TAXABLE  B ", setVirgule(taxableAmounts("TOTAL_B",sql,"INVOICE")), f));   
toPrint.add(new RRA_PRINT2_LINE("TOTAL TAXABLE  C ", setVirgule(taxableAmounts("TOTAL_C",sql,"INVOICE")), f));   
toPrint.add(new RRA_PRINT2_LINE("TOTAL TAXABLE  D ", setVirgule(taxableAmounts("TOTAL_D",sql,"INVOICE")), f));    
toPrint.add(new RRA_PRINT2_LINE("TOTAL TAXE  A ", setVirgule(taxAmounts("TAXE_A",sql,"INVOICE")), f));   
toPrint.add(new RRA_PRINT2_LINE("TOTAL TAXE  B ", setVirgule(taxB), f));   
toPrint.add(new RRA_PRINT2_LINE("TOTAL TAXE  C ", setVirgule(taxAmounts("TAXE_C",sql,"INVOICE")), f));   
toPrint.add(new RRA_PRINT2_LINE("TOTAL TAXE  D ", setVirgule(taxAmounts("TAXE_D",sql,"INVOICE")), f));   
toPrint.add(new RRA_PRINT2_LINE("-------- REFUND -----------   ", "", f));
toPrint.add(new RRA_PRINT2_LINE("TOTAL REFUND NR ", setVirgule(totalRefundNR(sql)), f)); 

for(int i=0;i<clients.size();i++)
{
    String clie=clients.get(i);
    double tot=totalRefundNRGoupe(clie,sql);
    if(tot!=0)
    {toPrint.add(new RRA_PRINT2_LINE("TOTAL GROUPE NS "+clie,
            setVirgule(tot), f));}
}

toPrint.add(new RRA_PRINT2_LINE("NUMBER REFUND NR ", setVirguleInt(numberRefundNR(sql)), f));
toPrint.add(new RRA_PRINT2_LINE("TOTAL COPY REFUND ", setVirgule(totalCR(sql)), f));
toPrint.add(new RRA_PRINT2_LINE("NUMBER COPY REFUND ", setVirguleInt(numberCR(sql)), f));
toPrint.add(new RRA_PRINT2_LINE("TOTAL REFUND CASH ", setVirgule(totalRefundNRCash(sql)), f));
toPrint.add(new RRA_PRINT2_LINE("TOTAL REFUND CREDIT ", setVirgule(totalRefundNRCredit(sql)), f));  
toPrint.add(new RRA_PRINT2_LINE("TOTAL TRAINING REFUND ", setVirgule(totalTR (sql)), f));
toPrint.add(new RRA_PRINT2_LINE("NUMBER TRAINING REFUND ", setVirguleInt(numberTR (sql)), f));
//toPrint.add(new RRA_PRINT2_LINE("--------- TAXE REFUND SALES----------   ", "", f));
toPrint.add(new RRA_PRINT2_LINE("TOTAL TAXABLE  A ", setVirgule(taxableAmounts("TOTAL_A",sql,"REFUND")), f));   
toPrint.add(new RRA_PRINT2_LINE("TOTAL TAXABLE  B ", setVirgule(taxableAmounts("TOTAL_B",sql,"REFUND")), f));   
toPrint.add(new RRA_PRINT2_LINE("TOTAL TAXABLE  C ", setVirgule(taxableAmounts("TOTAL_C",sql,"REFUND")), f));   
toPrint.add(new RRA_PRINT2_LINE("TOTAL TAXABLE  D ", setVirgule(taxableAmounts("TOTAL_D",sql,"REFUND")), f));    
toPrint.add(new RRA_PRINT2_LINE("TOTAL TAXE  A ", setVirgule(taxAmounts("TAXE_A",sql,"REFUND")), f));   
toPrint.add(new RRA_PRINT2_LINE("TOTAL TAXE  B ", setVirgule(taxAmounts("TAXE_B",sql,"REFUND")), f));   
toPrint.add(new RRA_PRINT2_LINE("TOTAL TAXE  C ", setVirgule(taxAmounts("TAXE_C",sql,"REFUND")), f));   
toPrint.add(new RRA_PRINT2_LINE("TOTAL TAXE  D ", setVirgule(taxAmounts("TAXE_D",sql,"REFUND")), f));   
toPrint.add(new RRA_PRINT2_LINE("-------- BUYS -----------   ", "", f));
toPrint.add(new RRA_PRINT2_LINE("TOTAL BUYS ", RRA_PRINT2.setVirgule(totalBL(),1 ,","), f)); 
toPrint.add(new RRA_PRINT2_LINE("NUMBER BUYS ", setVirguleInt(numberBL()), f));
double taxbl=taxBL();
toPrint.add(new RRA_PRINT2_LINE("TOTAL TAX DEDICTIBLE ", RRA_PRINT2.setVirgule(taxbl,1 ,","), f));
toPrint.add(new RRA_PRINT2_LINE("TOTAL TAX  NET ", RRA_PRINT2.setVirgule((taxB-taxbl),1 ,","), f));
toPrint.add(new RRA_PRINT2_LINE(" --------- # --------------  ", "", f));
toPrint.add(new RRA_PRINT2_LINE("OPENING DEPOSIT ", setVirgule(login("IN")), f)); 
toPrint.add(new RRA_PRINT2_LINE("DEPOSIT ", setVirgule(login("DEPOSIT")), f)); 
toPrint.add(new RRA_PRINT2_LINE("WITHDRAW ", setVirgule(login("RETRAIT")), f)); 
toPrint.add(new RRA_PRINT2_LINE("CLOSING ", setVirgule(login("OUT")), f)); 
toPrint.add(new RRA_PRINT2_LINE("NUMBER OF ITEM SOLD ", setVirguleInt(numberItemsSold(sql)), f));
toPrint.add(new RRA_PRINT2_LINE("TOTAL DISCOUNT ", setVirgule(totaDiscounts(sql) ), f)); 
toPrint.add(new RRA_PRINT2_LINE("NUMBER INCOMPLETE SALES ", setVirguleInt(numberIncompleteSales(sql)), f)); 
 
return toPrint;
}

}
