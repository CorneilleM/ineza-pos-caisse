/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package rra;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.StringTokenizer;
import pos.Product;

/**
 *
 * @author Kimenyi
 */
public class RRA_PACK {
  public double totalpriceA, totalpriceB, totalpriceC, totalpriceD,
          tvaPriceA, tvaPriceB, tvaPriceC, tvaPriceD;
     double tvaRateA, tvaRateB, tvaRateC, tvaRateD;
public String    rtype,  ttype,MRC ,TIN,MRCtime;
int id_invoice;
public LinkedList <Product> getProductList;
public String ClientTin=" ";
    public RRA_PACK(int id_invoice,
            double totalpriceA, double totalpriceB, double totalpriceC, double totalpriceD,
            double tvaPriceA, double tvaPriceB, double tvaPriceC, double tvaPriceD,
            double tvaRateA, double tvaRateB, double tvaRateC, double tvaRateD, 
              String rtype, String ttype,String MRC ,String TIN,
            LinkedList <Product> getProductList,String MRCtime)
    {
        this.id_invoice =  id_invoice;
        this.totalpriceA = totalpriceA ;
        this.totalpriceB = totalpriceB;
        this.totalpriceC = totalpriceC;
        this.totalpriceD = totalpriceD;
        this.tvaPriceA = tvaPriceA;
        this.tvaPriceB = tvaPriceB;
        this.tvaPriceC = tvaPriceC;
        this.tvaPriceD = tvaPriceD;
        this.tvaRateA = tvaRateA;
        this.tvaRateB = tvaRateB;
        this.tvaRateC = tvaRateC;
        this.tvaRateD = tvaRateD; 
        this.rtype = rtype;
        this.ttype = ttype;
        this.MRC=MRC;
        this.TIN=TIN;
        this.MRCtime=MRCtime;
        this.getProductList=getProductList;
        System.err.println(rtype +" *****constr*******"+ttype);
    }
// public double getTotalTaxable()
// {
//     return totalpriceA+totalpriceB+totalpriceC+totalpriceD;
// }
 public double getTotalTaxes()
 {
     return tvaPriceA+tvaPriceB+tvaPriceC+tvaPriceD;
 }
 public double getTotal()
 {
     return totalpriceA+totalpriceB+totalpriceC+totalpriceD;
 }
 
  public double getTotalA()
 {
     return totalpriceA;
 }
  public double getTotalB()
 {  
     return totalpriceB;
 }
  public double getTotalC()
 {
     return totalpriceC;
 }
  public double getTotalD()
 {
     return totalpriceD;
 }
    
}
