/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package rra; 

import pos.Client;

/**
 *
 * @author Kimenyi
 */
public class RRA_RECORD implements Comparable {
    
        public int ID  ;
	public String RTYPE,NUM_CLIENT ,  EMPLOYE, HEURE, EXTERNAL_DATA, COPY_DATA,MRC,TIN,DATE,CODEBAR;
	public double TOTAL  , TVA  , TOTAL_A, 
	 TOTAL_B , TOTAL_C  , TOTAL_D , TAXE_A , TAXE_B  , TAXE_C  , TAXE_D,CASH, CREDIT ;
        public Client umukiliya=null;
        public  int id_ref;
        public String  raison;
        
    public RRA_RECORD(int ID ,String RTYPE, String NUM_CLIENT, String EMPLOYE, String HEURE, 
            String EXTERNAL_DATA, String COPY_DATA,String MRC,String TIN,
            double TOTAL, double TVA, double TOTAL_A,
            double TOTAL_B, double TOTAL_C, double TOTAL_D, double TAXE_A, double TAXE_B, double TAXE_C, 
            double TAXE_D,String DATE,double CASH, double CREDIT) {
        this.ID  = ID ;
         this.DATE = DATE;
        this.RTYPE = RTYPE;
        this.NUM_CLIENT = NUM_CLIENT;
        this.EMPLOYE = EMPLOYE;
        this.HEURE = HEURE;
        this.MRC = MRC;
        this.TIN = TIN;
        this.EXTERNAL_DATA = EXTERNAL_DATA;
        this.COPY_DATA = COPY_DATA;
        this.TOTAL = TOTAL;
        this.TVA = TVA;
        this.TOTAL_A = TOTAL_A;
        this.TOTAL_B = TOTAL_B;
        this.TOTAL_C = TOTAL_C;
        this.TOTAL_D = TOTAL_D;
        this.TAXE_A = TAXE_A;
        this.TAXE_B = TAXE_B;
        this.TAXE_C = TAXE_C;
        this.TAXE_D = TAXE_D;
        this.CASH = CASH;
        this.CREDIT = CREDIT;
    } 
    @Override
    public int compareTo(Object o)
{
    return  HEURE.compareTo(((RRA_RECORD)o).HEURE);
}
}