/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package rra;

import java.awt.Font;
import java.util.LinkedList;
import pos.Db;

/**
 *
 * @author Kimenyi
 */
public class RRA_REPORT_Z {
    
    Db db ; 
    String hera,geza,tradename,tin,CIS,MRC ;
    double   tvaRateA,   tvaRateB,   tvaRateC,   tvaRateD;
    
    public RRA_REPORT_Z(Db db, String hera, String geza, String tradename, 
            String tin, String CIS, String MRC,double tvaRateA, double tvaRateB, double tvaRateC, double tvaRateD) {
        this.db = db;
        this.hera = hera;
        this.geza = geza;
        this.tradename = tradename;
        this.tin = tin;
        this.CIS = CIS;
        this.MRC = MRC;
        this.tvaRateA = tvaRateA;
        this.tvaRateB = tvaRateB;
        this.tvaRateC = tvaRateC;
        this.tvaRateD = tvaRateD;
    }

    
double totalSalesNS() 
{
return db.sum("invoice", "total", "EXTERNAL_DATA like '%,NS,%'", "HEURE >'"+hera+"' AND HEURE <'"+geza+"'");
} 
double totalSalesNSGoupe(String groupe)
{
return db.sum("invoice", "total", "EXTERNAL_DATA like '%,NS,%' and num_client='"+groupe+"'",
        "HEURE >'"+hera+"' AND HEURE <'"+geza+"'");
}
double totalRefundNRGoupe(String groupe)
{
return db.sum("REFUND", "total", "EXTERNAL_DATA like '%,NR,%' and num_client='"+groupe+"'",
        "HEURE >'"+hera+"' AND HEURE <'"+geza+"'");
}
int numberSalesNS()
{return db.count("invoice", "ID_INVOICE", "EXTERNAL_DATA like '%,NS,%'", "HEURE >'"+hera+"' AND HEURE <'"+geza+"'");}
double totalRefundNR()// including tax;
{
return db.sum("REFUND", "total", "EXTERNAL_DATA like '%,NR,%'", "HEURE >'"+hera+"' AND HEURE <'"+geza+"'");
} 
int  numberRefundNR()
{return db.count("REFUND", "ID_REFUND", "EXTERNAL_DATA like '%,NR,%'", "HEURE >'"+hera+"' AND HEURE <'"+geza+"'");}
       
double taxableAmounts(String rate,String table)// per applicable tax rates divided between sales (NS) and refunds (NR);
{
return db.sum(table, rate, "EXTERNAL_DATA like '%,NS,%' ", "HEURE >'"+hera+"' AND HEURE <'"+geza+"'");
}
double taxAmounts(String rate,String table) //per applicable tax rates divided between sales (NS) and refunds(NR);
{
return db.sum(table, rate, "EXTERNAL_DATA like '%,NS,%' ", "HEURE >'"+hera+"' AND HEURE <'"+geza+"'");
}
double  login(String in)
{ 
return db.sum("LOGIN", "ARGENT", "IN_OUT= '"+in+"' ", "LOG >'"+hera+"' AND LOG <'"+geza+"'");
}
int  numberItemsSold()
{double [] taxes= new double []{1,   1,   1,   1};    
return db.getPLU(hera, geza,taxes,"").size();
}
int numberCS() 
{return db.count("invoice", "ID_INVOICE", "COPY_DATA like '%,CS,%'", "HEURE >'"+hera+"' AND HEURE <'"+geza+"'");}

int numberCR()
{return db.count("REFUND", "ID_REFUND", "COPY_DATA like '%,CR,%'", "HEURE >'"+hera+"' AND HEURE <'"+geza+"'");}

double totalCS()
{
return db.sum("invoice", "total", "COPY_DATA like '%,CS,%'", "HEURE >'"+hera+"' AND HEURE <'"+geza+"'");
} 
double totalCR()
{
return db.sum("REFUND", "total", "COPY_DATA like '%,CR,%'", "HEURE >'"+hera+"' AND HEURE <'"+geza+"'");
}  
int numberTS ()
{return db.count("invoice", "ID_invoice", "EXTERNAL_DATA like '%,TS,%'", "HEURE >'"+hera+"' AND HEURE <'"+geza+"'");}
 int numberTR ()
{return db.count("REFUND", "ID_REFUND", "EXTERNAL_DATA like '%,TR,%'", "HEURE >'"+hera+"' AND HEURE <'"+geza+"'");}
double totalTS ()
{
return db.sum("invoice", "total", "EXTERNAL_DATA like '%,TS,%'", "HEURE >'"+hera+"' AND HEURE <'"+geza+"'");
} 
double totalTR ()
{
return db.sum("REFUND", "total", "EXTERNAL_DATA like '%,TR,%'", "HEURE >'"+hera+"' AND HEURE <'"+geza+"'");
} 
int numberPS ()
{
return db.count("BON_PROFORMA", "ID_PROFORMA", "EXTERNAL_DATA like '%,PS,%'", "HEURE >'"+hera+"' AND HEURE <'"+geza+"'");
}
double totalPS ()
{
return db.sum("BON_PROFORMA", "total", "EXTERNAL_DATA like '%,PS,%'", "HEURE >'"+hera+"' AND HEURE <'"+geza+"'");
}  
double totalSalesCash()// amount for all sales receipts labeled as NS, including tax;
{
return db.sum("invoice", "cash", "EXTERNAL_DATA like '%,NS,%' ",
  "HEURE >'"+hera+"' AND HEURE <'"+geza+"'");
} 
double totalSalesCredit()// amount for all sales receipts labeled as NS, including tax;
{
return db.sum("invoice", "credit", "EXTERNAL_DATA like '%,NS,%' ",
  "HEURE >'"+hera+"' AND HEURE <'"+geza+"'");
}
double totalRefundNRCash()// amount for all sales receipts labeled as NS, including tax;
{
return db.sum("REFUND", "cash", "EXTERNAL_DATA like '%,NR,%' ",
  "HEURE >'"+hera+"' AND HEURE <'"+geza+"'");
} 
double totalRefundNRCredit()// amount for all sales receipts labeled as NS, including tax;
{
return db.sum("REFUND", "credit", "EXTERNAL_DATA like '%,NR,%' ",
  "HEURE >'"+hera+"' AND HEURE <'"+geza+"'");
} 
double totaDiscounts()  
{
return db.sum("invoice", "REDUCTION", " HEURE >'"+hera+"' ",
  "  HEURE <'"+geza+"'");
}  
//xix. other registrations that have reduced the day's sales and their amount;
int numberIncompleteSales()
{return
db.count("invoice", "ID_INVOICE", "EXTERNAL_DATA = 'EMPTY'", "HEURE >'"+hera+"' AND HEURE <'"+geza+"'");
}

double totalBL()// including tax;
{
return db.sum("BON_LIVRAISON", "TOTAL", " HEURE >'"+hera+"'", " HEURE <'"+geza+"'");
}
int  numberBL()
{return db.count("BON_LIVRAISON", "ID_BON_LIVRAISON", " HEURE >'"+hera+"' ", " HEURE <'"+geza+"' ");}
double taxBL() //per applicable tax rates divided between sales (NS) and refunds(NR);
{
return db.sum("BON_LIVRAISON", "TVA", " HEURE >'"+hera+"'  ", " HEURE <'"+geza+"'");
}

String setVirguleInt(int in)
{
return " "+in;
}
public LinkedList <RRA_PRINT2_LINE> toPrint()
{
  //  daily report hera,geza,tradename,tin,CIS,MRC 
    
LinkedList <RRA_PRINT2_LINE> toPrint = new LinkedList();
Font f = new Font("Arial", Font.BOLD, 12 );
 
toPrint.add(new RRA_PRINT2_LINE(" Z REPORT ", "", f)); 
toPrint.add(new RRA_PRINT2_LINE(" START : "+hera, "", f)); 
toPrint.add(new RRA_PRINT2_LINE(" END : "+geza, "", f)); 
toPrint.add(new RRA_PRINT2_LINE(" TRADE NAME :"+tradename, "", f)); 
toPrint.add(new RRA_PRINT2_LINE(" TIN  : "+tin, "", f)); 
//toPrint.add(new RRA_PRINT2_LINE(" MRC : "+MRC, "", f));  
toPrint.add(new RRA_PRINT2_LINE(" CIS  : "+CIS, "", f)); 
toPrint.add(new RRA_PRINT2_LINE(" -------------------------  ", "", f));
toPrint.add(new RRA_PRINT2_LINE(" TAXE RATE A :"+RRA_PRINT2.setVirgule(tvaRateA,1 ,","), "", f)); 
toPrint.add(new RRA_PRINT2_LINE(" TAXE RATE B :"+RRA_PRINT2.setVirgule(tvaRateB,1 ,","), "", f)); 
toPrint.add(new RRA_PRINT2_LINE(" TAXE RATE C :"+RRA_PRINT2.setVirgule(tvaRateC,1 ,","), "", f)); 
toPrint.add(new RRA_PRINT2_LINE(" TAXE RATE D :"+RRA_PRINT2.setVirgule(tvaRateD,1 ,","), "", f));  
f = new Font("Arial", Font.PLAIN, 10 );
toPrint.add(new RRA_PRINT2_LINE(" -------- SALES -----------   ", "", f));
toPrint.add(new RRA_PRINT2_LINE("TOTAL SALES NS ", RRA_PRINT2.setVirgule(totalSalesNS(),1 ,","), f)); 
LinkedList<String> clients=db.lesClients();
for(int i=0;i<clients.size();i++)
{
    String clie=clients.get(i);
    double tot=totalSalesNSGoupe(clie);
    if(tot!=0)
    {toPrint.add(new RRA_PRINT2_LINE("TOTAL GROUPE NS "+clie,
            RRA_PRINT2.setVirgule(tot,1 ,","), f));}
}
 
toPrint.add(new RRA_PRINT2_LINE("NUMBER SALES NS ", setVirguleInt(numberSalesNS()), f));
toPrint.add(new RRA_PRINT2_LINE("TOTAL COPY SALE ", RRA_PRINT2.setVirgule(totalCS(),1 ,","), f));
toPrint.add(new RRA_PRINT2_LINE("NUMBER COPY SALE ", setVirguleInt(numberCS()), f));
toPrint.add(new RRA_PRINT2_LINE("TOTAL SALES CASH ", RRA_PRINT2.setVirgule(totalSalesCash(),1 ,","), f));
toPrint.add(new RRA_PRINT2_LINE("TOTAL SALES CREDIT ", RRA_PRINT2.setVirgule(totalSalesCredit(),1 ,","), f));
toPrint.add(new RRA_PRINT2_LINE("TOTAL TRAINING SALES ", RRA_PRINT2.setVirgule(totalTS (),1 ,","), f));
toPrint.add(new RRA_PRINT2_LINE("NUMBER TRAINING SALES ", setVirguleInt(numberTS ()), f)); 
toPrint.add(new RRA_PRINT2_LINE("TOTAL PROFORMA SALES", RRA_PRINT2.setVirgule(totalPS (),1 ,","), f));
toPrint.add(new RRA_PRINT2_LINE("NUMBER PROFORMA SALES ", setVirguleInt(numberPS ()), f)); 
toPrint.add(new RRA_PRINT2_LINE("--------- TAXE NORMAL SALES----------   ", "", f));
toPrint.add(new RRA_PRINT2_LINE("TOTAL TAXABLE  A ", RRA_PRINT2.setVirgule(taxableAmounts("TOTAL_A","INVOICE"),1 ,","), f));   
toPrint.add(new RRA_PRINT2_LINE("TOTAL TAXABLE  B ", RRA_PRINT2.setVirgule(taxableAmounts("TOTAL_B","INVOICE"),1 ,","), f));   
toPrint.add(new RRA_PRINT2_LINE("TOTAL TAXABLE  C ", RRA_PRINT2.setVirgule(taxableAmounts("TOTAL_C","INVOICE"),1 ,","), f));   
toPrint.add(new RRA_PRINT2_LINE("TOTAL TAXABLE  D ", RRA_PRINT2.setVirgule(taxableAmounts("TOTAL_D","INVOICE"),1 ,","), f));
double taxB=taxAmounts("TAXE_B","INVOICE");
toPrint.add(new RRA_PRINT2_LINE("TOTAL TAXE  A ", RRA_PRINT2.setVirgule(taxAmounts("TAXE_A","INVOICE"),1 ,","), f));   
toPrint.add(new RRA_PRINT2_LINE("TOTAL TAXE  B ", RRA_PRINT2.setVirgule(taxB,1 ,","), f));   
toPrint.add(new RRA_PRINT2_LINE("TOTAL TAXE  C ", RRA_PRINT2.setVirgule(taxAmounts("TAXE_C","INVOICE"),1 ,","), f));   
toPrint.add(new RRA_PRINT2_LINE("TOTAL TAXE  D ", RRA_PRINT2.setVirgule(taxAmounts("TAXE_D","INVOICE"),1 ,","), f));
toPrint.add(new RRA_PRINT2_LINE("   ", "", f));
toPrint.add(new RRA_PRINT2_LINE("-------- REFUND -----------   ", "", f));
toPrint.add(new RRA_PRINT2_LINE("TOTAL REFUND NR ", RRA_PRINT2.setVirgule(totalRefundNR(),1 ,","), f)); 

for(int i=0;i<clients.size();i++)
{
    String clie=clients.get(i);
    double tot=totalRefundNRGoupe(clie);
    if(tot!=0)
    {toPrint.add(new RRA_PRINT2_LINE("TOTAL GROUPE NR "+clie,
            RRA_PRINT2.setVirgule(tot,1 ,","), f));}
}

toPrint.add(new RRA_PRINT2_LINE("NUMBER REFUND NR ", setVirguleInt(numberRefundNR()), f));
toPrint.add(new RRA_PRINT2_LINE("TOTAL COPY REFUND ", RRA_PRINT2.setVirgule(totalCR(),1 ,","), f));
toPrint.add(new RRA_PRINT2_LINE("NUMBER COPY REFUND ", setVirguleInt(numberCR()), f));
toPrint.add(new RRA_PRINT2_LINE("TOTAL REFUND CASH ", RRA_PRINT2.setVirgule(totalRefundNRCash(),1 ,","), f));
toPrint.add(new RRA_PRINT2_LINE("TOTAL REFUND CREDIT ", RRA_PRINT2.setVirgule(totalRefundNRCredit(),1 ,","), f));  
toPrint.add(new RRA_PRINT2_LINE("TOTAL TRAINING REFUND ", RRA_PRINT2.setVirgule(totalTR (),1 ,","), f));
toPrint.add(new RRA_PRINT2_LINE("NUMBER TRAINING REFUND ", setVirguleInt(numberTR ()), f));
toPrint.add(new RRA_PRINT2_LINE("--------- TAXE REFUND SALES----------   ", "", f));
toPrint.add(new RRA_PRINT2_LINE("TOTAL TAXABLE  A ", RRA_PRINT2.setVirgule(taxableAmounts("TOTAL_A","REFUND"),1 ,","), f));   
toPrint.add(new RRA_PRINT2_LINE("TOTAL TAXABLE  B ", RRA_PRINT2.setVirgule(taxableAmounts("TOTAL_B","REFUND"),1 ,","), f));   
toPrint.add(new RRA_PRINT2_LINE("TOTAL TAXABLE  C ", RRA_PRINT2.setVirgule(taxableAmounts("TOTAL_C","REFUND"),1 ,","), f));   
toPrint.add(new RRA_PRINT2_LINE("TOTAL TAXABLE  D ", RRA_PRINT2.setVirgule(taxableAmounts("TOTAL_D","REFUND"),1 ,","), f));
toPrint.add(new RRA_PRINT2_LINE("TOTAL TAXE  A ", RRA_PRINT2.setVirgule(taxAmounts("TAXE_A","REFUND"),1 ,","), f));   
toPrint.add(new RRA_PRINT2_LINE("TOTAL TAXE  B ", RRA_PRINT2.setVirgule(taxAmounts("TAXE_B","REFUND"),1 ,","), f));   
toPrint.add(new RRA_PRINT2_LINE("TOTAL TAXE  C ", RRA_PRINT2.setVirgule(taxAmounts("TAXE_C","REFUND"),1 ,","), f));   
toPrint.add(new RRA_PRINT2_LINE("TOTAL TAXE  D ", RRA_PRINT2.setVirgule(taxAmounts("TAXE_D","REFUND"),1 ,","), f)); 
toPrint.add(new RRA_PRINT2_LINE("   ", "", f));


toPrint.add(new RRA_PRINT2_LINE("-------- BUYS -----------   ", "", f));
toPrint.add(new RRA_PRINT2_LINE("TOTAL BUYS ", RRA_PRINT2.setVirgule(totalBL(),1 ,","), f)); 
toPrint.add(new RRA_PRINT2_LINE("NUMBER BUYS ", setVirguleInt(numberBL()), f));
double taxbl=taxBL();
toPrint.add(new RRA_PRINT2_LINE("TOTAL TAX BUYS ", RRA_PRINT2.setVirgule(taxbl,1 ,","), f));
toPrint.add(new RRA_PRINT2_LINE("TOTAL TAX DEDICTIBLE ", RRA_PRINT2.setVirgule((taxB-taxbl),1 ,","), f));
toPrint.add(new RRA_PRINT2_LINE("   ", "", f));
toPrint.add(new RRA_PRINT2_LINE(" --------- # --------------  ", "", f));
toPrint.add(new RRA_PRINT2_LINE("OPENING DEPOSIT ", RRA_PRINT2.setVirgule(login("IN"),1 ,","), f)); 
toPrint.add(new RRA_PRINT2_LINE("DEPOSIT ", RRA_PRINT2.setVirgule(login("DEPOSIT"),1 ,","), f)); 
toPrint.add(new RRA_PRINT2_LINE("WITHDRAW ", RRA_PRINT2.setVirgule(login("RETRAIT"),1 ,","), f)); 
toPrint.add(new RRA_PRINT2_LINE("CLOSING ", RRA_PRINT2.setVirgule(login("OUT"),1 ,","), f)); 
toPrint.add(new RRA_PRINT2_LINE("NUMBER OF ITEM SOLD ", setVirguleInt(numberItemsSold()), f));
toPrint.add(new RRA_PRINT2_LINE("TOTAL DISCOUNT ", RRA_PRINT2.setVirgule(totaDiscounts(),1 ,","), f)); 
toPrint.add(new RRA_PRINT2_LINE("NUMBER INCOMPLETE SALES ", setVirguleInt(numberIncompleteSales()), f)); 
 
return toPrint;
}

}
