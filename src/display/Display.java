/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package display;

/**
 *
 * @author Kimenyi
 */ 
      
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Date;
import java.util.LinkedList;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingWorker; 
import pos.Cashier;
import pos.Employe;  
import pos.Proforma;

/**
 *
 * @author Kimenyi
 */
public class Display  extends JFrame {

final static int port = 5555;
JButton  clearButton,readyButton;  
static public JTextArea textArea;  
JTextField ready;
int Tosleep=500;
Display_Monitor gm; 
Display_Db db;
Employe umukozi;
public Display(Employe umukozi, String server,String dataBase ) 
    {  
    this.db= new Display_Db(server,dataBase);
    this.umukozi=umukozi;
    draw(); 
    rebaNet();
    clear();  ready();
    umvaAndoidPS();
    }

 private void draw()
{
    setTitle("Ishyiga DISPLAY"); 
    this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);  
    Container content = getContentPane(); 
     westPane(content); 
     setSize(1100,800);
     setVisible(true);

}
   
public void westPane(Container content )
{ 

JPanel panel = new JPanel();
panel.setLayout(new GridLayout(1,3 ));//
panel.setPreferredSize(new Dimension(500,100)); //400=largeur350=longeur
 
clearButton=new JButton("CLEAR");
clearButton.setBackground(new Color(255, 204, 102));
textArea=new JTextArea(20, 5); 
textArea    .setFont(new Font("Helvetica", Font.BOLD, 20));
readyButton=new JButton("READY");
readyButton.setBackground(  Color.GREEN);
ready =new JTextField();
panel.add(clearButton ); 
panel.add(ready ); 
panel.add(readyButton ); 

JPanel MAIN = new JPanel();
//MAIN.setLayout(new GridLayout(1,2));
MAIN.setPreferredSize(new Dimension(1000,600));
JScrollPane jt=new JScrollPane(textArea);
jt.setPreferredSize(new Dimension(1000,600));
MAIN.add(jt);  MAIN.add(panel);
 
content.add(MAIN); 
 
}    
 	    public static boolean isInteger(String s) {
	        try { 
	            Double.parseDouble(s); 
	        } catch(NumberFormatException e) { 
	            return false; 
	        }
	        // only got here if we didn't return false
	        return true;
	    }

private void ready()
{ 
readyButton.addActionListener(new ActionListener() {
    @Override
    public void actionPerformed(ActionEvent ae)
{
    if(ae.getSource()==readyButton && !isInteger(ready.getText())   )
    {
        JOptionPane.showMessageDialog(rootPane, ready.getText()+"  NOT A NUMBER");
    }
    else if(ae.getSource()==readyButton  )
    {  
         int id_Proforma=Integer.parseInt(ready.getText()) ; 
         
         Proforma profo=db.getProforma(id_Proforma);
         int n=0;
         
         if(profo==null)
         {JOptionPane.showMessageDialog(rootPane, ready.getText()+"  DOES NOT EXIST");}
         else
         {
         if(profo.keyinvoice!=null && (profo.keyinvoice.equals("READY") || profo.keyinvoice.equals("FACTURE")))
         {JOptionPane.showMessageDialog(rootPane, ready.getText()+" STATUS "
                 +profo.keyinvoice +"\n BY "+profo.employe+"  "+profo.reference); n=1;}
         else
         {
                 n = JOptionPane.showConfirmDialog(rootPane,
                                        "  ID CMD    =" +ready.getText()
                                        + "\n TIME IN  =" + profo.heure
                                        + "\n WAITER  =" + profo.num_Client
                                        + "\n TABLE =" + profo.nom_client
                                        + "\n " + profo.list ,
                                        " READY  "   , JOptionPane.YES_NO_OPTION);
         }  
         }
             
             
         if( n==0)
         { db.updateProformat(  id_Proforma,umukozi.NOM_EMPLOYE); }
         
    }
 }}); 
}  
private void clear()
{ 
clearButton.addActionListener(new ActionListener() {
    @Override
    public void actionPerformed(ActionEvent ae)
{
 if(ae.getSource()==clearButton  )
    { 
     
    String text=textArea.getText();
    String dateD=""+new Date(); 
    dateD=dateD.replaceAll(":", "-");  
    String  file="TRACK/TRACK_"+dateD+".txt";
    andika(text,file);
    textArea.setText(""); 
    
    }
 }}); 
}  
static void andika(String kwandika, String file) {

      File f = new File(file);
      try {
          PrintWriter sorti = new PrintWriter(new FileWriter(f));
          sorti.println(kwandika);
          sorti.close();
      } catch (Throwable e) {
          JOptionPane.showConfirmDialog(null, "ECRIRE DANS " + file + " IMPOSSIBLE \n" + e);
}
}
public void actionPerformed(ActionEvent e) { 
  
textArea.append(e.getActionCommand());
System.out.println(e.getActionCommand());   
}
    private void umvaAndoidPS()
{ 
textArea.append("\n Lancement Ishyiga DISPLAY VIA SOCKET :" ); 
/** Create a SwingWorker instance to run a compute-intens ive task */
final SwingWorker<String , String> worker = new SwingWorker<String, String>() 
{
/** Schedule a compute-intensive task in a background thread */
         @Override
protected String doInBackground() throws Exception {   
try { 
Thread t = new Thread();
while (true) 
{
if(Display_Monitor.queu.size() >(Display_Monitor.currentQueuId))
{ 
//textArea.append("\n SCD QUEU  SIZE :"+Display_Monitor.queu.size()+
//        " \n Processing :"+Display_Monitor.currentQueuId );
Display_Cmd rCmd=Display_Monitor.queu.get(Display_Monitor.currentQueuId); 
textArea.append("\n UMUKOZI:     "+rCmd.SENDER+"  ISAHA:    "+rCmd.SENT_TIME) ;
textArea.append("\n QTE          NAME                 PRIX     ID ") ; 
for(int i=0;i< rCmd.list.size(); i++)
{
 Display_Cmd.List_Cmd l=rCmd.list.get(i); 
  textArea.append("\n "+(int)l.qte+" |  "+l.name+"  |  "+l.price+"   |   "+l.id_article+" ") ; 
}  
rCmd.replyClient("OK");
 Display_Monitor.currentQueuId++;
}// iyo hari ikintu gitegereje
else // IYO NTAKINTU GITEGEREGE RYAMA T0SLEEP MILLISECOND
{
  Thread.sleep(Tosleep);
}
}// END WHILE
}// END TRY

catch (Exception e) 
{
 textArea.append("\n Exception  : "+e);  
} 
return  " "; 
}
@Override protected void done() { } 
@Override protected void process(java.util.List<String> chunks) { }
};
worker.execute();  // start the worker thread 
worker.addPropertyChangeListener(new PropertyChangeListener() { 
@Override public void propertyChange(PropertyChangeEvent evt) {
if (evt.getPropertyName().equals("progress")) { }}});  
}
 
public static int getQuantity(String s, String s1, int i) {
        int entier = -1;

        String in = JOptionPane.showInputDialog(null, s + s1);
        boolean done = true;

        while (in != null && i < 3 && done == true) {
            try {
                if (in.equals("")) {
                    i++;
                    in = JOptionPane.showInputDialog(null, s + " Enter a number ", "1");
                } else {
                    Integer in_int = new Integer(in);
                    entier = in_int.intValue();
                    done = false;
                    if (entier < 0 || entier == 0) {
                        entier = -1;
                    }
                } 
            } catch (NumberFormatException e) {
                i++;
                in = JOptionPane.showInputDialog(null, s + " = " + in + "= Not a number; Enter a number ");
                done = true;
            }
        }
        if (i == 3) {
            JOptionPane.showMessageDialog(null, " Too many try ", " Sorry we have to Exit ", JOptionPane.PLAIN_MESSAGE);
        }

        return entier;
    }
 private void rebaNet()
 {
     // textArea.append("Lancement SCD Controler  " );
 
/** Create a SwingWorker instance to run a compute-intensive task */
final SwingWorker<String , String> worker = new SwingWorker<String, String>() 
{
/** Schedule a compute-intensive task in a background thread */
         @Override
protected String doInBackground() throws Exception {
   
  gm =   new Display_Monitor( ); 
try {
    
 ServerSocket socketServeur = new ServerSocket(port); 
 textArea.append("\n PORT ISHYIGA : "+port);
 
 while (true) {
 Socket socketClient = socketServeur.accept();
 Display_Server t = new Display_Server(socketClient ); 
 t.start();
 }
 } catch (Exception e) { 
  textArea.append("SERVER SOCKET "+e);   
 } 
 return  " "; 
}
  /** Run in event-dispatching thread after doInBackground() completes */
@Override
protected void done() {
 
 textArea.append("  \n Result is...");                 
  
} 
 @Override
         protected void process(java.util.List<String> chunks) {
            // Get the latest result from the list
            String latestResult = chunks.get(chunks.size() - 1); 
            textArea.append("  \n Result is..."+latestResult);
         }
      };
             worker.execute();  // start the worker thread 
            //textArea.append("  Running...");
      /** Event handler for the PropertyChangeEvent of property "progress" */
      
worker.addPropertyChangeListener(new PropertyChangeListener() {
@Override
public void propertyChange(PropertyChangeEvent evt) {
if (evt.getPropertyName().equals("progress")) {  // check the property name
//pbWorker.setValue((Integer)evt.getNewValue());  // update progress bar
}
}
}); 
}

 @Override
    protected void processWindowEvent(WindowEvent evt) {
        if (WindowEvent.WINDOW_CLOSING == evt.getID()) {
            //default icon, custom title
            int n = JOptionPane.showConfirmDialog(null, "EXIT ISHYIGA "
                    , "ATTENTION", JOptionPane.YES_NO_OPTION);
            if (n == 0) {
                 
    String text=textArea.getText();
    String dateD=""+new Date(); 
    dateD=dateD.replaceAll(":", "-");  
    String  file="TRACK/TRACK_"+dateD+".txt";
    andika(text,file);
    textArea.setText("");
     System.exit(0);
            }
        }
    }
   
  public static void main(String[] args) { 
       
        try { 
            int id = getQuantity("NUMERO D'EMPLOYE ", " : CHIFFRE", 0);  
            if (id != -1) {

                JPanel pw = new JPanel();
                JPasswordField passwordField = new JPasswordField(10);
                JLabel label = new JLabel("MOT DE PASSE: ");
                pw.add(label);
                pw.add(passwordField);
                passwordField.setText("");
                JOptionPane.showConfirmDialog(null, pw);
                Integer in_int = new Integer(passwordField.getText());
                int carte = in_int.intValue();
//                int carte = 2;

                LinkedList<String> give = new LinkedList();

                File productFile = new File("log.txt");
                String ligne = null;
                BufferedReader entree = new BufferedReader(new FileReader(productFile));
                try {
                    ligne = entree.readLine();
                } catch (IOException e) {
                    System.out.println(e);
                }
                int i = 0;
                while (ligne != null) {
                    // System.out.println(i+ligne);
                    give.add(ligne);
                    i++;

                    try {
                        ligne = entree.readLine();
                    } catch (IOException e) {
                        JOptionPane.showMessageDialog(null, "QUESTION", " reba log ", JOptionPane.PLAIN_MESSAGE);
                    }
                }
                entree.close();
 
                String dataBase = give.get(2);
                String serv = give.get(1); 

                if (carte != -1 ) 
                { 
                     String smartdb="",smartserver="";
         
                    smartdb="NOT AVAILABLE";
                    smartserver="NOT AVAILABLE"; 
                Cashier c = new Cashier(id, serv, dataBase,"RWF",smartserver,smartdb ); 
                    
                    if (c != null && c.db.check_db(id, carte)) {
                        Employe n = c.db.getName(id); 
                        c.db.insertLogin2(n.NOM_EMPLOYE, "DIS", 0); 
                         new Display(n,serv, dataBase);
                    } else {
                        JOptionPane.showMessageDialog(null, "  MOT DE PASSE INCORRECTE ", " IKIBAZO ", JOptionPane.PLAIN_MESSAGE);
                    }
                }
            }
        } catch (Throwable t) {
            JOptionPane.showMessageDialog(null, " SERVEUR ETEINT " + t, " IKIBAZO ", JOptionPane.PLAIN_MESSAGE);
        }
    }

     
        
}
