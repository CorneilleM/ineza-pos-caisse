/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pos;

    /* 
    *Desktop class only can get when you use JDK 6 
    */  
    import java.awt.Desktop;        
    import javax.swing.JFileChooser;        
    import java.io.File;  
      
    public class OpenFileUsingJFileChooser2  
    {

    public OpenFileUsingJFileChooser2() 
    {
         
     //Create a file chooser  
     JFileChooser fileChooser=new JFileChooser();  
      
     //File chooser will appear in no windows parent  
     int a=fileChooser.showOpenDialog(null);  
      
     //Action that will take when user click open button  
     if(a==JFileChooser.APPROVE_OPTION)  
     {  
      //Get file that want to open  
      File fileToOpen=fileChooser.getSelectedFile();  
      
      try  
      {  
       //Open file using suitable program on computer.  
       //You don't need to tell what program to use.  
       //Java will select default program that your  
       //computer use to open the file.  
       //In windowsXP you can see what  
       //program that your computer use to open  
       //a file by right click on file,  
       //choose properties and see at  
       //Opens with : ProgramNameToOpenTheFile  
       Desktop.getDesktop().open(fileToOpen);  
      }  
      catch(Exception exception)  
      {  
       System.out.println("Problem occour when to open the file");  
      }  
     }
    }
        
    public static void main(String[]args)  
    {   
        new OpenFileUsingJFileChooser2();
    }  
    }  