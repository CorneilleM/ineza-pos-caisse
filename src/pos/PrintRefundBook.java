/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */  

package pos;
 
import com.barcodelib.barcode.DataMatrix;
import java.awt.*; 
import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import rra.RRA_PRINT2;
/**
 *
 * @author Kimenyi
 */

public class PrintRefundBook extends JFrame {

private Properties properties = new Properties();
LinkedList <Lot> lotList;
double [] ibiciro; 
String [] footer;
PrintTiers tier;
PrintVars vars;
int pageNum,margin = 0;  
boolean byempurimwe=false;
int pageW=0;

Db db;
Image img;
Graphics pg;
PrintJob pjob;
RRA_PRINT2 rp;
int negatif=1;
String raison="";
public PrintRefundBook(Db db,Image img,PrintTiers tier,PrintVars vars,LinkedList <Lot> lotList,
        double [] ibiciro,String [] footer, RRA_PRINT2 rp,String raison)  
{  
this.img=img;
this.footer=footer; 
this.vars=vars;
this.lotList=lotList;
this.db=db;
this.margin=getValue("margin");
this.ibiciro=ibiciro;
this.tier=tier;
this.rp=rp;
this.negatif=1; 
this.byempurimwe=PrintCommand();  
this.raison=raison;

} 

private boolean PrintCommand()
{
    
    pjob = getToolkit().getPrintJob(this, tier.intitule1+" "+vars.numero, properties);

    if (pjob != null) { 
    pg = pjob.getGraphics(); 
    pageW = pjob.getPageDimension().width - margin;
    
    if (pg != null) { 
    setHeader();
    hagati();
    setFooter();
    pg.dispose();
    }
    pjob.end();
    return true ;
    }
    else
    return false;
}
void setHeader()
{
    System.out.println("MODE:"+vars.mode);
    Font helv = new Font("Helvetica", Font.BOLD, 11);
    pg.setFont(helv);     
    pg.drawString( " Refund Date and Time:  "+rp.date_time_mrc
            , getValue("dateX")-150, getValue("dateY"));
    helv = new Font("Helvetica", Font.BOLD, 25);
    pg.setFont(helv);  
    
   // pg.drawImage(img, getValue("logoX")+margin, getValue("logoY"), pageW-30, 50, rootPane);
    int largeurLogo=150;
    int longeurLogo=45;
    
    if(getValue("largeurLogo")!=0 && getValue("longeurLogo")!=0)
    {
    largeurLogo=getValue("largeurLogo");
    longeurLogo=getValue("longeurLogo");
    }  
    pg.drawImage(img, getValue("logoX")+margin, getValue("logoY"), largeurLogo, longeurLogo, rootPane);

    helv = new Font("Helvetica", Font.PLAIN, getValue("adresseT"));
    //have to set the font to get any output
    pg.setFont(helv); 
    
    pg.drawString(getString("adresseV"), getValue("adresseX")+margin,  getValue("adresseY"));
    pg.drawString(getString("rueV"), getValue("rueX")+margin,  getValue("rueY"));
    pg.drawString(getString("localiteV"), getValue("localiteX")+margin,  getValue("localiteY"));
    pg.drawString(getString("telV"), getValue("telX")+margin, getValue("telY"));
    pg.drawString(getString("faxV"), getValue("faxX")+margin, getValue("faxY"));
    pg.drawString(getString("emailV"), getValue("emailX")+margin, getValue("emailY"));
    pg.drawString(getString("tvaV"), getValue("tvaX")+margin,  getValue("tvaY"));
     
    int largeur=getValue("clientLargeur");
    int steps=15;
    
    pg.drawRect(getValue("clientX"), getValue("clientY"), getValue("clientLongeur"), 
            largeur);
    
    Font helv2 = new Font("Helvetica", Font.BOLD, getValue("clientT")); 
    pg.setFont(helv2); 
    pg.drawString(tier.intitule2, getValue("clientX") + 10,   getValue("clientY") + steps); steps=steps+(largeur/6);
    helv2 = new Font("Helvetica", Font.PLAIN, getValue("clientT")-2);
    pg.setFont(helv2);
   // pg.drawString(tier.intitule2, getValue("clientX" ) + 10,  getValue("clientY" ) + steps); steps=steps+(largeur/6);
    pg.drawString(tier.adresse  , getValue("clientX" ) + 10,  getValue("clientY" ) + steps); steps=steps+(largeur/6);
    pg.drawString(tier.tel      , getValue("clientX" ) + 10,  getValue("clientY" ) + steps); steps=steps+(largeur/6);
    pg.drawString(tier.email    , getValue("clientX" ) + 10,  getValue("clientY" ) + steps); steps=steps+(largeur/6);
    pg.drawString("TIN : "+tier.divers   , getValue("clientX" ) + 10,  getValue("clientY" ) + steps); steps=steps+(largeur/6);
    
    Font helv1 = new Font(Font.SERIF, Font.ITALIC, getValue("factureT"));
    pg.setFont(helv1);
   //System.out.println("FACTURE "+getString("factureV") + (numero)+getValue("factureX")+margin+ getValue("factureY"));
     
    pg.drawString(vars.modality+" "+vars.numero, getValue("factureX")+margin, getValue("factureY"));
 // pg.drawString("FACTURE "+getString("factureV") + (numero), getValue("factureX")+margin, getValue("factureY"));
     
  
pg.drawString("REFUND BOOK ", getValue("referenceX")+margin, getValue("referenceY")-50); 
pg.drawString("Entry No.:" +vars.numero, getValue("referenceX")+margin, getValue("referenceY")-35);
pg.drawString("Refund Receipt No.(NR):" +vars.numero, getValue("referenceX")+margin, getValue("referenceY")-15);
pg.drawString("Refund Receipt No.(NS):" +rp.RECEIPT_NUMBER2, getValue("factureX")+margin, getValue("referenceY")+05);
 
      
} 

void setFooter()
{
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
    Font helv2 = new Font("Helvetica", Font.BOLD, getValue("prixTaille"));
    pg.setFont(helv2);
    int lp = getValue("lengthPrix");
    String[] enteteP = new String [lp+1];
    int[] longeurP = new int[lp+1];
    String [] variableP= new String [lp];
    longeurP[0]=0;

    for(int k=0;k<lp;k++)
    {
    enteteP[k]=getString("prix_"+(k+1)+"Valeur");
    if(enteteP[k].length()>2)
    enteteP[k]=enteteP[k]+vars.devise;
    ibiciro[k]=ibiciro[k]/vars.taux;
    longeurP[k+1]=getValue("prix_"+(k+1)+"Largeur");
    
    //System.out.println("prix_"+(k+1)+"Largeur "+ longeurP[k+1]);
    variableP[k]=getString("prix_"+(k+1)+"Variable"); 
    }
    
    int Xprix = getValue("longeurY");
    int marginP=getValue("longeurX");
    int  w =marginP ;
                         
    for (int i = 0; i < lp; i++) {
    w += (longeurP[i]); 
    
    int wid=w + (longeurP[i + 1]) - 20; 
    
    if(variableP[i].contains("int"))
    printInt(pg,setVirguleD(""+((int)ibiciro[i])), wid, Xprix+15);
    else if(variableP[i].equals("double"))    
    printInt(pg,setVirgule(((double)ibiciro[i])), wid, Xprix+15);
     
    pg.drawLine(w,Xprix-20, w, Xprix+20);//ligne zihagaze
    pg.drawString(enteteP[i], w + 2, Xprix - 5);// amazina 
    //+client.devise
    }
    
    if(lp!=0)
    {
    pg.drawLine(marginP, Xprix, w, Xprix);//umurongo wo hasi
    pg.drawRect(marginP, Xprix-20 , w-marginP , 40); // cadre ya entete
    }
    
helv2 = new Font("Helvetica", Font.PLAIN, 10 );
pg.setFont(helv2);

pg.drawString(   "REASON FOR REFUND  :   "+raison, 
         getValue("footer_7X" )-100, getValue("footer_7Y" ));
 pg.drawString(   "Name and Customer's signature : ---------------------------------- ", 
         getValue("footer_7X" )-100, getValue("footer_7Y" )+30);
pg.drawString(    "Name and Seller's signature   :------------------------------------- ",
       getValue("footer_7X" )-100, getValue("footer_7Y" )+60);

int diff= getValue("noticecadreDiff");   
pg.drawRect(margin+getValue("noticecadreX"), Xprix+18+diff , getValue("noticecadreL") , getValue("noticecadreR")); // cadre ya entete
pg.drawString("Note: Refund log book must befilled by the user of EBM with information indicating"
        + " that a previously issued NS (normal sale) ",   getValue("noticeligne1X" ) ,  Xprix+30+diff);
pg.drawString("receipt contained incorrect information. Both NS and NR (normal refund)"
        + " receipts must be enclosed here in for audit",  getValue("noticeligne1X" ),   Xprix+30+diff+diff);
    
}

void gufungaHasi(int y, int g,int marginFooter, String [] entete, int [] longeur)
{
    int w = margin;
    for (int i = 0; i < g; i++) {
    w += (longeur[i]);
    boolean gusota=(i<g+1 && longeur[i+1]!=0);
    // KWANDIKA ENTETE J I
    if(gusota)
    {
    pg.drawLine(w,y, w, marginFooter );//ligne zihagaze
    pg.drawString(entete[i], w + 2, y +15   );// amazina
    } 
    }  
    pg.drawLine(margin, marginFooter, w, marginFooter );//umurongo wo hasi
    pg.drawRect(margin, y , w - margin,25   ) ; // cadre ya entete
}
private void hagati () {   
    
    Font  enteteFont = new Font("Helvetica", Font.BOLD, getValue("enteteTaille"));
    Font  dataFont = new Font("Helvetica", Font.PLAIN, getValue("ecriture")); 
    
    pg.setFont(dataFont);
    FontMetrics fm = pg.getFontMetrics(dataFont);
    int fontHeight = fm.getHeight(); 
    int marginFooter =getValue("marginfooter"); // aho data zigarukira kurupapuro rwanyuma
    pageNum++;    
            int pageY=getValue("pageNum");
            int g = getValue("length");
            String[] entete = new String [g];
            int[] longeur = new int[g+1];
            String[] variable= new String [g];
            int [] dimension=new int[g];
            longeur[0]=0;

            int page=1; 
            int w = margin;
            int curHeightLigne = getValue("ligne"+page+"X");
            int y=  getValue("entete"+page+"X");

            for(int k=0;k<g;k++)
            { 
            entete[k]=getString("entete_"+(k+1)+"Valeur");
            longeur[k+1]=getValue("entete_"+(k+1)+"Largeur"); 
            variable[k]=getString("entete_"+(k+1)+"Variable");
            dimension[k]=getValue("entete_"+(k+1)+"Dimension"); 
            } 
 
 
            boolean done = true; 
            
for (int j = 0; j < lotList.size(); j++) 
{ 
    
            Lot ci = lotList.get(j);
             
 if (done) {curHeightLigne += fontHeight;} else { curHeightLigne += 2 * fontHeight;}
            w=margin;
            done = true;
            // KWANDIKA UMURONGO  J
            for (int i = 0; i < g; i++) {
                
            w += (longeur[i]);
            
            boolean gusota=(i<g+1 && longeur[i+1]!=0);
            // KWANDIKA DATA J I
            if(gusota)
            {    
            String valeur=ci.vars[i];
            if(valeur==null)
                valeur="";
            String variab=variable[i];
            int dim      =dimension[i];
            //System.out.println(valeur +"  " +variab );
            if(variab.contains("int"))
            printInt(pg,setVirguleD(""+((int)Double.parseDouble( valeur))), w + (longeur[i + 1]) - 5, curHeightLigne);
            else if(variab.equals("double"))     
            printInt(pg,setVirgule (Double.parseDouble( valeur)), w + (longeur[i + 1]) - 5, curHeightLigne);
            
            else if (valeur.length() < dim)
            {                
            pg.drawString(valeur, w + 5, curHeightLigne);
            }
            else
            {
            done = false;
            int index=kataNeza(valeur,dim);
            pg.drawString(valeur.substring(0,index), w + 5, curHeightLigne);
            pg.drawString(valeur.substring(index, valeur.length()), w + 5, fontHeight + curHeightLigne);
            }
            }
            } 
             
           // KUREBA NIBA BIDASOHOKA 
            
            // NIBA IYI PAGE IRENGA AHO PAGE YANYUMA IGARUKIRA RANGIZA CURRENT
          if ((curHeightLigne+20) >  marginFooter-20) {

            pg.setFont  (enteteFont); 
            gufungaHasi(  y,   g,   marginFooter,  entete,     longeur); 
            /////////////////////////////////////////////////////////////////////////////////////////////
            pg.dispose();
            pg = pjob.getGraphics(); 
            
            Font helv1 = new Font("Helvetica", Font.PLAIN, getValue("factureT"));
            pg.setFont(helv1);
            pg.drawString(vars.modality+getString("factureV") + (vars.numero), margin, margin + 25); 
            helv1 = new Font("Helvetica", Font.BOLD, 10);
            pg.setFont(helv1);
            pageNum++;page=2;
            pg.drawString("Page  " + pageNum, pageW / 2,pageY);
            curHeightLigne=getValue("entete"+page+"X")+25;
            y=getValue("entete"+page+"X");
            done = true;
             pg.setFont(dataFont);
            } // FIN TEST IYINDI PAGE
          
 } // FIN GUSHUSHANYA AMADATA

 pg.setFont(enteteFont); 
if(pageNum>1)
page=2;
y=getValue("entete"+page+"X");
int longeurLigne =getValue("longeurLigne");

            if((curHeightLigne+20) > longeurLigne )
            { 
            pg.setFont(enteteFont);
            gufungaHasi(  y,   g,    marginFooter,  entete,     longeur);
            /////////////////////////////////////////////////////////////////////////////////////////////
            pg.dispose();
            pg = pjob.getGraphics();
            
            Font helv1 = new Font("Helvetica", Font.PLAIN, getValue("factureT"));
            pg.setFont(helv1);
            pg.drawString(getString("factureV") + vars.numero, margin, margin + 25);
            helv1 = new Font("Helvetica", Font.BOLD, 10);
            pg.setFont(helv1);
            pageNum++;page=2;
            pg.drawString("Page  " + pageNum, pageW / 2,pageY);
              curHeightLigne=getValue("entete"+page+"X")+25;
            done = true;             
            gufungaHasi(  y,   g,   longeurLigne,  entete,     longeur);  
            }
            else
            { 
            gufungaHasi(  y,   g,    longeurLigne,  entete,     longeur);  
            }
            
           
  
} 
private int getValue(String s)
{

int ret=0; 
Variable var= db.getParam("PRINT",vars.mode,s+vars.mode);
 if(var!=null)
 {
Double val=new Double (var.value);
ret = val.intValue();
}

return ret;
}
String getString(String s)
{
String ret=" ";
 Variable var= db.getParam("PRINT",vars.mode,s+vars.mode);
 if(var!=null)
     ret=var.value;
     return ret;
}
public void printInt(Graphics pg,int nbre,int w,int h)
  {
  String s=setVirgule(nbre);

  int back=0;

  for(int i=s.length()-1;i>=0;i--)
  {
      if(s.charAt(i)==' ')
      back+=2;
      else
       back+=5;

      pg.drawString(""+s.charAt(i),w-back,h);

  }
}
public void printInt(Graphics pg,String s,int w,int h)
{
int back=0;
for(int i=s.length()-1;i>=0;i--)
  {
      if(s.charAt(i)==' ')
      back+=2;
      else
       back+=5;
pg.drawString(""+s.charAt(i),w-back,h);
}
}
String setVirgule(int frw)
{
String setString = ""+frw;
int l =setString.length();
if(l>3 && l<=6)
setString= setString.substring(0,l-3)+" "+setString.substring(l-3);
if(l>6)
{
String s1 = setString.substring(0,l-3);
int sl=s1.length();
setString=s1.substring(0,sl-3)+" "+s1.substring(sl-3)+" "+setString.substring(l-3)  ;
}
return setString;
}
String setVirgule(String setString, String uburyo)
{
    String espace=" ";
    if(uburyo.equals("int"))
            espace=" ";
    else     if(uburyo.equals("akadomoInt"))
              espace=" .";
    else  if(uburyo.equals("virguleInt"))
                espace=" ,";
    
int l =setString.length();
if(l>3 && l<=6)
setString= setString.substring(0,l-3)+espace+setString.substring(l-3) ;
if(l>6)
{
String s1 = setString.substring(0,l-3);
int sl=s1.length();
setString=s1.substring(0,sl-3)+espace+s1.substring(sl-3)+espace+setString.substring(l-3)  ;
}
return setString;
}  
String setVirguleD(String frw)
{
 //   System.out.println("**********before********"+frw);
    String setString = frw;
    if(frw.contains("."))
    {
    try
    {
     double d=Double.parseDouble(frw)  ; 
     
        d =((int)(d*1000))/1000.0;
        frw=""+frw;
        // System.out.println("********after**********"+frw);
StringTokenizer st1=new StringTokenizer (frw);
String entier =st1.nextToken(".");
//System.out.println(frw);
String decimal=st1.nextToken("").replace(".", "");
if(decimal.length()==1 && decimal.equals("0") )
decimal=".00";
else if (decimal.length()==1 && !decimal.equals("0") )
decimal="."+decimal+"0";
else
decimal="."+decimal.substring(0, 2);
  setString = entier+decimal;
    }
    catch(Throwable e)
    {System.out.println(frw+e);}
    

int l =setString.length();
if(l<2)
setString = "    "+frw;
else if(l<3)
setString = "  "+frw;
else if(l<4)
setString = "  "+frw;
int up=6;
int ju=up+3;
if(l>up && l<=ju)
{
setString= setString.substring(0,l-up)+","+setString.substring(l-up) ;
}
else if(l>ju)
{
setString=setString.substring(0,l-9)+","+setString.substring(l-9,l-6)+","+setString.substring(l-6,l)  ;
}
}
else
        return setVirgule(frw);
    
return setString;
}
String setVirgule(String setString)
{
int l =setString.length();
if(l>3 && l<=6)
setString= setString.substring(0,l-3)+" "+setString.substring(l-3) ;
if(l>6)
{
String s1 = setString.substring(0,l-3);
int sl=s1.length();
setString=s1.substring(0,sl-3)+" "+s1.substring(sl-3)+" "+setString.substring(l-3)  ;
}
return setString;
} 
String setVirgule(double frw)
    {
        String setString="";
        frw=frw*negatif;
        
       // System.out.println(frw +" negatif  " +negatif );
        if(frw>99999)
        {
            BigDecimal big=new BigDecimal(frw);
            int value=big.intValue();
            float dec=(float)(frw-value);
            
            StringTokenizer st1=new StringTokenizer (""+dec);
            st1.nextToken(".");
            
            String decimal=st1.nextToken("").replace(".", "");
            
            if(decimal.length()==1 && decimal.equals("0") )
                decimal=".00";
            else if (decimal.length()==1 && !decimal.equals("0") )
                decimal="."+decimal+"0";
            else
                decimal="."+decimal.substring(0, 2);
            
            setString = ""+value;  
            
            String toret=setVirgule2(setString)+decimal;
            if(negatif==-1 && frw>0)
                toret="-"+toret;
            return toret;
        }
        else
        {
            StringTokenizer st1=new StringTokenizer (""+frw);
            String entier =st1.nextToken(".");
            //System.out.println(frw);
            String decimal=st1.nextToken("").replace(".", "");
            
            if(decimal.length()==1 && decimal.equals("0") )    
                decimal=".00";
                    
            else if (decimal.length()==1 && !decimal.equals("0") )
                decimal="."+decimal+"0";
            else
                decimal="."+decimal.substring(0, 2);
            String toret=setVirgule2(""+entier)+decimal; 
            if(negatif==-1 && frw>0)
                toret="-"+toret;
            return toret;
        }
         
}

static String setVirgule2(String frw)
   {
       String setString="";
       int k=0;
       for(int i=(frw.length()-1);i>=0;i--)
       {
           k++;
           if((k-1)%3==0 && (k-1)!=0)
           {
               setString+=",";
           }
           setString+=frw.charAt(i);
       }
       return inverse(setString);
   }
   
   static String inverse(String frw)
   {
       String l="";
       for(int i=(frw.length()-1);i>=0;i--)
       {
           l+=frw.charAt(i);
       }
       return l;
   }

public String getFacture(int id,String Racine)
{
String fact=""+id; 
for(int i=Racine.length();i<8-fact.length();i++)
Racine=Racine+"0"; 
fact=Racine+fact; 
return fact ;  
}

public static int kataNeza(String m,int l)
{
    String [] espaces=m.split(" ");
    int s=espaces.length;
    int index=0;
    for(int i=0;i<s-1;i++)
    {
        espaces[i]=espaces[i]+" ";
    }
     
    for(int i=0;i<s;i++)
    {  
        index+=espaces[i].length();
        if(index>l)
        {
            index-=espaces[i].length();
            break;
        }
    } 
    return index; 
}


public static void main(String[] arghs)
{


}

}

