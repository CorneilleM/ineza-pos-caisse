/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ADMIN
 */
/*
 * and open the template in the editor.
 * To change this template, choose Tools | Templates
 */
package pos;
 
import java.awt.*;
import java.awt.event.*;
import java.util.LinkedList;
import javax.swing.*; 

/**
 *
 * @author ADMIN
 */
public class Askquetions2 extends JFrame
{
    JLabel categorieL ,prioriteL,titleL,phoneL,questionsL,suggestionL;
    JTextField title,phoneF,suggestion;
    JComboBox categorie,priorite;
    JButton sendButton,cancelButton; 
    JPanel mainPane = new JPanel();
    JTextArea question = new JTextArea(); 
    JScrollPane areapane;
    String questions[];
    LinkedList questionlist=new LinkedList();
    int qnumber=0;

     Font police;
     Color color;
     Db db;
     Employe umukozi;
     Container content;
     
    
    public Askquetions2(Db db,Employe m)
    {  
        content = getContentPane();
        police = new Font( db.getString("font"), Integer.parseInt( db.getString("style")),Integer.parseInt(db.getString("length")));
        this.db=db;
        this.umukozi=m;
        makeFrame();
        draw();
        saveButton(); 
    }
    
public void draw()
{
    this.setTitle("ASK   QUESTIONS");
    //End the process when clicking on Close
    enableEvents(WindowEvent.WINDOW_CLOSING); 
    //Define the size of the Comptabilité  ; here, 500 pixels of width and 650 pixels of height
    this.setSize(500,600);
    setVisible(true);
    Image im = Toolkit.getDefaultToolkit().getImage("logo.png");
    this.setIconImage(im); 
    this.setResizable(false);

    makePane(content);
}
public void makeFrame()
{
       categorieL=new JLabel(db.l(db.lang,  "V_CAT")); 
       categorieL.setFont(police); 
       
       suggestionL=new JLabel(db.l(db.lang,  "V_SUGGESTION")); 
       suggestionL.setFont(police); 
       
       prioriteL=new JLabel(db.l(db.lang,  "V_PRIORITE"));
       prioriteL.setFont(police);
       
       titleL=new JLabel(db.l(db.lang,  "V_TITRE"));
       titleL.setFont(police); 
       
       phoneL=new JLabel(db.l(db.lang,  "V_TEL")); 
       phoneL.setFont(police);
 
       questionsL=new JLabel(db.l(db.lang,  "V_IKIBAZO")); 
       questionsL.setFont(police);

       phoneF = new JTextField(""+umukozi.TEL_EMPLOYE); 
       phoneF.setFont(police); 
       
       categorie = new JComboBox(new String[]{"LIVRAISON","FACTURE","COMMANDER","CLOUD"}); 
       categorie.setFont(police); 
       
       priorite = new JComboBox(new String[]{"LOW","MEDIUM","HIGH","EXTREMELY HIGH"}); 
       priorite.setFont(police);  
       
       title = new JTextField(""); 
       title.setFont(police); 
       
       phoneF = new JTextField(""+umukozi.TEL_EMPLOYE); 
       phoneF.setFont(police);  
       
       suggestion = new JTextField(""); 
       suggestion.setFont(police);  
       
       question=new JTextArea(); 
//        title = new JTextField("");
//        police = new Font("Consolas", Font.BOLD, 14);
//        title.setFont(police); 

        sendButton = new JButton("SEND");
        police = new Font("Rockwell", Font.BOLD, 14);
//        saveButton.setPreferredSize(new Dimension(20,10));
        sendButton.setFont(police); 
        
         cancelButton = new JButton("CANCEL");
        police = new Font("Rockwell", Font.BOLD, 14);
//        add.setPreferredSize(new Dimension(20,10));
        cancelButton.setFont(police);
        
        
       areapane=new JScrollPane(question); 
      
}


    public void makePane(Container content)
{

    JPanel panelName=new JPanel();
    panelName.setLayout(new GridLayout(6,2,5,20));//
    panelName.setPreferredSize(new Dimension(400,400));

    
    panelName.add(categorieL);
    panelName.add(categorie);
    panelName.add(prioriteL);
    panelName.add(priorite);
    panelName.add(phoneL);
    panelName.add(phoneF);
    panelName.add(titleL);
    panelName.add(title);
    panelName.add(questionsL); 
   areapane=new JScrollPane(question);
   areapane.setPreferredSize(new Dimension(100,100)); 
//    JPanel paneArea=new JPanel();
//    paneArea.setLayout(new FlowLayout( ));//
//    paneArea.setPreferredSize(new Dimension(450,120));
    panelName.add(areapane); 
   // panelName.add(paneArea);
    panelName.add(suggestionL);
    panelName.add(suggestion);
    
    JPanel pane2=new JPanel();
    pane2.setLayout(new GridLayout(3,2,5,20));//
    pane2.setPreferredSize(new Dimension(100,100));
    
    JPanel panelList = new JPanel();
   // panelList.setLayout(new FlowLayout( ));
    panelList.setPreferredSize(new Dimension(200, 100)); 
    panelList.add(sendButton);
    panelList.add(cancelButton);
   

    mainPane.add(panelName,BorderLayout.NORTH);
    mainPane.add(pane2, BorderLayout.CENTER);
    mainPane.add(panelList,BorderLayout.SOUTH);

    content.add(mainPane);
    //return mainPane;
}  
    public void makestring()
    {
    String tosend="";
    tosend=db.getString("package")+";"+umukozi.NOM_EMPLOYE+";"+umukozi.TITRE_EMPLOYE+";"+categorie.getSelectedItem()+";"+priorite.getSelectedItem()+";"+title.getText()+";"+question.getText()+";"+suggestion.getText()+";"+phoneF.getText();
    tosend=tosend.replaceAll(";;",";-;");
    System.out.println(tosend);
    CommCloud product_Cloud=new CommCloud("http://www.ishyiga.com/test_cloud/ishyiga_support.php?user=Domitille&soc="+db.dbName,"data="+tosend);  
    System.out.println("ioioi   "+product_Cloud.sendlot); 
    
    if(product_Cloud.sendlot.contains("Question sent successfully"))
    {
        JOptionPane.showMessageDialog(null,db.l(db.lang,"V_QUESTION_SENT"),db.l(db.lang, "V_MERCI"),JOptionPane.INFORMATION_MESSAGE);    
        this.dispose();
    }
    else
    {
        JOptionPane.showMessageDialog(null,db.l(db.lang,"V_QUESTION_FAIL"),db.l(db.lang, "V_IKIBAZO"),JOptionPane.ERROR_MESSAGE);
    }

    } 
    
    
//      public void   deleteButon()
//    {
//        deleteButton.addActionListener(new ActionListener()
//        {public void actionPerformed(ActionEvent ae)
//        {
//            if(ae.getSource()==deleteButton&&questionlist.size()>0)
//            {
//                {
//                    
//                    try{
//                    questionlist.remove(list.getSelectedIndex());
//                    questions=getArray(questionlist);
//                    list.setListData(questions);  
//                    area.setText("");
//                    }
//                    catch(Exception e)
//                    {JOptionPane.showMessageDialog(null,db.l(db.lang, "V_SELECT")+" "+db.l(db.lang, "V_QUESTION")+" "+db.l(db.lang, "V_SVP"),db.l(db.lang, "V_IKIBAZO"),JOptionPane.WARNING_MESSAGE);}
//                } 
//            }
//            else
//                JOptionPane.showMessageDialog(null,db.l(db.lang, "V_SELECT")+" "+db.l(db.lang, "V_QUESTION")+" "+db.l(db.lang, "V_SVP"),db.l(db.lang, "V_IKIBAZO"),JOptionPane.WARNING_MESSAGE);
//                       
//        } 
//    });
// }
    
    
    
    public void saveButton()
    {
        sendButton.addActionListener(new ActionListener()
        {public void actionPerformed(ActionEvent ae)
        {
            if(ae.getSource()==sendButton)
            {
                {
                  makestring();
                }
     
            }
        } 
    });
 }
    
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public static int giveinteger (String value)
{
                      int integer=-1;
    try
    { 
        if(value.equals(""))
        {
         
JOptionPane.showMessageDialog(null," "+" ! ",value,JOptionPane.PLAIN_MESSAGE); //KORA FONCTION YABYO 
        integer=-1;
        }
        else 
        {
        Integer in_int=new Integer(value);
        integer = in_int.intValue();
        if(integer<0 || integer>100)
             {
         
JOptionPane.showMessageDialog(null," "+" ! ","0 > "+integer+" > 100",JOptionPane.PLAIN_MESSAGE);   
        integer=-1;
        }
        
        
        }
         }
    catch (NumberFormatException e)
    {
     JOptionPane.showMessageDialog(null,value,""+" ! ",JOptionPane.PLAIN_MESSAGE);   
   
    integer=-1;
    }
    return integer;
}

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
public static void main(String[] args) { 
    }
}
