package pos;

/*
Interface Ishyiga copyright 2007 Kimenyi Aimable Gestion des ventes et du stock
*/
import java.awt.*;
import java.awt.event.*;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import java.util.*;
import java.util.Date;
import javax.swing.JFrame;



public  class MainTiers extends JFrame
{

  JFrame frame;
  String datez;
  JList allClientList=new JList();
  final int LENTH=700;
  final int WIDT =900;
  JButton spy,vol,risk,view;
  JTextField debut,fin,search,sup,inf;
  JLabel debutL,finL,searchL,supL,infL,total,totalpriceL;
  JTable result;
  LinkedList<Client> client;
  LinkedList<Credit> CreditRes;

    String driver = "org.apache.derby.jdbc.NetWorkDriver";
    String dbName="ishyiga";  String  date,mois;
    String connectionURL;// = "jdbc:derby://localhost:1527/" + dbName + ";create=true";
    ResultSet outResult;
    Connection conn = null;
    Statement s;
    String server;
    PreparedStatement psInsert;
    
     public Db db;
    String nom;Container content;

    int totalprice=0;
MainTiers(String nom,String server )
{
    this.server=server;
    connectionURL = "jdbc:derby://"+server+":1527/" + dbName + ";create=true";
    this.nom=nom;
    frame=this;
    read();
    client=getOldClient("_RAMA");
    showProduct();
    getResult("","",2,3,"");
    draw();


}
 public  void read()
{
  Date d= new Date();

        String day="";
        String mm="";
        String year="";

         if(d.getDate()<10)
        day="0"+d.getDate();
        else
        day=""+d.getDate();
        if((d.getMonth()+1)<10)
        mm="0"+(d.getMonth()+1);
        else
        mm=""+(d.getMonth()+1);
        if((d.getYear()-100)<10)
        year="0"+(d.getYear()-100);
        else
        year=""+(d.getYear()-100);



                 datez =day+mm+year;

                 mois= mm;

    try {
     conn = DriverManager.getConnection(connectionURL);
    s = conn.createStatement();

    }
    catch (Throwable e){

      insertError(e+"","Read");
    }
}
void draw()
{
    
 content = getContentPane();
westPane(content);
centerPane(content);
eastPane(content);
view();
spy();
    setTitle(nom+"'s Ishyiga Clients Manager ");
enableEvents(WindowEvent.WINDOW_CLOSING);
		//on fixe  les composant del'interface
		setSize(WIDT,LENTH);
		setVisible(true);
}
/****************************************************(900,700)**********************/
public void westPane(Container content )
{

     JPanel commonWestPane = new JPanel();
     commonWestPane.setPreferredSize(new Dimension (350,700));

   JPanel  centerPane    = new JPanel();
centerPane.setSize(new Dimension(300,420));

    JScrollPane    scrollPaneProductList =new JScrollPane(allClientList);
	scrollPaneProductList.setPreferredSize(new Dimension(290,410));
	allClientList.setBackground(Color.pink);


	centerPane.add(scrollPaneProductList,BorderLayout.SOUTH);

commonWestPane.add(centerPane, BorderLayout.CENTER);
content.add(commonWestPane,BorderLayout.WEST);




}

/**************************************************************************/
public void centerPane(Container content )
{
     JPanel commonCenterPane = new JPanel();
     commonCenterPane.setPreferredSize(new Dimension (100,700));

     JPanel options = new JPanel();
     options.setLayout(new GridLayout(2,1));
     options.setSize(new Dimension(100,600));


     debutL=new JLabel ("Debut");debut=new JTextField();debut.setText("2009-01-04 08:00:00");
     finL=new JLabel ("Fin");    fin=new JTextField();fin.setText("2009-02-27 23:00:00");
     infL=new JLabel ("Inf");    inf=new JTextField();inf.setText("0");
     supL=new JLabel ("Sup");    sup=new JTextField();sup.setText("100000");

JPanel var = new JPanel();
var.setLayout(new GridLayout(8,1));
var.setSize(new Dimension(90,250));
     var.add(debutL);var.add(debut);
     var.add(finL);var.add(fin);
     var.add(infL);var.add(inf);
     var.add(supL);var.add(sup);
 JPanel choice = new JPanel();
     choice.setLayout(new GridLayout(4,1));
     choice.setSize(new Dimension(90,250));

     spy= new JButton("SPY");
     vol= new JButton("VOL");
     risk= new JButton("RISK");
     view= new JButton("VIEW");
     choice.add(spy);choice.add(vol);choice.add(risk);choice.add(view);

     options.add(var);
     options.add(choice);

     commonCenterPane.add(options, BorderLayout.CENTER);

     content.add(commonCenterPane,BorderLayout.CENTER);
     
}
String [][] newResult(int check)
{
    String [][]s1=null;
    totalprice=0;
    int linge=CreditRes.size();
    if(check==0)
    { s1=new String [linge][6];
        for(int i=0;i<linge;i++)
        {


            Credit C =CreditRes.get(i);

        s1[i][0]=""+C.ID_INVOICE;
        s1[i][1]=C.NUMERO_AFFILIE;
        s1[i][2]=C.NUMERO_QUITANCE;
        s1[i][3]=""+C.SOLDE;
        s1[i][4]=""+C.PAYED;
        s1[i][5]=""+(C.SOLDE*C.PAYED);
        totalprice+=(C.SOLDE*C.PAYED);
        }
    }
    if(check==1)
    { s1=new String [linge][4];
        for(int i=0;i<linge;i++)
        {


            Credit C =CreditRes.get(i);

        s1[i][0]=C.NUMERO_AFFILIE;
        s1[i][1]=C.NOM;
        s1[i][2]=C.PRENOM;
        s1[i][3]=""+C.PAYED;
        totalprice+=(C.PAYED);
        }
    }
       return s1;
}
/**************************************************************************/
public void eastPane(Container content )
{
     JPanel commonEastPane = new JPanel();
     commonEastPane.setPreferredSize(new Dimension (400,700));

        result = new javax.swing.JTable();
        
        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);


        result.setModel(new javax.swing.table.DefaultTableModel(newResult(0),new String [] {"Num", "Date","Article", "Qté","P.U","P.T"})
        {
            Class[] types = new Class [] {
               
                java.lang.String.class,
                java.lang.String.class,
                java.lang.String.class,
                java.lang.String.class,
                java.lang.String.class,
                 java.lang.String.class

            };

            @Override
            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });

 JScrollPane jTableList =new JScrollPane(result);
jTableList.setPreferredSize(new Dimension(300,500));

JPanel tot = new JPanel();
     tot.setLayout(new GridLayout(1,2));
     tot.setSize(new Dimension(200,50));

 total =new JLabel("TOTAL : ");
totalpriceL=new JLabel(""+totalprice);

tot.add(total);
tot.add(totalpriceL);


commonEastPane.add(jTableList,BorderLayout.NORTH);
commonEastPane.add(tot,BorderLayout.SOUTH);

content.add(commonEastPane,BorderLayout.EAST);



}
    public void showProduct()
{
   int c=client.size();

    Client [] control= new Client [c];
    for(int i=0;i<c;i++)
    {
     control[i]=client.get(i);
    }

allClientList.setListData(control);

}
    public static int getInt (String s)
{
    int entier=-1;
  try
    { Integer in_int=new Integer(s);
      entier = in_int.intValue();
    }
  catch (NumberFormatException e)
    {

    }

    return entier;
}

public void view()
{  view.addActionListener(
		new ActionListener()
    {  public void actionPerformed(ActionEvent ae)
	{  if(ae.getSource()==view && allClientList.getSelectedValue()!=null)
	{

               Client c = (Client)allClientList.getSelectedValue();
 getResultView(c.NUM_AFFILIATION);

 result.setModel(new javax.swing.table.DefaultTableModel(newResult(0),new String [] {"Id", "Num","Article", "Qté","P.U","P.T"})
        {
            Class[] types = new Class [] {

                java.lang.String.class,
                java.lang.String.class,
                java.lang.String.class,
                java.lang.String.class,
                java.lang.String.class,
                 java.lang.String.class

            };

            @Override
            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        totalpriceL.setText(""+totalprice);
       }}});

}
public void spy()
{  spy.addActionListener(
		new ActionListener()
    {  public void actionPerformed(ActionEvent ae)
	{  if(ae.getSource()==spy )
	{
CreditRes= new LinkedList();
           int s=getInt(sup.getText());
           for(int i=0;i<2000;i++)
           {
               Client c = client.get(i);
               int tot=getResultSpy(c.NUM_AFFILIATION);
              if( tot>s)
              {Credit k = new Credit(0, c.NUM_AFFILIATION, "", 0,tot, "", "", c.NOM_CLIENT, c.PRENOM_CLIENT, 0, "");
                    CreditRes.add(k);
              System.out.println(i+"     "+c+"     "+tot);
              }


           }


 result.setModel(new javax.swing.table.DefaultTableModel(newResult(1),new String [] {"Num", "Nom","Prenom", "Total"})
        {
            Class[] types = new Class [] {

                java.lang.String.class,
                java.lang.String.class,
                java.lang.String.class,
                java.lang.String.class

            };

            @Override
            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        totalpriceL.setText(""+totalprice);
       }}});

}
public void getResult(String d,String f,int in,int su,String db)
{
    CreditRes= new LinkedList();

    /*    try {
            outResult = s.executeQuery("select CREDIT.ID_INVOICE,NUMERO_AFFILIE,NAME_PRODUCT,quantite,date,price,nom_client,prenom_client  " +
                    "from invoice, APP.CLIENT"+db+",credit,list,product" +
                    " where invoice.num_client='"+db.replaceAll("_", "")+"' " +
                    "and invoice.HEURE >'"+d+"' AND invoice.HEURE <'"+f+"'" +
                    "and invoice.total >"+in+" AND invoice.total <"+su+" " +
                    "and credit.id_invoice=invoice.id_invoice and list.id_invoice=invoice.id_invoice and list.code_uni=product.code" +
                    "and credit.NUMERO_AFFILIE =APP.CLIENT"+db+".num_affiliation   ");
            while (outResult.next()) {
                
        int  ID_INVOICE = outResult.getInt(1);
        String NUMERO_AFFILIE = outResult.getString(2);
        int quantite = outResult.getInt(4);
        double price = outResult.getInt(6);
        String designation = outResult.getString(3);
        String dat = outResult.getString(5);
        String NOM = outResult.getString(7);
        String PRENOM = outResult.getString(8);
        
        Credit c = new Credit(ID_INVOICE, NUMERO_AFFILIE, designation, quantite,(int) price, "", "", NOM, PRENOM, 0, dat);
CreditRes.add(c);
            }
        } catch (SQLException ex) {
            Logger.getLogger(MainTiers.class.getName()).log(Level.SEVERE, null, ex);
        }*/
 Credit c = new Credit(1, "kim", "bro", 0,(int) 2, "", "", "IME", "AIM", 0, "200909");
CreditRes.add(c);
    
}
public void getResultView(String numAff)
{
    CreditRes= new LinkedList();

       try {
            outResult = s.executeQuery("select CREDIT.ID_INVOICE,NUMERO_AFFILIE,NAME_PRODUCT,quantite,price from credit,list,product where  credit.NUMERO_AFFILIE='"+numAff+"'   and list.id_invoice=credit.id_invoice and list.code_uni=product.code ");
            while (outResult.next()) {

        int  ID_INVOICE = outResult.getInt(1);
        String NUMERO_AFFILIE = outResult.getString(2);
        int quantite = outResult.getInt(4);
        double price = outResult.getInt(5);
        String designation = outResult.getString(3);
        String dat = "";
        String NOM = "";
        String PRENOM = "";

        Credit c = new Credit(ID_INVOICE, NUMERO_AFFILIE, designation, quantite,(int) price, "", "", NOM, PRENOM, 0, dat);
CreditRes.add(c);
            }
        } catch (SQLException ex) {
            Logger.getLogger(MainTiers.class.getName()).log(Level.SEVERE, null, ex);
        }

}
public int getResultSpy(String numAff)
{
    int  ID_INVOICE=0;
       try {
            outResult = s.executeQuery("select sum (quantite*price) from credit,list,product where  credit.NUMERO_AFFILIE='"+numAff+"'   and list.id_invoice=credit.id_invoice and list.code_uni=product.code ");
            while (outResult.next()) {

          ID_INVOICE = outResult.getInt(1);
       
            }
        } catch (SQLException ex) {
            Logger.getLogger(MainTiers.class.getName()).log(Level.SEVERE, null, ex);
        }
return ID_INVOICE;
}

public LinkedList <Client> getOldClient(String table)
{
 Client c =null;
   client = new LinkedList();
      try
    {
         if(table.equals("uni"))
         {
        outResult = s.executeQuery("select * from APP.CLIENT_UNIPHARMA ");
     while (outResult.next())
    {
        String NUM_AFFILIATIONa    =outResult.getString(1);
        String NOM_CLIENTa         =  outResult.getString(2);
        String PRENOM_CLIENTa      =  outResult.getString(3);
        int PERCENTAGEa  =outResult.getInt(4);
        int DATE_EXPa  =outResult.getInt(5);
        String  ASSURANCEa  =  outResult.getString(6);
        String  EMPLOYEURa  =  outResult.getString(7);
        String  SECTEURa  =  outResult.getString(8);
        String  VISAa  =  outResult.getString(9);
        String  CODE  =  outResult.getString(10);

  c =new Client(NUM_AFFILIATIONa,NOM_CLIENTa ,PRENOM_CLIENTa ,PERCENTAGEa,DATE_EXPa,ASSURANCEa ,EMPLOYEURa,SECTEURa,VISAa,CODE );

  client.add(c);
     }
    }

    else
               {
    outResult = s.executeQuery("select * from APP.CLIENT"+table+" order by nom_client,prenom_client,num_affiliation");
     while (outResult.next())
    {
        String NUM_AFFILIATIONa    =outResult.getString(1);
        String NOM_CLIENTa         =  outResult.getString(2);
        String PRENOM_CLIENTa      =  outResult.getString(3);
        int PERCENTAGEa  =outResult.getInt(4);
        int DATE_EXPa  =outResult.getInt(5);
        String  ASSURANCEa  =  outResult.getString(6);
        String  EMPLOYEURa  =  outResult.getString(7);
        String  SECTEURa  =  outResult.getString(8);
        String  VISAa  =  outResult.getString(9);
        String  CODE  =  outResult.getString(10);
  c =new Client(NUM_AFFILIATIONa,NOM_CLIENTa ,PRENOM_CLIENTa ,PERCENTAGEa,DATE_EXPa,ASSURANCEa ,EMPLOYEURa,SECTEURa,VISAa,CODE );
    //JOptionPane.showMessageDialog(null,c," autre ",JOptionPane.PLAIN_MESSAGE);
     client.add(c);

     }
     }
        //  Close the resultSet
    outResult.close();
      }  catch (Throwable e)  {
    insertError(e+"","getOldClient");
    }

return  client;

}
 void insertError(String desi,String ERR)
    {
        try {
            psInsert = conn.prepareStatement("insert into APP.ERROR(DESI,USERNAME)  values (?,?)");

            if(desi.length()>160)
                desi=desi.substring(0, 155);
             if(ERR.length()>160)
                ERR=ERR.substring(0, 155);

            psInsert.setString(1,desi );
            psInsert.setString(2, ERR);



            psInsert.executeUpdate();
            psInsert.close();
            JOptionPane.showMessageDialog(null," b "+desi, db.l(db.lang,"V_IKIBAZO"),JOptionPane.PLAIN_MESSAGE);



        } catch (Throwable ex) {
         JOptionPane.showMessageDialog(null," d "+ex,db.l(db.lang,"V_IKIBAZO"),JOptionPane.PLAIN_MESSAGE);   ;
        }
 }
public static void main(String[] arghs)
{
        try {
            StartServer d;
            d = new StartServer();
            int id = 1;
            new MainTiers("Kimenyi", "localhost");
        } catch (Throwable ex) {
            Logger.getLogger(MainTiers.class.getName()).log(Level.SEVERE, null, ex);
        }
        






}}

