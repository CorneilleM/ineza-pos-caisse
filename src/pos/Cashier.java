
package pos;


/*
cashier for Ishyiga copyright 2007 Kimenyi Aimable Gestion des ventes et du stock accepte 5 produits seulement de plus
*/
import java.util.*;
import javax.swing.JOptionPane;
import smart.MySQLConnector;
import smart.SmartPatient; 


public class Cashier
{

    String cashierName,cashierId;
    public String date;
    public Invoice  current; 	     	// La facture en cours
    LinkedList <Product>allProduct;   	// base de donnees des tous les produits
    LinkedList <Product>allProductVedette; 
    LinkedList<Client> allClient; 
    public Db db;
     public MySQLConnector smartConnection;
    int id_cashier;
    static  double yose2;
    LinkedList  <Vedette>vedette ;
    LinkedList <Credit> credit;
    static int compteur;
    int factNum=0;
    String devise=""; 
    String smartdb="",smartserver="";



public Cashier(int id,String server,String dataBase,String dev,String smartdb,String smartserver)
{ 
initCashier(  id,  server,  dataBase,  dev,  smartdb,  smartserver);
} 


private void initCashier(int id,String server,String dataBase,String dev,String smartdb,String smartserver)
{
     Date d= new Date();
    this.devise=dev;


        String day,mm,year;


        if(d.getDate()<10) {
        day="0"+d.getDate();
    }
        else {
        day=""+d.getDate();
    }
        if((d.getMonth()+1)<10) {
        mm="0"+(d.getMonth()+1);
    }
        else {
        mm=""+(d.getMonth()+1);
    }
        if((d.getYear()-100)<10) {
        year="0"+(d.getYear()-100);
    }
        else {
        year=""+(d.getYear()-100);
    }
            
            

        date=day+mm+year;
          basketStart("");
 
        this.credit=new LinkedList ();
        this.db=new Db (server,dataBase);
        db.devise=dev; 
        db.nom_empl=""+id; 
        db.read();
        db.emp();
        
        this.smartConnection=null;
        if(!smartserver.contains("NOT AVAILABLE") && smartserver.length()>3)
        {
            this.smartConnection=new MySQLConnector(smartserver,smartdb,db);
        }
        Main.tax=new Taxes(server,server, dataBase);
        stock("PRIX","");
 
        this.cashierName=db.getName(id).NOM_EMPLOYE;  
}
public void stock(String table,String  sqlFamille)
{    
this.allProduct  = db.doworkOther(table,sqlFamille);
System.out.println(new Date()+"size prod:"+allProduct.size());
this.vedette = db.doVedette(); 
this.allProductVedette=db.doworkVedette(vedette,table);  

}

/* Faire une facture au debut ou meme l'annulation*/


public void basketStart(String client)
	{ 
		current= new Invoice (cashierName,client,date); 
	}
    /* Saisit d'un produit peut etre ajuste e un scanneur AAAAAAA GERE ALA FIN D ACHAT*/

static long secIn(long time)
{
return (long)(time/1000);
}
public String estDansStock()
{
    String done = "";
     
      if( Main.tax.rraClient.isRRA  ) 
      {
//          String data =  Main.tax.rraClient.sendRequest("ID REQUEST END INVOICE"," "," ",""," ","N","S",0);
          String data =  Main.tax.rraClient.sendRequest("DATE AND TIME REQUEST"," "," ",""," ","N","S",0);
          
          if(data!=null && data.length()>4 && !data.contains("ERROR"))
          {
              System.out.println("IGIHE:::"+data);
              Date imashini=new Date();
              System.out.println("IMASHINI::"+imashini);
              //03/06/2014 11:32:59
              //0123456789012345678
              //IGIHE:::2015-03-22 10:45:05
              //IMASHINI::Sun Mar 22 11:45:05 CAT 2015
              if(data.length()>19)
              {
                  
                  data=data.substring(data.length()-19,  data.length());
              }
              String itariki=data.substring(0,2)+data.substring(3, 5)+data.substring(8, 10);
              System.out.println("IYI TAR:"+itariki);
              if(!itariki.equals(date))
              {
                  db.insertError(data.length()+"SDC TIME:"+data+"MRC:"+imashini, "ISAHA MRC YAMENETSE");
                  JOptionPane.showMessageDialog(null, db.l(db.lang,"V_HEURE"),"ATTENTION",JOptionPane.WARNING_MESSAGE );
                  return "CHECK THE TIME OF YOUR COMPUTER";
              }
              else
              {
                  int day=Integer.parseInt(data.substring(0,2));
                  int mm=Integer.parseInt(data.substring(3,5));
                  int yy=Integer.parseInt(data.substring(6,10));
                  
                  int hh=Integer.parseInt(data.substring(11,13));
                  int min=Integer.parseInt(data.substring(14,16));
                  int ss=Integer.parseInt(data.substring(17,19)); 
                  
                  Date yasdc=new Date(yy-1900,mm-1,day,hh,min,ss);
                  long diff=secIn(Math.abs(yasdc.getTime()-imashini.getTime()));
                  System.out.println("diff:"+diff);
                  if(diff>4000)
                  {
                      db.insertError(data.length()+"SDC TIME:"+data+" \n MRC:"+imashini+" \n DIFF"+diff, "ISAHA MRC YAMENETSE");   
                      JOptionPane.showMessageDialog(null, db.l(db.lang,"V_HEURE"),"ATTENTION",JOptionPane.WARNING_MESSAGE );
                      return "CHECK THE TIME OF YOUR COMPUTER";
                  }
              }
          }
          else
          {
              JOptionPane.showMessageDialog(null, db.l(db.lang,"V_ISHYIGA_CONNECTION"),"ATTENTION",JOptionPane.WARNING_MESSAGE );
              return "ISHYIGA NOT CONNECTED TO RRA VERIFY YOUR SDC DEVICE";
          }
      }
      
      LinkedList <Product> pro= current.getProductListDB();
      for(int j=0;j<pro.size() ;j++)
      {
          Product pi = pro.get(j);
          int  qty =pi.qty;
          
          if(pi.getQuantite()< qty )
          {
              done=pi.productName;
              break;
          }
      }
      return done;
}
/*Fin d'une facture */

public String BASKETENDSILENCIEUX(Credit cr,String waitress)
{ 
return db.doFinishVenteV4Silencieux(current,cr,waitress); 
}
public void BASKETEND(String client,Credit cr,double money,double change,String depot)
{ 
current.id_invoice=db.doFinishVenteV4(current,cr,money,change,depot);
factNum=current.id_invoice; 
}
public void PRESCRIPTIONEND(String client,SmartPatient sp)
{ 
current.id_invoice=db.doFinishPresction(current,sp);
factNum=current.id_invoice;
}
/* Quand un client ne veut pas un produit on peut l enlever*/
Product makeProductRemove (String ligne)
{
   			Product p=null;
		    StringTokenizer st1=new StringTokenizer (ligne);
			Integer code=new Integer(st1.nextToken("****"));
			int productCode = code.intValue();

			//recupere quantite
			Integer quantity=new Integer(st1.nextToken());
			int quantite =quantity.intValue();

				for(int j=0;j<current.getProductList().size() ;j++)
					{
					Product pi = (Product)(current.getProductList().get(j));
					int  sub =pi.productCode();
					int quant=pi.qty;
						if(quant==quantite && sub==productCode )
						{
						 p=pi;
						}
					}
		return p ;
}


/* Pour savoir combien des produits sont deja dans le panier et comparer au stock*/
int howManyProductInSale(int code, int k)
{
    int number=0;
    
    for(int j=0;j<current.getProductList().size() ;j++)
    {
        Product pi = (Product)(current.getProductList().get(j));
        int  codei =pi.productCode();
        
        int quant=pi.qty;
        
        if(codei==code )
        {
            number=number+quant;
        }
    }
    
    return number+k;
}

public Product getProduct(int d)
{
 Product p=null;
 for(int j=0;j<allProduct.size() ;j++)
        {
                Product  pi=(Product)(allProduct.get(j));
                 if(pi.productCode==d ) {
                p=pi;
            }
        }

return p;
}

public Product getProductVedette(int d)
{
 Product p=null;
for(int j=0;j<allProductVedette.size() ;j++)
        {
    
                Product  pi=allProductVedette.get(j);
               
                 if(pi.productCode==d) {
                p=pi;
            }
        }

return p;
}

public String getProductVedetteName(int d)
{
String name="";
for(int j=0;j<allProductVedette.size() ;j++)
        {
         Product  pi=allProductVedette.get(j);
       //   System.out.println(pi.productCode+"----- "+pi.productName);
         if(pi.productCode==d ) {
                name=pi.productName+" x "+ pi.qty ;
            }
        }
//System.out.println(d+"============== "+name);
return name;
}

boolean compatible(Product p,Client cli)
{
    if(!p.INTERACTION.equals("") && !p.DOSAGE.equals("NULL"))
    {
    String [] contre=(p.INTERACTION).split(";");
    for(int k=0;k<contre.length;k++) {
            if(cli.EMPLOYEUR.toUpperCase().contains(contre[k]))
            {
                JOptionPane.showMessageDialog(null, 
                        cli.NOM_CLIENT +" \n OBS : "+cli.EMPLOYEUR 
                        + "\n VS \n"
                        +p.productName +" \n CONTRE : "+p.INTERACTION ,
                        " INCOMPATIBLE ALERT", JOptionPane.ERROR_MESSAGE); 
                return false;
            }
        } 
    //System.out.println(p.observation);
    for(int j=0;j<current.getProductList().size() ;j++)
    {
        Product pi = (Product)(current.getProductList().get(j)); 
        System.out.println(pi.observation);
        for(int k=0;k<contre.length;k++) {  
            if(pi.INTERACTION.toUpperCase().contains(contre[k]))
            {
                JOptionPane.showMessageDialog(null, 
                        pi.productName +" \n OBS : "+pi.INTERACTION 
                        + "\n VS \n"
                        +p.productName +" \n CONTRE : "+p.INTERACTION ,
                        " INCOMPATIBLE ALERT", JOptionPane.ERROR_MESSAGE); 
                return false;
            }
        } 
        String [] thereContre=(pi.INTERACTION).split(" ");
        for(int k=0;k<thereContre.length;k++)  
        {
            if(p.INTERACTION.toUpperCase().contains(thereContre[k]))
            {
                JOptionPane.showMessageDialog(null, 
                        p.productName +" \n OBS : "+p.INTERACTION 
                        + "\n VS \n"
                        +pi.productName +" \n CONTRE : "+pi.INTERACTION ,
                        " INCOMPATIBLE ALERT", JOptionPane.ERROR_MESSAGE);
                return false;
            }}
    } 
    return true;
    }
    else {
        return true;
    }
}

}/* Fin Class Cashier*/