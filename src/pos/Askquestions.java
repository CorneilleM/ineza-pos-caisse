/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pos;

 
import java.awt.*; 
import java.awt.event.*;
import java.util.LinkedList;
import javax.swing.*; 

/**
 *
 * @author w
 */
public class Askquestions extends JFrame{
    JLabel nameL ,emailL,phoneL,questionsL;
    JTextField nameF,emailF,phoneF,title;
    JButton saveButton,add,deleteButton; 
    JPanel mainPane = new JPanel();
    JTextArea area = new JTextArea();
    JTextArea txtdescription = new JTextArea();
    JRadioButton rddocument,rdresource,rdexport,rdstandard,rdautre;
    JRadioButton rdlow,rdmedium,rdhigh,rdextremely;
    JComboBox cbcategorie;
    JComboBox cbpriority;
    JScrollPane areapane;
    JScrollPane listpane;
    
    Employe umukozi;
    Db db;
    
    String questions[];
    LinkedList questionlist=new LinkedList();
    int qnumber=0;

     Font police;
     Color color;
     Container content; 
    
    public Askquestions(Db db,Employe m)
    {   
        this.db=db;
        this.umukozi=m;
        content = getContentPane();
       
        makeFrame();
        draw();
        saveButton();
        addButton(); 
    }
    
public void draw()
{
    this.setTitle("ASK   QUESTIONS");
    //End the process when clicking on Close
    enableEvents(WindowEvent.WINDOW_CLOSING); 
    //Define the size of the Comptabilité  ; here, 500 pixels of width and 650 pixels of height
    this.setSize(800,665);
    Image im = Toolkit.getDefaultToolkit().getImage("logo.png");
    this.setIconImage(im); 
    setVisible(true);
    this.setResizable(true);
    this.setBackground(Color.BLUE);

    makePane(content);
}


public void makeFrame()
{
       nameL=new JLabel("NAME:");
       police = new Font("Arial", Font.BOLD, 14);
       nameL.setFont(police);

       nameF=new JTextField(umukozi.NOM_EMPLOYE);
       police = new Font("Arial", Font.BOLD, 14);
       nameF.setFont(police);
       nameF.setEditable(true);

       phoneL=new JLabel("PHONE:");
       police = new Font("Arial", Font.BOLD, 14);
       phoneL.setFont(police);
 
       emailL=new JLabel("EMAIL:");
       police = new Font("Arial", Font.BOLD, 14);
       emailL.setFont(police);
       
       title=new JTextField(" ");
       police = new Font("Arial", Font.BOLD, 14);
       title.setFont(police);

        phoneF = new JTextField(umukozi.TEL_EMPLOYE);
        police = new Font("Consolas", Font.BOLD, 14);
        phoneF.setFont(police);
        phoneF.setEditable(true);

        emailF = new JTextField("");
        police = new Font("Consolas", Font.BOLD, 14);
        emailF.setFont(police); 
        emailF.setEditable(true);
       
rddocument=new JRadioButton("document");
rdresource=new JRadioButton("resource");
rdexport=new JRadioButton("export..");
rdstandard=new JRadioButton("standard");
rdautre=new JRadioButton("uatre");
rdlow=new JRadioButton("low");
rdmedium=new JRadioButton("medium");
rdhigh=new JRadioButton("high");
rdextremely=new JRadioButton("extremely high"); 

ButtonGroup bg1 = new ButtonGroup();
bg1.add(rdautre);
bg1.add(rdstandard);
bg1.add(rdexport);
bg1.add(rdresource);
bg1.add(rddocument);
 
ButtonGroup bg2 = new ButtonGroup();
bg2.add(rdlow);
bg2.add(rdmedium);
bg2.add(rdhigh);
bg2.add(rdextremely);
 

 
        saveButton = new JButton("SEND");
        police = new Font("Rockwell", Font.BOLD, 14); 
        saveButton.setFont(police);
        
        add = new JButton("ADD");
        police = new Font("Rockwell", Font.BOLD, 14); 
        add.setFont(police);
        
         deleteButton = new JButton("DELETE");
        police = new Font("Rockwell", Font.BOLD, 14); 
        deleteButton.setFont(police);
        
        
       area=new JTextArea(""); 
       areapane=new JScrollPane(area);
       areapane.setPreferredSize(new Dimension(600,100));
       
       txtdescription=new JTextArea("");
       listpane=new JScrollPane(txtdescription);
       listpane.setPreferredSize(new Dimension(600,100));
       
} 

    public void makePane(Container content)
{
    JPanel panelTitle=new JPanel();
    panelTitle.setLayout(new FlowLayout(3,3,20));//
    panelTitle.setPreferredSize(new Dimension(790,220));
    panelTitle.setBackground(new Color(119,210,254));
    JLabel lbltitle=new JLabel("WELCOME TO ISHYIGA SUPPORT"); 
    lbltitle.setFont(new Font("Rockwell", Font.BOLD, 25));
    JLabel lblsubtitle=new JLabel("WELCOME TO ISHYIGA SUPPORT"); 
    lblsubtitle.setFont(new Font("Rockwell", Font.BOLD, 20)); 
    panelTitle.add(new Getpicture(new ImageIcon("image/bgroundd.png").getImage()));
    panelTitle.add(lbltitle);
    JPanel space=new JPanel();
    space.setPreferredSize(new Dimension(90,0));
    panelTitle.add(space); 
    panelTitle.add(new Getpicture(new ImageIcon("image/care.png").getImage()));
     
 
    JPanel paneInfo=new JPanel();
    paneInfo.setLayout(new GridLayout(2,3,40,6));//
    paneInfo.setPreferredSize(new Dimension(780,50));
    paneInfo.setBackground(new Color(119,210,254));
    paneInfo.add(nameL);
    paneInfo.add(phoneL);
    paneInfo.add(emailL);
    paneInfo.add(nameF); 
    paneInfo.add(phoneF); 
    paneInfo.add(emailF);
    
         
    JPanel panePriority=new JPanel();
    panePriority.setLayout(new FlowLayout(3));//
    panePriority.setPreferredSize(new Dimension(780,40));
    panePriority.setBackground(new Color(119,210,254));
    JLabel categoriel=new JLabel("CATEGORIE:");
    categoriel.setFont(new Font("Arial", Font.BOLD, 14));
    JLabel priority=new JLabel("PRIORITY:"); 
    priority.setFont(new Font("Arial", Font.BOLD, 14));
    cbcategorie=new JComboBox(new String[]{"FACTURE","LIVRAISON","COMMANDE","RAPPORT","CLOUD","AUTRES"});
    cbpriority=new JComboBox(new String[]{"LOW","MEDIUM","HIGH","EXTREMELY HIGH"});
    panePriority.add(categoriel);
    panePriority.add(cbcategorie);
    panePriority.add(priority);
    panePriority.add(cbpriority);
    
    
    JPanel paneTitle=new JPanel();
    paneTitle.setLayout(new FlowLayout(3));//
    paneTitle.setPreferredSize(new Dimension(780,40));
    paneTitle.setBackground(new Color(119,210,254));
    JLabel titlel=new JLabel("  TITLE:                      ");
    titlel.setFont(new Font("Rockwell", Font.BOLD, 14));
    title=new JTextField(30); 
    paneTitle.add(titlel);
    paneTitle.add(title);  
    
    JPanel centerPane=new JPanel();
    centerPane.setLayout(new FlowLayout(3));// 
    centerPane.setPreferredSize(new Dimension(790,140));
    centerPane.setBackground(new Color(119,210,254));
    centerPane.add(paneInfo); 
    centerPane.add(panePriority);
    centerPane.add(paneTitle); 
    
    
    
    JPanel panelList = new JPanel();
    panelList.setLayout(new FlowLayout());
    panelList.setPreferredSize(new Dimension(790, 250));
    panelList.setBackground(new Color(119,210,254));
    JLabel description=new JLabel("DESCRIPTION"); 
    description.setFont(new Font("Rockwell", Font.BOLD, 16));
    JLabel sugestion=new JLabel("SUGGESTIONS   ");
    sugestion.setFont(new Font("Rockwell", Font.BOLD, 16));
    panelList.add(description );
    panelList.add(listpane); 
    panelList.add(sugestion );
    panelList.add(areapane);
    panelList.add(saveButton);
    panelList.add(deleteButton);
   
    mainPane.add(panelTitle, BorderLayout.NORTH);
    mainPane.add(centerPane, BorderLayout.WEST); 
    mainPane.add(panelList, BorderLayout.SOUTH);

    content.add(mainPane);
    //return mainPane;
}
    
    public String[] getArray(LinkedList questionslist)
    {
    String array[]=new String[questionslist.size()];
    for(int i=0;i<questionslist.size();i++)
    {
    array[i]=new String();
    array[i]="*"+(String)questionslist.get(i);
    System.out.println("ss"+array[i]);
    }
    return array;
    }    
     
    
    
    public void addButton()
    {
        add.addActionListener(new ActionListener()
        {public void actionPerformed(ActionEvent ae)
        {
            if(ae.getSource()==add)
            {
                {
                    questionlist.add(area.getText());
                    questions=getArray(questionlist);
//                    list.setListData(questions);  
                    area.setText("");
                } 
            }
        } 
    });
 }
    
    
      public void   deleteButon()
    {
        deleteButton.addActionListener(new ActionListener()
        {public void actionPerformed(ActionEvent ae)
        {
            if(ae.getSource()==deleteButton&&questionlist.size()>0)
            {
                
            }
            else
                JOptionPane.showMessageDialog(null,"SELECT A QUESTION TO REMOVE PLS!");
        } 
    });
 } 
    
    
    public void saveButton()
    {
        saveButton.addActionListener(new ActionListener()
        {public void actionPerformed(ActionEvent ae)
        {
            if(ae.getSource()==saveButton)
            {
                {
                  makestring();
                }
     
            }
        } 
    });
 }
     
public void makestring()
{
    String tosend="";
    tosend=""+db.getString("package")+";"
            +umukozi.NOM_EMPLOYE+";"
            +umukozi.TITRE_EMPLOYE+";"
            +cbcategorie.getSelectedItem()+";"
            +cbpriority.getSelectedItem()+";"
            +title.getText()+";"+area.getText()+";"
            +txtdescription.getText()+";"
            +phoneF.getText();
    
    tosend=tosend.replaceAll(";;",";-;");
    System.out.println(tosend);
    CommCloud product_Cloud=new CommCloud("http://www.ishyiga"+db.getString("extension")+"/test_cloud/ishyiga_support.php?user=Domitille&soc"+db.dbName,"data="+tosend);
    System.out.println("ioioi "+product_Cloud.sendlot);
    
    if(product_Cloud.sendlot.contains("Question sent successfully"))
    {
        JOptionPane.showMessageDialog(null,db.l(db.lang,"V_QUESTION_SENT"),db.l(db.lang, "V_MERCI"),JOptionPane.INFORMATION_MESSAGE);
        this.dispose();
    }
    else
    {
        JOptionPane.showMessageDialog(null,db.l(db.lang,"V_QUESTION_FAIL"),db.l(db.lang, "V_IKIBAZO"),JOptionPane.ERROR_MESSAGE);
    }
    
}
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
public static void main(String[] args) {
//      new Askquestion();
    }
}
