package pos;

/* Product for Ishyiga copyright 2007 Kimenyi Aimable
*/


import java.awt.Color;
import java.util.Date;
import javax.swing.JOptionPane;
import rra.RRA_PRINT2;


public class Product implements  Comparable
{

	public int productCode,qty;
	public String productName,code,secondName;
        Lot courrant;
        String codeBar,observation,codeSoc;
        public double tva,currentPrice,prix_revient;
        private int quantity ;
        int choixLot=1;
        Db db;
        int qtyLive;
        String devise;
        
       //         courrant.qtyLive;
        String AVAILABILITY;
        Color availColor;
        String LASTTIME;
        String DOSAGE;
        String INTERACTION,famille; 
        public String classe;
        public double originalPrice;
        public double qtyDouble;
        
        
Product(int productCode,String productName,String code,int qty ,double currentPrice,double tva,String codeBar,String observation,String codeSoc,Db db)
{
    this.productCode=productCode;
    this.productName=productName;
//    if(productName.length()>55)
//    this.productName=productName.substring(0, 50);
    this.code=code;
    this.qty =qty;  this.qtyDouble =qty;
    this.currentPrice=currentPrice;
    this.db=db;
    this.tva=tva;
    this.codeBar=codeBar;
    this.observation=observation;
    this.codeSoc=codeSoc;
    this.devise="RWF";
    this.originalPrice=currentPrice;
    }
Product(int productCode,String productName,String code  ,double currentPrice,double tva,int qty )
{
 this.productCode=productCode;
 this.code=code;
 this.productName=productName;
 this.tva=tva; 
 this.currentPrice=currentPrice;
 this.originalPrice=currentPrice;
 this.qty=qty;  this.qtyDouble =qty;
}
Product( int productCode,String productName,double revient, double currentPrice,int qty,String Famille   )
{ 
 this.productCode=productCode;
 this.prix_revient=revient;
 this.productName=productName; 
 this.currentPrice=currentPrice;  
 this.qty=qty; 
 this.qtyDouble =qty;
 this.famille=Famille;
}
Product(int productCode,String productName,String code,double qty ,double currentPrice,double tva,String codeBar,String observation,String codeSoc,Db db,double pr)
{
    this.productCode=productCode;        
    this.prix_revient=pr;		
    this.productName=productName;
//                        if(productName.length()>55)
//                   this.productName=productName.substring(0, 50);
                this.code=code;
                this.qtyDouble =qty;
                this.currentPrice=currentPrice;
                this.db=db;
                this.tva=tva;
                this.codeBar=codeBar;
                this.observation=observation;
                this.codeSoc=codeSoc;
                this.devise=db.devise;
                this.originalPrice=currentPrice;
}
Product(int productCode,String productName,String code,int qty ,double currentPrice,double tva,String codeBar,String observation,String codeSoc,Db db,double pr)
{
    this.productCode=productCode;        
    this.prix_revient=pr;		
    this.productName=productName;
//                        if(productName.length()>55)
//                   this.productName=productName.substring(0, 50);
                this.code=code;
                this.qty =qty;  this.qtyDouble =qty;
                this.currentPrice=currentPrice;
                this.db=db;
                this.tva=tva;
                this.codeBar=codeBar;
                this.observation=observation;
                this.codeSoc=codeSoc;
                this.devise=db.devise;
                this.originalPrice=currentPrice;
//                if(this.currentPrice>0) {
//        System.out.println(this.currentPrice);
//    } 
}
public int qty ()
{ return qty; }
/* gestion de vente */

public String toPrint()
{
    String nom ;
    
  if(productName.length()>20) {
        nom=productName.substring(0,20)+".";
    }
  else {
        nom=productName;
    }
    
    int i = nom.length();
          
  while(i<=20)
  {
  nom=nom+" ";
  i++;
  }

return nom+" * "+qty+" = "+currentPrice*qty+" "+devise;
}
boolean contains(String find)
{
return  (productName.toUpperCase().contains(find)||
        code.toUpperCase().contains(find)||
        codeBar.toUpperCase().contains(find)||
        (secondName!= null && secondName.contains(find))
 );
}

// Qte Live
public int  getQuantite()
{
    int g =0;
    //System.out.println(codeSoc);
  //   JOptionPane.showMessageDialog(null,observation,db.observa,JOptionPane.PLAIN_MESSAGE);
//  if(!observation.equals("NULL")&& (db.observa.contains("_RAMA")||db.observa.contains("_MMI")))
    if(!observation.equals("NULL")&& (Main.observe_Tarif.contains("YES")))
   // if(!observation.equals("NULL"))
    {
        g=JOptionPane.showConfirmDialog(null,db.observa+" dit : "+this.observation,"Observation de "+this.productName,JOptionPane.INFORMATION_MESSAGE);
    // System.out.println(g);
    }
    if(g!=0) {
        g=0;
    }
    else
    {   
//         String depotSql="";
//                                if(umukozi.BP_EMPLOYE!=null && umukozi.BP_EMPLOYE.contains("#"))
//                                        { depotSql=" AND ID_LOTS  like '%" +umukozi.BP_EMPLOYE + "%'" ; } 
        
        g=db.getQuantitePerime2(productCode,""); 
//        g=db.getQuantite(productCode);        
    }
    
    return g;
}

/* gestion du stock de ce produit voir la qty */
public boolean check(int i)
{
    
    qtyLive =getQuantite();
	boolean done=false;
	 if( qtyLive >= i)
	 		{
	 			done=true;
	 		}
         else {
        db.insertMissed(productName,i,currentPrice,qtyLive );
    }
        
return done;
}
public double salePrice()
{
    System.out.println((currentPrice*qty)+":curr:"+currentPrice);
   	return currentPrice*qty;
}
public double salePrice2()
{
    System.out.println((currentPrice*qtyDouble)+":curr:"+currentPrice);
   	return currentPrice*qtyDouble;
}
public double originalPrice()
{
   	return originalPrice*qty;
}
public int productCode()
{
	return productCode;
}

    @Override
public String toString()
{
   
return productName+"****"+RRA_PRINT2.setVirgule(currentPrice,1, ",")+" "+devise;
}
public String mobiletxt()
{  
//    (String CODE_PRODUCT, String NAME_PRODUCT, double PRIX, 
//    double PRIX_REVIENT, double TVA, int qty, int MAXIMUM) 
//BAR2009G11;2009 GRAVES France. 750ml  ;29000.0;500.0;0.0;100;100;BA
//	  $CODE_PRODUCT.';'.$NAME_PRODUCT.';'.$PRIX.';'.$TVA.';'.$FAMILLE.';'.
  //  CUIEMINCÉ96;Emincé de Porc au Soya.;6400.0;0.0;CUISINE;null;210.0;250
return code+";"+productName+ ";"+currentPrice+";"+tva+";"+famille+";"+observation+";"+prix_revient+";"+qty ;
}    


public String toSell()
{
  String g ="****"+ codeSoc;
return productCode+"****"+qty+"****"+RRA_PRINT2.setVirgule(currentPrice*qty,1, ",")+" "+devise+"****"+productName+g;
}
public String toStock()
{
   // getQuantite();
return productCode+"***"+productName;//+"****"+courrant.id_LotS+"****"+courrant.qtyLive;
}
public String toSee()
{
    return productName+"****"+RRA_PRINT2.setVirgule(currentPrice,1, ",")+" "+devise;
}

public Product clone(int q)
{
    
Product p= new Product(productCode,productName, code, qty,currentPrice,tva,codeBar,observation,codeSoc,this.db,prix_revient);
p.qty=q;
p.AVAILABILITY=AVAILABILITY;
p.INTERACTION=INTERACTION;
p.DOSAGE=DOSAGE;
p.originalPrice=originalPrice;
p.INTERACTION=INTERACTION;
return p;

}
 
public Product cloneRet(int q)
{
Product p= new Product(productCode,productName, code, qty,currentPrice,tva,codeBar,observation,codeSoc,this.db);
p.qty=-q;

return p;

}

void setAvailability(Date last_stored_bup)
{
  Date today=new Date();      
  
  int diff=(int)((today.getTime()-last_stored_bup.getTime())/1000/60/60/24);
   LASTTIME=""+last_stored_bup.toLocaleString();
 // System.out.println(this+" "+diff +" "+today+" "+" "+last_stored_bup);
  if(diff<8 )
  {
  AVAILABILITY="GREEN";
  availColor=Color.GREEN;
   }
  else if( diff< 30)
  {
  AVAILABILITY="YELLOW";
  availColor=Color.YELLOW;
 
  }
   else if( diff< 90)
  {
  AVAILABILITY="ORANGE";
  availColor=Color.ORANGE;
   }
   else  
  {
  AVAILABILITY="RED";
  availColor=Color.red;
  } 
}

    @Override
    public int compareTo( Object o )
    {
        if( o instanceof Product ){
           Product r = (Product)o;


           return this.productName.compareTo( r.productName );
        }
        return -1;
    }

}