
package pos;
  
   import javax.swing.JFrame;
    import javax.swing.JPanel;
    import javax.swing.JOptionPane;
    import javax.swing.JTextField;
    import javax.swing.JLabel;
   import javax.swing.JButton;
   import java.awt.Color;
    import java.awt.Font;
    import java.awt.Dimension;
    import java.awt.event.ActionEvent;
    import java.awt.event.ActionListener;
   import java.awt.GridLayout;
import java.awt.Container;
import java.awt.event.WindowEvent;

    public class InsuranceInterface extends JFrame {
    Color color; Font police;
   JPanel mainInsurance = new JPanel();
    //Declares textfields for the panelprblm1 panel
    JTextField  NOM_ASSURANCEF,VALEUR_ASSURANCEF,FAMILLE_ASSURANCEF,VALEUR2_ASSURANCEF;

    JLabel  NOM_ASSURANCEL,VALEUR_ASSURANCEL,FAMILLE_ASSURANCEL,VALEUR2_ASSURANCEL;
    
    JButton saveInsurance;

    Container content;
   Db db;
   


public InsuranceInterface(String server,String dbName){

 this.db=new Db (server,dbName);
      db.read();
content = getContentPane();

    //Set the title to the InsuranceInterface frame
    this.setTitle("INSURANCE INTERFACE");
    
    this.enableEvents(WindowEvent.WINDOW_CLOSING);//kora kuburyo iri dirisha  nirifunga ridafunga andi madirishya bifitanye isano
    //Make the Comptabilitée frame not resizable
    this.setResizable(true);
    setVisible(true);
    //Define the size of the Comptabilité  ; here, 500 pixels of width and 650 pixels of height
    this.setSize(500,600);//600=largeur;800=longeur

     makeFrameInsurance();
     saveInsurance();
//     ecrireQuestion();
    }

   
   
    //Methode to make the Question frame by creating the component(JTextField,JLable,JButton,JList,JScrollPane,JFormattedTextField,MaskFormatter)and helper(color,police) objects for our containers
    public JPanel makeFrameInsurance() {

  
    NOM_ASSURANCEF = new JTextField("");
    police = new Font("Consolas", Font.BOLD, 16);
    NOM_ASSURANCEF.setForeground(Color.BLACK);
  color= new Color(175,215,255);//(r,g,b)
  VALEUR_ASSURANCEF = new JTextField("");
  FAMILLE_ASSURANCEF = new JTextField("CLIENT");
    FAMILLE_ASSURANCEF.setBackground(color);
  VALEUR2_ASSURANCEF = new JTextField(" RWANDA");
  
  
  NOM_ASSURANCEL = new JLabel("NOM_ASSURANCE");
    police = new Font("Comic Sans MS", Font.BOLD, 14);
     NOM_ASSURANCEL.setFont(police);
  VALEUR_ASSURANCEL = new JLabel(" VALEUR_ASSURANCE");
    police = new Font("Comic Sans MS", Font.BOLD, 14);
   VALEUR_ASSURANCEL = new JLabel("VALEUR_ASSURANCE");
    VALEUR_ASSURANCEL.setBackground(Color.PINK);

    FAMILLE_ASSURANCEL = new JLabel("FAMILLE_ASSURANCE");
    VALEUR2_ASSURANCEL = new JLabel("VALEUR2_ASSURANCE");
   
    
   saveInsurance = new JButton("save");
    police = new Font("Tahoma", Font.BOLD, 14);
    
  saveInsurance.setBackground(Color.GREEN);
  
    // Creates the paneljrnal1 panel to group textfileds,list and labels for the journal main panel
    JPanel window = new JPanel();
    
    window.setLayout(new GridLayout(5,2,5,20));//
    window.setPreferredSize(new Dimension(450,350)); //400=largeur350=longeur

    window.add(NOM_ASSURANCEL);
    window.add(NOM_ASSURANCEF);
    window.add( FAMILLE_ASSURANCEL);
    window.add( FAMILLE_ASSURANCEF);
   window.add(VALEUR_ASSURANCEL);
    window.add(VALEUR_ASSURANCEF);
    window.add(VALEUR2_ASSURANCEL);
   window.add( VALEUR2_ASSURANCEF);
   

    JPanel face = new JPanel();
   face.setLayout(new GridLayout(1,4));
   face.setPreferredSize(new Dimension(150,70));
//Add the saveCompte,viewCompte,deleteCompte,updateCompte to paneljrnal2
    face.add(saveInsurance);
    mainInsurance.add(window);
    mainInsurance.add(face);
    content.add(mainInsurance);
    return mainInsurance;
    }

 //ubaka fonction kuburyo n'ujukanada button ya ENVOYER ijye ihamagara fonction yo muri Db yitwa inserQuestion yinjizamo ibibazo
     public void saveInsurance()
    {
   saveInsurance.addActionListener(new ActionListener() {
    public void actionPerformed(ActionEvent ae)
    {
    if(ae.getSource()==saveInsurance )
    {
      
  //psInsert = conn.prepareStatement("insert into APP.VARIABLES(NOM_VARIABLE,VALUE_VARIABLE,FAMILLE_VARIABLE,VALUE2_VARIABLE)values(?,?,?,?)")
  
        //db.insertQuestion(pr);
     Insurance soras=new Insurance(NOM_ASSURANCEF.getText(),VALEUR_ASSURANCEF.getText(),FAMILLE_ASSURANCEF.getText(),VALEUR2_ASSURANCEF.getText()); //injizamo ikibazo       
 //System.out.println(NOM_ASSURANCEF.getText()+FAMILLE_ASSURANCEF.getText()+VALEUR_ASSURANCEF.getText()+VALEUR2_ASSURANCEF.getText());
     
 db.insertInsurance( soras);
  
 NOM_ASSURANCEF.setText("");
FAMILLE_ASSURANCEF.setText("");
VALEUR_ASSURANCEF.setText("");
VALEUR2_ASSURANCEL.setText("");
JOptionPane.showMessageDialog(null,"DATA ENTERED !");//menyesha ko byinjiyemo
}
}
}
);
} 
    }


