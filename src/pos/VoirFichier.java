package pos;
/*
 VoirFichier for Ishyiga copyright 2007 Kimenyi Aimable :pour imprime les fichiers
 */

import java.awt.*;
import java.awt.event.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import java.util.*;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import java.io.*;
import java.sql.*;
import java.util.Date;
import java.util.LinkedList;
import rra.*;

public class VoirFichier extends JFrame implements ActionListener {

    String nom;                             // le nom du Caissier
    LinkedList<Lot> lotList;
    int LENTH = 600;
    int WIDT = 600;
    JScrollPane scrollResult;   			// si il ya trop de resultat
    JList outList = new JList();
    JScrollPane jSrollPane1;
    JTable jTable, jTable2;
    JButton rapportButton, product, invoice, listP, clientRama, lot, creditButton, DownLoadButton, UpLoadButton, UpLoadMSButton, Update, sqlButton, compta, perim;
    String totaux = "";
    private JScrollPane jScrollPane1;
//   ## DEFINE VARIABLES SECTION ##
// define the driver to use ResultSet
    String driver = "org.apache.derby.jdbc.NetWorkDriver";
// thekim database name
    String dbName;
    String date, mois;
// define the Derby connection URL to use 192.168.0.113
//String connectionURL = "jdbc:derby://192.168.0.113:1527/" + dbName + ";create=true";
    String connectionURL;// = "jdbc:derby://localhost:1527/" + dbName + ";create=true";
    ResultSet outResult;
    Connection conn = null;
    Statement s;
    LinkedList<Product> allProduct;
    LinkedList allEmploye;
    LinkedList<Lot> lotL = new LinkedList();
    LinkedList<Lot> listlot = new LinkedList();
    LinkedList<Compta> compteL = new LinkedList();
    LinkedList<Client> client = new LinkedList();
    LinkedList<Credit> credit = new LinkedList();
    String creditDoc;
    LinkedList<Invoice> facture = new LinkedList();
    PreparedStatement psInsert;
    PreparedStatement psInsert1;
    LinkedList<List> list = new LinkedList();
    Db db;
    private JList societeJList = new JList();
    double soldeClient = 0;
    double CashClient = 0;
    double tvaYose = 0;
    int totalTax = 0;
    int de, fi;
    Font police;
    String[] control = null;
    String datez = "";
    String server;
    LinkedList<Assurance> ass;
    Employe umukozi;
    double YoseNtatva = 0;
    double Yose = 0;
    double yosePayed = 0;
    double yoseSolde = 0;
    String lang = "";
    String ch = "";
    String choixRpt = "";

    String oldReport = "";
    String IJISHOrEPORT = "CLASSIC";
    LinkedList<Byose> resByose = null;
    String tosend = "";
    Taxes tax;
// Constructeur avec la date et les options de voir listP ou invoices

    VoirFichier(Employe umukozi, String server, String dataBase, Taxes tax) {
        this.dbName = dataBase;
        this.server = server;
        connectionURL = "jdbc:derby://" + server + ":1527/" + dataBase + ";create=true";

        db = new Db(server, dataBase);

        this.nom = umukozi.NOM_EMPLOYE;
        this.umukozi = umukozi;
        this.lang = umukozi.LANGUE;
        this.db.lang = umukozi.LANGUE;
        this.product = new JButton("");
        this.rapportButton = new JButton("");
        this.clientRama = new JButton("");
        this.invoice = new JButton("");
        this.lot = new JButton("");
        this.listP = new JButton("");
        this.tax = tax;

        Date d = new Date();

        String day = "";
        String mm = "";
        String year = "";

        if (d.getDate() < 10) {
            day = "0" + d.getDate();
        } else {
            day = "" + d.getDate();
        }
        if ((d.getMonth() + 1) < 10) {
            mm = "0" + (d.getMonth() + 1);
        } else {
            mm = "" + (d.getMonth() + 1);
        }
        if ((d.getYear() - 100) < 10) {
            year = "0" + (d.getYear() - 100);
        } else {
            year = "" + (d.getYear() - 100);
        }

        datez = day + mm + year;
        draw();
        ass = assurance();
        Assurance[] assArray = new Assurance[ass.size()];
        for (int i = 0; i < ass.size(); i++) {
            assArray[i] = ass.get(i);
        }

        societeJList.setListData(assArray);
        societeJList.setSelectedIndex(0);
    }

    LinkedList<Assurance> assurance() {

        ass = new LinkedList();

        try {
            outResult = s.executeQuery("select NOM_VARIABLE,value_variable,value2_variable from APP.VARIABLES WHERE FAMILLE_VARIABLE='CLIENT' order by NOM_VARIABLE");

            while (outResult.next()) {
                ass.add(new Assurance(outResult.getString(1), outResult.getString(2), outResult.getString(3), "", ""));
            }

            control = new String[ass.size()];

            for (int i = 0; i < ass.size(); i++) {
                control[i] = ass.get(i).sigle;
            }
            //  Close the resultSet
            outResult.close();

        } catch (Throwable e) {

            db.insertError(e + "", " V ass");
        }
        return ass;
    }

    public void draw() {
        setTitle(nom + "'s Voir Fichier Session");

        //System.out.println("ABDS01".hashCode());
        this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);

        db.read();

        read();
        //on fixe  les composant del'interface
        Container content = getContentPane();

        eastPane(content);
        compta();
        product();
        //  rapportButton();
        clientRama();
        invoice();
        lot();
        credit();
        downLoad();
        upLoad();
        listP();
        upDate();
        sql();
        ms();
        perime();
        setSize(WIDT, LENTH);

        enableEvents(WindowEvent.WINDOW_CLOSING);
        Image im = Toolkit.getDefaultToolkit().getImage("logo.png");
        this.setIconImage(im);
        setVisible(true);
        raportt();
        // raport();
    }

    public void read() {
        try {
            // Create (if needed) and connect to the database
            conn = DriverManager.getConnection(connectionURL);
            System.out.println("Connected to database " + dbName);

            //   ## INITIAL SQL SECTION ##
            //   Create a statement to issue simple commands.
            s = conn.createStatement();

        } catch (Throwable e) {

            db.insertError(e + "", " V read");
            System.out.println("Throwable :" + e);
        }
    }

// Bouton pour la demande % date
    public void product() {
        product.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent ae) {
                        if (ae.getSource() == product) {
                            String db1 = "";
                            String db = "" + ((Assurance) societeJList.getSelectedValue()).sigle;
                            if (db.equals("CASHNORM")) {
                                db1 = "";
                            } else if (db.equals("RAMA") || db.equals("MMI") || db.equals("CORAR") || db.equals("SORAS") || db.equals("revient")) {
                                db1 = "_" + db;
                            } else {
                                db1 = "_SOCIETE";
                            }
                            //  System.out.println(db1);
                            dowork(db1);
                            new showJTable(initComponents(db1), db, server, dbName);
                        }
                    }
                });
    }

    public LinkedList dowork(String s1) {
        try {
            outResult = s.executeQuery("select id_product,name_product,code,prix" + s1 + ", tva,code_bar,observation,PRIX_REVIENT,COEF,FAMILLE from PRODUCT order by name_product");
            // JOptionPane.showMessageDialog(null,s," ERROR ",JOptionPane.PLAIN_MESSAGE); 
            allProduct = new LinkedList();
            //  Loop through the ResultSet and print the data
            while (outResult.next()) {
                int productCode = outResult.getInt(1);
                String productName = outResult.getString(2);
                String code = outResult.getString(3);
                double currentPrice = outResult.getDouble(4);
                double tva = outResult.getDouble(5);
                String codeBar = outResult.getString(6);
                String observation = outResult.getString(7);

                double rev = outResult.getDouble(8);
                double coef = outResult.getDouble(9);
                String famille = outResult.getString(10);

                Product p = null;

                if (s1.equals("")) {
                    p = new Product(productCode, productName, code, 1, currentPrice, coef, "" + rev, famille, "", db);
                } else {
                    p = new Product(productCode, productName, code, 1, currentPrice, tva, codeBar, observation, "", db);
                }

                if (currentPrice > 0) {
                    allProduct.add(p);
                }
            }
            //  Close the resultSet
            outResult.close();

            //  Beginning of the primary catch block: uses errorPrint method
        } catch (Throwable e) {
            db.insertError(e + "", " V dowork");
            System.out.println(" . . . exception other thrown:" + e);
        }

        return allProduct;
    }

    public void dowork1(String s1) {
        try {
            outResult = s.executeQuery(s1);
            // JOptionPane.showMessageDialog(null,s," ERROR ",JOptionPane.PLAIN_MESSAGE);

            allProduct = new LinkedList();
            //  Loop through the ResultSet and print the data
            while (outResult.next()) {

                int productCode = outResult.getInt(1);
                String productName = outResult.getString(2);
                String code = outResult.getString(3);
                double currentPrice = outResult.getDouble(4);
                double tva = outResult.getDouble(5);
                String codeBar = outResult.getString(6);
                String observation = outResult.getString(7);
                //  String codeSoc  =  outResult.getString(8);
                Product p = new Product(productCode, productName, code, 1, currentPrice, tva, codeBar, observation, "", db);
                if (currentPrice > 0) {
                    allProduct.add(p);
                }
            }
            //  Close the resultSet
            outResult.close();

            //  Beginning of the primary catch block: uses errorPrint method
        } catch (Throwable e) {
            db.insertError(e + "", " V dowork1");
            System.out.println(" . . . exception other thrown:" + e);
        }

    }

    public LinkedList< Client> getClient() {

        String db1 = "";
        String db2 = "" + ((Assurance) societeJList.getSelectedValue()).sigle;
        try {
            client = new LinkedList();
            if (db2.equals("RAMA")) {
                db1 = "select * from CLIENT_RAMA  order by nom_client,prenom_client";
            } else if (db2.equals("SOCIETE")) {
                db1 = "select * from CLIENT order by ASSURANCE, nom_client,prenom_client  ";
            } else if (db2.equals("CASHREMISE")) {
                db1 = "select * from CLIENT_UNIPHARMA  order by nom_client,prenom_client";
            } else {
                db1 = "select * from CLIENT where assurance='" + db2 + "'order by NOM_CLIENT,prenom_client";
            }

            if (db2.equals("MMI")) {
                JOptionPane.showMessageDialog(null, l(lang,"V_UNAUTHORISE"), l(lang,"V_ACCESSREFU"), JOptionPane.PLAIN_MESSAGE);
            } else {
                outResult = s.executeQuery(db1);
                while (outResult.next()) {

                    String NUM_AFFILIATIONa = outResult.getString(1);
                    String NOM_CLIENTa = outResult.getString(2);
                    String PRENOM_CLIENTa = outResult.getString(3);
                    int PERCENTAGEa = outResult.getInt(4);
                    int DATE_EXPa = outResult.getInt(5);
                    String ASSURANCEa = outResult.getString(6);
                    String EMPLOYEURa = outResult.getString(7);
                    String SECTEURa = outResult.getString(8);
                    String CODE = outResult.getString(10);
                    Client c = new Client(NUM_AFFILIATIONa, NOM_CLIENTa, PRENOM_CLIENTa, PERCENTAGEa, DATE_EXPa, ASSURANCEa, EMPLOYEURa, SECTEURa, "", CODE);

                    client.add(c);

                }
                //  Close the resultSet
                outResult.close();
            }
        } catch (Throwable e) {

            db.insertError(e + "", " V getClient");

            /*       Catch all exceptions and pass them to
             **       the exception reporting method             */
            System.out.println(" . .client . exception thrown:" + e);

        }

        return client;

    }

    public void getLot(String s1) {
        try {
            lotL = new LinkedList();
            outResult = s.executeQuery("select app.lot.id_product,name_product,id_LotS,date_exp,qty_start,qty_live,bon_livraison,code,id_lot from APP.LOT ,APP.PRODUCT where app.lot.id_product = app.product.id_product " + s1);
            System.out.println("select app.lot.id_product,name_product,id_LotS,date_exp,qty_start,qty_live,bon_livraison,code,id_lot from APP.LOT ,APP.PRODUCT where app.lot.id_product = app.product.id_product " + s1);

            while (outResult.next()) {
                int productCode = outResult.getInt(1);
                String name = outResult.getString(2);
                String id_LotS = outResult.getString(3);
                String dateExp = outResult.getString(4);
                int qtyStart = outResult.getInt(5);
                int qtyLive = outResult.getInt(6);
                String bon_livraison = outResult.getString(7);
                String code = outResult.getString(8);
                int dateDoc = outResult.getInt(9);

                lotL.add(new Lot(productCode, name, code, id_LotS, dateExp, dateDoc, qtyStart, qtyLive, bon_livraison));

            }

            //  Close the resultSet
            outResult.close();
        } catch (Throwable e) {
            System.out.println(" . .lot . exception thrown:" + e);
            db.insertError(e + "", " V lot");
        }
    }

    Product makeProduct(String ligne, int i) {

        //nom;code;prix;tva;obs
        StringTokenizer st1 = new StringTokenizer(ligne);
        //recuperer le nom

        String productName = st1.nextToken(";");
        // JOptionPane.showMessageDialog(null," In Put Error ",productName,JOptionPane.PLAIN_MESSAGE);

        String code = st1.nextToken(";");
  //  JOptionPane.showMessageDialog(null," In Put Error ",code,JOptionPane.PLAIN_MESSAGE);

        //recupere code
        String obs = st1.nextToken(";");
        // System.out.println(productName) ;
//JOptionPane.showMessageDialog(null," In Put Error ",obs,JOptionPane.PLAIN_MESSAGE);

        String d = st1.nextToken(";");

//JOptionPane.showMessageDialog(null," In Put Error ",d,JOptionPane.PLAIN_MESSAGE);
        if (d.contains(",")) {
            d = d.replace(',', '.');

        }
        double tva = 0;
        double price = 0;
        try {

            Double in_int = new Double(d);
            price = in_int.doubleValue();

            String d2 = st1.nextToken(";");
            //  JOptionPane.showMessageDialog(null," In Put Error ",d2,JOptionPane.PLAIN_MESSAGE);

            if (d2.contains(",")) {

                d2 = d2.replace(',', '.');
            }
            Double tv = new Double(d2);
            //int prix = in_int.intValue();
            // Double in_int=new Double(d);
            tva = tv.doubleValue();
        } catch (NumberFormatException e) {
            //JOptionPane.showMessageDialog(null," In Put Error ",ligne+" "+e,JOptionPane.PLAIN_MESSAGE);
            db.insertError(e + "", " V makepro");
        }

        return new Product(i, productName, code, 1, price, tva, code, obs, "", db);
    }

    Product makeProductSoc(String ligne, int i) {
        //AACE01;Ac. Acetylsalicylique 100mg 1comp;PGE;4,180000;0,0000
        StringTokenizer st1 = new StringTokenizer(ligne);
        //recuperer le nom
        String code = st1.nextToken(";");

        st1.nextToken(";");
        String fam = st1.nextToken(";");
        String d = st1.nextToken(";");
        double price = 0;
        try {
            if (d.contains("/")) {
                StringTokenizer st2 = new StringTokenizer(d);
                d = st2.nextToken("/");
            }
            if (d.contains(",")) {
                d = d.replace(',', '.');

            }

            Double in_int = new Double(d);
            price = in_int.doubleValue();
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(null, " In Put Error ", ligne + " " + e, JOptionPane.PLAIN_MESSAGE);
            db.insertError(e + "", " V mekeprosoc");
        }

        return new Product(i, "", code, 1, price, 0, fam, "", "", db);

    }

    Lot makeLot(String ligne) {

        // 13;KIPHARMAF;MAV0038;;101010;141008;30,0000;AGR2239
        Lot l = null;
        String ln = ligne.replaceFirst(";;", ";NULL;");

        // 13;KIPHARMAF;MAV0038;NULL;NULL;141008;30,0000;AGR2239
        // 13;KIPHARMAF;MAV0038;LOT1025;101010;141008;30,0000;AGR2239
        String ln1 = ln.replaceFirst(";;", ";NULL;");

        System.out.print(ln1);
        StringTokenizer st1 = new StringTokenizer(ln1);

        try {
            st1.nextToken(";");
            st1.nextToken(";");
            String code = st1.nextToken(";");
            String id_LotS = st1.nextToken(";");
            String dateEx = st1.nextToken(";");
            String datedoc = st1.nextToken(";");

            String quantite = st1.nextToken(";").replace(',', '.');
            String bl = st1.nextToken(";");

            Double qd = new Double(quantite);
            int qty = qd.intValue();

            String createString1 = "" + dateEx.substring(4) + "" + dateEx.substring(2, 4) + "" + dateEx.substring(0, 2);

            String createString = "" + datedoc.substring(4) + "" + datedoc.substring(2, 4) + "" + datedoc.substring(0, 2);
            Integer kkk = new Integer(createString);
            int dateDoc = kkk.intValue();

            if (!bl.equals("") && !code.equals("") && !quantite.equals("")) {
                if (!(!bl.equals("NULL") && dateEx.equals(""))) {
                    l = new Lot(0, "", code, id_LotS, createString1, dateDoc, qty, qty, bl);
                }
            }

        } catch (NumberFormatException e) {
            l = null;
            db.insertError(e + "", " V mlot");
            System.out.println(e);
        } catch (NoSuchElementException e) {
            l = null;
            db.insertError(e + "", " V mlot");
            System.out.println(e);
        }

        return l;
    }

    LinkedList<String> getFichier(String fichier) throws FileNotFoundException, IOException {
        LinkedList<String> give = new LinkedList();

        File productFile = new File(fichier);
        String ligne = null;
        BufferedReader entree = new BufferedReader(new FileReader(productFile));
        try {
            ligne = entree.readLine();
        } catch (IOException e) {
            System.out.println(e);
        }
        int i = 0;
        while (ligne != null) {
            // System.out.println(i+ligne);
            give.add(ligne);
            i++;
            try {
                ligne = entree.readLine();
            } catch (IOException e) {
                System.out.println(e);
            }
        }
        entree.close();
        return give;
    }

    public String getInvoice() {
        try {
            String ch = "" + ((Assurance) societeJList.getSelectedValue()).sigle;
//             String sql="  AND COMPTABILISE!='ANNULE' ";
            String sql = "  ";
            String Debut = "";
            String FIN = "";
            int id_invoice = 0;
            int g = 0;
            Yose = 0;
            tvaYose = 0;
            facture = new LinkedList();

            String firstChoix = (String) JOptionPane.showInputDialog(null, l(lang, "V_CHOICE"), l(lang,"V_MAKECHOICE"),
                    JOptionPane.QUESTION_MESSAGE, null, new String[]{
                        l(lang,"V_NET_INVOICE"),
                        l(lang,"V_NORMAL_INVOICE"),
                        l(lang,"V_FACTURE_ANNULE"), 
                        l(lang,"V_FACTURE_INCOMPLET"), 
                        l(lang,"V_BOSE")}, nom);

            
             if (firstChoix.equals(l(lang,"V_NORMAL_INVOICE"))){ 
//                    && !firstChoix.contains("ANNULE") && !firstChoix.contains("INCOMPLET")
//                     || !firstChoix.contains("CANCEL") && !firstChoix.contains("INCOMPLETE")) {
                String nAff = (String) JOptionPane.showInputDialog(null, l(lang, "V_CHOICE"), l(lang,"V_MAKECHOICE"),
                        JOptionPane.QUESTION_MESSAGE, null, new String[]{"SELECTED", l(lang,"V_BOSE")}, l(lang,"V_BOSE"));

                g = 0;
                id_invoice = 0;

                if (firstChoix.contains("NET")) {
                    sql = " AND COMPTABILISE!='ANNULE'";
                }

                if (nAff.equals("SELECTED")) {
                    String CHOIX = (String) JOptionPane.showInputDialog(null, l(lang, "V_CHOICE"), "Comptabiliser les factures",
                            JOptionPane.QUESTION_MESSAGE, null, new String[]{l(lang,"V_JOURNALIER"), l(lang,"V_INTERVALLE_DATE")}, l(lang,"V_INTERVALLE_DATE"));

                    if (CHOIX.equals(l(lang,"V_JOURNALIER"))) { //JOURNALIER
                        Debut = JOptionPane.showInputDialog("ENTREZ LA DATE FORMAT(010513)", db.datez);
                        sql += " AND INVOICE.NUM_CLIENT ='" + ch + "'  ";
//                        sql = " AND INVOICE.NUM_CLIENT ='" + ch + "'  AND COMPTABILISE!='ANNULE' ";

                        outResult = s.executeQuery("select * from APP.INVOICE where  invoice.DATE ='" + Debut + "' " + sql + " ORDER BY HEURE ");
                    } else if (CHOIX.equals(l(lang,"V_INTERVALLE_DATE"))) {//INTERVALLE DE DATE
                        Debut = JOptionPane.showInputDialog(l(lang, "V_DATEDEBUT"), db.startTime());
                        FIN = JOptionPane.showInputDialog(l(lang, "V_DATEFIN"), db.EndTime());
                        sql += " AND INVOICE.NUM_CLIENT ='" + ch + "' ";
//                        sql = " AND INVOICE.NUM_CLIENT ='" + ch + "'  AND COMPTABILISE!='ANNULE' ";

                        outResult = s.executeQuery("select * from APP.INVOICE where  invoice.HEURE >'" + Debut + "' AND invoice.HEURE <'" + FIN + " ' " + sql + " ORDER BY HEURE ");
                    }
                } else if (nAff.equals(l(lang,"V_BOSE"))) {//ALL
                    String CHOIX = (String) JOptionPane.showInputDialog(null, l(lang, "V_CHOICE"), "Comptabiliser les factures",
                            JOptionPane.QUESTION_MESSAGE, null, new String[]{l(lang,"V_JOURNALIER"), l(lang,"V_INTERVALLE_DATE")}, l(lang,"V_INTERVALLE_DATE"));

                    if (CHOIX.equals(l(lang,"V_JOURNALIER"))) { //JOURNALIER
                        Debut = JOptionPane.showInputDialog("ENTREZ LA DATE FORMAT(010513)", db.datez);

                        outResult = s.executeQuery("select * from APP.INVOICE where  invoice.DATE ='" + Debut + "' AND NUM_CLIENT IS NOT NULL " + sql + " ORDER BY HEURE ");
                    } else if (CHOIX.equals(l(lang,"V_INTERVALLE_DATE"))) { //INTERVALLE DE DATE
                        Debut = JOptionPane.showInputDialog(l(lang, "V_DATEDEBUT"), db.startTime());
                        FIN = JOptionPane.showInputDialog(l(lang, "V_DATEFIN"), db.EndTime());
                        outResult = s.executeQuery("select * from APP.INVOICE where  invoice.HEURE >'" + Debut + "' AND invoice.HEURE <'" + FIN + " '  AND NUM_CLIENT IS NOT NULL " + sql + " ORDER BY HEURE ");
                    }
                }

                while (outResult.next()) {
                    id_invoice = outResult.getInt(1);
                    if (g == 0) {
                        de = id_invoice;
                        g++;
                    }
                    String clientw = outResult.getString(2);
                    String z = outResult.getString(3);
                    double total = outResult.getDouble(4);
                    String id_employe = outResult.getString(5);
                    String time = "" + outResult.getTimestamp(6);
                    double tva = outResult.getDouble(7);
                    double reduction = outResult.getDouble(9);

                    if (umukozi.PWD_EMPLOYE.contains("RAPPORT")) {
                        //YoseNtatva=YoseNtatva+((int)(outResult.getDouble(7)*outResult.getInt(6)));
                        tvaYose = tvaYose + tva;
                        Yose = Yose + total;
                    }

                    facture.add(new Invoice(id_invoice, id_employe, z, clientw, total, time, tva, reduction,
                            outResult.getDouble("TOTAL_A"), outResult.getDouble("TOTAL_B"), outResult.getDouble("TOTAL_B") - outResult.getDouble("TAXE_B"),
                            outResult.getDouble("TAXE_B")));
                }
                fi = id_invoice;
                System.out.println("tva:" + tvaYose);
                //  Close the resultSet
                outResult.close();
            } else if (firstChoix.equals(l(lang,"V_NET_INVOICE"))) {
                String nAff = (String) JOptionPane.showInputDialog(null, l(lang, "V_CHOICE"), l(lang,"V_MAKECHOICE"),
                        JOptionPane.QUESTION_MESSAGE, null, new String[]{"SELECTED", l(lang,"V_BOSE")}, l(lang,"V_BOSE"));

                g = 0;
                id_invoice = 0;
 

                if (nAff.equals("SELECTED")) {
                    String CHOIX = (String) JOptionPane.showInputDialog(null, l(lang, "V_CHOICE"), "Comptabiliser les factures",
                            JOptionPane.QUESTION_MESSAGE, null, new String[]{l(lang,"V_JOURNALIER"), l(lang,"V_INTERVALLE_DATE")}, l(lang,"V_INTERVALLE_DATE"));

                    if (CHOIX.equals(l(lang,"V_JOURNALIER"))) { //JOURNALIER
                        Debut = JOptionPane.showInputDialog("ENTREZ LA DATE FORMAT(010513)", db.datez);
                        sql += " AND INVOICE.NUM_CLIENT ='" + ch + "'  ";
//                        sql = " AND INVOICE.NUM_CLIENT ='" + ch + "'  AND COMPTABILISE!='ANNULE' ";

                        outResult = s.executeQuery("select * from APP.INVOICE where  invoice.DATE ='" + Debut + "' " + sql + " ORDER BY HEURE ");
                    } else if (CHOIX.equals(l(lang,"V_INTERVALLE_DATE"))) {//INTERVALLE DE DATE
                        Debut = JOptionPane.showInputDialog(l(lang, "V_DATEDEBUT"), db.startTime());
                        FIN = JOptionPane.showInputDialog(l(lang, "V_DATEFIN"), db.EndTime());
                        sql += " AND INVOICE.NUM_CLIENT ='" + ch + "' ";
//                        sql = " AND INVOICE.NUM_CLIENT ='" + ch + "'  AND COMPTABILISE!='ANNULE' ";

                        outResult = s.executeQuery("select * from APP.INVOICE where  invoice.HEURE >'" + Debut + "' AND invoice.HEURE <'" + FIN + " ' " + sql + " ORDER BY HEURE ");
                    }
                } else if (nAff.equals(l(lang,"V_BOSE"))) {//ALL
                    String CHOIX = (String) JOptionPane.showInputDialog(null, l(lang, "V_CHOICE"), "Comptabiliser les factures",
                            JOptionPane.QUESTION_MESSAGE, null, new String[]{l(lang,"V_JOURNALIER"), l(lang,"V_INTERVALLE_DATE")}, l(lang,"V_INTERVALLE_DATE"));

                    if (CHOIX.equals(l(lang,"V_JOURNALIER"))) { //JOURNALIER
                        Debut = JOptionPane.showInputDialog("ENTREZ LA DATE FORMAT(010513)", db.datez);

                        outResult = s.executeQuery("select * from APP.INVOICE where  invoice.DATE ='" + Debut + "' AND NUM_CLIENT IS NOT NULL " + sql + " ORDER BY HEURE ");
                    } else if (CHOIX.equals(l(lang,"V_INTERVALLE_DATE"))) { //INTERVALLE DE DATE
                        Debut = JOptionPane.showInputDialog(l(lang, "V_DATEDEBUT"), db.startTime());
                        FIN = JOptionPane.showInputDialog(l(lang, "V_DATEFIN"), db.EndTime());
                        outResult = s.executeQuery("select * from APP.INVOICE where  invoice.HEURE >'" + Debut + "' AND invoice.HEURE <'" + FIN + " '  AND NUM_CLIENT IS NOT NULL " + sql + " ORDER BY HEURE ");
                    }
                }

                while (outResult.next()) {
                    id_invoice = outResult.getInt(1);
                    if (g == 0) {
                        de = id_invoice;
                        g++;
                    }
                    String clientw = outResult.getString(2);
                    String z = outResult.getString(3);
                    double total = outResult.getDouble(4);
                    String id_employe = outResult.getString(5);
                    String time = "" + outResult.getTimestamp(6);
                    double tva = outResult.getDouble(7);
                    double reduction = outResult.getDouble(9);

                    if (umukozi.PWD_EMPLOYE.contains("RAPPORT")) {
                        //YoseNtatva=YoseNtatva+((int)(outResult.getDouble(7)*outResult.getInt(6)));
                        tvaYose = tvaYose + tva;
                        Yose = Yose + total;
                    }

                    facture.add(new Invoice(id_invoice, id_employe, z, clientw, total, time, tva, reduction,
                            outResult.getDouble("TOTAL_A"), outResult.getDouble("TOTAL_B"), outResult.getDouble("TOTAL_B") - outResult.getDouble("TAXE_B"),
                            outResult.getDouble("TAXE_B")));
                }
                fi = id_invoice;
                System.out.println("tva:" + tvaYose);
                //  Close the resultSet
                outResult.close();
            } else if (firstChoix.equals(l(lang,"V_FACTURE_ANNULE"))){ //FACTURE ANNULE
            
                String CHOIX = (String) JOptionPane.showInputDialog(null, l(lang, "V_CHOICE"), "Faite votre choix",
                        JOptionPane.QUESTION_MESSAGE, null, new String[]{"JOURNALIER", "INTERVALLE DE DATE"}, nom);

                if (CHOIX.equals("JOURNALIER")) {
                    Debut = JOptionPane.showInputDialog("ENTREZ LA DATE FORMAT(010513)", db.datez);
                    //sql = " AND APP.REFUND.NUM_CLIENT ='" + ch + "' ";
                    sql = " ";
                    outResult = s.executeQuery("select * from APP.REFUND where  APP.REFUND.DATE ='" + Debut + "' AND NUM_CLIENT IS NOT NULL " + sql + " ORDER BY HEURE ");
                } else if (CHOIX.equals("INTERVALLE DE DATE")) {
                    Debut = JOptionPane.showInputDialog(l(lang, "V_DATEDEBUT"), db.startTime());
                    FIN = JOptionPane.showInputDialog(l(lang, "V_DATEFIN"), db.EndTime());
                    //sql = " AND APP.REFUND.NUM_CLIENT ='" + ch + "' ";
                    sql = " ";
                    outResult = s.executeQuery("select * from APP.REFUND where  APP.REFUND.HEURE >'" + Debut + "' AND APP.REFUND.HEURE <'" + FIN + " '  AND NUM_CLIENT IS NOT NULL " + sql + " ORDER BY HEURE ");
                    System.out.println("select * from APP.REFUND where  APP.REFUND.HEURE >'" + Debut + "' AND APP.REFUND.HEURE <'" + FIN + " ' " + sql + " ORDER BY HEURE ");

                }

                while (outResult.next()) {
                    id_invoice = outResult.getInt(1);
                    if (g == 0) {
                        de = id_invoice;
                        g++;
                    }
                    String clientw = outResult.getString(5);
                    String z = outResult.getString("DATE");
                    double total = outResult.getDouble("TOTAL");
                    String id_employe = outResult.getString("EMPLOYE");
                    String time = "" + outResult.getTimestamp("HEURE");
                    double tva = outResult.getDouble("TVA");
                    int id_inv = outResult.getInt("ID_INVOICE");

                    if (umukozi.PWD_EMPLOYE.contains("RAPPORT")) {
                        //YoseNtatva=YoseNtatva+((int)(outResult.getDouble(7)*outResult.getInt(6)));
                        tvaYose = tvaYose + tva;
                        Yose = Yose + total;
                    }

                    facture.add(new Invoice(id_invoice, id_employe, z, clientw, total, time, tva, id_inv,
                            outResult.getDouble("TOTAL_A"), outResult.getDouble("TOTAL_B"), outResult.getDouble("TOTAL_B") - outResult.getDouble("TAXE_B"),
                            outResult.getDouble("TAXE_B")));
                }
                fi = id_invoice;
                System.out.println("tva:" + tvaYose);
                //  Close the resultSet
                outResult.close();
            } else if (firstChoix.equals(l(lang,"V_FACTURE_INCOMPLET"))) { //FACTURE INCOMPLET
                Debut = JOptionPane.showInputDialog(l(lang, "V_DATEDEBUT"), db.startTime());
                FIN = JOptionPane.showInputDialog(l(lang, "V_DATEFIN"), db.EndTime());
                outResult = s.executeQuery("select * from APP.INVOICE where  invoice.HEURE >'" + Debut + "' AND invoice.HEURE <'" + FIN + " '  AND NUM_CLIENT IS NULL " + sql + " ORDER BY HEURE ");

                while (outResult.next()) {
                    id_invoice = outResult.getInt(1);
                    if (g == 0) {
                        de = id_invoice;
                        g++;
                    }
                    String clientw = outResult.getString(2);
                    String z = outResult.getString(3);
                    double total = outResult.getDouble(4);
                    String id_employe = outResult.getString(5);
                    String time = "" + outResult.getTimestamp(6);
                    double tva = outResult.getDouble(7);
                    double reduction = outResult.getDouble(9);

                    if (umukozi.PWD_EMPLOYE.contains("RAPPORT")) {
                        //YoseNtatva=YoseNtatva+((int)(outResult.getDouble(7)*outResult.getInt(6)));
                        tvaYose = tvaYose + tva;
                        Yose = Yose + total;
                    }

                    facture.add(new Invoice(id_invoice, id_employe, z, clientw, total, time, tva, reduction,
                            outResult.getDouble("TOTAL_A"), outResult.getDouble("TOTAL_B"), outResult.getDouble("TOTAL_B") - outResult.getDouble("TAXE_B"),
                            outResult.getDouble("TAXE_B")));
                }
                fi = id_invoice;
                System.out.println("tva:" + tvaYose);
                //  Close the resultSet
                outResult.close();
            }
            if (firstChoix.equals(l(lang,"V_BOSE"))) { //TOUT
                sql = "";
                String nAff = (String) JOptionPane.showInputDialog(null, l(lang, "V_CHOICE"), "Choix des factures",
                        JOptionPane.QUESTION_MESSAGE, null, new String[]{"SELECTED", "ALL"}, nom);

                g = 0;
                id_invoice = 0;

                if (nAff.equals("SELECTED")) {
                    String CHOIX = (String) JOptionPane.showInputDialog(null, l(lang, "V_CHOICE"), "Comptabiliser les factures",
                            JOptionPane.QUESTION_MESSAGE, null, new String[]{"JOURNALIER", "INTERVALLE DE DATE"}, nom);

                    if (CHOIX.equals("JOURNALIER")) {
                        Debut = JOptionPane.showInputDialog("ENTREZ LA DATE FORMAT(010513)", db.datez);
                        sql = " AND INVOICE.NUM_CLIENT ='" + ch + "' ";

                        outResult = s.executeQuery("select * from APP.INVOICE where  invoice.DATE ='" + Debut + "' " + sql + " ORDER BY HEURE ");
                    } else if (CHOIX.equals("INTERVALLE DE DATE")) {
                        Debut = JOptionPane.showInputDialog(l(lang, "V_DATEDEBUT"), db.startTime());
                        FIN = JOptionPane.showInputDialog(l(lang, "V_DATEFIN"), db.EndTime());
                        sql = " AND INVOICE.NUM_CLIENT ='" + ch + "' ";
                        outResult = s.executeQuery("select * from APP.INVOICE where  invoice.HEURE >'" + Debut + "' AND invoice.HEURE <'" + FIN + " ' " + sql + " ORDER BY HEURE ");
                    }
                } else if (nAff.equals("ALL")) {
                    String CHOIX = (String) JOptionPane.showInputDialog(null, l(lang, "V_CHOICE"), "Comptabiliser les factures",
                            JOptionPane.QUESTION_MESSAGE, null, new String[]{"JOURNALIER", "INTERVALLE DE DATE"}, nom);

                    if (CHOIX.equals("JOURNALIER")) {
                        Debut = JOptionPane.showInputDialog("ENTREZ LA DATE FORMAT(010513)", db.datez);

                        outResult = s.executeQuery("select * from APP.INVOICE where  invoice.DATE ='" + Debut + "' AND NUM_CLIENT IS NOT NULL " + sql + " ORDER BY HEURE ");
                    } else if (CHOIX.equals("INTERVALLE DE DATE")) {
                        Debut = JOptionPane.showInputDialog(l(lang, "V_DATEDEBUT"), db.startTime());
                        FIN = JOptionPane.showInputDialog(l(lang, "V_DATEFIN"), db.EndTime());
                        outResult = s.executeQuery("select * from APP.INVOICE where  invoice.HEURE >'" + Debut + "' AND invoice.HEURE <'" + FIN + " '  AND NUM_CLIENT IS NOT NULL " + sql + " ORDER BY HEURE ");
                    }
                }

                while (outResult.next()) {
                    id_invoice = outResult.getInt(1);
                    if (g == 0) {
                        de = id_invoice;
                        g++;
                    }
                    String clientw = outResult.getString(2);
                    String z = outResult.getString(3);
                    double total = outResult.getDouble(4);
                    String id_employe = outResult.getString(5);
                    String time = "" + outResult.getTimestamp(6);
                    double tva = outResult.getDouble(7);
                    double reduction = outResult.getDouble(9);

                    if (umukozi.PWD_EMPLOYE.contains("RAPPORT")) {
                        //YoseNtatva=YoseNtatva+((int)(outResult.getDouble(7)*outResult.getInt(6)));
                        tvaYose = tvaYose + tva;
                        Yose = Yose + total;
                    }

                    facture.add(new Invoice(id_invoice, id_employe, z, clientw, total, time, tva, reduction,
                            outResult.getDouble("TOTAL_A"), outResult.getDouble("TOTAL_B"), outResult.getDouble("TOTAL_B") - outResult.getDouble("TAXE_B"),
                            outResult.getDouble("TAXE_B")));
                }
                fi = id_invoice;
                System.out.println("tva:" + tvaYose);
                //  Close the resultSet
                outResult.close();
            }

            return firstChoix;
        } catch (Throwable e) {
            /*       Catch all exceptions and pass them to
             **       the exception reporting method             */
            System.out.println(" . . inv . exception thrown:");
            db.insertError(e + "", " V getinvoice");
        }
        return "";

    }

    public void getInvoiceView(String d) {
        credit = new LinkedList();
        try {

            outResult = s.executeQuery("select * from APP.INVOICE where   APP.INVOICE.DATE LIKE '%" + d + "%' and  APP.INVOICE.NUM_CLIENT='CASHNORM'  AND COMPTABILISE!='ANNULE'   ORDER BY HEURE ");

            while (outResult.next()) {
                int id_invoice = outResult.getInt(1);
                String clientw = outResult.getString(2);
                String z = outResult.getString(3);
                int total = outResult.getInt(4);
                String id_employe = outResult.getString(5);
                String time = ("" + outResult.getTimestamp(6)).substring(11);
                int tva = outResult.getInt(7);
                creditDoc = "FU0900" + outResult.getInt(8);

                credit.add(new Credit(id_invoice, id_employe, time, 0, total, z, "", "", tva));

            }
            outResult.close();

            outResult = s.executeQuery("select * from  APP.compta WHERE DATE LIKE '%" + d + "%' and intitule LIKE '%Client %'");

            while (outResult.next()) {
                String factur = "FU0900" + outResult.getInt(1);
                String dat = outResult.getString(2);
                int compte = outResult.getInt(3);
                String cli = outResult.getString(4);
                String saisi = outResult.getString(5);
                int cash = outResult.getInt(6);
                int cre = outResult.getInt(7);
                ((Assurance) societeJList.getSelectedValue()).facture = factur;
                credit.add(new Credit(outResult.getInt(1), cli, factur, 0, cash, dat, "", "", 0));
            }

            //  Close the resultSet
            outResult.close();

        } catch (Throwable e) {
            /*       Catch all exceptions and pass them to
             **       the exception reporting method             */
            System.out.println(" . . inv . exception thrown:");
            db.insertError(e + "", " V getinvoice");
        }
    }

    public void getCredit(String d,String opt) {
        soldeClient = 0;
        CashClient = 0;
        credit = new LinkedList();
        lotList = new LinkedList();
        try {

            if (d.length() > 3 || d.length() <= 6) {
                String stock = "" + ((Assurance) societeJList.getSelectedValue()).sigle;
                String dbb = "";
                String nAff = (String) JOptionPane.showInputDialog(null, l(lang, "V_CHOICE"), "PRINT EXCEL",
                        JOptionPane.QUESTION_MESSAGE, null, new String[]{"SPECIAL", "NORMAL"}, nom);

                String Debut = JOptionPane.showInputDialog(l(lang, "V_DATEDEBUT"), db.startTime());
                String FIN = JOptionPane.showInputDialog(l(lang, "V_DATEFIN"), db.EndTime());

                if (stock.equals("RAMA")) {
                    dbb = "_RAMA";

                    outResult = s.executeQuery("select APP.CREDIT.ID_INVOICE,NUMERO_AFFILIE,NUMERO_QUITANCE,SOLDE, PAYED,date,NOM_CLIENT,PRENOM_CLIENT,tva,percentage,document,beneficiaire,age,sexe,employeur from  APP.CREDIT,APP.INVOICE,APP.CLIENT" + dbb + " WHERE   invoice.HEURE >'" + Debut + "' AND invoice.HEURE <'" + FIN + "' and  APP.INVOICE.NUM_CLIENT='" + stock + "' and  CREDIT.ID_INVOICE =INVOICE.ID_INVOICE and num_affiliation=numero_affilie and cause='M'  AND COMPTABILISE!='ANNULE'  order by INVOICE.ID_INVOICE ");
                    System.out.println("select APP.CREDIT.ID_INVOICE,NUMERO_AFFILIE,NUMERO_QUITANCE,SOLDE, PAYED,date,NOM_CLIENT,PRENOM_CLIENT,tva,percentage,document,beneficiaire,age,sexe,employeur from  APP.CREDIT,APP.INVOICE,APP.CLIENT" + dbb + " WHERE   invoice.HEURE >'" + Debut + "' AND invoice.HEURE <'" + FIN + "' and  APP.INVOICE.NUM_CLIENT='" + stock + "' and  CREDIT.ID_INVOICE =INVOICE.ID_INVOICE and num_affiliation=numero_affilie and cause='M'  AND COMPTABILISE!='ANNULE'  order by INVOICE.ID_INVOICE ");

                    while (outResult.next()) {

                        int ID_INVOICE = outResult.getInt(1);
                        String NUMERO_AFFILIE = outResult.getString(2);
                        String NUMERO_QUITANCE = outResult.getString(3);
                        int SOLDE = outResult.getInt(4);
                        int PAYED = outResult.getInt(5);
                        String da = outResult.getString(6);
                        String nomO = outResult.getString(7);
                        String prenom = outResult.getString(8);
                        int tva = outResult.getInt(9);
                        int vtva = 0;
                        String beneficiaire = outResult.getString(12);
                        String age = outResult.getString(13);
                        String sexe = outResult.getString(14);
                        String employeur = outResult.getString(15);
                        ((Assurance) societeJList.getSelectedValue()).facture = creditDoc;

//System.out.println(beneficiaire);
                        Credit cc = new Credit(ID_INVOICE, NUMERO_AFFILIE, NUMERO_QUITANCE, SOLDE, PAYED, da, nomO + " " + prenom, beneficiaire, vtva);
                        cc.AGE = age;
                        cc.EMPLOYEUR = employeur;
                        cc.SEXE = sexe;

                        credit.add(cc);
                    }
                    if (nAff.equals("SPECIAL")) {
                        makePrint(d,opt);
                    }

                } else if (nAff.equals("NORMAL")) {
                    outResult = s.executeQuery("select APP.CREDIT.ID_INVOICE,NUMERO_AFFILIE,NUMERO_QUITANCE,SOLDE, PAYED,date,NOM_CLIENT,PRENOM_CLIENT,tva,percentage,document,age,sexe from  APP.CREDIT,APP.INVOICE,APP.CLIENT" + dbb + " WHERE  invoice.HEURE >'" + Debut + "' AND invoice.HEURE <'" + FIN + "' and APP.INVOICE.NUM_CLIENT='" + stock + "' and  CREDIT.ID_INVOICE =INVOICE.ID_INVOICE and num_affiliation=numero_affilie and cause='M' AND COMPTABILISE!='ANNULE' order by INVOICE.ID_INVOICE ");
                    System.out.println("select APP.CREDIT.ID_INVOICE,NUMERO_AFFILIE,NUMERO_QUITANCE,SOLDE, PAYED,date,NOM_CLIENT,PRENOM_CLIENT,tva,percentage,document from  APP.CREDIT,APP.INVOICE,APP.CLIENT" + dbb + " WHERE  invoice.HEURE >'" + Debut + "' AND invoice.HEURE <'" + FIN + "' and APP.INVOICE.NUM_CLIENT='" + stock + "' and  CREDIT.ID_INVOICE =INVOICE.ID_INVOICE and num_affiliation=numero_affilie and cause='M' AND COMPTABILISE!='ANNULE'  order by INVOICE.ID_INVOICE ");

//        outResult = s.executeQuery("select APP.CREDIT.ID_INVOICE,NUMERO_AFFILIE,NUMERO_QUITANCE,SOLDE, PAYED,date,NOM_CLIENT,PRENOM_CLIENT,tva,percentage,document from  APP.CREDIT,APP.INVOICE,APP.CLIENT"+dbb+" WHERE  invoice.HEURE >'"+Debut+"' AND invoice.HEURE <'"+FIN+"' and APP.INVOICE.DATE LIKE '%"+d+"%'and  APP.INVOICE.NUM_CLIENT='"+stock+"' and  CREDIT.ID_INVOICE =INVOICE.ID_INVOICE and num_affiliation=numero_affilie and cause='M' order by INVOICE.ID_INVOICE ");
//        System.out.println("select APP.CREDIT.ID_INVOICE,NUMERO_AFFILIE,NUMERO_QUITANCE,SOLDE, PAYED,date,NOM_CLIENT,PRENOM_CLIENT,tva,percentage,document from  APP.CREDIT,APP.INVOICE,APP.CLIENT"+dbb+" WHERE  invoice.HEURE >'"+Debut+"' AND invoice.HEURE <'"+FIN+"' and APP.INVOICE.DATE LIKE '%"+d+"%'and  APP.INVOICE.NUM_CLIENT='"+stock+"' and  CREDIT.ID_INVOICE =INVOICE.ID_INVOICE and num_affiliation=numero_affilie and cause='M' order by INVOICE.ID_INVOICE ");
                    while (outResult.next()) {
                        int ID_INVOICE = outResult.getInt(1);
                        String NUMERO_AFFILIE = outResult.getString(2);
                        String NUMERO_QUITANCE = outResult.getString(3);
                        int SOLDE = outResult.getInt(4);
                        int PAYED = outResult.getInt(5);
                        String da = outResult.getString(6);
                        String nomO = outResult.getString(7);
                        String prenom = outResult.getString(8);
                        int tva = outResult.getInt(9);
                        int vtva = (int) ((100 - outResult.getInt(10)) * tva) / 100;
                        Credit cre=new Credit(ID_INVOICE, NUMERO_AFFILIE, NUMERO_QUITANCE, SOLDE, PAYED, da, nomO, prenom, vtva, SOLDE + PAYED);
                        cre.AGE = outResult.getString("AGE");
                        cre.SEXE = outResult.getString("SEXE");
                         
                        credit.add(cre);

                    }

                } else {

                    String sql = "select APP.CREDIT.ID_INVOICE,NUMERO_AFFILIE,NUMERO_QUITANCE,SOLDE, PAYED,date,"
                            + " NOM_CLIENT,PRENOM_CLIENT,tva,percentage,document,age,sexe from  APP.CREDIT,APP.INVOICE,APP.CLIENT" + dbb + " "
                            + " WHERE  invoice.HEURE >'" + Debut + "' AND invoice.HEURE <'" + FIN + "' and APP.INVOICE.NUM_CLIENT='"
                            + stock + "' and  CREDIT.ID_INVOICE =INVOICE.ID_INVOICE and num_affiliation=numero_affilie and "
                            + " cause='M' AND COMPTABILISE!='ANNULE'  order by INVOICE.ID_INVOICE ";
                    outResult = s.executeQuery(sql);
                    System.out.println(sql);

                    //outResult = s.executeQuery("select APP.CREDIT.ID_INVOICE,NUMERO_AFFILIE,numero_quitance,SOLDE, PAYED,date,NOM_CLIENT,PRENOM_CLIENT,tva,percentage,document from  APP.CREDIT,APP.INVOICE,APP.CLIENT"+dbb+" WHERE invoice.HEURE >'"+Debut+"' AND invoice.HEURE <'"+FIN+"' and  APP.INVOICE.DATE LIKE '%"+d+"%'and  APP.INVOICE.NUM_CLIENT='"+stock+"' and  CREDIT.ID_INVOICE =INVOICE.ID_INVOICE and num_affiliation=numero_affilie and cause='M' order by INVOICE.ID_INVOICE ");
                    while (outResult.next()) {
                        int ID_INVOICE = outResult.getInt(1);
                        String NUMERO_AFFILIE = outResult.getString(2);
                        String NUMERO_QUITANCE = outResult.getString(3);
                        int SOLDE = outResult.getInt(4);
                        int PAYED = outResult.getInt(5);
                        String da = outResult.getString(6);
                        String nomO = outResult.getString(7);
                        String prenom = outResult.getString(8);
                        int tva = outResult.getInt(9);
                        int vtva = (int) ((100 - outResult.getInt(10)) * tva) / 100;
                        Credit cre = new Credit(ID_INVOICE, NUMERO_AFFILIE, NUMERO_QUITANCE,
                                SOLDE, PAYED, da, nomO, prenom, vtva);

                        cre.AGE = outResult.getString("AGE");
                        cre.SEXE = outResult.getString("SEXE");
                        credit.add(cre);

                    }
                    makePrint(d,opt);
//credit=new LinkedList();
                }
            } else {
                JOptionPane.showMessageDialog(null, d + " " + l(db.lang, "V_FORMAT") + " ", l(db.lang, "V_DATE"), JOptionPane.ERROR_MESSAGE);
            }
            outResult.close();
        } catch (Throwable e) {
            db.insertError(e + "", " V getcredit");
            System.out.println(" . .credit .. exception thrown:" + e);
        }
    }

    void makePrint(String d,String opt) {
        String option=opt;
        
        if(option.length()<2)
            option = (String) JOptionPane.showInputDialog(null, l(lang, "V_CHOICE"), "PRINT",
                JOptionPane.QUESTION_MESSAGE, null, new String[]{"S TOTAL", "GENERAL"}, nom);

        // J  = (String)   JOptionPane.QUESTION_MESSAGE, null, new String [] {"S TOTAL","GENERAL"}, nom);
// PrintWriter sorti = null;
//String file =d+"_"+option+"_"+((Assurance)societeJList.getSelectedValue()).sigle+".txt";
        lotList = new LinkedList();

        int totalG = 0;
        int tvaG = 0;

        for (int i = 0; i < credit.size(); i++) {
            double total = 0;
            double tva = 0;
            int k = 0;
            Credit cre = credit.get(i);
            double percentage = ((double) cre.SOLDE / (double) (cre.SOLDE + cre.PAYED));
            try {
                outResult = s.executeQuery("select PRODUCT.NAME_PRODUCT,LIST.PRICE,LIST.QUANTITE from  list,product WHERE list.id_invoice=" + cre.ID_INVOICE + " and  LIST.CODE_UNI=PRODUCT.CODE");
                while (outResult.next()) {
                    double price = (outResult.getDouble(2));
                    double subTot = (int) ((price * outResult.getInt(3) * (percentage)) + 0.3);
                    double tot = ((price * outResult.getInt(3)));
//System.out.println(price+"   "+subTot+"   "+percentage);
                    if (k == 0) {
                        if (option.equals("S TOTAL")) {
                            lotList.add(new Lot("" + cre.ID_INVOICE, cre.NUM_CLIENT, cre.NUMERO_QUITANCE, cre.NUMERO_AFFILIE, cre.NOM + " " + cre.PRENOM, outResult.getString(1), "" + outResult.getInt(3), "" + price, "" + subTot, "", ""));
                        }
                        if (option.equals("GENERAL")) {
                            lotList.add(new Lot("" + cre.ID_INVOICE, cre.NUM_CLIENT, cre.NUMERO_QUITANCE, cre.NUMERO_AFFILIE, cre.NOM + " " + cre.PRENOM, outResult.getString(1), "" + outResult.getInt(3), "" + price, "" + tot, "" + subTot, ""));
                        }
                    } else {
                        if (option.equals("S TOTAL")) {
                            lotList.add(new Lot("", "", "", "", "", outResult.getString(1), "" + outResult.getInt(3), "" + price, "" + subTot, "", ""));
                        }
                        if (option.equals("GENERAL")) {
                            lotList.add(new Lot("" + cre.ID_INVOICE, cre.NUM_CLIENT, cre.NUMERO_QUITANCE, cre.NUMERO_AFFILIE, cre.NOM + " " + cre.PRENOM, outResult.getString(1), "" + outResult.getInt(3), "" + price, "" + tot, "" + subTot, ""));
                        }

                    }
                    total = total + subTot;
                    k++;
                }
                if (option.equals("S TOTAL")) {
                    lotList.add(new Lot("", "", "", "", "", "     -------------------", "0", "0", "" + (int) total, "", ""));
                }

            } catch (Throwable ex) {
                Logger.getLogger(VoirFichier.class.getName()).log(Level.SEVERE, null, ex);
            }
            totalG = totalG + (int) total;
            tvaG = tvaG + (int) tva;
            //soldeClient=soldeClient+cre.SOLDE;
            //CashClient=CashClient+cre.PAYED;
        }
        mergeName(lotList);
    }

    void mergeName(LinkedList<Lot> huza) {
        // for(int i=0;i<huza.size();i++)
        // System.out.println(huza.get(i).u6+" qte "+huza.get(i).u7);

        LinkedList<Lot> join = new LinkedList();
        int j = 0;
        int qte = 0;
        double price = 0;
        double subT = 0;
        boolean done = true;

//System.out.println( " ==================================================================== ");
        for (int i = 0; i < huza.size(); i++) {
            Lot now = huza.get(i);
            j = i + 1;
            qte = Integer.parseInt(now.u7);
            price = Double.parseDouble(now.u8);
            subT = Double.parseDouble(now.u9);

//System.out.println(j+huza.get(j).u6+" ====================== "+i+now.u6);
            if (j < huza.size() && now.u6.equals(huza.get(j).u6) && huza.get(j).u1.equals("")) {
                while (now.u6.equals(huza.get(j).u6) && huza.get(j).u1.equals("")) {
                    qte = qte + Integer.parseInt(huza.get(j).u7);
//System.out.println(huza.get(j).u6+" ===========IN=========== "+subT);
                    subT = subT + Double.parseDouble(huza.get(j).u9);

//"","","","","",outResult.getString(1),""+outResult.getInt(3),""+price,""+subTot,"",""
                    i = j;
                    j++;

                    done = false;
                }
            }

            if (qte == 0) {
                join.add(now);
            } else {
                join.add(new Lot(now.u1, now.u2, now.u3, now.u4, now.u5, now.u6, "" + qte, "" + price, "" + (subT), now.u0, now.u11));
            }

            if (done) {
//System.out.println(now.u6+" qte "+qte);
//i=j;
            }
        }

//System.out.println( " ==================================================================== ");
//for(int i=0;i<join.size();i++)
        //  System.out.println(join.get(i).u6+" after qte "+join.get(i).u7);
        lotList = join;
    }

    static public void doOneCredit(String assurance, Statement stat) {
        JTable jTable = new javax.swing.JTable();
        LinkedList<Credit> credit = new LinkedList();

        double soldeClient = 0;
        double CashClient = 0;

        String STATUS = (String) JOptionPane.showInputDialog(null, "CHOIX", " ",
                JOptionPane.QUESTION_MESSAGE, null, new String[]{
                    "ALL", "PAYED", "NOT PAYED"}, "NOT PAYED");

        if (STATUS.equals("NOT PAYED")) {
            STATUS = " AND COMPTABILISE='NON' ";
        } else if (STATUS.equals("PAYED")) {
            STATUS = " AND COMPTABILISE='PAYED' ";
        } else {
            STATUS = " ";
        }

        String NOM = JOptionPane.showInputDialog("NOM OU PRENOM OU NUMERO AFFILIE ", "");
        String table = "";
        //String assurance = "";   

        if (assurance.equals("RAMA")) {
            table = "_RAMA";
        }

        Client c = null;
        LinkedList<Client> client = new LinkedList();
        try {
            if (table.equals("UNI")) {
                ResultSet outResult = stat.executeQuery(""
                        + " select * from APP.CLIENT  where (nom_client like '%" + NOM + "%' or prenom_client like '%" + NOM
                        + "%' or NUM_Affiliation like '%" + NOM + "%')  ");

                while (outResult.next()) {
                    String NUM_AFFILIATIONa = outResult.getString(1);
                    String NOM_CLIENTa = outResult.getString(2);
                    String PRENOM_CLIENTa = outResult.getString(3);
                    int PERCENTAGEa = outResult.getInt(4);
                    int DATE_EXPa = outResult.getInt(5);
                    String ASSURANCEa = outResult.getString(6);
                    String EMPLOYEURa = outResult.getString(7);
                    String SECTEURa = outResult.getString(8);
                    String VISAa = outResult.getString(9);
                    String CODE = outResult.getString(10);
                    String AGE = outResult.getString(11);
                    String SEXE = outResult.getString(12);
                    String LINK = outResult.getString(13);
                    String BENEFICIAIRE = outResult.getString(14);
                    c = new Client(NUM_AFFILIATIONa, NOM_CLIENTa, PRENOM_CLIENTa, PERCENTAGEa, DATE_EXPa, ASSURANCEa, EMPLOYEURa, SECTEURa, VISAa, CODE, AGE, SEXE, LINK, BENEFICIAIRE);
                    client.add(c);
                }
            } else if (assurance.equals("SOCIETE")) {
                ResultSet outResult = stat.executeQuery(""
                        + "select * from APP.CLIENT where (nom_client like '%" + NOM
                        + "%' or prenom_client like '%" + NOM + "%' or NUM_Affiliation like '%" + NOM + "%') ");

                while (outResult.next()) {
                    String NUM_AFFILIATIONa = outResult.getString(1);
                    String NOM_CLIENTa = outResult.getString(2);
                    String PRENOM_CLIENTa = outResult.getString(3);
                    int PERCENTAGEa = outResult.getInt(4);
                    int DATE_EXPa = outResult.getInt(5);
                    String ASSURANCEa = outResult.getString(6);
                    String EMPLOYEURa = outResult.getString(7);
                    String SECTEURa = outResult.getString(8);
                    String VISAa = outResult.getString(9);
                    String CODE = outResult.getString(10);
                    String AGE = outResult.getString(11);
                    String SEXE = outResult.getString(12);
                    String LINK = outResult.getString(13);
                    String BENEFICIAIRE = outResult.getString(14);
                    c = new Client(NUM_AFFILIATIONa, NOM_CLIENTa, PRENOM_CLIENTa, PERCENTAGEa, DATE_EXPa, ASSURANCEa, EMPLOYEURa, SECTEURa, VISAa, CODE, AGE, SEXE, LINK, BENEFICIAIRE);
                    client.add(c);
                }
            } else {
                ResultSet outResult = stat.executeQuery(""
                        + "select * from APP.CLIENT" + table + "  where (nom_client like '%" + NOM
                        + "%' or prenom_client like '%" + NOM + "%' or NUM_Affiliation like '%" + NOM
                        + "%')  ");
                while (outResult.next()) {
                    String NUM_AFFILIATIONa = outResult.getString(1);
                    String NOM_CLIENTa = outResult.getString(2);
                    String PRENOM_CLIENTa = outResult.getString(3);
                    int PERCENTAGEa = outResult.getInt(4);
                    int DATE_EXPa = outResult.getInt(5);
                    String ASSURANCEa = outResult.getString(6);
                    String EMPLOYEURa = outResult.getString(7);
                    String SECTEURa = outResult.getString(8);
                    String VISAa = outResult.getString(9);
                    String CODE = outResult.getString(10);
                    String AGE = outResult.getString(11);
                    String SEXE = outResult.getString(12);
                    String LINK = outResult.getString(13);
                    String BENEFICIAIRE = outResult.getString(14);
                    c = new Client(NUM_AFFILIATIONa, NOM_CLIENTa, PRENOM_CLIENTa, PERCENTAGEa, DATE_EXPa, ASSURANCEa, EMPLOYEURa, SECTEURa, VISAa, CODE, AGE, SEXE, LINK, BENEFICIAIRE);
                    client.add(c);
                }
                outResult.close();
            }
        } catch (Throwable e) {
            JOptionPane.showConfirmDialog(null, e + " getclintB ");
        }

        if (assurance.equals("RAMA")) {
            try {
                c = (Client) JOptionPane.showInputDialog(null, " ", "Client",
                        JOptionPane.QUESTION_MESSAGE, null, client.toArray(), "");

                ResultSet outResult = stat.executeQuery(
                        "select APP.CREDIT.ID_INVOICE,NUMERO_AFFILIE,NUMERO_QUITANCE,SOLDE, PAYED,date,NOM_CLIENT,PRENOM_CLIENT,"
                        + "tva,percentage,document,beneficiaire from  APP.CREDIT,APP.INVOICE,APP.CLIENT" + table + " WHERE  "
                        + " CREDIT.ID_INVOICE =INVOICE.ID_INVOICE and num_affiliation=numero_affilie"
                        + " and numero_affilie='" + c.NUM_AFFILIATION + "'   " + STATUS
                        + " order by INVOICE.heure ");

                while (outResult.next()) {

                    int ID_INVOICE = outResult.getInt(1);
                    String NUMERO_AFFILIE = outResult.getString(2);
                    String NUMERO_QUITANCE = outResult.getString(3);
                    double SOLDE = outResult.getDouble(4);
                    double PAYED = outResult.getDouble(5);
                    String da = outResult.getString(6);
                    String nomO = outResult.getString(7);
                    String prenom = outResult.getString(8);
                    int tva = outResult.getInt(9);
                    int vtva = (int) ((100 - outResult.getInt(10)) * tva) / 100;
                    String comptabilise = outResult.getString("comptabilise");
                    Credit credito = new Credit(ID_INVOICE, NUMERO_AFFILIE, NUMERO_QUITANCE, SOLDE,
                            PAYED, da, nomO, prenom, vtva, SOLDE + PAYED);
                    credito.comptabilise = comptabilise;
                    credito.departement = outResult.getString("departement");
                    credito.paid_time = outResult.getString("paid_time");
                    credito.serveur = outResult.getString("serveur");
                    credito.employe = outResult.getString(17);
                    String beneficiaire = outResult.getString(12);
//System.out.println(beneficiaire);
                    credit.add(credito);
                }
            } catch (SQLException ex) {
                Logger.getLogger(VoirFichier.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            try {
                String sql = "select APP.CREDIT.ID_INVOICE,NUMERO_AFFILIE,NUMERO_QUITANCE,SOLDE, PAYED,date,NOM_CLIENT,"
                        + " PRENOM_CLIENT,tva,percentage,document,beneficiaire,comptabilise,departement,paid_time,serveur,"
                        + " APP.INVOICE.employe "
                        + " from  APP.CREDIT,APP.INVOICE,APP.CLIENT" + table
                        + " WHERE  CREDIT.ID_INVOICE =INVOICE.ID_INVOICE and num_affiliation=numero_affilie and numero_affilie='"
                        + c.NUM_AFFILIATION + "'  " + STATUS + "  order by INVOICE.heure desc ";

                System.out.println(sql);

                ResultSet outResult = stat.executeQuery(sql);

                while (outResult.next()) {

                    int ID_INVOICE = outResult.getInt(1);
                    String NUMERO_AFFILIE = outResult.getString(2);
                    String NUMERO_QUITANCE = outResult.getString(3);
                    double SOLDE = outResult.getDouble(4);
                    double PAYED = outResult.getDouble(5);
                    String da = outResult.getString(6);
                    String nomO = outResult.getString(7);
                    String prenom = outResult.getString(8);
                    int tva = outResult.getInt(9);
                    int vtva = (int) ((100 - outResult.getInt(10)) * tva) / 100;
                    String comptabilise = outResult.getString("comptabilise");
                    Credit credito = new Credit(ID_INVOICE, NUMERO_AFFILIE, NUMERO_QUITANCE, SOLDE,
                            PAYED, da, nomO, prenom, vtva, SOLDE + PAYED);
                    credito.comptabilise = comptabilise;
                    credito.departement = outResult.getString("departement");
                    credito.paid_time = outResult.getString("paid_time");
                    credito.serveur = outResult.getString("serveur");
                    credito.employe = outResult.getString(17);

                    credit.add(credito);

                }
            } catch (SQLException ex) {
                Logger.getLogger(VoirFichier.class.getName()).log(Level.SEVERE, null, ex);
            }

            int linge = credit.size();

            String[][] s1 = new String[linge + 1][18];

            s1[0][0] = "ID_INVOICE";
            s1[0][1] = "DATE";
            s1[0][2] = "N° QUITTANCE";
            s1[0][3] = "N° AFF.";
            s1[0][4] = "BENEFICIARY NAMES";
            s1[0][5] = "CASHIER";
            s1[0][6] = "SEXE";
            s1[0][7] = "AFFILIATE NAME";
            s1[0][8] = "AFFILIATE AFFECTATION";
            s1[0][9] = "SOLDE";
            s1[0][10] = "PAYED";
            s1[0][11] = "TOTAL";
            s1[0][12] = "TVA";
            s1[0][13] = "ASSURANCE";
            s1[0][14] = "CAISSE";
            s1[0][15] = "TIME";
            s1[0][16] = "SERVEUR";
            s1[0][17] = "DPT";

            for (int i = 1; i < s1.length; i++) {
                Credit C = credit.get(i - 1);
                s1[i][0] = "" + C.ID_INVOICE;
                s1[i][1] = "" + C.NUM_CLIENT;
                s1[i][2] = C.NUMERO_QUITANCE;
                s1[i][3] = C.NUMERO_AFFILIE;
                s1[i][4] = C.PRENOM;
                s1[i][5] = C.employe;
                s1[i][6] = C.SEXE;
                s1[i][7] = C.NOM;
                s1[i][8] = C.EMPLOYEUR;
                s1[i][9] = "" + C.SOLDE;
                s1[i][10] = "" + C.PAYED;
                s1[i][11] = "" + (C.SOLDE + C.PAYED);
                s1[i][12] = "" + C.tva;
                s1[i][13] = assurance;
                s1[i][14] = C.comptabilise;
                s1[i][15] = C.paid_time;
                s1[i][16] = C.serveur;
                s1[i][17] = C.departement;

                if (C.comptabilise != null && C.comptabilise.equals("PAYED")) {
                    soldeClient = soldeClient + C.SOLDE;
                }

                CashClient = CashClient + (C.SOLDE + C.PAYED);

            }

            jTable.setModel(new javax.swing.table.DefaultTableModel(s1,
                    s1[0]) {
                        Class[] types = new Class[]{
                            java.lang.String.class,
                            java.lang.String.class,
                            java.lang.String.class,
                            java.lang.String.class,
                            java.lang.String.class,
                            java.lang.String.class,
                            java.lang.String.class,
                            java.lang.String.class,
                            java.lang.String.class,
                            java.lang.String.class,
                            java.lang.String.class,
                            java.lang.String.class,
                            java.lang.String.class,
                            java.lang.String.class,
                            java.lang.String.class
                        };

                    });
        }

        jTable.addMouseListener(new MouseListener() {
            Client getClient(int id_invoice) {
                Client infoClient = null;
                String client = "";
                String num_affiliation = "";
                String nom_client = "";
                String numero_quitance = "";
                int percentage = 0;
                String prenom_client = "";
                String employeur = "";
                double solde = 0, payed = 0;
                double perc = 0;

                try {
// Clients Societe
                    Statement statem = Db.connStatic.createStatement();
                    String sql = ""
                            + "select num_client,numero_affilie,numero_quitance,client.percentage,nom_client,prenom_client,"
                            + "invoice.key_invoice,client.employeur,invoice.cash,invoice.credit from  APP.INVOICE,app.credit,client "
                            + "where invoice.id_invoice=credit.id_invoice  "
                            + " and client.num_affiliation=credit.numero_affilie and  APP.INVOICE.id_invoice = " + id_invoice;
                    System.out.println(sql);
                    ResultSet outResult = statem.executeQuery(sql);

                    while (outResult.next()) {
                        client = outResult.getString("num_client");
                        num_affiliation = outResult.getString("numero_affilie");
                        nom_client = outResult.getString("nom_client");
                        numero_quitance = outResult.getString("numero_quitance");
                        percentage = outResult.getInt("percentage");
                        prenom_client = outResult.getString("prenom_client");
                        employeur = outResult.getString("employeur");
                        solde = outResult.getDouble("credit");
                        payed = outResult.getDouble("cash");
                        System.out.println("solde:" + solde + " payed:" + payed);
                        perc = (((double) solde / (double) (payed + solde)) * 100);
                        System.out.println("perc:" + perc);
                        infoClient = new Client(num_affiliation, nom_client, prenom_client, percentage, 0, "" + client, "" + employeur, "", "", "" + num_affiliation);
                        if (perc > 0) {
                            infoClient.PERCENTAGE_PART = perc;
                        }
                        return infoClient;
                    }
                    outResult.close();

                    if (infoClient == null) {

                        outResult = statem.executeQuery("select num_client,numero_affilie,nom_client,prenom_client,numero_quitance,client_RAMA.percentage,employeur"
                                + " from APP.INVOICE,APP.CREDIT,APP.CLIENT_RAMA "
                                + "where APP.INVOICE.id_invoice =  " + id_invoice + "   "
                                + "and APP.INVOICE.id_invoice = APP.credit.id_invoice and APP.client_RAMA.num_affiliation = APP.credit.numero_affilie  order by app.invoice.id_invoice");

                        System.out.println("select num_client,numero_affilie,nom_client,prenom_client,numero_quitance,client_RAMA.percentage,employeur"
                                + " from APP.INVOICE,APP.CREDIT,APP.CLIENT_RAMA "
                                + "where APP.INVOICE.id_invoice =  " + id_invoice + "   "
                                + "and APP.INVOICE.id_invoice = APP.credit.id_invoice and APP.client_RAMA.num_affiliation = APP.credit.numero_affilie  order by app.invoice.id_invoice");

                        while (outResult.next()) {
                            client = outResult.getString(1);
                            num_affiliation = outResult.getString(2);
                            nom_client = outResult.getString(3);
                            prenom_client = outResult.getString(4);
                            numero_quitance = outResult.getString(5);
                            percentage = outResult.getInt(6);
                            employeur = outResult.getString(7);
                            infoClient = new Client(num_affiliation, nom_client, prenom_client, percentage, 0, "" + client, "" + employeur, "", "", "" + num_affiliation);

                        }
                        outResult.close();
                        if (infoClient == null) {
                            outResult = statem.executeQuery("select name_client from APP.INVOICE "
                                    + "where APP.INVOICE.id_invoice =  " + id_invoice + "   ");
                            System.out.println("select name_client from APP.INVOICE "
                                    + "where APP.INVOICE.id_invoice =  " + id_invoice + "   ");

                            while (outResult.next()) {
                                client = outResult.getString(1);
                            }
                            return new Client("DIVERS", "", " " + client, 0, 0, "" + "", "" + "", "", "", "" + "");
                        } else {
                            return infoClient;
                        }
                    }
                } catch (Throwable e) {
                    System.out.println(e + " retour ");
                }
                return new Client("DIVERS", "", "CLIENT DIVERS", 0, 0, "" + "", "" + "", "", "", "" + "");
            }

            public void actionPerformed(ActionEvent ae) {
            }

            @Override
            public void mouseExited(MouseEvent ae) {
            }

            @Override
            public void mouseClicked(MouseEvent e) {

                JTable j = (JTable) e.getSource();
                int row = j.getSelectedRow();
                int col = j.getSelectedColumn();

                String id = (String) j.getValueAt(row, 0);
                String amount = (String) j.getValueAt(row, 10);
                String date = (String) j.getValueAt(row, 1);
                String ref = (String) j.getValueAt(row, 3);
                String affiliate = (String) j.getValueAt(row, 7);
                String ben = (String) j.getValueAt(row, 4);
                String employe = (String) j.getValueAt(row, 5);
                int id_invoice = Integer.parseInt(id);
                String erekana = id + " \n " + date + " \n " + affiliate
                        + " \n " + ben + " \n " + ref + " \n " + amount;

                int n = JOptionPane.showConfirmDialog(null, erekana);

                if (n == 0) {

                    LinkedList<Product> prod2 = new LinkedList();
                    String Operation = "NS";
                    String external_data = "external_data";
                    String date_time_mrc = "";
                    String tin = "";
                    String mrc = "";
                    String heure = "heure";
                    String quitance = "";
                    String nom_client = "";
                    String client = "";
                    double cash = 0;
                    double credit = 0;
                    try {

                        Statement statem = Db.connStatic.createStatement();
                        String createString1
                                = " update APP.INVOICE set EMPLOYE='" + Main.umukozi.NOM_EMPLOYE + "',DEPARTEMENT='" + Main.umukozi.BP_EMPLOYE + "'"
                                + ",comptabilise='PAYED',PAID_TIME= CURRENT TIMESTAMP,SERVEUR='" + employe + "' where ID_INVOICE= " + id + "";
                        System.out.println(createString1);
                        statem.execute(createString1);

                        String sql = ""
                                + "select  num_client, code_uni ,quantite,list.price,list.original_price,"
                                + "name_product,list.tva, " + external_data + ",  " + heure + ",invoice.tin,invoice.mrc,employe,name_client,cash,credit "
                                + "from APP.PRODUCT,APP.INVOICE ,APP.LIST "
                                + " where     "
                                + "  APP.INVOICE.id_invoice = " + id
                                + " and APP.INVOICE.id_invoice = APP.LIST.id_invoice  "
                                + " AND APP.PRODUCT.CODE=APP.LIST.CODE_UNI and " + external_data + " like '%," + Operation + ",%'"
                                + " order by  name_product";
                        System.out.println(sql);
                        try {
                            ResultSet outResult = statem.executeQuery(sql);

                            while (outResult.next()) {
                                client = outResult.getString(1);
                                String code = outResult.getString(2);
                                int quantite = outResult.getInt(3);
                                double price = outResult.getDouble(4);
                                double orig = outResult.getDouble(5);
                                String desi = outResult.getString(6);
                                if (price != orig) {
                                    desi += "# Discount -" + (int) (100 - Math.abs((price * 100) / orig)) + "%";
                                }
                                double tva = outResult.getDouble(7);
                                external_data = outResult.getString(8);
                                date_time_mrc = outResult.getString(9);
                                tin = outResult.getString("tin");
                                mrc = outResult.getString("mrc");
                                employe = outResult.getString("employe");
                                nom_client = outResult.getString("name_client");
                                cash = outResult.getDouble("cash");
                                credit = outResult.getDouble("credit");
                                Product pro = new Product(0, desi, code, quantite, price, tva, code, "", "", null);
                                pro.originalPrice = orig;
                                prod2.add(pro);
                            }
                            outResult.close();

                            outResult = statem.executeQuery("select APP.CREDIT.NUMERO_QUITANCE FROM APP.CREDIT WHERE APP.CREDIT.ID_INVOICE=" + id_invoice + "");

                            while (outResult.next()) {
                                quitance = outResult.getString(1);
                            }
                            outResult.close();

                        } catch (SQLException ex) {
                            Logger.getLogger(Db.class.getName()).log(Level.SEVERE, null, ex);
                        }

                        String Rtype = Operation.substring(0, 1), Ttype = Operation.substring(1, 2);

                        Client infoClient = getClient(id_invoice);
                        LinkedList<String> Clientinfo = new LinkedList<String>();
                        if (infoClient != null) {
                            Clientinfo.add(infoClient.NUM_AFFILIATION);
                            Clientinfo.add(infoClient.NOM_CLIENT);
                            Clientinfo.add(infoClient.PRENOM_CLIENT);
                            Clientinfo.add(infoClient.ASSURANCE);
                            Clientinfo.add("" + infoClient.PERCENTAGE);
                            Clientinfo.add(infoClient.EMPLOYEUR);
                            Clientinfo.add("" + infoClient.PERCENTAGE_PART);

                        }

                        System.out.println(" Operation Operation   " + Operation);

                        RRA_PACK rp = Taxes.getRpack4(id_invoice, prod2, Rtype, Ttype, Taxes.getNowTime(date_time_mrc), mrc);

                        if (rp != null) {
                            boolean second = true;
                            rp.ClientTin = tin;
                            System.out.println("SECOND:" + second + "  CLIENT ");
                            System.out.println(" photoCopyS CLIENT " + client);
                            System.out.println("  CLIENT2 " + nom_client);

                            if (nom_client == null) {
                                nom_client = "";
                            }

                            if (second && client != null && client.equals("CASHNORM") && nom_client.length() < 3) {
                                try {
                                    int nn = JOptionPane.showConfirmDialog(null, " VOULEZ-VOUS METTRE LE NOM SUR LE FACTURE? ", " NOM DU CLIENT ", JOptionPane.YES_NO_OPTION);
                                    if (nn == 0) {
                                        String izina = (JOptionPane.showInputDialog(Main.cashier.db.l(Main.cashier.db.lang, "V_NAMEORSURNAME"), " "));
                                        if (izina == null) {
                                            nom_client = "";
                                        } else {
                                            nom_client = izina;
                                            statem.execute("UPDATE APP.INVOICE SET NAME_CLIENT='" + izina + "' WHERE ID_INVOICE=" + id_invoice + "");
                                        }
                                    }
                                } catch (Exception ex) {
                                    nom_client = "";
                                }
                            }
                            Taxes.printRRA3(external_data, Rtype, Ttype, rp.getProductList, rp, id_invoice, 0, Clientinfo, employe,
                                    quitance, nom_client, 0, 0, cash, credit, statem, "PAYED", null, "");
                        }

                    } catch (SQLException ex) {
                        Logger.getLogger(VoirFichier.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }

            }

            ;  
            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }
        });

        // (JTable j,LinkedList <Credit> credit,String s,Assurance ass,String server,String DATABASE)  
        new showJTable(jTable, credit, " " + assurance + "    IDENI : "
                + RRA_PRINT2.setVirgule(soldeClient, 1, ",") + "    YOSE : " + RRA_PRINT2.setVirgule(CashClient, 1, ","),
                null, "", "");

    }

    private JTable initComponents(String s2) {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable = new javax.swing.JTable();
        int linge = allProduct.size();
        String[][] s1 = new String[linge][13];
        for (int i = 0; i < linge; i++) {
            Product p = allProduct.get(i);
            s1[i][0] = "" + p.productCode;
            s1[i][1] = p.code;
            s1[i][2] = p.productName;
            s1[i][3] = "" + p.currentPrice;
            s1[i][4] = "" + p.tva;
            s1[i][5] = "" + p.codeBar;
            s1[i][6] = "" + p.observation;
        }
        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        String tva = "Tva";
        String rev = "Code Bar";
        String famille = "Obs";

        if (s2.equals("")) {
            tva = "Coef";
            rev = "Prix_Rev";
            famille = "Famille";
        }

        jTable.setModel(new javax.swing.table.DefaultTableModel(s1,
                new String[]{
                    "CODE", "UNI_CODE", "NAME", "CURRENT PRICE", tva, rev, famille
                }
        ) {
            Class[] types = new Class[]{
                java.lang.String.class,
                java.lang.String.class,
                java.lang.String.class,
                java.lang.String.class,
                java.lang.String.class,
                java.lang.String.class,
                java.lang.String.class
            };

            @Override
            public Class getColumnClass(int columnIndex) {
                return types[columnIndex];
            }
        });

        return jTable;
    }// </editor-fold>

    private JTable clientV() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable = new javax.swing.JTable();
        int linge = client.size();
        String[][] s1 = new String[linge][10];
        for (int i = 0; i < linge; i++) {

            Client C = client.get(i);
            s1[i][0] = C.NOM_CLIENT;
            s1[i][1] = C.PRENOM_CLIENT;
            s1[i][2] = C.NUM_AFFILIATION;
            s1[i][3] = "" + C.SECTEUR;
            s1[i][4] = "" + C.DATE_EXP;
            s1[i][5] = "" + C.ASSURANCE;
            s1[i][6] = "" + C.PERCENTAGE;
            // s1[i][7]=""+C.TEL_CLIENT;
            s1[i][7] = "" + C.EMPLOYEUR;
            s1[i][8] = "" + C.CODE;
        }
        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jTable.setModel(new javax.swing.table.DefaultTableModel(s1,
                new String[]{
                    "NOM_CLIENT", "PRENOM_CLIENT", "NUM_AFFILIATION", "SECTEUR", "DATE_EXP", "ASSURANCE", " PERCENTAGE", "DPT", "CODE_CLIENT"
                }
        ) {
            Class[] types = new Class[]{
                java.lang.String.class,
                java.lang.String.class,
                java.lang.String.class,
                java.lang.String.class,
                java.lang.String.class,
                java.lang.String.class,
                java.lang.String.class,
                java.lang.String.class,
                java.lang.String.class

            };

            @Override
            public Class getColumnClass(int columnIndex) {
                return types[columnIndex];
            }
        });

        return jTable;
    }// </editor-fold>

    private JTable invoiceV(String choix) {
        String separateur = (String) JOptionPane.showInputDialog(null, l(lang, "V_CHOICE"), l(lang,"V_SEPARATOR"),
                JOptionPane.QUESTION_MESSAGE, null, new String[]{"", ",", "."}, nom);

        // tvaYose=0;
        String cc = "REDUCTION";
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable = new javax.swing.JTable();
        int linge = facture.size();
        double tot = 0, taxA = 0, taxB = 0, taxeB = 0, hrTaxeB = 0;
        String[][] s1 = new String[linge][12];
        for (int i = 0; i < linge; i++) {
            Invoice C = facture.get(i);

            s1[i][0] = "" + C.id_invoice;
            s1[i][1] = C.id_client;
            s1[i][2] = C.date;
            s1[i][3] = "" + RRA_PRINT2.setVirgule(C.tot, 1, separateur);
            s1[i][6] = "" + C.id_employe;
            s1[i][7] = "" + C.heure;
            s1[i][4] = "" + RRA_PRINT2.setVirgule(C.tva, 1, separateur);
            if (choix.contains("ANNULE")) {
                cc = "NUM INVOICE";
                s1[i][5] = "" + (int) C.REDUCTION;
            } else {
                s1[i][5] = "" + RRA_PRINT2.setVirgule(C.REDUCTION, 1, separateur);
            }
            s1[i][8] = "" + RRA_PRINT2.setVirgule(C.totA, 1, separateur);
            s1[i][9] = "" + RRA_PRINT2.setVirgule(C.totB, 1, separateur);
            s1[i][11] = "" + RRA_PRINT2.setVirgule(C.HORSTAXE, 1, separateur);
            s1[i][10] = "" + RRA_PRINT2.setVirgule(C.taxB, 1, separateur);

            tot += C.tot;
            taxA += C.totA;
            taxB += C.totB;
            taxeB += C.taxB;
            hrTaxeB += C.HORSTAXE;

            // tvaYose=tvaYose+C.tva ;
        }
        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        totaux = " TOTAL = " + RRA_PRINT2.setVirgule(tot, 1, ",") + "; TOTAL A= "
                + RRA_PRINT2.setVirgule(taxA, 1, ",") + " ;TOTAL B= "
                + RRA_PRINT2.setVirgule(taxB, 1, ",") + " ;TAXE B= "
                + RRA_PRINT2.setVirgule(taxeB, 1, ",") + " ;HORS TAXE B= "
                + RRA_PRINT2.setVirgule(hrTaxeB, 1, ",") + "   ";
        jTable.setModel(new javax.swing.table.DefaultTableModel(s1,
                new String[]{
                    "ID_INVOICE", "CLIENT", "DATE", "TOTAL", "TVA", "" + cc, "EMPLOYE", "HEURE", "TOTAL A", "TOTAL B", "TAXE B", "HORS TAX B"

                }
        ) {
            Class[] types = new Class[]{
                java.lang.String.class,
                java.lang.String.class,
                java.lang.String.class,
                java.lang.String.class,
                java.lang.String.class,
                java.lang.String.class,
                java.lang.String.class,
                java.lang.String.class,
                java.lang.String.class,
                java.lang.String.class,
                java.lang.String.class
            };

//            public Class getColumnClass(int columnIndex) {
//                return types [columnIndex];
//            }
        });

        return jTable;
    }// </editor-fold>

    void upDateFacture(String fact) {
        try {
            System.out.println(fact.substring(5, fact.length()));
            String f = fact.substring(5, fact.length());
            s = conn.createStatement();
            String createString1 = " update APP.VARIABLES set value_variable='" + f + "' where APP.variables.famille_variable= 'FACTURE'";
            s.execute(createString1);
        } catch (SQLException ex) {
            Logger.getLogger(VoirFichier.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    String getFacture() {
        String fac = "";

        try {
            outResult = s.executeQuery("select  * from APP.VARIABLES where APP.variables.famille_variable= 'FACTURE'");

            while (outResult.next()) {
                String num = outResult.getString(1);
                String value = outResult.getString(2);
                String fam = outResult.getString(3);

                Integer in_int = new Integer(value);
                int entier = in_int.intValue() + 1;

                fac += num + entier;

            }
        } catch (Throwable e) {
            db.insertError(e + "", "getfact");
        }
        return fac;
    }

    private JTable creditV() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable = new javax.swing.JTable();
        int linge = credit.size();
        String nAff = "OLD FORMAT";

        if (((Assurance) societeJList.getSelectedValue()).sigle.equals("RAMA")) {
            nAff = (String) JOptionPane.showInputDialog(null, l(lang, "V_CHOICE"), " ",
                    JOptionPane.QUESTION_MESSAGE, null, new String[]{"NEW FORMAT", "OLD FORMAT"}, nom);
        }
        
        if (nAff.equals("NEW FORMAT")) {
            //String[][] s1 = new String[linge + 1][14];
            
            String bon="",medecin="",agent="",poste=""; 
            
            String[][] s1 = new String[linge + 1][17];

            s1[0][0] = "ID_INVOICE";
            s1[0][1] = "DATE";
            //s1[0][2] = "N° QUITTANCE";
            s1[0][2] = "N° AFF.";
            s1[0][3] = "BENEFICIARY NAMES";
            s1[0][4] = "AGE";
            s1[0][5] = "SEXE";
            s1[0][6] = "AFFILIATE NAME";
            s1[0][7] = "AFFILIATE AFFECTATION";
            s1[0][9] = "SOLDE";
            s1[0][9] = "PAYED";
            s1[0][10] = "TOTAL";
            s1[0][11] = "TVA";
            s1[0][12] = "ASSURANCE";
            s1[0][13]=  "BON";
            s1[0][14] = "PRESCRIBER";
            s1[0][15] = "HEALTH FACILITY";
            s1[0][16] = "OM";
            
            
            //"BON", "Prescriber", "Health Facility", "OM"

            for (int i = 1; i < s1.length; i++) {
                Credit C = credit.get(i - 1);
                s1[i][0] = "" + C.ID_INVOICE;
                s1[i][1] = "" + C.NUM_CLIENT;
//                s1[i][2] = C.NUMERO_QUITANCE;
                
                
            //  sdsds2-kirezi-.......-chk-.......- 
//                            cree = C.NUMERO_QUITANCE;
                            //  System.out.println(cre.NUMERO_QUITANCE);
                            String quit = C.NUMERO_QUITANCE.replaceAll("-..-", "-");
       // System.out.println(quit);

                            //661212-IMMACULEE-..-KANOMBE-..-UWERA 
                            String st1[] = quit.split("-");

                            if (st1.length == 4) {
                                bon = st1[0];
                                medecin = st1[1];
                                agent = st1[2];
                                poste = st1[3];
                            }
                            else
                            {
                                bon=quit;
                            }
                
                
                s1[i][2] = C.NUMERO_AFFILIE;
                s1[i][3] = C.PRENOM;
                s1[i][4] = C.AGE;
                s1[i][5] = C.SEXE;
                s1[i][6] = C.NOM;
                s1[i][7] = C.EMPLOYEUR;
                s1[i][8] = "" + C.SOLDE;
                s1[i][9] = "" + C.PAYED;
                s1[i][10] = "" + (C.SOLDE + C.PAYED);
                s1[i][11] = "" + C.tva;
                s1[i][12] = "" + ((Assurance) societeJList.getSelectedValue()).sigle;
                
                s1[i][13]= bon;
                s1[i][14] = agent;
                s1[i][15] = poste;
                s1[i][16] = medecin;

                soldeClient = soldeClient + C.SOLDE;
                CashClient = CashClient + C.PAYED;

            }
            setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

            jTable.setModel(new javax.swing.table.DefaultTableModel(s1,
                    new String[]{
                        "ID_INVOICE", "DATE", "N° AFF.", "BENEFICIARY NAMES", "AGE", "SEXE", "AFFILIATE NAME", "AFFILIATE AFFECTATION", "SOLDE", "PAYED", "TOTAL", "TVA", "ASSURANCE","BON","PRESCRIBER","HEALTH FACILITY","OM"
                    }) {
                        Class[] types = new Class[]{
                            java.lang.String.class,
                            java.lang.String.class,
                            java.lang.String.class,
                            java.lang.String.class,
                            java.lang.String.class,
                            java.lang.String.class,
                            java.lang.String.class,
                            java.lang.String.class,
                            java.lang.String.class,
                            java.lang.String.class,
                            java.lang.String.class
                        };

//            public Class getColumnClass(int columnIndex) {
//                return types [columnIndex];
//            }
                    });
        } else if (nAff.equals("OLD FORMAT")) {

            String[][] s1 = new String[linge + 1][11];

            s1[0][0] = "ID_INVOICE";
            s1[0][1] = "N° AFF.";
            s1[0][2] = "N° QUITTANCE";
            s1[0][3] = "NOM";
            s1[0][4] = "PRENOM";
            s1[0][5] = "SOLDE";
            s1[0][6] = "PAYED";
            s1[0][7] = "TOTAL";
            s1[0][8] = "DATE";
            s1[0][9] = "TVA";
            s1[0][10] = "ASSURANCE";

            for (int i = 1; i < s1.length; i++) {

                Credit C = credit.get(i - 1);
                s1[i][0] = "" + C.ID_INVOICE;
                s1[i][1] = C.NUMERO_AFFILIE;
                s1[i][2] = C.NUMERO_QUITANCE;
                s1[i][3] = C.NOM;
                s1[i][4] = C.PRENOM;
                s1[i][5] = "" + C.SOLDE;
                s1[i][6] = "" + C.PAYED;
                s1[i][7] = "" + (C.SOLDE + C.PAYED);
                s1[i][8] = "" + C.NUM_CLIENT;
                s1[i][9] = "" + C.tva;
                s1[i][10] = "" + ((Assurance) societeJList.getSelectedValue()).sigle;

                soldeClient = soldeClient + C.SOLDE;
                CashClient = CashClient + C.PAYED;

            }
            setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

            jTable.setModel(new javax.swing.table.DefaultTableModel(s1,
                    new String[]{
                        "ID_INVOICE", "N° AFF.", "N° QUITTANCE", "NOM", "PRENOM", "SOLDE", "PAYED", "TOTAL", "DATE", "TVA", "ASSURANCE"
                    }) {
                        Class[] types = new Class[]{
                            java.lang.String.class,
                            java.lang.String.class,
                            java.lang.String.class,
                            java.lang.String.class,
                            java.lang.String.class,
                            java.lang.String.class,
                            java.lang.String.class,
                            java.lang.String.class,
                            java.lang.String.class,
                            java.lang.String.class,
                            java.lang.String.class
                        };

                        public Class getColumnClass(int columnIndex) {
                            return types[columnIndex];
                        }
                    });

        }

        return jTable;
    }// </editor-fold>

    private void creditV1(String da) {
        String in = JOptionPane.showInputDialog(null, "N° FACTURE SAGE ", "FU900297");
        PrintWriter sorti = null;
        try {
            String st = "" + ((Assurance) societeJList.getSelectedValue()).sigle;
            String file = "Factures/" + da + "_" + st + ".txt";
            File f = new File(file);
            sorti = new PrintWriter(new FileWriter(f));
            try {
                int linge = credit.size();
                String[][] s1 = new String[linge][10];
                soldeClient = 0;
                for (int i = 0; i < linge; i++) {
                    Credit C = credit.get(i);
                    String factureSage = "6;" + in + "; " + st + ";" + C.ID_INVOICE + ";" + C.NUMERO_AFFILIE + ";" + C.NUM_CLIENT + ";" + C.NUMERO_QUITANCE + ";" + C.NOM + ";" + C.PRENOM + ";" + C.SOLDE + ";1;ZCOMPTOIR;" + (C.SOLDE + C.PAYED) + ";" + C.PAYED + ";" + C.tva + ";" + datez;
                    sorti.println(factureSage);
                    //  JOptionPane.showMessageDialog(null,factureSage,"Downloaded",JOptionPane.PLAIN_MESSAGE);
                }
                outResult.close();
                JOptionPane.showMessageDialog(null, file, "Regarde sur  Serveur Ishyiga /dist/" + file, JOptionPane.PLAIN_MESSAGE);
            } catch (Throwable e) {
                db.insertError(e + "", "sage");
            }
            sorti.close();
        } catch (IOException ex) {
            Logger.getLogger(VoirFichier.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private JTable lotV() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable = new javax.swing.JTable();
        int linge = lotL.size();
        String[][] s1 = new String[linge][10];
        for (int i = 0; i < linge; i++) {
            Lot C = lotL.get(i);
            String datedoc = "" + C.id_lot;

            if (datedoc.length() == 5) {
                datedoc = "0" + C.id_lot;
            }

            String ddoc = "" + datedoc.substring(4) + "" + datedoc.substring(2, 4) + "" + datedoc.substring(0, 2);

            s1[i][0] = "" + C.productCode;
            s1[i][1] = "" + C.code;
            s1[i][2] = "" + C.proname;
            s1[i][3] = C.id_LotS;
            s1[i][4] = C.dateExp;
            s1[i][5] = ddoc;
            s1[i][6] = "" + C.qtyStart;
            s1[i][7] = "" + C.qtyLive;
            s1[i][8] = C.bon_livraison;
        }

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jTable.setModel(new javax.swing.table.DefaultTableModel(s1,
                new String[]{
                    "NUM", "CODE", "DESIGNATION", l(lang,"V_LOT"), l(lang,"V_DATEEXP"), "DATE DOC", "QTY_START", "QTY_LIVE", l(lang,"V_DELIVERY")

                }
        ) {
            Class[] types = new Class[]{
                java.lang.String.class,
                java.lang.String.class,
                java.lang.String.class,
                java.lang.String.class,
                java.lang.String.class,
                java.lang.String.class,
                java.lang.String.class,
                java.lang.String.class,
                java.lang.String.class
            };

            public Class getColumnClass(int columnIndex) {
                return types[columnIndex];
            }
        });

        return jTable;
    }// </editor-fold>

    public void lot() {
        lot.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent ae) {
                        if (ae.getSource() == lot) {
                            //"where bon_livraison !='uni001'"
                            getLot(" ORDER BY name_product,date_exp,id_lot");

                            new showJTable(lotV(), "LOT", server, dbName);
                        }
                    }
                });
    }

    private JTable lotVused() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable = new javax.swing.JTable();
        int linge = lotL.size();
        String[][] s1 = new String[linge][10];
        for (int i = 0; i < linge; i++) {

            Lot C = lotL.get(i);
            String dateExp = C.dateExp;
//String d=""+dateExp.substring(4)+""+dateExp.substring(2, 4)+""+dateExp.substring(0,2);

            String datedoc = "0" + C.id_lot;
            String ddoc = "" + datedoc.substring(4) + "" + datedoc.substring(2, 4) + "" + datedoc.substring(0, 2);

            s1[i][0] = "" + C.productCode;
            s1[i][1] = "" + C.code;
            s1[i][2] = "" + C.proname;
            s1[i][3] = C.id_LotS;
            s1[i][4] = dateExp;
            s1[i][5] = ddoc;
            s1[i][6] = "" + C.qtyStart;
            s1[i][7] = "" + C.qtyLive;
            s1[i][8] = C.bon_livraison;

        }
        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jTable.setModel(new javax.swing.table.DefaultTableModel(s1,
                new String[]{
                    "NUM", "CODE", "DESIGNATION", "LOT", "DATE EXP", "DATE DOC", "QTY_START", "QTY_LIVE", " BON_LIVRAISON"

                }
        ) {
            Class[] types = new Class[]{
                java.lang.String.class,
                java.lang.String.class,
                java.lang.String.class,
                java.lang.String.class,
                java.lang.String.class,
                java.lang.String.class,
                java.lang.String.class,
                java.lang.String.class,
                java.lang.String.class
            };

            public Class getColumnClass(int columnIndex) {
                return types[columnIndex];
            }
        });

        return jTable;
    }// </editor-fold>

    public void clientRama() {
        clientRama.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent ae) {
                        if (ae.getSource() == clientRama) {

                            if (!((Assurance) societeJList.getSelectedValue()).sigle.equals("MMI")) {

                                getClient();

                                new showJTable(clientV(), "CLIENT", server, dbName);
                            } else {
                                JOptionPane.showMessageDialog(null, "Data Protected");
                            }
                        }
                    }
                });
    }

    private JTable JCompta() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable = new javax.swing.JTable();
        int linge = compteL.size();
        String[][] s1 = new String[linge][13];
        for (int i = 0; i < linge; i++) {
            //CAIS-010204-411000-client-Pyt Fre 125-0,00-1800000,00-1800000,00-RAMA1;
            Compta C = compteL.get(i);

            s1[i][0] = C.livre;
            s1[i][1] = C.date;
            s1[i][3] = "" + C.compte;
            s1[i][2] = "" + C.cpte;
            s1[i][4] = C.libele;
            s1[i][5] = C.intitule;
            s1[i][6] = RRA_PRINT2.setVirgule(C.montant_cash, 1, ",");
            s1[i][7] = RRA_PRINT2.setVirgule(C.montant_credit, 1, ",");
            s1[i][8] = RRA_PRINT2.setVirgule(C.tva, 1, ",");

            s1[i][9] = RRA_PRINT2.setVirgule(C.TOTAL_A, 1, ",");
            s1[i][10] = RRA_PRINT2.setVirgule(C.TOTAL_B, 1, ",");
            s1[i][11] = RRA_PRINT2.setVirgule(C.TAXE_A, 1, ",");
            s1[i][12] = RRA_PRINT2.setVirgule(C.TAXE_B, 1, ",");
        }
        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jTable.setModel(new javax.swing.table.DefaultTableModel(s1,
                new String[]{
                    "LIVRE", "DATE", "CLIENT", "COMPTE", "LIBELE", "INTITULE", "DEBIT", "CREDIT", "TVA", "TOTAL_A", "TOTAL_B", "TAXE_A", "TAXE_B"
                }
        ) {
            Class[] types = new Class[]{
                java.lang.String.class,
                java.lang.String.class,
                java.lang.String.class,
                java.lang.String.class,
                java.lang.String.class,
                java.lang.String.class,
                java.lang.String.class,
                java.lang.String.class,
                java.lang.String.class
            };

//            public Class getColumnClass(int columnIndex) {
//                return types [columnIndex];
//            }
        });

        return jTable;
    }

    public int getMaxCompta() {
        int max = 0;
        try {

            outResult = s.executeQuery("select max(document) from  compta ");
            while (outResult.next()) {
                max = outResult.getInt(1);
            }

        } catch (SQLException ex) {
            db.insertError(ex + "", " NotCompta");
        }
        return max;
    }

    public void compta() {
        compta.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent ae) {
                        if (ae.getSource() == compta) {
                            String pwd = JOptionPane.showInputDialog(null, " Entrez le Mot de Passe ");

                            if (pwd.equals("uni")) {
                                String totalCompta = getCompta();
                                if (compteL.size() != 0) {
                                    new showJTable(JCompta(), "COMPTABILITE DE  " + (totalCompta), server, dbName);
                                } else {
                                    JOptionPane.showMessageDialog(null, l(lang,"V_RESULT"));
                                }

                            } else {
                                JOptionPane.showMessageDialog(null, l(lang,"V_ACCESSREFU"));
                            }

                        }
                    }
                });
    }

    public String getMois(String s1) {

        String m = "";
        if (s1.equals("01")) {
            m = "31";
        }
        if (s1.equals("02")) {
            m = "28";
        }
        if (s1.equals("03")) {
            m = "31";
        }
        if (s1.equals("04")) {
            m = "30";
        }
        if (s1.equals("05")) {
            m = "31";
        }
        if (s1.equals("06")) {
            m = "30";
        }
        if (s1.equals("07")) {
            m = "31";
        }
        if (s1.equals("08")) {
            m = "31";
        }
        if (s1.equals("09")) {
            m = "30";
        }
        if (s1.equals("10")) {
            m = "31";
        }
        if (s1.equals("11")) {
            m = "30";
        }
        if (s1.equals("12")) {
            m = "31";
        }
//System.out.println(s1+" -------- "+m);
        return m;
    }

    public boolean notCompta(String date, String client) {
        boolean done = true;
        try {

            outResult = s.executeQuery("select * from  compta where date='" + date + "' and intitule='" + client + "' ");
            while (outResult.next()) {
                done = false;
            }

        } catch (SQLException ex) {
            db.insertError(ex + "", " NotCompta");
        }
        return done;
    }

    public void insertCompta(int doc, String date, String client, double debit, double credit, String intitule, int compte, String libele, double tva, double valeur_mse, double TOTAL_A, double TOTAL_B, double TOTAL_C, double TOTAL_D, double TAXE_A, double TAXE_B, double TAXE_C, double TAXE_D) {
        try {
            int id = 0;

            psInsert = conn.prepareStatement("insert into APP.compta(date,compte,intitule,libele,montant_cash,montant_credit,tva,document,valeur_mse,TOTAL_A,TOTAL_B,TOTAL_C,TOTAL_D,TAXE_A,TAXE_B,TAXE_C,TAXE_D)  values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            psInsert.setString(1, date);
            psInsert.setInt(2, compte);
            psInsert.setString(3, intitule);
            psInsert.setString(4, libele);
            psInsert.setDouble(5, debit);
            psInsert.setDouble(6, credit);
            psInsert.setDouble(7, tva);
            psInsert.setInt(8, doc);
            psInsert.setDouble(9, valeur_mse);

            psInsert.setDouble(10, TOTAL_A);
            psInsert.setDouble(11, TOTAL_B);
            psInsert.setDouble(12, TOTAL_C);
            psInsert.setDouble(13, TOTAL_D);
            psInsert.setDouble(14, TAXE_A);
            psInsert.setDouble(15, TAXE_B);
            psInsert.setDouble(16, TAXE_C);
            psInsert.setDouble(17, TAXE_D);

            psInsert.executeUpdate();
            psInsert.close();

            if (!intitule.contains("Client")) {
                if (client.equals("CASHNORM")) {
                    s = conn.createStatement();
                    String createString1 = " update APP.invoice set document=" + doc + " where APP.invoice.num_client= '" + client + "' and  APP.INVOICE.DATE LIKE '%" + date + "%'  AND COMPTABILISE!='ANNULE' ";
                    s.execute(createString1);
                } else {
                    s = conn.createStatement();
                    String createString1 = " update APP.invoice set document=" + doc + " where APP.invoice.num_client= '" + client + "' and  APP.INVOICE.DATE LIKE '%" + date.substring(3, 6) + "%' AND COMPTABILISE!='ANNULE'  ";
                    s.execute(createString1);
                }

            }
        } catch (SQLException ex) {
            db.insertError(ex + "", " insertCompta");
        }
    }

    public String getCompta() {
        String nAff = (String) JOptionPane.showInputDialog(null, l(lang, "V_CHOICE"), "Comptabiliser les factures",
                JOptionPane.QUESTION_MESSAGE, null, new String[]{"La Journée (les Cash)", "Le Mois (Les crédits)", "Voir"}, nom);

        double somme = 0;
        try {
            if (nAff.equals("La Journée (les Cash)")) {
                compteL = new LinkedList();
                int doc = getMaxCompta();
                String s1 = JOptionPane.showInputDialog(null, " Inscrivez  la date 311211 ");
                String d = s1;
                if ((d.length() == 6)) {
                    if (notCompta(d, "CASHNORM")) {
                        for (int i = 0; i < control.length; i++) {

                            String cli = control[i];

                            String sql = "";

                            if (cli != null && cli.equals("RAMA")) {
                                sql = "_RAMA";
                            }
                            if (cli != null && cli.equals("CASHREMISE")) {
                                sql = "_UNIPHARMA";
                            }

                            if (!(cli.equals("CASHNORM"))) {
                                Compta compta = new Compta("" + (doc + 1), "Ventes médicaments", cli, 536000, 0, 0, d, "Client " + cli, 0, 0, 0, 0, 0, 0, 0, 0, 0);
                                compteL.add(compta);

                            }
                        }
                        String cli = "CASHNORM";
                        outResult = s.executeQuery("select SUM(total),sum(tva),sum(TOTAL_A),SUM(TOTAL_B),sum(TOTAL_C),SUM(TOTAL_D),sum(TAXE_A),SUM(TAXE_B),sum(TAXE_C),SUM(TAXE_D) from  APP.INVOICE WHERE APP.INVOICE.NUM_CLIENT='" + cli + "' and  APP.INVOICE.DATE LIKE '%" + s1 + "%'");
                        while (outResult.next()) {
                            double sum = outResult.getDouble(1);
                            double tva = outResult.getDouble(2);
                            double tota = outResult.getDouble(3);
                            double totb = outResult.getDouble(4);
                            double totc = outResult.getDouble(5);
                            double totd = outResult.getDouble(6);
                            double taxa = outResult.getDouble(7);
                            double taxb = outResult.getDouble(8);
                            double taxc = outResult.getDouble(9);
                            double taxd = outResult.getDouble(10);

                            if (sum != 0) {
                                somme = somme + sum;
                                compteL.add(new Compta("FU0090" + (doc + 1), "Ventes Comptoir", cli, 536000, sum, 0, d, cli, tva, tota, totb, totc, totd, taxa, taxb, taxc, taxd));
//insertCompta((doc+1),d,cli,sum,0,cli,411000,""+new Date(),tva);
                            }
                        }

                        //  Close the resultSet
                        outResult.close();

                    } else {
                        JOptionPane.showMessageDialog(null, " Izi Facture zabaye Comptabilize ", " DATE " + s1, JOptionPane.PLAIN_MESSAGE);
                    }
                } else {
                    JOptionPane.showMessageDialog(null, " Uburebure bwumunsi ni 6  => 311211 ", " DATE " + s1, JOptionPane.PLAIN_MESSAGE);
                }

            }
            if (nAff.equals("Le Mois (Les crédits)")) {

                String Debut = JOptionPane.showInputDialog(l(lang, "V_DATEDEBUT"), db.startTime());
                String FIN = JOptionPane.showInputDialog(l(lang, "V_DATEFIN"), db.EndTime());

                String s1 = JOptionPane.showInputDialog(null, " Inscrivez le mois: 0109 ");
                int doc = getMaxCompta() + 1;
                String d = s1;
                if ((d.length() == 4)) {
                    d = getMois(d.substring(0, 2)) + d;
                    if (notCompta(d, "RAMA")) {
                        compteL = new LinkedList();
                        for (int i = 0; i < control.length; i++) {

                            String cli = control[i];
                            String sql = "";
                            if (cli != null && cli.equals("RAMA")) {
                                sql = "_RAMA";
                            }
                            if (cli != null && cli.equals("CASHREMISE")) {
                                sql = "_UNIPHARMA";
                            }

                            Compta cpta = new Compta("" + doc, " Ventes", cli, 40, 0, 0, d, cli, 0, 0, 0, 0, 0, 0, 0, 0, 0);
                            cpta = amafarangaCompta(cpta, sql, cli, Debut, FIN);
                            if (cpta.valeur > 0) {
                                System.out.println("CLI:" + cli + ":CRE:" + cpta.montant_credit + ":cash:" + cpta.montant_cash);

                                insertCompta(doc, d, cli, cpta.montant_cash, cpta.montant_credit, cli, 0, " CPT AUTO ISHYIGA Du " + Debut + " au " + FIN, cpta.tva, cpta.valeur, cpta.TOTAL_A, cpta.TOTAL_B, cpta.TOTAL_C, cpta.TOTAL_D, cpta.TAXE_A, cpta.TAXE_B, cpta.TAXE_C, cpta.TAXE_D);
                                doc = doc + 1;
                            }
                        }
                    } else {
                        JOptionPane.showMessageDialog(null, " Izi Facture zabaye Comptabilize ", " DATE " + s1, JOptionPane.PLAIN_MESSAGE);
                    }

                } else {
                    JOptionPane.showMessageDialog(null, " Uburebure bwukezi ni 4  => 1211 ", " DATE " + s1, JOptionPane.PLAIN_MESSAGE);
                }

            }
            if (nAff.equals("Voir")) {
                String s1 = JOptionPane.showInputDialog(null, " Inscrivez  la date 311211 ");
                compteL = new LinkedList();
                outResult = s.executeQuery("select * from  APP.compta WHERE DATE='" + s1 + "'");
                System.out.append("select * from  APP.compta WHERE DATE='" + s1 + "'");
                while (outResult.next()) {
                    String factur = "FU014" + outResult.getInt(1);
                    int compte = outResult.getInt(3);
                    String cli = outResult.getString(4);
                    String saisi = outResult.getString(5);
                    double cash = outResult.getDouble("MONTANT_CASH");
                    double cre = outResult.getDouble("MONTANT_CREDIT");
                    double tva = outResult.getDouble("TVA");
                    double tota = outResult.getDouble("TOTAL_A");
                    double totb = outResult.getDouble("TOTAL_B");
                    double totc = outResult.getDouble("TOTAL_C");
                    double totd = outResult.getDouble("TOTAL_D");
                    double taxa = outResult.getDouble("TAXE_A");
                    double taxb = outResult.getDouble("TAXE_B");
                    double taxc = outResult.getDouble("TAXE_C");
                    double taxd = outResult.getDouble("TAXE_D");
                    somme = somme + cash;
                    compteL.add(new Compta(factur, saisi, cli, compte, cash, cre, s1, cli, tva, tota, totb, totc, totd, taxa, taxb, taxc, taxd));
                }

                //  Close the resultSet
                outResult.close();
            }
        } catch (Throwable e) {
            /*       Catch all exceptions and pass them to
             **       the exception reporting method             */
            db.insertError(e + "", " Insert Compta");
        }
        String comp = "Cash : " + somme;//Debit : "+setVirgule(debitCompta)+"  Credit : "+setVirgule(creditCompta)+"  Tot : "+setVirgule(totalCompta);
        return comp;
    }

    public Compta amafarangaCompta(Compta compta, String sql, String cli, String Debut, String FIN) {
        try {

            String sqla = "select SUM(payed),SUM(SOLDE), sum( (tva*client" + sql + ".PERCENTAGE)/100),sum(TOTAL_A),SUM(TOTAL_B),sum(TOTAL_C),SUM(TOTAL_D),sum(TAXE_A),SUM(TAXE_B),sum(TAXE_C),SUM(TAXE_D) from  APP.CREDIT,APP.INVOICE,client" + sql + " WHERE APP.INVOICE.NUM_CLIENT='" + cli + "' and  invoice.HEURE >'" + Debut + "' AND invoice.HEURE <'" + FIN + " ' and   CREDIT.ID_INVOICE =INVOICE.ID_INVOICE AND COMPTABILISE!='ANNULE'  and credit.NUMERO_AFFILIE=client" + sql + ".NUM_AFFILIATION";

            if (cli.equals("CASHNORM")) {
                sqla = "select SUM(TOTAL),0, sum(tva),sum(TOTAL_A),SUM(TOTAL_B),sum(TOTAL_C),SUM(TOTAL_D),sum(TAXE_A),SUM(TAXE_B),sum(TAXE_C),SUM(TAXE_D) from  APP.INVOICE  WHERE APP.INVOICE.NUM_CLIENT='" + cli + "' and  invoice.HEURE >'" + Debut + "' AND invoice.HEURE <'" + FIN + "' AND COMPTABILISE!='ANNULE' ";
            }

            System.out.println(sqla);
            outResult = s.executeQuery(sqla);

            while (outResult.next()) {
                double sum = outResult.getDouble(1);
                double tva = outResult.getDouble(3);
                double solde = outResult.getDouble(2);

                if (sum != 0 || solde != 0) {
                    compta.montant_credit = solde;
                    compta.montant_cash = sum;
                    compta.tva = tva;
                    compta.TOTAL_A = outResult.getDouble(4);
                    compta.TOTAL_B = outResult.getDouble(5);
                    compta.TOTAL_C = outResult.getDouble(6);
                    compta.TOTAL_D = outResult.getDouble(7);
                    compta.TAXE_A = outResult.getDouble(8);
                    compta.TAXE_B = outResult.getDouble(9);
                    compta.TAXE_C = outResult.getDouble(10);
                    compta.TAXE_D = outResult.getDouble(11);
                }
            }

            outResult = s.executeQuery("select sum(quantite*list.prix_revient)  from product,APP.INVOICE,APP.LIST where  APP.INVOICE.NUM_CLIENT='" + cli + "' and  invoice.HEURE >'" + Debut + "' AND invoice.HEURE <'" + FIN + " ' and  list.id_invoice=invoice.id_invoice  and code=code_uni AND COMPTABILISE!='ANNULE' ");
            System.out.println("select sum(quantite*list.prix_revient)  from product,APP.INVOICE,APP.LIST where  APP.INVOICE.NUM_CLIENT='" + cli + "' and  invoice.HEURE >'" + Debut + "' AND invoice.HEURE <'" + FIN + " ' and  list.id_invoice=invoice.id_invoice  and code=code_uni AND COMPTABILISE!='ANNULE' ");

            while (outResult.next()) {
                double val = outResult.getDouble(1);
                if (val != 0) {
                    compta.valeur = val;
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(VoirFichier.class.getName()).log(Level.SEVERE, null, ex);
        }
        return compta;
    }

    public void perime() {
        perim.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                if (ae.getSource() == perim) {
                    if (umukozi.LEVEL_EMPLOYE >= 7 || umukozi.PWD_EMPLOYE.contains("EMPLOYE")) {
                        int n = JOptionPane.showConfirmDialog(null, l(lang, "V_MUSHYA") + " = YES,   " + l(lang, "V_GUKOSORA") + " = NON", l(lang, "V_CHOICE"), JOptionPane.YES_NO_OPTION);
                        if (n == 0) {
                            new EmployeInterface(db);
                        } else {
                            db.emp();
                            Employe nom = (Employe) JOptionPane.showInputDialog(null, l(lang, "V_CHOICE"), l(lang, "V_NOM"),
                                    JOptionPane.QUESTION_MESSAGE, null, db.allEmploye.toArray(), "");

//                       String id  = (JOptionPane.showInputDialog(null, l(lang, "V_NUMERO")));
//                       Employe e= db.getName(Integer.parseInt(id));
                            new EmployeInterface(db, nom);
                        }
                    } else {
                        JOptionPane.showConfirmDialog(null, l(lang, "V_AUTORISATION"), l(lang, "V_PERMISSION"), JOptionPane.YES_NO_OPTION);
                    }
                }
            }
        });
    }

    public void perime(String s1) {
        int dateex = 0;
        try {
            try {
                Double in_d = new Double(s1);
                dateex = in_d.intValue();

            } catch (Throwable ex) {
                JOptionPane.showMessageDialog(null, l(lang, "V_IKIBAZO") + " " + l(lang, "V_DANS") + " " + l(lang, "V_INYANDIKO"), l(lang, "V_DATE") + " " + s1, JOptionPane.PLAIN_MESSAGE);
            }
            lotL = new LinkedList();
            outResult = s.executeQuery("select app.lot.id_product,name_product,id_LotS,date_exp,qty_start,qty_live,bon_livraison,code,id_lot from APP.lot ,APP.PRODUCT where app.lot.id_product = app.product.id_product order by name_product");

            while (outResult.next()) {
                int productCode = outResult.getInt(1);
                String name = outResult.getString(2);
                String id_LotS = outResult.getString(3);
                String datedoc = outResult.getString(4);
                int qtyStart = outResult.getInt(5);
                int qtyLive = outResult.getInt(6);
                String bon_livraison = outResult.getString(7);
                String code = outResult.getString(8);
                int dateDoc = outResult.getInt(9);

                String dd = "";
                if (datedoc.length() == 6) {
                    try {

                        dd = "" + datedoc.substring(4) + "" + datedoc.substring(2, 4) + "" + datedoc.substring(0, 2);

                        Double in_d = new Double(dd);
                        int df = in_d.intValue();
                        if (df <= dateex) {
                            lotL.add(new Lot(productCode, name, code, id_LotS, datedoc, dateDoc, qtyStart, qtyLive, bon_livraison));
                        }

                    } catch (Throwable ex) {
                        JOptionPane.showMessageDialog(null, ex + l(lang,"V_IKIBAZO") + l(lang,"V_DANS") + l(lang,"V_INYANDIKO"), dd + "  " + datedoc, JOptionPane.PLAIN_MESSAGE);
                    }
                }

            }
            //  Close the resultSet
            outResult.close();
        } catch (Throwable e) {
            /*       Catch all exceptions and pass them to
             **       the exception reporting method             */
            System.out.println(" . .lot . exception thrown:" + e);
            db.insertError(e + "", " V perime");
        }

    }

    public void credit() {
        creditButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                if (ae.getSource() == creditButton) {
                    if (umukozi.PWD_EMPLOYE.contains("CREDIT")) {

                        String d = JOptionPane.showInputDialog(null, " Date des Operations jour: 091211 mois:1211 ");
                        if (d.length() == 4) {
                            ((Assurance) societeJList.getSelectedValue()).date = getMois(d.substring(0, 2)) + d;
                        } else if (d.length() == 6) {
                            ((Assurance) societeJList.getSelectedValue()).date = d;
                        } else if (d.length() == 2) {
                            ((Assurance) societeJList.getSelectedValue()).date = "3112" + d;
                        }

                        String nAff = (String) JOptionPane.showInputDialog(null, l(lang,"V_MAKECHOICE"), " ",
                                JOptionPane.QUESTION_MESSAGE, null, new String[]{"GROUPE", "INDIVIDU", "EXPORTER", "MMI&RAMA"}, nom);
                        if (nAff.equals("INDIVIDU")) {
                            doOneCredit((((Assurance) societeJList.getSelectedValue()).sigle), db.s);
//                JTable jjj=creditV(); 
//                new showJTable(jjj,credit," "+ ((Assurance)societeJList.getSelectedValue()).sigle+"    SOLDE : "+setVirgule((int)soldeClient)+"    Cash : "+setVirgule((int)CashClient),(Assurance)societeJList.getSelectedValue(),server,dbName);
//                
                        } else if (nAff.equals("EXPORTER")) {
                            getRaportAss((((Assurance) societeJList.getSelectedValue()).sigle));
                        } else if (nAff.equals("MMI&RAMA")) {
                            LinkedList<Lot> lol = makePrint2(d);
                            new showJTable(speciale2(lol), "", server, dbName);
                        } else if (((Assurance) societeJList.getSelectedValue()).sigle.equals("CASHNORM")) {
                            getInvoiceView(d);
                            JTable jjj = creditV();
                            if (credit.size() != 0) {
                                new showJTable(jjj, credit, " " + ((Assurance) societeJList.getSelectedValue()).sigle + " - " + ((Assurance) societeJList.getSelectedValue()).facture + "    SOLDE : " + setVirgule((int) soldeClient) + "    Cash : " + setVirgule((int) CashClient), (Assurance) societeJList.getSelectedValue(), server, dbName);
                            } else {
                                JOptionPane.showMessageDialog(null, "Pas des ventes au " + d);
                            }
                        } else {
                            getCredit(d,"");
                            JTable jjj = null;
                            if (credit.size() != 0) {
                                jjj = creditV();
                            }
                            if (lotList.size() != 0) {
                                jjj = creditVList();
                            }

                            if (credit.size() != 0) {
                                new showJTable(jjj, credit, " " + ((Assurance) societeJList.getSelectedValue()).sigle + "    SOLDE : " + setVirgule((int) soldeClient) + "    Cash : " + setVirgule((int) CashClient), (Assurance) societeJList.getSelectedValue(), server, dbName);
                            } else if (lotList.size() != 0) {
                                new showJTable(jjj, credit, " " + ((Assurance) societeJList.getSelectedValue()).sigle + "    SOLDE : " + setVirgule((int) soldeClient) + "    Cash : " + setVirgule((int) CashClient), (Assurance) societeJList.getSelectedValue(), server, dbName);
                            } else {
                                JOptionPane.showMessageDialog(null, "Pas des ventes au " + d);
                            }
                        }
                    } else {
                        JOptionPane.showConfirmDialog(null, l(lang, "V_AUTORISATION"), l(lang, "V_PERMISSION"), JOptionPane.YES_NO_OPTION);
                    }

                }
            }
        });
    }

    private JTable creditVList() {
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable = new javax.swing.JTable();
        int linge = lotList.size();
        System.out.println("nyirimo");
        String[][] s1 = new String[linge + 1][12];

        s1[0][0] = "ID_INVOICE";
        s1[0][1] = "DATE";
        s1[0][2] = "QUITTANCE";
        s1[0][3] = "N AFFILIATION";
        s1[0][4] = "NOM ET PRENOM";
        s1[0][5] = "DESIGNATION";
        s1[0][6] = "QUANTITE";
        s1[0][7] = "PRIX";
        s1[0][8] = "TOTAL";
        s1[0][9] = " ";
        s1[0][10] = " ";
        s1[0][11] = " ";

        for (int i = 1; i <= lotList.size(); i++) {
            Lot C = lotList.get(i - 1);
            s1[i][0] = C.u1;
            s1[i][1] = C.u2;
            s1[i][2] = C.u3;
            s1[i][3] = C.u4;
            s1[i][4] = C.u5;
            s1[i][5] = C.u6;
            s1[i][6] = C.u7;
            s1[i][7] = C.u8;
            s1[i][8] = C.u9;
            s1[i][9] = C.u0;
            s1[i][10] = C.u11;

        }
        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jTable.setModel(new javax.swing.table.DefaultTableModel(s1,
                new String[]{
                    "L1", "L2", "L3", "L4", "L5", "L6", "L7", "L8", "L9", "L10", "L11"
                }
        ) {
            Class[] types = new Class[]{
                java.lang.String.class,
                java.lang.String.class,
                java.lang.String.class,
                java.lang.String.class,
                java.lang.String.class,
                java.lang.String.class,
                java.lang.String.class,
                java.lang.String.class,
                java.lang.String.class,
                java.lang.String.class,
                java.lang.String.class
            };

            public Class getColumnClass(int columnIndex) {
                return types[columnIndex];
            }
        });

        return jTable;
    }// </editor-fold>

    String setVirgule(int frw) {

        String setString = "" + frw;
        int l = setString.length();
        if (l < 2) {
            setString = "    " + frw;
        } else if (l < 3) {
            setString = "  " + frw;
        } else if (l < 4) {
            setString = "  " + frw;
        }

        if (l > 3 && l <= 6) {
            setString = setString.substring(0, l - 3) + "." + setString.substring(l - 3);
        }
        if (l > 6) {

            String s1 = setString.substring(0, l - 3);
            int sl = s1.length();
            setString = s1.substring(0, sl - 3) + "." + s1.substring(sl - 3) + "." + setString.substring(l - 3);

        }

        return setString;
    }

    public void getList1() {
        try {
            String Debut = JOptionPane.showInputDialog(null, l(lang, "V_IDINVOICE") + " " + l(lang, "V_DEBUT"));
            String Fin = JOptionPane.showInputDialog(null, l(lang, "V_IDINVOICE") + " " + l(lang, "V_FIN"));

            list = new LinkedList();
            outResult = s.executeQuery("select list.id_invoice,list.code_uni,list.num_lot,list.quantite,list.price,list.prix_revient,product.name_product from APP.LIST,APP.PRODUCT where id_invoice<" + Fin + " and id_invoice>" + Debut + " AND LIST.CODE_UNI=PRODUCT.CODE ORDER BY ID_INVOICE");
            System.out.println("select list.id_invoice,list.code_uni,list.num_lot,list.quantite,list.price,list.prix_revient,product.name_product from APP.LIST,APP.PRODUCT where id_invoice<" + Fin + " and id_invoice>" + Debut + " AND LIST.CODE_UNI=PRODUCT.CODE ORDER BY ID_INVOICE");

            int i = 0;
            while (outResult.next()) {

                list.add(new List(outResult.getInt(1), outResult.getInt(5), outResult.getInt(4), outResult.getString(3), outResult.getString(2), outResult.getString(7), outResult.getDouble(6)));

                i++;
            }
        } catch (Throwable e) {
            db.insertError(e + "", " V getlist");
        }

    }

    public void getList2(int Debut, int Fin) {
        try {

            list = new LinkedList();
            outResult = s.executeQuery("select * from APP.list,product where app.LIST.CODE_UNI=app.PRODUCT.CODE  and id_invoice<" + Fin + " and id_invoice>" + Debut + "  ORDER BY ID_INVOICE");

            int i = 0;
            while (outResult.next()) {

                list.add(new List(outResult.getInt(1), outResult.getInt(5), outResult.getInt(4), outResult.getString(3), outResult.getString(2), outResult.getString(7), outResult.getDouble(23)));

                i++;
            }
        } catch (Throwable e) {
            db.insertError(e + "", " getlist pour Rappport");
        }

    }

    int days(String m, int mult) {
        int days = 0;
        if (m.equals("01")) {
            days = (mult * 334) + 0;
        }
        if (m.equals("02")) {
            days = (mult * 334) + 31;
        }
        if (m.equals("03")) {
            days = (mult * 334) + 59;
        }
        if (m.equals("04")) {
            days = (mult * 334) + 90;
        }
        if (m.equals("05")) {
            days = (mult * 334) + 120;
        }
        if (m.equals("06")) {
            days = (mult * 334) + 151;
        }
        if (m.equals("07")) {
            days = (mult * 334) + 181;
        }
        if (m.equals("08")) {
            days = (mult * 334) + 212;
        }
        if (m.equals("09")) {
            days = (mult * 334) + 243;
        }
        if (m.equals("10")) {
            days = (mult * 334) + 273;
        }
        if (m.equals("11")) {
            days = (mult * 334) + 304;
        }
        if (m.equals("12")) {
            days = 334;
        }
        return days;
    }

//public void getRaport()
//{
//    try
//    {
//        String  Debut = JOptionPane.showInputDialog(l(lang,  "V_DATEDEBUT"),db.getString("debut"));
//        String FIN = JOptionPane.showInputDialog(l(lang,  "V_DATEFIN"),db.getString("fin"));
//        
//        YoseNtatva=0;
//        tvaYose=0;
//        Yose=0;
//        
//        if( umukozi.LEVEL_EMPLOYE>=4 || umukozi.PWD_EMPLOYE.contains("rapport"))
//        {
//            listlot = new LinkedList();
//            //System.out.println("select INVOICE.id_INVOICE,date,code,name_product,num_lot,quantite,list.price,list.prix_revient,employe,heure,num_client  from product,APP.INVOICE,APP.LIST where code=code_uni and  list.id_invoice=invoice.id_invoice  and invoice.HEURE >'"+Debut+"' AND invoice.HEURE <'"+FIN+"' ORDER BY invoice.ID_INVOICE");
//            String nAff  = (String)JOptionPane.showInputDialog(null, "TRIER", " ",
//                    JOptionPane.QUESTION_MESSAGE, null, new String [] { "ID","DESIGNATION" }, nom);
//            
//            String sql="";
//            if(nAff.equals("DESIGNATION"))
//            {
//                sql="ORDER BY name_product ";
//            }
//            else if(nAff.equals("ID"))
//            {
//                sql="ORDER BY INVOICE.id_INVOICE";
//                
//            }
//            
//            System.out.println("select INVOICE.id_INVOICE,date,code,name_product,num_lot,quantite,list.price,list.prix_revient,employe,heure,num_client,product.tva  from product,APP.INVOICE,APP.LIST where code=code_uni and  list.id_invoice=invoice.id_invoice  and invoice.HEURE >'"+Debut+"' AND invoice.HEURE <'"+FIN+"' "+sql);
//            outResult = s.executeQuery("select INVOICE.id_INVOICE,date,code,name_product,num_lot,quantite,list.price,list.prix_revient,employe,heure,num_client,product.tva  from product,APP.INVOICE,APP.LIST where code=code_uni and  list.id_invoice=invoice.id_invoice  and invoice.HEURE >'"+Debut+"' AND invoice.HEURE <'"+FIN+"' "+sql);
//            System.out.println("select INVOICE.id_INVOICE,date,code,name_product,num_lot,quantite,list.price,list.prix_revient,employe,heure,num_client,product.tva  from product,APP.INVOICE,APP.LIST where code=code_uni and  list.id_invoice=invoice.id_invoice  and invoice.HEURE >'"+Debut+"' AND invoice.HEURE <'"+FIN+"' "+sql);
//            
//            int i=0;
//            while (outResult.next())
//            {
//                
//                listlot.add(new Lot(
//                        ""+outResult.getInt(1),
//                        outResult.getString(2),
//                        outResult.getString(3),
//                        outResult.getString(4),
//                        outResult.getString(5),
//                        ""+outResult.getInt(6),
//                        ""+((int)(outResult.getDouble(7)*10000))/10000.0,
//                        ""+((int)(outResult.getDouble(8)*10000))/10000.0,
//                        outResult.getString(9),
//                        outResult.getString(10),
//                        outResult.getString(11),
//                        ""+outResult.getDouble(12),"","",""));
//                
//                if(  umukozi.PWD_EMPLOYE.contains("boss"))
//                {
//                    YoseNtatva=YoseNtatva+((int)(outResult.getDouble(7)*outResult.getInt(6)));
//                    tvaYose=tvaYose+((int)(outResult.getDouble(8)*outResult.getInt(6)));
//                    Yose=Yose+((int)((outResult.getDouble(7)-outResult.getDouble(8))*outResult.getInt(6)));                    
//                } 
//                i++;
//            }
//        }
//    }
//    catch(Throwable e)
//    {
//        db.insertError(e+""," V getlist");
//    } 
//}
//public void getRaport()
//{
// try
//{
//// String  Debut = JOptionPane.showInputDialog(l(lang,  "V_DATEDEBUT"),db.getString("debut"));
//// String FIN = JOptionPane.showInputDialog(l(lang,  "V_DATEFIN"),db.getString("fin"));
//
// YoseNtatva=0;
//    tvaYose=0;
//    Yose=0;
//    yosePayed=0;
//    yoseSolde=0;
//if( umukozi.LEVEL_EMPLOYE>=4 || umukozi.PWD_EMPLOYE.contains("rapport"))
//{
//listlot = new LinkedList();
////System.out.println("select INVOICE.id_INVOICE,date,code,name_product,num_lot,quantite,list.price,list.prix_revient,employe,heure,num_client  from product,APP.INVOICE,APP.LIST where code=code_uni and  list.id_invoice=invoice.id_invoice  and invoice.HEURE >'"+Debut+"' AND invoice.HEURE <'"+FIN+"' ORDER BY invoice.ID_INVOICE");
// String nAff  = (String)JOptionPane.showInputDialog(null,l(lang,  "V_CHOICE"), " ",
//    JOptionPane.QUESTION_MESSAGE, null, new String [] { "FACTURE","TOUT LES VENTES" },nom);
////
////String sql="";
// 
// if(nAff == null)
// {     
// }
// else if(nAff.equals("FACTURE"))
// {
//     ch=nAff;
//     importerVentes();
// }
// else if(nAff.equals("TOUT LES VENTES"))
// {
//     ch=nAff;
//     importer();     
// }
//   
////if(nAff.equals("ID"))
//
////else
////{ 
////    if(nAff.equals("DESIGNATION"))
////    { 
////        String NOM = JOptionPane.showInputDialog("CODE DU PRODUIT ","");        
////        LinkedList <Lot>tout=new LinkedList();
////        tout=importer();
////        listlot=new LinkedList();    
////        for(int i=0;i<tout.size();i++)
////        {  
////            if(!(listlot.get(i).u3.equals(NOM)))
////            { 
////                listlot.remove(i);
////            }
////        }
////    }
////    else if(nAff.equals("ID"))
////    {
////        sql="ORDER BY INVOICE.id_INVOICE";
////        
////    }
////outResult = s.executeQuery("select INVOICE.id_INVOICE,date,code,name_product,num_lot,quantite,list.price,list.prix_revient,employe,heure,num_client,product.tva  from product,APP.INVOICE,APP.LIST where code=code_uni and  list.id_invoice=invoice.id_invoice  and invoice.HEURE >'"+Debut+"' AND invoice.HEURE <'"+FIN+"' "+sql);
////System.out.println("select INVOICE.id_INVOICE,date,code,name_product,num_lot,quantite,list.price,list.prix_revient,employe,heure,num_client,product.tva  from product,APP.INVOICE,APP.LIST where code=code_uni and  list.id_invoice=invoice.id_invoice  and invoice.HEURE >'"+Debut+"' AND invoice.HEURE <'"+FIN+"' "+sql);
////   
////int i=0;
////        while (outResult.next())
////        {
////
////listlot.add(new Lot(
////        ""+outResult.getInt(1),
////        outResult.getString(2),
////        outResult.getString(3),
////        outResult.getString(4),
////        outResult.getString(5),
////        ""+outResult.getInt(6),
////        ""+((int)(outResult.getDouble(7)*10000))/10000.0,
////        ""+((int)(outResult.getDouble(8)*10000))/10000.0,
////        outResult.getString(9),
////        outResult.getString(10),
////       outResult.getString(11),
////       ""+outResult.getDouble(12),"","",""
////        )
////
////        );
////if(  umukozi.PWD_EMPLOYE.contains("boss"))
////{
////YoseNtatva=YoseNtatva+((int)(outResult.getDouble(7)*outResult.getInt(6)));
////tvaYose=tvaYose+((int)(outResult.getDouble(8)*outResult.getInt(6)));
////Yose=Yose+((int)((outResult.getDouble(7)-outResult.getDouble(8))*outResult.getInt(6)));   
////} 
////i++;
////        }
//
////}//CLOSE ELSE
//
//}}
//        catch(Throwable e)
//        {db.insertError(e+""," V getlist");} 
//}
    public void raportt() {
        rapportButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                if (ae.getSource() == rapportButton) {
                    boolean kabiri = false;
                    if (umukozi.PWD_EMPLOYE.contains("RAPPORT")) {
                        resByose = getRaport();
                        if (choixRpt.equals("CRM")) {

                            System.out.println("size:" + listlot.size());

                            jTable = new javax.swing.JTable();

                            String tit[] = new String[]{
                                "NUMERO_AFFILIE", "NOM_CLIENT", l(lang,"V_PRENOMCLIENT"), l(lang,"V_ASSURANCE"), l(lang,"V_EMPLOYE"),
                                "TELEFONE", "SEXE", "TOTAL"};

                            int size = listlot.size();
                            String s1[][] = new String[size + 1][tit.length];

                            s1[0] = tit;

                            for (int j = 0; j < size; j++) {

                                Lot list = listlot.get(j);

                                s1[j + 1][0] = list.u1;
                                s1[j + 1][1] = list.u2;
                                s1[j + 1][2] = list.u3;
                                s1[j + 1][3] = list.u4;
                                s1[j + 1][4] = list.u5;
                                s1[j + 1][5] = list.u6;
                                s1[j + 1][6] = list.u7;
                                s1[j + 1][7] = list.u8;

                            }

                            jTable.setModel(new javax.swing.table.DefaultTableModel(s1, tit));
                            jTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
                            jTable.getColumnModel().getColumn(1).setPreferredWidth(200);//.setWidth(200);
                            jTable.doLayout();
                            jTable.validate();

//totalTax=(int)(totalTax/1+0.18);
                            new showJTable(jTable, " Client Record Management ", server, dbName);

                            if (kabiri) {
                                tosend = tosend.replaceAll("&", " AND ");
                                tosend = tosend.replaceAll("'", "`");
                                System.out.println(tosend);
                                db.insertTracking(nom, "UPLOAD SALES BETWEEN " + resByose.getFirst().ID_INVOICE + " TO " + resByose.getLast().ID_INVOICE, size, "ISHYIGA");

                                CommCloud product_Cloud = new CommCloud("http://www.ishyiga" + db.getString("extension") + "/test_cloud/upload_sales.php?soc=" + db.dbName + "&user=" + umukozi.NOM_EMPLOYE, "stock=" + tosend);
                                //String response=product_Cloud.sendlot; 
                                db.insertTracking(nom, "RESULT OF SALES UPLOADED FROM " + resByose.getFirst().ID_INVOICE + " TO " + resByose.getLast().ID_INVOICE + ": " + product_Cloud.sendlot, size, "CLOUD");

//            db.insertTracking(nom, "RESULT YA UPLOAD SALES ", ligne, "CLOUD", received, last,dd, Integer.parseInt(line));
                                if (product_Cloud.sendlot.contains("DATABASE UPDATED")) {
                                    JOptionPane.showMessageDialog(null, " SALES UPLOADED SUCCESSFULLY");
                                } else {
                                    JOptionPane.showMessageDialog(null, " SALES NOT UPLOADED", " MESSAGE ", JOptionPane.WARNING_MESSAGE);
                                }
                            }

                        }
                        if (choixRpt.equals(l(lang,"V_TOUS_VENTE"))) {//TOUTES LES VENTES
                            System.out.println("size:" + resByose.size());
                            JOptionPane.showMessageDialog(null, RadioButton(), "OPTION", JOptionPane.INFORMATION_MESSAGE);

                            int size = resByose.size();
                            jTable = new javax.swing.JTable();
                            String s1[][] = null;
                            String tit[] = null;
                            System.err.println(IJISHOrEPORT);
                            soldeClient = 0;
                            CashClient = 0;
                            tvaYose = 0;
                            totalTax = 0;
                            String solde = "VALEUR VENTES ";
                            String cash = "VALEUR ACHATS ";
                            String tvaTotal = "VALEUR TVA ";
                            String valeurTaxable = "MONTANT TAXABLE ";
                            String inyungu = "INYUNGU ";
                            tosend = "";
                            if (IJISHOrEPORT.equals("CLASSIC")) {
                                tit = new String[]{
                                    "ID_INVOICE", "DATE", "CODE", "DESIGNATION", l(lang,"V_LOT"),
                                    l(lang,"V_QUANTITE"), l(lang,"V_PRICEUNITY"), l(lang,"V_PRIXR"), l(lang,"V_PRIXTOT"), l(lang,"V_TVA"), l(lang,"V_EMPLOYE"),
                                    l(lang,"V_ISAHA"), l(lang,"V_SOCIETE")
                                };
                            } else if (IJISHOrEPORT.equals("GROUPE")) {
                                tit = new String[]{
                                    "ID_INVOICE", "Matricule", "CODE MEDECIN", "NOM ET PRENOM ", "BENEFICIAIRE", "SERVICE",
                                    "MONTANT", "PAYED", " ", " "
                                };
                                solde = "SOLDE ";
                                cash = "PAYE ";
                            } else {
                                tit = new String[]{"ID_INVOICE", "DATE", "CODE", "DESIGNATION", l(lang,"V_LOT"),
                                    l(lang,"V_QUANTITE"), l(lang,"V_PRICEUNITY"), l(lang,"V_PRIXR"), l(lang,"V_PRIXTOT"), l(lang,"V_TVA"), l(lang,"V_EMPLOYE"),
                                    l(lang,"V_MATRICULE"), l(lang,"V_QUITTANCE"), l(lang,"V_NOMPRENOM"), l(lang,"V_BENEFICIAIRE"), l(lang,"V_POURCENTAGE"), l(lang,"V_ISAHA"), l(lang,"V_PRENOME")};
                            }
                            s1 = new String[size + 1][tit.length];
                            int currentBon = 0;
                            int res = 0;
                            kabiri = false;
                            tosend = "";
                            String kuzamura = "" + db.getString("kuzamura");

                            if (kuzamura.equals("YEGO") && umukozi.PWD_EMPLOYE.contains("SALES_UPLOAD")) {
                                int n = JOptionPane.showConfirmDialog(null, " DO YOU WANT TO UPLOAD IT?", "UPLOAD", JOptionPane.YES_NO_OPTION);

                                if (n == 0) {
                                    kabiri = true;
                                }

                            }

                            s1[0][0] = "ID_INVOICE";
                            s1[0][1] = "DATE";
                            s1[0][2] = "CODE";
                            s1[0][3] = "NAME_PRODUCT";
                            s1[0][4] = "NUM_LOT";
                            s1[0][5] = "QUANTITE";
                            s1[0][6] = "PRICE";
                            s1[0][7] = "PRICE_REVIENT";
                            s1[0][8] = "TOTAL";
                            s1[0][9] = "TVA";
                            s1[0][10] = "EMPLOYE";
                            s1[0][11] = "HEURE";
                            s1[0][12] = "ID_FRSS";
                            s1[0][11] = "NUM_AFFILIATION";
                            s1[0][12] = "NUMERO_QUITANCE";

                            if (IJISHOrEPORT.equals("GROUPE") || IJISHOrEPORT.equals("ALL")) {
                                s1[0][13] = "NOM_CLIENT";
                                s1[0][14] = "PRENOM_CLIENT";
                                s1[0][15] = "PERCENTAGE";
                                s1[0][16] = "HEURE";
                                s1[0][16] = "SOCIETE";
                            }

                            for (int j = 0; j < size; j++) {
                                Byose b = resByose.get(j);
//    if(!kabiri && j<size-1 && b.ID_INVOICE==resByose.get(j+1).ID_INVOICE && b.NUM_LOT.equals(resByose.get(j+1).NUM_LOT) && b.CODE.equals(resByose.get(j+1).CODE))
//    {
                                if (kabiri) {
                                    tosend = tosend + b.ID_INVOICE + ";" + b.DATE + ";" + b.NUM_AFFILIATION + ";" + b.NOM_CLIENT + ";" + b.PRENOM_CLIENT
                                            + ";" + b.PERCENTAGE + ";" + b.NUMERO_QUITANCE + ";" + b.ID_FRSS + ";" + b.CODE + ";" + b.NAME_PRODUCT + ";"
                                            + b.NUM_LOT.replaceAll(";", "") + ";" + b.QUANTITE + ";" + (b.PRICE * b.QUANTITE) + ";" + b.PRICE + ";" + ((int) (b.TVA * b.QUANTITE)) + ";" + b.PRICE_REVIENT + "#";
                                }
                                //    }

                                if (IJISHOrEPORT.equals("GROUPE")) {
                                    if (currentBon != b.ID_INVOICE) {
                                        s1[res + 1][0] = "" + b.ID_INVOICE;
                                        s1[res + 1][1] = b.NUM_AFFILIATION;
                                        s1[res + 1][2] = b.NUMERO_QUITANCE;
                                        s1[res + 1][3] = b.NOM_CLIENT;
                                        s1[res + 1][4] = b.PRENOM_CLIENT;
                                        s1[res + 1][5] = b.EMPLOYEUR;
                                        s1[res + 1][6] = "" + b.SOLDE;
                                        s1[res + 1][7] = "" + b.PAYED;
                                        soldeClient = soldeClient + b.SOLDE;
                                        CashClient = CashClient + b.PAYED;
                                        tvaYose = tvaYose + (b.TVA * b.QUANTITE);
                                        if (b.TVA > 0) {
                                            totalTax += (int) (b.PRICE * b.QUANTITE);
                                        }
                                        currentBon = b.ID_INVOICE;
                                        res++;
                                    }
                                }

                                if (IJISHOrEPORT.equals("CLASSIC") || IJISHOrEPORT.equals("GENERAL") || IJISHOrEPORT.equals("ALL")) {
                                    s1[j + 1][0] = "" + b.ID_INVOICE;
                                    s1[j + 1][1] = b.DATE;
                                    s1[j + 1][2] = b.CODE;
                                    s1[j + 1][3] = b.NAME_PRODUCT;
                                    s1[j + 1][4] = b.NUM_LOT;
                                    s1[j + 1][5] = "" + b.QUANTITE;
                                    s1[j + 1][6] = "" + b.PRICE;
                                    s1[j + 1][7] = "" + b.PRICE_REVIENT;
                                    s1[j + 1][8] = "" + (b.PRICE * b.QUANTITE);

                                    s1[j + 1][9] = "" + (int) (b.TVA * b.QUANTITE);
                                    s1[j + 1][10] = b.EMPLOYE;
                                    s1[j + 1][11] = b.HEURE;
                                    s1[j + 1][12] = b.ID_FRSS;
                                    soldeClient = soldeClient + (int) (b.PRICE * b.QUANTITE);
                                    CashClient = CashClient + (b.PRICE_REVIENT * b.QUANTITE);
                                    tvaYose = tvaYose + (int) (b.TVA * b.QUANTITE);

                                    if (b.TVA > 0) {
                                        totalTax += ((int) b.PRICE * b.QUANTITE);
                                    }
                                }

                                if (IJISHOrEPORT.equals("GENERAL") || IJISHOrEPORT.equals("ALL")) {
                                    s1[j + 1][11] = b.NUM_AFFILIATION;
                                    s1[j + 1][12] = b.NUMERO_QUITANCE;
                                    s1[j + 1][13] = b.NOM_CLIENT;
                                    s1[j + 1][14] = b.PRENOM_CLIENT;
                                    s1[j + 1][15] = "" + b.PERCENTAGE;
                                    s1[j + 1][16] = b.HEURE;
                                    s1[j + 1][17] = b.ID_FRSS;
                                }

                            }

                            jTable.setModel(new javax.swing.table.DefaultTableModel(s1, tit));
                            jTable.setAutoResizeMode(jTable.AUTO_RESIZE_ALL_COLUMNS);
                            jTable.getColumnModel().getColumn(3).setPreferredWidth(300);//.setWidth(200);
                            jTable.doLayout();
                            jTable.validate();

//totalTax=(int)(totalTax/1+0.18);
                            new showJTable(jTable, " Rapport " + solde + "=" + setVirgule((int) soldeClient) + "  " + cash + "="
                                    + setVirgule((int) CashClient) + "  " + tvaTotal + "=" + setVirgule((int) tvaYose) + "  " + valeurTaxable + "=" + setVirgule((int) (tvaYose / 0.18)) + " INYUNGU :" + setVirgule((int) (soldeClient - CashClient)), server, dbName);

                            if (kabiri) {
                                tosend = tosend.replaceAll("&", " AND ");
                                tosend = tosend.replaceAll("'", "`");
                                System.out.println(tosend);
                                db.insertTracking(nom, "UPLOAD SALES BETWEEN " + resByose.getFirst().ID_INVOICE + " TO " + resByose.getLast().ID_INVOICE, size, "ISHYIGA");

                                //SendLotCloud product_Cloud = new SendLotCloud("http://www.ishyiga" + db.getString("extension") + "/test_cloud/upload_sales.php?soc=" + db.dbName + "&user=" + umukozi.NOM_EMPLOYE, "stock=" + tosend);
                                //String response=product_Cloud.sendlot; 
                                //db.insertTracking(nom, "RESULT OF SALES UPLOADED FROM " + resByose.getFirst().ID_INVOICE + " TO " + resByose.getLast().ID_INVOICE + ": " + product_Cloud.sendlot, size, "CLOUD");
//            db.insertTracking(nom, "RESULT YA UPLOAD SALES ", ligne, "CLOUD", received, last,dd, Integer.parseInt(line));
//                                if (product_Cloud.sendlot.contains("DATABASE UPDATED")) {
//                                    JOptionPane.showMessageDialog(null, " SALES UPLOADED SUCCESSFULLY");
//                                } else {
//                                    JOptionPane.showMessageDialog(null, " SALES NOT UPLOADED", " MESSAGE ", JOptionPane.WARNING_MESSAGE);
//                                }
                            }

                        } else if (choixRpt.equals(l(lang,"V_FACTURE"))) { //FACTURE
                            IJISHOrEPORT = "NORMAL";
                            JOptionPane.showMessageDialog(null, RadioButton(), "OPTION", JOptionPane.INFORMATION_MESSAGE);
                            if (IJISHOrEPORT.equals("NORMAL")) {
                                int linge = listlot.size();
                                jScrollPane1 = new javax.swing.JScrollPane();
                                jTable = new javax.swing.JTable();
                                String[][] s1 = new String[1][1];
                                String[] title = new String[]{};
                                String header = "";

                                s1 = new String[linge + 1][10];
                                soldeClient = 0;
                                CashClient = 0;

                                s1[0][0] = "ID_INVOICE";
                                s1[0][1] = "CLIENT";
                                s1[0][2] = "DATE";
                                s1[0][3] = "EMPLOYE";
                                s1[0][4] = "SOLDE";
                                s1[0][5] = "PAYED";
                                s1[0][6] = "TOTAL";
                                s1[0][7] = "TVA";
                                s1[0][8] = "REDUCTION";
                                s1[0][9] = "HEURE";
                                for (int j = 1; j < s1.length; j++) {
                                    Lot li = listlot.get(j - 1);
                                    s1[j][0] = li.u1;
                                    s1[j][1] = li.u2;
                                    s1[j][2] = li.u3;
                                    s1[j][3] = li.u4;
                                    s1[j][4] = li.u5;
                                    s1[j][5] = li.u6;
                                    s1[j][6] = li.u7;
                                    s1[j][7] = li.u8;
                                    s1[j][8] = li.u9;
                                    s1[j][9] = li.u12;
                                }

                                title = new String[]{"ID_INVOICE", l(lang,"V_SOCIETE"), "DATE", l(lang,"V_EMPLOYE"), "SOLDE", "PAYED", "TOTAL", l(lang,"V_TVA"), "REDUCTION",l(lang,"V_ISAHA")};
                                header = " Raport TOTAL SOLDE : " + RRA_PRINT2.setVirgule(yoseSolde, 1, ",") + "   TOTAL PAYED : " + RRA_PRINT2.setVirgule(yosePayed, 1, ",") + " TOTAL TVA: " + RRA_PRINT2.setVirgule(tvaYose, 1, ",") + "   TOTAL GENERAL : " + RRA_PRINT2.setVirgule(Yose, 1, ",");

                                setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
                                jTable.setModel(new javax.swing.table.DefaultTableModel(s1, title) {
                                    Class[] types = new Class[]{
                                        java.lang.String.class,
                                        java.lang.String.class,
                                        java.lang.String.class,
                                        java.lang.String.class,
                                        java.lang.String.class,
                                        java.lang.String.class,
                                        java.lang.String.class,
                                        java.lang.String.class,
                                        java.lang.String.class,
                                        java.lang.String.class, java.lang.String.class,
                                        java.lang.String.class
                                    };

//                    public Class getColumnClass(int columnIndex) {
//                    return types [columnIndex];
//                    }
                                });
                                new showJTable(jTable, header, server, dbName);
                            } else if (IJISHOrEPORT.equals("RRA")) {
                                String separateur = (String) JOptionPane.showInputDialog(null, l(lang, "V_CHOICE"), l(lang,"V_SEPARATOR"),
                                        JOptionPane.QUESTION_MESSAGE, null, new String[]{"", ",", "."}, nom);
                                int linge = listlot.size();
                                jScrollPane1 = new javax.swing.JScrollPane();
                                jTable = new javax.swing.JTable();
                                String[][] s1 = new String[1][1];
                                String[] title = new String[]{};
                                String header = "";

                                s1 = new String[linge + 1][12];
                                soldeClient = 0;
                                CashClient = 0;
                                tvaYose = 0;
                                YoseNtatva = 0;

                                double yoseEx = 0;
                                double yoseExo = 0;

                                s1[0][0] = "TIN_NUMBER";
                                s1[0][1] = "NUMCLIENT(INVOICE)";
                                s1[0][2] = "NATURE OF GOODS";
                                s1[0][3] = "ID_INVOICE";
                                s1[0][4] = "DATE";
                                s1[0][5] = "Total Amount of Sales (VAT Exclusive)";
                                s1[0][6] = "Exempted Sales Amount/Montant Ventes Exonerées";
                                s1[0][7] = "Zero rated Sales Amount/Montant Ventes Taxées au Taux Zéro";
                                s1[0][8] = "Exports Amount/Montant des Exportations";
                                s1[0][9] = "Taxable Sales/Ventes Taxables";
                                s1[0][10] = "VAT/ TVA";
                                s1[0][11] = "Payed";

                                for (int j = 1; j < s1.length; j++) {
                                    Lot li = listlot.get(j - 1);
                                    s1[j][0] = "";
                                    s1[j][1] = "" + li.u2;
                                    s1[j][2] = "";
                                    s1[j][3] = "" + li.u1;
                                    s1[j][4] = "" + li.u3;
                                    s1[j][7] = "0";
                                    s1[j][8] = "0";
                                    s1[j][11] = li.u6;

//                    System.out.println("HANOOOOO:"+li.u8);
                                    if (li.u8.equals("0") || li.u8.equals("0.0")) {
                                        yoseEx += Double.parseDouble(li.u6);
                                        yoseExo += Double.parseDouble(li.u6);
//                        if(dbName.contains("CONSEIL"))
//                        { 
//                            s1[j][5]=li.u6;
//                            s1[j][6]=li.u6;                            
//                        }
//                        else
//                        {
                                        s1[j][5] = li.u7;
                                        s1[j][6] = li.u7;
//                        }
                                        s1[j][9] = "0";
                                        s1[j][10] = "0";
                                        s1[j][2] = "MEDICAMENTS";
                                    } else {
                                        double htv = 0;
                                        double tv = 0;
                                        double ex = 0;
                                        try {
                                            outResult = s.executeQuery("select price,product.tva,quantite from APP.PRODUCT,APP.LIST where ID_INVOICE=" + li.u1 + " AND app.LIST.CODE_UNI=app.PRODUCT.CODE ");

                                            while (outResult.next()) {
                                                double p = outResult.getDouble(1);
                                                double t = outResult.getDouble(2);
                                                int qty = outResult.getInt(3);

                                                if (t > 0) {
                                                    htv += ((p * qty) / (1 + t));
                                                } else {
                                                    ex += (p * qty);
                                                }

                                            }
                                            outResult.close();
                                        } catch (Throwable e) {
                                            /*       Catch all exceptions and pass them to
                                             **       the exception reporting method             */
                                            System.out.println(" . check Dock . exception thrown:");
                                            db.insertError(e + "", " V checkdoc");
                                        }

                                        double totale = Double.parseDouble(li.u7);
                                        double total = totale;
                                        tv = htv * 0.18;

                                        double tva = (tv);
                                        double htva = total - tva;

                                        yoseEx += htva;
                                        yoseExo += ex;
                                        s1[j][5] = "" + RRA_PRINT2.setVirgule(htva, 1, separateur);
                                        s1[j][6] = "" + RRA_PRINT2.setVirgule(ex, 1, separateur);
                                        s1[j][9] = "" + RRA_PRINT2.setVirgule((htv), 1, separateur);
                                        s1[j][10] = "" + RRA_PRINT2.setVirgule(tva, 1, separateur);
                                        s1[j][2] = "AUTRES PRODUITS";
                                        tvaYose += tva;
                                        YoseNtatva += (htv);
                                    }

//                    s1[j][0] = li.u1;   //"ID_INVOICE";
//                    s1[j][1] = li.u2;//"CLIENT";
//                    s1[j][2] = li.u3;//DATE";
//                    s1[j][3] = li.u4;//EMPLOYE";
//                    s1[j][4] = li.u5;//SOLDE";
//                    s1[j][5] = li.u6;//PAYED";
//                    s1[j][6] = li.u7;//TOTAL";
//                    s1[j][7] = li.u8;//TVA";
//                    s1[j][8] = li.u9;//REDUCTION";
//                    s1[j][9] = li.u12;//HEURE";
                                }

                                title = new String[]{"TIN_NUMBER", "NUMCLIENT(INVOICE)", "NATURE OF GOODS", "ID_INVOICE", "DATE",
                                    "Total Amount of Sales (VAT Exclusive)", "Exempted Sales Amount/Montant Ventes Exonerées",
                                    "Zero rated Sales Amount/Montant Ventes Taxées au Taux Zéro", "Exports Amount/Montant des Exportations",
                                    "Taxable Sales/Ventes Taxables", "VAT/ TVA", "Payed"};
                                header = " TOTAL PAYED : " + RRA_PRINT2.setVirgule(yosePayed, 1, ",") + " TOTAL Ex.TVA : " + RRA_PRINT2.setVirgule(yoseEx, 1, ",") + " TOTAL Exo.VENTE : " + RRA_PRINT2.setVirgule(yoseExo, 1, ",") + " TOTAL TAXABLES : " + RRA_PRINT2.setVirgule(YoseNtatva, 1, ",") + " TOTAL TVA : " + RRA_PRINT2.setVirgule(tvaYose, 1, ",") + "   TOTAL GENERAL : " + RRA_PRINT2.setVirgule(Yose, 1, ",");

                                setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
                                jTable.setModel(new javax.swing.table.DefaultTableModel(s1, title) {

                                    Class[] types = new Class[]{
                                        java.lang.String.class,
                                        java.lang.String.class,
                                        java.lang.String.class,
                                        java.lang.String.class,
                                        java.lang.String.class,
                                        java.lang.String.class,
                                        java.lang.String.class,
                                        java.lang.String.class,
                                        java.lang.String.class,
                                        java.lang.String.class, java.lang.String.class,
                                        java.lang.String.class
                                    };
//                    public Class getColumnClass(int columnIndex) {
//                    return types [columnIndex];
//                    }
                                });
                                new showJTable(jTable, header, server, dbName);

                            }
                        } else if (choixRpt.equals("RRA REPORT")) {
                            String nAff = (String) JOptionPane.showInputDialog(null, l(lang, "V_OPTIONS"), "", JOptionPane.QUESTION_MESSAGE, null,
                                    new String[]{"X REPORT", "Z REPORT", "PLU REPORT",}, "REPORT CHOICE");
                            if (nAff != null) {
                                if (nAff.equals("X REPORT")) {
                                    String sql = "";
                                    String mrcToShow = "";
                                    String nameEmp = "";
                                    String choix = (String) JOptionPane.showInputDialog(null, l(lang, "V_OPTIONS"),
                                            "", JOptionPane.QUESTION_MESSAGE, null,
                                            new String[]{"MRC", "EMPLOYE", "ALL"}, l(lang,"V_MAKECHOICE"));

                                    if (choix.equals("MRC")) {
                                        MRC mrc = (MRC) JOptionPane.showInputDialog(null, l(lang, "V_OPTIONS"),
                                                "", JOptionPane.QUESTION_MESSAGE, null,
                                                db.GetMRC("").toArray(), l(lang,"V_MAKECHOICE"));
                                        if (mrc != null) {
                                            sql = " AND MRC='" + mrc.MRC + "' ";
                                            mrcToShow = mrc.MRC;
                                        }
                                    } else if (choix.equals("EMPLOYE")) {
                                        db.emp();
                                        String[] emp = new String[db.allEmploye.size()];
                                        for (int i = 0; i < emp.length; i++) {
                                            emp[i] = ((Employe) db.allEmploye.get(i)).NOM_EMPLOYE;
                                        }
                                        nameEmp = (String) JOptionPane.showInputDialog(null, l(lang,"V_SELECT"), " IZINA RYANYU",
                                                JOptionPane.QUESTION_MESSAGE, null, emp, "");
                                        if (nameEmp != null) {
                                            sql = " AND EMPLOYE='" + nameEmp + "' ";
                                        }

                                    } else if (choix.contains("ALL")) {
                                        sql = " ";
                                    }

                                    if (choix != null) {
                                        String today = ("" + (new Date()).toLocaleString()).substring(0, 10);
                                        String hera = JOptionPane.showInputDialog("DATE", today);

                                        RRA_REPORT_X X = new RRA_REPORT_X(db, hera + " 00:00:00", hera + " 23:59:59", tax.getRRA("company"),
                                                tax.getRRA("tin"), "" + db.getRRA("version"), mrcToShow, tax.tvaRateA, tax.tvaRateB,
                                                tax.tvaRateC, tax.tvaRateD, nameEmp);

                                        new RRA_PRINT2(X.toPrint(sql));
                                    }

                                } else if (nAff.equals("Z REPORT")) {
                                    String hera = JOptionPane.showInputDialog("START TIME", "" + db.startTime());
                                    String geza = JOptionPane.showInputDialog("END TIME", "" + db.EndTime());

                                    RRA_REPORT_Z Z = new RRA_REPORT_Z(db, hera, geza, tax.getRRA("company"),
                                            tax.getRRA("adresse"), "" + tax.getRRA("version"), tax.getRRA("rtype"), tax.tvaRateA,
                                            tax.tvaRateB, tax.tvaRateC, tax.tvaRateD);

                                    new RRA_PRINT2(Z.toPrint());
                                } else if (nAff.equals("PLU REPORT")) {
                                    String hera = JOptionPane.showInputDialog("START TIME", "" + db.startTime());
                                    String geza = JOptionPane.showInputDialog("START TIME", "" + db.EndTime());
                                    RRA_REPORT_PLU x = new RRA_REPORT_PLU(db, hera, geza, tax.getRRA("company"),
                                            tax.getRRA("tin"), "" + tax.getRRA("version"), tax.getRRA("rtype"),
                                            tax.tvaRateA, tax.tvaRateB, tax.tvaRateC, tax.tvaRateD);
                                    new RRA_PRINT2(x.toPrint(), x.PLU(), 1);
                                }
                            }
                        }
                    } else {
                        JOptionPane.showConfirmDialog(null, l(lang, "V_AUTORISATION"), l(lang, "V_PERMISSION"), JOptionPane.YES_NO_OPTION);
                    }

                }
            }
        });
    }

    public void actionPerformed(ActionEvent e) {
        IJISHOrEPORT = e.getActionCommand();
        System.err.println("in cmd " + IJISHOrEPORT);
    }

//public void raport()
//{
//    rapportButton.addActionListener(new ActionListener(){
//        public void actionPerformed(ActionEvent ae)
//        {
//            if(ae.getSource()==rapportButton)
//            {
//                getRaport();
//                
//        int linge=listlot.size(); 
//        jScrollPane1 = new javax.swing.JScrollPane();
//        jTable = new javax.swing.JTable();
//        String [][]s1=new String[1][1];
//        String []title=new String[]{};
//        String header="";
//        
//        if(ch.equals("TOUT LES VENTES"))
//        {
//        s1=new String [linge+1][13];
//        soldeClient=0;
//        CashClient=0; 
//         
//            s1[0][0]="ID_INVOICE";
//            s1[0][1]="DATE";
//            s1[0][2]="CODE";
//            s1[0][3]="DESIGNATION";
//            s1[0][4]="LOT";
//            s1[0][5]="Quantite";
//            s1[0][6]="PRIX U";
//            s1[0][7]="PRIX R";
//            s1[0][8]="PRIX TOT";
//            s1[0][9]="EMPLOYE";
//            s1[0][10]="HEURE";
//            s1[0][11]="SOCIETE";
//            s1[0][12]="TVA";
//
//        for(int j=1;j<s1.length;j++)
//        {
//            Lot li=listlot.get(j-1);
//            s1[j][0]=li.u1;
//            s1[j][1]=li.u2;
//            s1[j][2]=li.u3;
//            s1[j][3]=li.u4;
//            s1[j][4]=li.u5;
//            s1[j][5]=li.u6; 
//            s1[j][6]=li.u7.replace('.', ',') ;
//            s1[j][7]=li.u8.replace('.', ',');
//            s1[j][8]=""+(double)(Integer.parseInt(li.u6))*(Double.parseDouble(li.u7));
//            s1[j][9]=li.u9;
//            s1[j][10]=li.u0.substring(11,li.u0.length() );
//            s1[j][11]=li.u11;
//            s1[j][12]=li.u12; 
//        }
//        if(!(umukozi.PWD_EMPLOYE.contains("boss")))
//        {
//            YoseNtatva=0;
//            tvaYose=0;
//            Yose=0;                    
//        }
//        title=new String[]{"ID_INVOICE", "DATE","CODE","DESIGNATION", "LOT", "Quantite", "PRIX U", "prix R","PRIX TOT", "EMPLOYE","HEURE","SOCIETE","TVA"};
//        header=" Raport Mwacuruje : "+setVirgule(YoseNtatva)+"   Ikiranguzo : "+setVirgule(tvaYose)+"   Inyungu : "+setVirgule(Yose)+"   % : "+setVirgule((int)((double)Yose/(double)tvaYose*100));
//        
//        }
//        else if(ch.equals("FACTURE"))
//        {
//            s1=new String [linge+1][10];
//            soldeClient=0;
//            CashClient=0; 
//         
//            s1[0][0]="ID_INVOICE";
//            s1[0][1]="CLIENT";
//            s1[0][2]="DATE";
//            s1[0][3]="EMPLOYE";            
//            s1[0][4]="SOLDE";
//            s1[0][5]="PAYED";
//            s1[0][6]="TOTAL";
//            s1[0][7]="TVA";
//            s1[0][8]="REDUCTION"; 
//            s1[0][9]="HEURE";
//        for(int j=1;j<s1.length;j++)
//        {
//            Lot li=listlot.get(j-1);
//            s1[j][0]=li.u1;
//            s1[j][1]=li.u2;
//            s1[j][2]=li.u3;
//            s1[j][3]=li.u4;
//            s1[j][4]=li.u5;
//            s1[j][5]=li.u6; 
//            s1[j][6]=li.u7;
//            s1[j][7]=li.u8;
//            s1[j][8]=li.u9;
//            s1[j][9]=li.u12;
//        }
//        if(!(umukozi.PWD_EMPLOYE.contains("boss")))
//        {
//            Yose=0;
//            yosePayed=0;
//            tvaYose=0;
//            Yose=0;
//            yoseSolde=0;
//        }
//        title=new String[]{"ID_INVOICE", "SOCIETE","DATE","EMPLOYE", "SOLDE", "PAYED", "TOTAL", "TVA","REDUCTION","HEURE"};
//        header=" Raport TOTAL SOLDE : "+setVirgule(yoseSolde)+"   TOTAL PAYED : "+setVirgule(yosePayed)+" TOTAL TVA: "+setVirgule(tvaYose)+"   TOTAL GENERAL : "+setVirgule(Yose);
//            
//        }
//
//        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
//        
//
//
//        jTable.setModel(new javax.swing.table.DefaultTableModel(s1,title)
//        {
//        Class[] types = new Class [] {
//        java.lang.String.class,
//        java.lang.String.class,
//        java.lang.String.class,
//        java.lang.String.class,
//        java.lang.String.class,
//        java.lang.String.class,
//        java.lang.String.class,
//        java.lang.String.class,
//        java.lang.String.class,
//        java.lang.String.class,  java.lang.String.class,
//        java.lang.String.class
//        };
//
////                    public Class getColumnClass(int columnIndex) {
////                    return types [columnIndex];
////                    }
//        });
// new showJTable(jTable,header,server,dbName);
//   
//        }
//        }});
//}
    public void listP() {
        listP.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent ae) {
                        if (ae.getSource() == listP) {

                            jScrollPane1 = new javax.swing.JScrollPane();
                            jTable = new javax.swing.JTable();
                            getList1();
                            String[][] s1 = new String[list.size()][7];

                            for (int i = 0; i < list.size(); i++) {

                                List l = list.get(i);
                                s1[i][0] = "" + l.id_invoice;
                                s1[i][1] = l.code;
                                s1[i][2] = l.name;
                                s1[i][3] = l.lot;
                                s1[i][4] = "" + l.qty;
                                s1[i][5] = "" + l.price;
                                s1[i][6] = "" + l.qty * l.price;
                            }

                            setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

                            jTable.setModel(new javax.swing.table.DefaultTableModel(s1,
                                            new String[]{
                                                "ID_INVOICE", "CODE ", "DESIGNATION", "No Lot", "Quantite", "prix Unitaire", "Prix Total"
                                            }
                                    ) {
                                        Class[] types = new Class[]{
                                            java.lang.String.class,
                                            java.lang.String.class,
                                            java.lang.String.class,
                                            java.lang.String.class,
                                            java.lang.String.class,
                                            java.lang.String.class
                                        };

//                    public Class getColumnClass(int columnIndex) {
//                    return types [columnIndex];
//                    }
                                    });

                            new showJTable(jTable, "List of Product", server, dbName);

                        }
                    }
                });
    }

    public void invoice() {
        invoice.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {

                if (ae.getSource() == invoice) {
                    if (umukozi.PWD_EMPLOYE.contains("FACTURE")) {
                        String f = getInvoice();
                        JTable jjj = invoiceV(f);
//            new showJTable(jjj,"FACTURES ANNULER ; TVA EST : 0.00"+"  FRW"+" TOTAL EST : 396,402.00",server,dbName); 
                        System.out.println(totaux);
                        new showJTable(jjj,facture,db, totaux, server, dbName);
                    } else {
                        JOptionPane.showConfirmDialog(null, l(lang, "V_AUTORISATION"), l(lang, "V_PERMISSION"), JOptionPane.YES_NO_OPTION);
                    }

                }

            }
        });
    }

    public static int getIn(String s, String s1, int i) {
        int entier = -1;

        String in = JOptionPane.showInputDialog(null, s + s1);
        boolean done = true;

        while (in != null && i < 3 && done == true) {
            try {
                if (in.equals("")) {
                    i++;
                    in = JOptionPane.showInputDialog(null, s + " Enter a number ");
                } else {
                    Integer in_int = new Integer(in);
                    entier = in_int.intValue();
                    done = false;
                }

            } catch (NumberFormatException e) {
                i++;
                in = JOptionPane.showInputDialog(null, s + " = " + in + " = Not a number; Enter a number ");
                done = true;
            }
        }
        if (i == 3) {
            JOptionPane.showMessageDialog(null, " Too many try ", " Sorry we have to Exit ", JOptionPane.PLAIN_MESSAGE);
        }
        return entier;
    }
////////////////////////////////////////////////////////////

    public void ms() {
        UpLoadMSButton.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent ae) {
                        if (ae.getSource() == UpLoadMSButton) {
                            if (umukozi.PWD_EMPLOYE.contains("CAISSE") || umukozi.LEVEL_EMPLOYE >= 10) {
                                String Debut = "";
                                String FIN = "";
                                String sql;
                                String nAff = (String) JOptionPane.showInputDialog(null, l(lang, "V_CHOICE"), "CHOICE",
                                        JOptionPane.QUESTION_MESSAGE, null, new String[]{"JOURNALIER", "INTERVALLE DE DATE","RAPPORT CAISSE"}, nom);

                                if (nAff.equals("JOURNALIER")) {
                                    Debut = JOptionPane.showInputDialog("ENTREZ LA DATE FORMAT(010512)", db.datez);
                                    new Caisse(Debut, server, dbName, FIN);
                                } else if (nAff.equals("INTERVALLE DE DATE")) {
                                    Debut = JOptionPane.showInputDialog(l(lang, "V_DATEDEBUT"), db.startTime());
                                    FIN = JOptionPane.showInputDialog(l(lang, "V_DATEFIN"), db.EndTime());
                                    new Caisse(Debut, server, dbName, FIN);
                                }
                                else if (nAff.equals("RAPPORT CAISSE")) {
                                    Debut = JOptionPane.showInputDialog(l(lang, "V_DATEDEBUT"), db.startTime());
                                    FIN = JOptionPane.showInputDialog(l(lang, "V_DATEFIN"), db.EndTime());
                                    new Caisse(Debut, server, dbName, FIN,true);
                                }
                                
                            } else {
                                JOptionPane.showConfirmDialog(null, l(lang, "V_AUTORISATION"), l(lang, "V_PERMISSION"), JOptionPane.YES_NO_OPTION);
                            }

                        }
                    }
                });
    }

    public void upLoad() {
        UpLoadButton.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent ae) {
                        if (ae.getSource() == UpLoadButton) {
                            try {
                                String ID = JOptionPane.showInputDialog("ENTREZ LE NUMERO DU BON À DÉVALIDER", "");
                                String CAUSE = JOptionPane.showInputDialog("ENTREZ LA CAUSE DE LA PERTE", "");

                                String doit = "" + datez + "/" + nom + " : ";

                                if (CAUSE.equals("M")) {
                                    doit = "";
                                }

                                s = conn.createStatement();
                                String createString1 = " update APP.CREDIT set CAUSE='" + doit + "" + CAUSE + "' where ID_INVOICE=" + ID;
                                s.execute(createString1);
                            } catch (SQLException ex) {
                                Logger.getLogger(VoirFichier.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                    }
                });
    }

    boolean checkDoc(String bon_livraison, int code, int datedoc) {
        boolean done = false;

        try {

            lotL = new LinkedList();
            outResult = s.executeQuery("select bon_livraison from APP.LOT where app.lot.bon_livraison='" + bon_livraison + "' and app.lot.id_product=" + code + "and app.lot.id_lot=" + datedoc);

            while (outResult.next()) {
                done = true;

                System.out.println("existe " + bon_livraison);
            }

            //  Close the resultSet
            outResult.close();
        } catch (Throwable e) {
            /*       Catch all exceptions and pass them to
             **       the exception reporting method             */
            System.out.println(" . check Dock . exception thrown:");
            db.insertError(e + "", " V checkdoc");
        }

        return done;
    }

    void setZero(int i) {
        try {

            s = conn.createStatement();
            String createString1 = " update APP.LOT set QTY_LIVE=" + 0 + ",QTY_START=" + 0 + ", BON_LIVRAISON= '" + 89 + "' where ID_PRODUCT=" + i + "";
            s.execute(createString1);

        } catch (SQLException ex) {
            db.insertError(ex + "", " V setzero");
        }

    }

    public void downLoad() {
        DownLoadButton.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent ae) {
                        if (ae.getSource() == DownLoadButton) {
                            try {
                                String nAff = (String) JOptionPane.showInputDialog(null, l(lang, "V_CHOICE"), "Fournisseur",
                                        JOptionPane.QUESTION_MESSAGE, null, new String[]{"ANNULÉS", "CHANGEMENTS", "DEVALIDÉS", "MANQUANTS", "MVT DE SORTIE", "SESSION", "Mis en Page Rama", "Mis en Page Autres"}, nom);

                                if (nAff.equals("Mis en Page Rama")) {
                                    new Map1(server, "FACTURE", dbName);
                                } else if (nAff.equals("Mis en Page Autres")) {
                                    new Map(server, dbName, "" + ((Assurance) societeJList.getSelectedValue()).sigle);
                                } else if (nAff.equals("ANNULÉS")) {
                                    String Debut = JOptionPane.showInputDialog(null, "Date 051211 ou Mois 1211");

                                    listlot = new LinkedList();
                                    String[] S = new String[]{
                                        "ID_INVOICE", "HEURE", "DATE", "CLIENT", "AFFILIE", "EMPLOYE", "CODE_QTE"
                                    };
                                    outResult = s.executeQuery("select *  from ANNULE WHERE ANNULE.DATE LIKE '%" + Debut + "%' ORDER BY ID_INVOICE ");
                                    int i = 0;
                                    while (outResult.next()) {

                                        listlot.add(new Lot(
                                                        "" + outResult.getInt(1),
                                                        "" + outResult.getTimestamp(2),
                                                        outResult.getString(3),
                                                        outResult.getString(4),
                                                        outResult.getString(5),
                                                        outResult.getString(6) + "  " + outResult.getString(8),
                                                        outResult.getString(7),
                                                        "",
                                                        "",
                                                        "",
                                                        ""
                                                )
                                        );

                                        i++;
                                    }

                                    int size = listlot.size();
                                    jScrollPane1 = new javax.swing.JScrollPane();
                                    jTable = new javax.swing.JTable();

                                    String s1[][] = new String[size][7];
                                    for (int j = 0; j < size; j++) {

                                        Lot li = listlot.get(j);
                                        s1[j][0] = li.u1;
                                        s1[j][1] = li.u2;
                                        s1[j][2] = li.u3;
                                        s1[j][3] = li.u4;
                                        s1[j][4] = li.u5;
                                        s1[j][5] = li.u6;
                                        s1[j][6] = li.u7;

                                    }
                                    setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

                                    jTable.setModel(new javax.swing.table.DefaultTableModel(s1, S
                                            ) {
                                                Class[] types = new Class[]{
                                                    java.lang.String.class,
                                                    java.lang.String.class,
                                                    java.lang.String.class,
                                                    java.lang.String.class,
                                                    java.lang.String.class,
                                                    java.lang.String.class,
                                                    java.lang.String.class
                                                };

                                                public Class getColumnClass(int columnIndex) {
                                                    return types[columnIndex];
                                                }
                                            });

                                    new showJTable(jTable, " ANNULE", server, dbName);

                                } else if (nAff.equals("CHANGEMENTS")) {
                                    listlot = new LinkedList();
                                    String Debut = JOptionPane.showInputDialog(l(lang, "V_DATEDEBUT"), db.startTime());
                                    String FIN = JOptionPane.showInputDialog(l(lang, "V_DATEFIN"), db.EndTime());

                                    String[] S = new String[]{
                                        "ID_CHANGE", "PRICE", "HEURE", "EMPLOYE", "DESIGNATION"
                                    };
                                    outResult = s.executeQuery("select *  from CHANGEMENT WHERE CHANGEMENT.HEURE >'" + Debut + "' AND CHANGEMENT.HEURE <'" + FIN + "' ORDER BY PRODUCTNAME");
                                    int i = 0;
                                    while (outResult.next()) {

                                        listlot.add(new Lot(
                                                        "" + outResult.getInt(1),
                                                        "" + outResult.getDouble(2),
                                                        "" + outResult.getTimestamp(3),
                                                        outResult.getString(4),
                                                        outResult.getString(5),
                                                        "",
                                                        "",
                                                        "",
                                                        "",
                                                        "",
                                                        ""
                                                )
                                        );

                                        i++;
                                    }
                                    int size = listlot.size();
                                    jScrollPane1 = new javax.swing.JScrollPane();
                                    jTable = new javax.swing.JTable();

                                    String s1[][] = new String[size][5];
                                    for (int j = 0; j < size; j++) {

                                        Lot li = listlot.get(j);
                                        s1[j][0] = li.u1;
                                        s1[j][1] = li.u2;
                                        s1[j][2] = li.u3;
                                        s1[j][3] = li.u4;
                                        s1[j][4] = li.u5;

                                    }
                                    setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

                                    jTable.setModel(new javax.swing.table.DefaultTableModel(s1, S
                                            ) {
                                                Class[] types = new Class[]{
                                                    java.lang.String.class,
                                                    java.lang.String.class,
                                                    java.lang.String.class,
                                                    java.lang.String.class,
                                                    java.lang.String.class
                                                };

                                                public Class getColumnClass(int columnIndex) {
                                                    return types[columnIndex];
                                                }
                                            });

                                    new showJTable(jTable, " CHANGEMENT", server, dbName);

                                } else if (nAff.equals("SESSION") && (umukozi.LEVEL_EMPLOYE >= 7 || umukozi.PWD_EMPLOYE.contains("session"))) {
                                    db.emp();
                                    System.out.println("all emp:" + db.allEmploye.size());
                                    String[] emp = new String[db.allEmploye.size() + 1];
                                    System.out.println("all emp:" + emp.length);
                                    int i;
                                    for (i = 0; i < emp.length - 1; i++) {
                                        emp[i] = ((Employe) db.allEmploye.get(i)).NOM_EMPLOYE;
                                    }
                                    System.out.println("i:" + i);
                                    emp[i] = "All";

                                    String nom = (String) JOptionPane.showInputDialog(null, "HITAMO", " IZINA RYANYU",
                                            JOptionPane.QUESTION_MESSAGE, null, emp, "");
                                    String Debut = JOptionPane.showInputDialog(l(lang, "V_DATEDEBUT"), db.startTime());
                                    String FIN = JOptionPane.showInputDialog(l(lang, "V_DATEFIN"), db.EndTime());

                                    listlot = new LinkedList();
                                    String[] S = new String[]{
                                        "ID SESSION", "NOM_EMPLOYE", "IN_OUT", "ARGENT DEBUT", "HEURE"
                                    };

                                    if (!nom.equals("All")) {
                                        outResult = s.executeQuery("select *  from LOGIN WHERE LOGIN.LOG >'" + Debut + "' AND LOGIN.LOG <'" + FIN + "' AND ID_EMPLOYE='" + nom + "' ORDER BY LOG");
                                    } else if (nom.equals("All")) {
                                        outResult = s.executeQuery("select *  from LOGIN WHERE LOGIN.LOG >'" + Debut + "' AND LOGIN.LOG <'" + FIN + "' ORDER BY LOG");
                                    }

                                    i = 0;
                                    while (outResult.next()) {

                                        listlot.add(new Lot(
                                                        "" + outResult.getInt(1),
                                                        outResult.getString(2),
                                                        "" + outResult.getString(3),
                                                        "" + outResult.getInt(4),
                                                        "" + outResult.getTimestamp(5),
                                                        "", "", "", "", "", ""));
                                        i++;
                                    }

                                    int size = listlot.size();
                                    jScrollPane1 = new javax.swing.JScrollPane();
                                    jTable = new javax.swing.JTable();

                                    String s1[][] = new String[size][5];
                                    for (int j = 0; j < size; j++) {

                                        Lot li = listlot.get(j);
                                        s1[j][0] = li.u1;
                                        s1[j][1] = li.u2;
                                        s1[j][2] = li.u3;
                                        s1[j][3] = li.u4;
                                        s1[j][4] = li.u5;

                                    }
                                    setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

                                    jTable.setModel(new javax.swing.table.DefaultTableModel(s1, S
                                            ) {
                                                Class[] types = new Class[]{
                                                    java.lang.String.class,
                                                    java.lang.String.class,
                                                    java.lang.String.class,
                                                    java.lang.String.class,
                                                    java.lang.String.class,
                                                    java.lang.String.class
                                                };

                                                public Class getColumnClass(int columnIndex) {
                                                    return types[columnIndex];
                                                }
                                            });

                                    new showJTable(jTable, " LES SESSION", server, dbName);

                                } else if (nAff.equals("MANQUANTS")) {

                                    String Debut = JOptionPane.showInputDialog(l(lang, "V_DATEDEBUT"), db.startTime());
                                    String FIN = JOptionPane.showInputDialog(l(lang, "V_DATEFIN"), db.EndTime());

                                    listlot = new LinkedList();
                                    String[] S = new String[]{
                                        "ID ", "DESIGNATION", "HEURE", "QUANTITE", "PRICE", "DISPONIBLE"
                                    };
                                    outResult = s.executeQuery("select *  from MISSED WHERE MISSED.HEURE >'" + Debut + "' AND MISSED.HEURE <'" + FIN + "' ORDER BY DESI ");
                                    int i = 0;
                                    while (outResult.next()) {

                                        listlot.add(new Lot(
                                                        "" + outResult.getInt(1),
                                                        outResult.getString(2),
                                                        "" + outResult.getTimestamp(3),
                                                        "" + outResult.getInt(4),
                                                        "" + outResult.getDouble(5),
                                                        "" + outResult.getInt(6),
                                                        "",
                                                        "",
                                                        "",
                                                        "",
                                                        ""
                                                )
                                        );

                                        i++;
                                    }

                                    int size = listlot.size();
                                    jScrollPane1 = new javax.swing.JScrollPane();
                                    jTable = new javax.swing.JTable();

                                    String s1[][] = new String[size][6];
                                    for (int j = 0; j < size; j++) {

                                        Lot li = listlot.get(j);
                                        s1[j][0] = li.u1;
                                        s1[j][1] = li.u2;
                                        s1[j][2] = li.u3;
                                        s1[j][3] = li.u4;
                                        s1[j][4] = li.u5;
                                        s1[j][5] = li.u6;

                                    }
                                    setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

                                    jTable.setModel(new javax.swing.table.DefaultTableModel(s1, S
                                            ) {
                                                Class[] types = new Class[]{
                                                    java.lang.String.class,
                                                    java.lang.String.class,
                                                    java.lang.String.class,
                                                    java.lang.String.class,
                                                    java.lang.String.class,
                                                    java.lang.String.class
                                                };

                                                public Class getColumnClass(int columnIndex) {
                                                    return types[columnIndex];
                                                }
                                            });

                                    new showJTable(jTable, " MANQUANTS", server, dbName);

                                } else if (nAff.equals("MVT DE SORTIE")) {

                                    listlot = new LinkedList();

                                    String[] S = new String[]{
                                        "ID_PRODUCT", "DESIGNATION", "LOT", "EXP", "QTE SORTIE", "BL", "MOTIF", "PRICE"
                                    };
                                    String DEBUT = JOptionPane.showInputDialog("ENTREZ LE CODE ARTICLE", "ALL");

                                    if (DEBUT.equals("ALL")) {
                                        outResult = s.executeQuery("select *  from MVT_OUT ORDER BY DESI ");
                                    } else {
                                        outResult = s.executeQuery("select *  from MVT_OUT WHERE ID_PRODUCT=" + DEBUT + " ORDER BY DESI ");
                                    }

                                    int i = 0;
                                    while (outResult.next()) {

                                        listlot.add(new Lot(
                                                        "" + outResult.getInt(1),
                                                        outResult.getString(2),
                                                        outResult.getString(3),
                                                        outResult.getString(4),
                                                        "" + outResult.getInt(5),
                                                        outResult.getString(6),
                                                        outResult.getString(7),
                                                        "" + outResult.getDouble(8),
                                                        "",
                                                        "",
                                                        ""
                                                ));

                                        i++;
                                    }

                                    int size = listlot.size();
                                    jScrollPane1 = new javax.swing.JScrollPane();
                                    jTable = new javax.swing.JTable();

                                    String s1[][] = new String[size][8];
                                    for (int j = 0; j < size; j++) {

                                        Lot li = listlot.get(j);
                                        s1[j][0] = li.u1;
                                        s1[j][1] = li.u2;
                                        s1[j][2] = li.u3;
                                        s1[j][3] = li.u4;
                                        s1[j][4] = li.u5;
                                        s1[j][5] = li.u6;
                                        s1[j][6] = li.u7;
                                        s1[j][7] = li.u8;

                                    }
                                    setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

                                    jTable.setModel(new javax.swing.table.DefaultTableModel(s1, S
                                            ) {
                                                Class[] types = new Class[]{
                                                    java.lang.String.class,
                                                    java.lang.String.class,
                                                    java.lang.String.class,
                                                    java.lang.String.class,
                                                    java.lang.String.class,
                                                    java.lang.String.class,
                                                    java.lang.String.class,
                                                    java.lang.String.class
                                                };

                                                public Class getColumnClass(int columnIndex) {
                                                    return types[columnIndex];
                                                }
                                            });

                                    new showJTable(jTable, " MOUVENT DE SORTIE", server, dbName);

                                }
                                if (nAff.equals("DEVALIDÉS")) {

                                    String d = JOptionPane.showInputDialog(null, "Date 051211 ou Mois 1211");

                                    credit = new LinkedList();

                                    if (d.length() > 3 || d.length() <= 6) {
                                        String stock = "" + ((Assurance) societeJList.getSelectedValue()).sigle;
                                        String dbb = "";
                                        if (stock.equals("RAMA")) {
                                            dbb = "_RAMA";
                                        }
                                        outResult = s.executeQuery("select APP.CREDIT.ID_INVOICE,NUMERO_AFFILIE,NUMERO_QUITANCE,SOLDE, PAYED,assurance,NOM_CLIENT,PRENOM_CLIENT,tva,percentage,cause,date from  APP.CREDIT,APP.INVOICE,APP.CLIENT" + dbb + " WHERE  APP.INVOICE.DATE LIKE '%" + d + "%' and  CREDIT.ID_INVOICE =INVOICE.ID_INVOICE and num_affiliation=numero_affilie and cause!='M' AND COMPTABILISE!='ANNULE'  order by INVOICE.ID_INVOICE ");
                                        while (outResult.next()) {
                                            int ID_INVOICE = outResult.getInt(1);
                                            String NUMERO_AFFILIE = outResult.getString(2);
                                            String NUMERO_QUITANCE = outResult.getString(3);
                                            int SOLDE = outResult.getInt(4);
                                            int PAYED = outResult.getInt(5);
                                            String ass = outResult.getString(6);
                                            String nomO = outResult.getString(7);
                                            String prenom = outResult.getString(8);
                                            int tva = outResult.getInt(9);
                                            int vtva = (int) ((100 - outResult.getInt(10)) * tva) / 100;
                                            String cause = outResult.getString(11);
                                            String dat = outResult.getString(12);
                                            credit.add(new Credit(ID_INVOICE, NUMERO_AFFILIE, NUMERO_QUITANCE, SOLDE, PAYED, ass, cause, nomO, prenom, vtva, dat)
                                            );

                                        }
                                    } else {
                                        JOptionPane.showMessageDialog(null, d + " " + l(db.lang, "V_FORMAT") + " ", l(db.lang, "V_DATE"), JOptionPane.ERROR_MESSAGE);
                                    }

                                    outResult.close();
                                    jScrollPane1 = new javax.swing.JScrollPane();
                                    jTable = new javax.swing.JTable();
                                    int linge = credit.size();
                                    String[][] s1 = new String[linge][11];
                                    soldeClient = 0;
                                    CashClient = 0;
                                    for (int i = 0; i < linge; i++) {

                                        Credit C = credit.get(i);
                                        s1[i][0] = "" + C.ID_INVOICE;
                                        s1[i][1] = C.NUMERO_AFFILIE;
                                        s1[i][2] = C.DATE;
                                        s1[i][3] = C.NUM_CLIENT;
                                        s1[i][4] = C.NUMERO_QUITANCE;
                                        s1[i][5] = C.NOM;
                                        s1[i][6] = C.PRENOM;
                                        s1[i][7] = "" + C.SOLDE;
                                        s1[i][8] = "" + C.PAYED;

                                        s1[i][9] = "" + C.tva;
                                        s1[i][10] = "" + C.RAISON;
                                        soldeClient = soldeClient + C.SOLDE;
                                        CashClient = CashClient + C.PAYED;

                                    }
                                    setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

                                    jTable.setModel(new javax.swing.table.DefaultTableModel(s1,
                                                    new String[]{
                                                        "ID_INVOICE", "N° AFF.", "DATE", "CLIENT", "N° QUITTANCE", "NOM", "PRENOM", "SOLDE", "PAYED", "TVA", "CAUSE"
                                                    }
                                            ) {
                                                Class[] types = new Class[]{
                                                    java.lang.String.class,
                                                    java.lang.String.class,
                                                    java.lang.String.class,
                                                    java.lang.String.class,
                                                    java.lang.String.class,
                                                    java.lang.String.class,
                                                    java.lang.String.class,
                                                    java.lang.String.class,
                                                    java.lang.String.class,
                                                    java.lang.String.class,
                                                    java.lang.String.class
                                                };

                                                public Class getColumnClass(int columnIndex) {
                                                    return types[columnIndex];
                                                }
                                            });
                                    new showJTable(jTable, " BONS DÉVALIDÉS Crédit : " + setVirgule((int) soldeClient) + "    Cash : " + setVirgule((int) CashClient), server, dbName);
                                }

                            } catch (Throwable ex) {
                                db.insertError(ex + "", " V download");
                            }
                        }
                    }
                });
    }

    public void getRaportAss(String assurance) {
        System.out.println("START " + new Date());
        String table = "";
        if (assurance.equals("RAMA")) {
            table = "_RAMA";
        }
        try {
            String Debut = JOptionPane.showInputDialog(l(lang, "V_DATEDEBUT"), db.startTime());
            String FIN = JOptionPane.showInputDialog(l(lang, "V_DATEFIN"), db.EndTime());
            String frss = JOptionPane.showInputDialog("DB ", dbName.toUpperCase());
            listlot = new LinkedList();
            outResult = s.executeQuery("select max (id_invoice) from APP.INVOICE where num_client='" + assurance + "' and  invoice.HEURE >'" + Debut + "' AND invoice.HEURE <'" + FIN + "' AND COMPTABILISE!='ANNULE'  ");
            int max_id_invoice = 0;
            while (outResult.next()) {
                max_id_invoice = outResult.getInt(1) + 1;
            }
            outResult.close();
            outResult = s.executeQuery("select min (id_invoice) from APP.INVOICE where num_client='" + assurance + "'"
                    + " and  invoice.HEURE >'" + Debut + "' AND invoice.HEURE <'" + FIN + "'  AND COMPTABILISE!='ANNULE' ");
            int min_id_invoice = 0;
            while (outResult.next()) {
                min_id_invoice = outResult.getInt(1) - 1;
            }
            boolean[] OK_IDS = new boolean[max_id_invoice];
            String[] toprint = new String[max_id_invoice];
            String iyokohereza = "";
            int ligne = 0;

            String file = assurance + "/" + frss + "/BYOSE.txt";
            String f2 = "F:\\PHARMACIE\\DATA\\BYOSE.txt";
            File f = new File(file);
            File ff = new File(f2);

            PrintWriter sorti = new PrintWriter(new FileWriter(f));
            PrintWriter sortib = new PrintWriter(new FileWriter(ff));
            outResult = s.executeQuery("select * from APP.INVOICE where num_client='" + assurance + "' and  invoice.HEURE >'" + Debut + "' AND invoice.HEURE <'" + FIN + "' AND COMPTABILISE!='ANNULE'  ORDER BY ID_INVOICE");

            System.out.println("select * from APP.INVOICE where num_client='" + assurance + "' and  invoice.HEURE >'" + Debut + "' AND invoice.HEURE <'" + FIN + "' AND COMPTABILISE!='ANNULE'  ORDER BY ID_INVOICE");

            while (outResult.next()) {
                toprint[outResult.getInt(1)] = "SCORE;" + outResult.getTimestamp("HEURE") + ";" + outResult.getString("EMPLOYE");
                OK_IDS[outResult.getInt(1)] = true;
            }
            outResult.close();
            System.out.println(file + "list INV" + new Date());
            outResult = s.executeQuery("select * from credit WHERE ID_INVOICE>" + min_id_invoice + " and  ID_INVOICE<"
                    + max_id_invoice + " ORDER BY ID_INVOICE");
            System.out.println("select * from credit WHERE ID_INVOICE>" + min_id_invoice + " and  ID_INVOICE<"
                    + max_id_invoice + " ORDER BY ID_INVOICE");
            while (outResult.next()) {
                if (outResult.getInt(1) < max_id_invoice && OK_IDS[outResult.getInt(1)]) {
                    toprint[outResult.getInt(1)] = toprint[outResult.getInt(1)] + ";" + outResult.getString("NUMERO_AFFILIE") + ";"
                            + siVideTire(outResult.getString("NUMERO_QUITANCE") + ";" + db.getPercentage(outResult.getString("NUMERO_AFFILIE"), table));
                }
            }
            outResult.close();

            LinkedList<String> byose = new LinkedList<String>();
            System.out.println("LIST INVOI " + new Date());
            outResult = s.executeQuery("select list.id_invoice,code_uni,SUM(quantite) as quantite,price from APP.LIST "
                    + "WHERE ID_INVOICE>" + min_id_invoice + " and  ID_INVOICE<"
                    + max_id_invoice + " GROUP BY ID_INVOICE,CODE_UNI,PRICE ORDER BY ID_INVOICE");

            System.out.println("select list.id_invoice,code_uni,SUM(quantite) as quantite,price from APP.LIST "
                    + "WHERE ID_INVOICE>" + min_id_invoice + " and  ID_INVOICE<"
                    + max_id_invoice + " GROUP BY ID_INVOICE,CODE_UNI,PRICE ORDER BY ID_INVOICE");

            while (outResult.next()) {
                //    System.err.println((outResult.getInt(1)<max_id_invoice && OK_IDS[outResult.getInt(1)])+
                //            ":cond:"+outResult.getInt(1));
                if (outResult.getInt(1) < max_id_invoice && OK_IDS[outResult.getInt(1)]) {
                    //    toprint[outResult.getInt(1)]=toprint[outResult.getInt(1)]+";"+outResult.getString("CODE_UNI")+";"+
                    //         outResult.getInt(1)+";"+outResult.getInt("QUANTITE")+";"+outResult.getInt("PRICE");

                    byose.add(toprint[outResult.getInt(1)] + ";" + outResult.getString("CODE_UNI") + ";"
                            + outResult.getInt(1) + ";" + outResult.getInt("QUANTITE") + ";" + outResult.getInt("PRICE"));

                    // String [] reba=toprint[outResult.getInt(1)].split(";");
                    // if(reba.length==9)
                    // {
                    //     sorti.println(toprint[outResult.getInt(1)]);
                    //     tosendCloud[outResult.getInt(1)]=tosendCloud[outResult.getInt(1)]+";"+outResult.getString("CODE_UNI")+";"+
                    //             outResult.getInt(1)+";"+outResult.getInt("QUANTITE")+";"+outResult.getInt("PRICE")+";0;"+assurance+"#";
                    //     iyokohereza+=tosendCloud[outResult.getInt(1)];
                    //     ligne++;
                    // }
                    // OK_IDS[outResult.getInt(1)]=false;
                }
            }
            for (int j = 0; j < byose.size(); j++) {
                String v = byose.get(j);
                iyokohereza = iyokohereza + v + ";0;" + assurance + "#";
                sorti.println(v);
                sortib.println(v);
            }

            sorti.close();
            sortib.close();

            System.out.println(file + "CREDIT INV" + new Date());
            file = assurance + "/" + frss + "/CLIENT.txt";
            f = new File(file);
            sorti = new PrintWriter(new FileWriter(f));
            outResult = s.executeQuery("select * from  client WHERE ASSURANCE='" + assurance + "'");
            while (outResult.next()) {
                String print = siVideTire(outResult.getString(1)) + ";" + siVideTire(outResult.getString(2)) + ";" + siVideTire(outResult.getString(3)) + ";" + outResult.getInt(4) + ";" + outResult.getInt(5) + ";" + siVideTire(outResult.getString(6)) + ";" + siVideTire(outResult.getString(7)) + ";" + siVideTire(outResult.getString(8)) + ";" + siVideTire(outResult.getString(9)) + ";" + siVideTire(outResult.getString(10));
                //System.out.println(print);
                sorti.println(print);
            }
            sorti.close();
            outResult.close();
            System.out.println("CLIENT " + new Date());

            file = assurance + "/" + frss + "/PRODUCT.txt";
            f = new File(file);
            sorti = new PrintWriter(new FileWriter(f));
            System.out.println("select ID_PRODUCT,NAME_PRODUCT,CODE,PRIX_" + assurance + ",TVA,OBSERVATION,"
                    + "CODE_SOC_" + assurance + ",FAMILLE "
                    + " from PRODUCT    ORDER BY NAME_PRODUCT ");
            outResult = s.executeQuery("select ID_PRODUCT,NAME_PRODUCT,CODE,PRIX_" + assurance + ",TVA,OBSERVATION,"
                    + "CODE_SOC_" + assurance + ",FAMILLE "
                    + " from PRODUCT    ORDER BY NAME_PRODUCT ");

            while (outResult.next()) {
                String print = outResult.getInt(1) + ";" + siVideTire(outResult.getString(2)) + ";"
                        + siVideTire(outResult.getString(3)) + ";" + outResult.getDouble(4) + ";"
                        + outResult.getDouble(5) + ";" + siVideTire(outResult.getString(6)) + ";"
                        + siVideTire(outResult.getString(7)) + ";" + siVideTire(outResult.getString(8));
                sorti.println(print);
            }
            sorti.close();
            //System.out.println("PROD "+new Date());
            outResult.close();

            int n = JOptionPane.showConfirmDialog(null, " DO YOU WANT TO UPLOAD IT?", "ZAMURA", JOptionPane.YES_NO_OPTION);

            if (n == 0) {
                db.insertTracking(nom, "KUZAMURA VENTE " + assurance + " FROM VOIRFICHIER/CREDIT/EXPORTER ", ligne, "ISHYIGA");
                System.out.println(iyokohereza);
                CommCloud product_Cloud = new CommCloud("http://www.ishyiga" + db.getString("extension") + "/test_cloud/upload_list_all.php?soc=" + db.dbName + "&user=" + umukozi.NOM_EMPLOYE, "list=" + iyokohereza);
                String response = product_Cloud.sendlot;

                if (!response.contains("NO CONNECTION")) {
                    StringTokenizer st2 = new StringTokenizer(response);
                    String received = st2.nextToken(";");
                    String processed = st2.nextToken(";");
                    String done_Sec = st2.nextToken(";");
                    String line = st2.nextToken(";");
                    String last = st2.nextToken(";");
                    double dd = Double.parseDouble(done_Sec);
                    db.insertTracking(nom, "RESULT YO KUZAMURA VENTE " + assurance + " ", ligne, "CLOUD", received, last, dd, Integer.parseInt(line));
                    System.err.println("**" + dd);
                } else {
                    db.insertTracking(nom, "NTA CONNEXION MU GUFATA VENTE ZA " + assurance + " ", 0, "CLOUD");
                }

            }
        } catch (Throwable e) {
            db.insertError(e + "", " V getlist");
        }
    }

//public void getRaportAss(String assurance)
//{
//    System.out.println("START "+new Date());
// try
//        {
// String  Debut = JOptionPane.showInputDialog(l(lang,  "V_DATEDEBUT"),db.getString("debut"));
// String FIN = JOptionPane.showInputDialog(l(lang,  "V_DATEFIN"),db.getString("fin"));
//String frss= JOptionPane.showInputDialog("DB ",dbName.toUpperCase());
//listlot = new LinkedList();
//outResult = s.executeQuery("select max (id_invoice) from APP.INVOICE where num_client='"+assurance+"' and  invoice.HEURE >'"+Debut+"' AND invoice.HEURE <'"+FIN+"' ");
//int max_id_invoice=0;
//while (outResult.next())
//{ 
//max_id_invoice=outResult.getInt(1)+1;
//}
//outResult.close();
//boolean [] OK_IDS=new boolean[max_id_invoice];
//String file =assurance+"/"+frss+"/Invoice_"+frss+".txt";
//File f = new File(file);
//PrintWriter sorti = new PrintWriter(new FileWriter(f)); 
//outResult = s.executeQuery("select * from APP.INVOICE where num_client='"+assurance+"' and  invoice.HEURE >'"+Debut+"' AND invoice.HEURE <'"+FIN+"' ");
//
//System.out.println( "select * from APP.INVOICE where num_client='"+assurance+"' and  invoice.HEURE >'"+Debut+"' AND invoice.HEURE <'"+FIN+"' ");
//
//while (outResult.next())
//{ 
//String print =frss+"#"+outResult.getInt(1)+"#"+siVideTire(outResult.getString(2))+"#"+siVideTire(outResult.getString(3))+"#"+outResult.getInt(4)+"#"+siVideTire(outResult.getString(5))+"#"+outResult.getTimestamp(6)+"#"+outResult.getInt(7)+"#"+outResult.getInt(8);
//OK_IDS[outResult.getInt(1)]=true;
//sorti.println(print);
//}
//sorti.close();
//outResult.close(); 
//file=assurance+"/"+frss+"/Credit_"+frss+".txt";
//System.out.println(file+"list INV"+new Date()); 
//f = new File(file);
// sorti = new PrintWriter(new FileWriter(f));
//outResult = s.executeQuery("select * from credit");
//while (outResult.next())
//{ 
//if(outResult.getInt(1)<max_id_invoice && OK_IDS[outResult.getInt(1)])
//{String print =frss+"#"+outResult.getInt(1)+"#"+siVideTire(outResult.getString(2))+"#"+siVideTire(outResult.getString(3))+"#"+outResult.getInt(4)+"#"+outResult.getInt(5)+"#"+siVideTire(outResult.getString(6));
////System.out.println(print);
//sorti.println(print);}
//}
//sorti.close();
//outResult.close();
//file=assurance+"/"+frss+"/Client_"+frss+".txt";
// System.out.println(file+"CREDIT INV"+new Date()); 
//f = new File(file);
// sorti = new PrintWriter(new FileWriter(f));
////outResult = s.executeQuery("select * from  client where assurance='"+assurance+"' ");
//outResult = s.executeQuery("select * from  client");
//while (outResult.next())
//{
//String print =siVideTire(outResult.getString(1))+"#"+siVideTire(outResult.getString(2))+"#"+siVideTire(outResult.getString(3))+"#"+outResult.getInt(4)+"#"+outResult.getInt(5)+"#"+siVideTire(outResult.getString(6))+"#"+siVideTire(outResult.getString(7))+"#"+siVideTire(outResult.getString(8))+"#"+siVideTire(outResult.getString(9))+"#"+siVideTire(outResult.getString(10));
////System.out.println(print);
//sorti.println(print);
//}
//sorti.close();
//outResult.close();
// System.out.println("CLIENT "+new Date());
//file=assurance+"/"+frss+"/List_"+frss+".txt";
//f = new File(file);
// sorti = new PrintWriter(new FileWriter(f));
//outResult = s.executeQuery("select list.id_invoice,code_uni,num_lot,quantite,price from APP.LIST");
//
//while (outResult.next())
//{
//    if(outResult.getInt(1)<max_id_invoice && OK_IDS[outResult.getInt(1)])
//{
//String print =outResult.getInt(1)+"#"+siVideTire(outResult.getString(2))+"#"+siVideTire(outResult.getString(3))+"#"+outResult.getInt(4)+"#"+outResult.getDouble(5);
////System.out.println(print);
//sorti.println(print);}
//}
//sorti.close();
//  System.out.println("LIST INVOI "+new Date());
//  
//file=assurance+"/"+frss+"/Product_"+frss+".txt";
//f = new File(file);
//sorti = new PrintWriter(new FileWriter(f));
//outResult = s.executeQuery("select ID_PRODUCT,NAME_PRODUCT,CODE,PRIX_"+assurance+",TVA,OBSERVATION,"
//        + "CODE_SOC_"+assurance+",FAMILLE "
//        + " from PRODUCT    ORDER BY NAME_PRODUCT ");
//while (outResult.next())
//{
//String print =outResult.getInt(1)+"#"+siVideTire(outResult.getString(2))+"#"+siVideTire(outResult.getString(3))+"#"+outResult.getDouble(4)+"#"+outResult.getDouble(5)+"#"+siVideTire(outResult.getString(6))+"#"+siVideTire(outResult.getString(7))+"#"+siVideTire(outResult.getString(8));
//sorti.println(print);
//}
//sorti.close();
// System.out.println("PROD "+new Date());
//outResult.close();}
//catch(Throwable e)
//{db.insertError(e+""," V getlist");}
//}
    String siVideTire(String s) {
        if (s.equals("")) {
            return "-";
        } else {
            return s;
        }

    }

    public void upDate() {
        Update.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                if (ae.getSource() == Update) {
                    String errorline = "NA";
                    try {

                        String nAff = (String) JOptionPane.showInputDialog(null, l(lang, "V_CHOICE"), " M  IS A JOUR",
                                JOptionPane.QUESTION_MESSAGE, null, new String[]{"ASSURANCE", "AFFILIE", "ASK QUESTION", "EXP", "RUN VARS", "RRA", "RECOVERY", "TARIF", "TVA", "LIST ARTICLES", "NEW ASSURANCE", "CHANGEMENT TARIF", "CHANGEMENT ASSURANCE", "WEB IMPORT", "BLOCKER ASSURANCE"}, nom);
                        if (nAff.equals("ASK QUESTION")) {
                            new FrameHtml();
                        }
                        if (nAff.equals("NEW ASSURANCE")) {
                            new InsuranceInterface(server, dbName);
                        }
                        if (nAff.equals("BLOCKER ASSURANCE") && umukozi.PWD_EMPLOYE.contains("ANNULE")) {
                            Variable blocke = db.getParam("RUN", "", "blockMAIN");
                            JPanel pane = new JPanel();
//                            pane.setLayout(new GridLayout(10,ass.size()/10));
                            //pane.setPreferredSize(new Dimension(ass.size()*200, 300));
                            if (blocke != null) {
                                JPanel paneP = new JPanel();
                                paneP.setLayout(new GridLayout(10, ass.size() / 10));
                                LinkedList< JCheckBox> jcheck = new LinkedList();

                                for (int i = 0; i < ass.size(); i++) {
                                    JCheckBox now = new JCheckBox(" " + ass.get(i).sigle);
                                    if (blocke.value.contains(ass.get(i).sigle)) {
                                        now.setSelected(true);
                                    }
                                    jcheck.add(now);
                                    paneP.add(now);
                                }
                                pane.add(paneP);
                                JOptionPane.showMessageDialog(null, pane, "Selectionne Assurance à blocker ", JOptionPane.INFORMATION_MESSAGE);
                                String toblock = "";
                                for (int i = 0; i < jcheck.size(); i++) {
                                    if (jcheck.get(i).isSelected()) {
                                        toblock += jcheck.get(i).getText();
                                    }
                                }
                                s.execute("update APP.VARIABLES set VALUE_VARIABLE='" + toblock + "', VALUE2_VARIABLE='" + toblock + "' WHERE NOM_VARIABLE='blockMAIN'");
                                JOptionPane.showMessageDialog(null, "OPERATION REUSSIE!!!");

                            }
                        }
                        if (nAff.equals("LIST ARTICLES")) {
                            String sql = "INSERT INTO `item` (`ID_PRODUCT`, `NAME_PRODUCT`, `CODE`, `PRIX_RAMA`, `CODE_SOC_RAMA`, `OBS_RAMA`, `PRIX_CORAR`, `CODE_SOC_CORAR`, `OBS_CORAR`, `PRIX_MMI`, `CODE_SOC_MMI`, `OBS_MMI`, `PRIX_SORAS`, `CODE_SOC_SORAS`, `OBS_SORAS`, `TVA`, `OBSERVATION`, `CODE_BAR`, `FAMILLE`, `MAXIMUM`,   `ID_USER`)  VALUES";
                            try {

                                outResult = s.executeQuery("select * from PRODUCT  ");
                                int l = 0;
                                while (outResult.next()) {
                                    if (l != 0) {
                                        sql = sql + ",";
                                    }

                                    l++;
                                    String name = outResult.getString("NAME_PRODUCT");
                                    if (name.contains("'")) {
                                        name = name.replaceAll("'", " ");
                                    }

                                    sql = sql + "(" + outResult.getInt("ID_PRODUCT") + ",'" + name + "','" + outResult.getString("CODE") + "'," + outResult.getDouble("PRIX_RAMA") + ",'" + outResult.getString("CODE_SOC_RAMA") + "','" + outResult.getString("OBSERVATION") + "'," + outResult.getDouble("PRIX_CORAR") + ",'" + outResult.getString("CODE_SOC_CORAR") + "',''," + outResult.getDouble("PRIX_MMI") + ",'" + outResult.getString("CODE_SOC_MMI") + "',''," + outResult.getDouble("PRIX_SORAS") + ",'" + outResult.getString("CODE_SOC_SORAS") + "',''," + outResult.getDouble("TVA") + ",'" + outResult.getString("OBSERVATION") + "','" + outResult.getString("CODE_BAR") + "','" + outResult.getString("FAMILLE") + "'," + outResult.getInt("MAXIMUM") + ",'1')";
                                }

                                PrintWriter sorti = new PrintWriter(new FileWriter("export.txt"));
                                sorti.println(sql);
                                outResult.close();
                                sorti.close();
                            } catch (Throwable e) {
                                db.insertError(e + "", "  DOWN LOAD FOR WEB");
                                System.out.println(" . . . DOWN LOAD FOR WEB" + e);
                            }

                        }
                        if (nAff.equals("WEB IMPORT")) {
                            Tarif tar = null;
                            String choice = (String) JOptionPane.showInputDialog(null, l(lang, "V_CHOICE"), " MIS A JOUR",
                                    JOptionPane.QUESTION_MESSAGE, null, new String[]{"TARIF", "UPLOAD TARIF"}, nom);
                            String selected = "_" + ((Assurance) societeJList.getSelectedValue()).sigle;

                            if (choice.equals("TARIF")) {

                                outResult = s.executeQuery("select * from tarif WHERE DES_TARIF='" + ((Assurance) societeJList.getSelectedValue()).sigle + "'");
                                while (outResult.next()) {
                                    tar = new Tarif(outResult.getString(1), outResult.getString(2), outResult.getString(3),
                                            outResult.getDouble(4), outResult.getString(8), outResult.getInt(5), outResult.getInt(6),
                                            outResult.getInt(7), outResult.getString("OBSERVE"), outResult.getString("SMART"));
                                }
                                outResult.close();

                                if (tar != null) {

                                    LinkedList<Lot> ancien = new LinkedList();
                                    outResult = s.executeQuery("select * from PRODUCT WHERE " + tar.VAR_TARIF + ">0");
                                    System.out.println("select * from PRODUCT WHERE " + tar.VAR_TARIF + ">0");
                                    while (outResult.next()) {

                                        String codeSoc;
                                        String code = outResult.getString("CODE");
                                        String price = outResult.getString(tar.VAR_TARIF);
                                        String obs = outResult.getString("OBSERVATION");
                                        if (selected.contains("RAMA") || selected.contains("MMI")) {
                                            codeSoc = outResult.getString("CODE_SOC" + selected);
                                        } else {
                                            codeSoc = "0";
                                        }

                                        ancien.add(new Lot(code, price, obs, codeSoc, "", "", "", "", "", "", ""));
                                    }
                                    System.out.println(" ancien  leng " + ancien.size());

                                    String file = "ANCIEN_PRIX" + (new Date().getTime()) + selected + ".txt";
                                    File f = new File(file);
                                    try {
                                        PrintWriter sorti = new PrintWriter(new FileWriter(f));
                                        for (int j = 0; j < ancien.size(); j++) {
                                            Lot v = ancien.get(j);
                                            //clientLargeurFACTURE1;50.0;PRINT;FACTURE1
                                            String line = v.u1 + ";" + v.u2 + ";" + v.u3 + ";" + v.u4;
                                            sorti.println(line);
                                        }
                                        sorti.close();
                                    } catch (Throwable e) {
                                        System.out.println("biranze pe " + e);
                                        db.insertError(e + "", "copy file map print");
                                    }
                                    //CommCloud product_Cloud = new CommCloud("http://www.ishyiga" + db.getString("extension") + "/test_cloud/get_tariff.php?soc=" + ((Assurance) societeJList.getSelectedValue()).sigle, selected);
                                    CommCloud product_Cloud = new CommCloud("http://www.ishyiga" + db.getString("extension") 
                                            + "/test_cloud/get_tariff.php?soc=" + ((Assurance) societeJList.getSelectedValue()).sigle+
                                            "&dbname=" + dbName, selected);
                                    String response = product_Cloud.sendlot;

                                    if (response.length() > 20) {
                                        if (selected.contains("RAMA") || selected.contains("MMI")) {
                                            s.execute("update APP.product set " + tar.VAR_TARIF + "=0 ,CODE_SOC" + selected + "='NULL' , OBSERVATION='NULL'");
                                            System.out.println("update APP.product set " + tar.VAR_TARIF + "=0 ,CODE_SOC" + selected + "='NULL' , OBSERVATION='NULL'");
                                        } else {
                                            s.execute("update APP.product set " + tar.VAR_TARIF + "=0 ");
                                            System.out.println("update APP.product set " + tar.VAR_TARIF + "=0 ");
                                        }

                                        String[] productCloud = product_Cloud.sendlot.split("#");
                                        // System.out.println("size:"+productCloud.length);

                                        for (int j = 0; j < productCloud.length; j++) {
                                            try {
                                                productCloud[j] = productCloud[j].replaceAll(";;", ";-;");
                                                StringTokenizer st2 = new StringTokenizer(productCloud[j]);
                                                String codeuni = st2.nextToken(";");
                                                String prixAss = st2.nextToken(";");
                                                String codeass = st2.nextToken(";");
                                                String obse = st2.nextToken(";");
                                                String cond = st2.nextToken(";");

                                                if (selected.contains("RAMA") || selected.contains("MMI")) {
                                                    s.execute("update APP.product set " + tar.VAR_TARIF + "=" + prixAss + ",CODE_SOC" + selected + "='" + codeass + "' , OBSERVATION='" + obse + "' where CODE = '" + codeuni + "'");
                                                    System.out.println("update APP.product set " + tar.VAR_TARIF + "=" + prixAss + ",CODE_SOC" + selected + "='" + codeass + "' , OBSERVATION='" + obse + "' where CODE = '" + codeuni + "'");

                                                } else {
                                                    s.execute("update APP.product set " + tar.VAR_TARIF + "=" + prixAss + " where CODE = '" + codeuni + "'");
                                                    System.out.println("update APP.product set " + tar.VAR_TARIF + "=" + prixAss + " where CODE = '" + codeuni + "'");
                                                }
                                            } catch (Exception e) {
                                                System.out.println("exception:" + e);
                                            }
                                        }
                                        JOptionPane.showMessageDialog(null, "TARIF " + selected + " UPDATED SUCCESS FULLY ");
                                    }
                                } else {
                                    JOptionPane.showMessageDialog(null, " " + selected + " IS NOT CONFIGURED IN YOUR SYSTEM ", "ATTENTION", JOptionPane.ERROR_MESSAGE);
                                }
                            } else if (choice.equals("UPLOAD TARIF")) {
                                String ass = ((Assurance) societeJList.getSelectedValue()).sigle;
                                String tosend = "";
                                String price = "";
//        String toQuery = "";
                                tar = null;

                                JPanel pw = new JPanel();
                                JPasswordField passwordField = new JPasswordField(10);
                                JLabel label = new JLabel(l(lang, "V_PWD"));
                                pw.add(label);
                                pw.add(passwordField);
                                passwordField.setText("");
                                JOptionPane.showConfirmDialog(null, pw);

                                if (passwordField.getText().equals("ALGO")) {
                                    outResult = s.executeQuery("select * from tarif WHERE DES_TARIF='" + ass + "'");
                                    while (outResult.next()) {
                                        tar = new Tarif(outResult.getString(1), outResult.getString(2), outResult.getString(3),
                                                outResult.getDouble(4), outResult.getString(8), outResult.getInt(5), outResult.getInt(6),
                                                outResult.getInt(7), outResult.getString("OBSERVE"), outResult.getString("SMART"));
                                    }
                                    outResult.close();

                                    if (tar != null) {

                                        outResult = s.executeQuery("select * from PRODUCT WHERE " + tar.VAR_TARIF + ">0 ORDER BY NAME_PRODUCT");
                                        while (outResult.next()) {
                                            String codeSoc = "";
                                            int id = outResult.getInt(1);
                                            String code = outResult.getString(3);
                                            price = outResult.getString(tar.VAR_TARIF);
                                            String obs = outResult.getString("OBSERVATION");
                                            obs = obs.replaceAll("<", " < ");

                                            if (ass.contains("RAMA") || ass.contains("MMI")) {
                                                codeSoc = outResult.getString("CODE_SOC_" + ass);
                                            } else {
                                                codeSoc = "0";
                                            }

                                            tosend = tosend + ass + ";" + code + ";" + codeSoc + ";" + price + ";" + obs + ";" + "1" + ";" + dbName + "#";

                                        }
                                        tosend = tosend.replaceAll(";;", ";-;");

                                        System.out.println(ass + "**" + tosend + "db:" + db.dbName);

                                        CommCloud product_Cloud = new CommCloud("http://www.ishyiga" + db.getString("extension") + "/test_cloud/upload_tariff.php?user=Domitille&soc=" + db.dbName + "&assur=" + ass, "tarriff=" + tosend);
                                        String response = product_Cloud.sendlot;
                                        System.out.println("ser:" + response);

                                        if (response.contains("NOT UPDATED")) {
                                            JOptionPane.showMessageDialog(null, "TARIF " + ass + " NOT UPLOADED SUCCESSFULLY", "", JOptionPane.WARNING_MESSAGE);
                                        } else {
                                            JOptionPane.showMessageDialog(null, "TARIF " + ass + " UPLOADED SUCCESSFULLY");

                                        }
                                    }
                                } else {
                                    JOptionPane.showMessageDialog(null, " WRONG PASSWORD", "", JOptionPane.WARNING_MESSAGE);
                                }

                            } else {

                                String sql = "INSERT INTO `prix_achat` (  `FRSS` , `CODE` , `CODE_FRSS` , `NAME_FRSS` , `COND_FRSS` , `PRIX_FRSS` , `INCOTERME` , `DELAIS_LIVRAISON` , `QUANTITE`  , `ID_USER`    ) VALUES";
                                try {

                                    outResult = s.executeQuery("select * from PRODUCT  ");
                                    int l = 0;
                                    while (outResult.next()) {

                                        l++;
                                        String name = outResult.getString("NAME_PRODUCT");
                                        if (name.contains("'")) {
                                            name = name.replaceAll("'", " ");
                                        }
                                        if (outResult.getString("CODE_FRSS").length() > 3 && l < 20) {
                                            sql = sql + ("('MULTIPHAR','" + outResult.getString("CODE") + "','" + outResult.getString("CODE_FRSS") + "','" + outResult.getString("DESIGNATION_FRSS") + "'," + outResult.getInt("CONDITIONNEMENT") + "," + outResult.getDouble("PRIX_REVIENT") + ",0,0," + (int) (Math.random() * 100) + ",'1'),");
                                        }
                                    }

                                    PrintWriter sorti = new PrintWriter(new FileWriter("exportPk.txt"));
                                    sorti.println(sql);
                                    outResult.close();
                                    sorti.close();
                                } catch (Throwable e) {
                                    db.insertError(e + "", "  DOWN LOAD FOR WEB");
                                    System.out.println(" . . . DOWN LOAD FOR WEB" + e);
                                }

                            }

                        }
                        if (nAff.equals("RRA")) {

                            JPanel pw = new JPanel();
                            JPasswordField passwordField = new JPasswordField(10);
                            JLabel label = new JLabel(l(lang, "V_PWD"));
                            pw.add(label);
                            pw.add(passwordField);
                            passwordField.setText("");
                            JOptionPane.showConfirmDialog(null, pw);

                            if (passwordField.getText().equals("ALGO")) {

                                LinkedList<Variable> vars = new LinkedList();
                                try {

                                    outResult = s.executeQuery("select  * from APP.VARIABLES where  APP.variables.famille_variable= 'RRA'  ");

                                    while (outResult.next()) {
                                        //  System.out.println(outResult.getString(2));
                                        String num = outResult.getString(1);
                                        String value = outResult.getString(2);
                                        String fam = outResult.getString(3);
                                        String value2 = outResult.getString(4);
                                        vars.add(new Variable(num, value, fam, value2));
                                    }

//        if(vars.size()>0)
//        {
//            new RRA_INTERFACE(db, vars,umukozi.NOM_EMPLOYE);
//        }
                                } catch (SQLException e) {
                                    System.out.println(e);
                                }

                            } else {
                                JOptionPane.showMessageDialog(null, "INVALID PASSWORD !!!!", " ATTENTION ", JOptionPane.WARNING_MESSAGE);
                            }
                        }

                        if (nAff.equals("RUN VARS")) {
                            String choice = (String) JOptionPane.showInputDialog(null, l(lang, "V_CHOICE"), " MIS A JOUR",
                                    JOptionPane.QUESTION_MESSAGE, null, new String[]{"NEW", "UPDATE"}, nom);
                            if (choice.equals("NEW")) {
                                new VariableInterface(db);

                            } else if (choice.equals("UPDATE")) {
                                LinkedList<Variable> vars = new LinkedList();
                                try {
                                    outResult = s.executeQuery("select  * from APP.VARIABLES where  APP.variables.famille_variable= 'RUN'  ");

                                    while (outResult.next()) {
                                        //  System.out.println(outResult.getString(2));
                                        String num = outResult.getString(1);
                                        String value = outResult.getString(2);
                                        String fam = outResult.getString(3);
                                        String value2 = outResult.getString(4);
                                        vars.add(new Variable(num, value, fam, value2));
                                    }
                                    Variable[] crtl = new Variable[vars.size()];
                                    for (int g = 0; g < vars.size(); g++) {
                                        crtl[g] = vars.get(g);
                                    }

                                    Variable v = (Variable) JOptionPane.showInputDialog(null, l(lang, "V_CHOICE"), "Vars",
                                            JOptionPane.QUESTION_MESSAGE, null, crtl, nom);

                                    new VariableInterface(db, v);

//        String value2= JOptionPane.showInputDialog("nouveau var ",v.value2);
//              s.execute(  " update APP.VARIABLES set value2_variable ='" + value2 + "'  where nom_variable='" + v.nom  + "' AND famille_variable='" + v.famille + "'");
                                } catch (Throwable e) {

                                }

                            }
                        }
                        if (nAff.equals("CHANGEMENT ASSURANCE")) {
                            LinkedList<Variable> vars = new LinkedList();
                            try {
                                outResult = s.executeQuery("select  * from APP.VARIABLES where  APP.variables.famille_variable= 'CLIENT'  ");

                                while (outResult.next()) {
                                    //  System.out.println(outResult.getString(2));
                                    String num = outResult.getString(1);
                                    String value = outResult.getString(2);
                                    String fam = outResult.getString(3);
                                    String value2 = outResult.getString(4);
                                    vars.add(new Variable(num, value, fam, value2));
                                }
                                Variable[] crtl = new Variable[vars.size()];
                                for (int g = 0; g < vars.size(); g++) {
                                    crtl[g] = vars.get(g);
                                }

                                Variable v = (Variable) JOptionPane.showInputDialog(null, l(lang, "V_CHOICE"), "ASSURANCE",
                                        JOptionPane.QUESTION_MESSAGE, null, crtl, nom);

                                String value2 = JOptionPane.showInputDialog("CHANGEMENT ", v.nom);
                                System.out.println(" update APP.VARIABLES set nom_variable='" + value2 + "'  where  nom_variable='" + v.nom + "' AND value2_variable ='" + v.value2 + "' AND famille_variable='" + v.famille + "'");

                                s.execute(" update APP.VARIABLES set nom_variable='" + value2 + "'  where  nom_variable='" + v.nom + "' AND value2_variable ='" + v.value2 + "' AND famille_variable='" + v.famille + "'");

                            } catch (Throwable e) {
                                ;
                            }
                        }
                        if (nAff.equals("CHANGEMENT TARIF")) {
                            LinkedList<Variable> vars = new LinkedList();
                            try {
                                outResult = s.executeQuery("select  * from APP.TARIF ");
                                int i = 0;
                                Tarif[] tar = new Tarif[10];
                                while (outResult.next() && i < 10) {

                                    String ID_TARIF = outResult.getString("ID_TARIF");
                                    String DES_TARIF = outResult.getString("DES_TARIF");
                                    String FONT_TARIF = outResult.getString("FONT_TARIF");
                                    String VAR_TARIF = outResult.getString("VAR_TARIF");
                                    double COEF_TARIF = outResult.getDouble("COEF_TARIF");
                                    int b = outResult.getInt("B_TARIF");
                                    int g = outResult.getInt("G_TARIF");
                                    int r = outResult.getInt("R_TARIF");
                                    String obs = outResult.getString("OBSERVE");
                                    String smart = outResult.getString("SMART");

                                    tar[i] = new Tarif(ID_TARIF, VAR_TARIF, DES_TARIF, COEF_TARIF, FONT_TARIF, r, g, b, obs, smart);
                                    i++;
                                }

                                Tarif t = (Tarif) JOptionPane.showInputDialog(null, l(lang, "V_CHOICE"), "TARIF",
                                        JOptionPane.QUESTION_MESSAGE, null, tar, nom);

                                String DES_TARIF = JOptionPane.showInputDialog("CHANGEMENT ", t.DES_TARIF);
                                String VAR_TARIF = JOptionPane.showInputDialog("CHANGEMENT ", t.VAR_TARIF);

                                System.out.println(" update APP.TARIF set DES_TARIF='" + DES_TARIF + "',VAR_TARIF='" + VAR_TARIF + "'  where   ID_TARIF ='" + t.ID_TARIF + "' ");

                                s.execute(" update APP.TARIF set DES_TARIF='" + DES_TARIF + "',VAR_TARIF='" + VAR_TARIF + "'  where   ID_TARIF ='" + t.ID_TARIF + "' ");

                            } catch (Throwable e) {
                                ;
                            }
                        }

                        if (nAff.equals("RECOVERY")) {
                            new Recovery(server, dbName);
                        }
                        if (nAff.equals("EXP")) {
                            LinkedList<String> guhindura = new LinkedList();
                            outResult = s.executeQuery("select * from APP.LOT   order by date_exp,id_lot ");

                            while (outResult.next()) {
                                int productCode = outResult.getInt(1);
                                int dateDoc = outResult.getInt(2);
                                String id_LotS = outResult.getString(3);
                                String date = outResult.getString(4);
                                String bon_livraison = outResult.getString(7);
                                /*
                                 if
                                 10<d1<21  et 10<d2<21     max(d1,d2)mm min(d1,d2)
                                 10<d1<21     d2mmd1
                                 10<d2<21     d1mmd2
                                 */
                                String resultat = date;
                                String dd1 = date.substring(4);
                                String dd2 = date.substring(0, 2);
                                String mm = date.substring(2, 4);
                                if (date.length() == 6) {
                                    int d1 = 0;
                                    int d2 = 0;
                                    try {
                                        d1 = Integer.parseInt(dd1);
                                        d2 = Integer.parseInt(dd2);
                                    } catch (NumberFormatException e) {
                                        JOptionPane.showInputDialog(null, s + "  Not a number ");
                                    }

                                    if (d1 < 21 && d1 > 10 && d2 < 21 && d2 > 10) {
//System.out.println(date+"  111111111111111111111111111111111111111       "+d1+" "+d2); 
                                        if (d1 < d2) {
                                            resultat = "" + dd2 + mm + dd1;
                                        } else {
                                            resultat = "" + dd1 + mm + dd2;
                                        }

//System.out.println(resultat);
// guhindura.add(new  Lot(0,"","", id_LotS,ret,dateDoc,qtyStart, qtyLive, bon_livraison));
                                        String sql = " update APP.LOT set DATE_EXP='" + resultat + "' where BON_LIVRAISON='" + bon_livraison + "' AND ID_LOT=" + dateDoc + " and ID_LOTS='" + id_LotS + "' and ID_PRODUCT=" + productCode + "";
                                        guhindura.add(sql);
                                    } else if (d1 < 21 && d1 > 10) {
                                        resultat = "" + dd2 + mm + dd1;
//System.out.println(date+" 2       "+d1+" "+d2);
// guhindura.add(new  Lot(0,"","", id_LotS,ret,dateDoc,qtyStart, qtyLive, bon_livraison));
                                        String sql = " update APP.LOT set DATE_EXP='" + resultat + "' where BON_LIVRAISON='" + bon_livraison + "' AND ID_LOT=" + dateDoc + " and ID_LOTS='" + id_LotS + "' and ID_PRODUCT=" + productCode + "";
                                        guhindura.add(sql);
//System.out.println(resultat);
                                    } else if (d2 < 21 && d2 > 10) {
                                        resultat = "" + dd1 + mm + dd2;
//System.out.println(date+" 3       "+d1+" "+d2);
//guhindura.add(new  Lot(0,"","", id_LotS,date1,dateDoc,qtyStart, qtyLive, bon_livraison));
                                        String sql = " update APP.LOT set DATE_EXP='" + resultat + "' where BON_LIVRAISON='" + bon_livraison + "' AND ID_LOT=" + dateDoc + " and ID_LOTS='" + id_LotS + "' and ID_PRODUCT=" + productCode + "";
                                        guhindura.add(sql);
//System.out.println(resultat);
                                    }
                                }
                            }
                            for (int i = 0; i < guhindura.size(); i++) {
                                s.execute(guhindura.get(i));
                            }

                        }

                        if (nAff.equals("TARIF")) {
                            String choix = (String) JOptionPane.showInputDialog(null, l(lang, "V_CHOICE"), " MIS A JOUR TARIF",
                                    JOptionPane.QUESTION_MESSAGE, null, new String[]{"ID", "CODE", "FRSS", "WEB", "PRIX->SOCIETE"}, nom);

                            if (choix.equals("PRIX->SOCIETE")) {
                                s.execute(" update APP.product set prix_societe=prix ");
                                JOptionPane.showMessageDialog(null, " UPDATE DONE SUCCESSFULLY ", " UPDATE PRODUCT ", JOptionPane.INFORMATION_MESSAGE);

                            } else if (choix.equals("SET TARIF")) {
                                new FamilleInterface(db);
                            } else {
                                String sql = "";
                                String selected = ((Assurance) societeJList.getSelectedValue()).sigle;
                                String desi = JOptionPane.showInputDialog("Fichier Tarif " + selected, selected + ".txt");

                                if (selected.equals("CASHNORM")) {
                                    selected = "";
                                } else {
                                    selected = "_" + selected;
                                }

                                LinkedList<String> list = getFichier(desi);
                                System.out.println("list size:" + list.size());

                                for (int i = 0; i < list.size(); i++) {
                                    System.out.println("list:" + list.get(i));
                                }

                                s = conn.createStatement();

                                if (!choix.equals("FRSS")) {
                                    s.execute("update APP.product set prix" + selected + "=0 ");
                                }

                                for (int i = 0; i < list.size(); i++) {
                                    errorline = list.get(i);
                                    StringTokenizer st1 = new StringTokenizer(errorline);
                                    String code = st1.nextToken(";");
                                    String prix = st1.nextToken(";");

                                    if (selected.equals("_RAMA")) {
                                        String codesoc = st1.nextToken(";");
                                        String obs = st1.nextToken(";");
                                        sql = ", CODE_SOC" + selected + "='" + codesoc + "', OBSERVATION='" + obs + "' ";
                                    }  //  System.out.println("update APP.product set prix"+selected+"="+prix+" "+sql+" where CODE = '"+code+"'");

                                    if (choix.equals("ID")) {
                                        System.out.println("update APP.product set prix" + selected + "=" + prix + " " + sql + "  where id_product = " + code);
                                        s.execute("update APP.product set prix" + selected + "=" + prix + " " + sql + "  where id_product = " + code);
                                    } else if (choix.equals("CODE")) {
                                        System.out.println("update APP.product set prix" + selected + "=" + prix + " " + sql + " where CODE = '" + code + "'");
                                        s.execute("update APP.product set prix" + selected + "=" + prix + " " + sql + " where CODE = '" + code + "'");
                                    } else if (choix.equals("WEB")) {
//            String codesoc =st1.nextToken(";");
//            String obs=st1.nextToken(";");
//            
//            if(selected.equals("_RAMA"))
//            {
//                sql=", CODE_SOC"+selected+"='"+codesoc+"', OBSERVATION='"+obs+"' ";
//                s.execute("update APP.product set prix"+selected+"="+0+", CODE_SOC='0', OBSERVATION='NULL' where prix"+selected+">0");
//            }
                                        System.out.println("update APP.product set prix" + selected + "=" + prix + " " + sql + " where CODE = '" + code + "'");
                                        s.execute("update APP.product set prix" + selected + "=" + prix + " " + sql + " where CODE = '" + code + "'");

                                    } else {
                                        //System.out.println("update APP.product set prix"+selected+"="+prix+"/CONDITIONNEMENT "+sql+" where CODE_FRSS = '"+code+"'");
                                        s.execute("update APP.product set prix" + selected + "=" + prix + "/CONDITIONNEMENT " + sql + " where CODE_FRSS = '" + code + "'");
                                    }
                                }
                                JOptionPane.showMessageDialog(null, " UPDATE DONE SUCCESSFULLY ", " UPDATE PRODUCT ", JOptionPane.INFORMATION_MESSAGE);

                            }

                        } else if (nAff.equals("AFFILIE")) {
                            LinkedList<Client> Lesaffilier = new LinkedList();
                            String choix = (String) JOptionPane.showInputDialog(null, "Faites votre choix", "CLOUDING ",
                                    JOptionPane.QUESTION_MESSAGE, null, new String[]{"CLOUD", "LOCAL"}, nom);

//           
                            if (choix.equals("CLOUD")) {
                                String selected = ((Assurance) societeJList.getSelectedValue()).sigle;
                                String lastSyncronised = db.getString("lastSyncroAffilie");
                                //last_sync="YYY-mm-dd HH:ii:ss"

                                db.insertTracking(nom, "GUFATA AFFILIE BA " + selected + " FROM VOIRFICHIER/UPDATE/AFFILIE ", 0, "ISHYIGA");
                                int id_track = getMaxTrack();
                                CommCloud product_Cloud = new CommCloud("http://www.ishyiga" + db.getString("extension") + "/test_cloud/get_list_aff.php?soc=" + db.dbName + "&user=PolTest&insurance=" + selected + "&id_track=" + id_track, "last_sync=" + lastSyncronised + "");
                                String response = product_Cloud.sendlot;
                                if (!response.contains("NO CONNECTION")) {
                                    String[] result = product_Cloud.sendlot.split(":::");

                                    StringTokenizer st2 = new StringTokenizer(result[1]);
                                    String rec = st2.nextToken(";");
                                    String exec = st2.nextToken(";");
                                    String done = st2.nextToken(";");
                                    st2.nextToken(";");
                                    String value = st2.nextToken(";");

                                    db.insertTracking(nom, "RESULT YO KUMANURA AFFILIE BA " + selected + " ", 0, "CLOUD", rec, exec, Double.parseDouble(done), Integer.parseInt(value));
                                    db.upDateVariable(new Variable("lastSyncroAffilieMAIN", exec, "RUN", exec), nom);
                                    if (!result[0].contains("DOWNLOAD FAILED")) {

                                        String[] affCloud = result[0].split("#");
//         System.err.println(result[0]);
                                        for (int i = 0; i < affCloud.length; i++) {
                                            //System.err.println(affCloud[i]);
                                            StringTokenizer st1 = new StringTokenizer(affCloud[i]);
                                            String idAff = st1.nextToken(";");
                                            String etat = st1.nextToken(";");
                                            String couv = st1.nextToken(";");
                                            String perc = st1.nextToken(";");
                                            String exp = st1.nextToken(";");
                                            String last_mod = st1.nextToken(";");
                                            String id_sh = st1.nextToken(";");
                                            String ka = exp.substring(8, 10) + exp.substring(5, 7) + exp.substring(2, 4);

                                            //System.out.println(idAff+"**"+etat+"**"+couv+"**"+perc+"**"+exp+"**"+last_mod+"**"+id_sh+"**"+ka+"**");
//             
                                            Client cl = new Client(idAff, " ", " ", Integer.parseInt(perc), Integer.parseInt(ka), selected, " ", " ", etat, idAff, " ", " ", " ", " ");
                                            if (etat.equals("INACTIF")) {
                                                cl.VISA = "N";
                                            } else {
                                                cl.VISA = "O";
                                            }
                                            Lesaffilier.add(cl);
                                            //System.out.println("byaje");
                                        }
                                        System.out.println("nahageze:");
                                        int compteur = Integer.parseInt(db.getParama("RUN", "compteurClient").value);
                                        //System.out.println("hapa:"+compteur);
                                        //String NUML="";
                                        for (int i = 0; i < Lesaffilier.size(); i++) {
                                            Client ex = Lesaffilier.get(i);
                                            Client c = db.getClientAff(ex.NUM_AFFILIATION, "", "");
                                            boolean ok = false;

                                            if (c == null) {
                                                compteur++;
                                                //NUML=""+compteur; 
                                                ex.CODE = "" + compteur;
                                                db.insertClientRamaAuto(ex, compteur);
                                                ok = true;
                                            } else {
                                                if (ex.ASSURANCE.equals("RAMA")) {
                                                    db.upDateClientR(ex, "client_rama", ex.NUM_AFFILIATION);
                                                    ok = true;
                                                } else {
                                                    db.upDateClientR(ex, "client", ex.NUM_AFFILIATION);
                                                    ok = true;
                                                }
                                            }
                                        }
                                        JOptionPane.showMessageDialog(null, " " + selected + " AFFILIATES UPDATED SUCCESSFULLY");
                                    } else if (result[0].contains("DOWNLOAD FAILED")) {
                                        JOptionPane.showMessageDialog(null, " " + selected + " AFFILIATES UPDATED SUCCESSFULLY");
                                    }
                                } else {
                                    db.insertTracking(nom, "NTA CONNEXION MU KUMANURA AFFILIE BA " + selected + " ", 0, "CLOUD");
                                }
                            } else {
                                String selected = ((Assurance) societeJList.getSelectedValue()).sigle;
                                String desi = JOptionPane.showInputDialog("Fichier Affilies " + selected, selected + ".txt");

                                String N = (String) JOptionPane.showInputDialog(null, l(lang, "V_CHOICE"), "Categorie affilie",
                                        JOptionPane.QUESTION_MESSAGE, null, new String[]{"O", "N"}, nom);

                                LinkedList<String> list = getFichier(desi);
                                if (selected.equals("RAMA") && N.equals("N")) {
                                    db.beforeNegatifRama();
                                }
                                int compteur = Integer.parseInt(db.getParama("RUN", "compteurClient").value);

                                for (int i = 0; i < list.size(); i++) {
                                    StringTokenizer st1 = new StringTokenizer(list.get(i));
                                    if (selected.equals("RAMA")) {
//1	0000169	A0169	TUYISENGE	SOLANGE	2011-02-01	2011-05-02	CHAMBRE DES DEPUTES

                                        System.out.println(list.get(i));
                                        st1.nextToken(";");
                                        String NUM_AFFILIATIONa = st1.nextToken(";");
                                        String NUM_AFFILIATIONa2 = st1.nextToken(";");
                                        String NOM_CLIENTa = st1.nextToken(";").replaceAll("'", "");
                                        String PRENOM_CLIENTa = st1.nextToken(";").replaceAll("'", "");
                                        int PERCENTAGEa = 15;
                                        st1.nextToken(";");
                                        String DATE = st1.nextToken(";");
                                        StringTokenizer st3 = new StringTokenizer(DATE.replace(" ", ""));
                                        String yy = st3.nextToken("-");
                                        String mm = st3.nextToken("-");
                                        String dd = st3.nextToken("-");
                                        String datedoc = yy.substring(2, 4) + mm + dd;
                                        Integer hhh = new Integer(datedoc);
                                        int DATE_EXPa = hhh.intValue();
                                        String ASSURANCEa = "RAMA";
                                        String EMPLOYEURa = st1.nextToken(";");
                                        String SECTEURa = "";
                                        String NUML = "";
                                        String NAISSANCE = "0";
                                        String SEXE = " ";
                                        String link = "";
                                        String BENEFICIAIRE = st1.nextToken(";").replaceAll("'", "");

                                        Client c = db.getClientAff(NUM_AFFILIATIONa, "_RAMA", "");
                                        boolean ok = false;

                                        if (c == null) {
                                            compteur++;
                                            NUML = "" + compteur;
                                            db.insertClientRamaAuto(new Client(NUM_AFFILIATIONa, NOM_CLIENTa, PRENOM_CLIENTa, PERCENTAGEa, DATE_EXPa, ASSURANCEa, EMPLOYEURa, SECTEURa, N, NUML, NAISSANCE, SEXE, link, BENEFICIAIRE), compteur);
                                            ok = true;
                                        } else {
                                            db.upDateClientR(new Client(NUM_AFFILIATIONa, NOM_CLIENTa, PRENOM_CLIENTa, PERCENTAGEa, DATE_EXPa, ASSURANCEa, EMPLOYEURa, SECTEURa, N, NUML, NAISSANCE, SEXE, link, BENEFICIAIRE), "client_rama", NUM_AFFILIATIONa);
                                            ok = true;
                                        }

                                        if (!NUM_AFFILIATIONa2.equals("N") && (!ok)) {
                                            c = db.getClientAff(NUM_AFFILIATIONa2, "_RAMA", "");
                                            if (c == null) {
                                                compteur++;
                                                db.insertClientRamaAuto(new Client(NUM_AFFILIATIONa2, NOM_CLIENTa, PRENOM_CLIENTa, PERCENTAGEa, DATE_EXPa, ASSURANCEa, EMPLOYEURa, SECTEURa, N, NUML, NAISSANCE, SEXE, link, BENEFICIAIRE), compteur);
                                            } else {
                                                db.upDateClientR(new Client(NUM_AFFILIATIONa2, NOM_CLIENTa, PRENOM_CLIENTa, PERCENTAGEa, DATE_EXPa, ASSURANCEa, EMPLOYEURa, SECTEURa, N, NUML, NAISSANCE, SEXE, link, BENEFICIAIRE), "client_rama", NUM_AFFILIATIONa2);
                                            }
                                        }
                                    } else {
                                        st1.nextToken(";");
                                        String numAff = st1.nextToken(";");
                                        String NAME = st1.nextToken(";");
                                        StringTokenizer st2 = new StringTokenizer(NAME);

                                        String prenom = st2.nextToken(" ");
                                        String nom = st2.nextToken("");
                                        st1.nextToken(";");
                                        st1.nextToken(";");
                                        st1.nextToken(";");
                                        String DATE = st1.nextToken(";");
                                        StringTokenizer st3 = new StringTokenizer(DATE.replace(" ", ""));

                                        String dd = st3.nextToken("/");
                                        String mm = st3.nextToken("/");
                                        String yy = st3.nextToken("/");

                                        String datedoc = yy.substring(2, 4) + mm + dd;
                                        // System.out.println(datedoc);

                                        String emp = st1.nextToken(";");
                                        System.out.println(i + " " + numAff + ";" + prenom + ";" + nom + ";" + emp + ";" + datedoc);
                                        Integer hhh = new Integer(datedoc);
                                        int dated = hhh.intValue();
                                        //RW00218 00;ANNE; ABAKUNZI;F;BANQUE COMMERCIALE du RWANDA;190310

                                        psInsert = conn.prepareStatement("insert into APP.CLIENT (NUM_AFFILIATION,NOM_CLIENT,PRENOM_CLIENT ,PERCENTAGE ,DATE_EXP , ASSURANCE,EMPLOYEUR,SECTEUR,CODE,VISA ) values (?,?,?,?,?,?,?,?,?,?)");

                                        psInsert.setString(1, numAff);
                                        psInsert.setString(2, nom);
                                        psInsert.setString(3, prenom);
                                        psInsert.setInt(4, 0);
                                        psInsert.setInt(5, dated);
                                        psInsert.setString(6, selected);
                                        psInsert.setString(7, emp);
                                        psInsert.setString(8, " ");
                                        psInsert.setString(9, "" + i);
                                        psInsert.setString(10, "");
                                        psInsert.executeUpdate();
                                        psInsert.close();
                                        System.out.println(list.get(i));
                                    }
                                }
                                db.updateCompteur("" + compteur);
                            }
                        } else if (nAff.equals("ASSURANCE")) {
                            String selected = ((Assurance) societeJList.getSelectedValue()).sigle;
                            String desi = JOptionPane.showInputDialog("ENTREZ La nouvelle designation de " + selected, ((Assurance) societeJList.getSelectedValue()).designation);
                            String adresse = JOptionPane.showInputDialog("ENTREZ La nouvelle Adresse de " + selected, ((Assurance) societeJList.getSelectedValue()).adresse);

                            s = conn.createStatement();
                            String createString1 = "update APP.VARIABLES set VALUE_variable='" + desi + "',VALUE2_variable='" + adresse + "' where APP.variables.nom_variable = '" + selected + "'";
                            s.execute(createString1);

                        } else if (nAff.equals("TVA")) {
                            String d = JOptionPane.showInputDialog(l(lang, "V_DATEDEBUT"), db.startTime());
                            String f = JOptionPane.showInputDialog(l(lang, "V_DATEFIN"), db.EndTime());
                            db.tva(d, f);
                        }
                    } catch (Throwable ex) {
                        db.insertError(ex + "" + errorline, " V upload");
                    }
                }
            }
        });
    }

    public void sql() {
        sqlButton.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent ae) {

                        if (ae.getSource() == sqlButton) {

                            try {

                                String pwd = JOptionPane.showInputDialog(null, " PWD ");

                                if (pwd.equals("kilo")) {
                                    String createString1 = "delete";
                                    String createString2 = JOptionPane.showInputDialog(null, " Ecrire votre syntaxe SQL ", "update APP.--  set  --   where --=--");

//if(!createString1.contains(" delete from lot"))
                                    s = conn.createStatement();
                                    s.execute(createString1);

                                }

                                if (pwd.equals("gamma")) {

                                    String createString1 = JOptionPane.showInputDialog(null, " Ecrire votre syntaxe SQL ", "update APP.--  set  --   where --=--");

                                    if (!createString1.contains("delete")) {
                                        s = conn.createStatement();
                                        s.execute(createString1);
                                    }
                                }

                            } catch (Throwable e) {

                                db.insertError(e + "", " V sql");
                            }

                        }
                    }
                });
    }

    public void upDateUni(double tva, double PRICE, String obs, String code) {
        try {

            s = conn.createStatement();
            String createString1 = " update product set prix=" + PRICE + ",prix_SOCIETE=" + (PRICE * 0.85) + ",tva=" + tva + ",observation='" + obs + "' where code='" + code + "'";
            s.execute(createString1);
        } catch (Throwable e) {
            db.insertError(e + "", " V upDateUni");
        }

    }

    public void upDateSoc(double PRICE, String code, String fam) {
        try {
            double coef = 0;

            String[] control = new String[]{
                "CA",
                "CB",
                "CC",
                "CHE",
                "CHF",
                "CH",
                "FTB",
                "FTC",
                "HA",
                "HC",
                "HL",
                "HM",
                "HP",
                "HR",
                "PGA",
                "PGE",
                "PLG",
                "PSA",
                "PSE",
                "PV"};

            double[] Ccoef = new double[]{
                1.967,
                1.967,
                1.967,
                1.967,
                1.967,
                1.967,
                2.085,
                2.085,
                1.667,
                1.667,
                1.667,
                1.667,
                1.667,
                1.667,
                1.667,
                1.667,
                1.787,
                1.563,
                1.563,
                1.563};

            for (int i = 0; i < Ccoef.length; i++) {
                if (control[i].equals(fam)) {
                    coef = Ccoef[i];
                }

            }

            s = conn.createStatement();
            String createString1 = " update product set prix_REVIENT=" + PRICE + ",FAMILLE='" + fam + "',COEF=" + coef + " where code='" + code + "'";
            s.execute(createString1);

        } catch (Throwable e) {
            db.insertError(e + "", " V upDatesoc");
        }

    }

    void addOneNew(Product p) {

        try {

            psInsert = conn.prepareStatement("insert into PRODUCT (id_product,name_product,code,prix, tva,code_bar,observation,prix_societe )  values (?,?,?,?,?,?,?,?)");

            psInsert.setInt(1, p.productCode);
            psInsert.setString(2, p.productName);
            psInsert.setString(3, p.code);
            psInsert.setDouble(4, p.currentPrice);
            psInsert.setDouble(5, p.tva);
            psInsert.setString(6, p.code);
            psInsert.setString(7, p.observation);
            psInsert.setDouble(8, p.currentPrice * 0.85);
            psInsert.executeUpdate();

            // Release the resources (clean up )
            psInsert.close();
        } catch (Throwable e) {
            db.insertError(e + "", " V addOneNew");
        }

    }

    public void addOneLot(Lot l) {
        try {

            psInsert = conn.prepareStatement("insert into APP.LOT (ID_PRODUCT,ID_LOT,ID_LOTS,DATE_EXP,QTY_START,QTY_LIVE,BON_LIVRAISON)  values (?,?,?,?,?,?,?)");

            psInsert.setInt(1, l.productCode);
            psInsert.setInt(2, l.id_lot);
            psInsert.setString(3, l.id_LotS);
            psInsert.setString(4, l.dateExp);
            psInsert.setInt(5, l.qtyStart);
            psInsert.setInt(6, l.qtyLive);
            psInsert.setString(7, l.bon_livraison);
            psInsert.executeUpdate();
            psInsert.close();

        } catch (Throwable e) {
            db.insertError(e + "", " V addOneLot");
        }

    }

//le panel pour Montrer le listP
    public void eastPane(Container content) {
        police = new Font("Consolas", Font.BOLD, 18);
        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(6, 3));
        product = new JButton(l(lang, "V_ARTICLE"));
        product.setBackground(Color.green);
        product.setFont(police);
        product.setToolTipText(l(lang, "V_ARTICLE"));
        // product.setFont(new Font("Helvetica", Font.PLAIN, 20));

        rapportButton = new JButton(l(lang, "V_RAPPORT"));
        rapportButton.setBackground(Color.green);
        // rapportButton.setFont(new Font("Helvetica", Font.PLAIN, 20));
        rapportButton.setFont(police);
        rapportButton.setToolTipText(l(lang, "V_RAPPORT"));

        compta = new JButton(l(lang, "V_COMPTABILITE"));
        compta.setBackground(Color.magenta);
        // compta.setFont(new Font("Helvetica", Font.PLAIN, 20));
        compta.setFont(police);
        compta.setToolTipText(l(lang, "V_COMPTABILITE"));

        lot = new JButton(l(lang, "V_STOCK"));
        lot.setBackground(Color.magenta);
        // lot.setFont(new Font("Helvetica", Font.PLAIN, 20));
        lot.setFont(police);
        lot.setToolTipText(l(lang, "V_STOCK"));

        perim = new JButton(l(lang, "V_EMPLOYE"));
        perim.setBackground(Color.magenta);
        //perim.setFont(new Font("Helvetica", Font.PLAIN, 20));
        perim.setFont(police);
        perim.setToolTipText(l(lang, "V_EMPLOYE"));

        listP = new JButton(l(lang, "V_VENTE"));
        //listP.setBackground(Color.magenta);
        //listP.setFont(new Font("Helvetica", Font.PLAIN, 20));
        listP.setFont(police);
        listP.setToolTipText(l(lang, "V_VENTE"));

        invoice = new JButton(l(lang, "V_FACTURE"));
        invoice.setFont(new Font("Helvetica", Font.PLAIN, 20));
        invoice.setFont(police);
        invoice.setToolTipText(l(lang, "V_FACTURE"));

        clientRama = new JButton(l(lang, "V_CLIENT"));
        clientRama.setBackground(Color.green);
        clientRama.setFont(new Font("Helvetica", Font.PLAIN, 20));
        clientRama.setFont(police);
        clientRama.setToolTipText(l(lang, "V_CLIENT"));

        creditButton = new JButton(l(lang, "V_CREDIT"));
        //creditButton.setFont(new Font("Helvetica", Font.PLAIN, 20));
        creditButton.setFont(police);
        creditButton.setToolTipText(l(lang, "V_CREDIT"));

        UpLoadButton = new JButton(l(lang, "V_DEVALIDER"));
        UpLoadButton.setBackground(Color.ORANGE);
        //UpLoadButton.setFont(new Font("Helvetica", Font.PLAIN, 20));
        UpLoadButton.setFont(police);
        UpLoadButton.setToolTipText(l(lang, "V_DEVALIDER"));

        UpLoadMSButton = new JButton(l(lang, "V_CAISSE"));
        UpLoadMSButton.setBackground(Color.ORANGE);
        // UpLoadMSButton.setFont(new Font("Helvetica", Font.PLAIN, 20));
        UpLoadMSButton.setFont(police);
        UpLoadMSButton.setToolTipText(l(lang, "V_CAISSE"));

        DownLoadButton = new JButton(l(lang, "V_IBYABAYE"));
        DownLoadButton.setBackground(Color.ORANGE);
        //DownLoadButton.setFont(new Font("Helvetica", Font.PLAIN, 20));
        DownLoadButton.setFont(police);
        DownLoadButton.setToolTipText(l(lang, "V_IBYABAYE"));

        Update = new JButton(l(lang, "V_UPDATE"));
        Update.setBackground(Color.PINK);
        //Update.setFont(new Font("Helvetica", Font.PLAIN, 20));
        Update.setFont(police);
        Update.setToolTipText(l(lang, "V_UPDATE"));

        sqlButton = new JButton(l(lang, "V_SQL"));
        sqlButton.setBackground(Color.PINK);
        //sqlButton.setFont(new Font("Helvetica", Font.PLAIN, 20));
        sqlButton.setFont(police);
        sqlButton.setToolTipText(l(lang, "V_SQL"));

        panel.setPreferredSize(new Dimension(550, 600));

        JScrollPane scrollPaneProductList = new JScrollPane(societeJList);
        scrollPaneProductList.setPreferredSize(new Dimension(40, 40));
        societeJList.setBackground(Color.pink);
        panel.add(scrollPaneProductList);

        panel.add(product);
        panel.add(clientRama);
        panel.add(compta);
        panel.add(lot);
        panel.add(perim);

        panel.add(invoice);
        panel.add(listP);
        panel.add(creditButton);
        panel.add(DownLoadButton);
        panel.add(UpLoadButton);
        panel.add(UpLoadMSButton);

        panel.add(Update);

        panel.add(sqlButton);
        panel.add(rapportButton);
        content.add(panel, BorderLayout.EAST);
    }

    @Override
    protected void processWindowEvent(WindowEvent evt) {
        if (WindowEvent.WINDOW_CLOSING == evt.getID()) {

            this.dispose();
       //System.exit(0);

            //default icon, custom title
//JOptionPane.showMessageDialog(this,"RIRAFUNGIRA RIMWE N'ISHYIGA"," IKIBAZO",JOptionPane.PLAIN_MESSAGE);
            //this.setDefaultCloseOperation(HIDE_ON_CLOSE); 
        }

    }

// LinkedList  importer()
//{
//    LinkedList< Client> clie =new LinkedList();
//    LinkedList< List> lis = new LinkedList();
//    LinkedList< Credit> cr = new LinkedList();
//    LinkedList< Invoice> in = new LinkedList();
//    LinkedList< Product> pr = new LinkedList(); 
//    Yose=0;
//    YoseNtatva=0;
//    tvaYose=0;
//    yosePayed=0;
//    yoseSolde=0;
//    listlot = new LinkedList();
//    int max_id_invoice=0;
//    int min_id_invoice=0; 
//    try
//    {
//        Thread t=new Thread();
//        String  Debut = JOptionPane.showInputDialog(l(lang,  "V_DATEDEBUT"),db.getString("debut"));
//        String FIN = JOptionPane.showInputDialog(l(lang,  "V_DATEFIN"),db.getString("fin"));
//        System.out.println("start:"+new Date());
//        outResult = s.executeQuery("select max (id_invoice) from APP.INVOICE where    invoice.HEURE >'"+Debut+"' AND invoice.HEURE <'"+FIN+"' ");
//        System.out.println("select max (id_invoice) from APP.INVOICE where  invoice.HEURE >'"+Debut+"' AND invoice.HEURE <'"+FIN+"' "); 
//        
//        while (outResult.next())
//        { 
//            max_id_invoice=outResult.getInt(1)+1;
//        } 
//        outResult.close();
//        System.out.println("max:"+max_id_invoice);
//        
//        
//        outResult = s.executeQuery("select min (id_invoice) from APP.INVOICE where    invoice.HEURE >'"+Debut+"' AND invoice.HEURE <'"+FIN+"' ");
//        System.out.println("select min (id_invoice) from APP.INVOICE where    invoice.HEURE >'"+Debut+"' AND invoice.HEURE <'"+FIN+"' ");
//        
//        while (outResult.next())
//        { 
//            min_id_invoice=outResult.getInt(1);
//        } 
//        outResult.close();
//        System.out.println("min:"+min_id_invoice);
//        boolean [] OK_IDS=new boolean[max_id_invoice];
// 
//        outResult = s.executeQuery("select * from APP.INVOICE where   invoice.HEURE >'"+Debut+"' AND invoice.HEURE <'"+FIN+"' ");
//        System.out.println("select * from APP.INVOICE where   invoice.HEURE >'"+Debut+"' AND invoice.HEURE <'"+FIN+"' ");
//        while (outResult.next())
//        { 
//            Invoice invo = new  Invoice(outResult.getInt(1),outResult.getString("EMPLOYE"),outResult.getString("DATE"), 
//                    outResult.getString("NUM_CLIENT"),(int) outResult.getDouble("TOTAL"),outResult.getString("HEURE"),
//                    (int) outResult.getDouble("TVA"),0); 
//            in.add(invo);
//            OK_IDS[outResult.getInt(1)]=true; 
//             
//        } 
//        outResult.close();  
//        
//        outResult = s.executeQuery("select * from credit where id_invoice>="+min_id_invoice+" and id_invoice<="+max_id_invoice+"");
//        System.out.println("select * from credit where id_invoice>="+min_id_invoice+" and id_invoice<="+max_id_invoice);
//        
//        while (outResult.next())
//        { 
//            
//            if(outResult.getInt(1)<max_id_invoice && OK_IDS[outResult.getInt(1)])
//            {
//                Credit cre=new Credit(outResult.getInt(1),outResult.getString("NUMERO_AFFILIE") ,
//                        outResult.getString("NUMERO_QUITANCE"),outResult.getInt("SOLDE") ,outResult.getInt("PAYED"));
//                cr.add(cre);
//            }
//            else
//                System.err.println(outResult.getInt(1));
//        } 
//        outResult.close();
//        System.out.println("select * from client"+new Date()+cr.size());
//        //outResult = s.executeQuery("select * from  client where assurance='"+assurance+"' ");
//        for(int i=0;i<cr.size();i++)
//        {
//            boolean found=false; 
//            
//            outResult = s.executeQuery("select * from  client  where num_affiliation='"+cr.get(i).NUMERO_AFFILIE +"'");
//      //  System.out.println("select * from  client  where num_affiliation='"+cr.get(i).NUMERO_AFFILIE +"'");
//        while (outResult.next())
//        {
//            Client cli=new Client(outResult.getString("NUM_AFFILIATION") , outResult.getString("NOM_CLIENT")  
//                    ,outResult.getString("PRENOM_CLIENT") ,outResult.getInt("PERCENTAGE"),
//                    outResult.getInt("DATE_EXP"),outResult.getString("ASSURANCE") ,outResult.getString("EMPLOYEUR")
//                    ,outResult.getString("SECTEUR"),outResult.getString("VISA"),outResult.getString("CODE") );
//            clie.add(cli);
//            found=true;
//        } 
//        if(found)
//            continue;
//        
//        outResult = s.executeQuery("select * from  client_rama where num_affiliation='"+cr.get(i).NUMERO_AFFILIE +"'");
//        //System.out.println("select * from  client_rama where num_affiliation='"+cr.get(i).NUMERO_AFFILIE +"'");
//        
//        while (outResult.next())
//        {
//            Client cli=new Client(outResult.getString("NUM_AFFILIATION") , outResult.getString("NOM_CLIENT")  
//                    ,outResult.getString("PRENOM_CLIENT") ,outResult.getInt("PERCENTAGE"),
//                    outResult.getInt("DATE_EXP"),outResult.getString("ASSURANCE") ,outResult.getString("EMPLOYEUR")
//                    ,outResult.getString("SECTEUR"),outResult.getString("VISA"),outResult.getString("CODE") );
//            clie.add(cli);
//            found=true;
//        }
//        if(found)
//            continue;
//        
//        outResult = s.executeQuery("select * from  client_unipharma where num_affiliation='"+cr.get(i).NUMERO_AFFILIE +"'");
//        //System.out.println("select * from  client_unipharma where num_affiliation='"+cr.get(i).NUMERO_AFFILIE +"'");
//        while (outResult.next())
//        {
//            Client cli=new Client(outResult.getString("NUM_AFFILIATION") , outResult.getString("NOM_CLIENT")  
//                    ,outResult.getString("PRENOM_CLIENT") ,outResult.getInt("PERCENTAGE"),
//                    outResult.getInt("DATE_EXP"),outResult.getString("ASSURANCE") ,outResult.getString("EMPLOYEUR")
//                    ,outResult.getString("SECTEUR"),outResult.getString("VISA"),outResult.getString("CODE") );
//            clie.add(cli);
//        } 
//        outResult.close(); 
//            
//        }
//        System.out.println("end:"+new Date());
//        
//        
//        outResult = s.executeQuery("select list.id_invoice,code_uni,num_lot,quantite,price,PRIX_REVIENT from APP.LIST where id_invoice>="+min_id_invoice+" and id_invoice<="+max_id_invoice+"order by id_invoice");
//        System.out.println("select list.id_invoice,code_uni,num_lot,quantite,price,PRIX_REVIENT from APP.LIST order by id_invoice");
//        String old="";
//        int oldQte=0;
//        String oldS="";
//        t.sleep(100);
//        
//        while (outResult.next())
//        {
//            String lot2 = outResult.getString(3);
//            int id_inv = outResult.getInt(1);
//            String code = outResult.getString(2);
//            double price = outResult.getDouble(5);
//            int qte=outResult.getInt(4);
//            double priceREVIENT = outResult.getDouble(6);
//            
//            if(id_inv>0 && id_inv<max_id_invoice && OK_IDS[id_inv])
//            { 
//                List li=new List (id_inv, price,qte,lot2,code,"",priceREVIENT); 
//                lis.add(li); 
//            }
//        }
//        outResult = s.executeQuery("select ID_PRODUCT,NAME_PRODUCT,CODE,0,TVA,OBSERVATION,"
//                + "'-',FAMILLE "
//                + " from PRODUCT where STATUS='OK' OR STATUS='SOMMEIL'    ORDER BY NAME_PRODUCT ");
//        System.out.println("select ID_PRODUCT,NAME_PRODUCT,CODE,0,TVA,OBSERVATION,"
//                + "'-',FAMILLE "
//                + " from PRODUCT where STATUS='OK' OR STATUS='SOMMEIL'    ORDER BY NAME_PRODUCT ");
//        while (outResult.next())
//        {
//            Product prod=new Product(outResult.getInt(1),outResult.getString(2),outResult.getString(3),0 ,0,outResult.getDouble(5),"",outResult.getString("OBSERVATION"),
//                    outResult.getString("FAMILLE"),db,0); 
//            pr.add(prod);
//        }  
//        outResult.close();
//        //t.sleep(5000); 
//    }
//    catch(Throwable e)
//    {
//        System.out.println(e+" k ");
//    }
//    
//    try
//    {   
//        String file = "RAPPORT.txt";
//        File f = new File(file);
//        PrintWriter sorti = new PrintWriter(new FileWriter(f)); 
//        System.out.println("CONV CREDIT "+new Date()+clie.size());
//        String k="ID_INVOICE#DATE#EMPLOYE#HEURE#ORDONNANCE#NOM_AFFILIE#PRENOM_AFFILIE#POURCENTAGE#NUM_AFFILIATION#ASSURANCE#CODE#DESIGNATION#NUM_LOT#QUANTITE#PRIX_VENTE#PRIX_REVIENT#FAMILLE";
//        sorti.println(k);
//        
//        //IMPORT CLIENT
//        HashMap <String,Client> clientSet=new HashMap();
//        
//        for(int i=0;i<clie.size();i++)
//        { 
//            Client v= clie.get(i); 
//            //System.out.println(v);
//            clientSet.put(v.NUM_AFFILIATION,v); 
//        }
//        System.out.println("CONV LIST "+new Date());      
//        
//        HashMap<String,Product> productSet=new HashMap();
//      
//        for(int i=0;i<pr.size();i++)
//        {
//            Product p=pr.get(i); 
//            productSet.put(p.code,p); 
//        }   
//        System.out.println("CONVERT "+new Date());   
//        /// insertion des donnÃ©es
//        ////////////////////////////////////////////////////////////////////////////////
//        
//        max_id_invoice=max_id_invoice+1;
//        System.out.println(+clie.size()+ " clients"+new Date());
//        
//        boolean reba [] =new boolean [max_id_invoice];
//        Invoice ensemble []= new Invoice [max_id_invoice];
//        
//        System.out.println(in.size()+"max_id_invoice  "+max_id_invoice); 
//        
//        for(int i=0;i<in.size();i++)
//        {
//            Invoice invoice2=in.get(i);
//            reba[invoice2.id_invoice] =true;
//            ensemble[invoice2.id_invoice] =invoice2;
//            //System.out.println(invoice2);
//        }  
//        System.out.println(+in.size()+ " INV"+new Date());
//        
//        for(int i=0;i<cr.size();i++)
//        { 
//            Credit credit2=cr.get(i);     
//            ensemble[credit2.ID_INVOICE].credit=credit2;
//           //System.out.println(credit2);
//        } 
//
//        for(int i=0;i<lis.size();i++)
//        {
//            List listo=lis.get(i);   
//            ensemble[listo.id_invoice].listInvoice.add(listo); 
//        }
//        System.out.println(lis.size()+ " lists"+new Date());
//        
//        for(int i=0;i<ensemble.length;i++)    
//        {
//            if(reba[i])
//            {
//                for(int j=0;j<ensemble[i].listInvoice.size();j++)   
//                { 
//                    if(ensemble[i].credit!=null )   
//                    {
//                        ensemble[i].client= clientSet.get(ensemble[i].credit.NUMERO_AFFILIE);
//                        //System.out.println(ensemble[i].credit +" "+ensemble[i].client+" "+ensemble[i].credit.NUMERO_AFFILIE);
//                    } 
//                    if( ensemble[i].listInvoice.get(j)!=null)   
//                    {        
//                        ensemble[i].listInvoice.get(j).product= productSet.get(ensemble[i].listInvoice.get(j).code);                        
//                        //System.out.println(ensemble[i].listInvoice.get(j).product);
//                    }
//                }
//            }    
//        }   
//        //JOptionPane.showMessageDialog(null," ense INV "+new Date(),"",JOptionPane.PLAIN_MESSAGE);
//
//        System.out.println( ensemble.length+ " fin ensemble "+new Date());   
//        
//        for(int i=min_id_invoice;i<ensemble.length;i++)    
//        {
//            Invoice t= ensemble[i]; 
//            //System.out.println(t);
//            //System.out.println(reba[i]);
//            if(reba[i])
//            { 
//                for(int j=0;j<ensemble[i].listInvoice.size();j++)   
//                { 
//                    List l=ensemble[i].listInvoice.get(j);
//                    //System.err.println(t); 
//                    //System.out.println(t.client); 
//                    //System.err.println(t.credit);
//                    String  name=  ""; 
//                    String famille="";
//                    if(l.product!=null)
//                    {
//                        name=l.product.productName;
//                        famille=l.product.codeSoc;
//                    } 
//                    
//                    if(t!=null && t.client!=null && t.credit!=null && l!=null )   
//                    { 
//                        Byose b=new Byose(t.id_invoice,t.date ,t.heure,t.id_employe,t.credit.NUMERO_AFFILIE,t.client.NOM_CLIENT
//                                ,"AGE" ,t.client.SEXE ,
//                                t.client.PRENOM_CLIENT ,t.client.PERCENTAGE,t.credit.NUMERO_QUITANCE,
//                                t.id_client,l.code,name,0 ,l.lot,l.qty,l.price,l.prix_revient ,l.qty*l.price,famille); 
//                        
//                        YoseNtatva=YoseNtatva+((int)(l.price*l.qty));
//                        tvaYose=tvaYose+((int)(l.prix_revient*l.qty));
//                        Yose=Yose+((int)((l.price-l.prix_revient)*l.qty));
//                        
//                        listlot.add(new Lot(""+t.id_invoice, t.date, l.code, name, l.lot,""+l.qty, ""+l.price, ""+l.prix_revient, t.id_employe, t.heure, t.id_client,""+l.product.tva, "", "", "")); 
//                         
//                        //System.out.p';,lrintln(b+"");
//                        ///db.insertByose(b);
//                        sorti.println(b);
//                    }
//                    else if(t!=null && l!=null )
//                    { 
//                        //System.err.println(t.id_client+"  cash  "+t.id_invoice);
//                        Byose b=new Byose(t.id_invoice,t.date ,t.heure,t.id_employe,"",""
//                                ," " ,"" ,
//                                "" ,0,"",
//                                t.id_client,l.code,name,0 ,l.lot,l.qty,l.price ,l.prix_revient,l.qty*l.price,famille );
//                        
//                        //System.out.println("C:"+t.id_invoice);
//                        
//                        YoseNtatva=YoseNtatva+((int)(l.price*l.qty));
//                        tvaYose=tvaYose+((int)(l.prix_revient*l.qty));
//                        Yose=Yose+((int)((l.price-l.prix_revient)*l.qty));   
//                        
//                        listlot.add(new Lot(""+t.id_invoice, t.date, l.code, name, l.lot,""+l.qty, ""+l.price, ""+l.prix_revient, t.id_employe, t.heure, t.id_client,""+l.product.tva, "", "", "")); 
//                         
//                        //System.out.println(b+"");
//                        sorti.println(b);
//                        ///db.insertByose(b);
//                    }
//                    else
//                        System.err.println(i+""+t);   
//                    
//                }}}  
//        sorti.close();       
//        ////////////////////////////////////////////////////////////////////////////////////
//    } 
//    catch (Throwable ex) 
//    {
//        JOptionPane.showMessageDialog(null," Shida ",""+ex,JOptionPane.PLAIN_MESSAGE);
//        System.err.println(""+ex);
//    }
//    
//    return listlot;
//    
//}
    LinkedList importerVentes() {
        LinkedList< Credit> cr = new LinkedList();
        LinkedList< Invoice> in = new LinkedList();
        listlot = new LinkedList();
        int max_id_invoice = 0;
        int min_id_invoice = 0;
        try {
            Thread t = new Thread();
            String Debut = JOptionPane.showInputDialog(l(lang, "V_DATEDEBUT"), db.startTime());
            String FIN = JOptionPane.showInputDialog(l(lang, "V_DATEFIN"), db.EndTime());
            System.out.println("start:" + new Date());
            outResult = s.executeQuery("select max (id_invoice) from APP.INVOICE where    invoice.HEURE >'" + Debut + "' AND invoice.HEURE <'" + FIN + "' AND COMPTABILISE!='ANNULE' AND NUM_CLIENT IS NOT NULL ");
            System.out.println("select max (id_invoice) from APP.INVOICE where  invoice.HEURE >'" + Debut + "' AND invoice.HEURE <'" + FIN + "' AND COMPTABILISE!='ANNULE' ");

            while (outResult.next()) {
                max_id_invoice = outResult.getInt(1) + 1;
            }
            outResult.close();
            System.out.println("max:" + max_id_invoice);

            outResult = s.executeQuery("select min (id_invoice) from APP.INVOICE where    invoice.HEURE >'" + Debut + "' AND invoice.HEURE <'" + FIN + "' AND COMPTABILISE!='ANNULE' AND NUM_CLIENT IS NOT NULL ");
            System.out.println("select min (id_invoice) from APP.INVOICE where    invoice.HEURE >'" + Debut + "' AND invoice.HEURE <'" + FIN + "' AND COMPTABILISE!='ANNULE'  ");

            while (outResult.next()) {
                min_id_invoice = outResult.getInt(1);
            }
            outResult.close();
            System.out.println("min:" + min_id_invoice);
            boolean[] OK_IDS = new boolean[max_id_invoice];

            outResult = s.executeQuery("select * from APP.INVOICE where   "
                    + "invoice.HEURE >'" + Debut + "' AND invoice.HEURE <'" + FIN + "' "
                    + "AND COMPTABILISE!='ANNULE' AND NUM_CLIENT IS NOT NULL ");
            System.out.println("select * from APP.INVOICE where   "
                    + "invoice.HEURE >'" + Debut + "' AND invoice.HEURE <'" + FIN + "' AND COMPTABILISE!='ANNULE'  ");
            while (outResult.next()) {
                Invoice invo = new Invoice(outResult.getInt(1), outResult.getString("EMPLOYE"), outResult.getString("DATE"),
                        outResult.getString("NUM_CLIENT"), outResult.getDouble("TOTAL"), outResult.getString("HEURE"),
                        outResult.getDouble("TVA"), 0);
                in.add(invo);
                OK_IDS[outResult.getInt(1)] = true;
            }
            outResult.close();

            outResult = s.executeQuery("select * from credit where id_invoice>=" + min_id_invoice + " and id_invoice<=" + max_id_invoice + "");
            System.out.println("select * from credit where id_invoice>=" + min_id_invoice + " and id_invoice<=" + max_id_invoice);

            while (outResult.next()) {

                if (outResult.getInt(1) < max_id_invoice && OK_IDS[outResult.getInt(1)]) {
                    Credit cre = new Credit(outResult.getInt(1), outResult.getString("NUMERO_AFFILIE"),
                            outResult.getString("NUMERO_QUITANCE"), outResult.getInt("SOLDE"), outResult.getInt("PAYED"));
                    cr.add(cre);
                } else {
                    System.err.println(outResult.getInt(1));
                }
            }
            outResult.close();
            //outResult = s.executeQuery("select * from  client where assurance='"+assurance+"' ");

            System.out.println("end:" + new Date());

            //t.sleep(5000); 
        } catch (Throwable e) {
            System.out.println(e + " k ");
        }

        try {
            String file = "RAPPORT_FACTURE.txt";
            File f = new File(file);
            PrintWriter sorti = new PrintWriter(new FileWriter(f));
            System.out.println(cr.size() + "CONV CREDIT " + new Date());
//        String k="ID_INVOICE#DATE#EMPLOYE#HEURE#ORDONNANCE#NUM_AFFILIATION#ASSURANCE#POURCENTAGE#NUM_AFFILIATION#ASSURANCE#CODE#DESIGNATION#NUM_LOT#QUANTITE#PRIX_VENTE#PRIX_REVIENT#FAMILLE";
//        sorti.println(k);

            //IMPORT CLIENT
            max_id_invoice = max_id_invoice + 1;

            boolean reba[] = new boolean[max_id_invoice];
            Invoice ensemble[] = new Invoice[max_id_invoice];

            System.out.println(in.size() + "max_id_invoice  " + max_id_invoice);

            for (int i = 0; i < in.size(); i++) {
                Invoice invoice2 = in.get(i);
                reba[invoice2.id_invoice] = true;
                ensemble[invoice2.id_invoice] = invoice2;
                //System.out.println(invoice2);
            }
            System.out.println(+in.size() + " INV" + new Date());

            for (int i = 0; i < cr.size(); i++) {
                Credit credit2 = cr.get(i);
                if (credit2.PAYED == 0 && credit2.SOLDE == 0) {
                    ensemble[credit2.ID_INVOICE].credit = null;

                } else {
                    ensemble[credit2.ID_INVOICE].credit = credit2;
                }
                //System.out.println(credit2);
            }

            //JOptionPane.showMessageDialog(null," ense INV "+new Date(),"",JOptionPane.PLAIN_MESSAGE);
            System.out.println(ensemble.length + " fin ensemble " + min_id_invoice + "**" + new Date());

            for (int i = min_id_invoice; i < ensemble.length; i++) {
                Invoice t = ensemble[i];
                //System.out.println(t.id_invoice);
                //System.out.println(reba[i]);
                if (reba[i]) {
                    //System.out.println(t.id_invoice);
//                for(int j=0;j<ensemble[i].;j++)   
//                { 
                    if (t != null && t.credit != null) {
                        Byose b = new Byose(t.id_invoice, t.id_client, t.date, t.heure, t.id_employe, t.tot, t.tva, t.credit.SOLDE, t.credit.PAYED, t.REDUCTION,
                                t.credit.NUMERO_AFFILIE, t.credit.NUMERO_QUITANCE);
                        System.out.println(t.tot + ":HAPA22:" + t.tva);
                        listlot.add(new Lot("" + t.id_invoice, t.id_client, t.date, t.id_employe, "" + t.credit.SOLDE, "" + t.credit.PAYED, "" + t.tot, "" + t.tva, "" + t.REDUCTION, t.credit.NUMERO_AFFILIE, t.credit.NUMERO_QUITANCE, "" + t.heure, "", "", ""));

                        yosePayed = yosePayed + ((t.credit.PAYED));
                        yoseSolde = yoseSolde + ((t.credit.SOLDE));
                        tvaYose = tvaYose + ((t.tva));
                        Yose = Yose + (((t.tot)));
                        //System.out.p';,lrintln(b+"");
                        ///db.insertByose(b);
                        sorti.println(b);
                    } else if (t != null) {
                        //System.err.println(t.id_client+"  cash  "+t.id_invoice);
                        Byose b = new Byose(t.id_invoice, t.id_client, t.date, t.heure, t.id_employe, t.tot, t.tva, 0, t.tot, t.REDUCTION,
                                "", "");
                        System.out.println(t.tot + ":CHCHCH22:" + t.tva);
                        listlot.add(new Lot("" + t.id_invoice, t.id_client, t.date, t.id_employe, "" + 0, "" + t.tot, "" + t.tot, "" + t.tva, "" + t.REDUCTION, "", "", "" + t.heure, "", "", ""));

                        yosePayed = yosePayed + ((t.tot));
                        tvaYose = tvaYose + ((t.tva));
                        Yose = Yose + (((t.tot)));
                        //System.out.println(b+"");
                        sorti.println(b);
                        ///db.insertByose(b);
                    } else {
                        System.err.println(i + "**" + t);
                    }
                }
            }
            sorti.close();
            ////////////////////////////////////////////////////////////////////////////////////
        } catch (Throwable ex) {
            JOptionPane.showMessageDialog(null, " Shida ", "" + ex, JOptionPane.PLAIN_MESSAGE);
            System.err.println("" + ex);
        }

        return listlot;
    }

    String l(String lang, String var) {
        return db.l(lang, var);
    }

    LinkedList crm() {
        LinkedList< Credit> cr2 = new LinkedList();
        listlot = new LinkedList();
        int max_id_invoice = 0;
        int min_id_invoice = 0;
        try {
            Thread t = new Thread();
            String Debut = JOptionPane.showInputDialog(l(lang, "V_DATEDEBUT"), Db.startTime());
            String FIN = JOptionPane.showInputDialog(l(lang, "V_DATEFIN"), Db.EndTime());
            System.out.println("start:" + new Date());
            outResult = s.executeQuery("select max (id_invoice) from APP.INVOICE where    invoice.HEURE >'" + Debut + "' AND invoice.HEURE <'" + FIN + "' AND COMPTABILISE!='ANNULE' AND NUM_CLIENT IS NOT NULL ");
            System.out.println("select max (id_invoice) from APP.INVOICE where  invoice.HEURE >'" + Debut + "' AND invoice.HEURE <'" + FIN + "' AND COMPTABILISE!='ANNULE' ");

            while (outResult.next()) {
                max_id_invoice = outResult.getInt(1) + 1;
            }
            outResult.close();
            System.out.println("max:" + max_id_invoice);

            outResult = s.executeQuery("select min (id_invoice) from APP.INVOICE where    invoice.HEURE >'" + Debut + "' AND invoice.HEURE <'" + FIN + "' AND COMPTABILISE!='ANNULE' AND NUM_CLIENT IS NOT NULL ");
            System.out.println("select min (id_invoice) from APP.INVOICE where    invoice.HEURE >'" + Debut + "' AND invoice.HEURE <'" + FIN + "' AND COMPTABILISE!='ANNULE'  ");

            while (outResult.next()) {
                min_id_invoice = outResult.getInt(1);
            }
            outResult.close();
            System.out.println("min:" + min_id_invoice);

            String sql = "select NUMERO_AFFILIE,sum(SOLDE+PAYED) as total from credit where id_invoice>="
                    + min_id_invoice + " and id_invoice<=" + max_id_invoice + "  group by NUMERO_AFFILIE order by total ";

            outResult = s.executeQuery(sql);
            while (outResult.next()) {
                Credit cre = new Credit(outResult.getInt(2), outResult.getString("NUMERO_AFFILIE"),
                        "", 0, 0);
                cr2.add(cre);
            }
            outResult.close();

            System.out.println(cr2.size() + " end credit :" + new Date());
        } catch (Throwable e) {
            System.out.println(e + " k ");
        }

        try {
            for (int i = 0; i < cr2.size(); i++) {
                Credit credit2 = cr2.get(i);

                try {
                    outResult = s.executeQuery("select * from APP.CLIENT where   "
                            + "NUM_AFFILIATION ='" + credit2.NUMERO_AFFILIE + "' ");
                    while (outResult.next()) {

                        listlot.add(new Lot("" + credit2.NUMERO_AFFILIE, outResult.getString("NOM_CLIENT"),
                                outResult.getString("PRENOM_CLIENT"), outResult.getString("ASSURANCE"),
                                outResult.getString("EMPLOYEUR"), outResult.getString("SECTEUR"),
                                outResult.getString("SEXE"), ""
                                + RRA_PRINT2.setVirgule((double) credit2.ID_INVOICE, 1, ""), "", "", "", "", "", "", ""));
                    }
                    outResult.close();

                } catch (Throwable ex) {
                    //  JOptionPane.showMessageDialog(null," Shida ",""+ex,JOptionPane.PLAIN_MESSAGE);
                    System.err.println(i + "  " + ex);
                }
            }

            System.out.println(" end Client :" + new Date());
        } catch (Throwable ex) {
            JOptionPane.showMessageDialog(null, " Shida ", "" + ex, JOptionPane.PLAIN_MESSAGE);
            System.err.println("" + ex);
        }

        return listlot;
    }

    public LinkedList<Byose> getRaport() {
        try {
// String  Debut = JOptionPane.showInputDialog(l(lang,  "V_DATEDEBUT"),db.getString("debut"));
// String FIN = JOptionPane.showInputDialog(l(lang,  "V_DATEFIN"),db.getString("fin"));

            YoseNtatva = 0;
            tvaYose = 0;
            totalTax = 0;
            Yose = 0;
            yosePayed = 0;
            yoseSolde = 0;
            if (umukozi.LEVEL_EMPLOYE >= 4 || umukozi.PWD_EMPLOYE.contains("RAPPORT")) {
                listlot = new LinkedList();
//System.out.println("select INVOICE.id_INVOICE,date,code,name_product,num_lot,quantite,list.price,list.prix_revient,employe,heure,num_client  from product,APP.INVOICE,APP.LIST where code=code_uni and  list.id_invoice=invoice.id_invoice  and invoice.HEURE >'"+Debut+"' AND invoice.HEURE <'"+FIN+"' ORDER BY invoice.ID_INVOICE");
                choixRpt = (String) JOptionPane.showInputDialog(null, l(lang,"V_CHOICE"), " ",
                        JOptionPane.QUESTION_MESSAGE, null, new String[]{l(lang,"V_FACTURE"), l(lang,"V_TOUS_VENTE"), "RRA REPORT", "CRM"},
                        l(lang,"V_TOUS_VENTE"));
//
//String sql="";

                if (choixRpt == null) {
                } else if (choixRpt.equals("CRM")) {
                    return crm();
                } else if (choixRpt.equals(l(lang,"V_FACTURE"))) {//FACTURE
                    return importerVentes();
                } else if (choixRpt.equals(l(lang,"V_TOUS_VENTE"))) {//TOUTES LES VENTES
                    return importer();
                }

            }
        } catch (Throwable e) {
            db.insertError(e + "", " V getlist");
        }
        return null;
    }

    LinkedList< Byose> importer() {
        LinkedList< Client> clie = new LinkedList();
        LinkedList< List> lis = new LinkedList();
        LinkedList< Credit> cr = new LinkedList();
        LinkedList< Invoice> in = new LinkedList();
        LinkedList< Product> pr = new LinkedList();
        LinkedList< Byose> byose = new LinkedList();
        Yose = 0;
        yosePayed = 0;
        yoseSolde = 0;
        listlot = new LinkedList();
        int max_id_invoice = 0;
        int min_id_invoice = 0;

        String selectedClient = "" + ((Assurance) societeJList.getSelectedValue()).sigle;
        int n = JOptionPane.showConfirmDialog(null, "DO YOU NEED " + selectedClient + " REPORT",
                " SELECTED CLIENT ", JOptionPane.YES_NO_OPTION);
        String reportClient = "";

        try {
            Thread t = new Thread();
            String Debut = JOptionPane.showInputDialog("ENTREZ LE TEMPS du debut", db.startTime());
            String FIN = JOptionPane.showInputDialog("ENTREZ LE TEMPS de la fin ", db.EndTime());

            if (n == 0) {
                reportClient = " NUM_CLIENT='" + selectedClient + "' AND ";
                oldReport = "TOUTES VENTES   \n CLIENT : " + selectedClient + " \n FROM " + Debut + " \n TO " + FIN;
            } else {
                oldReport = "TOUTES VENTES \n ALL CLIENTS  \nFROM " + Debut + " \nTO " + FIN;
            }

            System.out.println("start:" + new Date());
            outResult = s.executeQuery("select max (id_invoice) from APP.INVOICE where    invoice.HEURE >'" + Debut + "' AND invoice.HEURE <'" + FIN + "' AND COMPTABILISE!='ANNULE'  ");
            System.out.println("select max (id_invoice) from APP.INVOICE where  invoice.HEURE >'" + Debut + "' AND invoice.HEURE <'" + FIN + "' AND COMPTABILISE!='ANNULE'  ");

            while (outResult.next()) {
                max_id_invoice = outResult.getInt(1) + 1;
            }
            outResult.close();
            System.out.println("max:" + max_id_invoice);

            outResult = s.executeQuery("select min (id_invoice) from APP.INVOICE where   invoice.HEURE >'" + Debut + "' AND invoice.HEURE <'" + FIN + "' AND COMPTABILISE!='ANNULE'  ");
            System.out.println("select min (id_invoice) from APP.INVOICE where     invoice.HEURE >'" + Debut + "' AND invoice.HEURE <'" + FIN + "' AND COMPTABILISE!='ANNULE'  ");

            while (outResult.next()) {
                min_id_invoice = outResult.getInt(1);
            }
            outResult.close();
            System.out.println("min:" + min_id_invoice);
            boolean[] OK_IDS = new boolean[max_id_invoice];

            outResult = s.executeQuery("select * from APP.INVOICE where  " + reportClient + " "
                    + " invoice.HEURE >'" + Debut + "' AND invoice.HEURE <'" + FIN + "' AND COMPTABILISE!='ANNULE'  ");

            System.out.println("select * from APP.INVOICE where  " + reportClient + " "
                    + " invoice.HEURE >'" + Debut + "' AND invoice.HEURE <'" + FIN + "' AND COMPTABILISE!='ANNULE'  ");

            while (outResult.next()) {
                Invoice invo = new Invoice(outResult.getInt(1), outResult.getString("EMPLOYE"), outResult.getString("DATE"),
                        outResult.getString("NUM_CLIENT"), (int) outResult.getDouble("TOTAL"), outResult.getString("HEURE"),
                        (int) outResult.getDouble("TVA"), 0);
                in.add(invo);
                OK_IDS[outResult.getInt(1)] = true;
            }

            outResult.close();

            outResult = s.executeQuery("select * from credit where id_invoice>=" + min_id_invoice + " and id_invoice<=" + max_id_invoice + "");
            System.out.println("select * from credit where id_invoice>=" + min_id_invoice + " and id_invoice<=" + max_id_invoice);

            while (outResult.next()) {
                String numero_affilie = outResult.getString("NUMERO_AFFILIE").replaceAll("\'", "");
                if (outResult.getInt(1) < max_id_invoice && OK_IDS[outResult.getInt(1)]) {
                    Credit cre = new Credit(outResult.getInt(1), outResult.getString("NUMERO_AFFILIE"),
                            outResult.getString("NUMERO_QUITANCE"), outResult.getInt("SOLDE"), outResult.getInt("PAYED"));
                    cr.add(cre);
                } else {
                    System.err.println(outResult.getInt(1) + "**");
                }
            }
            outResult.close();
            System.out.println("select * from client" + new Date() + cr.size());
            //outResult = s.executeQuery("select * from  client where assurance='"+assurance+"' ");
            for (int i = 0; i < cr.size(); i++) {
                boolean found = false;

//            System.out.println("select * from  client  where num_affiliation='"+cr.get(i).NUMERO_AFFILIE +"'");
                outResult = s.executeQuery("select * from  client  where num_affiliation='" + cr.get(i).NUMERO_AFFILIE + "'");
                //  System.out.println("select * from  client  where num_affiliation='"+cr.get(i).NUMERO_AFFILIE +"'");
                while (outResult.next()) {
                    Client cli = new Client(outResult.getString("NUM_AFFILIATION"), outResult.getString("NOM_CLIENT"), outResult.getString("PRENOM_CLIENT"), outResult.getInt("PERCENTAGE"),
                            outResult.getInt("DATE_EXP"), outResult.getString("ASSURANCE"), outResult.getString("EMPLOYEUR"), outResult.getString("SECTEUR"), outResult.getString("VISA"), outResult.getString("CODE"));
                    clie.add(cli);
                    found = true;
                }
                if (found) {
                    continue;
                }

                outResult = s.executeQuery("select * from  client_rama where num_affiliation='" + cr.get(i).NUMERO_AFFILIE + "'");
                //System.out.println("select * from  client_rama where num_affiliation='"+cr.get(i).NUMERO_AFFILIE +"'");

                while (outResult.next()) {
                    Client cli = new Client(outResult.getString("NUM_AFFILIATION"), outResult.getString("NOM_CLIENT"), outResult.getString("PRENOM_CLIENT"), outResult.getInt("PERCENTAGE"),
                            outResult.getInt("DATE_EXP"), outResult.getString("ASSURANCE"), outResult.getString("EMPLOYEUR"), outResult.getString("SECTEUR"), outResult.getString("VISA"), outResult.getString("CODE"));
                    clie.add(cli);
                    found = true;
                }
                if (found) {
                    continue;
                }

                outResult = s.executeQuery("select * from  client_unipharma where num_affiliation='" + cr.get(i).NUMERO_AFFILIE + "'");
                //System.out.println("select * from  client_unipharma where num_affiliation='"+cr.get(i).NUMERO_AFFILIE +"'");
                while (outResult.next()) {
                    Client cli = new Client(outResult.getString("NUM_AFFILIATION"), outResult.getString("NOM_CLIENT"), outResult.getString("PRENOM_CLIENT"), outResult.getInt("PERCENTAGE"),
                            outResult.getInt("DATE_EXP"), outResult.getString("ASSURANCE"), outResult.getString("EMPLOYEUR"), outResult.getString("SECTEUR"), outResult.getString("VISA"), outResult.getString("CODE"));
                    clie.add(cli);
                }
                outResult.close();

            }
            System.out.println("end:" + new Date());

            outResult = s.executeQuery("select list.id_invoice,code_uni,num_lot,sum(quantite),price,PRIX_REVIENT from APP.LIST where id_invoice>=" + min_id_invoice + " and id_invoice<=" + max_id_invoice + " GROUP BY ID_INVOICE,CODE_UNI,NUM_LOT,PRICE,PRIX_REVIENT order by id_invoice");
            System.out.println("select list.id_invoice,code_uni,num_lot,sum(quantite),price,PRIX_REVIENT from APP.LIST where id_invoice>=" + min_id_invoice + " and id_invoice<=" + max_id_invoice + " GROUP BY ID_INVOICE,CODE_UNI,NUM_LOT,PRICE,PRIX_REVIENT order by id_invoice");
            String old = "";
            int oldQte = 0;
            String oldS = "";
            t.sleep(100);

            while (outResult.next()) {
                String lot2 = outResult.getString(3);
                int id_inv = outResult.getInt(1);
                String code = outResult.getString(2);
                double price = outResult.getDouble(5);
                int qte = outResult.getInt(4);
                double priceREVIENT = outResult.getDouble(6);

                if (id_inv > 0 && id_inv < max_id_invoice && OK_IDS[id_inv]) {
                    List li = new List(id_inv, price, qte, lot2, code, "", priceREVIENT);
                    lis.add(li);
                }
            }
            outResult = s.executeQuery("select ID_PRODUCT,NAME_PRODUCT,CODE,0,TVA,OBSERVATION,"
                    + "'-',FAMILLE "
                    + " from PRODUCT where STATUS='OK' OR STATUS='SOMMEIL'    ORDER BY NAME_PRODUCT ");
            System.out.println("select ID_PRODUCT,NAME_PRODUCT,CODE,0,TVA,OBSERVATION,"
                    + "'-',FAMILLE "
                    + " from PRODUCT where STATUS='OK' OR STATUS='SOMMEIL'    ORDER BY NAME_PRODUCT ");
            while (outResult.next()) {
                Product prod = new Product(outResult.getInt(1), outResult.getString(2), outResult.getString(3), 0, 0, outResult.getDouble(5), "", outResult.getString("OBSERVATION"),
                        outResult.getString("FAMILLE"), db, 0);
                pr.add(prod);
            }
            outResult.close();
            //t.sleep(5000); 
        } catch (Throwable e) {
            System.out.println(e + " k ");
        }

        try {
            String file = "RAPPORT" + ".txt";
            File f = new File(file);
            PrintWriter sorti = new PrintWriter(new FileWriter(f));
            System.out.println("CONV CREDIT " + new Date() + clie.size());
            String k = "ID_INVOICE#DATE#EMPLOYE#HEURE#NUM_AFFILIATION#NOM_AFFILIE#PRENOM_AFFILIE#POURCENTAGE#ORDONNANCE#ASSURANCE#CODE#DESIGNATION#NUM_LOT#QUANTITE#PRIX_VENTE#PRIX_REVIENT#FAMILLE";
            sorti.println(k);

            //IMPORT CLIENT
            HashMap<String, Client> clientSet = new HashMap();

            for (int i = 0; i < clie.size(); i++) {
                Client v = clie.get(i);
                //System.out.println(v);
                clientSet.put(v.NUM_AFFILIATION, v);
            }
            System.out.println("CONV LIST " + new Date());

            HashMap<String, Product> productSet = new HashMap();

            for (int i = 0; i < pr.size(); i++) {
                Product p = pr.get(i);
                productSet.put(p.code, p);
            }
            System.out.println("CONVERT " + new Date());
            /// insertion des donnÃƒÂ©es
            ////////////////////////////////////////////////////////////////////////////////

            max_id_invoice = max_id_invoice + 1;
            System.out.println(+clie.size() + " clients" + new Date());

            boolean reba[] = new boolean[max_id_invoice];
            Invoice ensemble[] = new Invoice[max_id_invoice];

            System.out.println(in.size() + "max_id_invoice  " + max_id_invoice);

            for (int i = 0; i < in.size(); i++) {
                Invoice invoice2 = in.get(i);
                reba[invoice2.id_invoice] = true;
                ensemble[invoice2.id_invoice] = invoice2;
                //System.out.println(invoice2);
            }
            System.out.println(+in.size() + " INV" + new Date());

            for (int i = 0; i < cr.size(); i++) {
                Credit credit2 = cr.get(i);
                ensemble[credit2.ID_INVOICE].credit = credit2;
                //System.out.println(credit2);
            }

            for (int i = 0; i < lis.size(); i++) {
                List listo = lis.get(i);
                ensemble[listo.id_invoice].listInvoice.add(listo);
            }
            System.out.println(lis.size() + " lists" + new Date());

            for (int i = 0; i < ensemble.length; i++) {
                if (reba[i]) {
                    for (int j = 0; j < ensemble[i].listInvoice.size(); j++) {
                        if (ensemble[i].credit != null) {
                            ensemble[i].client = clientSet.get(ensemble[i].credit.NUMERO_AFFILIE);
                            //System.out.println(ensemble[i].credit +" "+ensemble[i].client+" "+ensemble[i].credit.NUMERO_AFFILIE);
                        }
                        if (ensemble[i].listInvoice.get(j) != null) {
                            ensemble[i].listInvoice.get(j).product = productSet.get(ensemble[i].listInvoice.get(j).code);
                            //System.out.println(ensemble[i].listInvoice.get(j).product);
                        }
                    }
                }
            }
            //JOptionPane.showMessageDialog(null," ense INV "+new Date(),"",JOptionPane.PLAIN_MESSAGE);

            System.out.println(ensemble.length + " fin ensemble " + new Date());

            for (int i = min_id_invoice; i < ensemble.length; i++) {
                Invoice t = ensemble[i];
                //System.out.println(t);
                //System.out.println(reba[i]);
                if (reba[i]) {
                    for (int j = 0; j < ensemble[i].listInvoice.size(); j++) {
                        List l = ensemble[i].listInvoice.get(j);
                        //System.err.println(t); 
                        //System.out.println(t.client); 
                        //System.err.println(t.credit);
                        String name = "";
                        String famille = "";
                        if (l.product != null) {
                            name = l.product.productName;
                            famille = l.product.codeSoc;
                        }

                        if (t != null && t.client != null && t.credit != null && l != null) {
                            Byose b = new Byose(t.id_invoice, t.date, t.heure, t.id_employe, t.credit.NUMERO_AFFILIE, t.client.NOM_CLIENT, "AGE", t.client.SEXE,
                                    t.client.PRENOM_CLIENT, t.client.EMPLOYEUR, t.client.PERCENTAGE, t.credit.NUMERO_QUITANCE,
                                    t.id_client, l.code, name, 0, l.lot, l.qty, l.price, l.prix_revient, l.qty * l.price, famille, t.credit.PAYED, t.credit.SOLDE, l.product.tva);

                            YoseNtatva = (YoseNtatva + ((l.price * l.qty)));
                            tvaYose = (tvaYose + ((l.prix_revient * l.qty)));
                            Yose = (Yose + (((l.price - l.prix_revient) * l.qty)));

                            listlot.add(new Lot("" + t.id_invoice, t.date, l.code, name, l.lot, "" + l.qty, "" + l.price,
                                    "" + l.prix_revient, t.id_employe, t.heure, t.id_client, "" + l.product.tva, "", "", ""));

                            //System.out.p';,lrintln(b+"");
                            ///db.insertByose(b);
                            sorti.println(b);
                            byose.add(b);
                        } else if (t != null && l != null) {
                            //System.err.println(t.id_client+"  cash  "+t.id_invoice);
                            Byose b = new Byose(t.id_invoice, t.date, t.heure, t.id_employe, "", "", "", " ", "",
                                    "", 0, "",
                                    t.id_client, l.code, name, 0, l.lot, l.qty, l.price, l.prix_revient, l.qty * l.price, famille, 0, 0, l.product.tva);
                            byose.add(b);
                            //System.out.println("C:"+t.id_invoice);

                            YoseNtatva = (YoseNtatva + ((l.price * l.qty)));
                            tvaYose = (tvaYose + ((l.prix_revient * l.qty)));
                            Yose = (Yose + (((l.price - l.prix_revient) * l.qty)));

                            listlot.add(new Lot("" + t.id_invoice, t.date, l.code, name, l.lot, "" + l.qty, "" + l.price, "" + l.prix_revient, t.id_employe, t.heure, t.id_client, "" + l.product.tva, "", "", ""));

                            //System.out.println(b+"");
                            sorti.println(b);
                            ///db.insertByose(b);
                        } else {
                            System.err.println(i + "" + t);
                        }

                    }
                }
            }
            sorti.close();
            ////////////////////////////////////////////////////////////////////////////////////
        } catch (Throwable ex) {
            JOptionPane.showMessageDialog(null, " Shida ", "" + ex, JOptionPane.PLAIN_MESSAGE);
            System.err.println("" + ex);
        }

        return byose;
    }

    public JPanel RadioButton() {
//Create the radio buttons.
        JRadioButton ALL = new JRadioButton("ALL");
        ALL.setMnemonic(KeyEvent.VK_A);
        ALL.setActionCommand("ALL");
        JRadioButton GROUPE = new JRadioButton("GROUPE");
        GROUPE.setMnemonic(KeyEvent.VK_G);
        GROUPE.setActionCommand("GROUPE");
        JRadioButton GENERAL = new JRadioButton("GENERAL");
        GENERAL.setMnemonic(KeyEvent.VK_E);
        GENERAL.setActionCommand("GENERAL");
        JRadioButton RRA = new JRadioButton("RRA");
        GROUPE.setMnemonic(KeyEvent.VK_R);
        GROUPE.setActionCommand("RRA");
        JRadioButton CLASSIC = new JRadioButton("CLASSIC");
        CLASSIC.setMnemonic(KeyEvent.VK_C);
        CLASSIC.setActionCommand("CLASSIC");
        CLASSIC.setSelected(true);

        JRadioButton NORMALE = new JRadioButton("NORMAL");
        NORMALE.setMnemonic(KeyEvent.VK_C);
        NORMALE.setActionCommand("NORMAL");
        NORMALE.setSelected(true);

        ButtonGroup group = new ButtonGroup();
        if (choixRpt.equals("TOUTES LES VENTES")) {
            group.add(CLASSIC);
            group.add(ALL);
            group.add(GROUPE);
            group.add(GENERAL);

//Register a listener for the radio buttons.
            CLASSIC.addActionListener(this);
            ALL.addActionListener(this);
            GROUPE.addActionListener(this);
            GENERAL.addActionListener(this);

//Put the radio buttons in a column in a panel.
            JPanel radioPanel = new JPanel(new GridLayout(2, 2));
            radioPanel.add(CLASSIC);
            radioPanel.add(ALL);
            radioPanel.add(GROUPE);
            radioPanel.add(GENERAL);

            add(radioPanel, BorderLayout.LINE_START);

            return radioPanel;
        } else {
            group.add(NORMALE);
            group.add(RRA);

//Register a listener for the radio buttons.
            NORMALE.addActionListener(this);
            RRA.addActionListener(this);

//Put the radio buttons in a column in a panel.
            JPanel radioPanel = new JPanel(new GridLayout(2, 1));
            radioPanel.add(NORMALE);
            radioPanel.add(RRA);

            add(radioPanel, BorderLayout.LINE_START);

            return radioPanel;

        }
//  setBorder(BorderFactory.createEmptyBorder(20,20,20,20));

    }

    public int getMaxTrack() {
        int max = 0;
        try {

            outResult = s.executeQuery("select max(id_track) from  TRACKING_CLOUD ");
            while (outResult.next()) {
                max = outResult.getInt(1);
            }

        } catch (SQLException ex) {
            db.insertError(ex + "", " maxTrack");
        }
        return max;
    }

    private JTable speciale2(LinkedList<Lot> listlot) {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable = new javax.swing.JTable();
        int linge = listlot.size();
        String pr_col="LASTNAME";
        if(((Assurance) societeJList.getSelectedValue()).sigle.equals("RAMA"))
        {
            pr_col="BENEFICIAIRE";
        }
        String[][] s1 = new String[linge+1][17];
        String[] title = new String[]{
            "DATE", "ID", "NAME",pr_col, "AFFILIATE N°", "AGE", "SEXE", "DRUGS", "QTY", "PU", "PT", "ASS", "BON", "Prescriber", "Health Facility", "OM"
        };
        Lot ll=new Lot("ID", "DATE", "BON", "Prescriber", "Health Facility", "OM", "AFFILIATE N°", "NAMES",
                "DRUGS", "QTY", "PU", "PT", "ASS", "SEXE", "AGE");
        ll.u16=pr_col;
        
        listlot.addFirst(ll); 

        for (int i = 0; i < linge+1; i++) {
            Lot l = listlot.get(i);

            s1[i][0] = l.u2;  //id 
            s1[i][1] = l.u1; //date

            s1[i][2] = l.u8; // bon ;
            s1[i][3]=l.u16;
            s1[i][4] = l.u7;  //Prescriber 
            s1[i][5] = l.u15;  // healt 
            s1[i][6] = l.u14;  // om 
            s1[i][7] = l.u9;
            s1[i][8] = l.u0;
            s1[i][9] = l.u11;
            s1[i][10] = l.u12;
            s1[i][11] = l.u13;
            s1[i][12] = l.u3;
            s1[i][13] = l.u4;
            s1[i][14] = l.u5;
            s1[i][15] = l.u6;

        }
        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        jTable.setModel(new javax.swing.table.DefaultTableModel(s1, title));
        jTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        // jTable.getColumnModel().getColumn(1).setPreferredWidth(250);//.setWidth(200);
        jTable.doLayout();
        jTable.validate();

        return jTable;
    }// </editor-fold>

    LinkedList<Lot> makePrint2(String d) {

        String option = (String) JOptionPane.showInputDialog(null, l(lang, "V_CHOICE"), "PRINT",
                JOptionPane.QUESTION_MESSAGE, null, new String[]{"S TOTAL", "GENERAL"}, nom);

        getCredit(d,option);
        LinkedList<Lot> lotList = new LinkedList();
        int totalG = 0;
        int tvaG = 0;

        for (int i = 0; i < credit.size(); i++) {
            
            double total = 0;
            double tva = 0;
            int k = 0;
            Credit cre = credit.get(i);
            // System.out.println(cre.NOM);
            double percentage = ((double) cre.SOLDE / (double) (cre.SOLDE + cre.PAYED));
            String cree = "";
            try {
                outResult = s.executeQuery("select PRODUCT.NAME_PRODUCT,LIST.PRICE,LIST.QUANTITE,code_soc_rama from  list,product WHERE list.id_invoice=" + cre.ID_INVOICE + " and  LIST.CODE_UNI=PRODUCT.CODE");
                while (outResult.next()) {
                    double price2 = (outResult.getDouble(2));
                    double tot = (price2 * outResult.getInt(3));
                    double assur = ((tot * percentage) + 0.05);

                    String bon = cre.NUMERO_QUITANCE;
                    String medecin = "";
                    String agent = "";
                    String poste = "";
                    if (k == 0) {

                        if (((Assurance) societeJList.getSelectedValue()).sigle.equals("MMI")||((Assurance) societeJList.getSelectedValue()).sigle.equals("RAMA")) {

                            //  sdsds2-kirezi-.......-chk-.......- 
                            cree = cre.NUMERO_QUITANCE;
                            //  System.out.println(cre.NUMERO_QUITANCE);
                            String quit = cre.NUMERO_QUITANCE.replaceAll("-..-", "-");
       // System.out.println(quit);

                            //661212-IMMACULEE-..-KANOMBE-..-UWERA 
                            String st1[] = quit.split("-");

                            if (st1.length == 4) {
                                bon = st1[0];
                                medecin = st1[1];
                                agent = st1[2];
                                poste = st1[3];

                            }
                            else
                            {
                                bon=quit;
                            }

                            Lot l=new Lot("" + cre.ID_INVOICE, cre.NUM_CLIENT, bon, medecin, agent, poste, cre.NUMERO_AFFILIE,
                                    cre.NOM, outResult.getString(1), "" + outResult.getInt(3),
                                    "" + price2, "" + tot, "" + assur, cre.SEXE, cre.AGE);
                            l.u16=cre.PRENOM;
                            lotList.add(l);

                        }
                    } else {
                        if (option.equals("GENERAL")) {
                            if (((Assurance) societeJList.getSelectedValue()).sigle.equals("MMI")||((Assurance) societeJList.getSelectedValue()).sigle.equals("RAMA")) {

          //  sdsds2-kirezi-.......-chk-.......- 
                                //DR HATEGEKA-309970-..-CMP-..-0337
                                //661212-IMMACULEE-..-KANOMBE-..-UWERA 
                                //785305--..-P-..-RUBAGANGARA
                                //   ordonnance = (JOptionPane.showInputDialog(frame, l(lang, "V_NUMERO") + " " + l(lang, "V_ORDONNANCE") + "  " + c.ASSURANCE, "" + ordonnance));
                                //   ordonnance=ordonnance+"-"+ (JOptionPane.showInputDialog(frame, " Prescriber  "+c.ASSURANCE));
                                //   ordonnance=ordonnance+"-..";
                                //   ordonnance=ordonnance+"-"+ (JOptionPane.showInputDialog(frame, " Health Facility  "+c.ASSURANCE,"P"));
                                //   ordonnance=ordonnance+"-..";
                                //   ordonnance=ordonnance+"-"+ (JOptionPane.showInputDialog(frame, " Numéro Ordre des Médecins  ",c.NOM_CLIENT));
                                //   ordonnance= JOptionPane.showInputDialog(frame, " CONFIRME OU CHANGE SANS TOUCHER LES -  ",ordonnance);
                                cree = cre.NUMERO_QUITANCE;
                                //  System.out.println(cre.NUMERO_QUITANCE);
                                String quit = cre.NUMERO_QUITANCE.replaceAll("-..-", "-");
       // System.out.println(quit);

                                //661212-IMMACULEE-..-KANOMBE-..-UWERA 
                                String st1[] = quit.split("-");

                                if (st1.length == 4) {
                                    bon = st1[0];
                                    medecin = st1[1];
                                    agent = st1[2];
                                    poste = st1[3]; 
                                }
                                else
                            {
                                bon=quit;
                            }
                            }
                            Lot l=new Lot("" + cre.ID_INVOICE, cre.NUM_CLIENT, bon, medecin, agent, poste, cre.NUMERO_AFFILIE,
                                    cre.NOM, outResult.getString(1), "" + outResult.getInt(3),
                                    "" + price2, "" + tot, "" + assur, cre.SEXE, cre.AGE);
                            l.u16=cre.PRENOM;
                            lotList.add(l);
                        }
                        if (option.equals("S TOTAL")) {
                            lotList.add(new Lot("", "", "", "", "", "", "", "", outResult.getString(1),
                                    "" + outResult.getInt(3), "" + price2, "" + tot, ""+ assur, "", ""));

                        }
                    }

                    total = total + assur;
                    k++;
                }
            } catch (Throwable ex) {
                JOptionPane.showMessageDialog(null, " " + cree, "erreur", JOptionPane.PLAIN_MESSAGE);
                System.out.println(ex);
            }
//            if (option.equals("S TOTAL")) {
//                lotList.add(new Lot("", "", "", "", "", "", "", "", "     -------------------", "0", "0", "" , "", ""+ (int) total, ""));
//            }

            totalG = totalG + (int) total;
            tvaG = tvaG + (int) tva;
        }
        String[] footer = new String[0];
        Assurance c = (Assurance) societeJList.getSelectedValue();
        Client cli = new Client("", c.adresse, c.sigle, 0, 0, c.designation, "", "", "", "");
        return lotList;
    }

    public static void main(String[] arghs) throws FileNotFoundException, IOException {

//   Employe umukozi=  new Employe(1,2,0,0,"h","s","b","x","o","","","");
//   VoirFichier v= new VoirFichier(umukozi,"localhost","MUHIRE"); 
    }

}
