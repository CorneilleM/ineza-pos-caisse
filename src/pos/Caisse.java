package pos;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.WindowEvent;
import java.util.LinkedList;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;

public class Caisse extends JFrame {

    LinkedList<Employe> abakozi;
    LinkedList<Assurance> societe;
    LinkedList<String> amatariki;
    int[] cashSociete; //Here is the syntax for declaring an array variable cashSociete and specifying the type of array int
    int[] creditSociete;//Here is the syntax for declaring an array variable creditSociete and specifying the type of array int
    String[][] s;//Here is the syntax for declaring a two-dimensional array variable s and specifying the type of array string kuko jtable ifata string
    String[] soci;
    JTable jTable;
    Db db;
    int cashYumukozi, creditYumukozi;
    int cashYose, creditYose;
    String date;
    String sqldate = "";
    String fin;
    
    Caisse(String date, String serv, String dbName, String Fin,boolean t) {
        this.db = new Db(serv, dbName);
        this.date = date;
        this.fin = Fin;
        
        db.read();
        cashYose = 0;
        creditYose = 0;
        amatariki();
        societe();
        cashSociete = new int[societe.size() + 2];
        creditSociete = new int[societe.size() + 2];
        
        s = new String[amatariki.size() + 2][societe.size() * 2 + 3];
        soci = new String[societe.size() * 2 + 3];
        
        faireRapportCaisse();
        Container content = getContentPane();
        content.add(showCaisse());
        setTitle(" Show Caisse comptable ");// kwandika titre ya tableau
        enableEvents(WindowEvent.WINDOW_CLOSING);
//on fixe  les composant del'interface
        setSize(800, 800);
        Image im = Toolkit.getDefaultToolkit().getImage("logo.png");
        this.setIconImage(im);
        setVisible(true);
        
        
    }

    Caisse(String date, String serv, String dbName, String Fin) {
        this.db = new Db(serv, dbName);
        this.date = date;
        this.fin = Fin;
        if (!Fin.equals("")) {
            sqldate = " and HEURE>'" + date + "' AND HEURE<'" + Fin + "'";
        } else if (Fin.equals("")) {
            sqldate = " and date='" + date + "'";
        }
        db.read();
        cashYose = 0;
        creditYose = 0;
        abakozi();
        societe();
        cashSociete = new int[societe.size() + 2];
        creditSociete = new int[societe.size() + 2];

        // ikora array y'ubwoko bwa entier ifite uburebure bwa societe
        s = new String[abakozi.size() + 2][societe.size() * 2 + 3];
        soci = new String[societe.size() * 2 + 3];
        faireCaisse(date);
        Container content = getContentPane();
        content.add(showCaisse());
        setTitle(" Show Caisse comptable ");// kwandika titre ya tableau
        enableEvents(WindowEvent.WINDOW_CLOSING);
//on fixe  les composant del'interface
        setSize(800, 800);
        Image im = Toolkit.getDefaultToolkit().getImage("logo.png");
        this.setIconImage(im);
        setVisible(true);
//print();
    }

    void faireCaisse(String date) {
        soci[0] = "SOCIETE";//kwandika SOCIETE kuri tire y'amasociete
        for (int k = 1; k <= societe.size(); k++) {
            Assurance a = societe.get(k - 1);
            soci[(2 * k) - 1] = "C" + a.sigle;// kongeraho C kuri sigle y'amasociete
            soci[(2 * k)] = "R" + a.sigle;// kongera R kuri sigle y'amasociete
            s[0][(2 * k) - 1] = "C" + a.sigle;
            s[0][(2 * k)] = "R" + a.sigle;
        }
        s[0][(2 * societe.size() + 1)] = "TOT.CASH";
        s[0][(2 * societe.size() + 1) + 1] = "TOT.CREDIT";

        //kwandika amazina y'abakozi
        for (int i = 0; i < abakozi.size(); i++) {
            s[i + 1][0] = abakozi.get(i).NOM_EMPLOYE;
        }

        for (int i = 0; i < abakozi.size(); i++) {
            Employe e = abakozi.get(i);
            System.out.println(e.NOM_EMPLOYE);
            cashYumukozi = 0;
            creditYumukozi = 0;

            for (int k = 0; k < societe.size(); k++) {
                Assurance a = societe.get(k);
//System.out.print("   "+a.sigle+"  ");
                String sql = "";
                if (a.sigle.equals("CASHNORM")) {
                    sql = "Select sum(Total) from INVOICE where NUM_CLIENT='CASHNORM' AND COMPTABILISE!='ANNULE' and EMPLOYE= '" + e.NOM_EMPLOYE + "'" + sqldate;
                    System.out.println(sql);
                    cashnorm(sql, i, k + 1);
                } else {
                    sql = "Select sum(payed),sum(solde)from INVOICE,CREDIT WHERE INVOICE.ID_INVOICE=CREDIT.ID_INVOICE AND COMPTABILISE!='ANNULE' and NUM_CLIENT='" + a.sigle + "' and EMPLOYE='" + e.NOM_EMPLOYE + "'" + sqldate;
                    System.out.println(sql);
                    credit(sql, i, k + 1);
                }
            }

            s[i + 1][(societe.size() * 2) + 1] = "" + cashYumukozi;
            s[i + 1][(societe.size() * 2) + 2] = "" + creditYumukozi;
            System.out.println("  creditYumukozi  " + creditYumukozi);
        }

        int k = abakozi.size();
// gushiraho total z'amasociete muri s
        for (int i = 0; i <= (societe.size()); i++) {
            s[k + 1][(2 * i) + 1] = "" + cashSociete[i];
            s[k + 1][(2 * i)] = "" + creditSociete[i];
            cashYose = cashYose + cashSociete[i];
            creditYose = creditYose + creditSociete[i];
        }
        s[k + 1][(societe.size() * 2) + 1] = "" + cashYose;
        s[k + 1][(societe.size() * 2) + 2] = "" + creditYose;
        s[abakozi.size() + 1][0] = "TOTAL";

    }
    
    void faireRapportCaisse() {
        soci[0] = "SOCIETE";//kwandika SOCIETE kuri tire y'amasociete
        for (int k = 1; k <= societe.size(); k++) {
            Assurance a = societe.get(k - 1);
            soci[(2 * k) - 1] = "C" + a.sigle;// kongeraho C kuri sigle y'amasociete
            soci[(2 * k)] = "R" + a.sigle;// kongera R kuri sigle y'amasociete
            s[0][(2 * k) - 1] = "C" + a.sigle;
            s[0][(2 * k)] = "R" + a.sigle;
        }
        s[0][(2 * societe.size() + 1)] = "TOT.CASH";
        s[0][(2 * societe.size() + 1) + 1] = "TOT.CREDIT";

        //kwandika amatariki
        for (int i = 0; i < amatariki.size(); i++) {
            s[i + 1][0] = amatariki.get(i);
        }
        System.out.println("amatariki size:"+amatariki.size());
        for (int i = 0; i < amatariki.size(); i++) {
            String e = amatariki.get(i);
            System.out.println(i+":date:"+e);
            cashYumukozi = 0;
            creditYumukozi = 0;
            String debut=smartDateFormat(e)+" 00:00:00";
            String Fin=smartDateFormat(e)+" 23:59:59";
            if (!Fin.equals("")) {
            sqldate = " and HEURE>'" + debut + "' AND HEURE<'" + Fin + "'";
            } 

            for (int k = 0; k < societe.size(); k++) {
                Assurance a = societe.get(k);
//System.out.print("   "+a.sigle+"  ");
                String sql = "";
                if (a.sigle.equals("CASHNORM")) {
                    sql = "Select sum(Total) from INVOICE where NUM_CLIENT='CASHNORM' AND COMPTABILISE!='ANNULE' " + sqldate;
//                    System.out.println(sql);
                    cashnorm(sql, i, k + 1);
                } else {
                    sql = "Select sum(payed),sum(solde)from INVOICE,CREDIT WHERE INVOICE.ID_INVOICE=CREDIT.ID_INVOICE AND COMPTABILISE!='ANNULE' and NUM_CLIENT='" + a.sigle + "' " + sqldate;
//                    System.out.println(sql);
                    credit(sql, i, k + 1);
                }
            }

            s[i + 1][(societe.size() * 2) + 1] = "" + cashYumukozi;
            s[i + 1][(societe.size() * 2) + 2] = "" + creditYumukozi;
            System.out.println("  creditYumukozi  " + creditYumukozi);
        }

        int k = amatariki.size();
// gushiraho total z'amasociete muri s
        for (int i = 0; i <= (societe.size()); i++) {
            s[k + 1][(2 * i) + 1] = "" + cashSociete[i];
            s[k + 1][(2 * i)] = "" + creditSociete[i];
            cashYose = cashYose + cashSociete[i];
            creditYose = creditYose + creditSociete[i];
        }
        s[k + 1][(societe.size() * 2) + 1] = "" + cashYose;
        s[k + 1][(societe.size() * 2) + 2] = "" + creditYose;
        s[amatariki.size() + 1][0] = "TOTAL";

    }
    
    void abakozi() {

        abakozi = new LinkedList();
        try {
            System.out.println("date: " + date.length());
            if (date.length() == 6) {
                System.out.println("select * from APP.EMPLOYE where nom_employe in(select distinct employe from invoice where date='" + date + "' )");
                db.outResult = db.s.executeQuery("select * from APP.EMPLOYE where nom_employe in(select distinct employe from invoice where date='" + date + "' )");
            } else {
                System.out.println("select * from APP.EMPLOYE where nom_employe in(select distinct employe from invoice where HEURE>'" + date + "' AND HEURE<'" + fin + "')");
                db.outResult = db.s.executeQuery("select * from APP.EMPLOYE where nom_employe in(select distinct employe from invoice where HEURE>'" + date + "' AND HEURE<'" + fin + "')");
            }

            while (db.outResult.next()) {
                abakozi.add(new Employe(db.outResult.getInt(1), db.outResult.getInt(5), db.outResult.getInt(6), db.outResult.getInt(9), db.outResult.getString(2), db.outResult.getString(3), db.outResult.getString(4), db.outResult.getString(7), db.outResult.getString(8), db.outResult.getString(10), db.outResult.getString(11), db.outResult.getString(12)));
                System.out.println(db.outResult.getString(2));
            }
            System.out.println("ABAKOZI DUFITE:" + abakozi.size());
            //  Close the resultSet
            db.outResult.close();

            //  Beginning of the primary catch block: uses errorPrint method
        } catch (Throwable e) {
            //insertError(e+"","employe");
        }
    }

    void amatariki() {

        amatariki = new LinkedList();
        try {
            System.out.println("date: " + date.length());
             
            System.out.println("select DISTINCT INVOICE.DATE  from invoice where HEURE>'" + date + "' AND HEURE<'" + fin + "' order by INVOICE.DATE");
            db.outResult = db.s.executeQuery("select DISTINCT INVOICE.DATE  from invoice where HEURE>'" + date + "' AND HEURE<'" + fin + "' AND DATE IS NOT NULL order by INVOICE.DATE");
             

            while (db.outResult.next()) {
                amatariki.add(db.outResult.getString(1));
                //abakozi.add(new Employe(db.outResult.getInt(1), db.outResult.getInt(5), db.outResult.getInt(6), db.outResult.getInt(9), db.outResult.getString(2), db.outResult.getString(3), db.outResult.getString(4), db.outResult.getString(7), db.outResult.getString(8), db.outResult.getString(10), db.outResult.getString(11), db.outResult.getString(12)));
                //System.out.println(db.outResult.getString(2));
            }
            System.out.println("AMATARIKI DUFITE:" + amatariki.size());
            //  Close the resultSet
            db.outResult.close();

            //  Beginning of the primary catch block: uses errorPrint method
        } catch (Throwable e) {
            //insertError(e+"","employe");
        }
    }

    LinkedList<Assurance> societe() {
        societe = new LinkedList();
        try {
            db.outResult = db.s.executeQuery("select NOM_VARIABLE,value_variable,value2_variable from APP.VARIABLES WHERE FAMILLE_VARIABLE='CLIENT' ");
            while (db.outResult.next()) {
                societe.add(new Assurance(db.outResult.getString(1), db.outResult.getString(2), db.outResult.getString(3), "", ""));
            }
            db.outResult.close();
        } catch (Throwable e) {
            db.insertError(e + "", " getclient");
        }
        return societe;
    }

    void cashnorm(String sql, int i, int k) {
        try {
            db.outResult = db.s.executeQuery(sql);
            while (db.outResult.next()) {
                if (db.outResult.getInt(1) > 0) {
                    System.out.print("  i " + i + "   " + 2 * k + " 2*k  " + db.outResult.getInt(1) + "   " + 0 + "  ");
                }
                s[i + 1][(2 * k) - 1] = "" + db.outResult.getInt(1);
                s[i + 1][(2 * k)] = "" + 0;
                cashYumukozi = cashYumukozi + db.outResult.getInt(1);
                cashSociete[k - 1] = cashSociete[k - 1] + db.outResult.getInt(1);
            }
            db.outResult.close();
        } catch (Throwable e) {
            db.insertError(e + "", " cash");
        }
    }

    void credit(String sql, int i, int k) {
        try {
            db.outResult = db.s.executeQuery(sql);
            while (db.outResult.next()) {
//    if(db.outResult.getInt(2)>0)
//        //System.out.print("  i "+i+"   "+2*k+" 2*k  "+db.outResult.getInt(1)+"   "+db.outResult.getInt(2)+"  ");

                s[i + 1][(2 * k) - 1] = "" + db.outResult.getInt(1);
                s[i + 1][(2 * k)] = "" + db.outResult.getInt(2);
                creditYumukozi = creditYumukozi + db.outResult.getInt(2);
                cashYumukozi = cashYumukozi + db.outResult.getInt(1);
                cashSociete[k - 1] = cashSociete[k - 1] + db.outResult.getInt(1);
                creditSociete[k] = creditSociete[k] + db.outResult.getInt(2);
            }
            db.outResult.close();
        } catch (Throwable e) {
            db.insertError(e + "", " credit");
        }
    }
    
public String smartDateFormat(String theDate)
{
    //System.out.println("before:"+(theDate.length()));
    String yy=theDate.substring(theDate.length()-2,theDate.length());
    //System.out.println("yy:"+(yy));
    String mm=theDate.substring(theDate.length()-4,theDate.length()-2);
    //System.out.println("mm:"+(mm));
    String dd=theDate.substring(0,theDate.length()-4);
    //System.out.println("hapa:"+(dd));  
    //A regler en 2100
    return "20"+yy+"-"+mm+"-"+dd;
}

    JScrollPane showCaisse() {
        JTable jTable = new javax.swing.JTable();
        jTable.setModel(new javax.swing.table.DefaultTableModel(s, soci));
        jTable.setAutoResizeMode(jTable.AUTO_RESIZE_OFF);
        for (int i = 0; i < societe.size(); i++) {
            jTable.getColumnModel().getColumn(i).setPreferredWidth(80);
        }

        jTable.doLayout();
        jTable.setRowHeight(20);
        jTable.validate();
        JScrollPane jScrollPane1 = new javax.swing.JScrollPane(jTable);
        jScrollPane1.setPreferredSize(new Dimension(400, 400));
        return jScrollPane1;
    }// </editor-fold>

    public static void main(String[] arghs) {
        new Caisse("290610", "localhost", "Score", "");
    }
}