 

package pos;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import javax.swing.JPanel;

/**
 *
 * @author w
 */
 

class Getpicture extends JPanel {
//iyi class izacontrola ibintu byose bigendanye n' amafoto
  private Image img;

//  public ImagePanel(String img) {
//      this(new ImageIcon(img).getImage());
//
//       setLayout(new FlowLayout(FlowLayout.LEFT ,10,20));
//
//  }

  public Getpicture(Image img) {
    this.img = img;
    Dimension size = new Dimension(img.getWidth(this), img.getHeight(this));
    setPreferredSize(size);
    setMinimumSize(size);
    setMaximumSize(size);
    setSize(size);
    setLayout(null);
  }

  public void paintComponent(Graphics g) {
    g.drawImage(img, 0, 0, null);
  }

}
