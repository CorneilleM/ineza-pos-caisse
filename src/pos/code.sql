
package test;

/*
Interface Ishyiga copyright 2007 Kimenyi Aimable Gestion des ventes et du stock
*/
import java.io.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import javax.swing.JFrame;
import org.apache.derby.drda.NetworkServerControl;

public  class Main extends JFrame
{
    private  String nom,stock;                                // le nom du Caissier
    private int v;
    final int maxCode;
    final int LENTH=800;
    final int WIDT =1100;
    private JLabel labelProduit= new JLabel("STOCK ");
    private  int totalprice,tvaPrice;
    private  int k=1;

    private  JButton sendButton;                       // le boutton qui envoie la selection
    private  JButton removeButton;                     // le boutton qui enleve le produit
    private  JButton addButton;                        // le botton qui ajoute les produits
    private  JButton  cashButton,creditButton,scanButton ;					   // Pour gerer les vents

        private  JButton  stockButton ;
	private  JButton  searchButton;
	private  JButton  venteButton;
	private  JButton  actButton;
	private  JButton  annuleButton;
    private  JButton  rama,corar,soras,mmi,uni,societe,farg,aar ;
    private  JButton newClient,newProduct;
    private  NetworkServerControl server;
    private  JButton  unVedette ;
	private  JButton  deuxVedette;
	private  JButton  troisVedette;
	private  JButton  quatreVedette,cinqVedette,sixVedette,septVedette,huitVedette;
    String currentScan="";
    String ss="";
    int comp =0;
	private	JButton b1,b2,b3,b4,b5,b6,b7,b8,b9,b10,b11,b12;
	private int     i1,i2,i3,i4,i5,i6,i7,i8,i9,i10,i11,i12;
	private static JFrame frame;							   // cette fenetre


    private  JList  allProductList=new JList();        // un JList qui affiche tous tout les produits
    private  JList  saleJList=new JList();             // un JList qui affiche juste vente en cours
    private  JTextField texttoBESent = new JTextField();// la ou on ecrie les produits et leurs quantites
    private  JTextField search = new JTextField();
    private  JTextArea  total = new JTextArea();        // Gere le prix Total
    JTextArea textArea = new JTextArea();
    private  JScrollPane  scrollPaneProductList;     	// si il ya trop des produits qui depasse le Jpanel
    private  JScrollPane  scrollPaneallSale;   			// si il ya trop des vente qui depasse le cadre
    private  JPanel productPane ;
    private  JPanel salePane ;                      	//les panels qui composent la fenetre
    private  JPanel southPane ;
    private  JPanel pricePane;
    private  JPanel centerPane;
    private Cashier cashier; 							// Caisse courant
    private Properties properties = new Properties();

     private int today;
     private int virgule=1;
     int voirClient=0;
     Client currentClient= new Client("","" ,"" ,15,0,"" ,"","","","","","" );
     String serv,dataBase;
     Employe umukozi;
/********************************************************************************
le constructeur qui cree un frame
********************************************************************************/
  Main(Employe umukozi,Cashier c,String serv,String dataBase )
  {
      this.dataBase=dataBase;
         this.serv=serv;
            frame=this;
            v=0;
            stock="CASHNORM";
            this.umukozi=umukozi;
            nom=umukozi.NOM_EMPLOYE;
            cashier= c;
            maxCode=cashier.allProduct.size();
            showProduct();
            draw();
            setVedette(1);
            cashier.basketStart(stock);
            String datedoc=cashier.date;
            String d=""+datedoc.substring(4)+""+datedoc.substring(2, 4)+""+datedoc.substring(0,2);

        try
        {
        Integer hhh=new Integer (d);
        today = hhh.intValue();
        JOptionPane.showMessageDialog(null,"         Dutangiranye : "+new Date().toLocaleString()+"\n          Caisse Yawe irimo Amafaranga : "+Cashier.yose+" RWF\n                              Akazi Keza !!"," Murakazaneza "+nom,JOptionPane.PLAIN_MESSAGE);

        }
        catch(Throwable ex)
        {
        JOptionPane.showMessageDialog(null," Uyu munsi "+today," Itariki ",JOptionPane.PLAIN_MESSAGE);
        }

   if(cashier.db.ip.equals(getString("machineBcUp30Min")))
    try {

    Thread t = new Thread();
    t.start();
    int i=0;
    int time=30*60000;

    while(true)    {
        try {
            try {
            cashier.db.backUpDatabase(cashier.db.conn,getString("serveurBcUp30Min"));
                } catch (Throwable ex) {  cashier.db.insertError(ex+"","back up");}
            t.sleep(time);
           } catch (InterruptedException ex) {cashier.db.insertError(ex+"","back up thread");}

    }

    } catch (Throwable ex) {
     JOptionPane.showMessageDialog(null,"Serveur ntiyaste "+ex," Ikibazo ",JOptionPane.PLAIN_MESSAGE);
 }

  }
String getString(String s)
{
    String ret=" ";
    Variable var= cashier.db.getParam("RUN","MAIN",s+"MAIN");

    if(var!=null)
    ret=var.value2;
    return ret;
}

// on dessine les composants
public void draw()
{


    Container content = getContentPane();
    westPane(content);
    centerPane(content);
    eastPane(content);
    annule();
    cash();
    Credit();// Prendre l argent
    sendProduct();  // Ajout manuel
    addProduct();   // Ajout preselectionne
    removeProduct();// enlever un produit
    stockManager();
    un();deux();trois();quatre();cinq();six();sept();huit();
    b1();b2();b3();b4();b5();b6();b7();b8();b9();b10();b11();b12();
    ecoute();

    actualise();
    retour();
    search();
    uni();soc();rama();soras();corar();MMI();farg(); aar();
    newProduct(); newClient();


    setTitle(nom+"'s Ishyiga Session + TARIF "+stock);

enableEvents(WindowEvent.WINDOW_CLOSING);
		//on fixe  les composant del'interface
		setSize(WIDT,LENTH);
		setVisible(true);

	}
// AFFICHE MAJ le stock
public void showProduct()
{
    int c=cashier.allProduct.size();
   Product [] control= new Product [c];
		for(int i=0;i<c;i++)
		{
                    Product p =cashier.allProduct.get(i);
          	    control[i]=p;
                }
   allProductList.setListData(control);
}
/**************************************************************************
AFFICHE  du ////////////// stock
**************************************************************************/
public void westPane(Container content )
{

     JPanel commonWestPane = new JPanel();
     commonWestPane.setPreferredSize(new Dimension (300,700));



JPanel NorthPane  = new JPanel();
NorthPane.setSize(  new Dimension(300,30));

	addButton     = new JButton("BUY");
	addButton.setBackground(Color.orange);

	NorthPane.add(labelProduit, BorderLayout.WEST);
	NorthPane.add(addButton, BorderLayout.EAST);

commonWestPane.add(NorthPane, BorderLayout.NORTH);

centerPane    = new JPanel();
centerPane.setSize(new Dimension(300,420));

        scrollPaneProductList =new JScrollPane(allProductList);
	scrollPaneProductList.setPreferredSize(new Dimension(290,410));
	allProductList.setBackground(Color.PINK);


	centerPane.add(scrollPaneProductList,BorderLayout.SOUTH);

commonWestPane.add(centerPane, BorderLayout.CENTER);


southPane   = new JPanel();
southPane.setLayout(new GridLayout(3,1));
southPane.setSize(new Dimension(300,250));

    JPanel paneN = new JPanel();
    paneN.setPreferredSize(new Dimension (300,10));

        JLabel text   = new JLabel("CODE:");
	texttoBESent.setPreferredSize(new Dimension(80,30));
	sendButton    = new JButton("BUY");
	sendButton.setBackground(Color.orange);

            paneN.add(text, BorderLayout.WEST);
            paneN.add(texttoBESent, BorderLayout.CENTER);
            paneN.add(sendButton, BorderLayout.EAST);

    JPanel paneC = new JPanel();
    paneC.setPreferredSize(new Dimension (300,20));

        searchButton= new JButton("SEARCH:");
        search.setPreferredSize(new Dimension(140,30));

            paneC.add(searchButton, BorderLayout.WEST);
            paneC.add(search, BorderLayout.EAST);


         JPanel panel = new JPanel();
	panel.setLayout(new GridLayout(4,2));
	panel.setPreferredSize(new Dimension (300,60));

        unVedette =new JButton(getString("unVedette"));
	unVedette.setBackground(Color.green);
	deuxVedette=new JButton(getString("deuxVedette") );
	deuxVedette.setBackground(Color.yellow);
	troisVedette=new JButton(getString("troisVedette") );
	troisVedette.setBackground(Color.yellow);
	quatreVedette=new JButton(getString("quatreVedette") );
	quatreVedette.setBackground(Color.yellow);

        cinqVedette=new JButton(getString("cinqVedette") );
	//cinqVedette.setBackground(Color.pink);

       sixVedette=new JButton(getString("sixVedette") );
	//sixVedette.setBackground(Color.pink);

        septVedette=new JButton(getString("septVedette") );
	//septVedette.setBackground(Color.pink);

        huitVedette=new JButton(getString("huitVedette"));
	huitVedette.setBackground(Color.green);

                panel.add(unVedette);
                panel.add(deuxVedette);
                panel.add(troisVedette);
                panel.add(quatreVedette);
                panel.add(cinqVedette);
                panel.add(sixVedette);
                panel.add(septVedette);
                panel.add(huitVedette);




    southPane.add(paneN);
    southPane.add(paneC);
  southPane.add(panel);




commonWestPane.add(southPane, BorderLayout.SOUTH);


           content.add(commonWestPane, BorderLayout.WEST);


}

/**************************************************************************
AFFICHE du Vedettes
**************************************************************************/
public void centerPane(Container content )
{



		b1= new JButton();
              //  b1.setFont(new Font("Helvetica", Font.PLAIN, 20));
		b2= new JButton();
              //  b2.setFont(new Font("Helvetica", Font.PLAIN, 20));
		b2.setBackground(Color.orange);
		b3= new JButton();
		b3.setBackground(Color.yellow);
              //  b3.setFont(new Font("Helvetica", Font.PLAIN, 20));
		b4= new JButton();
		b4.setBackground(Color.pink);
              //  b4.setFont(new Font("Helvetica", Font.PLAIN, 20));
		b5= new JButton();
		b5.setBackground(Color.green);
              //  b5.setFont(new Font("Helvetica", Font.PLAIN, 20));
		b6= new JButton();
		b6.setBackground(Color.blue);
              //  b6.setFont(new Font("Helvetica", Font.PLAIN, 20));
		b7= new JButton();
		b7.setBackground(Color.gray);
              //  b7.setFont(new Font("Helvetica", Font.PLAIN, 20));
		b8= new JButton();
              //  b8.setFont(new Font("Helvetica", Font.PLAIN, 20));
		b9= new JButton();
		b9.setBackground(Color.orange);
              //  b9.setFont(new Font("Helvetica", Font.PLAIN, 20));
		b10= new JButton();
		b10.setBackground(Color.yellow);
              //  b10.setFont(new Font("Helvetica", Font.PLAIN, 20));
		b11= new JButton();
		b11.setBackground(Color.pink);
               // b11.setFont(new Font("Helvetica", Font.PLAIN, 20));
		b12= new JButton();
		b12.setBackground(Color.green);
               // b12.setFont(new Font("Helvetica", Font.PLAIN, 20));



	JPanel panel = new JPanel();
	panel.setLayout(new GridLayout(6,2));
	panel.setBackground(Color.yellow);
	panel.setPreferredSize(new Dimension (150,400));
	panel.setFont(new Font("Helvetica", Font.PLAIN, 40));
		panel.add(b1);
		panel.add(b2);
		panel.add(b3);
		panel.add(b4);
		panel.add(b5);
		panel.add(b6);
		panel.add(b7);
		panel.add(b8);
		panel.add(b9);
		panel.add(b10);
		panel.add(b11);
		panel.add(b12);

		content.add(panel, BorderLayout.CENTER);


}
/***********************************************************************
AFFICHE fenetre des achats BASKET
***********************************************************************/
public void eastPane(  Container content )
{

JPanel commonEasttPane = new JPanel();
commonEasttPane.setPreferredSize(new Dimension (300,700));


JPanel upPane = new JPanel();
upPane.setPreferredSize(new Dimension (300,30));



            JLabel items= new JLabel(getString("items") );

            annuleButton= new JButton(getString("annuleButton") );
            annuleButton.setBackground(Color.red);

            removeButton= new JButton(getString("removeButton") );
            removeButton.setBackground(Color.MAGENTA);


	upPane.add(annuleButton,BorderLayout.WEST);
	upPane.add(items,BorderLayout.CENTER);
	upPane.add(removeButton,BorderLayout.EAST);

 commonEasttPane.add(upPane,BorderLayout.NORTH);


salePane  =   new JPanel();
salePane.setPreferredSize(new Dimension (300,420));

            scrollPaneallSale =new JScrollPane(saleJList);
            scrollPaneallSale.setPreferredSize(new Dimension(290,330));
            saleJList.setBackground(Color.WHITE);




    pricePane   = new JPanel();
    pricePane.setPreferredSize(new Dimension (300,70));

	JLabel totalLabel= new JLabel("WISHYURWA");
	total.setFont(new Font("Helvetica", Font.PLAIN, 40));
	total.setText(totalprice+ "  RWF ");

        pricePane.add(totalLabel, BorderLayout.WEST);
	pricePane.add(total, BorderLayout.EAST);

salePane.add(scrollPaneallSale,BorderLayout.NORTH);
salePane.add(pricePane,BorderLayout.CENTER);


 commonEasttPane.add(salePane,BorderLayout.CENTER);

JPanel DownPane=new JPanel();
DownPane.setPreferredSize(new Dimension (300,250));

    JPanel money=new JPanel();
    money.setPreferredSize(new Dimension (300,50));

	cashButton= new JButton("CASH");
	cashButton.setBackground(Color.GREEN);

        venteButton= new JButton("RETOUR");
	venteButton.setBackground(Color.YELLOW);

        creditButton= new JButton("CREDIT");
	creditButton.setBackground(Color.red);

    money.add(cashButton,BorderLayout.WEST);
    money.add(creditButton,BorderLayout.EAST);
    money.add(venteButton,BorderLayout.EAST);
    DownPane.add(money,BorderLayout.NORTH);



JPanel paneChoix = new JPanel();
paneChoix.setLayout(new GridLayout(4,2));
paneChoix.setBackground(Color.yellow);
paneChoix.setPreferredSize(new Dimension (300,100));

        rama=new JButton(getString("rama") );
	rama.setBackground(Color.green);
        corar=new JButton(getString("corar") );
	corar.setBackground(Color.magenta);
        soras=new JButton(getString("soras") );
	soras.setBackground(Color.orange);
        mmi=new JButton(getString("mmi") );
        mmi.setBackground(Color.blue);
        uni=new JButton(getString("uni") );
	uni.setBackground(Color.yellow);
        societe=new JButton(getString("societe") );
	farg=new JButton(getString("farg") );
	//farg.setBackground(Color.yellow);

        aar=new JButton(getString("aar"));
	aar.setBackground(Color.lightGray);


        paneChoix.add(uni);
        paneChoix.add(societe);
        paneChoix.add(rama);
        paneChoix.add(soras);
        paneChoix.add(mmi);
        paneChoix.add(corar);
         paneChoix.add(aar);
          paneChoix.add(farg);

DownPane.add(paneChoix,BorderLayout.CENTER);


    JPanel data=new JPanel();
    data.setPreferredSize(new Dimension (300,100));

        scanButton= new JButton("Scan");
	scanButton.setBackground(Color.green);

	actButton   = new JButton("Set Up");
	actButton.setBackground(Color.ORANGE);

	stockButton = new JButton( "DATA" );
	stockButton.setBackground(Color.yellow);

	JPanel sPane= new JPanel();
	sPane.setSize(new Dimension(100,50));

	sPane.add(stockButton,BorderLayout.WEST);
        sPane.add(scanButton, BorderLayout.CENTER);
	sPane.add(actButton, BorderLayout.EAST);


   data.add(sPane,BorderLayout.NORTH);

    JPanel paneNew = new JPanel();
    paneNew.setPreferredSize(new Dimension (300,30));
    paneNew.setLayout(new GridLayout(1,2));

        newClient=new JButton("CLIENT");

        newProduct=new JButton("PRINT");
        newProduct.setBackground(Color.pink);

        paneNew.add(newClient);
        paneNew.add(newProduct);

    data.add(paneNew, BorderLayout.SOUTH);


DownPane.add(data,BorderLayout.SOUTH);

 commonEasttPane.add(DownPane,BorderLayout.SOUTH);


	content.add(commonEasttPane, BorderLayout.EAST);

}public void newClient()
{  newClient.addActionListener(
		new ActionListener()
    {  public void actionPerformed(ActionEvent ae)
	{
 if(ae.getSource()==newClient )
	{


   int n = JOptionPane.showConfirmDialog(frame,"UMUSHYA = OUI, NAHO GUKOSORA = NON"," HITAMO WITONZE ",JOptionPane.YES_NO_OPTION);

          if(n==0)
          {


String nAff  = (JOptionPane.showInputDialog(frame, " Scannez l'Identité ou OK "));
if(!(nAff==null||nAff.length()<32))
{
String nom1=nAff.substring(31, nAff.length());
String prenom="";
String nom="";
String mode="miniscule";
for(int i=0;i<nom1.length();i++)
{
char c=nom1.charAt(i);
int hash= ( ""+c).hashCode();
if((hash>=97&&hash<=122))
{
if(i==0)
mode="majiscule";
nom=nom+c;
}
else
prenom=prenom+c;
}
if(mode.equals("majiscule"))
{
prenom=(nom.charAt(nom.length()-1)+prenom).toUpperCase();
nom=(nom.substring(0, nom.length()-1)).toUpperCase();
}
else
{
nom=prenom.charAt(prenom.length()-1)+nom;
prenom=prenom.substring(0, prenom.length()-1);
String tmt=nom;
nom=prenom.toUpperCase();
prenom=tmt.toUpperCase();
}
String naissance=nAff.substring(2, 6);
String sexe=nAff.substring(7, 8);
String code=nAff.substring(9, 21);

System.out.println( nom   +""+prenom+""+  code+""+ naissance +""+ sexe);

new PersonInterface( cashier.db,  nom ,  prenom ,  code,  naissance  ,  sexe  );
}
else
new PersonInterface( cashier.db,  nom ,  "" , "", "" , ""  );
 }
          else
           {

      String nAff  = (JOptionPane.showInputDialog(frame, " Numero d'affilié"));
        Client c=null;
          String db="";


        if(stock.equals("RAMA"))
        {
        db="_RAMA";
        }
        if(stock.equals("CASHNORM"))
        db="UNI";
    if(nAff!=null )
       {
     String prenom="";
String nom="";
String code="";
String search="";
String naissance="";
String sexe="";
if(nAff.length()>32)
{
    System.out.println(nAff);
String nom1=nAff.substring(31, nAff.length());

String mode="miniscule";
for(int i=0;i<nom1.length();i++)
{
char j=nom1.charAt(i);
int hash= ( ""+j).hashCode();
 if((hash>=97&&hash<=122))
{
if(i==0)
mode="majiscule";
nom=nom+j;
}
else
prenom=prenom+j;
}
 if(mode.equals("majiscule"))
{

prenom=(nom.charAt(nom.length()-1)+prenom).toUpperCase();
nom=(nom.substring(0, nom.length()-1)).toUpperCase();

System.out.println(nom+"  majiscule "+prenom);
}
else
{
nom=prenom.charAt(prenom.length()-1)+nom;
prenom=prenom.substring(0, prenom.length()-1);
String tmt=nom;
nom=prenom.toUpperCase();
prenom=tmt.toUpperCase();
//System.out.println(nom+"  min "+prenom);
}
 sexe=nAff.substring(7, 8);
code=nAff.substring(9, 21);
naissance=nAff.substring(2, 6);
search=nom+"-"+prenom;
//System.out.println(search+naissance+sexe+code);
}
else
{
    search=nAff;
    code=nAff;
}
c= cashier.db.getClient(code,db);
    }
    if(c!=null)
    new PersonInterface(cashier.db,c);
    else
    JOptionPane.showMessageDialog(null," SOMA NEZA NUMERO Y'UMURWAYI"," IKIBAZO ",JOptionPane.PLAIN_MESSAGE);
}


}}});

}

public void newProduct()
{  newProduct.addActionListener(
		new ActionListener()
    {  public void actionPerformed(ActionEvent ae)
	{  if(ae.getSource()==newProduct )
	{
   String nAff  = (String)JOptionPane.showInputDialog(null, "Fait votre choix", " ",
    JOptionPane.QUESTION_MESSAGE, null, new String [] { "IMPRIMER FACTURE","TICKET DE CAISSE" ,"ANNULER"}, nom);

              if(nAff.equals("IMPRIMER FACTURE"))
            {

              int qtyCode = getIn("Numero Facture à imprimer"," ",0);
               LinkedList<String> printR = cashier.db.printRetard(qtyCode) ;

               if(printR.size()!=0)
               PrintCommand(printR.get(0),printR.get(1),printR.get(2),"","");


            }
              else if(nAff.equals("TICKET DE CAISSE"))
              {
       String nom  = JOptionPane.showInputDialog(frame, " Nom du Cassier ");
       String date  = JOptionPane.showInputDialog(frame, " Date des oprerations ");


              int CashierYose= cashier.db.CashierYose(nom,date);


                String sto = " UMUKESIYE : "+nom+" ARARANGIJE ";
                            sto=sto+"\n IGIHE :  "+date;
                            String sto1="\n-----------------------"; String sto2="\n----------------------- ";

                             sto1=sto1+"\n MWAKOREYE AMAFARANGA :  ";sto2=sto2+"\n"+setVirgule(CashierYose);
                             sto1=sto1+"\n-----------------------"; sto2=sto2+"\n----------------------- ";
                             sto1=sto1+"\n ";   sto2=sto2+"\n ";
                             sto1=sto1+"\n           IBIHE BYIZA";   sto2=sto2+"\n ";

                           PrintCommand(sto,sto1,sto2,"","");
              }

              else if(nAff.equals("ANNULER"))
                  {

if(umukozi.LEVEL_EMPLOYE>=5)
{  int qtyCode = getIn("Numero Facture à annulér"," ",0);
   cashier.db.annule(qtyCode,nom) ;}
else
         JOptionPane.showConfirmDialog(frame,  "Baza ubishinzwe  ","Nta burenganzira mufite ",JOptionPane.YES_NO_OPTION);




            }


           }}});

}

public void uni()
{  uni.addActionListener(
		new ActionListener()
    {  public void actionPerformed(ActionEvent ae)
	{  if(ae.getSource()==uni )
	{
                if(cashier.current.getProductList().size()<1)
           {  v=0;
              cashier.stock("");
             stock="CASHNORM";
               setTitle(nom+"'s Ishyiga Session + Tarif "+stock);

             showProduct();
              }
           else
               JOptionPane.showMessageDialog(frame,"RANGIZA FACTURE URIMO ya :"+stock,"IKIBAZO",JOptionPane.WARNING_MESSAGE);

           }}});
}
public void soc()
{  societe.addActionListener(
		new ActionListener()
    {  public void actionPerformed(ActionEvent ae)
	{  if(ae.getSource()==societe )
	{
                 if(cashier.current.getProductList().size()<1)
           { v=1;
              cashier.stock("_SOCIETE");
              stock=getString("societe");
               setTitle(nom+"'s Ishyiga Session + Tarif "+stock);
             showProduct(); }
           else
               JOptionPane.showMessageDialog(frame,"RANGIZA FACTURE URIMO ya :"+stock,"IKIBAZO",JOptionPane.WARNING_MESSAGE);

           }}});

}
public void soras()
{  soras.addActionListener(
		new ActionListener()
    {  public void actionPerformed(ActionEvent ae)
	{  if(ae.getSource()==soras )
	{
              if(cashier.current.getProductList().size()<1)
           { v=1;
            stock=getString("soras");
             cashier.stock("_SORAS");
             setTitle(nom+"'s Ishyiga Session + Tarif "+stock);
             showProduct();
              }
           else
               JOptionPane.showMessageDialog(frame,"RANGIZA FACTURE URIMO ya :"+stock,"IKIBAZO",JOptionPane.WARNING_MESSAGE);

        }}});

}
public void aar()
{  aar.addActionListener(
		new ActionListener()
    {  public void actionPerformed(ActionEvent ae)
	{  if(ae.getSource()==aar )
	{
              if(cashier.current.getProductList().size()<1)
           { v=1;
            stock=getString("aar");;
             cashier.stock("_AAR");
             setTitle(nom+"'s Ishyiga Session + Tarif "+stock);
             showProduct();
              }
           else
               JOptionPane.showMessageDialog(frame,"RANGIZA FACTURE URIMO ya :"+stock,"IKIBAZO",JOptionPane.WARNING_MESSAGE);

        }}});

}
public void farg()
{  farg.addActionListener(
		new ActionListener()
    {  public void actionPerformed(ActionEvent ae)
	{  if(ae.getSource()==farg )
	{
              if(cashier.current.getProductList().size()<1)
           {
             v=1;
             stock=getString("farg");;
             cashier.stock("_FARG");
             setTitle(nom+"'s Ishyiga Session + Tarif "+stock);
             showProduct();

           }
   else
               JOptionPane.showMessageDialog(frame,"RANGIZA FACTURE URIMO ya :"+stock,"IKIBAZO",JOptionPane.WARNING_MESSAGE);

}}});

}
public void MMI()
{  mmi.addActionListener(
		new ActionListener()
    {  public void actionPerformed(ActionEvent ae)
	{  if(ae.getSource()==mmi )
	{
           if(cashier.current.getProductList().size()<1)
           {
               v=1;
            stock=getString("mmi");
             cashier.stock("_MMI");
             setTitle(nom+"'s Ishyiga Session + Tarif "+stock);
             showProduct();}
           else
               JOptionPane.showMessageDialog(frame,"RANGIZA FACTURE URIMO ya :"+stock,"IKIBAZO",JOptionPane.WARNING_MESSAGE);

        }}});

}
public void rama()
{  rama.addActionListener(
		new ActionListener()
    {  public void actionPerformed(ActionEvent ae)
	{  if(ae.getSource()==rama )
	{
               if(cashier.current.getProductList().size()<1)
           {
            v=1;
            stock=getString("rama");
             cashier.stock("_RAMA");
             setTitle(nom+"'s Ishyiga Session + Tarif "+stock);
             showProduct();
             }
           else
               JOptionPane.showMessageDialog(frame,"RANGIZA FACTURE URIMO ya :"+stock,"IKIBAZO",JOptionPane.WARNING_MESSAGE);

        }}});

}


public void corar()
{  corar.addActionListener(
		new ActionListener()
    {  public void actionPerformed(ActionEvent ae)
	{  if(ae.getSource()==corar )
	{
           if(cashier.current.getProductList().size()<1)
           { v=1;
            stock=getString("corar");
             cashier.stock("_CORAR");
             setTitle(nom+"'s Ishyiga Session + Tarif "+stock);
             showProduct();
              }
           else
               JOptionPane.showMessageDialog(frame,"RANGIZA FACTURE URIMO ya :"+stock,"IKIBAZO",JOptionPane.WARNING_MESSAGE);

        }}});

}


/***************************************************************************************************
AJOUT un produit dans le panier par unite
**************************************************************************************************/
public void addProduct()
{  addButton.addActionListener(
		new ActionListener()
    {  public void actionPerformed(ActionEvent ae)
	{  if(ae.getSource()==addButton && allProductList.getSelectedValue()!=null)
	{

               Product p = (Product)allProductList.getSelectedValue();

                    if(p!=null)
{
                         int qtyCode = getIn(p.productName+"  ZINGAHE ??","  ",0);

                            if(p.check(cashier.howManyProductInSale(p.productCode(),qtyCode))&& qtyCode!=-1)
				{
				totalprice=0 ;
                tvaPrice=0;

				Product pi= p.clone(qtyCode);

				// On ajoute le produit dans le panier
				cashier.current.add(pi);

				// Mise a jour d interface et du prix
				int c =cashier.current.getProductList().size();
				String [] control= new String [c];

						for(int i=0;i<c;i++)
						{

                                                    Product p2 = cashier.current.getProductList().get(i);
						control[i]=p2.toSell();
                          int h =(int)((p2.salePrice())/(1+p2.tva));

                        totalprice = totalprice +(int)(p2.salePrice()) ;
                        tvaPrice=tvaPrice+(int)(h*p2.tva);
						total.setText(setVirgule(totalprice)+ "  RWF ");
						}
				saleJList.setListData(control);

				texttoBESent.setText(null);
				}

				else
				{
				JOptionPane.showMessageDialog(frame,p.productName+" HASIGE "+p.qtyLive,"IKIBAZO",JOptionPane.WARNING_MESSAGE);
				}
}
else
    				JOptionPane.showMessageDialog(frame,p.productCode+"  NABWO IRI MURI STOCK COURRANT.","IKIBAZO",JOptionPane.WARNING_MESSAGE);
			}}});



}


/**********************************************************************************************
Retour Mannuel
**********************************************************************************************/
public void retour()
{
	venteButton.addActionListener(new ActionListener() {
	public void actionPerformed(ActionEvent ae)
	{
if(ae.getSource()==venteButton && v!=0  )
     {
   JOptionPane.showMessageDialog(frame," PUSH  CREDIT BUTTON","IT'S A CREDIT",JOptionPane.PLAIN_MESSAGE);
  }

    if(ae.getSource()==venteButton && v==0  )
     {
    //    if(stock.equals("CASHREMISE"))

int facture = getIn("  Numero Facture ","  INT",0);
int kuramo = getIn("  Code du produit retourne","  INT",0);

int retour=cashier.db.retour(facture, kuramo, nom);
   totalprice=totalprice-retour;
      cashier.current.id_client="CASHNORM";

   int money =getIn( " ISHYURWA : "+setVirgule(totalprice)," INT",0);
    int rest=money-totalprice;
    // Si c est pas assez d argent
    boolean done=true;
    if(rest>=0 && rest<5000)
        done= false;
    while( money!=-1&& done)
    {
        if(money>=0 && rest<0  )
              money =getIn( money+" Ntahagije ... ISHYURWA : "+setVirgule(totalprice)," INT",0);
        if(rest>=5000)
            money =getIn( money+ " Nimenshi kubusa ...  ISHYURWA : "+setVirgule(totalprice)," INT",0);

    rest=money-totalprice;
    if(rest>=0 && rest<5000)
        done= false;
    }

    if(rest >= 0 && money!=-1 && rest<5000)
    {

      // Mise a jour du stock , du prix et du panier

//        cashier.upDateStock();
        String EstInStock=cashier.estDansStock();
         if(EstInStock.equals(""))
         {
             cashier.current.total();
        cashier.current.payed=totalprice;
        cashier.current.tot=totalprice;
        Cashier.yose=Cashier.yose+totalprice;
        cashier.current.tva=tvaPrice;
        PrintR(money,rest, tvaPrice,retour);
        saleJList.setListData(new String [0]);
        totalprice=0;
         tvaPrice=0;
        total.setText(setVirgule(rest)+ "  RWF ");

        /////////////////////////////////////////////////


        JOptionPane.showMessageDialog(frame,"SUBIZA "+rest,"MURAKOZE",JOptionPane.PLAIN_MESSAGE);
         }
         else
         JOptionPane.showMessageDialog(frame,EstInStock+" Uyu Muti bawugutanze None reba stock yawo","Itonde !!! ",JOptionPane.QUESTION_MESSAGE);

     }



    }
}});


}
public void addSentProduct(String sentText)
{
 int  s = cashier.allProduct.size();
			Product p=null;
  int productCode=0;
    try
    {
        if(sentText!=null&& sentText.equals(""))
        {
            JOptionPane.showMessageDialog(frame,sentText+"NTABWO ARI CODE","IKIBAZO",JOptionPane.WARNING_MESSAGE);
          }
        else
        {
        Integer in_int=new Integer(sentText);
        productCode = in_int.intValue();
        for(int i=0;i<s;i++)
		{

			Product p1= cashier.allProduct.get(i);
			if(p1.productCode==productCode)
			p=p1;
		}
        }
         }
    catch (NumberFormatException e)
    {
    JOptionPane.showMessageDialog(frame,sentText+"NTABWO ARI CODE","IKIBAZO",JOptionPane.WARNING_MESSAGE);
    }


			// on recupere le produit correspondant



if(p!=null)
{
        int qtyCode = getIn(p.productName+"  zingahe","  INT",0);

        if(p.check(cashier.howManyProductInSale(p.productCode(),qtyCode))&& qtyCode!=-1)
        {
        totalprice=0 ;
         tvaPrice=0;


        Product pi= p.clone(qtyCode);

        // On ajoute le produit dans le panier
        cashier.current.add(pi);

        // Mise a jour d interface et du prix
        int c =cashier.current.getProductList().size();
        String [] control= new String [c];

        for(int i=0;i<c;i++)
        {
            Product p2 = cashier.current.getProductList().get(i);
            control[i]=p2.toSell();
        int h =(int)((p2.salePrice())/(1+p2.tva));
            totalprice = totalprice +(int)(p2.salePrice()) ;
            tvaPrice=tvaPrice+(int)(h*p2.tva);

        total.setText(setVirgule(totalprice)+ "  RWF ");
        }
        saleJList.setListData(control);

        texttoBESent.setText(null);
        }

        else
        {
        JOptionPane.showMessageDialog(frame,p.productName+" HASIGAYE "+p.qtyLive,"IKIBAZO",JOptionPane.WARNING_MESSAGE);
        }
        }
        else
        JOptionPane.showMessageDialog(frame,productCode+"  NABWO IRI MURI STOCK COURRANT.","IKIBAZO",JOptionPane.WARNING_MESSAGE);
        }




/**********************************************************************************************
AJOUT Mannuel
**********************************************************************************************/
public void sendProduct()
{
	sendButton.addActionListener(new ActionListener() {
	public void actionPerformed(ActionEvent ae)
	{
			if(ae.getSource()==sendButton)
			{
			// On recupere le code du produit
			 String sentText = texttoBESent.getText() ;
                        addSentProduct(sentText);
}
        } });
}



/**********************************************************************************************
GESTION DU STOCK OUVERTURE DU FENETRE BUTTON
**********************************************************************************************/

public void stockManager()
{
   actButton.addActionListener(new ActionListener() {
    public void actionPerformed(ActionEvent ae)
    {
    if(ae.getSource()==actButton  )
    {

           if(umukozi.LEVEL_EMPLOYE>3)
              new Data(cashier,stock);
        else
         JOptionPane.showConfirmDialog(frame,  "Baza ubishinzwe  ","Nta burenganzira mufite ",JOptionPane.YES_NO_OPTION);


    }}});

}
/**********************************************************************************************
ACTUALIZE le stock
**********************************************************************************************/

public void actualise()
{  stockButton.addActionListener(
		new ActionListener()
	{  public void actionPerformed(ActionEvent ae)
	{  if(ae.getSource()==stockButton)
		{

                          new VoirFichier(umukozi,serv,dataBase);

		}
	 }});

}
LinkedList <String> trouve(LinkedList<Product>product,String find)
{

    LinkedList <String> trouve = new LinkedList ();
    LinkedList <String> trouver = new LinkedList ();
    Product p1=null;
    double p2=1000000;
    int p3=-1;
    String resString="";

    StringTokenizer st1=new StringTokenizer (find);
    while(st1.hasMoreTokens())
    trouve.add(st1.nextToken(" "));

    for(int i=0;i<cashier.allProduct.size();i++)
    {
    Product  p =cashier.allProduct.get(i);
    int j=0;



    while(j<trouve.size()&&(p.productName.toUpperCase()).contains(trouve.get(j).toUpperCase()))
        j++;
        if(j==trouve.size())
      {  resString=resString+ "\n CODE = "+p.productCode+" NAME : "+p.productName+" PRICE ="+p.currentPrice;
                      p1=p;
                      if(p.currentPrice<p2)
                    { p3=p.productCode;
                      p2=p.currentPrice;
                   } }

               }
trouver.add(resString);
trouver.add(""+p3);
return trouver;
}

/**********************************************************************************************
VOIR Fiche de stock
**********************************************************************************************/
public void search()
{  searchButton.addActionListener(
		new ActionListener()
	{  public void actionPerformed(ActionEvent ae)
	{  if(ae.getSource()==searchButton && search.getText().length()>2)
		{



        //Soie sertie n°2 + aig. ronde 1 mg 2 ce


              String s  = search.getText();
              LinkedList <String> trouve=trouve(cashier.allProduct,s);
               if(!trouve.get(1).equals("-1") )
               {String sentText = JOptionPane.showInputDialog(frame,trouve.getFirst(),trouve.get(1));

                if(sentText!=null)
                addSentProduct(sentText);}
                else
                JOptionPane.showConfirmDialog(frame,  s+" Ntubaho"," Ibisa na : "+s,JOptionPane.YES_NO_OPTION);
		}
	 }});
}

/**********************************************************************************************
GESTION des produits VEDETTES BUTTON
**********************************************************************************************/

public void b1()
{b1.addActionListener(new ActionListener(){  public void actionPerformed(ActionEvent ae){  if(ae.getSource()==b1) addOneVedette(i1); }});}
public void b2()
{b2.addActionListener(new ActionListener(){  public void actionPerformed(ActionEvent ae){  if(ae.getSource()==b2) addOneVedette(i2); }});}
public void b3()
{b3.addActionListener(new ActionListener(){  public void actionPerformed(ActionEvent ae){  if(ae.getSource()==b3) addOneVedette(i3); }});}
public void b4()
{b4.addActionListener(new ActionListener(){  public void actionPerformed(ActionEvent ae){  if(ae.getSource()==b4) addOneVedette(i4); }});}
public void b5()
{b5.addActionListener(new ActionListener(){  public void actionPerformed(ActionEvent ae){  if(ae.getSource()==b5) addOneVedette(i5); }});}
public void b6()
{b6.addActionListener(new ActionListener(){  public void actionPerformed(ActionEvent ae){  if(ae.getSource()==b6) addOneVedette(i6); }});}
public void b7()
{b7.addActionListener(new ActionListener(){  public void actionPerformed(ActionEvent ae){  if(ae.getSource()==b7) addOneVedette(i7); }});}
public void b8()
{b8.addActionListener(new ActionListener(){  public void actionPerformed(ActionEvent ae){  if(ae.getSource()==b8) addOneVedette(i8); }});}
public void b9()
{b9.addActionListener(new ActionListener(){  public void actionPerformed(ActionEvent ae){  if(ae.getSource()==b9) addOneVedette(i9); }});}
public void b10()
{b10.addActionListener(new ActionListener(){  public void actionPerformed(ActionEvent ae){  if(ae.getSource()==b10) addOneVedette(i10); }});}
public void b11()
{b11.addActionListener(new ActionListener(){  public void actionPerformed(ActionEvent ae){  if(ae.getSource()==b11) addOneVedette(i11); }});}
public void b12()
{b12.addActionListener(new ActionListener(){  public void actionPerformed(ActionEvent ae){  if(ae.getSource()==b12) addOneVedette(i12); }});}




public void un()
{unVedette.addActionListener(new ActionListener(){  public void actionPerformed(ActionEvent ae){  if(ae.getSource()==unVedette)
 setVedette(1);  }});}
public void deux()
{deuxVedette.addActionListener(new ActionListener(){  public void actionPerformed(ActionEvent ae){  if(ae.getSource()==deuxVedette)
 setVedette(2);  }});}
public void trois()
{troisVedette.addActionListener(new ActionListener(){  public void actionPerformed(ActionEvent ae){  if(ae.getSource()==troisVedette)
setVedette(3);  }});}
public void quatre()
{quatreVedette.addActionListener(new ActionListener(){  public void actionPerformed(ActionEvent ae){  if(ae.getSource()==quatreVedette)
setVedette(4); }});}
public void cinq()
{cinqVedette.addActionListener(new ActionListener(){  public void actionPerformed(ActionEvent ae){  if(ae.getSource()==cinqVedette)
setVedette(5); }});}
public void six()
{sixVedette.addActionListener(new ActionListener(){  public void actionPerformed(ActionEvent ae){  if(ae.getSource()==sixVedette)
setVedette(6); }});}
public void sept()
{septVedette.addActionListener(new ActionListener(){  public void actionPerformed(ActionEvent ae){  if(ae.getSource()==septVedette)
setVedette(7); }});}
public void huit()
{huitVedette.addActionListener(new ActionListener(){  public void actionPerformed(ActionEvent ae){  if(ae.getSource()==huitVedette)
 showProduct();  setVedette(8); }});}


/**********************************************************************************************
AJOUT dans le BASKET par son code
**********************************************************************************************/
public void addOneVedette(int pr)
{
                  Product pp=cashier.getProductVedette(pr);

		Product p=(pp).clone(pp.qty);
                int qte=pp.qty;
                if(p!=null)
{
		if(p.check(cashier.howManyProductInSale(p.productCode(),qte)))
		{
			totalprice=0 ;
                        tvaPrice=0;
			cashier.current.add(p);
			// Mise a jour d interface et du prix
			int c =cashier.current.getProductList().size();
			String [] control= new String [c];

			for(int i=0;i<c;i++)
				{


                Product p2 = cashier.current.getProductList().get(i);
                control[i]=p2.toSell();
              int h =(int)((p2.salePrice())/(1+p2.tva));
                totalprice= totalprice +(int)(p2.salePrice()) ;
                tvaPrice=tvaPrice+(int)(h*p2.tva);
                total.setText(setVirgule(totalprice)+ "  RWF ");


				}
				saleJList.setListData(control);
				}
		else
	    {
		JOptionPane.showMessageDialog(frame,p.productName+" HASIGAYE "+p.qtyLive,"IKIBAZO",JOptionPane.WARNING_MESSAGE);
		}
                }
                else
                    		JOptionPane.showMessageDialog(frame,pr+"  NTIBONEKA.","IKIBAZO",JOptionPane.WARNING_MESSAGE);

}
/**********************************************************************************************
AJOUT dans le BASKET par son code
**********************************************************************************************/
public void addOne(int pr)
{
		int  s = cashier.allProduct.size();
		Product p=null;
		for(int i=0;i<s;i++)
		{

			Product p1= cashier.allProduct.get(i);
			if(p1.productCode==pr)
                        p=p1.clone(1);
                       }


		if(p.check(cashier.howManyProductInSale(p.productCode(),1)))
		{
			totalprice=0 ;
                         tvaPrice=0;
			cashier.current.add(p);

			// Mise a jour d interface et du prix
			int c =cashier.current.getProductList().size();
			String [] control= new String [c];

			for(int i=0;i<c;i++)
				{

					 Product p2 = cashier.current.getProductList().get(i);
						control[i]=p2.toSell();
                                                 int h =(int)((p2.salePrice())/(1+p2.tva));
						totalprice= totalprice +(int)(p2.salePrice()) ;
                                                 tvaPrice=tvaPrice+(int)(h*p2.tva);

				}
				saleJList.setListData(control);
				}
		else
	    {
		JOptionPane.showMessageDialog(frame,p.productName+" HASIGAYE "+p.qtyLive,"IKIBAZO",JOptionPane.WARNING_MESSAGE);
		}}

/**********************************************************************************************
CHANGEMENT DE li Interface
**********************************************************************************************/
public void setVedette(int k)
{
this.k=k;
int    [] c     = new int [13];
int j=1;
for(int i=0;i<cashier.vedette.size();i++)
{
Vedette v2= (Vedette)cashier.vedette.get(i);
if(v2.cat==k)
{
  c[j]=v2.ID_PRODUCT;
    j++;
}
}

b1.setText(cashier.getProductVedetteName(c[1]));
b2.setText(cashier.getProductVedetteName(c[2]));
b3.setText(cashier.getProductVedetteName(c[3]));
b4.setText(cashier.getProductVedetteName(c[4]));
b5.setText(cashier.getProductVedetteName(c[5]));
b6.setText(cashier.getProductVedetteName(c[6]));
b7.setText(cashier.getProductVedetteName(c[7]));
b8.setText(cashier.getProductVedetteName(c[8]));
b9.setText(cashier.getProductVedetteName(c[9]));
b10.setText(cashier.getProductVedetteName(c[10]));
b11.setText(cashier.getProductVedetteName(c[11]));
b12.setText(cashier.getProductVedetteName(c[12]));

i1=cashier.getProductVedette(c[1]).productCode;
i2=cashier.getProductVedette(c[2]).productCode;
i3=cashier.getProductVedette(c[3]).productCode;
i4=cashier.getProductVedette(c[4]).productCode;
i5=cashier.getProductVedette(c[5]).productCode;
i6=cashier.getProductVedette(c[6]).productCode;
i7=cashier.getProductVedette(c[7]).productCode;
i8=cashier.getProductVedette(c[8]).productCode;
i9=cashier.getProductVedette(c[9]).productCode;
i10=cashier.getProductVedette(c[10]).productCode;
i11=cashier.getProductVedette(c[11]).productCode;
i12=cashier.getProductVedette(c[12]).productCode;
}
/***********************************************************************************************
ENLEVE un produit dans le panier BUTTON
***********************************************************************************************/
public void removeProduct()
{

	removeButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ae)
		{
			if(ae.getSource()==removeButton && saleJList.getSelectedValue()!=null )
				{

					String r = (String)saleJList.getSelectedValue();
					Product p = cashier.makeProductRemove(r);


					//On enleve par le Code du Produit
					cashier.current.remove(p);

					totalprice=totalprice - (int)(p.salePrice());
                    tvaPrice=tvaPrice -(int)(((p.salePrice())/(1+p.tva))*p.tva);
                    total.setText(setVirgule(totalprice)+ "  RWF ");

						// Mise a jour d interface et du prix
						int c =cashier.current.getProductList().size();
						String [] control= new String [c];

						for(int i=0;i<c;i++)
								control[i]=((Product)(cashier.current.getProductList().get(i))).toSell();

					saleJList.setListData(control);

		}}});

}

/***********************************************************************************************
ANNULE BUTTON
***********************************************************************************************/
public void annule()
{

		annuleButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae)
			{
				if(ae.getSource()==annuleButton  )
					{


					saleJList.setListData(new String [0]);
					cashier.basketStart(stock);
					totalprice=0;  tvaPrice=0;
					total.setText(setVirgule(totalprice)+ "  RWF ");

		}}});

}
public void ecoute()
{

   scanButton.addKeyListener(
		new KeyListener()
    {  public void actionPerformed(ActionEvent ae)
	{

       }

public void keyTyped(KeyEvent e) {

    char ck =e.getKeyChar() ;
    String cc =""+ck;
        if(ck==' ')


if(cc.hashCode()==10)
{
     Product p=null;
     for(int z =0;z<cashier.allProduct.size();z++)
    {   Product j =cashier.allProduct.get(z) ;
        if(j.codeBar.equals(ss))
        {  p=j; break; }
    }
    if(p!=null)
     {
      int qtyCode =1;// getIn(p.productName+"  zingahe","  INT",0);
      if(p.check(cashier.howManyProductInSale(p.productCode(),qtyCode))&& qtyCode!=-1)
      {     totalprice=0 ;
            tvaPrice=0 ;
            Product pi= p.clone(qtyCode);
            cashier.current.add(pi);
            int c =cashier.current.getProductList().size();
            String [] control= new String [c];
            for(int i=0;i<c;i++)
            {
            Product p2 = cashier.current.getProductList().get(i);
            control[i]=p2.toSell();
            int h =(int)((p2.salePrice())/(1+p2.tva));
            totalprice= totalprice +(int)(p2.salePrice()) ;
            tvaPrice=tvaPrice+(int)(h*p2.tva);
            total.setText(setVirgule(totalprice)+ "  RWF ");
            }
            saleJList.setListData(control);
            texttoBESent.setText(null);
      }

    else
    {
    JOptionPane.showMessageDialog(frame,p.productName+" HASIGAYE "+p.qtyLive,"IKIBAZO",JOptionPane.WARNING_MESSAGE);
    }
    }
    else
    JOptionPane.showMessageDialog(frame,currentScan+"  NABWO IRI MURI STOCK COURRANT.","IKIBAZO",JOptionPane.WARNING_MESSAGE);

    ss="";

    }

else
  ss=ss+ck;

}

            public void keyPressed(KeyEvent e) {      }

            public void keyReleased(KeyEvent e) {     }
        });

}
String setVirgule(int frw)
        {

String setString = ""+frw;
int l =setString.length();
if(l<2)
   setString = "    "+frw;
else if(l<3)
   setString = "  "+frw;
else if(l<4)
   setString = "  "+frw;

String vir=".";
if(virgule==0)
    vir=" .";

if(l>3 && l<=6)
setString= setString.substring(0,l-3)+vir+setString.substring(l-3) ;
if(l>6)
{

String s1 = setString.substring(0,l-3);
int sl=s1.length();
setString=s1.substring(0,sl-3)+vir+s1.substring(sl-3)+vir+setString.substring(l-3)  ;

}

return setString;
}
/***********************************************************************************************
//PAIEMENT BUTTON CASH
***********************************************************************************************/
public void cash()
{
cashButton.addActionListener(new ActionListener() {
public void actionPerformed(ActionEvent ae)
{
  if(ae.getSource()==cashButton && v!=0  )
     {
   JOptionPane.showMessageDialog(frame," PUSH  CREDIT BUTTON","IT'S A CREDIT",JOptionPane.PLAIN_MESSAGE);
  }

    if(ae.getSource()==cashButton && v==0 && totalprice>0 )
     {
    //    if(stock.equals("CASHREMISE"))
      cashier.current.id_client="CASHNORM";

   int money =getIn( " ISHYURWA : "+setVirgule(totalprice)," INT",0);
    int rest=money-totalprice;
    // Si c est pas assez d argent
    boolean done=true;
    if(rest>=0 && rest<5000)
        done= false;
    while( money!=-1&& done)
    {
        if(money>=0 && rest<0  )
              money =getIn( money+" Ntahagije ... ISHYURWA : "+setVirgule(totalprice)," INT",0);
        if(rest>=5000)
            money =getIn( money+ " Nimenshi kubusa ...  ISHYURWA : "+setVirgule(totalprice)," INT",0);

    rest=money-totalprice;
    if(rest>=0 && rest<5000)
        done= false;
    }

    if(rest >= 0 && money!=-1 && rest<5000)
    {

      // Mise a jour du stock , du prix et du panier

//        cashier.upDateStock();
        String EstInStock=cashier.estDansStock();
         if(EstInStock.equals(""))
         {
             cashier.current.total();
        cashier.current.payed=totalprice;
        cashier.current.tot=totalprice;
        Cashier.yose=Cashier.yose+totalprice;
        cashier.current.tva=tvaPrice;
        PrintU(money,rest, tvaPrice);
        saleJList.setListData(new String [0]);
        totalprice=0; tvaPrice=0;
        total.setText(setVirgule(rest)+ "  RWF ");

        /////////////////////////////////////////////////


        JOptionPane.showMessageDialog(frame,"SUBIZA "+rest,"MURAKOZE",JOptionPane.PLAIN_MESSAGE);
         }
         else
         JOptionPane.showMessageDialog(frame,EstInStock+" Uyu Muti bawugutanze None reba stock yawo","Itonde !!! ",JOptionPane.QUESTION_MESSAGE);

     }



    }
}});

}
Client [] checkClient(String name,String db)
{


LinkedList<Client> clii=new LinkedList();

 String resString="";

 String nom1="";
 String prenom1="";

    StringTokenizer st1=new StringTokenizer (name);

    if(st1.hasMoreTokens())
    nom1= st1.nextToken(" ") ;
    if(st1.hasMoreTokens())
    prenom1= st1.nextToken(" ") ;
    String sql="";
    if(stock.equals("SORAS")||stock.equals("CORAR")||stock.equals(("MMI"))||stock.equals(("RAMA")))
    sql="and assurance='"+stock+"'";


    clii=cashier.db.getOldClient("select * from APP.CLIENT"+db+" where  NOM_CLIENT like '%"+nom1+"%' "+sql);

     Client [] cli = new Client[clii.size()];
    for(int h=0;h<clii.size();h++)
    {
        cli[h]=clii.get(h);
    }
  return  cli;
}

/***********************************************************************************************
PAIEMENT BUTTON CREDIT
***********************************************************************************************/
public void Credit()
{

creditButton.addActionListener(new ActionListener() {
public void actionPerformed(ActionEvent ae)
{
if(ae.getSource()==creditButton && totalprice>0 )
     {




     Client c=null;
       String nAff  = (JOptionPane.showInputDialog(frame, " Numéro ; Nom; Numero d'Aff. "));

        String db="";
        if(stock.equals("RAMA"))
        {
        db="_RAMA";
        }

///////////////////////////////////////////
       if(nAff!=null )
       {
     String prenom="";
String nom="";
String code="";
String search="";
String naissance="";
String sexe="";
if(nAff.length()>32)
{
    System.out.println(nAff);
String nom1=nAff.substring(31, nAff.length());

String mode="miniscule";
for(int i=0;i<nom1.length();i++)
{
char j=nom1.charAt(i);
int hash= ( ""+j).hashCode();
 if((hash>=97&&hash<=122))
{
if(i==0)
mode="majiscule";
nom=nom+j;
}
else
prenom=prenom+j;
}
 if(mode.equals("majiscule"))
{

prenom=(nom.charAt(nom.length()-1)+prenom).toUpperCase();
nom=(nom.substring(0, nom.length()-1)).toUpperCase();

}
else
{
nom=prenom.charAt(prenom.length()-1)+nom;
prenom=prenom.substring(0, prenom.length()-1);
String tmt=nom;
nom=prenom.toUpperCase();
prenom=tmt.toUpperCase();
}
 sexe=nAff.substring(7, 8);
code=nAff.substring(9, 21);
naissance=nAff.substring(2, 6);
search=nom+"-"+prenom;
}
else
{
    search=nAff;
    code=nAff;
}
c= cashier.db.getClient(code,db);

 if(c==null)
    {
      c= (Client) JOptionPane.showInputDialog(null, "Faites votre choix", "Client",JOptionPane.QUESTION_MESSAGE, null, checkClient(nAff.toUpperCase(),db), " ");
    }

           if(c==null )
       {

        int n = JOptionPane.showConfirmDialog(frame,"URASHAKA KUMUSHYIRAMO",nAff+" ntawe dufite",JOptionPane.YES_NO_OPTION);
        if(n==0)
        {
            new PersonInterface( cashier.db,  nom ,  prenom ,  code,  naissance  ,  sexe  );

        } } }

 if(c!=null &&  c.DATE_EXP< today )
            {
            int n = JOptionPane.showConfirmDialog(frame,"Urashaka Kucyongera ",nAff+" Igihe cyarashize ",JOptionPane.WARNING_MESSAGE);

          if(n==0)
            {
                new PersonInterface(cashier.db,c);
            }

            c=null;
        }

        if(c!=null)
            {
                    if(c.ASSURANCE.equals(stock)||stock.equals("SOCIETE") ||(stock.equals("CASHNORM")))
                    {
                        int n = JOptionPane.showConfirmDialog(frame,
                        "  NOM_CLIENT     ="+c.NOM_CLIENT+
                        "\n PRENOM_CLIENT  ="+c.PRENOM_CLIENT+
                        "\n NUM_AFFILIATION="+c.NUM_AFFILIATION+
                        "\n PERCENTAGE     ="+c.PERCENTAGE +
                        "\n ASSURANCE        ="+c.ASSURANCE+
                        "\n DPT        ="+c.EMPLOYEUR,
                       " FICHE"+ nAff,JOptionPane.YES_NO_OPTION);
     if(n==0)
     {
String quittance= (JOptionPane.showInputDialog(frame, " Numero d'Ordonnance de "+c.ASSURANCE));

String raison="M";// (JOptionPane.showInputDialog(frame, " Impamvu y'uburwayi "));

int p =(int) (((totalprice*c.PERCENTAGE)*0.01)+0.5);

int ideni=0;
int money =0;
int rest=0;
if(c.ASSURANCE.equals("PARTICULIER"))
{
ideni=getIn( totalprice+"   ANDIKA IDENI : "," INT",0);
money=ideni;
p=ideni;
System.out.println(ideni +""+totalprice);
c.PERCENTAGE=(int)(((double)ideni/(double)totalprice)*100);
}
else
{ideni=p;
    money=getIn( " ISHYURWA : "+setVirgule(p)," INT",0);
}

rest=money-p;
// Si c est pas assez d argent
boolean done=true;
if(rest>=0 && rest<5000)
done= false;
while( money!=-1&& done)
    {

        if(money>=0 && rest<0  )
              money =getIn( money+" Ntahagije ... ISHYURWA : "+setVirgule(p)," INT",0);
        if(rest>=5000)
            money =getIn( money+ " Nimenshi kubusa ...  ISHYURWA : "+setVirgule(p)," INT",0);

    rest=money-p;
    if(rest>=0 && rest<5000)
        done= false;
    }

    if(rest >= 0 && money!=-1 && rest<5000)
    {

          cashier.current.id_client=c.ASSURANCE;
    // Mise a jour du stock , du prix et du panier
//cashier.upDateStock();
 String EstInStock=cashier.estDansStock();
         if(EstInStock.equals(""))
         {
cashier.current.tot=totalprice;
cashier.current.tva=tvaPrice;
cashier.current.total();
cashier.current.payed=p;
Cashier.yose=Cashier.yose+p;

if(quittance==null)
    quittance="";


Print(c,p,money,rest,quittance,tvaPrice);
cashier.db.doFinishCredit(new Credit(cashier.factNum,c.NUM_AFFILIATION,quittance,totalprice-ideni,ideni,raison));

saleJList.setListData(new String [0]);
totalprice=0; tvaPrice=0;
total.setText(rest+ "  RWF ");
JOptionPane.showMessageDialog(frame,"SUBIZA "+rest,"MURAKOZE",JOptionPane.PLAIN_MESSAGE);

//total.setText(totalprice+ "  RWF ");
}
         else
         JOptionPane.showMessageDialog(frame,EstInStock+" Uyu Muti bawugutanze None reba stock yawo","Itonde !!! ",JOptionPane.QUESTION_MESSAGE);



}
else
{
JOptionPane.showMessageDialog(frame," Invoice Canceled "," ERROR ",JOptionPane.PLAIN_MESSAGE);
cashier.basketStart(stock);
saleJList.setListData(new String [0]);
totalprice=0; tvaPrice=0;
//total.setText(totalprice+ "  RWF ");
total.setText(rest+ "  RWF ");
}


            }
                    }
                     else
                     {
                        System.out.println(stock);
                         System.out.println(c.ASSURANCE);
                        JOptionPane.showMessageDialog(frame,stock+" Le Numero d'affilié different du Tarif "+c.ASSURANCE," ERROR ",JOptionPane.PLAIN_MESSAGE);
                     }
            }
   }

}});

}
public void PrintU(int money,int rest,int tva )
{
String printStringUp=getString("bp")+ "\n  "+ getString("tel")+ "\n  "+ getString("tva");
System.out.println("--------------"+printStringUp);
String printStringE="";
String printStringW="";


 for(int i=0;i<cashier.current.getProductListDB().size();i++)
               {
                  Product p =cashier.current.getProductListDB().get(i);
                  printStringE=printStringE+ "\n  "+p.qty+ "  "+p.productName;
                  virgule=0;
                  printStringW=printStringW+ "\n"+setVirgule((int)(p.salePrice()));
                  virgule=1;
               }


    printStringE=printStringE+ "\n  ";
    printStringW=printStringW+ "\n   ";

    printStringE=printStringE+ "\n  TOTAL en RWF:      ";
    printStringW=printStringW+ "\n"+setVirgule(totalprice);

    printStringE=printStringE+ "\n  Tva : ";
    printStringW=printStringW+ "\n  "+setVirgule(tva);

    printStringE=printStringE+ "\n  CASH :";
    printStringW=printStringW+ "\n"+setVirgule(money);
    printStringE=printStringE+ "\n  ";
    printStringW=printStringW+ "\n ========= ";
    printStringE=printStringE+ "\n  CHANGE :";
    printStringW=printStringW+ "\n"+setVirgule(rest);





cashier.basketEnd(stock);
printStringUp=printStringUp+ "\n    FACTURE N°    : " +cashier.current.id_invoice;
printStringUp=printStringUp+ "\n    " +cashier.current.date +"   "+new Date().toLocaleString().substring(10)+"  #: "+nom;

printStringUp=printStringUp+"\n    -------------------------------------------   ";
String merci= " Merci de votre visite. "  ;

String down=" "  ;

cashier.basketStart(stock);

PrintCommand(printStringUp,printStringE,printStringW,down,merci);





}
public void PrintR(int money,int rest,int tva,int ret )
{
String printStringUp= getString("bp")+ "\n  "+ getString("tel")+ "\n  "+ getString("tva");

String printStringE="";
String printStringW="";


 for(int i=0;i<cashier.current.getProductListDB().size();i++)
               {
                   Product p =cashier.current.getProductListDB().get(i);

                  printStringE=printStringE+ "\n  "+p.qty+ "  "+p.productName;
                  printStringW=printStringW+ "\n"+setVirgule((int)(p.salePrice()));
               }


 printStringE=printStringE+ "\n  ";
printStringW=printStringW+ "\n   ";

printStringE=printStringE+ "\n  TOTAL en RWF: ";
printStringW=printStringW+ "\n"+setVirgule(totalprice+ret);

printStringE=printStringE+ "\n  Montant Retour : ";
printStringW=printStringW+ "\n"+setVirgule(ret);

printStringE=printStringE+ "\n  Solde ";
printStringW=printStringW+ "\n"+setVirgule(totalprice);

printStringE=printStringE+ "\n  Tva : ";
printStringW=printStringW+ "\n  "+setVirgule(tva);

 printStringE=printStringE+ "\n  CASH :";
printStringW=printStringW+ "\n"+setVirgule(money);
printStringE=printStringE+ "\n  ";
printStringW=printStringW+ "\n ========= ";
 printStringE=printStringE+ "\n  CHANGE :";
printStringW=printStringW+ "\n"+setVirgule(rest);





       cashier.basketEnd(stock);
        printStringUp=printStringUp+ "\n    FACTURE N°    : " +cashier.current.id_invoice;
        printStringUp=printStringUp+ "\n    " +cashier.current.date +"   "+new Date().toLocaleString().substring(10)+"  #: "+nom;

        printStringUp=printStringUp+"\n    -------------------------------------------   ";
String merci= " Merci de votre visite. "  ;

String down=" "  ;

cashier.basketStart(stock);

PrintCommand(printStringUp,printStringE,printStringW,down,merci);






}
public void Print(Client c,int affilier,int money,int rest,String quittance,int tva )
{
String printStringUp=getString("bp")+ "\n  "+ getString("tel")+ "\n  "+ getString("tva");
String printStringE="";
String printStringW="";


 for(int i=0;i<cashier.current.getProductListDB().size();i++)
               {
                  Product p =cashier.current.getProductListDB().get(i);

                 if(stock.equals("RAMA"))
                  printStringE=printStringE+ "\n  "+p.qty+ "  "+p.codeSoc+ "_"+p.productName;
                 else
                     printStringE=printStringE+ "\n  "+p.qty+ "  "+p.productName;

                  virgule=0;
                  printStringW=printStringW+ "\n"+setVirgule((int)(p.salePrice()));
                  virgule=1;
               }


 printStringE=printStringE+ "\n  ";
printStringW=printStringW+ "\n   ";

printStringE=printStringE+ "\n  TOTAL en RWF :    ";
printStringW=printStringW+ "\n"+setVirgule(totalprice);

printStringE=printStringE+ "\n  Tva : ";
printStringW=printStringW+ "\n  "+tva;

 printStringE=printStringE+ "\n  "+c.ASSURANCE+" "+(100-c.PERCENTAGE)+" % : ";
printStringW=printStringW+ "\n"+setVirgule(totalprice-affilier);
 printStringE=printStringE+ "\n  ADHERENT  "+c.PERCENTAGE+" % : ";
printStringW=printStringW+ "\n"+setVirgule(affilier);
 printStringE=printStringE+ "\n  CASH :";
printStringW=printStringW+ "\n"+setVirgule(money);
printStringE=printStringE+ "\n  ";
printStringW=printStringW+ "\n ========= ";
 printStringE=printStringE+ "\n  CHANGE :";
printStringW=printStringW+ "\n"+setVirgule(rest);



cashier.basketEnd(stock);
printStringUp=printStringUp+ "\n    FACTURE N°    : " +cashier.current.id_invoice;
printStringUp=printStringUp+ "\n    " +cashier.current.date +"   "+new Date().toLocaleString().substring(10)+"  #: "+nom;
printStringUp=printStringUp+"\n    -------------------------------------------   ";
printStringUp=printStringUp+"\n    Assureur            : "+c.ASSURANCE;
printStringUp=printStringUp+ "\n    N° Affiliation      : "+c.NUM_AFFILIATION;
printStringUp=printStringUp+ "\n    Nom et Prenom : "+c.NOM_CLIENT+ " "+c.PRENOM_CLIENT;
printStringUp=printStringUp+ "\n    Departement     : "+c.EMPLOYEUR;
printStringUp=printStringUp+ "\n    N° Quittance     : "+quittance;
printStringUp=printStringUp+"\n    -------------------------------------------   ";
String merci= " Merci de votre visite. "  ;

String down=""  ;

cashier.basketStart(stock);

PrintCommand(printStringUp,printStringE,printStringW,down,merci);




}
private void PrintCommand(String printStringUp,String printStringE,String printStringW,String down,String merci )
{
 PrintJob pjob = getToolkit().getPrintJob(Main.this, "Ishyiga ", properties);
         if (pjob != null) {
        Graphics pg = pjob.getGraphics();

        if (pg != null) {
          printLongString(pjob, pg, printStringUp,printStringE,printStringW,down,merci);
          pg.dispose();
        }
        pjob.end();
      }


}
  // I'm assuming a one-inch margin on all
  // four sides. This could be done better.
  private int margin = 72;

  // Print string to graphics via printjob
  // Does not deal with word wrap or tabs
  private void printLongString (PrintJob pjob, Graphics pg,String printStringUp,String printStringE,String printStringW,String down, String merci) {

    int pageNum = 1;
    int linesForThisPage = 0;
    int linesForThisJob = 0;
    // Note: String is immutable so won't change while printing.
    if (!(pg instanceof PrintGraphics)) {
      throw new IllegalArgumentException ("Graphics context not PrintGraphics");
    }


    StringReader sr = new StringReader(printStringE);
    LineNumberReader lnr = new LineNumberReader (sr);

    StringReader sr1 = new StringReader(printStringW);
    LineNumberReader lnr1 = new LineNumberReader (sr1);

    StringReader sr2 = new StringReader(printStringUp);
    LineNumberReader lnr2 = new LineNumberReader (sr2);


    String nextLine;
    String nextLineE;
     String nextLineW;

    int pageHeight = pjob.getPageDimension().height - margin;
    Font helv = new Font("Helvetica", Font.PLAIN, 10);
    //have to set the font to get any output
    pg.setFont (helv);
    FontMetrics fm = pg.getFontMetrics(helv);
    int fontHeight = fm.getHeight();
    int fontDescent = fm.getDescent();
    int curHeight = margin;

    try {
        do {
        nextLine = lnr2.readLine();
        if (nextLine != null) {
          if ((curHeight + fontHeight) > pageHeight) {
            // New Page
            //System.out.println(linesForThisPage + " lines printed for page " + pageNum);
            if(linesForThisPage == 0) {
               //System.out.println( "Font is too big for pages of this size; aborting...");
               break;
            }
            pageNum++;
            linesForThisPage = 0;
            pg.dispose();
            pg = pjob.getGraphics();
            if (pg != null) {
              pg.setFont(helv);
            }
            curHeight = 0;
          }
          curHeight += fontHeight;
          if (pg != null) {
              if(linesForThisPage==2)
              {

                //  pg.drawString("Ammoxyciline 200 mg ", margin, curHeight - fontDescent);

                  //pg.drawString("10.000 ", margin+170, curHeight - fontDescent);
              }
              pg.drawString(nextLine, margin, curHeight - fontDescent);

            linesForThisPage++;
            linesForThisJob++;

          }
          else {
            //System.out.println("pg null");
          }
        }
      } while (nextLine != null);



      do {
        nextLineE = lnr.readLine();
        nextLineW = lnr1.readLine();
        if (nextLineW != null) {
          if ((curHeight + fontHeight) > pageHeight) {
            // New Page
            //System.out.println(linesForThisPage + " lines printed for page " + pageNum);
            if(linesForThisPage == 0) {
               //System.out.println( "Font is too big for pages of this size; aborting...");
               break;
            }
            pageNum++;
            linesForThisPage = 0;
            pg.dispose();
            pg = pjob.getGraphics();
            if (pg != null) {
              pg.setFont(helv);
            }
            curHeight = 0;
          }
          curHeight += fontHeight;
          if (pg != null) {

              if(nextLineE.length()>35)
                  nextLineE=nextLineE.substring(0,34)+".";
              pg.drawString(nextLineE, margin, curHeight - fontDescent);

             // pg.drawString(nextLineW, margin+170+(5-nextLineW.length()), curHeight - fontDescent);


   printInt( pg,nextLineW,270,curHeight - fontDescent);
              linesForThisPage++;
            linesForThisJob++;

          }
          else {
            //System.out.println("pg null");
          }
        }
      } while (nextLineE != null);

    helv = new Font("Helvetica", Font.BOLD, 10);
    //have to set the font to get any output
    pg.setFont (helv);
   pg.drawString(merci, margin+50, curHeight + 10);

    helv = new Font("Helvetica", Font.BOLD, 7);
    //have to set the font to get any output
    pg.setFont (helv);
    pg.drawString(down, margin+10, curHeight + 20);

    }
    catch (EOFException eof) {
     cashier.db.insertError(eof+"","print");
    }
    catch (Throwable t) { // Anything else
    cashier.db.insertError(t+"","print");
    }

  }
public void printInt(Graphics pg,String s,int w,int h)
  {

  int back=0;

  for(int i=s.length()-1;i>=0;i--)
  {
      if(s.charAt(i)==' ')
      back+=2;
      else
       back+=5;

      pg.drawString(""+s.charAt(i),w-back,h);

  }

}

/**********************************************************************************************
EXIT POUR deconnecte l'usager qui quitte MAJ DE LA BASE DE DONNES
**********************************************************************************************/

    @Override
protected void processWindowEvent(WindowEvent evt)
{
   if(WindowEvent.WINDOW_CLOSING == evt.getID())
   {

			//default icon, custom title
			int n = JOptionPane.showConfirmDialog(frame,"URASHAKA KURANGIZA KOKO","TEKEREZA NIBA WARANGIJE BYOSE",JOptionPane.YES_NO_OPTION);
			if(n==0)
			{
			   try {
     LinkedList <String> list=cashier.db.getFichier("log.txt");
     AutoOut.backUpDatabase(cashier.db.conn , list.getFirst());//

               }
               catch (Throwable ex) {
    cashier.db.insertError(ex+"","back up");

    }
			   saleJList.setListData(new String [0]);

                           try
                           {
                           //cashier.db.doFinishSales(cashier.todaySales);
                           //cashier.db.doFinishCredit(cashier.credit);
                           cashier.db.insertLogin(nom,"out",Cashier.yose);


                           String sto = " UMUKESIYE : "+nom+" ARARANGIJE ";
                            sto=sto+"\n IGIHE :  "+new Date ().toLocaleString();
                            String sto1="\n-----------------------"; String sto2="\n----------------------- ";
                             //sto1=sto1+"\n";  sto2=sto2+"\n ";
                             sto1=sto1+"\n MWAKOREYE AMAFARANGA :  ";sto2=sto2+"\n"+setVirgule(Cashier.yose);
                             sto1=sto1+"\n-----------------------"; sto2=sto2+"\n----------------------- ";
                             sto1=sto1+"\n ";   sto2=sto2+"\n ";
                             sto1=sto1+"\n           IBIHE BYIZA";   sto2=sto2+"\n ";
                              //sto1=sto1+"\n";  sto2=sto2+"\n ";
                            // sto1=sto1+"\n";
                           PrintCommand(sto,sto1,sto2,"","");

                          cashier.db.closeConnection();


                            }  catch (Throwable e)  {cashier.db.insertError(e+"","processWindowEvent");}

			 JOptionPane.showMessageDialog(frame,Cashier.yose+" RWF","UFITE MURI CAISSE ",JOptionPane.PLAIN_MESSAGE);


			   System.exit(0);


                        }




	}

}
public static int getIn (String s,String s1,int i)
{
    int entier=-1;

    String in = JOptionPane.showInputDialog(null, s+s1);
   boolean done =true;

    while(in!=null && i<3&& done==true)
    try
    {
        if(in.equals(""))
        {
        i++;
       in = JOptionPane.showInputDialog(null, s+" Enter a number ","1");
        }
        else
        {
        Integer in_int=new Integer(in);
        entier = in_int.intValue();
        done=false;
        if(entier<0)
            entier=-1;
        }

     }


    catch (NumberFormatException e)
    {
        i++;
        in = JOptionPane.showInputDialog(null, s+" = "+in+"= Not a number; Enter a number ");
        done=true;
    }
     if(i==3)
        {
        JOptionPane.showMessageDialog(frame," Too many try "," Sorry we have to Exit ",JOptionPane.PLAIN_MESSAGE);
        }

    return entier;
}
/***************************************************************************
le main qui lance le clavardage
****************************************************************************/
public static void main(String[] arghs)
{
StartServer d;
try
{
  d  =new StartServer ();

int id = getIn("Nomero yumukozi "," : umubare",0);
 if(id!=-1)
{
 JPanel pw=new JPanel();
 JPasswordField passwordField = new JPasswordField(10);
 JLabel label = new JLabel("Umubare w'ibanga: ");
 pw.add(label);pw.add(passwordField);
 passwordField.setText("");
 JOptionPane.showConfirmDialog(null,pw);
 Integer in_int=new Integer(passwordField.getText());
 int carte = in_int.intValue();

       LinkedList<String> give= new LinkedList();

        File productFile= new File("log.txt");
        String  ligne =null;
        BufferedReader entree=new BufferedReader(new FileReader(productFile));
        try{ ligne=entree.readLine(); }
        catch (IOException e){ System.out.println(e);}
        int i=0;
        while(ligne!=null)
        {
        // System.out.println(i+ligne);
        give.add(ligne);
        i++;

        try{ ligne =entree.readLine();}
        catch (IOException e){ JOptionPane.showMessageDialog(null,"Ikibazo"," reba log ",JOptionPane.PLAIN_MESSAGE);}
        }
        entree.close();


    int frw = getIn("Amafaranga mutangiranye "," : INT",0);
    // String dataBase  = (JOptionPane.showInputDialog(frame, " dataBase ","score"));
//String serv="localhost";

 String dataBase=give.get(2);
  String serv=give.get(1);
 // String serv  = (JOptionPane.showInputDialog(frame, " Server ","192.168.0.100"));

// String serv="localhost";
        if(carte !=-1 && frw!=-1)
        {
            Cashier c  =new Cashier(id,serv,dataBase);
            c.db.ip=d.ip;

         //   JOptionPane.showMessageDialog(null," cashier"," in ",JOptionPane.PLAIN_MESSAGE);

            if(c!=null && c.db.check_db(id,carte))
        {     Employe n = c.db.getName(id);
              Cashier.yose=frw;
              c.db.insertLogin(n.NOM_EMPLOYE,"in",frw);
   // JOptionPane.showMessageDialog(null," Main"," in ",JOptionPane.PLAIN_MESSAGE);
           new Main(n,c,serv,dataBase);
        }
        else
          JOptionPane.showMessageDialog(null,"  UMUBARE WIBANGA SIWO "," IKIBAZO ",JOptionPane.PLAIN_MESSAGE);
           } } }
          catch(Throwable t)
        { JOptionPane.showMessageDialog(null," Fail to Open "+t," ERROR ",JOptionPane.PLAIN_MESSAGE);  }
           }
}



        /////////////////////////////////////////////////

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package test;
import java.awt.*;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Properties;
import java.util.StringTokenizer;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author Kimenyi
 */
public class Print extends JFrame {

LinkedList <Lot> lotList;
int pageNum = 0;
private Properties properties = new Properties();
int margin=0;
int numero;
double total,tva;
Client frss;
String date,reference,mode;
String [] footer;
AutoOut db;
Print(LinkedList <Lot> lotList,AutoOut db,String mode ,String date,Client frss ,int tva,double total,String [] footer)
{
    System.out.println(total);
this.lotList=lotList;
this.db=db;
this.mode=mode;
this.margin=getValue("margin");
this.date=date;
this.frss=frss;

this.tva=tva;
this.total=total;
this.footer=footer;
PrintCommand();

}
private void PrintCommand()
{
    PrintJob pjob = getToolkit().getPrintJob(this, "Ishyiga ", properties);
    if (pjob != null) {
    Graphics pg = pjob.getGraphics();
    if (pg != null) {

    printCommande(pjob, pg);
    pg.dispose();
    }
    pjob.end();
    }
}
private void printCommande (PrintJob pjob, Graphics pg) {



   if (!(pg instanceof PrintGraphics)) {
      throw new IllegalArgumentException ("Graphics context not PrintGraphics");
    }

    int pageH = pjob.getPageDimension().height - margin;
    int pageW = pjob.getPageDimension().width - margin;

  if (pg != null)
  {
    BufferedInputStream bis = null;
    try {
    Font helv = new Font("Helvetica", Font.BOLD, getValue("dateT"));
    pg.setFont(helv);
    Image img=null;
    bis = new BufferedInputStream(new FileInputStream(getString("logoV")));
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    try {
    int ch;
    while ((ch = bis.read()) != -1) {
    baos.write(ch);
    System.err.println(ch);
    }
    System.err.println(img);
    img = Toolkit.getDefaultToolkit().createImage(baos.toByteArray());
     System.err.println(img);
    System.err.println(img.getWidth(rootPane));
    } catch (IOException exception) {
    System.err.println("Error loading: ");
    }
    pg.drawString(getString("ville")+", le " + date.substring(0, 2) + "/" + date.substring(2, 4) + "/" + date.substring(4, 6), pageW - getValue("dateX"), getValue("dateY"));
    helv = new Font("Helvetica", Font.BOLD, 25);
    pg.setFont(helv);

    Thread t = new Thread();
    t.start();
    t.sleep(5000);
    pg.drawImage(img, getValue("logoX"), getValue("logoY"), 130, 35, rootPane);

    helv = new Font("Helvetica", Font.PLAIN, getValue("adresseT"));
    //have to set the font to get any output
    pg.setFont(helv);
    FontMetrics fm = pg.getFontMetrics(helv);
    int fontHeight = fm.getHeight();
    int fontDescent = fm.getDescent();

    pg.drawString(getString("adresseV"), getValue("adresseX"),  getValue("adresseY"));

    pg.drawString(getString("rueV"), getValue("rueX"),  getValue("rueY"));

    pg.drawString(getString("localiteV"), getValue("localiteX"),  getValue("localiteY"));

    pg.drawString(getString("telV"), getValue("telX"), getValue("telY"));

    pg.drawString(getString("faxV"), getValue("faxX"), getValue("faxY"));

    pg.drawString(getString("emailV"), getValue("emailX"), getValue("emailY"));

    pg.drawString(getString("tvaV"), getValue("tvaX"),  getValue("tvaY"));


    pg.drawRect(getValue("clientX"), getValue("clientY"), getValue("clientLongeur"), getValue("clientLargeur"));


    Font helv2 = new Font("Helvetica", Font.BOLD, getValue("clientT"));
    pg.setFont(helv2);

    pg.drawString(frss.ASSURANCE, getValue("clientX") + 10,  getValue("clientY") + 10);
    helv2 = new Font("Helvetica", Font.ITALIC, getValue("clientT"));
    pg.setFont(helv2);
    pg.drawString(frss.NOM_CLIENT, getValue("clientX" ) + 10, getValue("clientY" ) + 25);
    pg.drawString(frss.PRENOM_CLIENT, getValue("clientX" ) + 10,  getValue("clientY" ) + 40);

    Font helv1 = new Font("Helvetica", Font.BOLD, getValue("factureT"));
    pg.setFont(helv1);
     String ink = JOptionPane.showInputDialog(null, " Numero Facture "," ");

    pg.drawString(getString("factureV") +ink, getValue("factureX"), getValue("factureY"));
    helv2 = new Font("Helvetica", Font.PLAIN, getValue("referenceT"));
    pg.setFont(helv2);

   // pg.drawString(getString("referenceV") + reference, getValue("referenceX"), getValue("referenceY"));

    helv1 = new Font("Helvetica", Font.BOLD, 10);
    pg.setFont(helv1);
    pageNum++;
    pg.drawString("Page  " + pageNum, pageW / 2, pageH - 9);

    helv2 = new Font("Helvetica", Font.BOLD, 10);
    pg.setFont(helv2);


            int g = getValue("length");
            String[] entete = new String [g];
            int[] longeur = new int[g+1];
            String[] variable= new String [g];
            int [] dimension=new int[g];
            longeur[0]=0;

            int page=1;


            int y=  getValue("entete"+page+"X");

            for(int k=0;k<g;k++)
            {

            entete[k]=getString("entete_"+(k+1)+"Valeur");
            longeur[k+1]=getValue("entete_"+(k+1)+"Largeur");

            variable[k]=getString("entete_"+(k+1)+"Variable");
            dimension[k]=getValue("entete_"+(k+1)+"Dimension");
 // System.out.println(" d   "   +getValue("entete_"+(k+1)+"Dimension"));
            }

            int w = margin;

            int curHeightLigne = getValue("ligne"+page+"X");

            Font helvLot = new Font("Helvetica", Font.PLAIN, 10);
            pg.setFont(helvLot);
            FontMetrics fmLot = pg.getFontMetrics(helvLot);
            fontHeight = fmLot.getHeight();
            boolean done = true;

 for (int j = 0; j < lotList.size(); j++) {

            helvLot = new Font("Helvetica", Font.PLAIN, getValue("tailleLigne"));
            pg.setFont(helvLot);
            Lot ci = lotList.get(j);

            w = margin;

            if (done) {
            curHeightLigne += fontHeight;
            } else {
            curHeightLigne += 2 * fontHeight;
            }
            done = true;
            for (int i = 0; i < g; i++) {
            w += (longeur[i]);
            String valeur=ci.vars[i];

            String variab=variable[i];
            int dim      =dimension[i];

           System.out.println(valeur+" dim  "+dim);// setVirguleD(String frw)
            if(variab.equals("int"))
            printInt(pg,setVirgule(valeur), w + (longeur[i + 1]) - 10, curHeightLigne);

            else  if(variab.equals("double"))
            printInt(pg,setVirguleD(valeur), w + (longeur[i + 1]) - 10, curHeightLigne);

            else if (valeur.length() < dim)
            {
            pg.drawString(valeur, w + 5, curHeightLigne);
            }
            else
            {
            done = false;
            pg.drawString(valeur.substring(0, dim - 1), w + 5, curHeightLigne);
            pg.drawString(valeur.substring(dim - 1, valeur.length()), w + 5, fontHeight + curHeightLigne);
            }
            }

 int hauteur = pageH - getValue("marginfooter");

            if ((curHeightLigne+20) >  hauteur-20) {

            helv1 = new Font("Helvetica", Font.BOLD, getValue("enteteTaille"));
            pg.setFont(helv1);
            w = margin;

            for (int i = 0; i < g; i++) {
            w += (longeur[i]);
            pg.drawLine(w,y, w, hauteur);//ligne zihagaze
            pg.drawString(entete[i], w + 2, y +15);// amazina
            }
            pg.drawLine(margin, hauteur, w, hauteur);//umurongo wo hasi
            pg.drawRect(margin, y , w - margin,25 ); // cadre ya entete

            /////////////////////////////////////////////////////////////////////////////////////////////
            pg.dispose();
            pg = pjob.getGraphics();
            if (pg != null)
            {
            pg.setFont(helv);
            }
            helv1 = new Font("Helvetica", Font.PLAIN, getValue("factureT"));
            pg.setFont(helv1);

            pg.drawString(getString("factureV") + ink, margin, margin + 25);
            helv1 = new Font("Helvetica", Font.BOLD, 10);
            pg.setFont(helv1);
            pageNum++;page=2;
            pg.drawString("Page  " + pageNum, pageW / 2, pageH - 10);
              curHeightLigne=getValue("entete"+page+"X")+25;
              y=getValue("entete"+page+"X");
            done = true;
            }
 }


///////////////////////////////////////////////////////////////////////////////////////////////////////////

            if((curHeightLigne+20) > ( pageH -getValue("longeurLigne")) )
            {
                 int hauteur = pageH - getValue("marginfooter");
            helv1 = new Font("Helvetica", Font.BOLD, getValue("enteteTaille"));
            pg.setFont(helv1);
            w = margin;

            for (int i = 0; i < g; i++) {
            w += (longeur[i]);
            pg.drawLine(w,y, w, hauteur);//ligne zihagaze
            pg.drawString(entete[i], w + 2, y +15);// amazina
            }
            pg.drawLine(margin, hauteur, w, hauteur);//umurongo wo hasi
            pg.drawRect(margin, y , w - margin,25 ); // cadre ya entete

            /////////////////////////////////////////////////////////////////////////////////////////////
            pg.dispose();
            pg = pjob.getGraphics();
            if (pg != null)
            {
            pg.setFont(helv);
            }
            helv1 = new Font("Helvetica", Font.PLAIN, getValue("factureT"));
            pg.setFont(helv1);
            pg.drawString(getString("factureV") + numero, margin, margin + 25);
            helv1 = new Font("Helvetica", Font.BOLD, 10);
            pg.setFont(helv1);
            pageNum++;page=2;
           pg.drawString("Page  " + pageNum, pageW / 2, pageH - 10);
             curHeightLigne=getValue("entete"+page+"X")+25;
            done = true;
            hauteur = pageH - getValue("longeurLigne");
            helv1 = new Font("Helvetica", Font.BOLD, getValue("enteteTaille"));
            pg.setFont(helv1);
            w = margin;
            if(pageNum>1)
                page=2;
            y=getValue("entete"+page+"X");
            for (int i = 0; i < g; i++) {
            w += (longeur[i]);
            pg.drawLine(w,y, w, hauteur);//ligne zihagaze
            pg.drawString(entete[i], w + 2, y + 15);// amazina
            }
            pg.drawLine(margin, hauteur, w, hauteur);//umurongo wo hasi
            pg.drawRect(margin, y , w - margin, 25); // cadre ya entete

            }
            else
            {

            int hauteur = pageH - getValue("longeurLigne");
            helv1 = new Font("Helvetica", Font.BOLD, getValue("enteteTaille"));
            pg.setFont(helv1);
            w = margin;
            if(pageNum>1)
                page=2;
            y=getValue("entete"+page+"X");
            for (int i = 0; i < g; i++) {
            w += (longeur[i]);
            pg.drawLine(w,y, w, hauteur);//ligne zihagaze
            pg.drawString(entete[i], w + 2, y + 15);// amazina
            }
            pg.drawLine(margin, hauteur, w, hauteur);//umurongo wo hasi
            pg.drawRect(margin, y , w - margin, 25); // cadre ya entete
            }
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        w = margin;
                        helv2 = new Font("Helvetica", Font.BOLD, getValue("prixTaille"));
                        pg.setFont(helv2);
                        int lp = getValue("lengthPrix");
                        String[] enteteP = new String [lp+1];
                        int[] longeurP = new int[lp+1];
                        String[] variableP= new String [lp];
                        longeurP[0]=0;



                        for(int k=0;k<lp;k++)
                        {

                        enteteP[k]=getString("prix_"+(k+1)+"Valeur");
                        if(enteteP[k].length()>2)
                        enteteP[k]=enteteP[k]+"  FRW";
                        longeurP[k+1]=getValue("prix_"+(k+1)+"Largeur");
                        variableP[k]=getString("prix_"+(k+1)+"Variable");
                        System.out.println(longeurP[k+1]);
                        }
                        int Xprix = getValue("longeurPrix");
                        int marginP=getValue("marginPrix");
                        w =marginP ;
                        for (int i = 0; i < lp; i++) {
                        w += (longeurP[i]);
                        System.out.println(( w + (longeurP[i + 1]) - 40)+"  kkk "+(Xprix+25)+"  total "+total+"   i "+i);

                      int wid=w + (longeurP[i + 1]) - 20;
                        if(i==0)
                        printInt(pg,setVirgule( ""+(int)total),  wid, Xprix+15);
                        if(i==1)
                        printInt(pg,setVirgule(""+ (int)tva),  wid, Xprix+15);
                        if(i==2)
                        printInt(pg,setVirgule( ""+(int)(total-tva)),  wid, Xprix+15);

                        pg.drawLine(w,Xprix-20, w, Xprix+20);//ligne zihagaze
                        System.out.println( w +" x1 y1 "+(Xprix-20)+"  y2 "+ Xprix+20);
                        pg.drawString(enteteP[i], w + 2, Xprix - 5);// amazina
                       System.out.println(enteteP[i]+"entete   x=w+2   yxprix-5= "+(Xprix - 5));
//+frss.devise
                        }
                         if(lp!=0)
                        {
                        pg.drawLine(marginP, Xprix, w, Xprix);//umurongo wo hasi
                        System.out.println(marginP+"marginP  lineMidlle y=xprix   y= "+w);
                        pg.drawRect(marginP, Xprix-20 , w-marginP , 40); // cadre ya entete
                         System.out.println(marginP+" x   cadre y Xprix-20"+( Xprix-20)+" length= "+(w-marginP));

                        }


                                    helv2 = new Font("Helvetica", Font.BOLD, getValue("footer_1Taille" ) );
                                    pg.setFont(helv2);
                                    for(int v=1;v<getValue("lengthfooter");v++)
                                    pg.drawString(getString("footer_"+v+"Valeur")+  " "+footer[v], getValue("footer_"+v+"X" ), getValue("footer_"+v+"Y" ));


            } catch (InterruptedException ex) {
                System.out.println(ex);
            } catch (FileNotFoundException ex) {
                System.out.println(ex);
            }
           finally {
                try {
                    bis.close();
                } catch (IOException ex) {
                     System.out.println(ex);
                }
            }

  }

}

int getValue(String s)
{

int ret=0;
Variable var= db.getParam("PRINT",mode,s+mode);
 if(var!=null)
 {
Double val=new Double (var.value);
ret = val.intValue();
}

return ret;
}
String getString(String s)
{
String ret=" ";
 Variable var= db.getParam("PRINT",mode,s+mode);
 if(var!=null)
     ret=var.value;
     return ret;
}
public void printInt(Graphics pg,int nbre,int w,int h)
  {
  String s=setVirgule(nbre);

  int back=0;

  for(int i=s.length()-1;i>=0;i--)
  {
      if(s.charAt(i)==' ')
      back+=2;
      else
       back+=5;

      pg.drawString(""+s.charAt(i),w-back,h);

  }
}
public void printInt(Graphics pg,String s,int w,int h)
  {

  int back=0;

  for(int i=s.length()-1;i>=0;i--)
  {
      if(s.charAt(i)==' ')
      back+=2;
      else
       back+=5;


      pg.drawString(""+s.charAt(i),w-back,h);

  }




  }
  String setVirgule(int frw)
        {

String setString = ""+frw;
int l =setString.length();



if(l>3 && l<=6)
setString= setString.substring(0,l-3)+" "+setString.substring(l-3) ;
if(l>6)
{

String s1 = setString.substring(0,l-3);
int sl=s1.length();
setString=s1.substring(0,sl-3)+" "+s1.substring(sl-3)+" "+setString.substring(l-3)  ;

}

return setString;
}
  String setVirgule(String setString)
        {


int l =setString.length();



if(l>3 && l<=6)
setString= setString.substring(0,l-3)+" "+setString.substring(l-3) ;
if(l>6)
{

String s1 = setString.substring(0,l-3);
int sl=s1.length();
setString=s1.substring(0,sl-3)+" "+s1.substring(sl-3)+" "+setString.substring(l-3)  ;

}

return setString;
}
  String setVirguleD(String frw)
        {

StringTokenizer st1=new StringTokenizer (frw);

    String entier =st1.nextToken(".");
System.out.println(frw);
    String decimal=st1.nextToken("").replace(".", "");

    if(decimal.length()==1 && decimal.equals("0") )
            decimal=".00";
    else if (decimal.length()==1 && !decimal.equals("0") )
          decimal="."+decimal+"0";
    else
    decimal="."+decimal.substring(0, 2);

String setString = entier+decimal;

int l =setString.length();
if(l<2)
   setString = "    "+frw;
else if(l<3)
   setString = "  "+frw;
else if(l<4)
   setString = "  "+frw;

int up=6;
int ju=up+3;
if(l>up && l<=ju)
{
    setString= setString.substring(0,l-up)+","+setString.substring(l-up) ;
}
else if(l>ju)
{
setString=setString.substring(0,l-9)+","+setString.substring(l-9,l-6)+","+setString.substring(l-6,l)  ;

}

return setString;
}

public static void main(String[] arghs)
{
    //new PrintDoc();
}




}


//////////////////////////////////////////////////


package test;



import java.io.*;

import java.sql.*;
import java.util.Date;
import java.util.LinkedList;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public  class AutoOut
{
    //jdbc:derby://192.168.0.113:1527/ishyiga;create=true
//   ## DEFINE VARIABLES SECTION ##
// define the driver to use ResultSet
String driver = "org.apache.derby.jdbc.NetWorkDriver";

// define the Derby connection URL to use  192.168.0.113 99.240.205.21
String connectionURL ;//= "jdbc:derby://localhost:1527/" + dbName + ";create=true";
//String connectionURL = "jdbc:derby://99.240.205.21:1527/" + dbName + ";create=true";
//String connectionURL = "jdbc:derby://192.168.0.113:1527/" + dbName + ";create=true";
ResultSet outResult, outResult1;
Connection conn = null;
Statement s;
LinkedList <Product>allProduct ;
LinkedList allEmploye ;
LinkedList<Lot>lot = new LinkedList();
PreparedStatement psInsert;
PreparedStatement psInsert1;
String  datez,mois,observa,ip;
 LinkedList <String>controlInvoice ;
   AutoOut(String server,String dataBase){
connectionURL = "jdbc:derby://"+server+":1527/" + dataBase + ";create=true";

}
 public  void read()
{
  Date d= new Date();

        String day="";
        String mm="";
        String year="";


         if(d.getDate()<10)
        day="0"+d.getDate();
        else
        day=""+d.getDate();
        if((d.getMonth()+1)<10)
        mm="0"+(d.getMonth()+1);
        else
        mm=""+(d.getMonth()+1);
        if((d.getYear()-100)<10)
        year="0"+(d.getYear()-100);
        else
        year=""+(d.getYear()-100);



                 datez =day+mm+year;

                 mois= mm;

    try {
    // Create (if needed) and connect to the database
    conn = DriverManager.getConnection(connectionURL);

    s = conn.createStatement();
System.out.println("connection etablie");
    }
    catch (Throwable e){

      insertError(e+"","Read");
    }
}
 public static void backUpDatabase(Connection conn,String file)throws SQLException
    {
    // Get today's date as a string:
    java.text.SimpleDateFormat todaysDate =new java.text.SimpleDateFormat("yyyy-MM-dd");
int f= (int)(5*Math.random());
String backupdirectory=file;


    CallableStatement cs = conn.prepareCall("CALL SYSCS_UTIL.SYSCS_BACKUP_DATABASE(?)");

    cs.setString(1, backupdirectory);
    cs.execute();
    cs.close();
    }

public int  getQuantite(int pro)
{

    int id=0;
    try
{
outResult = s.executeQuery("select  sum(qty_live) from APP.lot where id_product= "+pro);
while (outResult.next())
{
id =outResult.getInt(1);

}
  }  catch (Throwable e)  {
   insertError(e+"","getQuantite") ;
    }

 return id;
}
String [] clients ()
{
    String [] control= null;
    LinkedList<String> clien  =new LinkedList();
    try
    {


    outResult = s.executeQuery("select NOM_VARIABLE from APP.VARIABLES WHERE FAMILLE_VARIABLE='CLIENT' order by NOM_VARIABLE ");



    while (outResult.next())
    {
      clien.add(outResult.getString(1));
    }

    //  Close the resultSet
outResult.close();

      }  catch (Throwable e)  {

   insertError(e+""," V getlot");
    }
    control= new String [clien.size()];
    for(int i=0;i<clien.size();i++)
        control[i]=clien.get(i);
return control;

}

public int  CashierYose(String nom,String date)
{

    int id=0;
    try
{
outResult = s.executeQuery("select  SUM(total) from APP.INVOICE where num_client='CASHNORM'AND APP.INVOICE.DATE='"+date+"' AND EMPLOYE='"+nom+"'");
while (outResult.next())
{
id =outResult.getInt(1);

}
outResult = s.executeQuery("select  SUM(payed) from APP.INVOICE,APP.CREDIT where num_client!='CASHNORM'AND APP.INVOICE.DATE='"+date+"' AND EMPLOYE='"+nom+"' and app.credit.id_invoice=app.invoice.id_invoice");
while (outResult.next())
{
id =id+outResult.getInt(1);

}
  }  catch (Throwable e)  {
   insertError(e+"","CashierYose") ;
    }

 return id;
}

 void changePrice  (int c,double p,double tva,String db,String name,String  productName)
 {

   try
    {

       if(db.equals("CASHNORM"))
          s.execute(" update APP.PRODUCT set PRIX ="+p+", TVA = "+tva+" where ID_PRODUCT="+c+"");
       else
        s.execute(" update APP.PRODUCT set PRIX_"+db+" ="+p+ "where ID_PRODUCT="+c+"");

       if(c!=5)
       insertChangePrice(name,productName,p);

          JOptionPane.showMessageDialog(null,db," INPUT ",JOptionPane.PLAIN_MESSAGE);

     }  catch (Throwable e)  {
         insertError(e+"","changePrice") ;
     }

 }
  void changePriceRama  (int c,double p,String soc,String db,String name,String  productName)
 {

   try
    {


        s.execute(" update APP.PRODUCT set PRIX_RAMA ="+p+ ",CODE_SOC_RAMA = '"+soc+ "' where ID_PRODUCT="+c+"");

       if(c!=5)
       insertChangePrice(name,productName,p);

          JOptionPane.showMessageDialog(null,db," INPUT ",JOptionPane.PLAIN_MESSAGE);

     }  catch (Throwable e)  {
         insertError(e+"","changePriceRama") ;
     }

 }
 void insertMissed(String desi,int qty,double price,int qty_dispo)
    {
        try {
            psInsert = conn.prepareStatement("insert into APP.MISSED(DESI,QUANTITE,PRICE,qty_dispo)  values (?,?,?,?)");

            psInsert.setString(1, desi);
            psInsert.setInt(2, qty);
            psInsert.setDouble(3, price);
            psInsert.setInt(4, qty_dispo);

            psInsert.executeUpdate();
            psInsert.close();
        } catch (SQLException ex) {
             insertError(ex+"","insertMissed") ;
        }


          }

 void insertError(String desi,String ERR)
    {
        try {
            psInsert = conn.prepareStatement("insert into APP.ERROR(DESI,USERNAME)  values (?,?)");

            if(desi.length()>160)
                desi=desi.substring(0, 155);
             if(ERR.length()>160)
                ERR=ERR.substring(0, 155);

            psInsert.setString(1,desi );
            psInsert.setString(2, ERR);



            psInsert.executeUpdate();
            psInsert.close();
            JOptionPane.showMessageDialog(null,"  "+desi," Ikibazo ",JOptionPane.PLAIN_MESSAGE);



        } catch (Throwable ex) {
         JOptionPane.showMessageDialog(null,"  "+ex," Ikibazo ",JOptionPane.PLAIN_MESSAGE);   ;
        }


          }
 void insertChangePrice(String USERNAME ,String PRODUCTNAME,double PRICE)
    {
        try {
            psInsert = conn.prepareStatement("insert into APP.CHANGEMENT(PRICE,USERNAME,PRODUCTNAME)  values (?,?,?)");


            psInsert.setDouble(1, PRICE);
            psInsert.setString(2, USERNAME );
            psInsert.setString(3, PRODUCTNAME );
            psInsert.executeUpdate();
            psInsert.close();
        } catch (Throwable ex) {
        insertError(ex+"","insertChangePrice");
        }


          }



public void addOneLot(Lot l)
{
     try
    {

     psInsert = conn.prepareStatement("insert into APP.LOT (ID_PRODUCT,ID_LOT,ID_LOTS,DATE_EXP,QTY_START,QTY_LIVE,BON_LIVRAISON)  values (?,?,?,?,?,?,?)");

    psInsert.setInt(1,l.productCode);
    psInsert.setInt(2,l.id_lot);
    psInsert.setString(3,l.id_LotS);
    psInsert.setString(4,l.dateExp);
    psInsert.setInt(5,l.qtyStart);
    psInsert.setInt(6,l.qtyLive);
    psInsert.setString(7,l.bon_livraison);
    psInsert.executeUpdate();
        psInsert.close();

          }  catch (Throwable e)  {   insertError(e+"","addOneLot"); }

}
void upDatePr(Product p)
{
 Statement sU;
        try {


            sU = conn.createStatement();
             String createString1 = " update APP.list set prix_revient=" +p.prix_revient+ " where id_invoice> 35555 and code_uni='"+p.code+"'";
 sU.execute(createString1);
        } catch (SQLException ex) {
            Logger.getLogger(AutoOut.class.getName()).log(Level.SEVERE, null, ex);
        }




}
public LinkedList doworkOther(String s1)
{
    try
    {
         observa=s1;
        System.out.println(s1);
        if(s1.equals("")|| s1.equals("_SOCIETE")|| s1.equals("_AAR")|| s1.equals("_FARG"))
        {
       outResult = s.executeQuery("select id_product,name_product,code,prix"+s1+", tva,code_bar,observation,prix_revient from PRODUCT order by NAME_PRODUCT");
    //  System.out.println(s1);
    allProduct= new LinkedList();
    //  Loop through the ResultSet and print the data
    while (outResult.next())
    {
    int productCode=outResult.getInt(1);
    String productName   =  outResult.getString(2);
    String code  =  outResult.getString(3);
    double currentPrice=outResult.getDouble(4);
    double tva=outResult.getDouble(5);
    String codeBar  =  outResult.getString(6);
    String observation  =  outResult.getString(7);
    // String codeSoc  =  outResult.getString(8);
    double pr=outResult.getDouble(8);

    Product p= new Product(productCode,productName, code, 1,currentPrice,tva,codeBar,observation,code,this,pr);

    if(p.currentPrice>0)
    allProduct.add(p);
     }
    //  Close the resultSet
    outResult.close();
    }

    if(s1.equals("_RAMA")|| s1.equals("_MMI")|| s1.equals("_CORAR")|| s1.equals("_SORAS"))
        {
       // System.out.println(s1);
       outResult = s.executeQuery("select id_product,name_product,code,prix"+s1+", tva,code_bar,observation,code_soc"+s1+",prix_revient from PRODUCT order by NAME_PRODUCT");

    allProduct= new LinkedList();
    //  Loop through the ResultSet and print the data
    while (outResult.next())
    {
        int productCode=outResult.getInt(1);
            String productName   =  outResult.getString(2);
            String code  =  outResult.getString(3);
            double currentPrice=outResult.getDouble(4);
            double tva=outResult.getDouble(5);
            String codeBar  =  outResult.getString(6);
            String observation  =  outResult.getString(7);
            String codeSoc  =  outResult.getString(8);
            double pr=outResult.getDouble(9);

    Product p= new Product(productCode,productName, code, 1,currentPrice,tva,codeBar,observation,codeSoc,this,pr);
     if(p.currentPrice>0)
            allProduct.add(p);
     }
    //  Close the resultSet
    outResult.close();
    }

    //  Beginning of the primary catch block: uses errorPrint method
    }  catch (Throwable e)  {
    insertError(e+"","doworkOther");
    }

    return allProduct;
}

public LinkedList <List > listTva(String d,String f)
{
    LinkedList <List >   li= new LinkedList();
    try
    {
        outResult = s.executeQuery("SELECT APP.INVOICE.ID_INVOICE,CODE_UNI,QUANTITE,(APP.LIST.QUANTITE*APP.LIST.PRICE*APP.PRODUCT.TVA) FROM APP.INVOICE,APP.LIST,APP.PRODUCT WHERE  invoice.HEURE >'"+d+"' AND invoice.HEURE <'"+f+"' and APP.INVOICE.ID_INVOICE=APP.LIST.ID_INVOICE AND APP.LIST.CODE_UNI=APP.PRODUCT.CODE and product.TVA > 0");

        //  Loop through the ResultSet and print the data
        while (outResult.next())
        {
        int id=outResult.getInt(1);
        String code   =  outResult.getString(2);
        int qte=outResult.getInt(3);
        double currentPrice=outResult.getDouble(4);
         li.add(new List (id,  currentPrice,qte,"", code));
     }
    //  Close the resultSet
    outResult.close();

    //  Beginning of the primary catch block: uses errorPrint method
    }  catch (Throwable e)  {
    insertError(e+"  list tva ","tva ");
    }

    return li;
}
public void listPrixErrone(String d,String f,String cli)
{

    LinkedList <List >   li= new LinkedList();
    try
    {String j="_"+cli;
     if(cli.equals(""))
        j=cli;
        outResult = s.executeQuery("SELECT APP.INVOICE.ID_INVOICE,CODE_UNI,QUANTITE,prix"+j+",price FROM APP.INVOICE,APP.LIST,product  where invoice.num_client='"+cli+"'  and invoice.id_invoice > " + d + " and invoice.id_invoice < " + f  +" and invoice.id_invoice=list.id_invoice AND APP.LIST.CODE_UNI=APP.PRODUCT.CODE" );

        //  Loop through the ResultSet and print the data
        while (outResult.next())
        {
        int id=outResult.getInt(1);
        String code   =  outResult.getString(2);
        int qte=outResult.getInt(3);
        double currentPrice=outResult.getDouble(4);
         li.add(new List (id,  currentPrice,qte,"", code));
     }
    //  Close the resultSet
    outResult.close();

    for(int i=0;i<li.size();i++)
    {
        List p=li.get(i);

           System.out.println("update list set price=" + p.price + " where invoice="+p.id_invoice+" and code_uni='" + p.code + "'");
                s.execute(" update list set price=" + p.price + " where id_invoice="+p.id_invoice+" and code_uni='" + p.code + "'");
    }
    } catch (SQLException ex) {
                Logger.getLogger(AutoOut.class.getName()).log(Level.SEVERE, null, ex);
            }
    }


public void tva(String d,String f)
{
    LinkedList <List > li=listTva(d,f);
    try
    {
         s.execute(" update APP.invoice set tva =0  WHERE  invoice.HEURE >'"+d+"' AND invoice.HEURE <'"+f+"'");
        for(int i=0;i<li.size();i++)
         s.execute(" update APP.invoice set tva =tva+"+li.get(i).price+" where id_invoice="+li.get(i).id_invoice);
     }  catch (Throwable e)  {  insertError(e+"tva","fixtva");}
}

public boolean existe(String s1)
{
    boolean done = false;
    try
    {
       outResult = s.executeQuery("select * from PRODUCT where code='"+s1+"'");

    while (outResult.next())
    {
            done=true;
         JOptionPane.showMessageDialog(null," existe"," THANK YOU ",JOptionPane.PLAIN_MESSAGE);

     }
    outResult.close();

    }

catch (Throwable e)  {
    insertError(e+"","existe");
    }

    return done;
}
public void UpdateDesignation()
{
    try
    {

   /*    outResult = s.executeQuery("select id_product,name_product from PRODUCT order by NAME_PRODUCT");
    //
    allProduct= new LinkedList();
    //  Loop through the ResultSet and print the data
    while (outResult.next())
    {

   int code=outResult.getInt(1);
    String productName   =  outResult.getString(2);
     String sl=productName.replace('É', 'E');

     sl=productName.replace('è', 'e');
  Product p= new Product(code,sl, "", 1,0,0,"","","",this);
  System.out.println(sl);
    allProduct.add(p);


        }
    //  Close the resultSet
    outResult.close();*/

/* for(int i=0;i<allProduct.size();i++)
 { System.out.println(allProduct.get(i));

   String productName=allProduct.get(i).productName;
   if(!productName.contains("'"))*/
    s.execute(" update APP.product set name_product ='Perdolan Bebe 12 Supp 1mg' where id_product="+1374);


    // }




    //  Beginning of the primary catch block: uses errorPrint method
    }  catch (Throwable e)  {
    insertError(e+"","doworkOther");
    }

}

 LinkedList<Product> doworkOtherData(String s1)
{
    try
    {
        if(s1.equals("")|| s1.equals("_SOCIETE")|| s1.equals("_AAR")|| s1.equals("_FARG"))
        {
       outResult = s.executeQuery("select id_product,name_product,code,prix"+s1+", tva,code_bar,observation from PRODUCT order by NAME_PRODUCT");
    //  System.out.println(s1);
    allProduct= new LinkedList();
    //  Loop through the ResultSet and print the data
    while (outResult.next())
    {
        int productCode=outResult.getInt(1);
            String productName   =  outResult.getString(2);
            String code  =  outResult.getString(3);
            double currentPrice=outResult.getDouble(4);
            double tva=outResult.getDouble(5);
            String codeBar  =  outResult.getString(6);
            String observation  =  outResult.getString(7);
           // String codeSoc  =  outResult.getString(8);
            Product p= new Product(productCode,productName, code, 1,currentPrice,tva,codeBar,observation,code,this);
          //  if(p.currentPrice>0)
            allProduct.add(p);
     }
    //  Close the resultSet
    outResult.close();
    }

    if(s1.equals("_RAMA")|| s1.equals("_MMI")|| s1.equals("_CORAR")|| s1.equals("_SORAS"))
        {
        System.out.println(s1);
       outResult = s.executeQuery("select id_product,name_product,code,prix"+s1+", tva,code_bar,observation,code_soc"+s1+" from PRODUCT order by NAME_PRODUCT");

    allProduct= new LinkedList();
    //  Loop through the ResultSet and print the data
    while (outResult.next())
    {
        int productCode=outResult.getInt(1);
            String productName   =  outResult.getString(2);
            String code  =  outResult.getString(3);
            double currentPrice=outResult.getDouble(4);
            double tva=outResult.getDouble(5);
            String codeBar  =  outResult.getString(6);
            String observation  =  outResult.getString(7);
            String codeSoc  =  outResult.getString(8);
            Product p= new Product(productCode,productName, code, 1,currentPrice,tva,codeBar,observation,codeSoc,this);
           // if(p.currentPrice>0)
            allProduct.add(p);

           // System.out.println(p);
     }
    //  Close the resultSet
    outResult.close();
    }

    //  Beginning of the primary catch block: uses errorPrint method
    }  catch (Throwable e)  {
 insertError(e+"","doworkOther"); }

    return allProduct;
}

Product makeProduct (String ligne,int i)
{
        StringTokenizer st1=new StringTokenizer (ligne);


        //recuperer le nom
        String productName =st1.nextToken(";");
        //recupere code
        String code=st1.nextToken(";");
       // String d=st1.nextToken(";");

      StringTokenizer st2=new StringTokenizer (code);
     String d =st2.nextToken("/");


       // System.out.println(code);

Double in_int=new Double(d);
//int prix = in_int.intValue();
// Double in_int=new Double(d);
double price = in_int.doubleValue();
        //recupere quantite

        return  new Product(i,productName, code, 1,price,0,code,"","",null);


        }

 Product getProduct (int pr)
{

   Product p=null;

    try
    {
    outResult = s.executeQuery("select id_product,name_product,code,prix, tva,code_bar,observation from PRODUCT WHERE ID_PRODUCT = "+pr);


    //  Loop through the ResultSet and print the data
    while (outResult.next())
    {

             int productCode=outResult.getInt(1);
            String productName   =  outResult.getString(2);
            String code  =  outResult.getString(3);
            double currentPrice=outResult.getDouble(4);
            double tva=outResult.getDouble(5);
            String codeBar  =  outResult.getString(6);
            String observation  =  outResult.getString(7);
             p= new Product(productCode,productName, code, 1,currentPrice,tva,codeBar,observation,code,this);

     }
    //  Close the resultSet
    outResult.close();


    //  Beginning of the primary catch block: uses errorPrint method
    }  catch (Throwable e)  {
    insertError(e+"","getProduct");
    }

    return p;
}
LinkedList <Product> doworkVedette(LinkedList<Vedette>v,String s1)
{
    LinkedList <Product> allProductVedette = new LinkedList();

    try
    {

        for(int i =0;i<v.size();i++)
        {
            Vedette ve=v.get(i);


     if(s1.equals("")|| s1.equals("_SOCIETE"))
        {
       outResult = s.executeQuery("select id_product,name_product,code,prix"+s1+", tva,code_bar,observation,prix_revient from PRODUCT WHERE ID_PRODUCT = "+ve.ID_PRODUCT+" order by NAME_PRODUCT");

    allProduct= new LinkedList();
    //  Loop through the ResultSet and print the data
    while (outResult.next())
    {
        int productCode=outResult.getInt(1);
            String productName   =  outResult.getString(2);
            String code  =  outResult.getString(3);
            double currentPrice=outResult.getDouble(4);
            double tva=outResult.getDouble(5);
            String codeBar  =  outResult.getString(6);
            String observation  =  outResult.getString(7);
           // String codeSoc  =  outResult.getString(8);
                        double pr=outResult.getDouble(8);

            Product p= new Product(productCode,productName, code, 1,currentPrice,tva,codeBar,observation,code,this,pr);
           p.qty=ve.qte;
            // if(p.currentPrice>0)
            allProductVedette.add(p);
     }
    //  Close the resultSet
    outResult.close();
    }

    if(s1.equals("_RAMA")|| s1.equals("_MMI")|| s1.equals("_CORAR")|| s1.equals("_SORAS"))
        {
       outResult = s.executeQuery("select id_product,name_product,code,prix"+s1+", tva,code_bar,observation,code_soc"+s1+",prix_revient from PRODUCT WHERE ID_PRODUCT = "+ve.ID_PRODUCT+"order by NAME_PRODUCT");

    allProduct= new LinkedList();
    //  Loop through the ResultSet and print the data
    while (outResult.next())
    {
        int productCode=outResult.getInt(1);
            String productName   =  outResult.getString(2);
            String code  =  outResult.getString(3);
            double currentPrice=outResult.getDouble(4);
            double tva=outResult.getDouble(5);
            String codeBar  =  outResult.getString(6);
            String observation  =  outResult.getString(7);
            String codeSoc  =  outResult.getString(8);
            double pr =outResult.getDouble(9);

            Product p= new Product(productCode,productName, code, 1,currentPrice,tva,codeBar,observation,code,this,pr);
            p.qty=ve.qte;
                   allProductVedette.add(p);
     }
    //  Close the resultSet
    outResult.close();
    }}}
      catch (Throwable e)  {
 insertError(e+"","doworkVedette");
    }

    return allProductVedette;
}
public void insertVedette()
{

    try
    {
       psInsert = conn.prepareStatement("insert into APP.VEDETTE (ID_VEDETTE,TYPE_VEDETTE ,CAT_VEDETTE,NUM_VEDETTE)  values (?,?,?,?)");
       int j=0;
         for(int i=0;i<12;i++)
       {
         psInsert.setInt(1,j);
         psInsert.setString(2,"VEDETTE");
         psInsert.setInt(3,1);
         psInsert.setInt(4,i);
         psInsert.executeUpdate();
         j++;
        }
         for(int i=0;i<12;i++)
       {     psInsert.setInt(1,j);
             psInsert.setString(2,"BIOTIQUE");
         psInsert.setInt(3,2);
         psInsert.setInt(4,i);
         psInsert.executeUpdate();
           j++;
        }
         for(int i=0;i<12;i++)
       {   psInsert.setInt(1,j);
             psInsert.setString(2,"HYPERTENSEUR.");
         psInsert.setInt(3,3);
         psInsert.setInt(4,i);
         psInsert.executeUpdate(); j++;
        }
         for(int i=0;i<12;i++)
       {
         psInsert.setInt(1,j);j++;
         psInsert.setString(2,"PROTOZOAIRE");
         psInsert.setInt(3,4);
         psInsert.setInt(4,i);
         psInsert.executeUpdate();
        } for(int i=0;i<12;i++)
       {
         psInsert.setInt(1,j);j++;
         psInsert.setString(2,"DIABETIQUE");
         psInsert.setInt(3,5);
         psInsert.setInt(4,i);
         psInsert.executeUpdate();
        }
        for(int i=0;i<12;i++)
       {
         psInsert.setInt(1,j);j++;
         psInsert.setString(2,"DOULEUR");
         psInsert.setInt(3,6);
         psInsert.setInt(4,i);
         psInsert.executeUpdate();
        }
        for(int i=0;i<12;i++)
       {
         psInsert.setInt(1,j);j++;
         psInsert.setString(2,"VERMIFUGE");
         psInsert.setInt(3,7);
         psInsert.setInt(4,i);
         psInsert.executeUpdate();
        }
        for(int i=0;i<12;i++)
       {
         psInsert.setInt(1,j);j++;
         psInsert.setString(2,"HISTAMINIQUE");
         psInsert.setInt(3,8);
         psInsert.setInt(4,i);
         psInsert.executeUpdate();
        }








      // Release the resources (clean up )
    psInsert.close();


     }  catch (Throwable e)  {  insertError(e+"","insertVedette");}

}
public LinkedList doVedette()
{
       LinkedList  vedette = new LinkedList();
    try
    {
    outResult = s.executeQuery("select * from APP.VEDETTE ");

    //  Loop through the ResultSet and print the data
    while (outResult.next())
    {
         int id =outResult.getInt(1);
         String type =outResult.getString(2);
         int cat =outResult.getInt(3);
        int num =outResult.getInt(4);
        int pro =outResult.getInt(5);
         int qte =outResult.getInt(6);
        // System.out.println(type);
        vedette.add(new Vedette(id, num,cat,type,pro,qte));
     }
    //  Close the resultSet
    outResult.close();




    //  Beginning of the primary catch block: uses errorPrint method
    }  catch (Throwable e)  {
    insertError(e+"","doVedette");
    }
   return vedette;

}

public void fixVedette(Vedette v)
{
    try
    {
        System.out.println(" update APP.VEDETTE set ID_PRODUCT ="+v.ID_PRODUCT+", qte="+v.qte+" where ID_VEDETTE="+v.id+"");
          s.execute(" update APP.VEDETTE set ID_PRODUCT ="+v.ID_PRODUCT+", qte="+v.qte+" where ID_VEDETTE="+v.id+"");

     }  catch (Throwable e)  {  insertError(e+"","fixVedette");}

}

void emp()
{
    try
  {


    outResult = s.executeQuery("select * from APP.EMPLOYE ");
    allEmploye= new LinkedList();
    //  Loop through the ResultSet and print the data
   while (outResult.next())
    {
    allEmploye.add(new Employe(outResult.getInt(1),outResult.getInt(5),outResult.getInt(6),outResult.getInt(9),outResult.getString(2),outResult.getString(3),outResult.getString(4),outResult.getString(7),outResult.getString(8)));
    }

    //  Close the resultSet
    outResult.close();



    //  Beginning of the primary catch block: uses errorPrint method
    }  catch (Throwable e)  {
   insertError(e+"","employe");
    }



}
public boolean check_db(int id, int carte)
{
int c =allEmploye.size();
		boolean done=false;

		for(int i=0;i<c;i++)
		{
			Employe e= ((Employe)(allEmploye.get(i)));
                        if(e.check(id, carte))
                            done=true;

}
return done;
}

public Employe getName(int id)
{

Employe s1 =new Employe(0,0,0,0,"","","","","");
      try
  {


    outResult = s.executeQuery("select * from APP.EMPLOYE WHERE ID_EMPLOYE="+id);
    allEmploye= new LinkedList();
    //  Loop through the ResultSet and print the data
   while (outResult.next())
    {
    s1=new Employe(outResult.getInt(1),outResult.getInt(5),outResult.getInt(6),outResult.getInt(9),outResult.getString(2),outResult.getString(3),outResult.getString(4),outResult.getString(7),outResult.getString(8));
   allEmploye.add(s1);
   }

    //  Close the resultSet
    outResult.close();
   }  catch (Throwable e)  {

   JOptionPane.showMessageDialog(null," Umukozi  "," Ikibazo ",JOptionPane.ERROR_MESSAGE);


          }
return s1;
}
public Employe getEmp(String  name)
{

Employe s1 =new Employe(0,0,0,0,"","","","","");
      try
  {


    outResult = s.executeQuery("select * from APP.EMPLOYE WHERE NOM_EMPLOYE='"+name+"'");
    allEmploye= new LinkedList();
    //  Loop through the ResultSet and print the data
   while (outResult.next())
    {
    s1=new Employe(outResult.getInt(1),outResult.getInt(5),outResult.getInt(6),outResult.getInt(9),outResult.getString(2),outResult.getString(3),outResult.getString(4),outResult.getString(7),outResult.getString(8));
   allEmploye.add(s1);
   }

    //  Close the resultSet
    outResult.close();
   }  catch (Throwable e)  {

   JOptionPane.showMessageDialog(null," Umukozi  "," Ikibazo ",JOptionPane.ERROR_MESSAGE);


          }
return s1;
}

public void insertEmp(Employe c)
{
     try
    {

psInsert = conn.prepareStatement("insert into APP.employe  (ID_EMPLOYE,NOM_EMPLOYE,PRENOM_EMPLOYE ,BP_EMPLOYE " +
        ",CATRE_INDETITE , LEVEL_EMPLOYE,TITRE_EMPLOYE,PWD_EMPLOYE,TEL_EMPLOYE  ) values (?,?,?,?,?,?,?,?,? )");


psInsert.setInt(1,c.ID_EMPLOYE);
psInsert.setString(2,c.NOM_EMPLOYE);
psInsert.setString(3,c.PRENOM_EMPLOYE);
psInsert.setString(4,c.BP_EMPLOYE);
psInsert.setInt(5,c.CATRE_INDETITE);
psInsert.setInt(6,c.LEVEL_EMPLOYE);
psInsert.setString(7,c.TITRE_EMPLOYE);
psInsert.setString(8,c.PWD_EMPLOYE);
psInsert.setInt(9,c.TEL_EMPLOYE);
psInsert.executeUpdate();
    psInsert.close();
    JOptionPane.showMessageDialog(null," UMUKIRIYA ARINJIYE NTAKIBAZO "," MURAKOZE ",JOptionPane.PLAIN_MESSAGE);


          }  catch (Throwable e)  {

   JOptionPane.showMessageDialog(null," Umukiriya ashobora kuba arimo  "," DUPLICAGE ",JOptionPane.ERROR_MESSAGE);


          }
 }
public void upDateEmp(Employe c,int code )
 {
        try {

            Statement sU = conn.createStatement();
            String createString1 = " update Employe set NOM_EMPLOYE='" + c.NOM_EMPLOYE + "',PRENOM_EMPLOYE='" + c.PRENOM_EMPLOYE + "' ," +
                    "CATRE_INDETITE=" + c.CATRE_INDETITE + " ,LEVEL_EMPLOYE=" + c.LEVEL_EMPLOYE + " , BP_EMPLOYE='" + c.BP_EMPLOYE + "'," +
                    "TITRE_EMPLOYE='" + c.TITRE_EMPLOYE + "',PWD_EMPLOYE='" + c.PWD_EMPLOYE + "' ,TEL_EMPLOYE=" + c.TEL_EMPLOYE + " where ID_EMPLOYE=" + code;
            sU.execute(createString1);
            JOptionPane.showMessageDialog(null," UMUKIRIYA ARINJIYE NTAKIBAZO "," MURAKOZE ",JOptionPane.PLAIN_MESSAGE);

        } catch (SQLException ex) {

            JOptionPane.showMessageDialog(null,"  "+ex," UpDate Client Fail " ,JOptionPane.ERROR_MESSAGE);
        insertError(ex+"","update");     }


}
public void insertClient(Client c)
{
     try
    {
         String db = "";

         if(c.ASSURANCE.equals("RAMA"))
             db="_RAMA";
         if(c.ASSURANCE.equals("CASHREMISE"))
    {
         db="_UNIPHARMA";
         }



    psInsert = conn.prepareStatement("insert into APP.CLIENT"+db+" (NUM_AFFILIATION,NOM_CLIENT,PRENOM_CLIENT ,PERCENTAGE ,DATE_EXP , ASSURANCE,EMPLOYEUR,SECTEUR,CODE,AGE,SEXE ) values (?,?,?,?,?,?,?,?,?,?,?)");


psInsert.setString(1,c.NUM_AFFILIATION);
psInsert.setString(2,c.NOM_CLIENT);
psInsert.setString(3,c.PRENOM_CLIENT);
psInsert.setInt(4,c.PERCENTAGE);
psInsert.setInt(5,c.DATE_EXP);
psInsert.setString(6,c.ASSURANCE);
psInsert.setString(7,c.EMPLOYEUR);
psInsert.setString(8,c.SECTEUR);
psInsert.setString(9,c.CODE);
psInsert.setString(10,c.AGE);
psInsert.setString(11,c.SEXE);
psInsert.executeUpdate();
    psInsert.close();
    JOptionPane.showMessageDialog(null," UMUKIRIYA ARINJIYE NTAKIBAZO "," MURAKOZE ",JOptionPane.PLAIN_MESSAGE);


          }  catch (Throwable e)  {
              System.out.println(e);
   JOptionPane.showMessageDialog(null," Umukiriya ashobora kuba arimo  "," DUPLICAGE ",JOptionPane.ERROR_MESSAGE);


          }
 }
public void upDateClient(Client c,String db,String nAff )
 {
        try {
            Statement sU = conn.createStatement();
            String createString1 = " update APP." + db + " set NOM_CLIENT='" + c.NOM_CLIENT + "',PRENOM_CLIENT='" + c.PRENOM_CLIENT + "' ,PERCENTAGE=" + c.PERCENTAGE + " ,DATE_EXP=" + c.DATE_EXP + " , ASSURANCE='" + c.ASSURANCE + "',EMPLOYEUR='" + c.EMPLOYEUR + "',SECTEUR='" + c.SECTEUR + "' ,VISA='" + c.VISA + "'where CODE='" + nAff + "'";
            sU.execute(createString1);
            JOptionPane.showMessageDialog(null," UMUKIRIYA ARINJIYE NTAKIBAZO "," MURAKOZE ",JOptionPane.PLAIN_MESSAGE);

        } catch (SQLException ex) {

            JOptionPane.showMessageDialog(null,"  "+ex," UpDate Client Fail " ,JOptionPane.ERROR_MESSAGE);
        insertError(ex+"","update");     }


}
public void upDateClientIn( )
 {
        try {


            Statement sU = conn.createStatement();
            int i = 0;
             outResult = s.executeQuery("select * from APP.CLIENT_UNIPHARMA ");
     while (outResult.next())
    {
         i++;
          String NUM_AFFILIATIONa    =outResult.getString(1);
       String createString1 = " update APP.CLIENT_UNIPHARMA set CODE='"+ i+"' WHERE NUM_AFFILIATION='" + NUM_AFFILIATIONa + "'";
            sU.execute(createString1);
   }
              outResult = s.executeQuery("select * from APP.CLIENT ");
     while (outResult.next())
    {
         i++;
          String NUM_AFFILIATIONa    =outResult.getString(1);
       String createString1 = " update APP.CLIENT set CODE='"+  i+"' WHERE NUM_AFFILIATION='" + NUM_AFFILIATIONa + "'";
            sU.execute(createString1);
   }
                  outResult = s.executeQuery("select * from APP.CLIENT_RAMA ");
     while (outResult.next())
    {
         i++;
          String NUM_AFFILIATIONa    =outResult.getString(1);
       String createString1 = " update APP.CLIENT_RAMA set CODE='"+  i+"' WHERE NUM_AFFILIATION='" + NUM_AFFILIATIONa + "'";
            sU.execute(createString1);
   }





        } catch (SQLException ex) {
            Logger.getLogger(AutoOut.class.getName()).log(Level.SEVERE, null, ex);
        }


}


public void getList()
{
try
    {
        outResult1 = s.executeQuery("select ID_INVOICE,CODE_UNI,QUANTITE,ID_PRODUCT from APP.LIST,APP.PRODUCT  WHERE PRODUCT.CODE=LIST.CODE_UNI");

        int k = 0 ;

        LinkedList <Integer> inve= new LinkedList();
          LinkedList <String> codel=new LinkedList();
          LinkedList <Integer> qt=new LinkedList();
          LinkedList <Integer> i=new LinkedList();

        while (outResult1.next() )
        {

         int INV  =outResult1.getInt(1);
        String code   =  outResult1.getString(2);
        int quantity  =  outResult1.getInt(3);
        int ID  =  outResult1.getInt(4);
        boolean done=true;

        inve.add(INV);
        codel.add(code);
        qt.add(quantity);
        i.add(ID);


        }
        outResult1.close();


for(int y = 40;y<inve.size();y++)
{
    boolean done=true;
         outResult = s.executeQuery("select * from APP.LOT where  ID_PRODUCT="+i.get(y)+"order by date_exp,id_lot ");
              int quantity=qt.get(y);
              String code= codel.get(y);
              int INV = inve.get(y);
         while (outResult.next()&& quantity>0 )
                {
                            int productCode =outResult.getInt(1);
                            int id =outResult.getInt(2);
                            String  id_LotS=  outResult.getString(3);
                            String date1=outResult.getString(4);
                            int qtyStart=outResult.getInt(5);
                            int qtyLive=outResult.getInt(6);
                            String bon_livraison=  outResult.getString(7);
 System.out.println(codel.get(y)+"   "+qtyLive+"   "+id_LotS);
 done=false;


                                    if(qtyLive>quantity)
                                    {

                                    qtyLive=qtyLive-quantity;


                Statement  sL= conn.createStatement();
                String createString2 = " update APP.LIST set num_lot='"+id_LotS+"' where code_uni='"+code+"'and id_invoice="+INV;

                sL.execute(createString2);



                                    Statement  sU = conn.createStatement();
                                    String createString1 = " update APP.LOT set QTY_LIVE="+qtyLive+" where BON_LIVRAISON='"+bon_livraison+"'AND ID_LOT="+id+" and ID_LOTS='"+id_LotS+"' and ID_PRODUCT="+productCode+"";

                                    sU.execute(createString1);
                                    quantity=0;

                                    //   System.out.println(id_LotS+" if "+qtyLive+" qtty "+quantity);
                                    done=false;
                                    }
                 else if(qtyLive<quantity)
                                {
                                quantity=quantity-qtyLive;

Statement  sL= conn.createStatement();
                String createString2 = " update APP.LIST set num_lot='"+id_LotS+"' where code_uni='"+code+"'and id_invoice="+INV;

                sL.execute(createString2);
                                PreparedStatement  psInsert2 = conn.prepareStatement("insert into APP.USED_LOT(ID_PRODUCT,ID_LOT,ID_LOTS,DATE_EXP,QTY_START,QTY_LIVE,BON_LIVRAISON)  values (?,?,?,?,?,?,?)");

                                psInsert2.setInt(1,productCode);
                                psInsert2.setInt(2,id);
                                psInsert2.setString(3,id_LotS);
                                psInsert2.setString(4,date1);
                                psInsert2.setInt(5,qtyStart);
                                psInsert2.setInt(6,qtyLive);
                                psInsert2.setString(7,bon_livraison);
                                psInsert2.executeUpdate();
                                psInsert2.close();

                                Statement sD = conn.createStatement();
                                String createString1 = " delete from lot where ID_LOT="+id+" and BON_LIVRAISON='"+bon_livraison+"' and id_LotS= '"+id_LotS+"' and ID_PRODUCT= "+productCode;

                                sD.execute(createString1);

                                }
                    else if(qtyLive==quantity)
                            {

                            if(quantity!=0)
                            {Statement  sL= conn.createStatement();
                String createString2 = " update APP.LIST set num_lot='"+id_LotS+"' where code_uni='"+code+"'and id_invoice="+INV;

                sL.execute(createString2);
                            }

                            quantity=quantity-qtyLive; done=false;
                            PreparedStatement  psInsert2 = conn.prepareStatement("insert into APP.USED_LOT(ID_PRODUCT,ID_LOT,ID_LOTS,DATE_EXP,QTY_START,QTY_LIVE,BON_LIVRAISON)  values (?,?,?,?,?,?,?)");

                            psInsert2.setInt(1,productCode);
                            psInsert2.setInt(2,id);
                            psInsert2.setString(3,id_LotS);
                            psInsert2.setString(4,date1);
                            psInsert2.setInt(5,qtyStart);
                            psInsert2.setInt(6,qtyLive);
                            psInsert2.setString(7,bon_livraison);
                            psInsert2.executeUpdate();
                            psInsert2.close();

                            Statement sD = conn.createStatement();
                            String createString1 = " delete from lot where ID_LOT="+id+" and BON_LIVRAISON='"+bon_livraison+"' and id_LotS= '"+id_LotS+"' and ID_PRODUCT= "+productCode;
                            sD.execute(createString1);

                            }
                     }
         if(done)
          System.out.println(codel.get(y));


        //  Close the resultSet
        outResult.close();
          }

}

       catch (Throwable e)  {
   insertError(e+"","getList");
    }

}

public Client getClient(String entier,String table)
{
 Client c =null;
 //JOptionPane.showMessageDialog(null,entier,table,JOptionPane.PLAIN_MESSAGE);


      try
    {
         if(table.equals("UNI"))
         {
        outResult = s.executeQuery("select * from APP.CLIENT_UNIPHARMA where CODE ='"+entier+"'");
     while (outResult.next())
    {
        String NUM_AFFILIATIONa    =outResult.getString(1);
        String NOM_CLIENTa         =  outResult.getString(2);
        String PRENOM_CLIENTa      =  outResult.getString(3);
        int PERCENTAGEa  =outResult.getInt(4);
        int DATE_EXPa  =outResult.getInt(5);
        String  ASSURANCEa  =  outResult.getString(6);
        String  EMPLOYEURa  =  outResult.getString(7);
         String  SECTEURa  =  outResult.getString(8);
          String  VISAa  =  outResult.getString(9);
          String  CODE  =  outResult.getString(10);
  c =new Client(NUM_AFFILIATIONa,NOM_CLIENTa ,PRENOM_CLIENTa ,PERCENTAGEa,DATE_EXPa,ASSURANCEa ,EMPLOYEURa,SECTEURa,VISAa,CODE );
   }
    }

    else
               {
    outResult = s.executeQuery("select * from APP.CLIENT"+table+" where CODE ='"+entier+"'");
     while (outResult.next())
    {
        String NUM_AFFILIATIONa    =outResult.getString(1);
        String NOM_CLIENTa         =  outResult.getString(2);
        String PRENOM_CLIENTa      =  outResult.getString(3);
        int PERCENTAGEa  =outResult.getInt(4);
        int DATE_EXPa  =outResult.getInt(5);
        String  ASSURANCEa  =  outResult.getString(6);
        String  EMPLOYEURa  =  outResult.getString(7);
         String  SECTEURa  =  outResult.getString(8);
          String  VISAa  =  outResult.getString(9);
      String  CODE  =  outResult.getString(10);
  c =new Client(NUM_AFFILIATIONa,NOM_CLIENTa ,PRENOM_CLIENTa ,PERCENTAGEa,DATE_EXPa,ASSURANCEa ,EMPLOYEURa,SECTEURa,VISAa,CODE );
     //JOptionPane.showMessageDialog(null,c," autre ",JOptionPane.PLAIN_MESSAGE);


     }
     }



        //  Close the resultSet
    outResult.close();
      }  catch (Throwable e)  {
  insertError(e+"","getclint");
    }

return  c;



}

public Client getClientA(String entier,String table)
{
 Client c =null;
 //JOptionPane.showMessageDialog(null,entier,table,JOptionPane.PLAIN_MESSAGE);


      try
    {
         if(table.equals("UNI"))
         {
        outResult = s.executeQuery("select * from APP.CLIENT_UNIPHARMA where NUM_AFFILIATION ='"+entier+"'");
     while (outResult.next())
    {
        String NUM_AFFILIATIONa    =outResult.getString(1);
        String NOM_CLIENTa         =  outResult.getString(2);
        String PRENOM_CLIENTa      =  outResult.getString(3);
        int PERCENTAGEa  =outResult.getInt(4);
        int DATE_EXPa  =outResult.getInt(5);
        String  ASSURANCEa  =  outResult.getString(6);
        String  EMPLOYEURa  =  outResult.getString(7);
         String  SECTEURa  =  outResult.getString(8);
          String  VISAa  =  outResult.getString(9);
          String  CODE  =  outResult.getString(10);
  c =new Client(NUM_AFFILIATIONa,NOM_CLIENTa ,PRENOM_CLIENTa ,PERCENTAGEa,DATE_EXPa,ASSURANCEa ,EMPLOYEURa,SECTEURa,VISAa,CODE );
   }
    }

    else
               {
    outResult = s.executeQuery("select * from APP.CLIENT"+table+" where NUM_AFFILIATION ='"+entier+"'");
     while (outResult.next())
    {
        String NUM_AFFILIATIONa    =outResult.getString(1);
        String NOM_CLIENTa         =  outResult.getString(2);
        String PRENOM_CLIENTa      =  outResult.getString(3);
        int PERCENTAGEa  =outResult.getInt(4);
        int DATE_EXPa  =outResult.getInt(5);
        String  ASSURANCEa  =  outResult.getString(6);
        String  EMPLOYEURa  =  outResult.getString(7);
         String  SECTEURa  =  outResult.getString(8);
          String  VISAa  =  outResult.getString(9);
      String  CODE  =  outResult.getString(10);
  c =new Client(NUM_AFFILIATIONa,NOM_CLIENTa ,PRENOM_CLIENTa ,PERCENTAGEa,DATE_EXPa,ASSURANCEa ,EMPLOYEURa,SECTEURa,VISAa,CODE );
     //JOptionPane.showMessageDialog(null,c," autre ",JOptionPane.PLAIN_MESSAGE);


     }
     }



        //  Close the resultSet
    outResult.close();
      }  catch (Throwable e)  {
  insertError(e+"","getclint");
    }

return  c;



}
public LinkedList <Client> getOldClient(String table)
{
 Client c =null;
    LinkedList <Client> client = new LinkedList();
      try
    {

    outResult = s.executeQuery(table);
     while (outResult.next())
    {
        String NUM_AFFILIATIONa    =outResult.getString(1);
        String NOM_CLIENTa         =  outResult.getString(2);
        String PRENOM_CLIENTa      =  outResult.getString(3);
        int PERCENTAGEa  =outResult.getInt(4);
        int DATE_EXPa  =outResult.getInt(5);
        String  ASSURANCEa  =  outResult.getString(6);
        String  EMPLOYEURa  =  outResult.getString(7);
        String  SECTEURa  =  outResult.getString(8);
        String  VISAa  =  outResult.getString(9);
        String  CODE  =  outResult.getString(10);
  c =new Client(NUM_AFFILIATIONa,NOM_CLIENTa ,PRENOM_CLIENTa ,PERCENTAGEa,DATE_EXPa,ASSURANCEa ,EMPLOYEURa,SECTEURa,VISAa,CODE );
     //JOptionPane.showMessageDialog(null,c," autre ",JOptionPane.PLAIN_MESSAGE);
     client.add(c);


     }
        //  Close the resultSet
    outResult.close();
      }  catch (Throwable e)  {
    insertError(e+"","getOldClient");
    }

return  client;



}
 public void insertLogin(String id,String in_out,int frw)
{

    try
    {

       psInsert = conn.prepareStatement("insert into APP.LOGIN (ID_EMPLOYE,IN_OUT,ARGENT )  values (?,?,?)");


         psInsert.setString(1,id);
         psInsert.setString(2,in_out);
         psInsert.setInt(3,frw);
         psInsert.executeUpdate();
          psInsert.close();
          }  catch (Throwable e)  {
  insertError(e+"","insertLogin");
    }}
 void addOneNew(Product p)
{
    try
    {

    psInsert = conn.prepareStatement("insert into PRODUCT (id_product,name_product,code,prix,prix_SOCIETE, tva,code_bar,observation )  values (?,?,?,?,?,?,?,?)");



    psInsert.setInt(1,p.productCode);
    psInsert.setString(2,p.productName);
    psInsert.setString (3,p.code );
    psInsert.setDouble(4,p.currentPrice);
    psInsert.setDouble(5,(p.currentPrice)*0.85);
    psInsert.setDouble(6,p.tva);
     psInsert.setString (7,p.code );
      psInsert.setString (8,p.observation );

    psInsert.executeUpdate();

    // Release the resources (clean up )
    psInsert.close();
    }  catch (Throwable e)  { insertError(e+"","addonenew");}


}

   LinkedList <String>getFichier(String fichier)throws FileNotFoundException, IOException
{
    LinkedList<String> give= new LinkedList();

        File productFile= new File(fichier);
        String  ligne =null;
        BufferedReader entree=new BufferedReader(new FileReader(productFile));
        try{ ligne=entree.readLine(); }
        catch (IOException e){ System.out.println(e);}
        int i=0;
        while(ligne!=null)
        {
        // System.out.println(i+ligne);
        give.add(ligne);
        i++;

        try{ ligne =entree.readLine();}
        catch (IOException e){  insertError(e+"","getFichier");}
        }
        entree.close();
return give;
}

   public int getMaxCodeMachine()
{
int id =0;
try
{
outResult = s.executeQuery("select max(ID_PRODUCT) from APP.PRODUCT ");
while (outResult.next())
{
id =outResult.getInt(1);
}
 outResult.close();
      }  catch (Throwable e)  {
    insertError(e+"","getmaxcodemachine");
    }
return id;

}
public int getIdFacture(String id_client,String date,int  tot, String id_employe,int tva)
{
int id =0;
String id_clientdb="";
String datedb="";
int totdb=0;
String id_employedb="";
int tvadb=0;
String heure="";
boolean done=true;int compteur=0;
try
{
outResult = s.executeQuery("select  max(ID_INVOICE) from APP.INVOICE ");
while (outResult.next())
{
id =outResult.getInt(1);
}

while(done ||compteur>5)
{

compteur++;

outResult = s.executeQuery("select  * from APP.INVOICE where ID_INVOICE= "+id);
while (outResult.next())
{

    id_clientdb=outResult.getString(2);
    datedb=outResult.getString(3);
    totdb=outResult.getInt(4);
    id_employedb=outResult.getString(5);
    heure=""+outResult.getTimestamp(6);
    tvadb=outResult.getInt(7);

}
if(id_clientdb.equals(id_client)&& datedb.equals(date)&& totdb==tot && id_employedb.equals(id_employe)&&tvadb==tva)
{
    done=false;

//JOptionPane.showMessageDialog(null,compteur,"ok "+id,JOptionPane.PLAIN_MESSAGE);


}
else
id=id-1;

}
 outResult.close();
      }  catch (Throwable e)  {
    insertError(e+"","getIdfacture");
    }
 this.controlInvoice = new LinkedList();
       controlInvoice.add("  FA"+id);
        controlInvoice.add(heure);
 if(compteur==5)
  insertError(id+" "+tot+""+date+""+id_client," manque idfacture");
return id;

}
 int doFinishVente(Invoice invoicePrint)
    {
    int fact=0;
    try
    {


        psInsert = conn.prepareStatement("insert into APP.INVOICE (NUM_CLIENT,DATE ,TOTAL,EMPLOYE,TVA )  values (?,?,?,?,?)");

            psInsert.setString(1,invoicePrint.id_client);
            psInsert.setString(2,invoicePrint.date);
            psInsert.setInt(3,invoicePrint.tot);
            psInsert.setString(4,invoicePrint.id_employe);
            psInsert.setInt(5,invoicePrint.tva);
         psInsert.executeUpdate();
         psInsert.close();


 fact= getIdFacture( invoicePrint.id_client, invoicePrint.date, invoicePrint.tot,  invoicePrint.id_employe, invoicePrint.tva );

           psInsert1 = conn.prepareStatement("insert into APP.LIST (ID_INVOICE,CODE_UNI,NUM_LOT,QUANTITE,PRICE,prix_revient )  values (?,?,?,?,?,?)");
            LinkedList<Product> productPrint = invoicePrint.getProductListDB();
            for(int j= 0;j<productPrint.size();j++) // ecrire les produits
            {


                    Product p =productPrint.get(j);
                  int quantity =p.qty();
                     boolean done=true;


         outResult = s.executeQuery("select * from APP.LOT where  ID_PRODUCT="+p.productCode+" order by date_exp,id_lot ");
         while (outResult.next()&& done)
        {
                    int productCode =outResult.getInt(1);
                    int id =outResult.getInt(2);
                    String  id_LotS=  outResult.getString(3);
                    String date1=outResult.getString(4);
                    int qtyStart=outResult.getInt(5);
                    int qtyLive=outResult.getInt(6);
                    String bon_livraison=  outResult.getString(7);

                 if(qtyLive>quantity)
                 {

                qtyLive=qtyLive-quantity;



                    psInsert1.setInt(1,fact);
                    psInsert1.setString(2,p.code);
                    psInsert1.setString(3,id_LotS);
                    psInsert1.setInt(4,quantity);
                    psInsert1.setDouble(5,p.currentPrice);
                    psInsert1.setDouble(6,p.prix_revient);
                    psInsert1.executeUpdate();

                Statement  sU = conn.createStatement();
    String createString1 = " update APP.LOT set QTY_LIVE="+qtyLive+" where BON_LIVRAISON='"+bon_livraison+"' AND ID_LOT="+id+" and ID_LOTS='"+id_LotS+"' and ID_PRODUCT="+p.productCode+"";

                sU.execute(createString1);
                quantity=0;

             //   System.out.println(id_LotS+" if "+qtyLive+" qtty "+quantity);
                   done=false;
                 }
                 else if(qtyLive<quantity)
                 {
                     quantity=quantity-qtyLive;


                 //    System.out.println(id_LotS+" else "+qtyLive+" qtty "+quantity);


                    psInsert1.setInt(1,fact);
                    psInsert1.setString(2,p.code);
                    psInsert1.setString(3,id_LotS);
                    psInsert1.setInt(4,qtyLive);
                    psInsert1.setDouble(5,p.currentPrice);
                    psInsert1.setDouble(6,p.prix_revient);
                    psInsert1.executeUpdate();

           PreparedStatement  psInsert2 = conn.prepareStatement("insert into APP.USED_LOT(ID_PRODUCT,ID_LOT,ID_LOTS,DATE_EXP,QTY_START,QTY_LIVE,BON_LIVRAISON)  values (?,?,?,?,?,?,?)");

            psInsert2.setInt(1,productCode);
            psInsert2.setInt(2,id);
            psInsert2.setString(3,id_LotS);
            psInsert2.setString(4,date1);
            psInsert2.setInt(5,qtyStart);
            psInsert2.setInt(6,qtyLive);
            psInsert2.setString(7,bon_livraison);
            psInsert2.executeUpdate();
            psInsert2.close();

         Statement sD = conn.createStatement();
           String createString1 = " delete from lot where ID_LOT="+id+" and BON_LIVRAISON='"+bon_livraison+"' and id_LotS= '"+id_LotS+"' and ID_PRODUCT= "+productCode;

        sD.execute(createString1);

                 }
                    else if(qtyLive==quantity)
                 {



                   //  System.out.println(id_LotS+" = "+qtyLive+" qtty "+quantity);

                     if(quantity!=0)
                     {psInsert1.setInt(1,fact);
                    psInsert1.setString(2,p.code);
                    psInsert1.setString(3,id_LotS);
                    psInsert1.setInt(4,qtyLive);
                    psInsert1.setDouble(5,p.currentPrice);
                    psInsert1.setDouble(6,p.prix_revient);
                    psInsert1.executeUpdate();
                     }

                          quantity=quantity-qtyLive; done=false;
           PreparedStatement  psInsert2 = conn.prepareStatement("insert into APP.USED_LOT(ID_PRODUCT,ID_LOT,ID_LOTS,DATE_EXP,QTY_START,QTY_LIVE,BON_LIVRAISON)  values (?,?,?,?,?,?,?)");

            psInsert2.setInt(1,productCode);
            psInsert2.setInt(2,id);
            psInsert2.setString(3,id_LotS);
            psInsert2.setString(4,date1);
            psInsert2.setInt(5,qtyStart);
            psInsert2.setInt(6,qtyLive);
            psInsert2.setString(7,bon_livraison);
            psInsert2.executeUpdate();
            psInsert2.close();

         Statement sD = conn.createStatement();
      String createString1 = " delete from lot where ID_LOT="+id+" and BON_LIVRAISON='"+bon_livraison+"' and id_LotS= '"+id_LotS+"' and ID_PRODUCT= "+productCode;
   sD.execute(createString1);

                 }
                     }

        //  Close the resultSet
        outResult.close();




            }

         psInsert1.close();

                    // Release the resources (clean up )



    //  Beginning of the primary catch block: uses errorPrint method
        }  catch (Throwable e)  {
         insertError(e+"","doFinishVente");
        }

  return fact;
}
   public void doFinishCredit(Credit c)
{

try
{

    psInsert = conn.prepareStatement("insert into APP.CREDIT (ID_INVOICE,NUMERO_AFFILIE ,NUMERO_QUITANCE,SOLDE,PAYED,CAUSE )  values (?,?,?,?,?,?)");


        psInsert.setInt(1,c.ID_INVOICE);
        psInsert.setString(2,c.NUMERO_AFFILIE);
        psInsert.setString(3,c.NUMERO_QUITANCE);
        psInsert.setInt(4,c.SOLDE);
        psInsert.setInt(5,c.PAYED);
        psInsert.setString(6,c.RAISON);

        psInsert.executeUpdate();
          //JOptionPane.showMessageDialog(null,c.NUMERO_AFFILIE,"rama",JOptionPane.PLAIN_MESSAGE);



    		// Release the resources (clean up )
    psInsert.close();


//  Beginning of the primary catch block: uses errorPrint method
    }  catch (Throwable e)  {
 insertError(e+"","doFinishCredit") ;
    }

    }
   String getString(String s)
{
    String ret=" ";
    Variable var=  getParam("RUN","MAIN",s+"MAIN");
    if(var!=null)
    ret=var.value2;
    return ret;
}
   public LinkedList<String>  printRetard(int id_invoice)
{
    LinkedList<String> printR =new LinkedList<String>();
String printStringUp=getString("bp")+ "\n  "+ getString("tel")+ "\n  "+ getString("tva");
int totale=0;
boolean done=false;
String printStringE="";
String printStringW="";

        String client="";
        String date1="";
        String code=  "";
        String lot1 ="";
        int quantite =0;
        double price =0;
        String desi ="";
        String   num_affiliation="";
        String  nom_client="";
        String  prenom_client="";
        int  percentage=0;
        String   numero_quitance  ="";

try
    {
            // Clients Cash
    outResult = s.executeQuery("" +
            "select app.invoice.id_invoice,num_client,date,code_uni,num_lot,quantite" +
            ",list.price,name_product from APP.PRODUCT,APP.INVOICE ,APP.LIST where " +
            "APP.INVOICE.NUM_CLIENT='CASHNORM' " +
            "and APP.INVOICE.id_invoice = "+id_invoice+"  " +
            "and APP.INVOICE.id_invoice = APP.LIST.id_invoice " +
            "and  APP.PRODUCT.CODE=APP.LIST.CODE_UNI order" +
            " by  app.invoice.id_invoice");

    while (outResult.next())
    {

         client=outResult.getString(2);
         date1=outResult.getString(3);
         code=  outResult.getString(4);
         lot1 =outResult.getString(5);
         quantite =outResult.getInt(6);
         price =outResult.getDouble(7);
         desi =outResult.getString(8);
 printStringE=printStringE+ "\n  "+quantite+ "  "+desi;
 printStringW=printStringW+ "\n "+(int)(quantite*price);
      totale=totale+(int)(quantite*price);
             done=true;
     }
      outResult.close();
       // Clients Cash  Unipharma
     outResult = s.executeQuery("" +
             "select app.invoice.id_invoice,num_client,date,code_uni,num_lot," +
             "quantite,list.price,name_product,num_affiliation,nom_client," +
             "prenom_client,percentage,numero_quitance " +
             "from APP.PRODUCT,APP.INVOICE ,APP.LIST,APP.CREDIT,APP.CLIENT_UNIPHARMA " +
             "where APP.INVOICE.NUM_CLIENT ='CASHREMISE'  " +
             "and APP.credit.numero_affilie =  APP.client_UNIPHARMA.num_affiliation " +
             "and APP.INVOICE.id_invoice = "+id_invoice+" " +
             "and APP.INVOICE.id_invoice = APP.credit.id_invoice " +
             "and APP.INVOICE.id_invoice = APP.LIST.id_invoice " +
             "AND APP.PRODUCT.CODE=APP.LIST.CODE_UNI " +
             "order by app.invoice.id_invoice");

    while (outResult.next())
    {
         client=outResult.getString(2);
         date1=outResult.getString(3);
         code=  outResult.getString(4);
         lot1 =outResult.getString(5);
         quantite =outResult.getInt(6);
        price =outResult.getDouble(7);
         desi =outResult.getString(8);
           num_affiliation=outResult.getString(9);
          nom_client=outResult.getString(10);
          prenom_client=outResult.getString(11);
          percentage=outResult.getInt(12);
           numero_quitance  =outResult.getString(13);
printStringE=printStringE+ "\n  "+quantite+ "  "+desi;
 printStringW=printStringW+ "\n "+(int)(quantite*price);
      totale=totale+(int)(quantite*price);


     }

    //  Close the resultSet  and date ='"+d+"'
    outResult.close();

      // Clients Rama

     outResult = s.executeQuery("" +
             "select app.invoice.id_invoice,num_client,date,code_uni,num_lot,quantite,list.price," +
             "name_product,num_affiliation,nom_client,prenom_client,percentage,numero_quitance " +
             "from APP.PRODUCT,APP.INVOICE ,APP.LIST,APP.CREDIT,APP.CLIENT_RAMA " +
             "where APP.INVOICE.NUM_CLIENT ='RAMA'" +
             "and APP.client_RAMA.num_affiliation = APP.credit.numero_affilie " +
             "and APP.INVOICE.id_invoice = "+id_invoice+" " +
             "and APP.INVOICE.id_invoice = APP.credit.id_invoice " +
             "and APP.INVOICE.id_invoice = APP.LIST.id_invoice  " +
             "and APP.PRODUCT.CODE=APP.LIST.CODE_UNI " +
             "order by app.invoice.id_invoice");

    while (outResult.next())
    {
         client=outResult.getString(2);
         date1=outResult.getString(3);
         code=  outResult.getString(4);
         lot1 =outResult.getString(5);
         quantite =outResult.getInt(6);
          price =outResult.getDouble(7);
         desi =outResult.getString(8);
           num_affiliation=outResult.getString(9);
          nom_client=outResult.getString(10);
          prenom_client=outResult.getString(11);
          percentage=outResult.getInt(12);
           numero_quitance  =outResult.getString(13);
printStringE=printStringE+ "\n  "+quantite+ "  "+desi;
 printStringW=printStringW+ "\n "+(int)(quantite*price);
       totale=totale+(int)(quantite*price);

     }

    //  Close the resultSet  and date ='"+d+"'
    outResult.close();


     // Clients Societe

     outResult = s.executeQuery("" +
             "select app.invoice.id_invoice,num_client,date,code_uni,num_lot,quantite,list.price,name_product" +
             ",num_affiliation,nom_client,prenom_client,percentage,numero_quitance " +
             "from APP.PRODUCT,APP.INVOICE ,APP.LIST,APP.CREDIT,APP.CLIENT " +
             "where  APP.INVOICE.NUM_CLIENT !='CASHNORM'" +
             "and APP.INVOICE.NUM_CLIENT !='CASHREMISE'" +
             "and APP.INVOICE.NUM_CLIENT !='RAMA'" +
             "and APP.client.num_affiliation = APP.credit.numero_affilie " +
             "and APP.INVOICE.id_invoice = "+id_invoice+
             " and APP.INVOICE.id_invoice = APP.credit.id_invoice " +
             "and APP.INVOICE.id_invoice = APP.LIST.id_invoice  " +
             "AND APP.PRODUCT.CODE=APP.LIST.CODE_UNI" +
             " order by app.invoice.id_invoice");

    while (outResult.next())
    {

         client=outResult.getString(2);
         date1=outResult.getString(3);
         code=  outResult.getString(4);
         lot1 =outResult.getString(5);
         quantite =outResult.getInt(6);
         price =outResult.getDouble(7);
         desi =outResult.getString(8);
           num_affiliation=outResult.getString(9);
        nom_client=outResult.getString(10);
        prenom_client=outResult.getString(11);
          percentage=outResult.getInt(12);
         numero_quitance  =outResult.getString(13);
printStringE=printStringE+ "\n  "+quantite+ "  "+desi;
 printStringW=printStringW+ "\n "+(int)(quantite*price);
    totale=totale+(int)(quantite*price);



     }
     if(done)
   {
          printStringUp=printStringUp+ "\n    FACTURE N°    : " +id_invoice;
     printStringE=printStringE+ "\n  ";
printStringW=printStringW+ "\n   ";

printStringE=printStringE+ "\n  TOTAL en RWF: ";
printStringW=printStringW+ "\n"+(int)totale;

printStringE=printStringE+ "\n  Tva 18% inclus ";
printStringW=printStringW+ "\n ========= ";



         printStringE=printStringE+ "\n           Murakoze ! "  ;
           printStringW=printStringW+ "\n ";
           printStringE=printStringE+ "\n              "  ;
      printStringW=printStringW+ "\n ";

     }

     else
         {


   printStringUp=printStringUp+ "\n    FACTURE N°    : " +id_invoice;
        printStringUp=printStringUp+ "\n    " +date1 ;
        printStringUp=printStringUp+"\n    -------------------------------------------   ";
        printStringUp=printStringUp+"\n    Assureur            : "+client;
        printStringUp=printStringUp+ "\n    N° Affiliation      : "+num_affiliation;
        printStringUp=printStringUp+ "\n    Nom et Prenom : "+nom_client+ " "+prenom_client;
     //   printStringUp=printStringUp+ "\n    Departement     : "+c.EMPLOYEUR;
        printStringUp=printStringUp+ "\n    N° Quittance     : "+numero_quitance;
        printStringUp=printStringUp+"\n    -------------------------------------------   ";





         printStringE=printStringE+ "\n  ";
printStringW=printStringW+ "\n   ";

printStringE=printStringE+ "\n  TOTAL en RWF: ";
printStringW=printStringW+ "\n"+totale;

 printStringE=printStringE+ "\n  "+client+" "+(100-percentage)+" % : ";
printStringW=printStringW+ "\n"+(totale- (int)(totale*percentage*0.01));
 printStringE=printStringE+ "\n  ADHERENT  "+percentage+" % : ";
printStringW=printStringW+ "\n"+(int)(totale*percentage*0.01);
         printStringE=printStringE+ "\n           Murakoze ! "  ;
           printStringW=printStringW+ "\n ";
           printStringE=printStringE+ "\n              "  ;
      printStringW=printStringW+ "\n ";

     }
    //  Close the resultSet  and date ='"+d+"'
    outResult.close();
 printR.add(printStringUp);
  printR.add(printStringE);
   printR.add(printStringW);
   //JOptionPane.showMessageDialog(null,"BL VENTES SANS DOWNLOADED PROBLEMES","MURAKOZE",JOptionPane.PLAIN_MESSAGE);

      }  catch (Throwable e)  {
   insertError(e+"","printRetard");
    }
  return printR;
}

public void  annule(int id_invoice,String nom)
{
    LinkedList <List> todelete=new LinkedList();

String printStringUp="    REBA NEZA KO IYI FACTURE ARI IYO GUSIBA ";
int totale=0;
boolean done=false;
boolean done1=false;
String printStringE="";
String printStringW="";

        String client="";
        String date1="";
        String code=  "";
        String lot1 ="";
        int quantite =0;
        double price =0;
        String desi ="";
        String   num_affiliation="";
        String  nom_client="";
        String  prenom_client="";
        int  percentage=0;
        String   numero_quitance  ="";

try
    {
            // Clients Cash
    outResult = s.executeQuery("" +
            "select app.invoice.id_invoice,num_client,date,code_uni,num_lot,quantite" +
            ",list.price,name_product from APP.PRODUCT,APP.INVOICE ,APP.LIST where " +
            "APP.INVOICE.NUM_CLIENT='CASHNORM' " +
            "and APP.INVOICE.id_invoice = "+id_invoice+"  " +
            "and APP.INVOICE.id_invoice = APP.LIST.id_invoice " +
            "and  APP.PRODUCT.CODE=APP.LIST.CODE_UNI order" +
            " by  app.invoice.id_invoice");

    while (outResult.next())
    {

         client=outResult.getString(2);
         date1=outResult.getString(3);
         code=  outResult.getString(4);
         lot1 =outResult.getString(5);
         quantite =outResult.getInt(6);
         price =outResult.getDouble(7);
         desi =outResult.getString(8);
 printStringE=printStringE+ "\n  "+quantite+ "  "+desi+ "    "+(int)(quantite*price);
printStringW=printStringW+"("+code+"-"+quantite+")";
todelete.add(new List(id_invoice,price,quantite,lot1,code));

      totale=totale+(int)(quantite*price);
             done=true;
     }
      outResult.close();
       // Clients Cash  Unipharma
     outResult = s.executeQuery("" +
             "select app.invoice.id_invoice,num_client,date,code_uni,num_lot," +
             "quantite,list.price,name_product,num_affiliation,nom_client," +
             "prenom_client,percentage,numero_quitance " +
             "from APP.PRODUCT,APP.INVOICE ,APP.LIST,APP.CREDIT,APP.CLIENT_UNIPHARMA " +
             "where APP.INVOICE.NUM_CLIENT ='CASHREMISE'  " +
             "and APP.credit.numero_affilie =  APP.client_UNIPHARMA.num_affiliation " +
             "and APP.INVOICE.id_invoice = "+id_invoice+" " +
             "and APP.INVOICE.id_invoice = APP.credit.id_invoice " +
             "and APP.INVOICE.id_invoice = APP.LIST.id_invoice " +
             "AND APP.PRODUCT.CODE=APP.LIST.CODE_UNI " +
             "order by app.invoice.id_invoice");

    while (outResult.next())
    {
         client=outResult.getString(2);
         date1=outResult.getString(3);
         code=  outResult.getString(4);
         lot1 =outResult.getString(5);
         quantite =outResult.getInt(6);
        price =outResult.getDouble(7);
        desi =outResult.getString(8);
         num_affiliation=outResult.getString(9);
          nom_client=outResult.getString(10);
          prenom_client=outResult.getString(11);
          percentage=outResult.getInt(12);
           numero_quitance  =outResult.getString(13);
printStringE=printStringE+ "\n  "+quantite+ "  "+desi+ "    "+(int)(quantite*price);
printStringW=printStringW+"("+code+"-"+quantite+")";
todelete.add(new List(id_invoice,price,quantite,lot1,code));
      totale=totale+(int)(quantite*price);
         done1=true;

     }

    //  Close the resultSet  and date ='"+d+"'
    outResult.close();

      // Clients Rama

     outResult = s.executeQuery("" +
             "select app.invoice.id_invoice,num_client,date,code_uni,num_lot,quantite,list.price," +
             "name_product,num_affiliation,nom_client,prenom_client,percentage,numero_quitance " +
             "from APP.PRODUCT,APP.INVOICE ,APP.LIST,APP.CREDIT,APP.CLIENT_RAMA " +
             "where APP.INVOICE.NUM_CLIENT ='RAMA'" +
             "and APP.client_RAMA.num_affiliation = APP.credit.numero_affilie " +
             "and APP.INVOICE.id_invoice = "+id_invoice+" " +
             "and APP.INVOICE.id_invoice = APP.credit.id_invoice " +
             "and APP.INVOICE.id_invoice = APP.LIST.id_invoice  " +
             "and APP.PRODUCT.CODE=APP.LIST.CODE_UNI " +
             "order by app.invoice.id_invoice");

    while (outResult.next())
    {
         client=outResult.getString(2);
         date1=outResult.getString(3);
         code=  outResult.getString(4);
         lot1 =outResult.getString(5);
         quantite =outResult.getInt(6);
          price =outResult.getDouble(7);
         desi =outResult.getString(8);
           num_affiliation=outResult.getString(9);
          nom_client=outResult.getString(10);
          prenom_client=outResult.getString(11);
          percentage=outResult.getInt(12);
           numero_quitance  =outResult.getString(13);
printStringE=printStringE+ "\n  "+quantite+ "  "+desi+ "    "+(int)(quantite*price);
printStringW=printStringW+"("+code+"-"+quantite+")";
todelete.add(new List(id_invoice,price,quantite,lot1,code));
       totale=totale+(int)(quantite*price);
              done1=true;
     }

    //  Close the resultSet  and date ='"+d+"'
    outResult.close();


     // Clients Societe

     outResult = s.executeQuery("" +
             "select app.invoice.id_invoice,num_client,date,code_uni,num_lot,quantite,list.price,name_product" +
             ",num_affiliation,nom_client,prenom_client,percentage,numero_quitance " +
             "from APP.PRODUCT,APP.INVOICE ,APP.LIST,APP.CREDIT,APP.CLIENT " +
             "where  APP.INVOICE.NUM_CLIENT !='CASHNORM'" +
             "and APP.INVOICE.NUM_CLIENT !='CASHREMISE'" +
             "and APP.INVOICE.NUM_CLIENT !='RAMA'" +
             "and APP.client.num_affiliation = APP.credit.numero_affilie " +
             "and APP.INVOICE.id_invoice = "+id_invoice+
             " and APP.INVOICE.id_invoice = APP.credit.id_invoice " +
             "and APP.INVOICE.id_invoice = APP.LIST.id_invoice  " +
             "AND APP.PRODUCT.CODE=APP.LIST.CODE_UNI" +
             " order by app.invoice.id_invoice");

    while (outResult.next())
    {

         client=outResult.getString(2);
         date1=outResult.getString(3);
         code=  outResult.getString(4);
         lot1 =outResult.getString(5);
         quantite =outResult.getInt(6);
         price =outResult.getDouble(7);
         desi =outResult.getString(8);
           num_affiliation=outResult.getString(9);
        nom_client=outResult.getString(10);
        prenom_client=outResult.getString(11);
          percentage=outResult.getInt(12);
         numero_quitance  =outResult.getString(13);
printStringE=printStringE+ "\n  "+quantite+ "  "+desi+ "    "+(int)(quantite*price);
printStringW=printStringW+"("+code+"-"+quantite+")";
todelete.add(new List(id_invoice,price,quantite,lot1,code));
    totale=totale+(int)(quantite*price);
        done1=true;


     }
     if(done)
   {
          printStringUp=printStringUp+ "\n    FACTURE N°    : " +id_invoice;
     printStringE=printStringE+ "\n  ";


printStringE=printStringE+ "\n  TOTAL en RWF: "+(int)totale;


     }

     else
         {


   printStringUp=printStringUp+ "\n    FACTURE N°    : " +id_invoice;
        printStringUp=printStringUp+ "\n    " +date1 ;
        printStringUp=printStringUp+"\n    -------------------------------------------   ";
        printStringUp=printStringUp+"\n    Assureur            : "+client;
        printStringUp=printStringUp+ "\n    N° Affiliation      : "+num_affiliation;
        printStringUp=printStringUp+ "\n    Nom et Prenom : "+nom_client+ " "+prenom_client;
     //   printStringUp=printStringUp+ "\n    Departement     : "+c.EMPLOYEUR;
        printStringUp=printStringUp+ "\n    N° Quittance     : "+numero_quitance;
        printStringUp=printStringUp+"\n    -------------------------------------------   ";





         printStringE=printStringE+ "\n  ";


printStringE=printStringE+ "\n  TOTAL en RWF: "+totale;


 printStringE=printStringE+ "\n  "+client+" "+(100-percentage)+" % : "+(totale- (int)(totale*percentage*0.01));

 printStringE=printStringE+ "\n  ADHERENT  "+percentage+" % : "+(int)(totale*percentage*0.01);



     }
    //  Close the resultSet  and date ='"+d+"'
    outResult.close();
    if(done ||done1)
    {


           int n = JOptionPane.showConfirmDialog(null,
                        printStringUp+
                        "\n"+printStringE,
                      "GUSIBA FACTURE ????? ",JOptionPane.YES_NO_OPTION);
                if(n==0)
                {

           Annule deleted=new Annule(id_invoice,num_affiliation,nom,client,printStringW,date1);

      if(retourMse(todelete))
      {
          insertDeleted(deleted);
          deleteInvoice(id_invoice);
         JOptionPane.showMessageDialog(null,"FACTURE N° "+id_invoice+" ANNULEE","",JOptionPane.PLAIN_MESSAGE);
       }
      else
       JOptionPane.showMessageDialog(null,"kwanira FACTURE N° "+id_invoice+"  biranze ","Ikibazo",JOptionPane.PLAIN_MESSAGE);

    }


                }else
    JOptionPane.showMessageDialog(null,"FACTURE N° "+id_invoice+" NTIBAHO","",JOptionPane.PLAIN_MESSAGE);


   //JOptionPane.showMessageDialog(null,"BL VENTES SANS DOWNLOADED PROBLEMES","MURAKOZE",JOptionPane.PLAIN_MESSAGE);

      }  catch (Throwable e)  {
   insertError(e+"","ANNULE");
    }

}
public int  retour(int id_invoice,int productcode,String nom)
{
    LinkedList <List> todelete=new LinkedList();

String printStringUp="    REBA NEZA KO IYI FACTURE ARIYO ya RETOUR  ";
int totale=0;
boolean done=false;
boolean done1=false;
String printStringE="";
String printStringW="";

        String client="";
        String date1="";
        String code=  "";
        String lot1 ="";
        int quantite =0;
        double price =0;
        String desi ="";
        String   num_affiliation="";
        String  nom_client="";
        String  prenom_client="";
        int  percentage=0;
        String   numero_quitance  ="";

try
    {
            // Clients Cash
    outResult = s.executeQuery("" +
            "select app.invoice.id_invoice,num_client,date,code_uni,num_lot,quantite" +
            ",list.price,name_product from APP.PRODUCT,APP.INVOICE ,APP.LIST where " +
            "APP.INVOICE.NUM_CLIENT='CASHNORM' " +
            "and APP.INVOICE.id_invoice = "+id_invoice+"  " +
            "and APP.INVOICE.id_invoice = APP.LIST.id_invoice " +
            "and  APP.PRODUCT.CODE=APP.LIST.CODE_UNI " +
            "AND APP.PRODUCT.ID_PRODUCT ="+productcode+"" +
            " order by  app.invoice.id_invoice");

    while (outResult.next())
    {

         client=outResult.getString(2);
         date1=outResult.getString(3);
         code=  outResult.getString(4);
         lot1 =outResult.getString(5);
         quantite =outResult.getInt(6);
         price =outResult.getDouble(7);
         desi =outResult.getString(8);
 printStringE=printStringE+ "\n  "+quantite+ "  "+desi+ "    "+(int)(quantite*price);
printStringW=printStringW+"("+code+"-"+quantite+")";
todelete.add(new List(id_invoice,price,quantite,lot1,code));

      totale=totale+(int)(quantite*price);
             done=true;
     }
      outResult.close();

       // Clients Cash  Unipharma
     outResult = s.executeQuery("" +
             "select app.invoice.id_invoice,num_client,date,code_uni,num_lot," +
             "quantite,list.price,name_product,num_affiliation,nom_client," +
             "prenom_client,percentage,numero_quitance " +
             "from APP.PRODUCT,APP.INVOICE ,APP.LIST,APP.CREDIT,APP.CLIENT_UNIPHARMA " +
             "where APP.INVOICE.NUM_CLIENT ='CASHREMISE'  " +
             "and APP.credit.numero_affilie =  APP.client_UNIPHARMA.num_affiliation " +
             "and APP.INVOICE.id_invoice = "+id_invoice+" " +
             "and APP.INVOICE.id_invoice = APP.credit.id_invoice " +
             "and APP.INVOICE.id_invoice = APP.LIST.id_invoice " +
             "AND APP.PRODUCT.CODE=APP.LIST.CODE_UNI " +
              "AND APP.PRODUCT.ID_PRODUCT="+productcode+"" +
             "order by app.invoice.id_invoice");

    while (outResult.next())
    {
         client=outResult.getString(2);
         date1=outResult.getString(3);
         code=  outResult.getString(4);
         lot1 =outResult.getString(5);
         quantite =outResult.getInt(6);
        price =outResult.getDouble(7);
        desi =outResult.getString(8);
         num_affiliation=outResult.getString(9);
          nom_client=outResult.getString(10);
          prenom_client=outResult.getString(11);
          percentage=outResult.getInt(12);
           numero_quitance  =outResult.getString(13);
printStringE=printStringE+ "\n  "+quantite+ "  "+desi+ "    "+(int)(quantite*price);
printStringW=printStringW+"("+code+"-"+quantite+")";
todelete.add(new List(id_invoice,price,quantite,lot1,code));
      totale=totale+(int)(quantite*price);
         done1=true;

     }

    //  Close the resultSet  and date ='"+d+"'
    outResult.close();
      // Clients Rama

     outResult = s.executeQuery("" +
             "select app.invoice.id_invoice,num_client,date,code_uni,num_lot,quantite,list.price," +
             "name_product,num_affiliation,nom_client,prenom_client,percentage,numero_quitance " +
             "from APP.PRODUCT,APP.INVOICE ,APP.LIST,APP.CREDIT,APP.CLIENT_RAMA " +
             "where APP.INVOICE.NUM_CLIENT ='RAMA'" +
             "and APP.client_RAMA.num_affiliation = APP.credit.numero_affilie " +
             "and APP.INVOICE.id_invoice = "+id_invoice+" " +
             "and APP.INVOICE.id_invoice = APP.credit.id_invoice " +
             "and APP.INVOICE.id_invoice = APP.LIST.id_invoice  " +
             "and APP.PRODUCT.CODE=APP.LIST.CODE_UNI " +
              "AND APP.PRODUCT.ID_PRODUCT="+productcode+"" +
             "order by app.invoice.id_invoice");

    while (outResult.next())
    {
         client=outResult.getString(2);
         date1=outResult.getString(3);
         code=  outResult.getString(4);
         lot1 =outResult.getString(5);
         quantite =outResult.getInt(6);
          price =outResult.getDouble(7);
         desi =outResult.getString(8);
           num_affiliation=outResult.getString(9);
          nom_client=outResult.getString(10);
          prenom_client=outResult.getString(11);
          percentage=outResult.getInt(12);
           numero_quitance  =outResult.getString(13);
printStringE=printStringE+ "\n  "+quantite+ "  "+desi+ "    "+(int)(quantite*price);
printStringW=printStringW+"("+code+"-"+quantite+")";
todelete.add(new List(id_invoice,price,quantite,lot1,code));
       totale=totale+(int)(quantite*price);
              done1=true;
     }

    //  Close the resultSet  and date ='"+d+"'
    outResult.close();


     // Clients Societe
      outResult = s.executeQuery("" +
             "select app.invoice.id_invoice,num_client,date,code_uni,num_lot,quantite,list.price,name_product" +
             ",num_affiliation,nom_client,prenom_client,percentage,numero_quitance " +
             "from APP.PRODUCT,APP.INVOICE ,APP.LIST,APP.CREDIT,APP.CLIENT " +
             "where  APP.INVOICE.NUM_CLIENT !='CASHNORM' " +
             "and APP.INVOICE.NUM_CLIENT !='CASHREMISE' " +
             "and APP.INVOICE.NUM_CLIENT !='RAMA'"  +
             "and APP.client.num_affiliation = APP.credit.numero_affilie " +
             "and APP.INVOICE.id_invoice = "+id_invoice+
             " and APP.INVOICE.id_invoice = APP.credit.id_invoice " +
             " and APP.INVOICE.id_invoice = APP.LIST.id_invoice  " +
             " AND APP.PRODUCT.CODE=APP.LIST.CODE_UNI " +
             " AND APP.PRODUCT.ID_PRODUCT="+productcode+" " +
             " order by app.invoice.id_invoice");

    while (outResult.next())
    {

         client=outResult.getString(2);
         date1=outResult.getString(3);
         code=  outResult.getString(4);
         lot1 =outResult.getString(5);
         quantite =outResult.getInt(6);
         price =outResult.getDouble(7);
         desi =outResult.getString(8);
           num_affiliation=outResult.getString(9);
        nom_client=outResult.getString(10);
        prenom_client=outResult.getString(11);
          percentage=outResult.getInt(12);
         numero_quitance  =outResult.getString(13);
printStringE=printStringE+ "\n  "+quantite+ "  "+desi+ "    "+(int)(quantite*price);
printStringW=printStringW+"("+code+"-"+quantite+")";
todelete.add(new List(id_invoice,price,quantite,lot1,code));
    totale=totale+(int)(quantite*price);
        done1=true;


     }
     if(done)
   {
          printStringUp=printStringUp+ "\n    FACTURE N°    : " +id_invoice;
     printStringE=printStringE+ "\n  ";


printStringE=printStringE+ "\n  TOTAL en RWF: "+(int)totale;


     }

     else
         {


   printStringUp=printStringUp+ "\n    FACTURE N°    : " +id_invoice;
        printStringUp=printStringUp+ "\n    " +date1 ;
        printStringUp=printStringUp+"\n    -------------------------------------------   ";
        printStringUp=printStringUp+"\n    Assureur            : "+client;
        printStringUp=printStringUp+ "\n    N° Affiliation      : "+num_affiliation;
        printStringUp=printStringUp+ "\n    Nom et Prenom : "+nom_client+ " "+prenom_client;
     //   printStringUp=printStringUp+ "\n    Departement     : "+c.EMPLOYEUR;
        printStringUp=printStringUp+ "\n    N° Quittance     : "+numero_quitance;
        printStringUp=printStringUp+"\n    -------------------------------------------   ";





         printStringE=printStringE+ "\n  ";


printStringE=printStringE+ "\n  TOTAL en RWF: "+totale;


 printStringE=printStringE+ "\n  "+client+" "+(100-percentage)+" % : "+(totale- (int)(totale*percentage*0.01));

 printStringE=printStringE+ "\n  ADHERENT  "+percentage+" % : "+(int)(totale*percentage*0.01);
totale=(totale- (int)(totale*percentage*0.01));


     }
    //  Close the resultSet  and date ='"+d+"'
    outResult.close();

     if(done ||done1)
    {


           int n = JOptionPane.showConfirmDialog(null,
                        printStringUp+
                        "\n "+printStringE+
                        "\n MONTANT RETOUR "+totale,
                      "RETOUR ????? ",JOptionPane.YES_NO_OPTION);
         //  JOptionPane.showMessageDialog(null,"FACTURE N° "+n+" ANNULEE","",JOptionPane.PLAIN_MESSAGE);
if(n==0)
{

    Annule deleted=new Annule(id_invoice,num_affiliation,nom+" R",client,printStringW,date1);

    if(retourMse(todelete))
    {
    insertDeleted(deleted);

   // JOptionPane.showMessageDialog(null,"FACTURE N° "+id_invoice+" ANNULEE","",JOptionPane.PLAIN_MESSAGE);
    }
}
else
    totale=0;


                }else
    JOptionPane.showMessageDialog(null,"FACTURE N° "+id_invoice+" NTIBAHO","",JOptionPane.PLAIN_MESSAGE);

    }  catch (Throwable e)  {
   insertError(e+"","retour ");
    }
return totale;
}
public boolean retourMse(LinkedList <List> todelete)
{
    boolean done=true;
    for(int i=0;i< todelete.size();i++)
    {
            try {
                List l = todelete.get(i);

                if(getLot(l))
                {
                    Statement sD = conn.createStatement();
                     String createString1 = " delete from list where id_invoice=" + l.id_invoice + " and code_uni='" + l.code + "'";
                      sD.execute(createString1);

                }
                else{
                  done=false;
                        JOptionPane.showMessageDialog(null,"kwanira FACTURE N° "+l.id_invoice+" umuti wa "+l.code+" biranze ","Ikibazo",JOptionPane.PLAIN_MESSAGE);


             break;
                }






            }  catch (Throwable e)  {
   insertError(e+"","retourMse");
    }

    }
   return done;
}
public boolean  getLot(List l)
{
    boolean done=false;
          try
    {

         Lot lotToUpdate=null;
    outResult = s.executeQuery("" +
            "select * from APP.LOT,APP.PRODUCT" +
            " where code='"+l.code+"' and " +
            "APP.LOT.ID_PRODUCT=APP.PRODUCT.ID_PRODUCT " +
            "and id_LotS ='"+l.lot+"' ");


     while (outResult.next())
    {

    lotToUpdate=new Lot( outResult.getInt(1),"",outResult.getString(10),outResult.getString(3),"",outResult.getInt(2),0,0, outResult.getString(7));
          done=true;    break;

     }
    outResult.close();
    if(lotToUpdate!=null)
    {
s = conn.createStatement();
String createString1 = "" +
        "update APP.LOT set QTY_LIVE=QTY_LIVE+" + l.qty + "" +
        "where id_product="+lotToUpdate.productCode+" " +
        "and ID_LOTS= '" + lotToUpdate.id_LotS + "'" +
        "and BON_LIVRAISON= '" + lotToUpdate.bon_livraison + "'" +
        "and ID_LOT=" + lotToUpdate.id_lot;
s.execute(createString1);

    }
    else
     {
    outResult = s.executeQuery("" +
            "select * from APP.USED_LOT,APP.PRODUCT" +
            " where code='"+l.code+"' and " +
            "APP.USED_LOT.ID_PRODUCT=APP.PRODUCT.ID_PRODUCT " +
            "and id_LotS ='"+l.lot+"' ");
       while (outResult.next())
    {

   lotToUpdate=new Lot( outResult.getInt(1),"",outResult.getString(10),outResult.getString(3),outResult.getString(4),outResult.getInt(2),outResult.getInt(5),outResult.getInt(6), outResult.getString(7));
            done=true;   break;

     }
    outResult.close();

     if(lotToUpdate!=null)
    {
          psInsert = conn.prepareStatement("insert into APP.LOT(ID_PRODUCT,ID_LOT,ID_LOTS,DATE_EXP,QTY_START,QTY_LIVE,BON_LIVRAISON)  values (?,?,?,?,?,?,?)");

        psInsert.setInt(1,lotToUpdate.productCode);
        psInsert.setInt(2,lotToUpdate.id_lot);
        psInsert.setString(3,lotToUpdate.id_LotS);
        psInsert.setString(4,lotToUpdate.dateExp);
        psInsert.setInt(5,lotToUpdate.qtyStart);
        psInsert.setInt(6,l.qty);
        psInsert.setString(7,lotToUpdate.bon_livraison);
        psInsert.executeUpdate();
        psInsert.close();

s = conn.createStatement();
 String createString1 = " " +
         "delete from USED_LOT where ID_LOT="+lotToUpdate.id_lot+" " +
         "and BON_LIVRAISON='"+lotToUpdate.bon_livraison +"' " +
         "and id_LotS= '"+lotToUpdate.id_LotS+"' " +
         "and ID_PRODUCT= "+lotToUpdate.productCode;

s.execute(createString1);

    }


    }

      }  catch (Throwable e)  {
         insertError(e+""," get lot");
    }
    return done;
}


  public void insertDeleted(Annule d)
  {
 try
    {
psInsert = conn.prepareStatement("insert into APP.ANNULE(ID_INVOICE,DATE,NUM_CLIENT ,NUMERO_AFFILIE ,EMPLOYE ,CODE_QTE )  values (?,?,?,?,?,?)");

    psInsert.setInt(1,d.ID_INVOICE);
    psInsert.setString(2,d.DATE);
    psInsert.setString(3,d.NUM_CLIENT);
    psInsert.setString(4,d.NUMERO_AFFILIE);
    psInsert.setString(5,d.EMPLOYE);
    psInsert.setString(6,d.CODE_QTE);

        psInsert.executeUpdate();
        psInsert.close();
   }  catch (Throwable e)  { insertError(e+"","insertDeleted"); }
  }

public void  deleteInvoice(int id_invoice)
{
        try {
            Statement sD = conn.createStatement();
            String createString1 = " delete from invoice where id_invoice=" + id_invoice;
            sD.execute(createString1);
            createString1 = " delete from credit where id_invoice=" + id_invoice;
            sD.execute(createString1);

        } catch (Throwable e)  { insertError(e+"","deleteInvoice"); }
}


public void  bcpTxt()throws FileNotFoundException, IOException
{




                   String  file="BL_VENTES/"+(new Date().getMonth()+1)+"/"+datez+".txt";
         //   JOptionPane.showMessageDialog(null,file,"Downloading Sales",JOptionPane.PLAIN_MESSAGE);


          File f = new File(file);

        PrintWriter sorti=new PrintWriter(new FileWriter(f));

    try
    {
            // Clients Cash
    outResult = s.executeQuery("" +
"select app.invoice.id_invoice,num_client,date,code_uni,num_lot,quantite" +
            ",list.price,name_product from APP.PRODUCT,APP.INVOICE ,APP.LIST where " +
            "APP.INVOICE.id_invoice = APP.LIST.id_invoice " +
             "and APP.INVOICE.date = '"+datez+"' " +
            "AND APP.PRODUCT.CODE=APP.LIST.CODE_UNI order" +
            " by app.invoice.id_invoice");

    while (outResult.next())
    {

        int  id_invoice=  outResult.getInt(1);
        String client=outResult.getString(2);
        String date1=outResult.getString(3);
        String code=  outResult.getString(4);
        String lot1 =outResult.getString(5);
        int quantite =outResult.getInt(6);
        double price =outResult.getDouble(7);
        String desi =outResult.getString(8);


        String factureSage="13;BL"+id_invoice+";"+client+";"+ date1 +";"+code+";"+desi+";"+lot1+";"+quantite+";"+price;
        sorti.println(factureSage);
       //  JOptionPane.showMessageDialog(null,factureSage,"Downloaded",JOptionPane.PLAIN_MESSAGE);

     }
      outResult.close();

      }  catch (Throwable e)  {
   insertError(e+"","bcupText");
    }
    sorti.close();
}

public void closeConnection()
{
try
{
//  JOptionPane.showMessageDialog(null,ip," ip1 ",JOptionPane.PLAIN_MESSAGE);

if(ip.equals("elephant/192.168.0.113"))
      bcpTxt();

    conn.close();

    if (driver.equals("org.apache.derby.jdbc.EmbeddedDriver")) {
    boolean gotSQLExc = false;
    try {
    DriverManager.getConnection("jdbc:derby:;shutdown=true");
    } catch (SQLException se)  {
    if ( se.getSQLState().equals("XJ015") ) {
    gotSQLExc = true;
    }
    }
    if (!gotSQLExc) {
    System.out.println("Database did not shut down normally");
    }  else  {
    System.out.println("Database shut down normally");
    }
   }
}
 catch (Throwable e)  {}
}
public void upDateCodeBar(String codebar,int code)
{
    try {
    s = conn.createStatement();
    String createString1 = " update APP.PRODUCT set CODE_BAR='"+codebar+"' where ID_PRODUCT="+code+"";
    s.execute(createString1);

    }
    catch (Throwable e){
          JOptionPane.showMessageDialog(null,e,"code bar existant ",JOptionPane.PLAIN_MESSAGE);

    }

}
 void addOneLot(int p,Lot neW)
{


    try
    {
psInsert = conn.prepareStatement("insert into APP.LOT(ID_PRODUCT,ID_LOT,ID_LOTS,DATE_EXP,QTY_START,QTY_LIVE,BON_LIVRAISON)  values (?,?,?,?,?,?,?)");

        psInsert.setInt(1,p);
        psInsert.setInt(2,neW.id_lot);
        psInsert.setString(3,neW.id_LotS);
        psInsert.setString(4,neW.dateExp);
        psInsert.setInt(5,neW.qtyStart);
        psInsert.setInt(6,neW.qtyLive);
        psInsert.setString(7,neW.bon_livraison);
        psInsert.executeUpdate();
        psInsert.close();
   }  catch (Throwable e)  { insertError(e+"","addonecodeBar"); }


}

 void addOneCode()
{

   doworkOther("");
    try
    {
        for(int i=0;i<allProduct.size();i++)
        {
    psInsert = conn.prepareStatement("insert into CODEBAR(CODE_MACHINE,CODE_BAR)  values (?,?)");
    psInsert.setInt(1,i);
    psInsert.setString(2,"c"+i);

    psInsert.executeUpdate();
        }
    // Release the resources (clean up )
    psInsert.close();
    }  catch (Throwable e)  { insertError(e+"","addonecodeBar"); }


}
 Variable getParam(String famille,String mode, String nom)
{
     // System.out.println("select  * from APP.VARIABLES where APP.variables.nom_variable= '"+nom+"'   and APP.variables.famille_variable= '"+famille+"' ");
    Variable par = null;
try
{
outResult = s.executeQuery("select  * from APP.VARIABLES where APP.variables.nom_variable= '"+nom+"'  and APP.variables.famille_variable= '"+famille+"' ");

while (outResult.next())
{
  //  System.out.println(outResult.getString(2));
        String num =outResult.getString(1);
        String value =outResult.getString(2);
        String fam =outResult.getString(3);
        String value2 =outResult.getString(4);
        par=(new Variable(num,value,fam,value2));
}}
catch (Throwable e){
      insertError(e+"","getparm autoout");
 }
return     par;
}

 LinkedList<Variable>getParam(String famille)
{
    LinkedList<Variable> par = new LinkedList ();
try
{
outResult = s.executeQuery("select  * from APP.VARIABLES where APP.variables.famille_variable= '"+famille+"'");

while (outResult.next())
{
//    System.out.println(outResult.getString(2));
        String num =outResult.getString(1);
        String value =outResult.getString(2);
        String fam =outResult.getString(3);
        par.add(new Variable(num,value,fam));
}}
catch (Throwable e){
      insertError(e+"","getparm autoout");
 }
return     par;
}

void updateParameter(LinkedList<Variable> par)
{

    for(int i=0;i<par.size();i++)
  {

   Variable v= par.get(i);
   //System.out.println(" update APP.VARIABLES set value_variable ='" + v.value + "',value2_variable ='" + v.value2 + "' where nom_variable='" + v.nom + "' AND famille_variable='" + v.famille + "'");
            try {

                s.execute(  " update APP.VARIABLES set value_variable ='" + v.value + "'  where nom_variable='" + v.nom+v.value2 + "' AND famille_variable='" + v.famille + "'");
            } catch (SQLException ex) {
                insertError(ex+"","updateparam"+v.nom);
            }    }
}
public static void main(String[] arghs)
{
    AutoOut db =new AutoOut("localhost","pharmavie");
    db.read();
    db.listPrixErrone("0","3572","MMI");
}



 }


//////////////////////////////////////////////

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package test;
import java.io.*;
import java.awt.*;
import java.awt.event.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import java.util.*;
import javax.swing.JFrame;
/**
 *
 * @author aimable
 */
public class PersonInterface extends JFrame{

private  JTextField
                    NUM_AFFILIATION,
                    CODE,
                    NOM_CLIENT,
                    PRENOM_CLIENT,
                    PERCENTAGE,
                    DATE_EXP,
                    EMPLOYEUR,
                    SECTEUR,SEXE,NAISSANCE ;
//ASSURANCE,

private JLabel      SEXEL,NAISSANCEL,
                    NUM_AFFILIATIONL,
                    CODEL,
                    NOM_CLIENTL,
                    PRENOM_CLIENTL,
                    PERCENTAGEL,
                    DATE_EXPL,
                    ASSURANCEL,
                    EMPLOYEURL,
                    SECTEURL ;

private JButton send;
private AutoOut db;
private Client c ;


private  JList  societeJList=new JList();
String soc,what;

PersonInterface( AutoOut db, String nom ,String prenom ,String code,String naissance  ,String sexe )
{
this.db=db;
what="";
 makeFrame(" "," ",  nom ,  prenom ,  code,  naissance  ,  sexe  );
// makeFrame(stock,nAff);
    send();
    draw();


}
PersonInterface(AutoOut db,Client c)
{
    what="UPDATE";
    this.db=db;
    this.c=c;
 makeFrame(c.ASSURANCE,c.NUM_AFFILIATION,  c.NOM_CLIENT ,  c.PRENOM_CLIENT ,  c.CODE,  c.AGE  ,  c.SEXE  );
    send();
    draw();


}
public void makeFrame(String stock,String nA,String nom ,String prenom ,String code,String naissance  ,String sexe )
{
     this.CODE= new JTextField(); CODE.setText(code);
    this.NUM_AFFILIATION= new JTextField();
    this.NOM_CLIENT= new JTextField();NOM_CLIENT.setText(nom);
    this.PRENOM_CLIENT= new JTextField();PRENOM_CLIENT.setText(prenom);
        this.SEXE= new JTextField();SEXE.setText(sexe);
            this.NAISSANCE= new JTextField();NAISSANCE.setText(naissance);
    this.PERCENTAGE= new JTextField();
    this.DATE_EXP= new JTextField();
    DATE_EXP.setText("311210");
    this.EMPLOYEUR= new JTextField();
    this.SECTEUR = new JTextField();
     this.CODEL=new JLabel("CODE ");
    this.NUM_AFFILIATIONL=new JLabel("N° AFFILIATION");
    this.NOM_CLIENTL=new JLabel("NOM_CLIENT");
    this.PRENOM_CLIENTL=new JLabel("PRENOM_CLIENT");
    this.PERCENTAGEL=new JLabel("POURCENTAGE");
    this.DATE_EXPL=new JLabel("DATE_EXP:311210");
    this.ASSURANCEL=new JLabel("ASSURANCE");
    this.EMPLOYEURL=new JLabel("EMPLOYEUR");
    this.SECTEURL=new JLabel("TEL:");
      this.SEXEL=new JLabel("SEXE :");
              this.NAISSANCEL=new JLabel("NAISSANCE:");
     this.send=new JButton("ENVOYER");
        soc=stock;



String [] control= db.clients();

         societeJList.setListData(control);
        societeJList.setSelectedIndex(0)    ;
}

public void draw()
{
    setTitle("Register a new Client for Ishyiga");

    this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);


    //on fixe  les composant del'interface
    Container content = getContentPane();


    if(what.equals("UPDATE"))
      UpDatePane(content);
        else
     westPane(content);

     setSize(500,600);
     setVisible(true);

}
public static int getIn (String in)
{
                      int productCode=-1;
    try
    {
        if(in.equals(""))
        {

JOptionPane.showMessageDialog(null,"Ubusa  Ntabwo ari  Pourcentage ",in,JOptionPane.PLAIN_MESSAGE);
        productCode=-1;
        }
        else
        {
        Integer in_int=new Integer(in);
        productCode = in_int.intValue();
        if(productCode<0 || productCode>100)
             {

JOptionPane.showMessageDialog(null,"Ikibazo kuri  Pourcentage ",""+productCode,JOptionPane.PLAIN_MESSAGE);
        productCode=-1;
        }


        }
         }
    catch (NumberFormatException e)
    {
     JOptionPane.showMessageDialog(null,in," Ntabwo ari  Pourcentage ",JOptionPane.PLAIN_MESSAGE);

    productCode=-1;
    }
    return productCode;
}



/**************************************************************************
affiche du stock
**************************************************************************/

public void westPane(Container content )
{
JPanel Pane  = new JPanel();

Pane.setPreferredSize(new Dimension (600,700));

JPanel panel = new JPanel();
panel.setLayout(new GridLayout(11,2));
panel.setPreferredSize(new Dimension (300,500));


  panel.add(CODEL);panel.add(CODE);
// if(!soc.equals("CASHREMISE"))   {

 //}

panel.add(NOM_CLIENTL);panel.add(NOM_CLIENT);
panel.add(PRENOM_CLIENTL);panel.add(PRENOM_CLIENT);
panel.add(NAISSANCEL);panel.add(NAISSANCE);
panel.add(SEXEL);panel.add(SEXE);
panel.add(SECTEURL);panel.add(SECTEUR);
panel.add(ASSURANCEL);
 JScrollPane    scrollPaneProductList =new JScrollPane(societeJList);
  scrollPaneProductList.setPreferredSize(new Dimension (40,40));
  societeJList.setBackground(Color.pink);
 panel.add(scrollPaneProductList);
  panel.add(NUM_AFFILIATIONL);panel.add(NUM_AFFILIATION);
 panel.add(PERCENTAGEL);panel.add(PERCENTAGE);
 panel.add(DATE_EXPL);panel.add(DATE_EXP);
panel.add(EMPLOYEURL);panel.add(EMPLOYEUR);



Pane.add(panel,BorderLayout.CENTER);
Pane.add(send,BorderLayout.SOUTH);

content.add(Pane, BorderLayout.WEST);

}
public void UpDatePane(Container content )
{
JPanel Pane  = new JPanel();

Pane.setPreferredSize(new Dimension (600,700));

JPanel panel = new JPanel();
panel.setLayout(new GridLayout(11,2));
panel.setPreferredSize(new Dimension (300,500));
CODE.setText(c.CODE);
NUM_AFFILIATION.setText(c.NUM_AFFILIATION);

NOM_CLIENT.setText(c.NOM_CLIENT);
PRENOM_CLIENT.setText(c.PRENOM_CLIENT);
PERCENTAGE.setText(""+c.PERCENTAGE);
DATE_EXP.setText("");
//  ASSURANCE.setText(c.ASSURANCE);
 EMPLOYEUR.setText(c.EMPLOYEUR);
SECTEUR.setText(c.SECTEUR);
NAISSANCE.setText(c.AGE);
SEXE.setText(c.SEXE);

     panel.add(NUM_AFFILIATIONL);panel.add(new JLabel(c.NUM_AFFILIATION));
 //}
 panel.add(CODEL);panel.add(new JLabel(c.CODE));
panel.add(NOM_CLIENTL);panel.add(NOM_CLIENT);
panel.add(PRENOM_CLIENTL);panel.add(PRENOM_CLIENT);
panel.add(NAISSANCEL);panel.add(NAISSANCE);
panel.add(SEXEL);panel.add(SEXE);
panel.add(PERCENTAGEL);panel.add(PERCENTAGE);
panel.add(DATE_EXPL);panel.add(DATE_EXP);

panel.add(ASSURANCEL);

  JScrollPane    scrollPaneProductList =new JScrollPane(societeJList);
	scrollPaneProductList.setPreferredSize(new Dimension (40,40));
  societeJList.setBackground(Color.pink);
 panel.add(scrollPaneProductList);



panel.add(EMPLOYEURL);panel.add(EMPLOYEUR);
panel.add(SECTEURL);panel.add(SECTEUR);

send.setText("UPDATE");
Pane.add(panel,BorderLayout.CENTER);

Pane.add(send,BorderLayout.SOUTH);

content.add(Pane, BorderLayout.WEST);

}





/**********************************************************************************************
AJOUT Mannuel
**********************************************************************************************/
public void send()
{

	send.addActionListener(new ActionListener() {
	public void actionPerformed(ActionEvent ae)
	{
    if(ae.getSource()==send && (societeJList.getSelectedValue()!= null || soc.equals("SOCIETE")  ))
    {

        String DBB="CLIENT";
    String NUM_AFFILIATIONa    =  NUM_AFFILIATION.getText().toUpperCase();

    String nAff="'"+NUM_AFFILIATIONa+"'";

    String NOM_CLIENTa =  NOM_CLIENT.getText().toUpperCase();
    String PRENOM_CLIENTa   =  PRENOM_CLIENT.getText().toUpperCase();

    int PERCENTAGEa  =getIn (PERCENTAGE.getText());
    if(societeJList.getSelectedValue().equals("RAMA"))
    {    PERCENTAGEa=15;DBB="CLIENT_RAMA";}

    String datedoc=DATE_EXP.getText();

    int dated=0;



    String  ASSURANCEa  ="";


    ASSURANCEa= ((String)societeJList.getSelectedValue()).toUpperCase();

    String EMPLOYEURa =EMPLOYEUR.getText().toUpperCase();
    String  SECTEURa  = SECTEUR.getText().toUpperCase();
    try
    {
    if( (datedoc.length()==6))
    {
    String createString1=""+datedoc.substring(4)+""+datedoc.substring(2, 4)+""+datedoc.substring(0,2);
    Integer hhh=new Integer (createString1);
    dated = hhh.intValue();

     int DATE_EXPa  =dated;
     if(soc.equals("CASHREMISE"))
     {DBB="CLIENT_UNIPHARMA";  }


        if(!(PERCENTAGEa==-1))
        if(!NUM_AFFILIATIONa.equals("") || soc.equals("CASHREMISE"))
        if(!NOM_CLIENTa.equals("")&& !PRENOM_CLIENTa.equals(""))
        if(!ASSURANCEa.equals(""))
        {



            if(what.equals("UPDATE"))
               db.upDateClient(new Client(NUM_AFFILIATIONa,NOM_CLIENTa ,PRENOM_CLIENTa ,PERCENTAGEa,DATE_EXPa,ASSURANCEa ,EMPLOYEURa,SECTEURa,"",CODE.getText(),NAISSANCE.getText(),SEXE.getText()),DBB,CODE.getText());

            else
              db.insertClient( new Client(NUM_AFFILIATIONa,NOM_CLIENTa ,PRENOM_CLIENTa ,PERCENTAGEa,DATE_EXPa,ASSURANCEa ,EMPLOYEURa,SECTEURa,"",CODE.getText(),NAISSANCE.getText(),SEXE.getText()));

        }
        else
            JOptionPane.showMessageDialog(null," Hari Ikibazo mu nyandiko "," ASSURANCE ",JOptionPane.PLAIN_MESSAGE);
         else
            JOptionPane.showMessageDialog(null," Hari Ikibazo mu nyandiko "," AMAZINA ",JOptionPane.PLAIN_MESSAGE);
         else
            JOptionPane.showMessageDialog(null," Hari Ikibazo mu nyandiko "," NUM_AFFILIATION ",JOptionPane.PLAIN_MESSAGE);

    } else
         JOptionPane.showMessageDialog(null,datedoc.length()+" Reba Uburebure ko busa na 311208  "," Itariki ",JOptionPane.PLAIN_MESSAGE);




    }

    catch(Throwable ex)
    {
    JOptionPane.showMessageDialog(null," Itariki ifite ikibazo MU nyandiko "," Itariki ",JOptionPane.PLAIN_MESSAGE);
    }









                        }
			}});

}
 @Override
protected void processWindowEvent(WindowEvent evt)
{
   if(WindowEvent.WINDOW_CLOSING == evt.getID())
   {
	   this.dispose();
			//default icon, custom title
//JOptionPane.showMessageDialog(this,"RIRAFUNGIRA RIMWE N'ISHYIGA"," IKIBAZO",JOptionPane.PLAIN_MESSAGE);
        			//this.setDefaultCloseOperation(HIDE_ON_CLOSE);


	}

}


public static void main(String[] arghs)
{
        try {
            StartServer d = new StartServer();
            AutoOut db = new AutoOut("localhost","ishyiga");
            db.read();

            //new PersonInterface(db, "", "RAMA");

        } catch (Throwable ex) {
            Logger.getLogger(PersonInterface.class.getName()).log(Level.SEVERE, null, ex);
        }

}
}


////////////////////

iki
select NAME_PRODUCT ,PRIX_OUT,QTY_OUT,ID_INVOICE from APP.LIST,APP.LOT,APP.PRODUCT where APP.LIST.ID_LOT=APP.LOT.ID_LOTS AND APP.PRODUCT.ID_PRODUCT=APP.LOT.ID_PRODUCT ORDER BY ID_INVOICE

CREATE TABLE APP.PRODUCT 
        (ID_PRODUCT INT NOT NULL GENERATED ALWAYS AS IDENTITY,
        CODE_PRODUCT VARCHAR(15)UNIQUE NOT NULL,
        ID_FAMILLE VARCHAR(15)  UNIQUE NOT NULL,
        NAME_PRODUCT VARCHAR(32)UNIQUE NOT NULL,
        UNITE VARCHAR(15) NOT NULL,
        IN_PRICE INT NOT NULL,
        RAMA_PRICE INT NOT NULL,
        CORAR_PRICE INT NOT NULL,
        SORAS_PRICE INT NOT NULL,
        MMI_PRICE INT NOT NULL,
        TAXE INT NOT NULL)

    create table "APP".VEDETTE
    (
            ID_VEDETTE INT NOT NULL ,
            TYPE_VEDETTE VARCHAR(15) not null,
            CAT_VEDETTE INTEGER not null,
            NUM_VEDETTE INTEGER not null,
            ID_PRODUCT INTEGER default 0 not null
    )
select * from APP.PRODUCT contains
create table "APP".PRODUCT
(
ID_PRODUCT INTEGER  NOT NULL ,
','NAME_PRODUCT VARCHAR(100) not null,
','PRICE INTEGER not null
)
create table "APP".LOT
(
','ID_PRODUCT INTEGER not null,
','ID_LOT INTEGER not null,
','ID_LOTS VARCHAR(20) not null unique,
','DATE_EXP VARCHAR(20) not null ,
','QTY_START INTEGER not null,
','QTY_LIVE INTEGER not null,
','BON_LIVRAISON VARCHAR(20)
)

create table "APP".INVOICE
(
','ID_INVOICE VARCHAR(32) NOT NULL,
','HOUR_INVOICE  VARCHAR(32) NOT NULL,
','PAYED_INVOICE INTEGER not null,
','TOTAL_INVOICE INTEGER not null,
','ID_EMPLOYE VARCHAR(32) NOT NULL
)


        create table "APP".PRODUCT_UNI
        (
                ID_PRODUCT INTEGER  NOT NULL ,
                CODE_PRODUCT VARCHAR(50) not null,
-                ID_FAMILLE VARCHAR(50) not null,
                NAME_PRODUCT VARCHAR(100) not null,
                COEF_CASH INTEGER  not null,
                COEF_SOCIETE INTEGER  not null
        )
create table "APP".PRODUCT
(
        ID_PRODUCT INTEGER  NOT NULL ,
','NAME_PRODUCT VARCHAR(100) not null,
','PRICE INTEGER not null
)

 create table "APP".LOT
(
','ID_PRODUCT INTEGER not null,
','ID_LOT INTEGER not null,
','ID_LOTS VARCHAR(20) not null unique,
','DATE_EXP VARCHAR(10) not null ,
','QTY_START INTEGER not null,
','QTY_LIVE INTEGER not null,
','BON_LIVRAISON VARCHAR(20)
)
create table "APP".INVOICE
(
','ID_INVOICE VARCHAR(50) not null unique,
','HOUR_INVOICE TIMESTAMP default CURRENT_TIMESTAMP,
','PAYED_INVOICE INTEGER not null,
','TOTAL_INVOICE INTEGER not null,
','ID_EMPLOYE VARCHAR(20) not null
)
create table "APP".LOGIN
(
','ID_EMPLOYE VARCHAR(30) not null ,
','HOUR_INVOICE TIMESTAMP default CURRENT_TIMESTAMP,
','ID_EMPLOYE VARCHAR(5) not null,
        ARGENT     INTEGER not null
)
insert into APP.EMPLOYE (ID_EMPLOYE,NOM_EMPLOYE,PRENOM_EMPLOYE,BP_EMPLOYE,CATRE_INDETITE,LEVEL_EMPLOYE,TITRE_EMPLOYE,PWD_EMPLOYE,TEL_EMPLOYE)  
values 
(1,'Kimenyi',' ','6153KGL',11,0,'CEO','beta1',03230909)
,
(2,'Gaetan',' ','6153KGL',202,0,'CEO','beta1',03230909),
(3,'Benoite',' ','6153KGL',323,0,'CEO','beta1',03230909),
(4,'Donnat',' ','6153KGL',434,0,'CEO','beta1',03230909),
(5,'Théorèse',' ','6153KGL',545,0,'CEO','beta1',03230909),
(9,'Alphonse',' ','6153KGL',909,0,'CEO','beta1',03230909),
(6,'Desire',' ','6153KGL',676,0,'CEO','beta1',03230909),
(8,'Nsengiyumva',' ','6153KGL',876,0,'CEO','beta1',03230909),
(10,'Dalius',' ','6153KGL',111,0,'CEO','beta1',03230909)




Gaetan
Benoite
Donnat
Joseph
Nsengiyumva
Dalius










insert into APP.EMPLOYE (ID_EMPLOYE,NOM_EMPLOYE,PRENOM_EMPLOYE,BP_EMPLOYE,CATRE_INDETITE,LEVEL_EMPLOYE,TITRE_EMPLOYE,PWD_EMPLOYE,TEL_EMPLOYE)  
values (3,'Bugingo','Richard','6153KGL',113,0,'CEO','beta1',03230909)

insert into APP.EMPLOYE (ID_EMPLOYE,NOM_EMPLOYE,PRENOM_EMPLOYE,BP_EMPLOYE,CATRE_INDETITE,LEVEL_EMPLOYE,TITRE_EMPLOYE,PWD_EMPLOYE,TEL_EMPLOYE)  
values (4,'Butera','Annacreti','6153KGL',1224,0,'CEO','beta1',03230909)

insert into APP.EMPLOYE (ID_EMPLOYE,NOM_EMPLOYE,PRENOM_EMPLOYE,BP_EMPLOYE,CATRE_INDETITE,LEVEL_EMPLOYE,TITRE_EMPLOYE,PWD_EMPLOYE,TEL_EMPLOYE)  
values (5,'Nyirashuli','Jeanne','6153KGL',555,0,'CEO','beta1',03230909)
insert into APP.EMPLOYE (ID_EMPLOYE,NOM_EMPLOYE,PRENOM_EMPLOYE,BP_EMPLOYE,CATRE_INDETITE,LEVEL_EMPLOYE,TITRE_EMPLOYE,PWD_EMPLOYE,TEL_EMPLOYE)  
values (6,'Mugabo','Jeanne','6153KGL',254,0,'CEO','beta1',03230909)

insert into APP.EMPLOYE (ID_EMPLOYE,NOM_EMPLOYE,PRENOM_EMPLOYE,BP_EMPLOYE,CATRE_INDETITE,LEVEL_EMPLOYE,TITRE_EMPLOYE,PWD_EMPLOYE,TEL_EMPLOYE)  
values (7,'Mukankusi','Domitille','6153KGL',778,0,'CEO','beta1',03230909)

insert into APP.EMPLOYE (ID_EMPLOYE,NOM_EMPLOYE,PRENOM_EMPLOYE,BP_EMPLOYE,CATRE_INDETITE,LEVEL_EMPLOYE,TITRE_EMPLOYE,PWD_EMPLOYE,TEL_EMPLOYE)  
values (8,'Habiyaremye','Janvier','6153KGL',110,0,'CEO','beta1',03230909)
insert into APP.EMPLOYE (ID_EMPLOYE,NOM_EMPLOYE,PRENOM_EMPLOYE,BP_EMPLOYE,CATRE_INDETITE,LEVEL_EMPLOYE,TITRE_EMPLOYE,PWD_EMPLOYE,TEL_EMPLOYE)  
values (9,'Kampire','Odette','6153KGL',989,0,'CEO','beta1',03230909)

insert into APP.EMPLOYE (ID_EMPLOYE,NOM_EMPLOYE,PRENOM_EMPLOYE,BP_EMPLOYE,CATRE_INDETITE,LEVEL_EMPLOYE,TITRE_EMPLOYE,PWD_EMPLOYE,TEL_EMPLOYE)  
values (10,'Gakuba','Odille','6153KGL',1018,0,'CEO','beta1',03230909)

insert into K.EMPLOYE (ID_EMPLOYE,NOM_EMPLOYE,PRENOM_EMPLOYE,BP_EMPLOYE,CATRE_INDETITE,LEVEL_EMPLOYE,TITRE_EMPLOYE,PWD_EMPLOYE,TEL_EMPLOYE)  
values (2,'kime','amedy','6153KGL',2,0,'CEO','beta1',08880066)

insert into APP.EMPLOYE (ID_EMPLOYE,NOM_EMPLOYE,PRENOM_EMPLOYE,BP_EMPLOYE,CATRE_INDETITE,LEVEL_EMPLOYE,TITRE_EMPLOYE,PWD_EMPLOYE,TEL_EMPLOYE)
values (23,'Kwizera','Yvonne','6153KGL',2332,0,'CEO','beta1',08880066)




create table "APP".PRODUCT
(
','ID_PRODUCT INT NOT NULL GENERATED ALWAYS AS IDENTITY,
','CODE_PRODUCT VARCHAR(15) not null unique,
','ID_FAMILLE VARCHAR(15) not null unique,
','NAME_PRODUCT VARCHAR(32) not null unique,
','UNITE VARCHAR(15) not null,
','IN_PRICE INTEGER not null,
','RAMA_PRICE INTEGER not null,
','CORAR_PRICE INTEGER not null,
','SORAS_PRICE INTEGER not null,
','MMI_PRICE INTEGER not null,
','CASH_PRICE INTEGER not null,
','SOCIETE_PRICE INTEGER not null,
','TAXE INTEGER not null
)

create table LIST(
ID_LOT      VARCHAR(15) not null,
ID_INVOICE  VARCHAR(50) not null,
PRIX_OUT  INTEGER NOT NULL,
QTY_OUT INTEGER NOT NULL
)

CREATE TABLE CREDIT(
ID_INVOICE  VARCHAR(50) not null,
NUMERO_AFFILIE VARCHAR(50) not null ,
NUMERO_QUITANCE  VARCHAR(50) not null PRIMARY KEY,
SOLDE INTEGER NOT NULL,
PAYED INTEGER NOT NULL,
CAUSE  VARCHAR(50) 
)

CREATE TABLE CLIENT 
(NUM_AFFILIATION VARCHAR(20)NOT NULL PRIMARY KEY, 
NOM_CLIENT VARCHAR(15) NOT NULL,
PRENOM_CLIENT VARCHAR(15) NOT NULL,
CATRE_INDETITE INT NOT NULL , 
PERCENTAGE INT NOT NULL ,
SOCIETE VARCHAR(15) NOT NULL,
ANNE_NAISSANCE INT NOT NULL,
TEL_CLIENT INT NOT NULL  )

CREATE TABLE CLIENT_RAMA 
(NUM_AFFILIATION VARCHAR(20) NOT NULL PRIMARY KEY, 
NOM_CLIENT VARCHAR(15) NOT NULL,
PRENOM_CLIENT VARCHAR(15) NOT NULL,
CATRE_INDETITE INT NOT NULL , 
PERCENTAGE INT NOT NULL ,
DPT VARCHAR(15) NOT NULL,
ANNE_NAISSANCE INT NOT NULL,
TEL_CLIENT INT NOT NULL  )

UPDATE PRODUCT_RAMA SET ID_PRODUCT=2043 WHERE ID_PRODUCT=384


select * from APP.PRODUCT_RAMA order by NAME_PRODUCT

create table "APP".ANNULE
(
','ID_INVOICE INTEGER not null,
','HEURE TIMESTAMP default CURRENT TIMESTAMP not null,
','DATE VARCHAR(6),
','NUM_CLIENT VARCHAR(15),
','NUMERO_AFFILIE VARCHAR(50) not null,
','EMPLOYE VARCHAR(15),
','CODE_QTE VARCHAR(150)
)
rangePoids','3','PAR
searchRange','2009-01-04 12:00:00','PAR
periode','7','PAR
MultipleSemaineCorrele','0.5','PAR
MultipleValAberant','0.5','PAR
MultipleStockMin','0.3','PAR
productSup','347','PAR
productInf','344','PAR
ecartType','1.5','PAR

insert into APP.VARIABLES (NOM_VARIABLE,VALUE_VARIABLE,FAMILLE_VARIABLE) VALUES ('rangePoids','3','PAR')

insert into APP.VARIABLES (NOM_VARIABLE,VALUE_VARIABLE,FAMILLE_VARIABLE) VALUES
('MultipleSemaineCorrele','2009-01-04 12:00:00','PAR'),
('MultipleSemaineCorrele','2009-01-04 12:00:00','PAR'),
('MultipleValAberant','0.5','PAR'),
('MultipleStockMin','0.3','PAR'),
('productSup','7','PAR'),
('productInf','7','PAR'),
('ecartType','1.5','PAR')
update APP.INVOICE set num_client='ENGENRWANDA' where num_client='TOTALRWANDA' and heure >'2009-06-01 00:23:49'
insert into APP.VARIABLES (NOM_VARIABLE,VALUE_VARIABLE,FAMILLE_VARIABLE,,VALUE2_VARIABLE) VALUES

('CASHNORM',' ','CLIENT','KIGALI -  RWANDA'),
('CORAR',' ','CLIENT','KIGALI -  RWANDA'),
('SORAS',' ','CLIENT','KIGALI -  RWANDA'),
('MMI',' ','CLIENT','KIGALI -  RWANDA'),
('UNR',' ','CLIENT','KIGALI -  RWANDA')



('MEDECINS',' ','CLIENT','KIGALI -  RWANDA'),
('MMI',' ','CLIENT','KIGALI -  RWANDA'),
('PARTICULIER',' ','CLIENT','KIGALI -  RWANDA'),
('RAMA',' ','CLIENT','KIGALI -  RWANDA'),
('SORAS',' ','CLIENT','KIGALI -  RWANDA'),
('UNR',' ','CLIENT','KIGALI -  RWANDA'),
('URWEGO',' ','CLIENT','KIGALI -  RWANDA'),
('COGEAR',' ','CLIENT'),
('COGEBANQUE',' ','CLIENT'),
('COMPASSION',' ','CLIENT'),
('COMPULEC',' ','CLIENT'),
('CORAR',' ','CLIENT'),
('EAC',' ','CLIENT'),
('KIE',' ','CLIENT'),
('KIPHARMA',' ','CLIENT'),
('MMI',' ','CLIENT'),
('NOVOTEL',' ','CLIENT'),
('OXFAMGB',' ','CLIENT'),
('RAMA',' ','CLIENT'),
('RWANDAMOTOR',' ','CLIENT'),
('SAVETHECHILDEN',' ','CLIENT'),
('SFB',' ','CLIENT'),
('SOGERWA',' ','CLIENT'),
('SONARWA',' ','CLIENT'),
('SORAS',' ','CLIENT'),
('TOTALRWANDA',' ','CLIENT'),
('UBPR',' ','CLIENT'),
('URWEGO',' ','CLIENT'),
('UBPRMUHIMA',' ','CLIENT'),
('UMWALIMUSACCO',' ','CLIENT'),
('UNIPHARMA',' ','CLIENT'),
('VANBREDA',' ','CLIENT'),
('CLIENTRAMA',' ','CLIENT'),
('CLIENTSORAS',' ','CLIENT'),
('CLIENTMMI',' ','CLIENT'),
('CLIENTCORAR',' ','CLIENT'),
('CLIENTURWEGO',' ','CLIENT')

delete from APP.VARIABLES where app.VARIABLES.FAMILLE_VARIABLE='FAM'

insert into APP.VARIABLES (NOM_VARIABLE,VALUE_VARIABLE,FAMILLE_VARIABLE) VALUES
('CA','1.347','FAM'),
('CB','1.347','FAM'),
('CC','1.782','FAM'),
('CHE','1.347','FAM'),
('CHF','1.347','FAM'),
('CH','1.347','FAM'),
('FTB','1.194','FAM'),
('FTC','1.19','FAM'),
('HA','1.090','FAM'),
('HC','1.343','FAM'),
('HL','1.343','FAM'),
('HM','1.343','FAM'),
('HP','1.343','FAM'),
('HR','1.343','FAM'),
('PGA','1.667','FAM'),
('PGE','1.343','FAM'),
('PLG','1.235','FAM'),
('PSA','1.563','FAM'),
('PSE','1.259','FAM'),
('PV','1.259','FAM')

insert into APP.VARIABLES (NOM_VARIABLE,VALUE_VARIABLE,FAMILLE_VARIABLE) VALUES
('dateY','13','PRINT')
('affilie','13','PRINT'),
('quittance','16','PRINT')


,
('nom','18','PRINT'),
('prenom','14','PRINT'),
('cadresse','10','PRINT'),
('cdate','10','PRINT'),
('cclient','10','PRINT'),
('cfacture','15','PRINT'),
('creference','10','PRINT'),
('ctitle','10','PRINT'),
('cgenerale','8','PRINT'),
('ctotal','10','PRINT'),
('cglobal','10','PRINT'),
('logoX','1','PRINT'),
('logoY','1','PRINT'),
('clientY','5','PRINT'),
('clientX','5','PRINT'),
('dateX','5','PRINT'),
('adresseY','5','PRINT'),
('margin','50','PRINT'),
('ligne','10','PRINT'),
('longeur','69','PRINT')



insert into APP.VARIABLES (NOM_VARIABLE,VALUE_VARIABLE,FAMILLE_VARIABLE,VALUE2_VARIABLE) VALUES
('CODE','10','PRINT','FACTURE'),
('DESIGNATION','10','PRINT','FACTURE'),
('LOT','10','PRINT','FACTURE'),
('EXP','10','PRINT','FACTURE'),
('QTE','10','PRINT','FACTURE'),
('PRIX U.','10','PRINT','FACTURE'),
('TVA','10','PRINT','FACTURE'),
('TOTAL','10','PRINT','FACTURE'),
('SIZECODE','10','PRINT','FACTURE'),
('SIZEDESIGNATION','10','PRINT','FACTURE'),
('SIZELOT','10','PRINT','FACTURE'),
('SIZEEXP','10','PRINT','FACTURE'),
('SIZEQTE','10','PRINT','FACTURE'),
('SIZEPRIX U.','10','PRINT','FACTURE'),
('SIZETVA','10','PRINT','FACTURE'),
('SIZETOTAL','10','PRINT','FACTURE'),
('marginDownNotFull','120','PRINT','FACTURE'),
('marginDownFull','30','PRINT','FACTURE')

('prixCadreX','280','PRINT','FACTURE'),
('prixCadreY','5','PRINT','FACTURE'),
('prixCadreL','287','PRINT','FACTURE'),
('prixCadreR','40','PRINT','FACTURE'),
('prixligne1X','370','PRINT','FACTURE'),
('prixligne2X','460','PRINT','FACTURE'),
('totalHT','285','PRINT','FACTURE'),
('totalTVA','375','PRINT','FACTURE'),
('totalTTC','480','PRINT','FACTURE'),
('prixligne3X','280','PRINT','FACTURE'),
('prixtotalHT','365','PRINT','FACTURE'),
('prixtotalTVA','425','PRINT','FACTURE'),
('noticeligne1X','100','PRINT','FACTURE'),
('noticeligne1Y','60','PRINT','FACTURE'),
('noticeligne2Y','70','PRINT','FACTURE'),
('noticeligne3Y','80','PRINT','FACTURE'),
('compte2X','390','PRINT','FACTURE'),
('compte1X','0','PRINT','FACTURE'),
('compteStart','20','PRINT','FACTURE'),
('noticecadreX','0','PRINT','FACTURE'),
('noticecadreY','0','PRINT','FACTURE'),
('noticecadreL','0','PRINT','FACTURE'),
('noticecadreR','40','PRINT','FACTURE'),
('prixtotalTTC','500','PRINT','FACTURE'),
('LBoxClient','200','PRINT','FACTURE'),
('RBoxClient','50','PRINT','FACTURE')

insert into APP.VARIABLES (NOM_VARIABLE,VALUE_VARIABLE,FAMILLE_VARIABLE,VALUE2_VARIABLE) VALUES
('AFACTURE','10','PRINT','CODE'),
('BFACTURE','10','PRINT','DESIGNATION'),
('CFACTURE','10','PRINT','LOT'),
('DFACTURE','10','PRINT','EXP'),
('EFACTURE','10','PRINT','QTE'),
('FFACTURE','10','PRINT','PRIX_U'),
('GFACTURE','10','PRINT','TVA'),
('HFACTURE','10','PRINT','TOTAL'),
('ACOMMANDE','10','PRINT','CODE'),
('BCOMMANDE','10','PRINT','DESIGNATION'),
('CCOMMANDE','10','PRINT','C_FRSS'),
('DCOMMANDE','10','PRINT','CONDIT.'),
('ECOMMANDE','10','PRINT','QTE'),
('FCOMMANDE','10','PRINT','POIDS'),
('GCOMMANDE','10','PRINT','VOLUME'),
('HCOMMANDE','10','PRINT','PRIX_U'),
('ICOMMANDE','10','PRINT','TOTAL'),
('ALIVRAISON','10','PRINT','CODE'),
('BLIVRAISON','10','PRINT','DESIGNATION'),
('CLIVRAISON','10','PRINT','LOT'),
('DLIVRAISON','10','PRINT','EXP'),
('ELIVRAISON','10','PRINT','QTE'),
('FLIVRAISON','10','PRINT','PRIX_U'),
('GLIVRAISON','10','PRINT','PRIX_R'),
('HLIVRAISON','10','PRINT','TOTAL'),
('APROFORMA','10','PRINT','CODE'),
('BPROFORMA','10','PRINT','DESIGNATION'),
('EPROFORMA','10','PRINT','QTE'),
('FPROFORMA','10','PRINT','PRIX_U'),
('GPROFORMA','10','PRINT','TVA'),
('HPROFORMA','10','PRINT','TOTAL'),
('CPROFORMA','10','PRINT','LOT'),
('DPROFORMA','10','PRINT','EXP'),

('ADEMANDE','10','PRINT','CODE'),
('BDEMANDE','10','PRINT','DESIGNATION'),
('CDEMANDE','10','PRINT','LOT'),
('DDEMANDE','10','PRINT','EXP'),
('EDEMANDE','10','PRINT','QTE'),
('FDEMANDE','10','PRINT','VOLUME'),
('GDEMANDE','10','PRINT','POIDS'),
('HDEMANDE','10','PRINT','PRIX'),

('LENGTHLIVRAISON','7','PRINT','CODE'),
('LENGTHCOMMANDE','7','PRINT','DESIGNATION'),
('LENGTHPROFORMA','7','PRINT','LOT'),
('LENGTHDEMANDE','7','PRINT','EXP'),


('LENGTHFACTURE','7','PRINT','QTE')



insert into APP.VARIABLES (NOM_VARIABLE,VALUE_VARIABLE,FAMILLE_VARIABLE) VALUES
('affilieL','7','PRINT'),
('quittanceL','14','PRINT')


update  APP.VARIABLES set nom_variable='ABL' where  nom_variable='CODE' AND VALUE2_VARIABLE='CODE'

update  APP.VARIABLES set nom_variable='CASHNORM' where  nom_variable='COMPTOIR' 

update employe set nom_employe='Kimenyi' where nom_employe= 'kama'
update APP.VARIABLES set VALUE_variable='INSURRANCE \n Kigali BP 3333' where APP.variables.nom_variable = 'AAR';
update variables set value_variable='Assurance Maladie ' where famille_variable='CLIENT'
delete from compta
insert into compta (document,date,compte,intitule,libele,montant_cash,montant_credit,tva)values (109,'050109',411000,'CASHNORM','Wed May 20 08:19:59 GMT+02:00 2009',1006910,0,3606)

insert into APP.VARIABLES (NOM_VARIABLE,VALUE_VARIABLE,FAMILLE_VARIABLE,VALUE2_VARIABLE) VALUES
('nomMAIN','10','RUN','PHARMACIE'),
('telMAIN','10','RUN','Tel : '),
('tvaMAIN','10','RUN','TVA'),
('bpMAIN','10','RUN','KGL'),
('machineBcUp30MinMAIN','10','RUN','elephant/192.168.0.112'),
('serveurBcUp30MinMAIN','10','RUN','192.168.0.110'),
('unVedetteMAIN','10','RUN','VEDETTE'),
('deuxVedetteMAIN','10','RUN','IMITI'),
('troisVedetteMAIN','10','RUN','ALIMENT BÉBÉ'),
('quatreVedetteMAIN','10','RUN','COSMETIQUE'),
('cinqVedetteMAIN','10','RUN','PAMPERS'),
('sixVedetteMAIN','10','RUN','ANTIDOULEUR'),
('septVedetteMAIN','10','RUN','VERMIFUGE'),
('huitVedetteMAIN','10','RUN','ANTIHISTALIQUE'),
('itemsMAIN','10','RUN','AGASEKE'),
('annuleButtonMAIN','10','RUN','ALL'),
('removeButtonMAIN','10','RUN','ONE'),
('ramaMAIN','10','RUN','RAMA"'),
('corarMAIN','10','RUN','CORAR'),
('sorasMAIN','10','RUN','SORAS'),
('mmiMAIN','10','RUN','MMI '),
('uniMAIN','10','RUN','SCORE'),
('societeMAIN','10','RUN','SOCIETE'),
('fargMAIN','10','RUN',''),
('aarMAIN','10','RUN','STAFF'),
('nomMAIN','10','RUN','PHARMACIE'),
('telMAIN','10','RUN','Tel'),
('tvaMAIN','10','RUN','TVA'),
('bpMAIN','10','RUN','KGL'),
('debutMAIN','10','RUN','2010-01-01 01:00:00'),
('finMAIN','10','RUN','2010-01-31 23:59:00')


insert into APP.VARIABLES (NOM_VARIABLE,VALUE_VARIABLE,FAMILLE_VARIABLE,VALUE2_VARIABLE) VALUES
('dayToview','10','RUN','10')