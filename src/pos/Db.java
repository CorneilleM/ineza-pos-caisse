package pos;

import clinic.Actes;
import clinic.Visite;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Image;
import java.io.*;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.*;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.SortedSet;
import java.util.StringTokenizer;
import java.util.TreeSet;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.sql.rowset.serial.SerialBlob;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import static pos.Main.lang;
import static pos.Main.tax;
import rra.*;
import smart.*; 

public class Db {
    //jdbc:derby://192.168.0.113:1527/ishyiga;create=true
//   ## DEFINE VARIABLES SECTION ##
// define the driver to use ResultSet

    String driver = "org.apache.derby.jdbc.NetWorkDriver";

// define the Derby connection URL to use  192.168.0.113 99.240.205.21
    String connectionURL;//= "jdbc:derby://localhost:1527/" + dbName + ";create=true";
//String connectionURL = "jdbc:derby://99.240.205.21:1527/" + dbName + ";create=true";
//String connectionURL = "jdbc:derby://192.168.0.113:1527/" + dbName + ";create=true";
    ResultSet outResult, outResult1;

    public Connection conn = null;
    static Connection connStatic = null;

    Statement s;
    LinkedList<Product> allProduct, allProductall;
    LinkedList allEmploye;
    LinkedList<Lot> lot = new LinkedList();
    PreparedStatement psInsert;
    PreparedStatement psInsert1;
    public String datez, mois, observa, dbName, special, devise, nom_empl = "";
    public static String ip,lang;
    LinkedList<String> controlInvoice;
    public Image img1 = null;
    static public Image rraLogo = null;
    public static RRA_PACK invoicePack = null;
    String codeBar = "";
    public LinkedList<Lot> lotList;
    public Vector<String> lines = new Vector<String>();

    public Db(String server, String dataBase) {
        this.dbName = dataBase;
        connectionURL = "jdbc:derby://" + server + ":1527/" + dataBase;//+ ";user=Root; password=123";
        System.out.println(connectionURL);

    }

    public void read() {
        Date d = new Date();

        String day = "";
        String mm = "";
        String year = "";

        if (d.getDate() < 10) {
            day = "0" + d.getDate();
        } else {
            day = "" + d.getDate();
        }
        if ((d.getMonth() + 1) < 10) {
            mm = "0" + (d.getMonth() + 1);
        } else {
            mm = "" + (d.getMonth() + 1);
        }
        if ((d.getYear() - 100) < 10) {
            year = "0" + (d.getYear() - 100);
        } else {
            year = "" + (d.getYear() - 100);
        }

        datez = day + mm + year;

        mois = mm;

        try {
            // Create (if needed) and connect to the database
            conn = DriverManager.getConnection(connectionURL);
            connStatic = DriverManager.getConnection(connectionURL);
            String version = conn.getMetaData().getDatabaseProductVersion();
            s = conn.createStatement();
            System.out.println(version);
            boolean sasa = popIfDerbyVersion(version);
            System.out.println(version + " " + sasa);

            System.out.println(nom_empl + "connection etablie TO  " + connectionURL);
//     this.MRC=getRRA("mrc");
//      this.TIN=getRRA("tin");    
//    if(isRRA)
//        rra = new RRA_SDC (getRRA("tin"), getRRA("mrc"), "BBBCCNNNNNN",conn,nom_empl,getRRA("defaultPort")) ;
        } catch (Throwable e) {
            System.err.println(nom_empl + "connection etablie TO " + connectionURL + e);

            //ror(e+"","Read"); 
        }

        String imgPath = getValue("logoV", "FACTURE");
        String rraLog = getValue("rraLogoV", "FACTURERRA");

        System.out.println(rraLog + " " + imgPath);
        try {
            rraLogo = getImage(rraLog);
        } catch (Throwable e) {
            System.out.println(e + "errrr to  " + rraLog);
        }

        try {
            img1 = getImage(imgPath);
        } catch (Throwable e) {
            System.out.println(e + "errrr to  " + imgPath);
        }

    }

    public static boolean popIfDerbyVersion(String version) {

        if (version == null) {
            JOptionPane.showMessageDialog(null, " CALL ISHYIGA SUPPORT (TEL: 252 600 493)\n DERBY VERSION  " + version,
                    " DERBY VERSION PROBLEM ", JOptionPane.PLAIN_MESSAGE);
            return false;
        }
        try {
            String sp[] = version.split("\\."); //10.11.1.2
            System.out.println(sp.length);
            int rimwe = Integer.parseInt(sp[0]);
            int kabiri = Integer.parseInt(sp[1]);

            if (10 > rimwe || 8 > kabiri) {
                JOptionPane.showMessageDialog(null, " CALL ISHYIGA SUPPORT (TEL: 252 600 493) \n DERBY VERSION  " + version, " DERBY VERSION PROBLEM ", JOptionPane.PLAIN_MESSAGE);
//  System.exit(1);
                return false;
            }
        } catch (Throwable e) {
            JOptionPane.showMessageDialog(null, " CALL ISHYIGA SUPPORT (TEL: 252 600 493)\n DERBY NOT VERSION  " + version, " DERBY VERSION PROBLEM ", JOptionPane.PLAIN_MESSAGE);
            return false;
        }
        return true;
    }

    public Image getImage(String path) {
        try {
            System.out.println("PATH:" + path);
            return ImageIO.read(new File(path));
        } catch (IOException ex) {
            Logger.getLogger(Map.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    String getValue(String s, String mod) {
        String ret = " ";
        Variable var = getParam("PRINT", mod, s + mod);
        if (var != null) {
            ret = var.value;
        }
        return ret;
    }

    public static void backUpDatabase(Connection conn, String file) throws SQLException {
        // Get today's date as a string:
        java.text.SimpleDateFormat todaysDate = new java.text.SimpleDateFormat("yyyy-MM-dd");
        int f = (int) (5 * Math.random());
        String backupdirectory = file + f;

        CallableStatement cs = conn.prepareCall("CALL SYSCS_UTIL.SYSCS_BACKUP_DATABASE(?)");

        cs.setString(1, backupdirectory);
        cs.execute();
        cs.close();
    }

    public int getQuantitePerime2(int pro, String depotSql) {
        String p = (" QTY****LOT****EXP") + "\n " + "------------------------------ ";
        int id = 0;
        int qtyPerime = 0, qtyTotal = 0;
        String dateExp, lot;
        try {

            String sql = "select  qty_live,id_lots,date_exp from APP.lot where id_product= " + pro
                    + "  " + depotSql;
            System.out.println(" QTY****LOT****EXP" + sql);
            outResult = s.executeQuery(sql);
            while (outResult.next()) {
                id = outResult.getInt(1);
                lot = outResult.getString(2);
                dateExp = outResult.getString(3);
                qtyTotal = qtyTotal + id;
                if (Integer.parseInt(gucurika(datez)) > Integer.parseInt(gucurika(dateExp))) {
                    qtyPerime = qtyPerime + id;
                    p = p + ("\n" + id + "**" + lot + "**" + dateExp);
                }
            }
            if (qtyPerime > 0) {
                p = " TOTAL: " + qtyTotal + " PERIME:" + qtyPerime + "\n " + p;
                JOptionPane.showMessageDialog(null, "" + p,
                        l(lang,"V_PRODUCTEXP") + pro, JOptionPane.INFORMATION_MESSAGE);
                return -1;
            }
        } catch (Throwable e) {
            insertError(e + "", "getQuantite");
        }
        return (qtyTotal - qtyPerime);
    }

    public int getSum(String aff, String col) {

        int id = 0;
        try {
            outResult = s.executeQuery("select  sum(" + col + ") from APP.client where num_affiliation='" + aff + "'");
            while (outResult.next()) {
                id = outResult.getInt(1);

            }
        } catch (Throwable e) {
            insertError(e + "", "getQuantite");
        }

        return id;
    }

    public int getQuantiteV7(String code) {

        int id = 0;
        try {
            outResult = s.executeQuery("select  sum(qty_live) from APP.lot where code_lot='" + code + "'");
            while (outResult.next()) {
                id = outResult.getInt(1);

            }
        } catch (Throwable e) {
            insertError(e + "", "getQuantiteV7");
        }

        return id;
    }

    public static String gucurika(String exp) {
        int n = exp.length();
        System.out.println(n);
        String icuritse = "";
        for (int i = n - 1; i > 0; i--) {
            icuritse = icuritse + exp.substring(i - 1, i + 1);
            i = i - 1;
        }
        System.out.println(icuritse);
        return icuritse;
    }

    String[] clients() {
        String[] control = null;
        LinkedList<String> clien = new LinkedList();
        try {
            outResult = s.executeQuery("select NOM_VARIABLE from APP.VARIABLES WHERE FAMILLE_VARIABLE='CLIENT' order by NOM_VARIABLE ");

            while (outResult.next()) {
                clien.add(outResult.getString(1));
            }
    //  Close the resultSet
            outResult.close();

        } catch (Throwable e) {

            insertError(e + "", " V getlot");
        }
        control = new String[clien.size()];
        for (int i = 0; i < clien.size(); i++) {
            control[i] = clien.get(i);
        }
        return control;

    }

    public double getTotal(int id_invoice) {
        double tot = 0;
        try {
            System.out.println("11");

            outResult = s.executeQuery("select  total from APP.INVOICE where id_invoice=" + id_invoice + "  ");
            System.out.println("select  total from APP.INVOICE where id_invoice=" + id_invoice + " ");

            while (outResult.next()) {
                tot = outResult.getDouble(1);
            }
        } catch (Throwable e) {
            insertError(e + "", "getfacture");
        }
        return tot;
    }

    public int refundYose(String nom, String Debut, String FIN) {
        int sum = 0;
        try {
            String sql="select  SUM(APP.REFUND.CASH) from APP.refund, APP.INVOICE where "
                    + " APP.REFUND.HEURE >'" + Debut + "' AND  APP.REFUND.HEURE <'" + FIN + "' "
                    + " AND APP.INVOICE.ID_INVOICE = APP.REFUND.ID_INVOICE AND APP.INVOICE.EMPLOYE = '" + nom + "'";
             System.out.println(sql);
             outResult = s.executeQuery(sql); 

            while (outResult.next()) {
                return outResult.getInt(1);
            }
        } catch (Throwable e) {
            insertError(e + "", "CashierYose");
        }
        return sum;
    }

    public int CashierYose(String nom, String Debut, String FIN) {
        int sum = 0;
        int id = 0;
        try {
            System.out.println("11");
            outResult = s.executeQuery("select  total from APP.INVOICE where num_client='CASHNORM' and invoice.HEURE >'" + Debut + "' AND invoice.HEURE <'" + FIN + "' AND EMPLOYE='" + nom + "'"); // AND COMPTABILISE!='ANNULE' 
            System.out.println("select  total from APP.INVOICE where num_client='CASHNORM' and invoice.HEURE >'" + Debut + "' AND invoice.HEURE <'" + FIN + "' AND EMPLOYE='" + nom + "'   ");

            while (outResult.next()) {
                sum = sum + outResult.getInt(1);
            }
            LinkedList<List> inv = new LinkedList();
            LinkedList<List> cre = new LinkedList();
            //inv.add(  new List (0,0,0,"",""));
            System.out.println("22:" + sum);
            outResult = s.executeQuery("select  id_invoice,total,num_client from APP.INVOICE  where num_client!='CASHNORM' and invoice.HEURE >'" + Debut + "' AND invoice.HEURE <'" + FIN + "' AND EMPLOYE='" + nom + "'  order by id_invoice");
            System.out.println("select  id_invoice,total,num_client from APP.INVOICE  where num_client!='CASHNORM' and invoice.HEURE >'" + Debut + "' AND invoice.HEURE <'" + FIN + "' AND EMPLOYE='" + nom + "'  order by id_invoice");

            while (outResult.next()) {
                id = outResult.getInt(1);
                inv.add(new List(id, 0, 0, "", ""));
            }
            System.out.println("size:" + inv.size());
            if (inv.size() == 0) {
                inv.add(new List(0, 0, 0, "", ""));
            }

            System.out.println("33:" + (inv.getFirst().id_invoice - 1) + "**" + (inv.getLast().id_invoice + 1));

            outResult = s.executeQuery("select  id_invoice,payed from APP.CREDIT where  id_invoice>" + (inv.getFirst().id_invoice - 1) + " and  id_invoice<" + (inv.getLast().id_invoice + 1) + " order by id_invoice");
            System.out.println("select  id_invoice,payed from APP.CREDIT where  id_invoice>" + (inv.getFirst().id_invoice - 1) + " and  id_invoice<" + (inv.getLast().id_invoice + 1) + " order by id_invoice");

            while (outResult.next()) {
                id = outResult.getInt(1);
                cre.add(new List(id, outResult.getDouble(2), 0, "", ""));
            }

            System.out.println("invoice:" + inv.size() + "credi:" + cre.size());

            for (int i = 0; i < inv.size(); i++) {
                //System.out.println(i+":QWE:");
                for (int j = i; j < cre.size(); j++) {
                    //System.err.println("invoice:"+inv.get(i).id_invoice+"credi:"+cre.get(j).id_invoice+"MONTA:"+cre.get(j).price);
                    if (inv.get(i).id_invoice == cre.get(j).id_invoice) {
                        System.err.println("invoice:" + inv.get(i).id_invoice + "credi:" + cre.get(j).id_invoice + "MONTA:" + cre.get(j).price);
                        sum = sum + (int) cre.get(j).price;
                    }
                }
            }
        } catch (Throwable e) {
            insertError(e + "", "CashierYose");
        }
        return sum;
    }

    public String CashierBon(String nom, String Debut, String FIN) {
        String bons = "";
        try {
//        outResult = s.executeQuery("select  id_invoice,total,num_client,DATE from APP.INVOICE  where  invoice.HEURE >'"+Debut+"' AND invoice.HEURE <'"+FIN+"' AND EMPLOYE='"+nom+"' order by id_invoice");

            outResult = s.executeQuery("select  id_invoice,total,num_client from APP.INVOICE  where num_client!='CASHNORM' and invoice.HEURE >'" + Debut + "' AND invoice.HEURE <'" + FIN + "' AND EMPLOYE='" + nom + "' AND COMPTABILISE!='ANNULE' order by id_invoice");

            while (outResult.next()) {
                int id = outResult.getInt(1);
                int tot = outResult.getInt(2);
                String client = outResult.getString(3);
//String date=outResult.getString(4); 
//System.out.println(client);
                bons = bons + " \n " + id + "  " + client;

            }
        } catch (Throwable e) {
            insertError(e + "", "BonZose");
        }
        // System.out.println(bons); 
        return bons;
    }

    public String CashierTot(String nom, String Debut, String FIN) {
        String bons = "";
        try {
//        outResult = s.executeQuery("select  id_invoice,total,num_client from APP.INVOICE  where invoice.HEURE >'"+Debut+"' AND invoice.HEURE <'"+FIN+"' AND EMPLOYE='"+nom+"' order by id_invoice");

            outResult = s.executeQuery("select  id_invoice,total,num_client from APP.INVOICE  where num_client!='CASHNORM' and invoice.HEURE >'" + Debut + "' AND invoice.HEURE <'" + FIN + "' AND EMPLOYE='" + nom + "' AND COMPTABILISE!='ANNULE' order by id_invoice");
            while (outResult.next()) {
                int id = outResult.getInt(1);
                int tot = outResult.getInt(2);
                String client = outResult.getString(3);
//System.out.println(client);
                bons = bons + " \n " + "  " + tot;

            }
        } catch (Throwable e) {
            insertError(e + "", "BonZose");
        }
        //System.out.println(bons); 
        return bons;
    }

    void changePrice(int c, double p, double tva, String db, String name, String productName) {

        try {

            if (db.equals("CASHNORM")) {
                s.execute(" update APP.PRODUCT set PRIX =" + p + ", STATUS='OK' where ID_PRODUCT=" + c + " ");
            } else {
                s.execute(" update APP.PRODUCT set " + db + " =" + p + " , STATUS='OK' where ID_PRODUCT=" + c + "");
            }

//       if(c!=5)//PREPARATION MAGISTRALE
            insertChangePrice(name, productName, p);

        } catch (Throwable e) {
            insertError(e + "", "changePrice");
        }

    }

    void changePriceRama(int c, double p, String soc, String db, String name, String productName, String obs) {

        try {
            s.execute(" update APP.PRODUCT set PRIX_RAMA =" + p + ",OBSERVATION='" + obs + "' ,CODE_SOC_RAMA = '" + soc + "' where ID_PRODUCT=" + c + "");

            if (c != 5) {
                insertChangePrice(name, productName, p);
            }

            JOptionPane.showMessageDialog(null, db, " INPUT ", JOptionPane.PLAIN_MESSAGE);

        } catch (Throwable e) {
            insertError(e + "", "changePriceRama");
        }

    }

    void insertMissed(String desi, int qty, double price, int qty_dispo) {
        try {
            psInsert = conn.prepareStatement("insert into APP.MISSED(DESI,QUANTITE,PRICE,qty_dispo)  values (?,?,?,?)");

            psInsert.setString(1, desi);
            psInsert.setInt(2, qty);
            psInsert.setDouble(3, price);
            psInsert.setInt(4, qty_dispo);

            psInsert.executeUpdate();
            psInsert.close();
        } catch (SQLException ex) {
            insertError(ex + "", "insertMissed");
        }

    }

    public void insertError(String desi, String ERR) {
        try {
            psInsert = conn.prepareStatement("insert into APP.ERROR(DESI,USERNAME)  values (?,?)");

            if (desi.length() > 160) {
                desi = desi.substring(0, 155);
            }
            if (ERR.length() > 160) {
                ERR = ERR.substring(0, 155);
            }

            psInsert.setString(1, desi);
            psInsert.setString(2, ERR);
            psInsert.executeUpdate();
            psInsert.close();
            JOptionPane.showMessageDialog(null, "  " + desi, l(lang,"V_ERROR"), JOptionPane.PLAIN_MESSAGE);

        } catch (Throwable ex) {
            JOptionPane.showMessageDialog(null, "  " + ex, l(lang,"V_ERROR"), JOptionPane.PLAIN_MESSAGE);;
        }

    }

    void insertChangePrice(String USERNAME, String PRODUCTNAME, double PRICE) {
        try {
            psInsert = conn.prepareStatement("insert into APP.CHANGEMENT(PRICE,USERNAME,PRODUCTNAME)  values (?,?,?)");

            psInsert.setDouble(1, PRICE);
            psInsert.setString(2, USERNAME);
            psInsert.setString(3, PRODUCTNAME);
            psInsert.executeUpdate();
            psInsert.close();
        } catch (Throwable ex) {
            insertError(ex + "", "insertChangePrice");
        }

    }

    void insertInCredit(Credit cr) {
        try {
            psInsert = conn.prepareStatement("insert into APP.CREDIT(ID_INVOICE,NUMERO_AFFILIE,NUMERO_QUITANCE,SOLDE,PAYED,CAUSE,KEY_INVOICE)  values (?,?,?,?,?,?,?)");

            psInsert.setDouble(1, cr.ID_INVOICE);
            psInsert.setString(2, cr.NUMERO_AFFILIE);
            psInsert.setString(3, cr.NUMERO_QUITANCE);
            psInsert.setDouble(4, cr.SOLDE);
            psInsert.setDouble(5, cr.PAYED);
            psInsert.setString(6, "M");
            psInsert.setString(7, cr.DATE);

            psInsert.executeUpdate();
            psInsert.close();
        } catch (Throwable ex) {
            insertError(ex + "", "insertChangePrice");
        }

    }

    public void addOneLot(Lot l) {
        try {
            String query = "insert into APP.LOT (ID_PRODUCT,ID_LOT,ID_LOTS,DATE_EXP,QTY_START,QTY_LIVE,BON_LIVRAISON)  values (" + l.productCode + "," + l.id_lot + "," + l.id_LotS + ",?,?,?,?)";

            psInsert = conn.prepareStatement("insert into APP.LOT (ID_PRODUCT,ID_LOT,ID_LOTS,DATE_EXP,QTY_START,QTY_LIVE,BON_LIVRAISON)  values (?,?,?,?,?,?,?)");

            psInsert.setInt(1, l.productCode);
            psInsert.setInt(2, l.id_lot);
            psInsert.setString(3, l.id_LotS);
            psInsert.setString(4, l.dateExp);
            psInsert.setInt(5, l.qtyStart);
            psInsert.setInt(6, l.qtyLive);
            psInsert.setString(7, l.bon_livraison);
            System.out.println(psInsert.toString());
            psInsert.executeUpdate();
            psInsert.close();

        } catch (Throwable e) {
            insertError(e + "", "addOneLot");
        }

    }

    void upDatePr(Product p) {
        Statement sU;
        try {

            sU = conn.createStatement();
            String createString1 = " update APP.list set prix_revient=" + p.prix_revient + " where id_invoice> 35555 and code_uni='" + p.code + "'";
            sU.execute(createString1);
        } catch (SQLException ex) {
            Logger.getLogger(Db.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public LinkedList doworkOther(String s1, String sqlFamille) {
        try {
            observa = s1;
            System.out.println(s1);

            if (s1.contains("RAMA")) {
                System.out.println("select id_product,name_product,code," + s1 + ", tva,code_bar,observation,"
                        + "code_soc_RAMA,prix_revient,LAST_STORED_BUP,DOSAGE,INTERACTION from PRODUCT  where status='OK' " + sqlFamille + " order by NAME_PRODUCT");

                outResult = s.executeQuery("select id_product,name_product,code," + s1 + ", tva,code_bar,observation,"
                        + "code_soc_RAMA,prix_revient,LAST_STORED_BUP,DOSAGE,INTERACTION,DESIGNATION_FRSS from PRODUCT  where status='OK' and prix_rama>0 " + sqlFamille + " order by NAME_PRODUCT");

                allProduct = new LinkedList();
                //  Loop through the ResultSet and print the data
                while (outResult.next()) {
                    int productCode = outResult.getInt(1);
                    String productName = outResult.getString(2);
                    String code = outResult.getString(3);
                    double currentPrice = outResult.getDouble(4);
                    double tva = outResult.getDouble(5);
                    String codeBar = outResult.getString(6);
                    String observation = outResult.getString(7);
                    String codeSoc = outResult.getString(8);
                    double pr = outResult.getDouble(9);

                    Product p = new Product(productCode, productName, code, 1, currentPrice, tva, codeBar, observation, codeSoc, this, pr);
                    Date last_stored_bup = new Date(outResult.getTimestamp(10).getYear(),
                            outResult.getTimestamp(10).getMonth(),
                            outResult.getTimestamp(10).getDate());
                    p.setAvailability(last_stored_bup);
                    p.DOSAGE = outResult.getString(11);
                    p.INTERACTION = outResult.getString(12);
                    p.secondName = outResult.getString(13);
                    if (p.currentPrice > 0) {
                        allProduct.add(p);
                    }
                }
                //  Close the resultSet
                outResult.close();
            } else {
                outResult = s.executeQuery("select id_product,name_product,code," + s1 + ", tva,code_bar,observation,"
                        + "prix_revient,LAST_STORED_BUP,DOSAGE,INTERACTION from APP.PRODUCT where status='OK'  " + sqlFamille + " order by NAME_PRODUCT");
                System.out.println("select id_product,name_product,code," + s1 + ", tva,code_bar,observation,"
                        + "prix_revient,LAST_STORED_BUP,DOSAGE,INTERACTION from APP.PRODUCT where status='OK'  " + sqlFamille + " order by NAME_PRODUCT");

                allProduct = new LinkedList();
                //  Loop through the ResultSet and print the data
                while (outResult.next()) {
                    int productCode = outResult.getInt(1);
                    String productName = outResult.getString(2);
                    String code = outResult.getString(3);
                    double currentPrice = outResult.getDouble(4);
                    double tva = outResult.getDouble(5);
                    String codeBar = outResult.getString(6);
                    String observation = outResult.getString(7);
                    // String codeSoc  =  outResult.getString(8);
                    double pr = outResult.getDouble(8);

                    Product p = new Product(productCode, productName, code, 1, currentPrice, tva, codeBar, observation, code, this, pr);
//System.out.println("S1:"+s1+"***"+p.currentPrice);
                    Date last_stored_bup = new Date(outResult.getTimestamp(9).getYear(),
                            outResult.getTimestamp(9).getMonth(),
                            outResult.getTimestamp(9).getDate());

                    p.setAvailability(last_stored_bup);

                    p.DOSAGE = outResult.getString(10);
                    p.INTERACTION = outResult.getString(11);

                    if (p.currentPrice > 0) {
                        allProduct.add(p);
                    }
                }
                //  Close the resultSet
                outResult.close();
            }

            //  Beginning of the primary catch block: uses errorPrint method
        } catch (Throwable e) {
            insertError(e + "", "doworkOther");
        }

        return allProduct;
    }

    
    public LinkedList<Product> doworkOtherAll2(String s1) {

        try {
            observa = s1;
            System.out.println(s1);

            if (s1.contains("RAMA")) {
                allProductall = new LinkedList();
                String sql = "select id_product,DESIGNATION_FRSS,code," + s1 + ", tva,code_bar,observation,"
                        + "code_soc_RAMA,prix_revient,LAST_STORED_BUP,DOSAGE,INTERACTION,name_product from PRODUCT  "
                        + "  order by DESIGNATION_FRSS";

                System.out.println(sql);

                outResult = s.executeQuery(sql);

                //  Loop through the ResultSet and print the data
                while (outResult.next()) {
                    int productCode = outResult.getInt(1);
                    String productName = outResult.getString(2)+" | "+outResult.getString(13);
                    String code = outResult.getString(3);
                    double currentPrice = outResult.getDouble(4);
                    double tva = outResult.getDouble(5);
                    String codeBar = outResult.getString(6);
                    String observation = outResult.getString(7);
                    String codeSoc = outResult.getString(8);
                    double pr = outResult.getDouble(9);

                    Product p = new Product(productCode, productName, code, 1, currentPrice, tva, codeBar, observation, codeSoc, this, pr);
                    Date last_stored_bup = new Date(outResult.getTimestamp(10).getYear(),
                            outResult.getTimestamp(10).getMonth(),
                            outResult.getTimestamp(10).getDate());
                    p.setAvailability(last_stored_bup);
                    p.DOSAGE = outResult.getString(11);
                    p.INTERACTION = outResult.getString(12); 
                    allProductall.add(p);
                }
                //  Close the resultSet
                outResult.close();
            } else {
                outResult = s.executeQuery("select id_product,DESIGNATION_FRSS,code," + s1 + ", tva,code_bar,observation,"
                        + "prix_revient,LAST_STORED_BUP,DOSAGE,INTERACTION,name_product from APP.PRODUCT   order by DESIGNATION_FRSS");
                System.out.println("select id_product,name_product,code," + s1 + ", tva,code_bar,observation,"
                        + "prix_revient,LAST_STORED_BUP,DOSAGE,INTERACTION from APP.PRODUCT   order by NAME_PRODUCT");
                allProductall = new LinkedList();
                //  Loop through the ResultSet and print the data
                while (outResult.next()) {
                    int productCode = outResult.getInt(1);
                    String productName = outResult.getString(2)+" | "+outResult.getString(12);
                    String code = outResult.getString(3);
                    double currentPrice = outResult.getDouble(4);
                    double tva = outResult.getDouble(5);
                    String codeBar = outResult.getString(6);
                    String observation = outResult.getString(7);
                    // String codeSoc  =  outResult.getString(8);
                    double pr = outResult.getDouble(8);

                    Product p = new Product(productCode, productName, code, 1, currentPrice, tva, codeBar, observation, code, this, pr);
//System.out.println("S1:"+s1+"***"+p.currentPrice);
                    Date last_stored_bup = new Date(outResult.getTimestamp(9).getYear(),
                            outResult.getTimestamp(9).getMonth(),
                            outResult.getTimestamp(9).getDate());

                    p.setAvailability(last_stored_bup);

                    p.DOSAGE = outResult.getString(10);
                    p.INTERACTION = outResult.getString(11);

                    allProductall.add(p);
                }
                //  Close the resultSet
                outResult.close();
            }

            //  Beginning of the primary catch block: uses errorPrint method
        } catch (Throwable e) {
            insertError(e + "", "doworkOther");
        }

        return allProduct;
    }
    public LinkedList<Product> doworkOtherAll(String s1) {

        try {
            observa = s1;
            System.out.println(s1);

            if (s1.contains("RAMA")) {
                allProductall = new LinkedList();
                String sql = "select id_product,name_product,code," + s1 + ", tva,code_bar,observation,"
                        + "code_soc_RAMA,prix_revient,LAST_STORED_BUP,DOSAGE,INTERACTION from PRODUCT  "
                        + "  order by NAME_PRODUCT";

                System.out.println(sql);

                outResult = s.executeQuery(sql);

                //  Loop through the ResultSet and print the data
                while (outResult.next()) {
                    int productCode = outResult.getInt(1);
                    String productName = outResult.getString(2);
                    String code = outResult.getString(3);
                    double currentPrice = outResult.getDouble(4);
                    double tva = outResult.getDouble(5);
                    String codeBar = outResult.getString(6);
                    String observation = outResult.getString(7);
                    String codeSoc = outResult.getString(8);
                    double pr = outResult.getDouble(9);

                    Product p = new Product(productCode, productName, code, 1, currentPrice, tva, codeBar, observation, codeSoc, this, pr);
                    Date last_stored_bup = new Date(outResult.getTimestamp(10).getYear(),
                            outResult.getTimestamp(10).getMonth(),
                            outResult.getTimestamp(10).getDate());
                    p.setAvailability(last_stored_bup);
                    p.DOSAGE = outResult.getString(11);
                    p.INTERACTION = outResult.getString(12);

                    allProductall.add(p);
                }
                //  Close the resultSet
                outResult.close();
            } else {
                outResult = s.executeQuery("select id_product,name_product,code," + s1 + ", tva,code_bar,observation,"
                        + "prix_revient,LAST_STORED_BUP,DOSAGE,INTERACTION from APP.PRODUCT   order by NAME_PRODUCT");
                System.out.println("select id_product,name_product,code," + s1 + ", tva,code_bar,observation,"
                        + "prix_revient,LAST_STORED_BUP,DOSAGE,INTERACTION from APP.PRODUCT   order by NAME_PRODUCT");
                allProductall = new LinkedList();
                //  Loop through the ResultSet and print the data
                while (outResult.next()) {
                    int productCode = outResult.getInt(1);
                    String productName = outResult.getString(2);
                    String code = outResult.getString(3);
                    double currentPrice = outResult.getDouble(4);
                    double tva = outResult.getDouble(5);
                    String codeBar = outResult.getString(6);
                    String observation = outResult.getString(7);
                    // String codeSoc  =  outResult.getString(8);
                    double pr = outResult.getDouble(8);

                    Product p = new Product(productCode, productName, code, 1, currentPrice, tva, codeBar, observation, code, this, pr);
//System.out.println("S1:"+s1+"***"+p.currentPrice);
                    Date last_stored_bup = new Date(outResult.getTimestamp(9).getYear(),
                            outResult.getTimestamp(9).getMonth(),
                            outResult.getTimestamp(9).getDate());

                    p.setAvailability(last_stored_bup);

                    p.DOSAGE = outResult.getString(10);
                    p.INTERACTION = outResult.getString(11);

                    allProductall.add(p);
                }
                //  Close the resultSet
                outResult.close();
            }

            //  Beginning of the primary catch block: uses errorPrint method
        } catch (Throwable e) {
            insertError(e + "", "doworkOther");
        }

        return allProduct;
    }

    public String doworkOtherMobile() {
        String resultat = "";
        try {
            String sql = "select APP.PRODUCT.ID_PRODUCT,name_product,prix,PRIX_REVIENT,sum(QTY_LIVE),FAMILLE,code  "
                    + " from APP.PRODUCT,APP.LOT WHERE APP.LOT.ID_PRODUCT=APP.PRODUCT.ID_PRODUCT   "
                    + "group by APP.PRODUCT.ID_PRODUCT,name_product,prix,PRIX_REVIENT,FAMILLE,code";
            System.out.println(sql);
            outResult = s.executeQuery(sql);

            while (outResult.next()) {
                Product p = new Product(outResult.getInt(1), outResult.getString("name_product"),
                        outResult.getDouble("PRIX_REVIENT"),
                        outResult.getDouble("prix"), outResult.getInt(5), outResult.getString("FAMILLE"));
                if (p.currentPrice > 0) {

                    p.code = outResult.getString("code");
                    //CUIEMINCÉ96;Emincé de Porc au Soya.;6400.0;0.0;CUISINE;null;210.0;250
                    resultat += p.mobiletxt() + "#";
                }
            }
            //  Close the resultSet
            outResult.close();
            sql = "select * from APP.CPT_COMPTE";
            System.out.println(sql);
            /*outResult = s.executeQuery(sql);
 
             while (outResult.next())
             {   
             Product p =new Product(
             Integer.parseInt(outResult.getString("NUM_COMPTE")),
             outResult.getString("NOM_COMPTE"),
             Integer.parseInt(outResult.getString("NUM_COMPTE")), 
             Integer.parseInt(outResult.getString("NUM_COMPTE")),outResult.getInt("CLASSE"), "COMPTA" ) ; 
             p.code=outResult.getString(outResult.getString("NUM_COMPTE"));
             resultat+=p.mobiletxt()+"#"; 
             } 
             //  Close the resultSet
             outResult.close(); 
             } 
             resultat+=(new Product(5000, "SPECIAL", 0,0,0 ," " )).mobiletxt();
             */

            //  Beginning of the primary catch block: uses errorPrint method
        } catch (Throwable e) {
            insertError(e + "", "doworkOtherMobile");
        }

        return resultat;
    }

    public String getEmploye(String id, String carte) {
        try {

            System.out.println("select NOM_EMPLOYE from APP.EMPLOYE "
                    + " WHERE ID_EMPLOYE=" + id + " and CATRE_INDETITE =" + carte);
            outResult = s.executeQuery("select NOM_EMPLOYE from APP.EMPLOYE "
                    + " WHERE ID_EMPLOYE=" + id + " and CATRE_INDETITE =" + carte);

            //  Loop through the ResultSet and print the data
            while (outResult.next()) {
                return outResult.getString(1);

            }

            //  Close the resultSet
            outResult.close();

            //  Beginning of the primary catch block: uses errorPrint method
        } catch (Throwable e) {
            insertError(e + "", "employe");
        }

        return "NOONE";

    }

    public Product getProduct(String codee, String sqlflle, String s1) {

        Product p = null;
        try {

            outResult = s.executeQuery("select id_product,name_product,code," + s1 + "tva,code_bar,observation,"
                    + "prix_revient,LAST_STORED_BUP,DOSAGE,INTERACTION from APP.PRODUCT where code='" + codee + "' " + sqlflle + " order by NAME_PRODUCT");

            System.out.println("select id_product,name_product,code, tva,code_bar,observation,"
                    + "prix_revient,LAST_STORED_BUP,DOSAGE,INTERACTION from APP.PRODUCT where code='" + codee + "' " + sqlflle + " order by NAME_PRODUCT");

            while (outResult.next()) {
                int productCode = outResult.getInt(1);
                String productName = outResult.getString(2);
                String code = outResult.getString(3);
                double currentPrice = outResult.getDouble(4);
                double tva = outResult.getDouble(5);
                String codeBar = outResult.getString(6);
                String observation = outResult.getString(7);
                // String codeSoc  =  outResult.getString(8);
                double pr = outResult.getDouble(8);

                p = new Product(productCode, productName, code, 1, currentPrice, tva, codeBar, observation, code, this, pr);
//System.out.println("S1:"+s1+"***"+p.currentPrice);
                Date last_stored_bup = new Date(outResult.getTimestamp(9).getYear(),
                        outResult.getTimestamp(9).getMonth(),
                        outResult.getTimestamp(9).getDate());

                p.setAvailability(last_stored_bup);

                p.DOSAGE = outResult.getString(10);
                p.INTERACTION = outResult.getString(11);

                //  Close the resultSet
//    outResult.close();
            }
        } //  Beginning of the primary catch block: uses errorPrint method
        catch (Throwable e) {
            insertError(e + "", "doworkOther");
        }
        return p;

    }

    public Product getProduct2(String codee, double qty, double price) {
        Product p = null;
        try {

            outResult = s.executeQuery("select id_product,name_product,prix,tva,code,observation,"
                    + "prix_revient from APP.PRODUCT where code='" + codee + "'   ");
            System.out.println("select id_product,name_product,prix,tva,code,observation,"
                    + "prix_revient from APP.PRODUCT where code='" + codee + "'   ");
            while (outResult.next()) {
                int productCode = outResult.getInt(1);
                String productName = outResult.getString(2);
                double currentPrice = outResult.getDouble(3);
                double tva = outResult.getDouble(4);
                String code = outResult.getString(5);
                String observation = outResult.getString(6);
                double pr = outResult.getDouble(7);
                System.out.println(qty + "---------1----------");

                if (price == 0) {
                    price = currentPrice;
                }
                p = new Product(productCode, productName, code, qty, price, tva, codeBar, observation, code,
                        this, pr);
                p.qty = (int) qty;
            }
        } //  Beginning of the primary catch block: uses errorPrint method
        catch (Throwable e) {
            insertError(e + "", "doworkOther");
        }
        return p;

    }

    public String getProductDesi(String codee) {

        Product p = null;
        try {

            outResult = s.executeQuery("select  name_product  from APP.PRODUCT where code='" + codee + "'  ");

            while (outResult.next()) {
                return (outResult.getString(1));
            }
            outResult.close();
        } //  Beginning of the primary catch block: uses errorPrint method
        catch (Throwable e) {
            insertError(e + "", "doworkOther");
        }
        return "";

    }

    public Invoice[] printByChoice(String choix, String rtype) {
        LinkedList<Invoice> facture = new LinkedList();
        try {
            if (choix.equals("PAR GROUPE")) {
                String[] control = clients();
                String client = (String) JOptionPane.showInputDialog(null, "Fait votre choix", "Impression ",
                        JOptionPane.QUESTION_MESSAGE, null, control, "");                
                String Debut = JOptionPane.showInputDialog("ENTREZ LE TEMPS du debut", startTime());
                String FIN = JOptionPane.showInputDialog("ENTREZ LE TEMPS de la fin ", EndTime());

                String sql = "select * from APP.INVOICE where invoice.num_client='" + client + "' AND "
                        + "COMPTABILISE!='ANNULE'  and invoice.HEURE >'" + Debut + "' AND invoice.HEURE <'" + FIN +"' AND "
                        + "  (external_data like '%," + rtype + ",%' or copy_data like '%," + rtype + ",%' ) ORDER BY HEURE ";
                if (rtype.contains("R")) {
                    sql = "select  id_refund, num_client,date,total, employe,heure,0,0,cash,credit   from "
                            + " APP.REFUND    where  num_client='" + client + "' AND REFUND.HEURE >'" + Debut + "' AND"
                            + " REFUND.HEURE <'" + FIN + "'   and (external_data like '%," + rtype + ",%' or copy_data like '%," + rtype + ",%' )  ";
                }
                System.out.println(sql);
                outResult = s.executeQuery(sql);

                while (outResult.next()) {
                    int id_invoice = outResult.getInt(1);
                    String clientw = outResult.getString(2);
                    String z = outResult.getString(3);
                    double total = outResult.getDouble(4);
                    String id_employe = outResult.getString(5);
                    String time = "" + outResult.getTimestamp(6);
                    double tva = outResult.getDouble(7);
                    double reduction = outResult.getDouble(8);
                    double cash = outResult.getDouble("cash");
                    double credit = outResult.getDouble("credit");
                    Invoice inv = new Invoice(id_invoice, id_employe, z, clientw, total, time, tva, reduction);
                    inv.CASH = cash;
                    inv.CREDIT = credit;
                    facture.add(inv);
                }
            }
            if (choix.equals("PAR HEURE")) {
                String Debut = JOptionPane.showInputDialog("ENTREZ LE TEMPS du debut", startTime());
                String FIN = JOptionPane.showInputDialog("ENTREZ LE TEMPS de la fin ", EndTime());

                outResult = s.executeQuery("select * from APP.INVOICE where  invoice.HEURE >'" + Debut + "' AND invoice.HEURE <'" + FIN + " ' AND COMPTABILISE!='ANNULE' ORDER BY HEURE ");
                while (outResult.next()) {
                    int id_invoice = outResult.getInt(1);
                    String clientw = outResult.getString(2);
                    String z = outResult.getString(3);
                    double total = outResult.getDouble(4);
                    String id_employe = outResult.getString(5);
                    String time = "" + outResult.getTimestamp(6);
                    double tva = outResult.getDouble(7);
                    double reduction = outResult.getDouble(9);
                    double cash = outResult.getDouble("cash");
                    double credit = outResult.getDouble("credit");
                    facture.add(new Invoice(id_invoice, id_employe, z, clientw, total, time, tva, reduction));
                }
            }
            outResult.close();
        } catch (Throwable e) {
            /*  Catch all exceptions and pass them to
             **  the exception reporting method             */
            System.out.println(" . . inv . exception thrown:");
            insertError(e + "", " V reimpression facture");
        }

        Invoice[] factureArray = new Invoice[facture.size()];

        for (int i = 0; i < factureArray.length; i++) {
            factureArray[i] = facture.get(i);
        }

        return factureArray;
    }

    public Invoice[] printByHeure(String choix) {
        LinkedList<Invoice> facture = new LinkedList();
        try {
            if (choix.equals("PAR HEURE")) {
                String Debut = JOptionPane.showInputDialog("ENTREZ LE TEMPS du debut", startTime());
                String FIN = JOptionPane.showInputDialog("ENTREZ LE TEMPS de la fin ", EndTime());

                outResult = s.executeQuery("select * from APP.INVOICE where  invoice.HEURE >'" + Debut + "' AND invoice.HEURE <'" + FIN + " ' AND COMPTABILISE!='ANNULE' AND NUM_CLIENT IS NULL ORDER BY HEURE ");
                while (outResult.next()) {
                    int id_invoice = outResult.getInt(1);
                    String clientw = outResult.getString(2);
                    String z = outResult.getString(3);
                    double total = outResult.getDouble(4);
                    String id_employe = outResult.getString(5);
                    String time = "" + outResult.getTimestamp(6);
                    double tva = outResult.getDouble(7);
                    double reduction = outResult.getDouble(9);
                    double cash = outResult.getDouble("cash");
                    double credit = outResult.getDouble("credit");
                    facture.add(new Invoice(id_invoice, id_employe, z, clientw, total, time, tva, reduction));
                }
            }
            outResult.close();
        } catch (Throwable e) {
            /*  Catch all exceptions and pass them to
             **  the exception reporting method             */
            System.out.println(" . . inv . exception thrown:");
            insertError(e + "", " V reimpression facture");
        }

        Invoice[] factureArray = new Invoice[facture.size()];

        for (int i = 0; i < factureArray.length; i++) {
            factureArray[i] = facture.get(i);
        }

        return factureArray;
    }

    public Proforma[] Proformat(String choix) {
        LinkedList<Proforma> facture = new LinkedList();

        try {
            outResult = s.executeQuery("select * from APP.bon_proforma where  (NOM_CLIENT LIKE'%" + choix + "%' "
                    + " or REFERENCE LIKE'%" + choix + "%' or NUM_CLIENT LIKE'%" + choix.toUpperCase() + "%') "
                    + " and STATUS='OPEN' ORDER BY HEURE ");

            System.out.println("select * from APP.bon_proforma where  (NOM_CLIENT LIKE'%" + choix + "%' "
                    + " or REFERENCE LIKE'%" + choix + "%' or NUM_CLIENT LIKE'%" + choix.toUpperCase() + "%') "
                    + " and STATUS='OPEN' ORDER BY HEURE ");

            while (outResult.next()) {
                int id_proforma = outResult.getInt(1);
                String num_client = outResult.getString(2);
                String nom_client = outResult.getString(3);
                String date = outResult.getString(4);
                double total = outResult.getDouble(5);
                String id_employe = outResult.getString(6);
                String time = "" + outResult.getTimestamp(7);
                double tva = outResult.getDouble(8);
                double reduction = outResult.getDouble(9);
                String ref = outResult.getString(11);
                String keyinvoice = outResult.getString(10);
                facture.add(new Proforma(id_proforma, num_client, nom_client, date, total, reduction, tva, id_employe, time, ref, keyinvoice));
            }
            outResult.close();
        } catch (Throwable e) {
            System.out.println(" . . inv . exception thrown:");
            insertError(e + "", " V getproforma");
        }

        Proforma[] factureArray = new Proforma[facture.size()];

        for (int i = 0; i < factureArray.length; i++) {
            factureArray[i] = facture.get(i);
        }

        return factureArray;
    }

    public Proforma getProformaExtKey(String ExtKey) {
        Proforma profo = null;

        try {
            String sql = "select * from APP.bon_proforma where  EXT_KEY='" + ExtKey + "' ";
            outResult = s.executeQuery(sql);
            System.out.println(sql);

            while (outResult.next()) {
                int id_proforma = outResult.getInt(1);
                String num_client = outResult.getString(2);
                String nom_client = outResult.getString(3);
                String date = outResult.getString(4);
                double total = outResult.getDouble(5);
                String id_employe = outResult.getString(6);
                String time = "" + outResult.getTimestamp(7);
                double tva = outResult.getDouble(8);
                double reduction = outResult.getDouble(9);
                String ref = outResult.getString(11);
                String keyinvoice = outResult.getString(10);
                profo = (new Proforma(id_proforma, num_client, nom_client, date, total, reduction, tva, id_employe, time, ref, keyinvoice));
            }
            outResult.close();
        } catch (Throwable e) {
            System.out.println(" . . inv . exception thrown:");
            insertError(e + "", " V getproforma");
        }

        return profo;
    }

    public void updateProformat(LinkedList<Proforma> currentOpenProforma, String status) {
        try {
            for (int i = 0; i < currentOpenProforma.size(); i++) {
                Proforma ret = currentOpenProforma.get(i);
                System.out.println(" update APP.bon_proforma set transformed= current timestamp, STATUS='" + status + "'  where "
                        + " id_proforma=" + ret.id_Proforma + "");
                s.execute(" update APP.bon_proforma set transformed= current timestamp, STATUS='" + status + "'  where "
                        + " id_proforma=" + ret.id_Proforma + "");
            }
        } catch (Throwable e) {
            System.out.println(" . .updateProformat :");
            insertError(e + "", " V updateProformat");
        }

    }

    public LinkedList<List> listTva(String d, String f) {
        LinkedList<List> li = new LinkedList();
        try {
            outResult = s.executeQuery("SELECT APP.INVOICE.ID_INVOICE,CODE_UNI,QUANTITE,(APP.LIST.QUANTITE*APP.LIST.PRICE*APP.PRODUCT.TVA) FROM APP.INVOICE,APP.LIST,APP.PRODUCT WHERE  invoice.HEURE >'" + d + "' AND invoice.HEURE <'" + f + "' and APP.INVOICE.ID_INVOICE=APP.LIST.ID_INVOICE AND APP.LIST.CODE_UNI=APP.PRODUCT.CODE and product.TVA > 0");

            //  Loop through the ResultSet and print the data
            while (outResult.next()) {
                int id = outResult.getInt(1);
                String code = outResult.getString(2);
                int qte = outResult.getInt(3);
                double currentPrice = outResult.getDouble(4);
                li.add(new List(id, currentPrice, qte, "", code));
            }
            //  Close the resultSet
            outResult.close();

            //  Beginning of the primary catch block: uses errorPrint method
        } catch (Throwable e) {
            insertError(e + "  list tva ", "tva ");
        }

        return li;
    }

    public LinkedList<List> listProforma(int id_proforma) {
        LinkedList<List> li = new LinkedList();
        try {
            System.out.println("SELECT APP.LIST_PROFORMA.ID_PROFORMA,CODE_UNI,QUANTITE,APP.LIST_PROFORMA.PRICE,APP.PRODUCT.NAME_PRODUCT,PRODUCT.ID_PRODUCT,PRODUCT.TVA FROM APP.bon_proforma,APP.LIST_PROFORMA,APP.PRODUCT WHERE  APP.bon_proforma.ID_PROFORMA=APP.LIST_PROFORMA.ID_PROFORMA AND APP.LIST_PROFORMA.CODE_UNI=APP.PRODUCT.CODE and LIST_PROFORMA.ID_PROFORMA=" + id_proforma + " ");

            outResult = s.executeQuery("SELECT APP.LIST_PROFORMA.ID_PROFORMA,CODE_UNI,QUANTITE,APP.LIST_PROFORMA.PRICE,APP.PRODUCT.NAME_PRODUCT,PRODUCT.ID_PRODUCT,PRODUCT.TVA FROM APP.bon_proforma,APP.LIST_PROFORMA,APP.PRODUCT WHERE  APP.bon_proforma.ID_PROFORMA=APP.LIST_PROFORMA.ID_PROFORMA AND APP.LIST_PROFORMA.CODE_UNI=APP.PRODUCT.CODE and LIST_PROFORMA.ID_PROFORMA=" + id_proforma + " ");

            //  Loop through the ResultSet and print the data
            while (outResult.next()) {
                int id = outResult.getInt(1);
                String code = outResult.getString(2);
                int qte = outResult.getInt(3);
                double currentPrice = outResult.getDouble(4);
                String desi = outResult.getString(5);
                int id_pro = outResult.getInt(6);
                double tva = outResult.getDouble(7);
                li.add(new List(id, currentPrice, qte, desi, code, id_pro, tva));
            }
            //  Close the resultSet
            outResult.close();

            //  Beginning of the primary catch block: uses errorPrint method
        } catch (Throwable e) {
            insertError(e + " list_proforma ", "list proforma ");
        }

        return li;
    }

    public void listPrixErrone(String d, String f, String cli) {

        LinkedList<List> li = new LinkedList();
        try {
            String j = "_" + cli;
            if (cli.equals("")) {
                j = cli;
            }
            outResult = s.executeQuery("SELECT APP.INVOICE.ID_INVOICE,CODE_UNI,QUANTITE,prix" + j + ",price FROM APP.INVOICE,APP.LIST,product  where invoice.num_client='" + cli + "'  and invoice.id_invoice > " + d + " and invoice.id_invoice < " + f + " and invoice.id_invoice=list.id_invoice AND APP.LIST.CODE_UNI=APP.PRODUCT.CODE");

            //  Loop through the ResultSet and print the data
            while (outResult.next()) {
                int id = outResult.getInt(1);
                String code = outResult.getString(2);
                int qte = outResult.getInt(3);
                double currentPrice = outResult.getDouble(4);
                li.add(new List(id, currentPrice, qte, "", code));
            }
            //  Close the resultSet
            outResult.close();

            for (int i = 0; i < li.size(); i++) {
                List p = li.get(i);

                System.out.println("update list set price=" + p.price + " where invoice=" + p.id_invoice + " and code_uni='" + p.code + "'");
                s.execute(" update list set price=" + p.price + " where id_invoice=" + p.id_invoice + " and code_uni='" + p.code + "'");
            }
        } catch (SQLException ex) {
            Logger.getLogger(Db.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void tva(String d, String f) {
        LinkedList<List> li = listTva(d, f);
        try {
            s.execute(" update APP.invoice set tva =0  WHERE  invoice.HEURE >'" + d + "' AND invoice.HEURE <'" + f + "'");
            for (int i = 0; i < li.size(); i++) {
                s.execute(" update APP.invoice set tva =tva+" + li.get(i).price + " where id_invoice=" + li.get(i).id_invoice);
            }
        } catch (Throwable e) {
            insertError(e + "tva", "fixtva");
        }
    }

    public boolean existe(String s1) {
        boolean done = false;
        try {
            outResult = s.executeQuery("select * from APP.PRODUCT where code='" + s1 + "'");

            while (outResult.next()) {
                done = true;
                JOptionPane.showMessageDialog(null, l(lang,"V_IRIMO"), l(lang,"V_MERCI"), JOptionPane.PLAIN_MESSAGE);

            }
            outResult.close();

        } catch (Throwable e) {
            insertError(e + "", "existe");
        }

        return done;
    }

    void insertTarif(Tarif fa) {
        try {

            psInsert = conn.prepareStatement("insert into APP.FAMILLE(ID_TARIF,DES_TARIF,VAR_TARIF,COEF_TARIF,R_TARIF,G_TARIF,B_TARIF,FONT_TARIF,OBSERVE,SMART)values(?,?,?,?,?,?,?,?,?,?)");

            psInsert.setString(1, fa.ID_TARIF);
            psInsert.setString(2, fa.DES_TARIF);
            psInsert.setString(3, fa.VAR_TARIF);
            psInsert.setDouble(4, fa.COEF_TARIF);
            psInsert.setInt(5, fa.r);
            psInsert.setInt(6, fa.g);
            psInsert.setInt(7, fa.b);
            psInsert.setString(8, fa.FONT_TARIF);
            psInsert.setString(9, fa.OBSERVE);
            psInsert.setString(10, fa.SMART);
            psInsert.executeUpdate();
            psInsert.close();
        } catch (SQLException ex) {
            insertError("" + ex, "insertTarif");
            System.out.println(fa.toString() + " " + ex);
        }
    }

    public void upDateTarif(Tarif fa) {
        try {

            String str = " update APP.TARIF set DES_TARIF='" + fa.DES_TARIF + "' ,VAR_TARIF='" + fa.VAR_TARIF + "' "
                    + " ,COEF_TARIF=" + fa.COEF_TARIF + " ,R_TARIF=" + fa.r + ",G_TARIF=" + fa.g + ",B_TARIF=" + fa.b + ""
                    + ",FONT_TARIF='" + fa.FONT_TARIF + "' ,OBSERVE='" + fa.OBSERVE + "' where ID_TARIF='" + fa.ID_TARIF + "'";

            s.execute(str);
            JOptionPane.showMessageDialog(null, l(lang,"V_UPDATE_REUSSI"), l(lang,"V_MERCI"), JOptionPane.INFORMATION_MESSAGE);

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "  " + ex, l(lang,"V_UPDATE_FAIL"), JOptionPane.ERROR_MESSAGE);
            insertError(ex + "", "updateTarif");
        }
    }

    void insertInsurance(Insurance izina) {//hano turinjizamo dukoresheje objet
        try {
            psInsert = conn.prepareStatement("insert into APP.VARIABLES(NOM_VARIABLE,VALUE_VARIABLE,FAMILLE_VARIABLE,VALUE2_VARIABLE)values(?,?,?,?)");//injizamo
//aha ama attribut ya objet yo agomba kwandikwa uko asanzwe ameze
            //nom_assurance,valeur_assurance,famille_assurance,valeur2_assurance;
            //nomAssurance,valeurAssurance,familleAssurance,valeur2Assurance
            psInsert.setString(1, izina.nomAssurance);
            psInsert.setString(2, izina.valeurAssurance);
            psInsert.setString(3, izina.familleAssurance);
            psInsert.setString(4, izina.valeur2Assurance);
            psInsert.executeUpdate();
            psInsert.close();
        } catch (SQLException ex) {
            System.out.println(" " + ex);
        }
        System.out.println("IKIBAZO CYAKIRIWE!!!!!!!!!!!!!");

    }

    public void UpdateDesignation() {
        try {

            /*    outResult = s.executeQuery("select id_product,name_product from PRODUCT order by NAME_PRODUCT");
             //
             allProduct= new LinkedList();
             //  Loop through the ResultSet and print the data
             while (outResult.next())
             {

             int code=outResult.getInt(1);
             String productName   =  outResult.getString(2);
             String sl=productName.replace('°', 'E');

             sl=productName.replace('°', 'e');
             Product p= new Product(code,sl, "", 1,0,0,"","","",this);
             System.out.println(sl);
             allProduct.add(p);


             }
             //  Close the resultSet
             outResult.close();*/

            /* for(int i=0;i<allProduct.size();i++)
             { System.out.println(allProduct.get(i));

             String productName=allProduct.get(i).productName;
             if(!productName.contains("'"))*/
            s.execute(" update APP.product set name_product ='Perdolan Bebe 12 Supp 1mg' where id_product=" + 1374);

    // }
            //  Beginning of the primary catch block: uses errorPrint method
        } catch (Throwable e) {
            insertError(e + "", "doworkOther");
        }

    }

    LinkedList<Product> doworkOtherData(String s1, String sql) {
        try {
            if (s1.contains("RAMA")) {
                System.out.println(s1);
                outResult = s.executeQuery("select id_product,name_product,code," + s1 + ", tva,code_bar,observation,code_soc_RAMA from APP.PRODUCT " + sql + " order by NAME_PRODUCT");
                System.out.println("select id_product,name_product,code," + s1 + ", tva,code_bar,observation,code_soc_RAMA from APP.PRODUCT " + sql + " order by NAME_PRODUCT");

                allProduct = new LinkedList();
                //  Loop through the ResultSet and print the data
                while (outResult.next()) {
                    int productCode = outResult.getInt(1);
                    String productName = outResult.getString(2);
                    String code = outResult.getString(3);
                    double currentPrice = outResult.getDouble(4);
                    double tva = outResult.getDouble(5);
                    String codeBar = outResult.getString(6);
                    String observation = outResult.getString(7);
                    String codeSoc = outResult.getString(8);
                    Product p = new Product(productCode, productName, code, 1, currentPrice, tva, codeBar, observation, codeSoc, this);
                    // if(p.currentPrice>0)
                    allProduct.add(p);

                    // System.out.println(p);
                }
                //  Close the resultSet
                outResult.close();
            } else {
                outResult = s.executeQuery("select id_product,name_product,code," + s1 + ", tva,code_bar,observation from APP.PRODUCT " + sql + "  order by NAME_PRODUCT");
                //  System.out.println(s1);
                allProduct = new LinkedList();
                //  Loop through the ResultSet and print the data
                while (outResult.next()) {
                    int productCode = outResult.getInt(1);
                    String productName = outResult.getString(2);
                    String code = outResult.getString(3);
                    double currentPrice = outResult.getDouble(4);
                    double tva = outResult.getDouble(5);
                    String codeBar = outResult.getString(6);
                    String observation = outResult.getString(7);
                    // String codeSoc  =  outResult.getString(8);
                    Product p = new Product(productCode, productName, code, 1, currentPrice, tva, codeBar, observation, code, this);
                    //  if(p.currentPrice>0)
                    allProduct.add(p);
                }
                //  Close the resultSet
                outResult.close();
            }

            //  Beginning of the primary catch block: uses errorPrint method
        } catch (Throwable e) {
            insertError(e + "", "doworkOther");
        }

        return allProduct;
    }

    Product makeProduct(String ligne, int i) {
        StringTokenizer st1 = new StringTokenizer(ligne);

        //recuperer le nom
        String productName = st1.nextToken(";");
        //recupere code
        String code = st1.nextToken(";");
        // String d=st1.nextToken(";");

        StringTokenizer st2 = new StringTokenizer(code);
        String d = st2.nextToken("/");

       // System.out.println(code);
        Double in_int = new Double(d);
//int prix = in_int.intValue();
// Double in_int=new Double(d);
        double price = in_int.doubleValue();
        //recupere quantite

        return new Product(i, productName, code, 1, price, 0, code, "", "", this);

    }

    Product getProduct(int pr) {

        Product p = null;

        try {
            outResult = s.executeQuery("select id_product,name_product,code,prix, tva,code_bar,observation from APP.PRODUCT WHERE ID_PRODUCT = " + pr);

            //  Loop through the ResultSet and print the data
            while (outResult.next()) {

                int productCode = outResult.getInt(1);
                String productName = outResult.getString(2);
                String code = outResult.getString(3);
                double currentPrice = outResult.getDouble(4);
                double tva = outResult.getDouble(5);
                String codeBar = outResult.getString(6);
                String observation = outResult.getString(7);
                p = new Product(productCode, productName, code, 1, currentPrice, tva, codeBar, observation, code, this);

            }
            //  Close the resultSet
            outResult.close();

            //  Beginning of the primary catch block: uses errorPrint method
        } catch (Throwable e) {
            insertError(e + "", "getProduct");
        }

        return p;
    }

    LinkedList<Product> doworkVedette(LinkedList<Vedette> v, String s1) {
        LinkedList<Product> allProductVedette = new LinkedList();

        try {

            for (int i = 0; i < v.size(); i++) {
                Vedette ve = v.get(i);

                if (s1.contains("RAMA")) {
                    outResult = s.executeQuery("select id_product,name_product,code," + s1 + ", tva,code_bar,"
                            + "observation,code_soc_rama,prix_revient,INTERACTION"
                            + " from APP.PRODUCT WHERE ID_PRODUCT = " + ve.ID_PRODUCT + "order by NAME_PRODUCT");

                    allProduct = new LinkedList();
                    //  Loop through the ResultSet and print the data
                    while (outResult.next()) {
                        int productCode = outResult.getInt(1);
                        String productName = outResult.getString(2);

                        String code = outResult.getString(3);
                        double currentPrice = outResult.getDouble(4);
                        double tva = outResult.getDouble(5);
                        String codeBar = outResult.getString(6);
                        String observation = outResult.getString(7);
                        String codeSoc = outResult.getString(8);
                        double pr = outResult.getDouble(9);

                        Product p = new Product(productCode, productName, code, 1, currentPrice, tva, codeBar, observation, codeSoc, this, pr);
                        p.devise = devise;
                        p.qty = ve.qte;
                        p.INTERACTION = outResult.getString("INTERACTION");
                        // System.out.println("++++  "+ p.INTERACTION);
                        allProductVedette.add(p);
                    }
                    //  Close the resultSet
                    outResult.close();
                } else {
                    outResult = s.executeQuery("select id_product,name_product,code," + s1 + ", tva,code_bar"
                            + ",observation,prix_revient,INTERACTION from"
                            + " APP.PRODUCT WHERE ID_PRODUCT = " + ve.ID_PRODUCT + " order by NAME_PRODUCT");
                    allProduct = new LinkedList();
                    //  Loop through the ResultSet and print the data
                    while (outResult.next()) {
                        int productCode = outResult.getInt(1);
                        String productName = outResult.getString(2);
                        String code = outResult.getString(3);
                        double currentPrice = outResult.getDouble(4);
                        double tva = outResult.getDouble(5);
                        String codeBar = outResult.getString(6);
                        String observation = outResult.getString(7);
                        // String codeSoc  =  outResult.getString(8);
                        double pr = outResult.getDouble(8);

                        Product p = new Product(productCode, productName, code, 1, currentPrice, tva, codeBar, observation, code, this, pr);
                        p.qty = ve.qte;
                        p.devise = devise;
                        p.INTERACTION = outResult.getString("INTERACTION");
            //System.out.println("++++  "+ p.INTERACTION);
                        // if(p.currentPrice>0)
                        allProductVedette.add(p);
                    }
                    //  Close the resultSet
                    outResult.close();
                }

            }
        } catch (Throwable e) {
            insertError(e + "", "doworkVedette");
        }

        return allProductVedette;
    }

    public void insertVedette() {

        try {
            psInsert = conn.prepareStatement("insert into APP.VEDETTE (ID_VEDETTE,TYPE_VEDETTE ,CAT_VEDETTE,NUM_VEDETTE)  values (?,?,?,?)");
            int j = 0;
            for (int i = 0; i < 12; i++) {
                psInsert.setInt(1, j);
                psInsert.setString(2, "VEDETTE");
                psInsert.setInt(3, 1);
                psInsert.setInt(4, i);
                psInsert.executeUpdate();
                j++;
            }
            for (int i = 0; i < 12; i++) {
                psInsert.setInt(1, j);
                psInsert.setString(2, "BIOTIQUE");
                psInsert.setInt(3, 2);
                psInsert.setInt(4, i);
                psInsert.executeUpdate();
                j++;
            }
            for (int i = 0; i < 12; i++) {
                psInsert.setInt(1, j);
                psInsert.setString(2, "HYPERTENSEUR.");
                psInsert.setInt(3, 3);
                psInsert.setInt(4, i);
                psInsert.executeUpdate();
                j++;
            }
            for (int i = 0; i < 12; i++) {
                psInsert.setInt(1, j);
                j++;
                psInsert.setString(2, "PROTOZOAIRE");
                psInsert.setInt(3, 4);
                psInsert.setInt(4, i);
                psInsert.executeUpdate();
            }
            for (int i = 0; i < 12; i++) {
                psInsert.setInt(1, j);
                j++;
                psInsert.setString(2, "DIABETIQUE");
                psInsert.setInt(3, 5);
                psInsert.setInt(4, i);
                psInsert.executeUpdate();
            }
            for (int i = 0; i < 12; i++) {
                psInsert.setInt(1, j);
                j++;
                psInsert.setString(2, "DOULEUR");
                psInsert.setInt(3, 6);
                psInsert.setInt(4, i);
                psInsert.executeUpdate();
            }
            for (int i = 0; i < 12; i++) {
                psInsert.setInt(1, j);
                j++;
                psInsert.setString(2, "VERMIFUGE");
                psInsert.setInt(3, 7);
                psInsert.setInt(4, i);
                psInsert.executeUpdate();
            }
            for (int i = 0; i < 12; i++) {
                psInsert.setInt(1, j);
                j++;
                psInsert.setString(2, "HISTAMINIQUE");
                psInsert.setInt(3, 8);
                psInsert.setInt(4, i);
                psInsert.executeUpdate();
            }

            // Release the resources (clean up )
            psInsert.close();

        } catch (Throwable e) {
            insertError(e + "", "insertVedette");
        }

    }

    public LinkedList doVedette() {
        LinkedList vedette = new LinkedList();
        try {
            outResult = s.executeQuery("select * from APP.VEDETTE ");

            //  Loop through the ResultSet and print the data
            while (outResult.next()) {
                int id = outResult.getInt(1);
                String type = outResult.getString(2);
                int cat = outResult.getInt(3);
                int num = outResult.getInt(4);
                int pro = outResult.getInt(5);
                int qte = outResult.getInt(6);
                String path = outResult.getString(7);
                if (Main.tax.MRC2 == null) {
                    path = " ";
                } else {
                    path = Main.tax.MRC2.PATH_VEDETTE1 + "/" + outResult.getString(7);
                }
                //         System.out.println(path);
                vedette.add(new Vedette(id, num, cat, type, pro, qte, path));
            }
            //  Close the resultSet
            outResult.close();

            //  Beginning of the primary catch block: uses errorPrint method
        } catch (Throwable e) {
            insertError(e + "", "doVedette");
        }
        return vedette;

    }

    public void fixVedette(Vedette v) {
        try {
            String sql = " update APP.VEDETTE set PATH ='" + v.path + "',ID_PRODUCT =" + v.ID_PRODUCT + ", qte=" + v.qte
                    + " where ID_VEDETTE=" + v.id + "";
            System.out.println(sql);
            s.execute(sql);
        } catch (Throwable e) {
            insertError(e + "", "fixVedette");
        }

    }

    void emp() {
        try {

            System.out.println("select * from APP.EMPLOYE ");
            outResult = s.executeQuery("select * from APP.EMPLOYE ");
            allEmploye = new LinkedList();
            //  Loop through the ResultSet and print the data
            while (outResult.next()) {
                allEmploye.add(new Employe(outResult.getInt(1), outResult.getInt(5), outResult.getInt(6), outResult.getInt(9), outResult.getString(2), outResult.getString(3), outResult.getString(4), outResult.getString(7), outResult.getString(8), outResult.getString(10), outResult.getString(11), outResult.getString(12)));
            }

            //  Close the resultSet
            outResult.close();

            //  Beginning of the primary catch block: uses errorPrint method
        } catch (Throwable e) {
            insertError(e + "", "employe");
        }

    }

    int maxEmp() {

        int id = 0;
        try {

            outResult = s.executeQuery("select max(id_employe) from APP.EMPLOYE ");
            allEmploye = new LinkedList();
            //  Loop through the ResultSet and print the data
            while (outResult.next()) {
                id = outResult.getInt(1);
            }

            //  Close the resultSet
            outResult.close();

            //  Beginning of the primary catch block: uses errorPrint method
        } catch (Throwable e) {
            insertError(e + "", "employe");
        }
        return (id + 1);
    }

    public boolean check_db(int id, int carte) {
        int c = allEmploye.size();
        boolean done = false;

        for (int i = 0; i < c; i++) {
            Employe e = ((Employe) (allEmploye.get(i)));
            if (e.check(id, carte)) {
                done = true;
            }

        }
        return done;
    }

    public Employe getName(int id) {

        Employe s1 = new Employe(0, 0, 0, 0, "", "", "", "", "", "", "", "");
        try {

            outResult = s.executeQuery("select * from APP.EMPLOYE WHERE ID_EMPLOYE=" + id);
            allEmploye = new LinkedList();
            //  Loop through the ResultSet and print the data
            while (outResult.next()) {
                s1 = new Employe(outResult.getInt(1), outResult.getInt(5), outResult.getInt(6), outResult.getInt(9), outResult.getString(2), outResult.getString(3), outResult.getString(4), outResult.getString(7), outResult.getString(8), outResult.getString(10), outResult.getString(11), outResult.getString(12));
                allEmploye.add(s1);
            }

            //  Close the resultSet
            outResult.close();
        } catch (Throwable e) {

            JOptionPane.showMessageDialog(null, l(lang,"V_EMPLOYE"), l(lang,"V_IKIBAZO"), JOptionPane.ERROR_MESSAGE);

        }
        return s1;
    }

    public Employe getEmp(String name) {

        Employe s1 = new Employe(0, 0, 0, 0, "", "", "", "", "", "", "", "");
        try {

            outResult = s.executeQuery("select * from APP.EMPLOYE WHERE NOM_EMPLOYE='" + name + "'");
            allEmploye = new LinkedList();
            //  Loop through the ResultSet and print the data
            while (outResult.next()) {
                s1 = new Employe(outResult.getInt(1), outResult.getInt(5), outResult.getInt(6), outResult.getInt(9), outResult.getString(2), outResult.getString(3), outResult.getString(4), outResult.getString(7), outResult.getString(8), outResult.getString(10), outResult.getString(11), outResult.getString(12));
                allEmploye.add(s1);
            }

            //  Close the resultSet
            outResult.close();
        } catch (Throwable e) {

            JOptionPane.showMessageDialog(null, l(lang,"V_EMPLOYE"), l(lang,"V_IKIBAZO"), JOptionPane.ERROR_MESSAGE);

        }
        return s1;
    }

    public void insertEmp(Employe c) {
        try {
            int a = 0;

            outResult = s.executeQuery("select * from APP.EMPLOYE WHERE NOM_EMPLOYE='" + c.NOM_EMPLOYE + "' AND ID_EMPLOYE=" + c.ID_EMPLOYE + "");
            //  Loop through the ResultSet and print the data
            while (outResult.next()) {
                a++;
            }
            outResult.close();
            if (a == 0) {
                psInsert = conn.prepareStatement("insert into APP.employe  (ID_EMPLOYE,NOM_EMPLOYE,PRENOM_EMPLOYE ,BP_EMPLOYE "
                        + ",CATRE_INDETITE , LEVEL_EMPLOYE,TITRE_EMPLOYE,PWD_EMPLOYE,TEL_EMPLOYE,LANGUE,AUTORISATION,EMAIL) values (?,?,?,?,?,?,?,?,?,?,?,?)");

                psInsert.setInt(1, c.ID_EMPLOYE);
                psInsert.setString(2, c.NOM_EMPLOYE);
                psInsert.setString(3, c.PRENOM_EMPLOYE);
                psInsert.setString(4, c.BP_EMPLOYE);
                psInsert.setInt(5, c.CATRE_INDETITE);
                psInsert.setInt(6, c.LEVEL_EMPLOYE);
                psInsert.setString(7, c.TITRE_EMPLOYE);
                psInsert.setString(8, c.PWD_EMPLOYE);
                psInsert.setInt(9, c.TEL_EMPLOYE);
                psInsert.setString(10, c.LANGUE);
                psInsert.setString(11, c.AUTORISATION);
                psInsert.setString(12, c.EMAIL);
                psInsert.executeUpdate();
                psInsert.close();
                JOptionPane.showMessageDialog(null, l(lang, "V_EMPLOYE") + " " + l(lang, "V_ARINJIYE") + " " + l(lang, "V_NTAKIBAZO"), l(lang, "V_MERCI"), JOptionPane.INFORMATION_MESSAGE);
            }

        } catch (Throwable e) {

            JOptionPane.showMessageDialog(null, l(lang, "V_EMPLOYE") + " " + l(lang, "V_ARIMO"), l(lang, "V_ATTENTION"), JOptionPane.ERROR_MESSAGE);

        }
    }

    public void insertVariable(Variable c) {
        try {

            psInsert = conn.prepareStatement("insert into APP.VARIABLES (NOM_VARIABLE,VALUE_VARIABLE,FAMILLE_VARIABLE ,VALUE2_VARIABLE)"
                    + " values (?,?,?,?)");
            psInsert.setString(1, c.nom);
            psInsert.setString(2, c.value);
            psInsert.setString(3, c.famille);
            psInsert.setString(4, c.value2);
            psInsert.executeUpdate();
            psInsert.close();
            JOptionPane.showMessageDialog(null, l(lang,"V_ADDED"), l(lang,"V_MERCI"), JOptionPane.PLAIN_MESSAGE);

        } catch (Throwable e) {

            JOptionPane.showMessageDialog(null, l(lang,"V_ARIMO"), l(lang,"V_DUPLICATION"), JOptionPane.ERROR_MESSAGE);

        }
    }

    public void upDateEmp(Employe c, int code) {
        try {

            Statement sU = conn.createStatement();
            System.out.println(" update Employe set NOM_EMPLOYE='" + c.NOM_EMPLOYE + "',PRENOM_EMPLOYE='" + c.PRENOM_EMPLOYE + "' ,"
                    + "CATRE_INDETITE=" + c.CATRE_INDETITE + " ,LEVEL_EMPLOYE=" + c.LEVEL_EMPLOYE + " , BP_EMPLOYE='" + c.BP_EMPLOYE + "',"
                    + "TITRE_EMPLOYE='" + c.TITRE_EMPLOYE + "',LANGUE='" + c.LANGUE + "' ,PWD_EMPLOYE='" + c.PWD_EMPLOYE + "' ,TEL_EMPLOYE=" + c.TEL_EMPLOYE + " where ID_EMPLOYE=" + code);

            String createString1 = " update Employe set NOM_EMPLOYE='" + c.NOM_EMPLOYE + "',PRENOM_EMPLOYE='" + c.PRENOM_EMPLOYE + "' ,"
                    + "CATRE_INDETITE=" + c.CATRE_INDETITE + " ,LEVEL_EMPLOYE=" + c.LEVEL_EMPLOYE + " , BP_EMPLOYE='" + c.BP_EMPLOYE + "',"
                    + "TITRE_EMPLOYE='" + c.TITRE_EMPLOYE + "',LANGUE='" + c.LANGUE + "' ,PWD_EMPLOYE='" + c.PWD_EMPLOYE + "' ,TEL_EMPLOYE=" + c.TEL_EMPLOYE + " , AUTORISATION='" + c.AUTORISATION + "' ,EMAIL='" + c.EMAIL + "' where ID_EMPLOYE=" + code;
            sU.execute(createString1);
            JOptionPane.showMessageDialog(null, l(lang, "V_CLIENT") + " " + l(lang, "V_ARINJIYE") + " " + l(lang, "V_NTAKIBAZO"), l(lang, "V_MERCI"), JOptionPane.INFORMATION_MESSAGE);

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "  " + ex, l(lang,"V_UPDATE_FAIL"), JOptionPane.ERROR_MESSAGE);
            insertError(ex + "", "update");
        }
    }

    public void upDateVariable(Variable c, String nom, boolean b) {
        try {
            Statement sU = conn.createStatement();
            String createString1 = " update VARIABLES set VALUE_VARIABLE='" + c.value + "',FAMILLE_VARIABLE='" + c.famille + "' ,"
                    + "VALUE2_VARIABLE='" + c.value2 + "'  where NOM_VARIABLE='" + c.nom + "'";
            sU.execute(createString1);
            // JOptionPane.showMessageDialog(null," VARIABLE UPDATED SUCCESSFULLY "," MURAKOZE ",JOptionPane.PLAIN_MESSAGE);

        } catch (SQLException ex) {

            JOptionPane.showMessageDialog(null, "  " + ex, l(lang,"V_UPDATE_FAIL"), JOptionPane.ERROR_MESSAGE);
            insertError(ex + "", "update");
        }

    }

    public void upDateVariable(Variable c, String nom) {
        try {
            Statement sU = conn.createStatement();
            String createString1 = " update VARIABLES set VALUE_VARIABLE='" + c.value + "',FAMILLE_VARIABLE='" + c.famille + "' ,"
                    + "VALUE2_VARIABLE='" + c.value2 + "'  where NOM_VARIABLE='" + c.nom + "'";
            sU.execute(createString1);
            JOptionPane.showMessageDialog(null, l(lang,"V_UPDATE_ADDED"), l(lang,"V_MERCI"), JOptionPane.PLAIN_MESSAGE);

        } catch (SQLException ex) {

            JOptionPane.showMessageDialog(null, "  " + ex, l(lang,"V_UPDATE_FAIL"), JOptionPane.ERROR_MESSAGE);
            insertError(ex + "", "update");
        }

    }

    public void insertClient(Client c) {
        try {
            String db = "";

            if (c.ASSURANCE.equals("RAMA")) {
                db = "_RAMA";
            }
            if (c.ASSURANCE.equals("CASHREMISE")) {
                db = "_UNIPHARMA";
            }
            psInsert = conn.prepareStatement("insert into APP.CLIENT" + db + " (NUM_AFFILIATION,NOM_CLIENT,PRENOM_CLIENT ,PERCENTAGE ,DATE_EXP , ASSURANCE,EMPLOYEUR,SECTEUR,CODE,AGE,SEXE,LIEN,BENEFICIAIRE ) values (?,?,?,?,?,?,?,?,?,?,?,?,?)");

            psInsert.setString(1, c.NUM_AFFILIATION);
            psInsert.setString(2, c.NOM_CLIENT);
            psInsert.setString(3, c.PRENOM_CLIENT);
            psInsert.setInt(4, c.PERCENTAGE);
            psInsert.setInt(5, c.DATE_EXP);
            psInsert.setString(6, c.ASSURANCE);
            psInsert.setString(7, c.EMPLOYEUR);
            psInsert.setString(8, c.SECTEUR);
            psInsert.setString(9, c.CODE);
            psInsert.setString(10, c.AGE);
            psInsert.setString(11, c.SEXE);
            psInsert.setString(12, c.LIEN);
            psInsert.setString(13, c.BENEFICIAIRE);
            psInsert.executeUpdate();
            psInsert.close();
            JOptionPane.showMessageDialog(null, l(lang, "V_CLIENT") + " " + l(lang, "V_ARINJIYE") + " " + l(lang, "V_NTAKIBAZO"), l(lang, "V_MERCI"), JOptionPane.INFORMATION_MESSAGE);
            updateCompteur(c.CODE);

        } catch (Throwable e) {

            JOptionPane.showMessageDialog(null, l(lang, "V_CLIENT") + " " + l(lang, "V_ARIMO"), l(lang, "V_ATTENTION"), JOptionPane.ERROR_MESSAGE);
        }
    }

    public void insertClienttracking(Client c) {
        try {
            String db = "";

            if (c.ASSURANCE.equals("RAMA")) {
                db = "_RAMA";
            }

            psInsert = conn.prepareStatement("insert into APP.CLIENT" + db + "_TRACKING (NUM_AFFILIATION,NOM_CLIENT,PRENOM_CLIENT ,PERCENTAGE ,DATE_EXP , ASSURANCE,EMPLOYEUR,SECTEUR,CODE,AGE,SEXE,LIEN,BENEFICIAIRE,EMPLOYE ) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

            psInsert.setString(1, c.NUM_AFFILIATION);
            psInsert.setString(2, c.NOM_CLIENT);
            psInsert.setString(3, c.PRENOM_CLIENT);
            psInsert.setInt(4, c.PERCENTAGE);
            psInsert.setInt(5, c.DATE_EXP);
            psInsert.setString(6, c.ASSURANCE);
            psInsert.setString(7, c.EMPLOYEUR);
            psInsert.setString(8, c.SECTEUR);
            psInsert.setString(9, c.CODE);
            psInsert.setString(10, c.AGE);
            psInsert.setString(11, c.SEXE);
            psInsert.setString(12, c.LIEN);
            psInsert.setString(13, c.BENEFICIAIRE);
            psInsert.setString(14, Main.umukozi.NOM_EMPLOYE);
            psInsert.executeUpdate();
            psInsert.close();
            //JOptionPane.showMessageDialog(null,l(lang, "V_CLIENT")+" "+l(lang, "V_ARINJIYE")+" "+l(lang, "V_NTAKIBAZO"),l(lang, "V_MERCI"),JOptionPane.INFORMATION_MESSAGE);
//updateCompteur(c.CODE) ; 

        } catch (Throwable e) {

            JOptionPane.showMessageDialog(null, l(lang, "V_CLIENT") + " " + l(lang, "V_ARIMO"), l(lang, "V_ATTENTION"), JOptionPane.ERROR_MESSAGE);
        }
    }

    void updateCompteur(String code) {
        if (Integer.parseInt(code) > 0) {
            try {
                Statement sU = conn.createStatement();
                String createString1 = "UPDATE APP.VARIABLES SET VALUE_VARIABLE = '" + code + "' WHERE NOM_VARIABLE = 'compteurClient'  AND FAMILLE_VARIABLE = 'RUN' ";
                sU.execute(createString1);
            } catch (Throwable e) {
                insertError(e + " code" + code, "updateCompteur");
            }
        }

    }

    public boolean insertClientRamaAuto(Client c, int compeur) {
        try {
            String db = "";
            if (c.ASSURANCE.equals("RAMA")) {
                db = "_RAMA";
            }
            if (c.ASSURANCE.equals("CASHREMISE")) {
                db = "_UNIPHARMA";
            }
            psInsert = conn.prepareStatement("insert into APP.CLIENT" + db + " (NUM_AFFILIATION,NOM_CLIENT,PRENOM_CLIENT ,PERCENTAGE ,DATE_EXP , ASSURANCE,EMPLOYEUR,SECTEUR,CODE,AGE,SEXE,LIEN,BENEFICIAIRE,visa ) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            System.out.println("test:" + c.NUM_AFFILIATION + c.NOM_CLIENT + c.PRENOM_CLIENT + c.PERCENTAGE + c.DATE_EXP + c.ASSURANCE + c.EMPLOYEUR + c.SECTEUR + c.CODE);
            psInsert.setString(1, c.NUM_AFFILIATION);
            psInsert.setString(2, c.NOM_CLIENT);
            psInsert.setString(3, c.PRENOM_CLIENT);
            psInsert.setInt(4, c.PERCENTAGE);
            psInsert.setInt(5, c.DATE_EXP);
            psInsert.setString(6, c.ASSURANCE);
            psInsert.setString(7, c.EMPLOYEUR);
            psInsert.setString(8, c.SECTEUR);
            psInsert.setString(9, c.CODE);

            if (dbName.contains("ishyiga") || dbName.contains("KCT")) {
                psInsert.setString(10, c.AGE);
            } else {
                psInsert.setInt(10, Integer.parseInt(c.AGE));
            }

            psInsert.setString(11, c.SEXE);
            psInsert.setString(12, c.LIEN);
            psInsert.setString(13, c.BENEFICIAIRE);
            psInsert.setString(14, c.VISA);
            psInsert.executeUpdate();
            psInsert.close();
            //JOptionPane.showMessageDialog(null," UMUKIRIYA ARINJIYE NTAKIBAZO "," MURAKOZE ",JOptionPane.PLAIN_MESSAGE);
            updateCompteur(c.CODE);
        } catch (Throwable e) {
            System.out.println("Error" + c.NUM_AFFILIATION + e);
            //  JOptionPane.showMessageDialog(null," Umukiriya ashobora kuba arimo  "," DUPLICAGE ",JOptionPane.ERROR_MESSAGE);
            return true;
        }

        return false;
    }

    public boolean insertClientR(Client c) {
        try {
            String db = "";

            if (c.ASSURANCE.equals("RAMA")) {
                db = "_RAMA";
            }
            if (c.ASSURANCE.equals("CASHREMISE")) {
                db = "_UNIPHARMA";
            }
            psInsert = conn.prepareStatement("insert into APP.CLIENT" + db + " (NUM_AFFILIATION,NOM_CLIENT,PRENOM_CLIENT ,PERCENTAGE ,DATE_EXP , ASSURANCE,EMPLOYEUR,SECTEUR,CODE,AGE,SEXE,LIEN,BENEFICIAIRE,visa ) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

            psInsert.setString(1, c.NUM_AFFILIATION);
            psInsert.setString(2, c.NOM_CLIENT);
            psInsert.setString(3, c.PRENOM_CLIENT);
            psInsert.setInt(4, c.PERCENTAGE);
            psInsert.setInt(5, c.DATE_EXP);
            psInsert.setString(6, c.ASSURANCE);
            psInsert.setString(7, c.EMPLOYEUR);
            psInsert.setString(8, c.SECTEUR);
            psInsert.setString(9, c.CODE);
//if(dbName.contains("ishyiga")||dbName.contains("KCT"))
            psInsert.setString(10, c.AGE);
//else
//psInsert.setInt(10,Integer.parseInt(c.AGE));

            psInsert.setString(11, c.SEXE);
            psInsert.setString(12, c.LIEN);
            psInsert.setString(13, c.BENEFICIAIRE);
            psInsert.setString(14, c.VISA);
            psInsert.executeUpdate();
            psInsert.close();
            // JOptionPane.showMessageDialog(null,l(lang, "V_CLIENT")+" "+l(lang, "V_ARINJIYE")+" "+l(lang, "V_NTAKIBAZO"),l(lang, "V_MERCI"),JOptionPane.INFORMATION_MESSAGE);
            updateCompteur(c.CODE);

        } catch (Throwable e) {

            JOptionPane.showMessageDialog(null, l(lang, "V_CLIENT") + " " + l(lang, "V_ARIMO"), l(lang, "V_ATTENTION"), JOptionPane.ERROR_MESSAGE);

            return false;
        }
        return true;
    }

    public boolean insertClientR_tracking(Client c) {
        try {
            String db = "";

            if (c.ASSURANCE.equals("RAMA")) {
                db = "_RAMA";
            }

            psInsert = conn.prepareStatement("insert into APP.CLIENT" + db + "_TRACKING (NUM_AFFILIATION,NOM_CLIENT,PRENOM_CLIENT ,PERCENTAGE ,DATE_EXP , ASSURANCE,EMPLOYEUR,SECTEUR,CODE,AGE,SEXE,LIEN,BENEFICIAIRE,visa,EMPLOYE) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

            psInsert.setString(1, c.NUM_AFFILIATION);
            psInsert.setString(2, c.NOM_CLIENT);
            psInsert.setString(3, c.PRENOM_CLIENT);
            psInsert.setInt(4, c.PERCENTAGE);
            psInsert.setInt(5, c.DATE_EXP);
            psInsert.setString(6, c.ASSURANCE);
            psInsert.setString(7, c.EMPLOYEUR);
            psInsert.setString(8, c.SECTEUR);
            psInsert.setString(9, c.CODE);
//if(dbName.contains("ishyiga")||dbName.contains("KCT"))
            psInsert.setString(10, c.AGE);
//else
//psInsert.setInt(10,Integer.parseInt(c.AGE));

            psInsert.setString(11, c.SEXE);
            psInsert.setString(12, c.LIEN);
            psInsert.setString(13, c.BENEFICIAIRE);
            psInsert.setString(14, c.VISA);
            psInsert.setString(15, Main.umukozi.NOM_EMPLOYE);
            psInsert.executeUpdate();
            psInsert.close();
 // JOptionPane.showMessageDialog(null,l(lang, "V_CLIENT")+" "+l(lang, "V_ARINJIYE")+" "+l(lang, "V_NTAKIBAZO"),l(lang, "V_MERCI"),JOptionPane.INFORMATION_MESSAGE);
            //updateCompteur(c.CODE) ; 

        } catch (Throwable e) {
            System.out.println("kumeneka:" + e);

            JOptionPane.showMessageDialog(null, l(lang, "V_CLIENT") + " " + l(lang, "V_ARIMO"), l(lang, "V_ATTENTION"), JOptionPane.ERROR_MESSAGE);

            return false;
        }
        return true;
    }

    public void upDateClient(Client c, String db, String nAff) {
        String age = "AGE='" + c.AGE + "'";
        try {
            Statement sU = conn.createStatement();
            String createString1 = "update APP." + db + " set NOM_CLIENT='" + c.NOM_CLIENT + "',PRENOM_CLIENT='" + c.PRENOM_CLIENT + "' ,PERCENTAGE=" + c.PERCENTAGE + " ,DATE_EXP=" + c.DATE_EXP + " , ASSURANCE='" + c.ASSURANCE + "',EMPLOYEUR='" + c.EMPLOYEUR + "',SECTEUR='" + c.SECTEUR + "',LIEN='" + c.LIEN + "',BENEFICIAIRE='" + c.BENEFICIAIRE + "'," + age + ",SEXE='" + c.SEXE + "'     where NUM_AFFILIATION='" + nAff + "'";
            System.out.println(createString1);
            sU.execute(createString1);
            JOptionPane.showMessageDialog(null, l(lang, "V_CLIENT") + " " + l(lang, "V_ARINJIYE") + " " + l(lang, "V_NTAKIBAZO"), l(lang, "V_MERCI"), JOptionPane.INFORMATION_MESSAGE);

        } catch (SQLException ex) {

            JOptionPane.showMessageDialog(null, "  " + ex, l(lang,"V_UPDATE_FAIL"), JOptionPane.ERROR_MESSAGE);
            insertError(ex + "", "update");
        }
    }

    public boolean upDateClientR(Client c, String db, String nAff) {
        try {
            Statement sU = conn.createStatement();
            String age = "" + DataType("SELECT AGE FROM CLIENT_RAMA");
            if (age.contains("VARCHAR")) {
                age = "AGE='" + c.AGE + "'";
            } else {
                age = "AGE=" + c.AGE + " ";
            }

            String createString1 = "update APP." + db + " set NOM_CLIENT='" + c.NOM_CLIENT + "',PRENOM_CLIENT='" + c.PRENOM_CLIENT + "' ,PERCENTAGE=" + c.PERCENTAGE
                    + " ,DATE_EXP=" + c.DATE_EXP + " , VISA='" + c.VISA + "', ASSURANCE='" + c.ASSURANCE + "',EMPLOYEUR='" + c.EMPLOYEUR + "',SECTEUR='" + c.SECTEUR
                    + "',LIEN='" + c.LIEN + "',BENEFICIAIRE='" + c.BENEFICIAIRE + "'," + age + " ,SEXE='" + c.SEXE + "' , CHANGED=CURRENT TIMESTAMP     where NUM_AFFILIATION='" + nAff + "'";
            // System.err.println(createString1);
            sU.execute(createString1);
            return true;
        } catch (SQLException ex) {

            JOptionPane.showMessageDialog(null, "  " + ex, l(lang,"V_UPDATE_FAIL"), JOptionPane.ERROR_MESSAGE);
            insertError(ex + "", "update");
        }

        return false;
    }

    public boolean hasColumn(ResultSet rs, String columnName) {
        try {
            ResultSetMetaData rsmd = rs.getMetaData();
            int columns = rsmd.getColumnCount();
            for (int x = 1; x <= columns; x++) {
                if (columnName.equals(rsmd.getColumnName(x))) {
                    return true;
                }
            }
        } catch (Exception e) {
            insertError(e + "", "hasColumn:" + columnName);
        }
        return false;

    }

    public String DataType(String sql) {
        try {
            outResult = s.executeQuery(sql);

            ResultSetMetaData rsmd = outResult.getMetaData();
            System.out.println("data type:" + rsmd.getColumnTypeName(1));
            return rsmd.getColumnTypeName(1);
        } catch (SQLException e) {
            System.err.println(e);
            return "INTEGER";
        }
    }

    public boolean beforeNegatifRama() {
        try {
            Statement sU = conn.createStatement();
            String createString1 = "update APP.client_rama set VISA='O' ";
            sU.execute(createString1);
            return true;
        } catch (SQLException ex) {

            JOptionPane.showMessageDialog(null, "  " + ex, l(lang,"V_UPDATE_FAIL"), JOptionPane.ERROR_MESSAGE);
            insertError(ex + "", "update");
        }

        return false;
    }

    public void upDateclient(Client c, String db, String nAff) {
        try {
            Statement sU = conn.createStatement();
            ///  System.out.println("update APP." + db + " set NOM_CLIENT='" + c.NOM_CLIENT + "',PRENOM_CLIENT='" + c.PRENOM_CLIENT + "' ,PERCENTAGE=" + c.PERCENTAGE + " ,DATE_EXP=" + c.DATE_EXP + " , ASSURANCE='" + c.ASSURANCE + "',EMPLOYEUR='" + c.EMPLOYEUR + "',SECTEUR='" + c.SECTEUR + "',LIEN='" + c.LIEN+ "',BENEFICIAIRE='" + c.BENEFICIAIRE+ "',AGE='" + c.AGE+ "',SEXE='" + c.SEXE  + "'   where NUM_AFFILIATION='" + nAff + "'");
            String createString1 = "update APP." + db + " set NOM_CLIENT='" + c.NOM_CLIENT + "',PRENOM_CLIENT='" + c.PRENOM_CLIENT + "' ,PERCENTAGE=" + c.PERCENTAGE + " ,DATE_EXP=" + c.DATE_EXP + " , ASSURANCE='" + c.ASSURANCE + "',EMPLOYEUR='" + c.EMPLOYEUR + "',SECTEUR='" + c.SECTEUR + "',LIEN='" + c.LIEN + "',BENEFICIAIRE='" + c.BENEFICIAIRE + "',AGE='" + c.AGE + "',SEXE='" + c.SEXE + "'     where NUM_AFFILIATION='" + nAff + "'";
            sU.execute(createString1);
            //JOptionPane.showMessageDialog(null," UMUKIRIYA ARINJIYE NTAKIBAZO "," MURAKOZE ",JOptionPane.PLAIN_MESSAGE);

        } catch (SQLException ex) {

            JOptionPane.showMessageDialog(null, "  " + ex, l(lang,"V_UPDATE_FAIL"), JOptionPane.ERROR_MESSAGE);
            insertError(ex + "", "update");
        }

    }

    public void upDateClientIn() {
        try {

            Statement sU = conn.createStatement();
            int i = 0;
            outResult = s.executeQuery("select * from APP.CLIENT_UNIPHARMA ");
            while (outResult.next()) {
                i++;
                String NUM_AFFILIATIONa = outResult.getString(1);
                String createString1 = " update APP.CLIENT_UNIPHARMA set CODE='" + i + "' WHERE NUM_AFFILIATION='" + NUM_AFFILIATIONa + "'";
                sU.execute(createString1);
            }
            outResult = s.executeQuery("select * from APP.CLIENT ");
            while (outResult.next()) {
                i++;
                String NUM_AFFILIATIONa = outResult.getString(1);
                String createString1 = " update APP.CLIENT set CODE='" + i + "' WHERE NUM_AFFILIATION='" + NUM_AFFILIATIONa + "'";
                sU.execute(createString1);
            }
            outResult = s.executeQuery("select * from APP.CLIENT_RAMA ");
            while (outResult.next()) {
                i++;
                String NUM_AFFILIATIONa = outResult.getString(1);
                String createString1 = " update APP.CLIENT_RAMA set CODE='" + i + "' WHERE NUM_AFFILIATION='" + NUM_AFFILIATIONa + "'";
                sU.execute(createString1);
            }

        } catch (SQLException ex) {
            Logger.getLogger(Db.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void getList() {
        try {
            outResult1 = s.executeQuery("select ID_INVOICE,CODE_UNI,QUANTITE,ID_PRODUCT from APP.LIST,APP.PRODUCT  WHERE PRODUCT.CODE=LIST.CODE_UNI");

            int k = 0;

            LinkedList<Integer> inve = new LinkedList();
            LinkedList<String> codel = new LinkedList();
            LinkedList<Integer> qt = new LinkedList();
            LinkedList<Integer> i = new LinkedList();

            while (outResult1.next()) {

                int INV = outResult1.getInt(1);
                String code = outResult1.getString(2);
                int quantity = outResult1.getInt(3);
                int ID = outResult1.getInt(4);
                boolean done = true;

                inve.add(INV);
                codel.add(code);
                qt.add(quantity);
                i.add(ID);

            }
            outResult1.close();

            for (int y = 40; y < inve.size(); y++) {
                boolean done = true;
                outResult = s.executeQuery("select * from APP.LOT where  ID_PRODUCT=" + i.get(y) + "order by date_exp,id_lot ");
                int quantity = qt.get(y);
                String code = codel.get(y);
                int INV = inve.get(y);
                while (outResult.next() && quantity > 0) {
                    int productCode = outResult.getInt(1);
                    int id = outResult.getInt(2);
                    String id_LotS = outResult.getString(3);
                    String date1 = outResult.getString(4);
                    int qtyStart = outResult.getInt(5);
                    int qtyLive = outResult.getInt(6);
                    String bon_livraison = outResult.getString(7);
                    System.out.println(codel.get(y) + "   " + qtyLive + "   " + id_LotS);
                    done = false;

                    if (qtyLive > quantity) {

                        qtyLive = qtyLive - quantity;

                        Statement sL = conn.createStatement();
                        String createString2 = " update APP.LIST set num_lot='" + id_LotS + "' where code_uni='" + code + "'and id_invoice=" + INV;

                        sL.execute(createString2);

                        Statement sU = conn.createStatement();
                        String createString1 = " update APP.LOT set QTY_LIVE=" + qtyLive + " where BON_LIVRAISON='" + bon_livraison + "'AND ID_LOT=" + id + " and ID_LOTS='" + id_LotS + "' and ID_PRODUCT=" + productCode + "";

                        sU.execute(createString1);
                        quantity = 0;

                        //   System.out.println(id_LotS+" if "+qtyLive+" qtty "+quantity);
                        done = false;
                    } else if (qtyLive < quantity) {
                        quantity = quantity - qtyLive;

                        Statement sL = conn.createStatement();
                        String createString2 = " update APP.LIST set num_lot='" + id_LotS + "' where code_uni='" + code + "'and id_invoice=" + INV;

                        sL.execute(createString2);
                        PreparedStatement psInsert2 = conn.prepareStatement("insert into APP.USED_LOT(ID_PRODUCT,ID_LOT,ID_LOTS,DATE_EXP,QTY_START,QTY_LIVE,BON_LIVRAISON)  values (?,?,?,?,?,?,?)");

                        psInsert2.setInt(1, productCode);
                        psInsert2.setInt(2, id);
                        psInsert2.setString(3, id_LotS);
                        psInsert2.setString(4, date1);
                        psInsert2.setInt(5, qtyStart);
                        psInsert2.setInt(6, qtyLive);
                        psInsert2.setString(7, bon_livraison);
                        psInsert2.executeUpdate();
                        psInsert2.close();

                        Statement sD = conn.createStatement();
                        String createString1 = " delete from lot where ID_LOT=" + id + " and BON_LIVRAISON='" + bon_livraison + "' and date_exp= '" + date1 + "' and id_LotS= '" + id_LotS + "' and ID_PRODUCT= " + productCode;

                        sD.execute(createString1);

                    } else if (qtyLive == quantity) {

                        if (quantity != 0) {
                            Statement sL = conn.createStatement();
                            String createString2 = " update APP.LIST set num_lot='" + id_LotS + "' where code_uni='" + code + "'and id_invoice=" + INV;

                            sL.execute(createString2);
                        }

                        quantity = quantity - qtyLive;
                        done = false;
                        PreparedStatement psInsert2 = conn.prepareStatement("insert into APP.USED_LOT(ID_PRODUCT,ID_LOT,ID_LOTS,DATE_EXP,QTY_START,QTY_LIVE,BON_LIVRAISON)  values (?,?,?,?,?,?,?)");

                        psInsert2.setInt(1, productCode);
                        psInsert2.setInt(2, id);
                        psInsert2.setString(3, id_LotS);
                        psInsert2.setString(4, date1);
                        psInsert2.setInt(5, qtyStart);
                        psInsert2.setInt(6, qtyLive);
                        psInsert2.setString(7, bon_livraison);
                        psInsert2.executeUpdate();
                        psInsert2.close();

                        Statement sD = conn.createStatement();
                        String createString1 = " delete from lot where ID_LOT=" + id + " and BON_LIVRAISON='" + bon_livraison + "' and id_LotS= '" + id_LotS + "' and ID_PRODUCT= " + productCode;
                        sD.execute(createString1);

                    }
                }
                if (done) {
                    System.out.println(codel.get(y));
                }

                //  Close the resultSet
                outResult.close();
            }

        } catch (Throwable e) {
            insertError(e + "", "getList");
        }

    }

    public Client getClient(String entier, String table) {
        Client c = null;
 //JOptionPane.showMessageDialog(null,entier,table,JOptionPane.PLAIN_MESSAGE);

        try {
            if (table.equals("CASHNORM") && !entier.equals("DIVERS")) {
                c = new Client("CASHNORM", "CASHNORM", "CASHNORM", 0, 0, "CASHNORM", "", "", "", "CASHNORM");
            } else if (table.equals("UNI")) {
                outResult = s.executeQuery("select * from APP.CLIENT_UNIPHARMA where CODE ='" + entier + "'");
                while (outResult.next()) {
                    String NUM_AFFILIATIONa = outResult.getString(1);
                    String NOM_CLIENTa = outResult.getString(2);
                    String PRENOM_CLIENTa = outResult.getString(3);
                    int PERCENTAGEa = outResult.getInt(4);
                    int DATE_EXPa = outResult.getInt(5);
                    String ASSURANCEa = outResult.getString(6);
                    String EMPLOYEURa = outResult.getString(7);
                    String SECTEURa = outResult.getString(8);
                    String VISAa = outResult.getString(9);
                    String CODE = outResult.getString(10);
                    String AGE = outResult.getString(11);
                    String SEXE = outResult.getString(12);
                    String LINK = outResult.getString(13);
                    String BENEFICIAIRE = outResult.getString(14);
                    c = new Client(NUM_AFFILIATIONa, NOM_CLIENTa, PRENOM_CLIENTa, PERCENTAGEa, DATE_EXPa, ASSURANCEa, EMPLOYEURa, SECTEURa, VISAa, CODE, AGE, SEXE, LINK, BENEFICIAIRE);
                }
            } else {
                outResult = s.executeQuery("select * from APP.CLIENT" + table + " where CODE ='" + entier + "'");
                while (outResult.next()) {
                    String NUM_AFFILIATIONa = outResult.getString(1);
                    String NOM_CLIENTa = outResult.getString(2);
                    String PRENOM_CLIENTa = outResult.getString(3);
                    int PERCENTAGEa = outResult.getInt(4);
                    int DATE_EXPa = outResult.getInt(5);
                    String ASSURANCEa = outResult.getString(6);
                    String EMPLOYEURa = outResult.getString(7);
                    String SECTEURa = outResult.getString(8);
                    String VISAa = outResult.getString(9);
                    String CODE = outResult.getString(10);
                    String AGE = outResult.getString(11);
                    String SEXE = outResult.getString(12);
                    String LINK = outResult.getString(13);
                    String BENEFICIAIRE = outResult.getString(14);
                    c = new Client(NUM_AFFILIATIONa, NOM_CLIENTa, PRENOM_CLIENTa, PERCENTAGEa, DATE_EXPa, ASSURANCEa, EMPLOYEURa, SECTEURa, VISAa, CODE, AGE, SEXE, LINK, BENEFICIAIRE);
                }
            }
            //  Close the resultSet
            outResult.close();
        } catch (Throwable e) {
            insertError(e + "", "getclint");
        }

        return c;
    }

    public Client getClientAff(String entier, String table, String sql) {
        Client c = null;

        try {
            {
                outResult = s.executeQuery("select * from APP.CLIENT" + table + " where NUM_AFFILIATION ='" + entier + "' " + sql);
                System.out.println("select * from APP.CLIENT" + table + " where NUM_AFFILIATION ='" + entier + "' " + sql);

                while (outResult.next()) {
                    String NUM_AFFILIATIONa = outResult.getString(1);
                    String NOM_CLIENTa = outResult.getString(2);
                    String PRENOM_CLIENTa = outResult.getString(3);
                    int PERCENTAGEa = outResult.getInt(4);
                    int DATE_EXPa = outResult.getInt(5);
                    String ASSURANCEa = outResult.getString(6);
                    System.out.println(ASSURANCEa);
                    String EMPLOYEURa = outResult.getString(7);
                    String SECTEURa = outResult.getString(8);
                    String VISAa = outResult.getString(9);
                    String CODE = outResult.getString(10);
                    String AGE = outResult.getString(11);
                    String SEXE = outResult.getString(12);
                    String LINK = outResult.getString(13);
                    String BENEFICIAIRE = outResult.getString(14);
                    return new Client(NUM_AFFILIATIONa, NOM_CLIENTa, PRENOM_CLIENTa, PERCENTAGEa, DATE_EXPa, ASSURANCEa, EMPLOYEURa, SECTEURa, VISAa, CODE, AGE, SEXE, LINK, BENEFICIAIRE);
                }
            }
        //  Close the resultSet
            outResult.close();
        } catch (Throwable e) {
            insertError(e + "", "getclint");
        }

        return c;
    }

    public Client[] getClientScann(String entier, String table) {
        LinkedList<Client> p = new LinkedList<Client>();

        try {                    
                System.out.println("select * from APP.CLIENT" + table + " where NUM_AFFILIATION like '%" + entier + "%' ");
                outResult = s.executeQuery("select * from APP.CLIENT" + table + " where NUM_AFFILIATION like '%" + entier + "%' ");
                
                while (outResult.next()) {
                    String NUM_AFFILIATIONa = outResult.getString("NUM_AFFILIATION");
                    String NOM_CLIENTa = outResult.getString("NOM_CLIENT");
                    
                    String PRENOM_CLIENTa = outResult.getString("PRENOM_CLIENT");
                    int PERCENTAGEa = outResult.getInt("PERCENTAGE");
                    int DATE_EXPa = outResult.getInt("DATE_EXP");
                    String ASSURANCEa = outResult.getString("ASSURANCE");
                    String EMPLOYEURa = outResult.getString("EMPLOYEUR");
                    String SECTEURa = outResult.getString("SECTEUR");
                    String VISAa = outResult.getString("VISA");
                    String CODE = outResult.getString("CODE");
                    String AGE = outResult.getString("AGE");
                    String SEXE = outResult.getString("SEXE");
                    String LINK = outResult.getString("LIEN");
                    String BENEFICIAIRE = outResult.getString("BENEFICIAIRE");

                    p.add(new Client(NUM_AFFILIATIONa, NOM_CLIENTa, PRENOM_CLIENTa, PERCENTAGEa, DATE_EXPa, ASSURANCEa, EMPLOYEURa, SECTEURa, VISAa, CODE, AGE, SEXE, LINK, BENEFICIAIRE));

                }
        //  Close the resultSet
            outResult.close();
        } catch (Throwable e) {
            insertError(e + "", "getclint");
        }

        Client[] cli = new Client[p.size()];
        for (int h = 0; h < p.size(); h++) {
            cli[h] = p.get(h);
        }
        return cli;
    }

    public Client[] getClientB(String entier, String table, String assurance) {
        Client c = null;
        //JOptionPane.showMessageDialog(null,entier,table,JOptionPane.PLAIN_MESSAGE);

        LinkedList<Client> client = new LinkedList();

        try {
            if (table.equals("UNI")) {
                outResult = s.executeQuery(""
                        + "select * from APP.CLIENT  where (nom_client like '%" + entier + "%' or prenom_client like '%" + entier + "%' or NUM_Affiliation like '%" + entier + "%') AND ASSURANCE!='RAMA' AND ASSURANCE!='CORAR' AND ASSURANCE!='MMI' AND ASSURANCE!='AAR' AND ASSURANCE!='SORAS' ");
                while (outResult.next()) {
                    String NUM_AFFILIATIONa = outResult.getString(1);
                    String NOM_CLIENTa = outResult.getString(2);
                    String PRENOM_CLIENTa = outResult.getString(3);
                    int PERCENTAGEa = outResult.getInt(4);
                    int DATE_EXPa = outResult.getInt(5);
                    String ASSURANCEa = outResult.getString(6);
                    String EMPLOYEURa = outResult.getString(7);
                    String SECTEURa = outResult.getString(8);
                    String VISAa = outResult.getString(9);
                    String CODE = outResult.getString(10);
                    String AGE = outResult.getString(11);
                    String SEXE = outResult.getString(12);
                    String LINK = outResult.getString(13);
                    String BENEFICIAIRE = outResult.getString(14);
                    c = new Client(NUM_AFFILIATIONa, NOM_CLIENTa, PRENOM_CLIENTa, PERCENTAGEa, DATE_EXPa, ASSURANCEa, EMPLOYEURa, SECTEURa, VISAa, CODE, AGE, SEXE, LINK, BENEFICIAIRE);
                    client.add(c);
                }
            } else if (assurance.equals("SOCIETE")) {
                outResult = s.executeQuery(""
                        + "select * from APP.CLIENT where (nom_client like '%" + entier + "%' or prenom_client like '%" + entier + "%' or NUM_Affiliation like '%" + entier + "%') AND ASSURANCE!='RAMA' AND ASSURANCE!='CORAR' AND ASSURANCE!='MMI' AND ASSURANCE!='AAR' AND ASSURANCE!='SORAS' ");
                while (outResult.next()) {
                    String NUM_AFFILIATIONa = outResult.getString(1);
                    String NOM_CLIENTa = outResult.getString(2);
                    String PRENOM_CLIENTa = outResult.getString(3);
                    int PERCENTAGEa = outResult.getInt(4);
                    int DATE_EXPa = outResult.getInt(5);
                    String ASSURANCEa = outResult.getString(6);
                    String EMPLOYEURa = outResult.getString(7);
                    String SECTEURa = outResult.getString(8);
                    String VISAa = outResult.getString(9);
                    String CODE = outResult.getString(10);
                    String AGE = outResult.getString(11);
                    String SEXE = outResult.getString(12);
                    String LINK = outResult.getString(13);
                    String BENEFICIAIRE = outResult.getString(14);
                    c = new Client(NUM_AFFILIATIONa, NOM_CLIENTa, PRENOM_CLIENTa, PERCENTAGEa, DATE_EXPa, ASSURANCEa, EMPLOYEURa, SECTEURa, VISAa, CODE, AGE, SEXE, LINK, BENEFICIAIRE);
                    client.add(c);
                }
            } else {
                outResult = s.executeQuery(""
                        + "select * from APP.CLIENT" + table + "  where (nom_client like '%" + entier + "%' or prenom_client like '%" + entier + "%' or NUM_Affiliation like '%" + entier + "%') AND ASSURANCE='" + assurance + "'");
                while (outResult.next()) {
                    String NUM_AFFILIATIONa = outResult.getString(1);
                    String NOM_CLIENTa = outResult.getString(2);
                    String PRENOM_CLIENTa = outResult.getString(3);
                    int PERCENTAGEa = outResult.getInt(4);
                    int DATE_EXPa = outResult.getInt(5);
                    String ASSURANCEa = outResult.getString(6);
                    String EMPLOYEURa = outResult.getString(7);
                    String SECTEURa = outResult.getString(8);
                    String VISAa = outResult.getString(9);
                    String CODE = outResult.getString(10);
                    String AGE = outResult.getString(11);
                    String SEXE = outResult.getString(12);
                    String LINK = outResult.getString(13);
                    String BENEFICIAIRE = outResult.getString(14);
                    c = new Client(NUM_AFFILIATIONa, NOM_CLIENTa, PRENOM_CLIENTa, PERCENTAGEa, DATE_EXPa, ASSURANCEa, EMPLOYEURa, SECTEURa, VISAa, CODE, AGE, SEXE, LINK, BENEFICIAIRE);
                    client.add(c);
                }
            }
            outResult.close();
        } catch (Throwable e) {
            insertError(e + "", "getclintB");
        }

        Client[] cli = new Client[client.size()];
        for (int h = 0; h < client.size(); h++) {
            cli[h] = client.get(h);
        }
        return cli;
    }

    public Client[] getClientV(String entier, String table, String assurance) {
        System.out.println(entier);
        LinkedList<Client> client = new LinkedList();
        try {
            
            String sql = ("select * from APP.CLIENT" + table + " where ");
            String sql2 = ("   ORDER BY nom_client");
            System.out.println(sql);
            System.out.println("boolean:" + ((assurance.equals("MMI") || assurance.equals("CORAR") || assurance.equals("SORAS") || assurance.equals("RADIANT") || assurance.equals("BRITAM"))));
            
            if(!assurance.equals("SOCIETE"))
            {
                sql2 = ("  AND ASSURANCE='" + assurance + "'  ORDER BY nom_client");
            }
            else
            {
                LinkedList<Tarif> allTarif=getAllTarif();
                sql2="";
                for(int i=0;i<allTarif.size();i++)
                {
                    if(allTarif.get(i).DES_TARIF.length()>2)
                        sql2= sql2+(" AND ASSURANCE !='"+allTarif.get(i).DES_TARIF+"' ");
                }
                sql2=sql2+" ORDER BY nom_client";
            }
            
//            if (assurance.equals("MMI") || assurance.equals("CORAR") || assurance.equals("SORAS")) {
//                sql2 = ("  AND ASSURANCE='" + assurance + "'  ORDER BY nom_client");
//            } else if (assurance.equals("SOCIETE")) {
//                sql2 = (" AND (ASSURANCE!='MMI' AND ASSURANCE!='CORAR' AND ASSURANCE!='SORAS' AND ASSURANCE!='AAR' AND ASSURANCE!='RADIANT' AND ASSURANCE!='BRITAM') ORDER BY nom_client");
//            }
            StringTokenizer st1 = new StringTokenizer(entier);
            int i = 0;
            String token1 = "";
            String token2 = "";
            while (st1.hasMoreTokens()) {
                if (i != 1) {
                    token1 = st1.nextToken(" ");
                }
                if (i == 1) {
                    token2 = st1.nextToken(" ");
                }
                i++;
            }
            token2.replaceAll(" ", "");
            String query = "";
            if (i > 1) {
                query = "( nom_client like      '%" + token1 + "%' AND prenom_client   like   '%" + token2 + "%') OR "
                        + "( nom_client like      '%" + token1 + "%' AND NUM_Affiliation like   '%" + token2 + "%') OR "
                        + "( NUM_Affiliation like '%" + token1 + "%' AND prenom_client   like   '%" + token2 + "%') OR "
                        + "( NUM_Affiliation like '%" + token1 + "%' AND nom_client      like   '%" + token2 + "%') OR "
                        + "( prenom_client like   '%" + token1 + "%' AND prenom_client   like   '%" + token2 + "%') OR "
                        + "( NUM_Affiliation like   '%" + token1 + "%' AND nom_client      like   '%" + token2 + "%') OR"
                        + "( CODE like   '%" + token1 + "%' AND CODE  like   '%" + token2 + "%')";
            } else {
                query = "nom_client like '%" + token1 + "%' or prenom_client like '%" + token1 + "%' or NUM_Affiliation like '%" + token1 + "%' or CODE like '%" + token1 + "%' ";
            }

            sql = sql + " (" + query + ") " + sql2;
            System.out.println(sql);
            outResult = s.executeQuery(sql);
            while (outResult.next()) {
                String NUM_AFFILIATIONa = outResult.getString("NUM_AFFILIATION");
                String NOM_CLIENTa = outResult.getString("NOM_CLIENT");
                String PRENOM_CLIENTa = outResult.getString("PRENOM_CLIENT");
                int PERCENTAGEa = outResult.getInt("PERCENTAGE");
                int DATE_EXPa = outResult.getInt("DATE_EXP");
                String ASSURANCEa = outResult.getString("ASSURANCE");
                String EMPLOYEURa = outResult.getString("EMPLOYEUR");
                String SECTEURa = outResult.getString("SECTEUR");
                String VISAa = outResult.getString("VISA");
                String CODE = outResult.getString("CODE");
                String AGE = outResult.getString("AGE");
                String SEXE = outResult.getString("SEXE");
                String LINK = outResult.getString("LIEN");
                String BENEFICIAIRE = outResult.getString("BENEFICIAIRE");
                Client c = new Client(NUM_AFFILIATIONa, NOM_CLIENTa, PRENOM_CLIENTa, PERCENTAGEa, DATE_EXPa, ASSURANCEa, EMPLOYEURa, SECTEURa, VISAa, CODE, AGE, SEXE, LINK, BENEFICIAIRE);
                client.add(c);
            }
            //  Close the resultSet
            outResult.close();

            if (assurance.equals("CASHNORM") && (dbName.contains("KCT") || dbName.contains("ishyiga"))) {
                outResult = s.executeQuery("select * from APP.CLIENT_UNIPHARMA where  (nom_client like '%" + entier + "%' or prenom_client like '%" + entier + "%' or NUM_Affiliation like '%" + entier + "%' or CODE like '%" + entier + "%' )    ORDER BY nom_client");
                while (outResult.next()) {
                    String NUM_AFFILIATIONa = outResult.getString(1);
                    String NOM_CLIENTa = outResult.getString(2);
                    String PRENOM_CLIENTa = outResult.getString(3);
                    int PERCENTAGEa = outResult.getInt(4);
                    int DATE_EXPa = outResult.getInt(5);
                    String ASSURANCEa = outResult.getString(6);
                    String EMPLOYEURa = outResult.getString(7);
                    String SECTEURa = outResult.getString(8);
                    String VISAa = outResult.getString(9);
                    String CODE = outResult.getString(10);
                    String AGE = outResult.getString(11);
                    String SEXE = outResult.getString(12);
                    String LINK = outResult.getString(13);
                    String BENEFICIAIRE = outResult.getString(14);
                    Client c = new Client(NUM_AFFILIATIONa, NOM_CLIENTa, PRENOM_CLIENTa, PERCENTAGEa, DATE_EXPa, ASSURANCEa, EMPLOYEURa, SECTEURa, VISAa, CODE, AGE, SEXE, LINK, BENEFICIAIRE);
                    client.add(c);
                }
                //  Close the resultSet
                outResult.close();
            }

        } catch (Throwable e) {
            insertError(e + "", "getclintB");
        }

        Client[] cli = new Client[client.size()];
        for (int h = 0; h < client.size(); h++) {
            cli[h] = client.get(h);
        }
        return cli;
    }

    public Client getClientA(String entier, String table, String ass) {
        Client c = null;
 //JOptionPane.showMessageDialog(null,entier,table,JOptionPane.PLAIN_MESSAGE);

        try {
            if (table.equals("UNI")) {
                outResult = s.executeQuery("select * from APP.CLIENT_UNIPHARMA where NUM_AFFILIATION ='" + entier + "'");
                while (outResult.next()) {
                    String NUM_AFFILIATIONa = outResult.getString(1);
                    String NOM_CLIENTa = outResult.getString(2);
                    String PRENOM_CLIENTa = outResult.getString(3);
                    int PERCENTAGEa = outResult.getInt(4);
                    int DATE_EXPa = outResult.getInt(5);
                    String ASSURANCEa = outResult.getString(6);
                    String EMPLOYEURa = outResult.getString(7);
                    String SECTEURa = outResult.getString(8);
                    String VISAa = outResult.getString(9);
                    String CODE = outResult.getString(10);
                    c = new Client(NUM_AFFILIATIONa, NOM_CLIENTa, PRENOM_CLIENTa, PERCENTAGEa, DATE_EXPa, ASSURANCEa, EMPLOYEURa, SECTEURa, VISAa, CODE);
                }
            } else {
                outResult = s.executeQuery("select * from APP.CLIENT" + table + " where NUM_AFFILIATION ='" + entier + "' AND ASSURANCE='" + ass + "'");
                while (outResult.next()) {
                    String NUM_AFFILIATIONa = outResult.getString(1);
                    String NOM_CLIENTa = outResult.getString(2);
                    String PRENOM_CLIENTa = outResult.getString(3);
                    int PERCENTAGEa = outResult.getInt(4);
                    int DATE_EXPa = outResult.getInt(5);
                    String ASSURANCEa = outResult.getString(6);
                    String EMPLOYEURa = outResult.getString(7);
                    String SECTEURa = outResult.getString(8);
                    String VISAa = outResult.getString(9);
                    String CODE = outResult.getString(10);
                    c = new Client(NUM_AFFILIATIONa, NOM_CLIENTa, PRENOM_CLIENTa, PERCENTAGEa, DATE_EXPa, ASSURANCEa, EMPLOYEURa, SECTEURa, VISAa, CODE);
     //JOptionPane.showMessageDialog(null,c," autre ",JOptionPane.PLAIN_MESSAGE);

                }
            }

            //  Close the resultSet
            outResult.close();
        } catch (Throwable e) {
            insertError(e + "", "getclint");
        }

        return c;

    }

    public LinkedList<Client> getOldClient(String table) {
        Client c = null;
        LinkedList<Client> client = new LinkedList();
        try {

            outResult = s.executeQuery(table);
            while (outResult.next()) {
                String NUM_AFFILIATIONa = outResult.getString(1);
                String NOM_CLIENTa = outResult.getString(2);
                String PRENOM_CLIENTa = outResult.getString(3);
                int PERCENTAGEa = outResult.getInt(4);
                int DATE_EXPa = outResult.getInt(5);
                String ASSURANCEa = outResult.getString(6);
                String EMPLOYEURa = outResult.getString(7);
                String SECTEURa = outResult.getString(8);
                String VISAa = outResult.getString(9);
                String CODE = outResult.getString(10);
                c = new Client(NUM_AFFILIATIONa, NOM_CLIENTa, PRENOM_CLIENTa, PERCENTAGEa, DATE_EXPa, ASSURANCEa, EMPLOYEURa, SECTEURa, VISAa, CODE);
                //JOptionPane.showMessageDialog(null,c," autre ",JOptionPane.PLAIN_MESSAGE);
                client.add(c);

            }
            //  Close the resultSet
            outResult.close();
        } catch (Throwable e) {
            insertError(e + "", "getOldClient");
        }

        return client;

    }

    public void insertLogin2(String id, String in_out, double frw) {
        try {
            psInsert = conn.prepareStatement("insert into APP.LOGIN (ID_EMPLOYE,IN_OUT,ARGENT )  values (?,?,?)");

            psInsert.setString(1, id + "  " + Main.versionIshyiga);
            psInsert.setString(2, in_out);
            psInsert.setDouble(3, frw);
            psInsert.executeUpdate();
            psInsert.close();
        } catch (Throwable e) {
            insertError(e + "", "insertLogin");
        }
    }

    void addOneNew(Product p) {
        try {

            psInsert = conn.prepareStatement("insert into PRODUCT (id_product,name_product,code,prix,prix_SOCIETE, tva,code_bar,observation )  values (?,?,?,?,?,?,?,?)");

            psInsert.setInt(1, p.productCode);
            psInsert.setString(2, p.productName);
            psInsert.setString(3, p.code);
            psInsert.setDouble(4, p.currentPrice);
            psInsert.setDouble(5, (p.currentPrice) * 0.85);
            psInsert.setDouble(6, p.tva);
            psInsert.setString(7, p.code);
            psInsert.setString(8, p.observation);

            psInsert.executeUpdate();

            // Release the resources (clean up )
            psInsert.close();
        } catch (Throwable e) {
            insertError(e + "", "addonenew");
        }

    }

    LinkedList<String> getFichier(String fichier) throws FileNotFoundException, IOException {
        LinkedList<String> give = new LinkedList();

        File productFile = new File(fichier);
        String ligne = null;
        BufferedReader entree = new BufferedReader(new FileReader(productFile));
        try {
            ligne = entree.readLine();
        } catch (IOException e) {
            System.out.println(e);
        }
        int i = 0;
        while (ligne != null) {
            // System.out.println(i+ligne);
            give.add(ligne);
            i++;

            try {
                ligne = entree.readLine();
            } catch (IOException e) {
                insertError(e + "", "getFichier");
            }
        }
        entree.close();
        return give;
    }

    public int getMaxCodeMachine() {
        int id = 0;
        try {
            outResult = s.executeQuery("select max(ID_PRODUCT) from APP.PRODUCT ");
            while (outResult.next()) {
                id = outResult.getInt(1);
            }
            outResult.close();
        } catch (Throwable e) {
            insertError(e + "", "getmaxcodemachine");
        }
        return id;

    }

    public int getMaxInvoice(String NS) {
        int id = 0;
        try {
            String sql = "";
            if (NS.contains("R")) {
                sql = "select max(ID_REFUND) from APP.REFUND";
            } else {
                sql = "select max(ID_INVOICE) from APP.INVOICE ";
            }

            outResult = s.executeQuery(sql);
            while (outResult.next()) {
                id = outResult.getInt(1);
            }
            outResult.close();
        } catch (Throwable e) {
            insertError(e + "", "getmaxINV");
        }
        return id;

    }

    public int getRefInvoice(int id_invoce) {

        try {
            String sql = "select  ID_REFUND  from APP.REFUND where id_invoice=" + id_invoce;
            outResult = s.executeQuery(sql);
            while (outResult.next()) {
                return outResult.getInt(1);
            }
            outResult.close();
        } catch (Throwable e) {
            insertError(e + "", "getmaxINV");
        }
        return -1;

    }

    public int getPercentage(String aff, String table) {
        int id = 0;
        try {
            outResult = s.executeQuery("select percentage from APP.CLIENT" + table + " where  num_Affiliation='" + aff + "'");
            while (outResult.next()) {
                id = outResult.getInt(1);
            }
            outResult.close();
        } catch (Throwable e) {
            insertError(e + "", "getPercentage");
        }
        return id;

    }

//public int getIdFacture(String id_client,String date,int  tot, String id_employe,int tva)
//{
//int id =0;
//String id_clientdb="";
//String datedb="";
//int totdb=0;
//String id_employedb="";
//int tvadb=0;
//String heure="";
//boolean done=true;int compteur=0;
//try
//{
//outResult = s.executeQuery("select  max(ID_INVOICE) from APP.INVOICE ");
//while (outResult.next())
//{
//id =outResult.getInt(1);
//}
//
//while(done ||compteur>5)
//{
//
//compteur++;
//
//outResult = s.executeQuery("select  * from APP.INVOICE where ID_INVOICE= "+id);
//while (outResult.next())
//{
//
//    id_clientdb=outResult.getString(2);
//    datedb=outResult.getString(3);
//    totdb=outResult.getInt(4);
//    id_employedb=outResult.getString(5);
//    heure=""+outResult.getTimestamp(6);
//    tvadb=outResult.getInt(7);
//
//}
//if(id_clientdb.equals(id_client)&& datedb.equals(date)&& totdb==tot && id_employedb.equals(id_employe)&&tvadb==tva)
//{
//    done=false;
//
////JOptionPane.showMessageDialog(null,compteur,"ok "+id,JOptionPane.PLAIN_MESSAGE);
//
//
//}
//else
//id=id-1;
//
//}
// outResult.close();
//      }  catch (Throwable e)  {
//    insertError(e+"","getIdfacture");
//    }
// this.controlInvoice = new LinkedList();
//       controlInvoice.add("  FA"+id);
//        controlInvoice.add(heure);
// if(compteur==5)
//  insertError(id+" "+tot+""+date+""+id_client," manque idfacture");
//return id;
//
//}
    public int getIdFacture(String keyInvoice) {

        try {
            outResult = s.executeQuery("select  ID_INVOICE from APP.INVOICE WHERE KEY_INVOICE='" + keyInvoice + "'");
            while (outResult.next()) {
                return (outResult.getInt(1));
            }

            outResult.close();
        } catch (Throwable e) {
            insertError(e + "", "getIdfacture");
        }
        return -1;

    }

    public int getIdProforma(String keyInvoice) {

        try {
            outResult = s.executeQuery("select  ID_PROFORMA from APP.bon_proforma WHERE KEY_INVOICE='" + keyInvoice + "'");
            System.out.println("select  ID_PROFORMA from APP.bon_proforma WHERE KEY_INVOICE='" + keyInvoice + "'");
            
            while (outResult.next()) {
                return (outResult.getInt(1));
            }

            outResult.close();
        } catch (Throwable e) {
            insertError(e + "", "getIdPROFORMA");
        }
        return -1;
    }

    public int getMaxPresction(String keyPRESCRIPTION) {
        try {
            outResult = s.executeQuery("select  ID_PRESCRIPTION from APP.PRESCRIPTION WHERE KEY_PRESCRIPTION='" + keyPRESCRIPTION + "'");
            while (outResult.next()) {
                return (outResult.getInt(1));
            }
            outResult.close();
        } catch (Throwable e) {
            insertError(e + "", "keyPRESCRIPTION");
        }
        return -1;

    }

    public String getMaxHeurePresction(String keyPRESCRIPTION) {
        try {
            outResult = s.executeQuery("select  HEURE from APP.PRESCRIPTION WHERE KEY_PRESCRIPTION='" + keyPRESCRIPTION + "'");
            while (outResult.next()) {
                return (outResult.getString(1));
            }
            outResult.close();
        } catch (Throwable e) {
            insertError(e + "", "keyPRESCRIPTION");
        }
        return "2000-01-01 00:00:00";

    }

    String smartXml(Invoice inv,
            String Scheme_Code, String Scheme_Plan,
            String Date_Of_Birth, SmartPatient sp) {

        System.out.println(inv.heure);
        String Start_Date = inv.heure.substring(0, 11).replaceAll("/", "-");
        String Start_Time = inv.heure.substring(10);
        System.out.println("mrc time:" + Start_Date);

        String doXmlServices = "";

        LinkedList<Product> productPrint = inv.getProductListDB();
        for (int j = 0; j < productPrint.size(); j++) // ecrire les produits
        {
            Product p = productPrint.get(j);

            String name = p.productName;
            System.out.println("name::" + name);
            String doXmlDignostic = SmartFwdXml.doXmlPrescription("SP", "ISHYIGA", p.code, "Prescription",
                    p.DOSAGE, "BUP", p.code, p.productName, p.qty);

            String doXmlService = SmartFwdXml.doXmlService(sp.PracticeNumber, inv.id_invoice, "INV0" + inv.id_invoice,
                    Start_Date, Start_Time, "SH", "REASON", doXmlDignostic, j + 1);
            doXmlServices += doXmlService;
        }

        return SmartFwdXml.doXmlClaim(sp.PracticeNumber, inv.id_invoice, Start_Date, Start_Time, 0, doXmlServices, Scheme_Code, Scheme_Plan,
                inv.client.NOM_CLIENT, inv.client.PRENOM_CLIENT,
                Date_Of_Birth, getRRA("company"), inv.CASH, inv.CREDIT, sp,
                productPrint.size(), inv.tot);
    }

    int doFinishPresction(Invoice invoicePrint, SmartPatient sp) {
        String keyId = "" + new Date() + "" + invoicePrint.id_employe;
        String toSend = "";
        String toPrint = "";

        int FACT = -1;
        try {
            psInsert = conn.prepareStatement("insert into APP.PRESCRIPTION (NUM_AFFILIATION,DATE ,TOTAL,EMPLOYE,OBSERVATION"
                    + ",KEY_PRESCRIPTION,TARIF)  values (?,?,?,?,?,?,?)");

            psInsert.setString(1, invoicePrint.client.NUM_AFFILIATION);
            psInsert.setString(2, invoicePrint.date);
            psInsert.setDouble(3, invoicePrint.tot);
            psInsert.setString(4, invoicePrint.id_employe);
            psInsert.setString(5, invoicePrint.OBSERVATION_INVOICE);
            psInsert.setString(6, keyId);
            psInsert.setString(7, invoicePrint.id_client);
            psInsert.executeUpdate();
            psInsert.close();
            System.out.println("hapa tu");
            FACT = getMaxPresction(keyId);
            invoicePrint.id_invoice = FACT;
            toSend = FACT + ";" + invoicePrint.id_client + ";" + invoicePrint.client.NUM_AFFILIATION + ";"
                    + invoicePrint.date + ";" + invoicePrint.id_employe + ";" + new Date().toLocaleString() + ";" + invoicePrint.OBSERVATION_INVOICE + ":::";

            toPrint = invoicePrint.client.NUM_AFFILIATION + ";" + invoicePrint.id_client + ";" + invoicePrint.id_employe + ";"
                    + FACT + "-" + invoicePrint.OBSERVATION_INVOICE + "-"
                    + new Date().toLocaleString() + ":::";

            psInsert = conn.prepareStatement("insert into APP.LIST_PRESCRIPTION (ID_PRESCRIPTION,CODE_UNI,DOSAGE"
                    + ",QUANTITE,PRICE,LIST_ID_PRODUCT,AVAILABILITY)  values (?,?,?,?,?,?,?)");
            LinkedList<Product> productPrint = invoicePrint.getProductListDB();
            for (int j = 0; j < productPrint.size(); j++) // ecrire les produits
            {
                Product p = productPrint.get(j);

                psInsert.setInt(1, FACT);
                psInsert.setString(2, p.code);
                psInsert.setString(3, p.DOSAGE);
                psInsert.setInt(4, p.qty());
                psInsert.setDouble(5, p.currentPrice);
                psInsert.setInt(6, p.productCode);
                psInsert.setString(7, p.AVAILABILITY);
                psInsert.executeUpdate();

                toSend = toSend + "" + p.code + ";" + p.DOSAGE + ";" + p.qty() + "#";
                toPrint = toPrint + "" + p.code + ";" + p.qty() + ";" + p.DOSAGE + "#";
            }
            toSend = toSend + ">>>";
            toPrint = toPrint + ":::";

            System.out.println("TOPRINT:" + toPrint);

            invoicePrint.CODE_BAR = toPrint;
            invoicePrint.heure = getMaxHeurePresction(keyId);
            System.out.println("ntangiye print prescr end");

            CommCloud product_Cloud = new CommCloud("http://www.ishyiga" + getString("extension") + "/test_cloud/upload_prescription.php?soc=" + dbName + "&user=" + nom_empl, "data=" + toSend);
            System.out.println("" + product_Cloud.sendlot);

            System.out.println("" + invoicePrint.client);
            System.out.println("" + invoicePrint.client.AGE);
            String smartXml = smartXml(invoicePrint,
                    "Scheme_Code", "Scheme_Plan", invoicePrint.client.AGE, sp);

            System.out.println(smartXml);

            new PrintPrescription(invoicePrint, img1,
                    new String[]{getString("bp"),
                        getString("tel"), getString("tva")});

        } catch (Throwable e) {
            insertError(e + "", "FACT");
        }
        return FACT;
    }

    int doFinishProforma(Invoice invoicePrint, String ref, String name, String ext) {
        String keyIdInvoice = "" + new Date() + "" + invoicePrint.id_employe + ip;
        System.out.println("key invoice:" + keyIdInvoice + ip);
        try {
            Statement sD = conn.createStatement();
            psInsert = conn.prepareStatement("insert into APP.bon_proforma (NUM_CLIENT,DATE ,TOTAL,EMPLOYE,TVA,"
                    + "REDUCTION,KEY_INVOICE,REFERENCE,NOM_CLIENT,EXT_KEY)  values (?,?,?,?,?,?,?,?,?,?)");

            psInsert.setString(1, invoicePrint.id_client);
            psInsert.setString(2, invoicePrint.date);
            psInsert.setDouble(3, invoicePrint.tot);
            psInsert.setString(4, invoicePrint.id_employe);
            psInsert.setDouble(5, invoicePrint.tva);
            psInsert.setDouble(6, invoicePrint.REDUCTION);
            psInsert.setString(7, keyIdInvoice);
            psInsert.setString(8, ref);
            psInsert.setString(9, name);
            psInsert.setString(10, ext);
            psInsert.executeUpdate();
            psInsert.close();

            psInsert1 = conn.prepareStatement("insert into APP.LIST_PROFORMA (CODE_UNI,QUANTITE,PRICE,KEY_INVOICE,ID_PROFORMA,LIST_ID_PRODUCT)  values (?,?,?,?,?,?)");
            LinkedList<Product> productPrint = invoicePrint.getProductListDB();
            for (int j = 0; j < productPrint.size(); j++) // ecrire les produits
            {
                Product p = productPrint.get(j);
                int quantity = p.qty();

                psInsert1.setString(1, p.code);
                psInsert1.setInt(2, quantity);
                psInsert1.setDouble(3, p.currentPrice);
                psInsert1.setString(4, keyIdInvoice);
                psInsert1.setInt(5, -1);
                psInsert1.setInt(6, p.productCode);
                psInsert1.executeUpdate();
            }
            psInsert1.close();

            // Release the resources (clean up )
            int fact = getIdProforma(keyIdInvoice);
            System.out.println(fact + "key invoice:" + keyIdInvoice);
            //  update list, credit
            sD = conn.createStatement();
            String listUpdateSql = " update APP.LIST_PROFORMA  SET ID_PROFORMA=" + fact + ", ID_ANCIEN=" + fact + " where  key_invoice= '" + keyIdInvoice + "' and id_PROFORMA=" + -1 + "";
            sD.execute(listUpdateSql);
            return fact;
        } catch (Throwable e) {
            insertError(e + "", "doFinishPROFORMA");
        }
        return -1;
    }

    String keyFinishProforma(Invoice invoicePrint, String ref, String name) {
        String keyIdInvoice = "" + new Date() + "" + invoicePrint.id_employe + ip;
        System.out.println("key invoice:" + keyIdInvoice + ip);
        try {
            psInsert1 = conn.prepareStatement("insert into APP.LIST_PROFORMA (CODE_UNI,QUANTITE,PRICE,KEY_INVOICE,ID_PROFORMA,LIST_ID_PRODUCT)  values (?,?,?,?,?,?)");
            LinkedList<Product> productPrint = invoicePrint.getProductListDB();
            for (int j = 0; j < productPrint.size(); j++) // ecrire les produits
            {
                Product p = productPrint.get(j);
                int quantity = p.qty();
                psInsert1.setString(1, p.code);
                psInsert1.setInt(2, quantity);
                psInsert1.setDouble(3, p.currentPrice);
                psInsert1.setString(4, keyIdInvoice);
                psInsert1.setInt(5, -1);
                psInsert1.setInt(6, p.productCode);
                psInsert1.executeUpdate();
            }
            psInsert1.close();

//         // Release the resources (clean up )
//         int fact=  getIdProforma(keyIdInvoice);
//         System.out.println(fact+"key invoice:"+keyIdInvoice);
//         //  update list, credit
//         sD = conn.createStatement();
//         String listUpdateSql = " update APP.LIST_PROFORMA  SET ID_PROFORMA="+fact+", ID_ANCIEN="+fact+" where  key_invoice= '"+keyIdInvoice+"' and id_PROFORMA="+-1+"";
//         sD.execute(listUpdateSql); 
            return keyIdInvoice;
        } catch (Throwable e) {
            insertError(e + "", "doFinishPROFORMA");
        }
        return null;
    }

    int doFinishUpdateProforma(Invoice invoicePrint, String ref, String name, int id_pro, String keyIdInvoice) {
        System.out.println("key invoice:" + keyIdInvoice + ip);
        try {
            Statement sD = conn.createStatement();
            sD.execute("UPDATE APP.bon_proforma SET NUM_CLIENT='" + invoicePrint.id_client + "' "
                    + " ,DATE='" + invoicePrint.date + "' , TOTAL=" + invoicePrint.tot + " , EMPLOYE='" + invoicePrint.id_employe + "'"
                    + " , TVA=" + invoicePrint.tva + " , REDUCTION=" + invoicePrint.REDUCTION + " , REFERENCE='" + ref + "'"
                    + " , NOM_CLIENT='" + name + "' WHERE ID_PROFORMA=" + id_pro + "");

            sD.execute("UPDATE APP.LIST_PROFORMA SET ID_ANCIEN=" + id_pro + " "
                    + " ,ID_PROFORMA=0  WHERE ID_PROFORMA=" + id_pro + "");

            psInsert1 = conn.prepareStatement("insert into APP.LIST_PROFORMA (CODE_UNI,QUANTITE,PRICE,KEY_INVOICE,ID_PROFORMA,LIST_ID_PRODUCT)  values (?,?,?,?,?,?)");
            LinkedList<Product> productPrint = invoicePrint.getProductListDB();
            for (int j = 0; j < productPrint.size(); j++) // ecrire les produits
            {
                Product p = productPrint.get(j);
                int quantity = p.qty();

                psInsert1.setString(1, p.code);
                psInsert1.setInt(2, quantity);
                psInsert1.setDouble(3, p.currentPrice);
                psInsert1.setString(4, keyIdInvoice);
                psInsert1.setInt(5, -1);
                psInsert1.setInt(6, p.productCode);
                psInsert1.executeUpdate();
            }
            psInsert1.close();

            // Release the resources (clean up )
            int fact = getIdProforma(keyIdInvoice);
            System.out.println(fact + "key invoice:" + keyIdInvoice);
            //  update list, credit
            sD = conn.createStatement();
            String listUpdateSql = " update APP.LIST_PROFORMA  SET ID_PROFORMA=" + fact + ", ID_ANCIEN=" + fact + " where  key_invoice= '" + keyIdInvoice + "' and id_PROFORMA=" + -1 + "";
            sD.execute(listUpdateSql);
            return fact;
        } catch (Throwable e) {
            insertError(e + "", "doupdatePROFORMA");
        }
        return -1;
    }

    public String getString(String s) {
        String ret = "";
        Variable var = getParam("RUN", "MAIN", s + "MAIN");
        if (var != null) {
            ret = var.value2;
        }
        return ret;
    }

    public String getRRA(String s) {
        String ret = " ";
        Variable var = getParam("RRA", "RRA", s + "RRA");
        if (var != null) {
            ret = var.value2;
        }
        return ret;
    }

    public void printA5(int id_invoice, String ASS, Employe umukozi, String type) {
//String printStringUp="    Tel: 573489 - BP: 263 - KIGALI \n    N ° TVA : 10000111 ";
        int totale = 0;
        LinkedList<Product> prod2 = new LinkedList();
        lotList = new LinkedList();
        String client = "";
        String num_affiliation = "";
        String nom_client = "";
        String prenom_client = "";
        int percentage = 0;
        String secteur = "";
        String employeur = "";
        String date1 = "";
        String code = "";
        String heure = "";
        String mrc = "";
        String code_qte = "";
        int quantite = 0;
        double price = 0;
        String desi = "";
        String numero_quitance = "";
        String code_soc = "";
        String employe = "";
        int aff = 0;
        String external_data = "";
        String NAISSANCE = "";
        String SEXE = "";
        String LIEN = "";
        String BENEFICIAIRE = "";
        int payed = 0;
        int solde = 0;

        try {
//// Clients Rama

            String sql = "";

            if (ASS.equals("RAMA")) {
sql = ("select secteur,invoice.employe ,"
        + "date,code_uni,num_lot,quantite,list.price, name_product,num_affiliation,"
        + "nom_client,prenom_client,percentage,numero_quitance,"
        + "code_soc_rama,employeur,APP.LIST.tva,AGE,SEXE,LIEN,BENEFICIAIRE,solde,payed,"
        + "external_data,heure,mrc "
        + " from APP.PRODUCT,APP.INVOICE ,APP.LIST,APP.CREDIT,APP.CLIENT_RAMA "
        + "where APP.INVOICE.NUM_CLIENT ='RAMA' and APP.INVOICE.id_invoice =  " + id_invoice + "  "
        + " and APP.INVOICE.id_invoice = APP.credit.id_invoice and APP.client_RAMA.num_affiliation = "
        + "APP.credit.numero_affilie  and APP.INVOICE.id_invoice = APP.LIST.id_invoice  "
        + "and APP.PRODUCT.CODE=APP.LIST.CODE_UNI  order by APP.PRODUCT.name_product");

            } else {
                sql = ("select secteur,invoice.employe ,"
                        + "date,code_uni,num_lot,quantite,list.price, name_product,num_affiliation,"
                        + "nom_client,prenom_client,percentage,numero_quitance,"
                        + " code_soc_MMI,employeur,APP.LIST.tva ,AGE,SEXE,LIEN,BENEFICIAIRE,solde,payed,external_data,"
                        + "heure,mrc "
                        + " from APP.PRODUCT,APP.INVOICE ,APP.LIST,APP.CREDIT,APP.CLIENT "
                        + "where APP.INVOICE.NUM_CLIENT ='" + ASS + "' and APP.INVOICE.id_invoice =  " + id_invoice + "  "
                        + " and APP.INVOICE.id_invoice = APP.credit.id_invoice and APP.client.num_affiliation = "
                        + "APP.credit.numero_affilie  and APP.INVOICE.id_invoice = APP.LIST.id_invoice  "
                        + "and APP.PRODUCT.CODE=APP.LIST.CODE_UNI  order by APP.PRODUCT.name_product");

            }

            System.out.println(sql);

            outResult = s.executeQuery(sql);
            int i = 0;
            while (outResult.next()) {
                secteur = outResult.getString(1);
                employe = outResult.getString(2);
                date1 = outResult.getString(3);
                code = outResult.getString(4);
                quantite = outResult.getInt(6);
                price = outResult.getDouble(7);
                desi = outResult.getString(8);
                num_affiliation = outResult.getString(9);
                nom_client = outResult.getString(10);
                prenom_client = outResult.getString(11);
                percentage = outResult.getInt(12);
                numero_quitance = outResult.getString(13);
                code_soc = outResult.getString(14);
                employeur = outResult.getString(15);
                double Ptva = outResult.getDouble(16);
                NAISSANCE = outResult.getString(17);
                SEXE = outResult.getString(18);
                LIEN = outResult.getString(19);
                BENEFICIAIRE = outResult.getString(20);
                solde = outResult.getInt("solde");
                payed = outResult.getInt("payed");
                external_data = outResult.getString("external_data");
                heure = outResult.getString("heure");
                mrc = outResult.getString("mrc");
                i++;

//            System.out.println(numero_quitance); 
                int y = (int) ((((int) ((quantite * price)) * (percentage / 100.0))));
                int affilie = (int) (((((quantite * price)) * (percentage / 100.0))));
                int ramaP = (int) (((((quantite * price)) * (1 - percentage / 100.0))));

                aff = aff + y;
                totale = totale + (int) (quantite * price);
                Lot l = new Lot(code_soc, (i) + ". " + desi, "" + quantite, "" + setVirgule((int) price),
                        "" + setVirgule((int) (quantite * price)), "" + setVirgule(affilie), "" + setVirgule(ramaP),
                        "", "", "", "");
                prod2.add(new Product(1, desi, code, quantite, price, Ptva, code, "", "", this));

                lotList.add(l);
            }
            String[] sp = external_data.split(",");
////>SDC001000638,8,36,NS,02/09/2013 09:31:09,Q3CYMKAPARLNCBXR,AF4VYRUIXYVJVUWHKYZGE2RTDY 
            RRA_PACK rp = Main.tax.getRpack4(id_invoice, prod2,
                    "" + getRRA("rtype"), "S", heure, mrc);
            System.err.println("HAPA11111111111" + rp.MRC);
            RRA_PRINT2 printer = null;
            int margin = 5;
            String marginVar = getRRA("margin");
            try {
                margin = Integer.parseInt(marginVar);
            } catch (NumberFormatException ex) {
                System.err.println(marginVar + "   marginVar   " + ex);
            }
            if (Main.tax.rraClient.isRRA) {
                printer = new RRA_PRINT2(getRRA("company"), getString("bp"), getRRA("City"),
                        getRRA("tin"), "" + rp.ClientTin,
                        (rp.getTotal()),
                        (rp.getTotalA()), (rp.getTotalB()),
                        (rp.getTotalTaxes()), (rp.getTotalTaxes()),
                        (rp.getTotal()), sp[0], sp[1] + "/" + sp[2] + "  " + sp[3],
                        sp[6], sp[5], id_invoice, rp.MRC, "" + getRRA("rtype"), "S",
                        getRRA("version"), Main.tax.getNowTime(heure), sp[4], employe, s, margin);
            } else {
                printer = new RRA_PRINT2(getRRA("company"), getString("bp"), getRRA("City"),
                        getRRA("tin"), "" + rp.ClientTin,
                        (rp.getTotal()),
                        (rp.getTotalA()), (rp.getTotalB()),
                        (rp.getTotalTaxes()), (rp.getTotalTaxes()),
                        (rp.getTotal()), "", "" + "/" + "  ",
                        "", "", id_invoice, rp.MRC, "" + getRRA("rtype"), "S",
                        getRRA("version"), Main.tax.getNowTime(heure), "", employe, s, margin);
            }

            aff = (int) ((totale * (percentage / 100.0)) + 0.5);

            System.out.println("nahageze2" + getString("fact"));
            //PrintCommandM(printStringUp,printStringE,printStringW,printStringW1,down,merci,visa,reception);String EMPLOYEUR,String SECTEUR,String VISA,String CODE
// new Client(NUM_AFFILIATIONa,NOM_CLIENTa ,PRENOM_CLIENTa ,PERCENTAGEa,DATE_EXPa,ASSURANCEa ,EMPLOYEURa,SECTEURa,"",CODE.getText(),NAISSANCE.getText(),SEXE.getText(),link))
            new Print(lotList, this, type, date1,heure,
                    new Client(num_affiliation, nom_client, prenom_client, 0, 0, ASS, 
                            employeur, secteur, "", "",
                            NAISSANCE, SEXE, LIEN, BENEFICIAIRE),
                    id_invoice, (int) aff, totale,
                    new String[]{"", "", "", "", "", "", "", "", "", ""},
                    numero_quitance, employe, "" + ASS, printer, 1);

        } catch (Throwable e) {
            insertError(e + "", "printRetard");
        }
    }

    public LinkedList<String> printRetardRRACOPY2(int id_invoice, String printStringUp,
            Employe umukozi, RRA_CLIENT rc, String NS) {

        LinkedList<String> printR = new LinkedList<String>();
        LinkedList<Product> prod2 = new LinkedList();
        LinkedList<String> byose = new LinkedList<String>();
        Client infoClient = null;
        //String printStringUp="    Tel: 573489 - BP: 263 - KIGALI \n    N ° TVA : 10000111 ";
        String Rtype = "C";
        String Ttype = "S";
        if (NS.contains("R")) {
            Ttype = "R";
        }

        double totale = 0;
        LinkedList lotList = new LinkedList();
        boolean mut = false;
        boolean done = false;
        boolean annule = false;

        String ch = "";
        String printStringE = "";
        String printStringW = "";

        String client = "";
        String num_affiliation = "";
        String nom_client = "";
        String prenom_client = "";
        int percentage = 0;
        String secteur = "";
        String employeur = "";
        String date1 = "";
        String code = "";
        String lot1 = "";
        String heure5 = "";
        String employe = "";
        int quantite = 0;
        double price = 0;
        double Ptva = 0;
        int id_prod = 0;
        String desi = "";
        String numero_quitance = "";
        String code_soc = "";
        double aff = 0;
        String key_invoice = "";
        String code_qte = "";
        String mrc = "";
        String external_data = "";
        String copy_data = "";
        int neg = 1;
        int payed = 0;
        int solde = 0;
        int id_ref = 0;
        String tin = "";
        int id_invrefu = 0;
        try {
            if (Ttype.contains("R")) {
                // facture annule ou retour
                outResult = s.executeQuery("select *  from APP.REFUND  "
                        + " where APP.REFUND.id_refund = " + id_invoice + " order by id_refund");
                System.out.println("select *  from APP.REFUND   where APP.REFUND.id_refund = " + id_invoice + " order by id_refund");

                while (outResult.next()) {
                    id_invrefu = outResult.getInt(2);
                    heure5 = outResult.getString("HEURE_COPY");
                    date1 = outResult.getString(4);
                    client = outResult.getString(5);
                    num_affiliation = outResult.getString(6);
                    employe = outResult.getString(7);
                    code_qte = outResult.getString(8);
                    external_data = outResult.getString(10);
                    copy_data = outResult.getString(11);
                    tin = outResult.getString("tin");
                    id_ref = outResult.getInt(2);
                    mrc = outResult.getString("mrc");
                    annule = true;
                    Ttype = "R";
                }
                outResult.close();

                if (annule) {
                    JOptionPane.showMessageDialog(null, l(lang,"V_REFUND_MESSAGE"), "MESSAGE", JOptionPane.WARNING_MESSAGE);
                    infoClient = Main.tax.getClient(id_invrefu);
                    outResult = s.executeQuery("select *  from APP.REFUND_LIST   where APP.REFUND_LIST.id_refund = " + id_invoice + " order by id_refund");
                    System.out.println("select *  from APP.REFUND_LIST   where APP.REFUND_LIST.id_refund = " + id_invoice + " order by id_refund");

                    while (outResult.next()) {
                        code = outResult.getString(3);
                        lot1 = outResult.getString(4);
                        quantite = outResult.getInt(5);
                        price = outResult.getDouble(6);
                        id_prod = outResult.getInt(9);
                        Ptva = outResult.getDouble(10);
                        desi = outResult.getString(11);
                        Ttype = "R";
                        done = true;
//            Product p = new Product(id_invoice, desi, code, quantite, price, Ptva, code, "", "", this);
                        prod2.add(new Product(id_prod, desi, code, quantite, (price), Ptva, code, "", "", this));
                        System.out.println("**" + id_prod + "**" + desi + "**" + code + "**" + quantite + "**" + (price) + "**" + Ptva + "**" + code);

                        totale = totale + (quantite * price);
                        System.out.println("SIZEEEEEE:" + prod2.size());
                    }

                    outResult.close();
                    if (prod2.size() == 0) {
                        done = true;
                        System.out.println("codeQQQQQQ:" + code_qte);
                        String codeq = code_qte;
                        while (codeq.length() > 0) {
//        System.out.println(codeq.indexOf(")", codeq.indexOf('(')));
                            String rea = codeq.substring(codeq.indexOf('(') + 1, codeq.indexOf(")", codeq.indexOf('(')));
                            byose.add(rea);
                            codeq = codeq.substring(codeq.indexOf(")", codeq.indexOf('(')) + 1, codeq.length());
                            System.out.println("hapa:" + codeq);
                        }

                        for (int i = 0; i < byose.size(); i++) {
                            String kamwe = byose.get(i);
                            if (kamwe.indexOf("-") == kamwe.lastIndexOf("-")) {
                                StringTokenizer st1 = new StringTokenizer(kamwe);
                                code = st1.nextToken("-");
                                quantite = Integer.parseInt(st1.nextToken("-"));
                                price = 0;
                                Product p = getProduct(code, " ", price + ",");
                                desi = p.productName;
                                printStringE = printStringE + "\n" + quantite + "  " + desi;
                                printStringW = printStringW + "\n" + setVirgule((quantite * price));
                                prod2.add(new Product(p.productCode, desi, code, quantite, (price), p.tva, code, "", "", this));

                                totale = totale + (quantite * price);
                            } else {
                                StringTokenizer st1 = new StringTokenizer(kamwe);
                                code = st1.nextToken("-");
                                quantite = Integer.parseInt(st1.nextToken("-"));
                                price = Double.parseDouble(st1.nextToken("-")) / quantite;
                                Product p = getProduct(code, " ", price + ",");
                                desi = p.productName;
                                printStringE = printStringE + "\n" + quantite + "  " + desi;
                                printStringW = printStringW + "\n" + setVirgule((quantite * price));
                                totale = totale + (quantite * price);
                                prod2.add(new Product(p.productCode, desi, code, quantite, (price), p.tva, code, "", "", this));

                            }
                        }

                        if (employe.contains("-R")) {
                            annule = false;
                            done = false;
                        } else {
                            annule = true;
                        }
                    }
                }
            }

            if (!annule && !done) {

// Clients Cash
                outResult = s.executeQuery(""
                        + "select app.invoice.id_invoice,num_client,date,code_uni,num_lot,quantite"
                        + ",list.price,name_product,HEURE_COPY,invoice.employe,invoice.tva,invoice.key_invoice,"
                        + "list.tva,product.id_product,external_data,copy_data ,invoice.tin,invoice.mrc"
                        + " from APP.PRODUCT,APP.INVOICE ,APP.LIST where "
                        + "APP.INVOICE.NUM_CLIENT='CASHNORM' "
                        + "and APP.INVOICE.id_invoice = " + id_invoice + "  "
                        + "and APP.INVOICE.id_invoice = APP.LIST.id_invoice "
                        + " and  APP.PRODUCT.CODE=APP.LIST.CODE_UNI order"
                        + " by  app.invoice.id_invoice");

                while (outResult.next()) {

                    client = outResult.getString(2);
                    date1 = outResult.getString(3);
                    code = outResult.getString(4);
                    lot1 = outResult.getString(5);
                    quantite = outResult.getInt(6);
                    price = outResult.getDouble(7);
                    desi = outResult.getString(8);
                    Ptva = outResult.getDouble(13);
                    id_prod = outResult.getInt(14);
                    printStringE = printStringE + "\n" + quantite + "  " + desi;
                    printStringW = printStringW + "\n" + setVirgule((quantite * price));
                    totale = totale + (quantite * price);
                    done = true;
                    heure5 = "" + outResult.getTimestamp("HEURE_COPY");
                    employe = outResult.getString(10);
                    key_invoice = outResult.getString(12);
                    external_data = outResult.getString("external_data");
                    copy_data = outResult.getString("copy_data");
                    tin = outResult.getString("tin");
                    mrc = outResult.getString("mrc");
                    prod2.add(new Product(id_prod, desi, code, quantite, price, Ptva, code, "", "", this));

                }
                outResult.close();
/////CLIENT CASHNORM AVEC LE NOM

                outResult = s.executeQuery(""
                        + "select NUMERO_AFFILIE,NUMERO_QUITANCE FROM APP.CREDIT WHERE ID_INVOICE=" + id_invoice + "");
                while (outResult.next()) {
                    num_affiliation = outResult.getString(1);
                    numero_quitance = outResult.getString(2);
                }
                infoClient = new Client("DIVERS", numero_quitance, "", 100, 0, "CASHNORM", "", "", "", "");
                outResult.close();

                /// Clients Cash  Unipharma
                outResult = s.executeQuery(""
                        + "select app.invoice.id_invoice,num_client,date,code_uni,num_lot,"
                        + "quantite,list.price,name_product,num_affiliation,nom_client,"
                        + "prenom_client,percentage,numero_quitance,HEURE_COPY,invoice.employe,invoice.tva,"
                        + "list.tva,product.id_product,external_data,copy_data,invoice.tin,invoice.mrc  "
                        + "from APP.PRODUCT,APP.INVOICE ,APP.LIST,APP.CREDIT,APP.CLIENT_UNIPHARMA "
                        + "where APP.INVOICE.NUM_CLIENT ='CASHREMISE'  "
                        + "and APP.credit.numero_affilie =  APP.client_UNIPHARMA.num_affiliation "
                        + "and APP.INVOICE.id_invoice = " + id_invoice + " "
                        + "and APP.INVOICE.id_invoice = APP.credit.id_invoice "
                        + "and APP.INVOICE.id_invoice = APP.LIST.id_invoice "
                        + "AND APP.PRODUCT.CODE=APP.LIST.CODE_UNI "
                        + "order by app.invoice.id_invoice");

                System.out.println(""
                        + "select app.invoice.id_invoice,num_client,date,code_uni,num_lot,"
                        + "quantite,list.price,name_product,num_affiliation,nom_client,"
                        + "prenom_client,percentage,numero_quitance,heure,invoice.employe,invoice.tva,product.tva  "
                        + "from APP.PRODUCT,APP.INVOICE ,APP.LIST,APP.CREDIT,APP.CLIENT_UNIPHARMA "
                        + "where APP.INVOICE.NUM_CLIENT ='CASHREMISE'  "
                        + "and APP.credit.numero_affilie =  APP.client_UNIPHARMA.num_affiliation "
                        + "and APP.INVOICE.id_invoice = " + id_invoice + " "
                        + "and APP.INVOICE.id_invoice = APP.credit.id_invoice "
                        + "and APP.INVOICE.id_invoice = APP.LIST.id_invoice "
                        + "AND APP.PRODUCT.CODE=APP.LIST.CODE_UNI "
                        + "order by app.invoice.id_invoice");

                while (outResult.next()) {
                    client = outResult.getString(2);
                    date1 = outResult.getString(3);
                    code = outResult.getString(4);
                    lot1 = outResult.getString(5);
                    quantite = outResult.getInt(6);
                    price = outResult.getDouble(7);
                    desi = outResult.getString(8);
                    num_affiliation = outResult.getString(9);
                    nom_client = outResult.getString(10);
                    prenom_client = outResult.getString(11);
                    percentage = outResult.getInt(12);
                    numero_quitance = outResult.getString(13);
                    printStringE = printStringE + "\n" + quantite + "  " + desi;
                    printStringW = printStringW + "\n" + setVirgule((quantite * price));
                    totale = totale + (quantite * price);
                    heure5 = "" + outResult.getTimestamp("HEURE_COPY");
                    employe = outResult.getString(15);
                    Ptva = outResult.getDouble(17);
                    id_prod = outResult.getInt(18);
                    external_data = outResult.getString("external_data");
                    copy_data = outResult.getString("copy_data");
                    tin = outResult.getString("tin");
                    mrc = outResult.getString("mrc");
                    prod2.add(new Product(id_prod, desi, code, quantite, price, Ptva, code, "", "", this));

                }
                infoClient = new Client(num_affiliation, nom_client, prenom_client, percentage, 0, "CASHREMISE", "", "", "", "");
                //  Close the resultSet  and date ='"+d+"'
                outResult.close();

            //// Clients Rama
                outResult = s.executeQuery("select secteur,employe ,date,code_uni,num_lot,quantite,list.price, "
                        + "name_product,num_affiliation,nom_client,prenom_client,percentage,numero_quitance,"
                        + "code_soc_rama,employeur,invoice.tva,AGE,SEXE,LIEN,BENEFICIAIRE,list.tva,product.id_product,"
                        + "external_data,copy_data,invoice.tin,HEURE_COPY,invoice.mrc  "
                        + "from APP.PRODUCT,APP.INVOICE ,APP.LIST,APP.CREDIT,APP.CLIENT_RAMA where APP.INVOICE.NUM_CLIENT ='RAMA' and APP.INVOICE.id_invoice =  " + id_invoice + "   and APP.INVOICE.id_invoice = APP.credit.id_invoice and APP.client_RAMA.num_affiliation = APP.credit.numero_affilie  and APP.INVOICE.id_invoice = APP.LIST.id_invoice  and APP.PRODUCT.CODE=APP.LIST.CODE_UNI  order by app.invoice.id_invoice");

                int i = 0;
                while (outResult.next()) {
                    secteur = outResult.getString(1);
                    client = outResult.getString(2);
                    date1 = outResult.getString(3);
                    code = outResult.getString(4);
                    lot1 = outResult.getString(5);
                    quantite = outResult.getInt(6);
                    price = outResult.getDouble(7);
                    desi = outResult.getString(8);
                    num_affiliation = outResult.getString(9);
                    nom_client = outResult.getString(10);
                    prenom_client = outResult.getString(11);
                    percentage = outResult.getInt(12);
                    numero_quitance = outResult.getString(13);
                    code_soc = outResult.getString(14);
                    employeur = outResult.getString(15);
                    //  tva =outResult.getDouble(16);
                    Ptva = outResult.getDouble(21);
                    id_prod = outResult.getInt(22);
                    external_data = outResult.getString("external_data");
                    copy_data = outResult.getString("copy_data");
                    tin = outResult.getString("tin");
                    mrc = outResult.getString("mrc");
                    heure5 = "" + outResult.getTimestamp("HEURE_COPY");
                    i++;
//            System.out.println(numero_quitance); 
                    double y = (((((quantite * price)) * (percentage / 100.0))));
                    mut = true;

                    double affilie = (((((quantite * price)) * (percentage / 100.0))));
                    double ramaP = (((((quantite * price)) * (1 - percentage / 100.0))));

                    aff = aff + y;
                    totale = totale + (int) (quantite * price);

                    Lot l = new Lot(code_soc, (i) + ". " + desi, "" + quantite, "" + setVirgule(price), "" + setVirgule((int) (quantite * price)), "" + setVirgule(affilie), "" + setVirgule(ramaP), "", "", "", "");
                    lotList.add(l);
                    prod2.add(new Product(id_prod, desi, code, quantite, price, Ptva, code, "", "", this));
                }
                aff = (int) ((totale * (percentage / 100.0)) + 0.5);
                infoClient = new Client(num_affiliation, nom_client, prenom_client, percentage, 0, "RAMA", "" + employeur, "", "", "" + num_affiliation);

                ///// Clients Societe
                if (!mut) {
                    outResult = s.executeQuery(""
                            + "select secteur,num_client,date,code_uni,num_lot,quantite,list.price,name_product"
                            + ",num_affiliation,nom_client,prenom_client,percentage,numero_quitance,HEURE_COPY,invoice.employe"
                            + ",invoice.tva,solde,payed,list.tva,product.id_product,external_data,copy_data,"
                            + "invoice.tin,invoice.mrc "
                            + "from APP.PRODUCT,APP.INVOICE ,APP.LIST,APP.CREDIT,APP.CLIENT "
                            + "where  APP.INVOICE.NUM_CLIENT !='CASHNORM'"
                            + "and APP.INVOICE.NUM_CLIENT !='CASHREMISE'"
                            + "and APP.INVOICE.NUM_CLIENT !='RAMA'"
                            + "and APP.client.num_affiliation = APP.credit.numero_affilie "
                            + "and APP.INVOICE.id_invoice = " + id_invoice
                            + " and APP.INVOICE.id_invoice = APP.credit.id_invoice "
                            + "and APP.INVOICE.id_invoice = APP.LIST.id_invoice  "
                            + "AND APP.PRODUCT.CODE=APP.LIST.CODE_UNI"
                            + " order by app.invoice.id_invoice");

                    System.out.println(""
                            + "select secteur,num_client,date,code_uni,num_lot,quantite,list.price,name_product"
                            + ",num_affiliation,nom_client,prenom_client,percentage,numero_quitance,"
                            + "heure,employe,invoice.tva "
                            + "from APP.PRODUCT,APP.INVOICE ,APP.LIST,APP.CREDIT,APP.CLIENT "
                            + "where  APP.INVOICE.NUM_CLIENT !='CASHNORM'"
                            + "and APP.INVOICE.NUM_CLIENT !='CASHREMISE'"
                            + "and APP.INVOICE.NUM_CLIENT !='RAMA'"
                            + "and APP.client.num_affiliation = APP.credit.numero_affilie "
                            + "and APP.INVOICE.id_invoice = " + id_invoice
                            + " and APP.INVOICE.id_invoice = APP.credit.id_invoice "
                            + "and APP.INVOICE.id_invoice = APP.LIST.id_invoice  "
                            + "AND APP.PRODUCT.CODE=APP.LIST.CODE_UNI"
                            + " order by app.invoice.id_invoice");
                    i = 0;
                    while (outResult.next()) {

                        client = outResult.getString(2);
                        date1 = outResult.getString(3);
                        code = outResult.getString(4);
                        lot1 = outResult.getString(5);
                        quantite = outResult.getInt(6);
                        price = outResult.getDouble(7);
                        desi = outResult.getString(8);
                        num_affiliation = outResult.getString(9);
                        nom_client = outResult.getString(10);
                        prenom_client = outResult.getString(11);
                        percentage = outResult.getInt(12);
                        numero_quitance = outResult.getString(13);
                        printStringE = printStringE + "\n" + quantite + "  " + desi;
                        printStringW = printStringW + "\n" + setVirgule((quantite * price));
                        totale = totale + (quantite * price);
                        heure5 = "" + outResult.getTimestamp("HEURE_COPY");
                        employe = outResult.getString(15);
                        solde = outResult.getInt("solde");
                        payed = outResult.getInt("payed");
                        Ptva = outResult.getDouble(19);
                        id_prod = outResult.getInt(20);
                        external_data = outResult.getString("external_data");
                        copy_data = outResult.getString("copy_data");
                        tin = outResult.getString("tin");
                        mrc = outResult.getString("mrc");
                        i++;

                        double affilie = (totale - (totale * percentage * 0.01));
                        double ramaP = (totale * percentage * 0.01);

                        Lot l = new Lot(code_soc, (i) + ". " + desi, "" + quantite, "" + setVirgule(price), "" + setVirgule((int) (quantite * price)), "" + setVirgule(affilie), "" + setVirgule(ramaP), "", "", "", "");
                        lotList.add(l);
                        prod2.add(new Product(id_prod, desi, code, quantite, price, Ptva, code, "", "", this));
                    }
                    infoClient = new Client(num_affiliation, nom_client, prenom_client, percentage, 0, "" + client, "", "", "", "" + num_affiliation);
                    outResult.close();
                }
            }
            //GUSENNYA MODE YA PRINT
            System.out.println(Rtype + "+" + copy_data + ":" + external_data);
            if (Rtype.equals("C") && copy_data.equals("EMPTY") && !external_data.contains(",TS,") && !external_data.contains(",TR,")) {

                RRA_PACK rp = Main.tax.getRpack4(id_invoice, prod2,
                        Rtype, Ttype, "", mrc);
                if (rp != null) {
                    rp.ClientTin = tin;
                    String toSend = rc.getSaleString4(rp);
                    System.err.println(NS + "  toSend:*******" + toSend);
                    String data = rc.sendRequest("SEND RECEIPT", toSend, " ", "TIME", "", Rtype, Ttype, id_invoice);
                    if (data != null && !data.contains("AN ERROR MSG")) {

                        String heure_copy = "";
                        outResult = s.executeQuery("select HEURE_COPY from APP.INVOICE "
                                + " WHERE APP.INVOICE.id_invoice =  " + id_invoice + "");
                        System.err.println("select HEURE_COPY from APP.INVOICE "
                                + " WHERE APP.INVOICE.id_invoice =  " + id_invoice + "");
                        while (outResult.next()) {
                            heure_copy = outResult.getString(1);
                        }
                        if (NS.contains("S")) {

                            outResult = s.executeQuery("select HEURE_COPY from APP.INVOICE "
                                    + " WHERE APP.INVOICE.id_invoice =  " + id_invoice + "");
                            System.err.println("select HEURE_COPY from APP.INVOICE "
                                    + " WHERE APP.INVOICE.id_invoice =  " + id_invoice + "");
                            while (outResult.next()) {
                                heure_copy = outResult.getString(1);
                            }

                        } else if (NS.contains("R")) {
                            System.err.println("select HEURE_COPY from APP.INVOICE "
                                    + " WHERE APP.INVOICE.id_invoice =  " + id_invoice + "");
                            outResult = s.executeQuery("select HEURE_COPY from APP.REFUND "
                                    + " WHERE APP.REFUND.id_REFUND =  " + id_invoice + "");
                            while (outResult.next()) {
                                heure_copy = outResult.getString(1);
                            }
                        }

                        String donne[] = data.split("#");
                        if (donne.length == 3) {
                            String resp = "";
                            if (Ttype.equals("S")) {
                                neg = 1;
                                resp = getExternalDataV4("COPY_DATA", id_invoice, "INVOICE", "ID_INVOICE");
                            } else {
                                neg = -1;
                                resp = getExternalDataV4("COPY_DATA", id_invoice, "REFUND", "ID_REFUND");
                            }

                            String[] sp = resp.split(",");
////>SDC001000638,8,36,NS,02/09/2013 09:31:09,Q3CYMKAPARLNCBXR,AF4VYRUIXYVJVUWHKYZGE2RTDY 

                            int margin = 5;
                            String marginVar = getRRA("margin");
                            try {
                                margin = Integer.parseInt(marginVar);
                            } catch (NumberFormatException ex) {
                                System.err.println(marginVar + "   marginVar   " + ex);
                            }
                            LinkedList<String> Clientinfo = Leclient(infoClient);
                            RRA_PRINT2 printer = new RRA_PRINT2(getRRA("company"), getString("bp"), getRRA("City"),
                                    getRRA("tin"), "" + rp.ClientTin,
                                    (rp.getTotal()),
                                    (rp.getTotalA()), (rp.getTotalB()),
                                    (rp.getTotalTaxes()), (rp.getTotalTaxes()), (rp.getTotal()), sp[0], sp[1] + "/" + sp[2] + "  " + sp[3],
                                    sp[6], sp[5], id_invoice, rp.MRC, rp.getProductList, Rtype, Ttype,
                                    getRRA("version"), neg, id_ref, Clientinfo, Main.tax.getNowTime(heure_copy), sp[4], umukozi.NOM_EMPLOYE,
                                    numero_quitance, s, "", margin, 0, 0);

                            printR = new LinkedList<String>();
                            return printR;
                        } else {
                            printR = new LinkedList<String>();
                            return printR;
                        }
                    } else if (data.contains("AN ERROR MSG")) {
                        String[] msg = data.split("#");
                        JOptionPane.showMessageDialog(null, "" + msg[0], "" + msg[1], JOptionPane.WARNING_MESSAGE);
                    }
                } else {
                    JOptionPane.showMessageDialog(null, l(lang,"V_ISHYIGA_CONNECTION"), "ATTENTION", JOptionPane.WARNING_MESSAGE);
                    return null;
                }
            } else {
                JOptionPane.showMessageDialog(null, l(lang,"V_INVOICE_COPY"), "ATTENTION", JOptionPane.WARNING_MESSAGE);
                printR = new LinkedList<String>();
                return null;
            }

        } catch (Throwable e) {
            insertError(e + "", "printRetard");
        }
        if (mut) {
            printR = new LinkedList<String>();
        }
        return printR;

    }

    String setVirgule(int frw) {
        String setString = "" + frw;
        int l = setString.length();
        if (l < 2) {
            setString = "    " + frw;
        } else if (l < 3) {
            setString = "  " + frw;
        } else if (l < 4) {
            setString = "  " + frw;
        }
        String vir = ".";
        if (l > 3 && l <= 6) {
            setString = setString.substring(0, l - 3) + vir + setString.substring(l - 3);
        }
        if (l > 6) {

            String s1 = setString.substring(0, l - 3);
            int sl = s1.length();
            setString = s1.substring(0, sl - 3) + vir + s1.substring(sl - 3) + vir + setString.substring(l - 3);

        }

        return setString;
    }

    public void annuleRRA(int id_invoice, String nom, String RAIS, RRA_CLIENT rc) {
        LinkedList<List> todelete = new LinkedList();
        LinkedList<Lot> toAnnule = new LinkedList();
        LinkedList<Product> prod = new LinkedList();
        Client infoClient = null;

        String printStringUp = "    REBA NEZA KO IYI FACTURE ARI IYO GUSIBA ";
        int totale = 0;
        boolean done = false;
        boolean done1 = false;
        String printStringE = "";
        String printStringW = "";

        String client = "";
        String date1 = "";
        String code = "";
        String lot1 = "";
        int quantite = 0;
        double price = 0;
        double tva = 0;
        String desi = "";
        String num_affiliation = "";
        String nom_client = "";
        String prenom_client = "";
        int percentage = 0;
        int id_prod = 0;
        String numero_quitance = "";
        String key_invoice = "";
        String tin2 = "";
        String mrc = "";
        double CASH = 0;
        double CREDIT = 0;
        String heure = "";
        String izina = "";

        try {
            String sql = ""
                    + "select app.invoice.id_invoice,num_client,date,code_uni,num_lot,quantite,"
                    + " list.price,name_product,list.tva,product.id_product,INVOICE.key_invoice,"
                    + " ORIGINAL_PRICE,LIST.DATE_EXP,invoice.tin,INVOICE.CASH,INVOICE.CREDIT,invoice.mrc,invoice.name_client "
                    + " from APP.PRODUCT,APP.INVOICE ,APP.LIST where "
                    + " APP.INVOICE.NUM_CLIENT='CASHNORM' "
                    + " and APP.INVOICE.id_invoice = " + id_invoice + "  "
                    + " and APP.INVOICE.id_invoice = APP.LIST.id_invoice "
                    + " and  APP.PRODUCT.CODE=APP.LIST.CODE_UNI "
                    + " and app.invoice.comptabilise!='ANNULE' order by  app.invoice.id_invoice";
            System.out.println(sql);
            // Clients Cash
            outResult = s.executeQuery(sql);

            while (outResult.next()) {

                client = outResult.getString(2);
                date1 = outResult.getString(3);
                code = outResult.getString(4);
                lot1 = outResult.getString(5);
                quantite = outResult.getInt(6);
                price = outResult.getDouble(7);
                desi = outResult.getString(8);
                tva = outResult.getDouble(9);
                id_prod = outResult.getInt(10);
                key_invoice = outResult.getString(11);
                tin2 = outResult.getString("tin");
                CASH = outResult.getDouble("cash");
                CREDIT = outResult.getDouble("credit");
                mrc = outResult.getString("mrc");
                izina = outResult.getString("name_client");

                printStringE = printStringE + "\n  " + quantite + "  " + desi + "    " + (int) (quantite * price);
                printStringW = printStringW + "(" + code + "-" + quantite + "-" + (int) (price) + ")";

                todelete.add(new List(id_invoice, price, quantite, lot1, code));
                prod.add(new Product(id_prod, desi, code, quantite, price, tva, code, "", "", this));
                Lot sort = new Lot(id_prod, desi, code, lot1, "", 0, quantite, quantite, "");
                sort.prodLot = new Product(id_prod, desi, code, quantite, price, tva, code, "", "", this);
                sort.prodLot.originalPrice = outResult.getDouble(12);
                if (sort.prodLot.originalPrice != sort.prodLot.currentPrice) {
                    sort.prodLot.productName += "# Discount -" + (int) (100 - ((sort.prodLot.currentPrice * 100) / sort.prodLot.originalPrice)) + "%";
                }
                System.out.println(sort.prodLot.currentPrice + "  " + sort.prodLot.originalPrice);
                toAnnule.add(sort);
                totale = totale + (int) (quantite * price);
                done = true;
            }
            outResult.close();
            sql = ""
                    + "select app.invoice.id_invoice,num_client,date,code_uni,num_lot,"
                    + " quantite,list.price,name_product,num_affiliation,nom_client,"
                    + " prenom_client,percentage,numero_quitance,list.tva,product.id_product,"
                    + " INVOICE.key_invoice,ORIGINAL_PRICE,invoice.tin,INVOICE.CASH,INVOICE.CREDIT,invoice.mrc "
                    + " from APP.PRODUCT,APP.INVOICE ,APP.LIST,APP.CREDIT,APP.CLIENT_UNIPHARMA "
                    + " where APP.INVOICE.NUM_CLIENT ='CASHREMISE'  "
                    + " and APP.credit.numero_affilie =  APP.client_UNIPHARMA.num_affiliation "
                    + " and APP.INVOICE.id_invoice = " + id_invoice + " "
                    + " and APP.INVOICE.id_invoice = APP.credit.id_invoice "
                    + " and APP.INVOICE.id_invoice = APP.LIST.id_invoice "
                    + " AND APP.PRODUCT.CODE=APP.LIST.CODE_UNI "
                    + " and app.invoice.comptabilise!='ANNULE' order by app.invoice.id_invoice";
            System.out.println(sql);
       // Clients Cash  Unipharma

            if (!done) {
                outResult = s.executeQuery(sql);

                while (outResult.next()) {
                    client = outResult.getString(2);
                    date1 = outResult.getString(3);
                    code = outResult.getString(4);
                    lot1 = outResult.getString(5);
                    quantite = outResult.getInt(6);
                    price = outResult.getDouble(7);
                    desi = outResult.getString(8);
                    num_affiliation = outResult.getString(9);
                    nom_client = outResult.getString(10);
                    prenom_client = outResult.getString(11);
                    percentage = outResult.getInt(12);
                    numero_quitance = outResult.getString(13);
                    tva = outResult.getDouble(14);
                    id_prod = outResult.getInt(15);
                    key_invoice = outResult.getString(16);
                    tin2 = outResult.getString("tin");
                    mrc = outResult.getString("mrc");
                    CASH = outResult.getDouble("CASH");
                    CREDIT = outResult.getDouble("CREDIT");

                    printStringE = printStringE + "\n  " + quantite + "  " + desi + "    " + (int) (quantite * price);
                    printStringW = printStringW + "(" + code + "-" + quantite + "-" + (int) (quantite * price) + ")";
                    todelete.add(new List(id_invoice, price, quantite, lot1, code));
                    prod.add(new Product(id_prod, desi, code, quantite, price, tva, code, "", "", this));
                    Lot sort = new Lot(id_prod, desi, code, lot1, "", 0, quantite, quantite, "");
                    sort.prodLot = new Product(id_prod, desi, code, quantite, price, tva, code, "", "", this);
                    sort.prodLot.originalPrice = outResult.getDouble(17);
                    if (sort.prodLot.originalPrice != sort.prodLot.currentPrice) {
                        sort.prodLot.productName += "# Discount -" + (int) (100 - ((sort.prodLot.currentPrice * 100) / sort.prodLot.originalPrice)) + "%";
                    }
                    System.out.println(sort.prodLot.currentPrice + "  " + sort.prodLot.originalPrice);

                    toAnnule.add(sort);
                    totale = totale + (int) (quantite * price);
                    done1 = true;
                }
            }
            infoClient = new Client("" + num_affiliation, "" + nom_client, "" + prenom_client, percentage, 0, "CASHREMISE", "", "", "", "");

            //  Close the resultSet  and date ='"+d+"'
            outResult.close();

      // Clients Rama
            sql = ""
                    + " select app.invoice.id_invoice,num_client,date,code_uni,num_lot,quantite,list.price,"
                    + " name_product,num_affiliation,nom_client,prenom_client,percentage,numero_quitance,"
                    + " list.tva,product.id_product,INVOICE.key_invoice,ORIGINAL_PRICE,invoice.tin,"
                    + "INVOICE.CASH,INVOICE.CREDIT,invoice.mrc "
                    + " from APP.PRODUCT,APP.INVOICE ,APP.LIST,APP.CREDIT,APP.CLIENT_RAMA "
                    + " where APP.INVOICE.NUM_CLIENT ='RAMA'"
                    + " and APP.client_RAMA.num_affiliation = APP.credit.numero_affilie "
                    + " and APP.INVOICE.id_invoice = " + id_invoice + " "
                    + " and APP.INVOICE.id_invoice = APP.credit.id_invoice "
                    + " and APP.INVOICE.id_invoice = APP.LIST.id_invoice  "
                    + " and APP.PRODUCT.CODE=APP.LIST.CODE_UNI "
                    + " and app.invoice.comptabilise!='ANNULE' order by app.invoice.id_invoice";
            System.out.println(sql);
            // Clients Cash  Unipharma
            if (!done) {
                outResult = s.executeQuery(sql);
                while (outResult.next()) {
                    client = outResult.getString(2);
                    date1 = outResult.getString(3);
                    code = outResult.getString(4);
                    lot1 = outResult.getString(5);
                    quantite = outResult.getInt(6);
                    price = outResult.getDouble(7);
                    desi = outResult.getString(8);
                    num_affiliation = outResult.getString(9);
                    nom_client = outResult.getString(10);
                    prenom_client = outResult.getString(11);
                    percentage = outResult.getInt(12);
                    numero_quitance = outResult.getString(13);
                    tva = outResult.getDouble(14);
                    id_prod = outResult.getInt(15);
                    key_invoice = outResult.getString(16);
                    tin2 = outResult.getString("tin");
                    CASH = outResult.getDouble("CASH");
                    CREDIT = outResult.getDouble("CREDIT");
                    mrc = outResult.getString("mrc");

                    printStringE = printStringE + "\n  " + quantite + "  " + desi + "    " + (int) (quantite * price);
                    printStringW = printStringW + "(" + code + "-" + quantite + "-" + (int) (quantite * price) + ")";
                    todelete.add(new List(id_invoice, price, quantite, lot1, code));
                    prod.add(new Product(id_prod, desi, code, quantite, price, tva, code, "", "", this));
                    Lot sort = new Lot(id_prod, desi, code, lot1, "", 0, quantite, quantite, "");
                    sort.prodLot = new Product(id_prod, desi, code, quantite, price, tva, code, "", "", this);
                    sort.prodLot.originalPrice = outResult.getDouble(17);
                    if (sort.prodLot.originalPrice != sort.prodLot.currentPrice) {
                        sort.prodLot.productName += "# Discount -" + (int) (100 - ((sort.prodLot.currentPrice * 100) / sort.prodLot.originalPrice)) + "%";
                    }
                    System.out.println(sort.prodLot.currentPrice + "  " + sort.prodLot.originalPrice);
                    toAnnule.add(sort);
                    totale = totale + (int) (quantite * price);

                    done1 = true;
                }
            }
    //        infoClient = new Client(num_affiliation, nom_client, prenom_client, percentage, 0, "RAMA", "", "", "", "" + num_affiliation);
            infoClient=new Client(num_affiliation, nom_client, prenom_client,percentage, 0, "RAMA", "", "", "", ""+num_affiliation);
    

            //  Close the resultSet  and date ='"+d+"'
            outResult.close();

            // Clients Societe
            sql = ""
                    + "select app.invoice.id_invoice,num_client,date,code_uni,num_lot,quantite,list.price,name_product"
                    + ",num_affiliation,nom_client,prenom_client,percentage,numero_quitance,list.tva,product.id_product,"
                    + "INVOICE.key_invoice,ORIGINAL_PRICE,invoice.tin,INVOICE.CASH,INVOICE.CREDIT ,invoice.mrc"
                    + " from APP.PRODUCT,APP.INVOICE ,APP.LIST,APP.CREDIT,APP.CLIENT "
                    + " where  APP.INVOICE.NUM_CLIENT !='CASHNORM'"
                    + " and APP.INVOICE.NUM_CLIENT !='CASHREMISE'"
                    + " and APP.INVOICE.NUM_CLIENT !='RAMA'"
                    + " and APP.client.num_affiliation = APP.credit.numero_affilie "
                    + " and APP.INVOICE.id_invoice = " + id_invoice
                    + " and APP.INVOICE.id_invoice = APP.credit.id_invoice "
                    + " and APP.INVOICE.id_invoice = APP.LIST.id_invoice  "
                    + " AND APP.PRODUCT.CODE=APP.LIST.CODE_UNI"
                    + " and app.invoice.comptabilise!='ANNULE'  order by app.invoice.id_invoice";
            System.out.println(sql);

            if (!done) {
                outResult = s.executeQuery(sql);
                while (outResult.next()) {

                    client = outResult.getString(2);
                    date1 = outResult.getString(3);
                    code = outResult.getString(4);
                    lot1 = outResult.getString(5);
                    quantite = outResult.getInt(6);
                    price = outResult.getDouble(7);
                    desi = outResult.getString(8);
                    num_affiliation = outResult.getString(9);
                    nom_client = outResult.getString(10);
                    prenom_client = outResult.getString(11);
                    percentage = outResult.getInt(12);
                    numero_quitance = outResult.getString(13);
                    tva = outResult.getDouble(14);
                    id_prod = outResult.getInt(15);
                    key_invoice = outResult.getString(16);
                    tin2 = outResult.getString("tin");
                    CASH = outResult.getDouble("CASH");
                    CREDIT = outResult.getDouble("CREDIT");
                    mrc = outResult.getString("mrc");

                    printStringE = printStringE + "\n  " + quantite + "  " + desi + "    " + (int) (quantite * price);
                    printStringW = printStringW + "(" + code + "-" + quantite + "-" + (int) (quantite * price) + ")";
                    todelete.add(new List(id_invoice, price, quantite, lot1, code));
                    prod.add(new Product(id_prod, desi, code, quantite, price, tva, code, "", "", this));
                    Lot sort = new Lot(id_prod, desi, code, lot1, "", 0, quantite, quantite, "");
                    sort.prodLot = new Product(id_prod, desi, code, quantite, price, tva, code, "", "", this);
                    sort.prodLot.originalPrice = outResult.getDouble(17);
                    if (sort.prodLot.originalPrice != sort.prodLot.currentPrice) {
                        sort.prodLot.productName += "# Discount -" + (int) (100 - ((sort.prodLot.currentPrice * 100) / sort.prodLot.originalPrice)) + "%";
                    }
                    System.out.println(sort.prodLot.currentPrice + "  " + sort.prodLot.originalPrice);

                    toAnnule.add(sort);
                    totale = totale + (int) (quantite * price);
                    done1 = true;
                }
            }
            //infoClient = new Client(num_affiliation, nom_client, prenom_client, percentage, 0, "" + client, "", "", "", "" + num_affiliation);
            infoClient=new Client(num_affiliation, nom_client, prenom_client,percentage, 0, ""+client, "", "", "", ""+num_affiliation);
    

            if (done) {
                printStringUp = printStringUp + "\n    FACTURE N°    : " + id_invoice;
                printStringE = printStringE + "\n  ";
                printStringE = printStringE + "\n  TOTAL en RWF: " + (int) totale;
            } else {
                printStringUp = printStringUp + "\n    FACTURE N°    : " + id_invoice;
                printStringUp = printStringUp + "\n    " + date1;
                printStringUp = printStringUp + "\n    -------------------------------------------   ";
                printStringUp = printStringUp + "\n    Assureur            : " + client;
                printStringUp = printStringUp + "\n    N° Affiliation      : " + num_affiliation;
                printStringUp = printStringUp + "\n    Nom et Prenom : " + nom_client + " " + prenom_client;
                //   printStringUp=printStringUp+ "\n    Departement     : "+c.EMPLOYEUR;
                printStringUp = printStringUp + "\n    N° Quittance     : " + numero_quitance;
                printStringUp = printStringUp + "\n    -------------------------------------------   ";

                printStringE = printStringE + "\n  ";

                printStringE = printStringE + "\n  TOTAL en RWF: " + totale;

                printStringE = printStringE + "\n  " + client + " " + (100 - percentage) + " % : " + (totale - (int) (totale * percentage * 0.01));

                printStringE = printStringE + "\n  ADHERENT  " + percentage + " % : " + (int) (totale * percentage * 0.01);

            }
            //  Close the resultSet  and date ='"+d+"'
            outResult.close();
            if (done || done1) {
                int n = JOptionPane.showConfirmDialog(null,
                        printStringUp
                        + "\n" + printStringE,
                        "GUSIBA FACTURE ????? ", JOptionPane.YES_NO_OPTION);
                if (n == 0) {
                    System.out.println("KEY INV:" + key_invoice);
                    insertAnnuleListV4(toAnnule, id_invoice, key_invoice);
                    insertRefundListV4(toAnnule, id_invoice, key_invoice);

                    RRA_PACK rp = Main.tax.getRpack4(0, prod, getRRA("rtype"), "R", "", mrc);
                    if (rp != null) {
                        rp.ClientTin = tin2;
                        int id_refund = -1;
                        String toSend = rc.getSaleString2(rp);
                        String data = rc.sendRequest("SEND RECEIPT", toSend, key_invoice, "", " ", getRRA("rtype"), "R", id_invoice);
                        System.err.println("data:*******" + data);

                        if (data != null && !data.contains("ERROR")) {
                            String spp[] = data.split("#");
                            try {
                                id_refund = Integer.parseInt(spp[0]);
                                //id_refund=getMaxValue("MAX(ID_REFUND)","'"+key_invoice+"'", "REFUND", "KEY_INVOICE");
                            } catch (NumberFormatException e) {
                                insertError("" + e, "SDC ID_INVOICE");
                                deleteRefundList(key_invoice);
                                id_refund = -1;
                            }
                            if (id_refund != -1) {
                                Annule deleted = new Annule(id_invoice, num_affiliation, nom, client, printStringW, date1, RAIS);

                                if (retourMse(todelete, false)) {
                                    String rais = (JOptionPane.showInputDialog(null, " REASON"));
                                    rais = rais.replaceAll("'", "`");

                                    String time_rec_mrc = insertRefund(deleted, rais, rp, key_invoice, id_refund, id_invoice, CASH, CREDIT);
                                    insertDeleted(deleted, rais);
                                    deleteInvoice(id_invoice);
                                    String resp = getExternalDataV4("EXTERNAL_DATA", id_refund, "REFUND", "ID_REFUND");
                                    if (resp != null) {
                                        resp = resp.replaceAll(",,", ",#,#");
                                        String[] sp = resp.split(",");

                                        LinkedList<String> Clientinfo = Leclient(infoClient);
                                        int margin = 5;
                                        String marginVar = getRRA("margin");
                                        try {
                                            margin = Integer.parseInt(marginVar);
                                        } catch (NumberFormatException ex) {
                                            System.err.println(marginVar + "   marginVar   " + ex);
                                        }
                                        if (!Main.tax.rraClient.isRRA) {
                                            new RRA_PRINT2(getRRA("company"), getString("bp"), getRRA("City"), getRRA("tin"), "" + rp.ClientTin,
                                                    (rp.getTotal()), (rp.getTotalA()), (rp.getTotalB()), (rp.getTotalTaxes()), (rp.getTotalTaxes()),
                                                    (rp.getTotal()), "", "/" + "  ", "", "", id_refund, rp.MRC, rp.getProductList, getRRA("rtype"), "R",
                                                    getRRA("version"), -1, id_invoice, Clientinfo, Main.tax.getNowTime(time_rec_mrc), "", nom, numero_quitance, s, izina, margin, 0, 0);
                                        } else {
                                            new RRA_PRINT2(getRRA("company"), getString("bp"), getRRA("City"), getRRA("tin"), "" + rp.ClientTin,
                                                    (rp.getTotal()), (rp.getTotalA()), (rp.getTotalB()), (rp.getTotalTaxes()),
                                                    (rp.getTotalTaxes()), (rp.getTotal()), sp[0], sp[1] + "/" + sp[2] + "  " + sp[3],
                                                    sp[6], sp[5], id_refund, rp.MRC, rp.getProductList, getRRA("rtype"), "R", getRRA("version"), -1, id_invoice, Clientinfo, Main.tax.getNowTime(time_rec_mrc), sp[4], nom, numero_quitance, s, izina, margin, 0, 0);
                                        }
                                        JOptionPane.showMessageDialog(null, l(lang,"V_FACTURE_NUMERO") + id_invoice + l(lang,"V_ANNULEES"), "", JOptionPane.PLAIN_MESSAGE);
                                    } else {
                                        JOptionPane.showMessageDialog(null, l(lang,"V_UPDATE_FAIL") + id_invoice + " ", l(lang,"V_IKIBAZO"), JOptionPane.PLAIN_MESSAGE);
                                    }

                                } else {
                                    JOptionPane.showMessageDialog(null, l(lang,"V_ANNULER_FACTURE") + id_invoice + l(lang,"V_BIRANZE"), l(lang,"V_IKIBAZO"), JOptionPane.PLAIN_MESSAGE);
                                }
                            }
                        } else {
                            updateListAnnule(id_invoice);
                            JOptionPane.showMessageDialog(null, l(lang,"V_ISHYIGA_CONNECTION"), "ATTENTION", JOptionPane.WARNING_MESSAGE);
                        }
                    }
                }
            } else {
                JOptionPane.showMessageDialog(null, l(lang,"V_FACTURE_NUMERO") + id_invoice + l(lang,"V_NTIBAHO"), "", JOptionPane.PLAIN_MESSAGE);
            }

  // JOptionPane.showMessageDialog(null,"BL VENTES SANS DOWNLOADED PROBLEMES","MURAKOZE",JOptionPane.PLAIN_MESSAGE);
        } catch (Throwable e) {
            insertError(e + "", "ANNULE");
        }

    }

    public int retour(int id_invoice, int productcode, String nom) {
        LinkedList<List> todelete = new LinkedList();

        String printStringUp = "    REBA NEZA KO IYI FACTURE ARIYO ya RETOUR  ";
        int totale = 0;
        boolean done = false;
        boolean done1 = false;
        String printStringE = "";
        String printStringW = "";

        String client = "";
        String date1 = "";
        String code = "";
        String lot1 = "";
        int quantite = 0;
        double price = 0;
        String desi = "";
        String num_affiliation = "";
        String nom_client = "";
        String prenom_client = "";
        int percentage = 0;
        String numero_quitance = "";

        try {
            // Clients Cash
            outResult = s.executeQuery(""
                    + "select app.invoice.id_invoice,num_client,date,code_uni,num_lot,quantite"
                    + ",list.price,name_product from APP.PRODUCT,APP.INVOICE ,APP.LIST where "
                    + "APP.INVOICE.NUM_CLIENT='CASHNORM' "
                    + "and APP.INVOICE.id_invoice = " + id_invoice + "  "
                    + "and APP.INVOICE.id_invoice = APP.LIST.id_invoice "
                    + "and  APP.PRODUCT.CODE=APP.LIST.CODE_UNI "
                    + "AND APP.PRODUCT.ID_PRODUCT =" + productcode + ""
                    + " order by  app.invoice.id_invoice");

            while (outResult.next()) {

                client = outResult.getString(2);
                date1 = outResult.getString(3);
                code = outResult.getString(4);
                lot1 = outResult.getString(5);
                quantite = outResult.getInt(6);
                price = outResult.getDouble(7);
                desi = outResult.getString(8);
                printStringE = printStringE + "\n  " + quantite + "  " + desi + "    " + (int) (quantite * price);
                printStringW = printStringW + "(" + code + "-" + quantite + ")";
                todelete.add(new List(id_invoice, price, quantite, lot1, code));

                totale = totale + (int) (quantite * price);
                done = true;
            }
            outResult.close();

            // Clients Cash  Unipharma
            outResult = s.executeQuery(""
                    + "select app.invoice.id_invoice,num_client,date,code_uni,num_lot,"
                    + "quantite,list.price,name_product,num_affiliation,nom_client,"
                    + "prenom_client,percentage,numero_quitance "
                    + "from APP.PRODUCT,APP.INVOICE ,APP.LIST,APP.CREDIT,APP.CLIENT_UNIPHARMA "
                    + "where APP.INVOICE.NUM_CLIENT ='CASHREMISE'  "
                    + "and APP.credit.numero_affilie =  APP.client_UNIPHARMA.num_affiliation "
                    + "and APP.INVOICE.id_invoice = " + id_invoice + " "
                    + "and APP.INVOICE.id_invoice = APP.credit.id_invoice "
                    + "and APP.INVOICE.id_invoice = APP.LIST.id_invoice "
                    + "AND APP.PRODUCT.CODE=APP.LIST.CODE_UNI "
                    + "AND APP.PRODUCT.ID_PRODUCT=" + productcode + ""
                    + "order by app.invoice.id_invoice");

            while (outResult.next()) {
                client = outResult.getString(2);
                date1 = outResult.getString(3);
                code = outResult.getString(4);
                lot1 = outResult.getString(5);
                quantite = outResult.getInt(6);
                price = outResult.getDouble(7);
                desi = outResult.getString(8);
                num_affiliation = outResult.getString(9);
                nom_client = outResult.getString(10);
                prenom_client = outResult.getString(11);
                percentage = outResult.getInt(12);
                numero_quitance = outResult.getString(13);
                printStringE = printStringE + "\n  " + quantite + "  " + desi + "    " + (int) (quantite * price);
                printStringW = printStringW + "(" + code + "-" + quantite + ")";
                todelete.add(new List(id_invoice, price, quantite, lot1, code));
                totale = totale + (int) (quantite * price);
                done1 = true;

            }

            //  Close the resultSet  and date ='"+d+"'
            outResult.close();
            // Clients Rama

            outResult = s.executeQuery(""
                    + "select app.invoice.id_invoice,num_client,date,code_uni,num_lot,quantite,list.price,"
                    + "name_product,num_affiliation,nom_client,prenom_client,percentage,numero_quitance "
                    + "from APP.PRODUCT,APP.INVOICE ,APP.LIST,APP.CREDIT,APP.CLIENT_RAMA "
                    + "where APP.INVOICE.NUM_CLIENT ='RAMA'"
                    + "and APP.client_RAMA.num_affiliation = APP.credit.numero_affilie "
                    + "and APP.INVOICE.id_invoice = " + id_invoice + " "
                    + "and APP.INVOICE.id_invoice = APP.credit.id_invoice "
                    + "and APP.INVOICE.id_invoice = APP.LIST.id_invoice  "
                    + "and APP.PRODUCT.CODE=APP.LIST.CODE_UNI "
                    + "AND APP.PRODUCT.ID_PRODUCT=" + productcode + ""
                    + "order by app.invoice.id_invoice");

            while (outResult.next()) {
                client = outResult.getString(2);
                date1 = outResult.getString(3);
                code = outResult.getString(4);
                lot1 = outResult.getString(5);
                quantite = outResult.getInt(6);
                price = outResult.getDouble(7);
                desi = outResult.getString(8);
                num_affiliation = outResult.getString(9);
                nom_client = outResult.getString(10);
                prenom_client = outResult.getString(11);
                percentage = outResult.getInt(12);
                numero_quitance = outResult.getString(13);
                printStringE = printStringE + "\n  " + quantite + "  " + desi + "    " + (int) (quantite * price);
                printStringW = printStringW + "(" + code + "-" + quantite + ")";
                todelete.add(new List(id_invoice, price, quantite, lot1, code));
                totale = totale + (int) (quantite * price);
                done1 = true;
            }

            //  Close the resultSet  and date ='"+d+"'
            outResult.close();

            // Clients Societe
            outResult = s.executeQuery(""
                    + "select app.invoice.id_invoice,num_client,date,code_uni,num_lot,quantite,list.price,name_product"
                    + ",num_affiliation,nom_client,prenom_client,percentage,numero_quitance "
                    + "from APP.PRODUCT,APP.INVOICE ,APP.LIST,APP.CREDIT,APP.CLIENT "
                    + "where  APP.INVOICE.NUM_CLIENT !='CASHNORM' "
                    + "and APP.INVOICE.NUM_CLIENT !='CASHREMISE' "
                    + "and APP.INVOICE.NUM_CLIENT !='RAMA'"
                    + "and APP.client.num_affiliation = APP.credit.numero_affilie "
                    + "and APP.INVOICE.id_invoice = " + id_invoice
                    + " and APP.INVOICE.id_invoice = APP.credit.id_invoice "
                    + " and APP.INVOICE.id_invoice = APP.LIST.id_invoice  "
                    + " AND APP.PRODUCT.CODE=APP.LIST.CODE_UNI "
                    + " AND APP.PRODUCT.ID_PRODUCT=" + productcode + " "
                    + " order by app.invoice.id_invoice");

            while (outResult.next()) {

                client = outResult.getString(2);
                date1 = outResult.getString(3);
                code = outResult.getString(4);
                lot1 = outResult.getString(5);
                quantite = outResult.getInt(6);
                price = outResult.getDouble(7);
                desi = outResult.getString(8);
                num_affiliation = outResult.getString(9);
                nom_client = outResult.getString(10);
                prenom_client = outResult.getString(11);
                percentage = outResult.getInt(12);
                numero_quitance = outResult.getString(13);
                printStringE = printStringE + "\n  " + quantite + "  " + desi + "    " + (int) (quantite * price);
                printStringW = printStringW + "(" + code + "-" + quantite + ")";
                todelete.add(new List(id_invoice, price, quantite, lot1, code));
                totale = totale + (int) (quantite * price);
                done1 = true;

            }
            if (done) {
                printStringUp = printStringUp + "\n    FACTURE N°    : " + id_invoice;
                printStringE = printStringE + "\n  ";

                printStringE = printStringE + "\n  TOTAL en RWF: " + (int) totale;

            } else {

                printStringUp = printStringUp + "\n    FACTURE N°    : " + id_invoice;
                printStringUp = printStringUp + "\n    " + date1;
                printStringUp = printStringUp + "\n    -------------------------------------------   ";
                printStringUp = printStringUp + "\n    Assureur            : " + client;
                printStringUp = printStringUp + "\n    N° Affiliation      : " + num_affiliation;
                printStringUp = printStringUp + "\n    Nom et Prenom : " + nom_client + " " + prenom_client;
                //   printStringUp=printStringUp+ "\n    Departement     : "+c.EMPLOYEUR;
                printStringUp = printStringUp + "\n    N° Quittance     : " + numero_quitance;
                printStringUp = printStringUp + "\n    -------------------------------------------   ";

                printStringE = printStringE + "\n  ";

                printStringE = printStringE + "\n  TOTAL en RWF: " + totale;

                printStringE = printStringE + "\n  " + client + " " + (100 - percentage) + " % : " + (totale - (int) (totale * percentage * 0.01));

                printStringE = printStringE + "\n  ADHERENT  " + percentage + " % : " + (int) (totale * percentage * 0.01);
                totale = (totale);

            }
            //  Close the resultSet  and date ='"+d+"'
            outResult.close();

            if (done || done1) {

                int n = JOptionPane.showConfirmDialog(null,
                        printStringUp
                        + "\n " + printStringE
                        + "\n MONTANT RETOUR " + totale,
                        "RETOUR ????? ", JOptionPane.YES_NO_OPTION);
         //  JOptionPane.showMessageDialog(null,"FACTURE N° "+n+" ANNULEE","",JOptionPane.PLAIN_MESSAGE);
                if (n == 0) {

                    Annule deleted = new Annule(id_invoice, num_affiliation, nom + " R", client, printStringW, date1, "RETOUR");

                    if (retourMse(todelete, false)) {
                        String rais = (JOptionPane.showInputDialog(null, l(lang,"V_IMPAMVU")));
                        insertDeleted(deleted, rais);
                        // JOptionPane.showMessageDialog(null,"FACTURE N° "+id_invoice+" ANNULEE","",JOptionPane.PLAIN_MESSAGE);
                    }
                } else {
                    totale = 0;
                }
            } else {
                JOptionPane.showMessageDialog(null, l(lang,"V_FACTURE_NUMERO") + id_invoice + l(lang,"V_NTIBAHO"), "", JOptionPane.PLAIN_MESSAGE);
            }

        } catch (Throwable e) {
            insertError(e + "", "retour ");
        }
        return totale;
    }

    public boolean retourMse(LinkedList<List> todelete, boolean retour) {
        boolean done = true;
        for (int i = 0; i < todelete.size(); i++) {
            try {
                List l = todelete.get(i);

                if (getLot(l)) {
                    Statement sD = conn.createStatement();

//                        String createString1 = " update invoice set TOTAL=TOTAL-"+(int)(l.price*l.qty)+", TVA=TVA-"+(int)((l.tva*l.qty)+0.5)+" where id_invoice=" + l.id_invoice ;
//                        sD.execute(createString1);                         
                    String createString1 = " UPDATE list SET BON_LIVRAISON='ANNULE' where id_invoice=" + l.id_invoice + " and code_uni='" + l.code + "' and num_lot='" + l.lot + "'";
                    sD.execute(createString1);

                } else {
                    done = false;
                    JOptionPane.showMessageDialog(null, l(lang,"V_ANNULER_FACTURE") + l.id_invoice + " umuti wa " + l.code + l(lang,"V_BIRANZE"), l(lang,"V_IKIBAZO"), JOptionPane.PLAIN_MESSAGE);

                    break;
                }

            } catch (Throwable e) {
                insertError(e + "", "retourMse");
            }

        }
        return done;
    }

    public boolean getLot(List l) {
        boolean done = false;
        double revient = l.prix_revient;

        try {
            Lot lotToUpdate = null;
            outResult = s.executeQuery(""
                    + "select * from APP.LOT,APP.PRODUCT"
                    + " where code='" + l.code + "' and "
                    + "APP.LOT.ID_PRODUCT=APP.PRODUCT.ID_PRODUCT "
                    + "and id_LotS ='" + l.lot + "' ");

            while (outResult.next()) {
                lotToUpdate = new Lot(outResult.getInt(1), "", outResult.getString(10), outResult.getString(3), "", outResult.getInt(2), 0, 0, outResult.getString(7));
                done = true;
                break;
            }
            outResult.close();

            if (lotToUpdate != null) {
                s = conn.createStatement();
                String createString1 = ""
                        + "update APP.LOT set QTY_LIVE=QTY_LIVE+" + l.qty + ""
                        + "where id_product=" + lotToUpdate.productCode + " "
                        + "and ID_LOTS= '" + lotToUpdate.id_LotS + "'"
                        + "and BON_LIVRAISON= '" + lotToUpdate.bon_livraison + "'"
                        + "and ID_LOT=" + lotToUpdate.id_lot;
                s.execute(createString1);

            } else {
                outResult = s.executeQuery(""
                        + "select * from APP.USED_LOT,APP.PRODUCT"
                        + " where code='" + l.code + "' and "
                        + "APP.USED_LOT.ID_PRODUCT=APP.PRODUCT.ID_PRODUCT "
                        + "and id_LotS ='" + l.lot + "' ");
                while (outResult.next()) {
                    lotToUpdate = new Lot(outResult.getInt(1), "", outResult.getString(10), outResult.getString(3), outResult.getString(4), outResult.getInt(2), outResult.getInt(5), outResult.getInt(6), outResult.getString(7));
                    revient = outResult.getDouble("revient");
                    done = true;
                    break;
                }
                outResult.close();

                if (lotToUpdate != null) {
                    psInsert = conn.prepareStatement("insert into APP.LOT(ID_PRODUCT,ID_LOT,ID_LOTS,DATE_EXP,QTY_START,QTY_LIVE,BON_LIVRAISON,REVIENT,CODE_LOT)  values (?,?,?,?,?,?,?,?,?)");
                    psInsert.setInt(1, lotToUpdate.productCode);
                    psInsert.setInt(2, lotToUpdate.id_lot);
                    psInsert.setString(3, lotToUpdate.id_LotS);
                    psInsert.setString(4, lotToUpdate.dateExp);
                    psInsert.setInt(5, lotToUpdate.qtyStart);
                    psInsert.setInt(6, l.qty);
                    psInsert.setString(7, lotToUpdate.bon_livraison);
                    psInsert.setDouble(8, revient);
                    psInsert.setString(9, l.code);
                    psInsert.executeUpdate();
                    psInsert.close();

                    s = conn.createStatement();
                    String createString1 = " "
                            + "delete from USED_LOT where ID_LOT=" + lotToUpdate.id_lot + " "
                            + "and BON_LIVRAISON='" + lotToUpdate.bon_livraison + "' "
                            + "and id_LotS= '" + lotToUpdate.id_LotS + "' "
                            + "and DATE_EXP= '" + lotToUpdate.dateExp + "' "
                            + "and ID_PRODUCT= " + lotToUpdate.productCode;

                    s.execute(createString1);
                }
            }

        } catch (Throwable e) {
            insertError(e + "", " get lot");
        }
        return done;
    }

    public void insertDeleted(Annule d, String raison) {
        try {
            s = conn.createStatement();
            String createString1 = ""
                    + "update APP.ANNULE set DATE='" + d.DATE + "', NUM_CLIENT='" + d.NUM_CLIENT + "', "
                    + "NUMERO_AFFILIE='" + d.NUMERO_AFFILIE + "', EMPLOYE='" + d.EMPLOYE + "', "
                    + "CODE_QTE='" + d.CODE_QTE + "',RAISON='" + raison + "' where id_invoice=" + d.ID_INVOICE + "";
            s.execute(createString1);
//        
//        
//psInsert = conn.prepareStatement("insert into APP.ANNULE(ID_INVOICE,DATE,NUM_CLIENT ,NUMERO_AFFILIE ,EMPLOYE ,CODE_QTE,RAISON )  values (?,?,?,?,?,?,?)");
//
//    psInsert.setInt(1,d.ID_INVOICE);
//    psInsert.setString(2,d.DATE);
//    psInsert.setString(3,d.NUM_CLIENT);
//    psInsert.setString(4,d.NUMERO_AFFILIE);
//    psInsert.setString(5,d.EMPLOYE);
//    psInsert.setString(6,d.CODE_QTE);
//    psInsert.setString(7,raison);
//        psInsert.executeUpdate();
//        psInsert.close();
        } catch (Throwable e) {
            insertError(e + "", "insertDeleted");
        }
    }

    public String insertRefund(Annule d, String raison, RRA_PACK rp, String key, int id_refund, int id_invoice, double CASH, double CREDIT) {
        try {
            s = conn.createStatement();

            String listUpdateSql = (" update APP.REFUND  SET ID_INVOICE=" + id_invoice + ", NUM_CLIENT='" + d.NUM_CLIENT + "', DATE='" + d.DATE + "'"
                    + " , TOTAL=" + RRA_PRINT2.setSansVirgule(rp.getTotal()) + ", TVA=" + RRA_PRINT2.setSansVirgule(rp.getTotalTaxes()) + " "
                    + ", EMPLOYE='" + d.EMPLOYE + "' ,CODE_QTE='" + d.CODE_QTE + "',RAISON='" + raison + "' "
                    + " , NUMERO_AFFILIE='" + d.NUMERO_AFFILIE + "', TOTAL_A=" + RRA_PRINT2.setSansVirgule(rp.getTotalA()) + ","
                    + " TOTAL_B=" + RRA_PRINT2.setSansVirgule(rp.getTotalB()) + " , TOTAL_C=" + RRA_PRINT2.setSansVirgule(rp.getTotalD()) + ", "
                    + "TOTAL_D=" + RRA_PRINT2.setSansVirgule(rp.getTotalD()) + " , TAXE_A=" + RRA_PRINT2.setSansVirgule(rp.tvaPriceA) + ", "
                    + "TAXE_B=" + RRA_PRINT2.setSansVirgule(rp.tvaPriceB) + " , TAXE_C=" + RRA_PRINT2.setSansVirgule(rp.tvaPriceC) + ", "
                    + "TAXE_D=" + RRA_PRINT2.setSansVirgule(rp.tvaPriceD) + ", MRC='" + rp.MRC + "', TIN='" + rp.ClientTin + "'"
                    + ", CASH=" + CASH + ", CREDIT=" + CREDIT + ""
                    + " where  key_invoice= '" + key + "' and id_refund=" + id_refund + "");

            System.out.println(listUpdateSql);
            s.execute(listUpdateSql);
            System.out.println(""
                    + "update APP.REFUND_LIST set ID_REFUND=" + id_refund + " "
                    + " where  key_invoice= '" + key + "' and id_invoice=-1");
            String createString1 = "update APP.REFUND_LIST set ID_REFUND=" + id_refund + " "
                    + " where  key_invoice= '" + key + "' and id_REFUND=-1";
            s.execute(createString1);

            outResult = s.executeQuery("select  heure from APP.REFUND WHERE  ID_REFUND=" + id_refund + "");
            while (outResult.next()) {
                return (outResult.getString(1));
            }
            outResult.close();
        } catch (Throwable e) {
            insertError(e + "", "updateRefund");
        }
        return "";
    }

    public void deleteInvoice(int id_invoice) {
        try {
            Statement sD = conn.createStatement();
//            String createString1 = " delete from invoice where id_invoice=" + id_invoice;
            String createString1 = " update invoice set comptabilise='ANNULE' where id_invoice=" + id_invoice;
            sD.execute(createString1);
//            createString1 = " delete from credit where id_invoice=" + id_invoice;
            createString1 = " update credit set cause='ANNULE' where id_invoice=" + id_invoice;
            sD.execute(createString1);

        } catch (Throwable e) {
            insertError(e + "", "deleteInvoice");
        }
    }

    public void bcpTxt() throws FileNotFoundException, IOException {
        String file = "BL_VENTES/" + (new Date().getMonth() + 1) + "/" + datez + ".txt";
         //   JOptionPane.showMessageDialog(null,file,"Downloading Sales",JOptionPane.PLAIN_MESSAGE);

        File f = new File(file);

        PrintWriter sorti = new PrintWriter(new FileWriter(f));

        try {
            // Clients Cash
            outResult = s.executeQuery(""
                    + "select app.invoice.id_invoice,num_client,date,code_uni,num_lot,quantite"
                    + ",list.price,name_product from APP.PRODUCT,APP.INVOICE ,APP.LIST where "
                    + "APP.INVOICE.id_invoice = APP.LIST.id_invoice "
                    + "and APP.INVOICE.date = '" + datez + "' "
                    + "AND APP.PRODUCT.CODE=APP.LIST.CODE_UNI order"
                    + " by app.invoice.id_invoice");

            while (outResult.next()) {

                int id_invoice = outResult.getInt(1);
                String client = outResult.getString(2);
                String date1 = outResult.getString(3);
                String code = outResult.getString(4);
                String lot1 = outResult.getString(5);
                int quantite = outResult.getInt(6);
                double price = outResult.getDouble(7);
                String desi = outResult.getString(8);

                String factureSage = "13;BL" + id_invoice + ";" + client + ";" + date1 + ";" + code + ";" + desi + ";" + lot1 + ";" + quantite + ";" + price;
                sorti.println(factureSage);
                //  JOptionPane.showMessageDialog(null,factureSage,"Downloaded",JOptionPane.PLAIN_MESSAGE);

            }
            outResult.close();

        } catch (Throwable e) {
            insertError(e + "", "bcupText");
        }
        sorti.close();
    }

    public void closeConnection() {
        try {
//  JOptionPane.showMessageDialog(null,ip," ip1 ",JOptionPane.PLAIN_MESSAGE);

            if (ip.equals("elephant/192.168.0.113")) {
                bcpTxt();
            }

            conn.close();

            if (driver.equals("org.apache.derby.jdbc.EmbeddedDriver")) {
                boolean gotSQLExc = false;
                try {
                    DriverManager.getConnection("jdbc:derby:;shutdown=true");
                } catch (SQLException se) {
                    if (se.getSQLState().equals("XJ015")) {
                        gotSQLExc = true;
                    }
                }
                if (!gotSQLExc) {
                    System.out.println("Database did not shut down normally");
                } else {
                    System.out.println("Database shut down normally");
                }
            }
        } catch (Throwable e) {
        }
    }

    public void upDateCodeBar(String codebar, int code) {
        try {
            s = conn.createStatement();
            String createString1 = " update APP.PRODUCT set CODE_BAR='" + codebar + "' where ID_PRODUCT=" + code + "";
            s.execute(createString1);

        } catch (Throwable e) {
            JOptionPane.showMessageDialog(null, e, l(lang,"V_CODEBAR_EXIST"), JOptionPane.PLAIN_MESSAGE);

        }

    }

    void addOneLot(int p, Lot neW) {

        try {
            psInsert = conn.prepareStatement("insert into APP.LOT(ID_PRODUCT,ID_LOT,ID_LOTS,DATE_EXP,QTY_START,QTY_LIVE,BON_LIVRAISON)  values (?,?,?,?,?,?,?)");

            psInsert.setInt(1, p);
            psInsert.setInt(2, neW.id_lot);
            psInsert.setString(3, neW.id_LotS);
            psInsert.setString(4, neW.dateExp);
            psInsert.setInt(5, neW.qtyStart);
            psInsert.setInt(6, neW.qtyLive);
            psInsert.setString(7, neW.bon_livraison);
            psInsert.executeUpdate();
            psInsert.close();
        } catch (Throwable e) {
            insertError(e + "", "addonecodeBar");
        }

    }

    void addOneCode() {

        doworkOther("", "");
        try {
            for (int i = 0; i < allProduct.size(); i++) {
                psInsert = conn.prepareStatement("insert into CODEBAR(CODE_MACHINE,CODE_BAR)  values (?,?)");
                psInsert.setInt(1, i);
                psInsert.setString(2, "c" + i);

                psInsert.executeUpdate();
            }
            // Release the resources (clean up )
            psInsert.close();
        } catch (Throwable e) {
            insertError(e + "", "addonecodeBar");
        }

    }

    Variable getParama(String famille, String nom) {
        /// System.out.println("select  * from APP.VARIABLES where APP.variables.famille_variable= '"+nom+"' and APP.variables.value2_variable= and APP.variables.famille_variable= '"+famille+"' ");

        Variable par = null;
        try {
            System.out.println("NOM VAR:" + nom + " FAMILLE:" + famille);
            outResult = s.executeQuery("select  * from APP.VARIABLES where APP.variables.nom_variable= '" + nom + "'  and APP.variables.famille_variable= '" + famille + "' ");

            while (outResult.next()) {
                //  System.out.println(outResult.getString(2));
                String num = outResult.getString(1);
                String value = outResult.getString(2);
                String fam = outResult.getString(3);
                String value2 = outResult.getString(4);
                par = (new Variable(num, value, fam, value2));
            }

            outResult.close();
        } catch (Throwable e) {
            insertError(e + "", "getparm autoout");
        }
//System.out.println("PAR:"+par.value);
        return par;
    }

    void updateParameter1(LinkedList<Variable> par) {
        for (int i = 0; i < par.size(); i++) {

            Variable v = par.get(i);
            //System.out.println(" update APP.VARIABLES set value_variable ='" + v.value + "',value2_variable ='" + v.value2 + "' where nom_variable='" + v.nom + "' AND famille_variable='" + v.famille + "'");
            try {

                s.execute("update APP.VARIABLES set value_variable ='" + v.value + "'  where nom_variable='" + v.nom + v.value2 + "' AND famille_variable='" + v.famille + "'");
            } catch (SQLException ex) {
                insertError(ex + "", "updateparam" + v.nom);
            }
        }
    }

    void updateParameter2(LinkedList<Variable> par, String soc) {
        for (int i = 0; i < par.size(); i++) {

            Variable v = par.get(i);
            // System.out.println(" update APP.VARIABLES set value_variable ='" + v.value + "',value2_variable ='" + v.value2 + "' where nom_variable='" + v.nom+soc + "' AND famille_variable='" + v.famille + "'");
            try { 
                s.execute(" update APP.VARIABLES set value_variable ='" + v.value + "'  where nom_variable='" + v.nom + soc + "' AND famille_variable='" + v.famille + "'");
            } catch (SQLException ex) {
                insertError(ex + "", "updateparam" + v.nom);
            }
        }
    }

    Variable getParam(String famille, String mode, String nom) {
        // System.out.println("select  * from APP.VARIABLES where APP.variables.nom_variable= '"+nom+"'   and APP.variables.famille_variable= '"+famille+"' ");
        Variable par = null;
        try {

          //  System.out.println("select  * from APP.VARIABLES where APP.variables.nom_variable= '" + nom + "'  and APP.variables.famille_variable= '" + famille + "' ");
            outResult = s.executeQuery("select  * from APP.VARIABLES where APP.variables.nom_variable= '" + nom + "'  and APP.variables.famille_variable= '" + famille + "' ");
//System.out.println("select  * from APP.VARIABLES where APP.variables.nom_variable= '"+nom+"'  and APP.variables.famille_variable= '"+famille+"' ");

            while (outResult.next()) {
                //  System.out.println(outResult.getString(2));
                String num = outResult.getString(1);
                String value = outResult.getString(2);
                String fam = outResult.getString(3);
                String value2 = outResult.getString(4);
                par = (new Variable(num, value, fam, value2));
            }
        } catch (Throwable e) {
            insertError(e + "", "getparm autoout");
        }
        return par;
    }

    public LinkedList<Variable> getParam(String famille) {
        LinkedList<Variable> par = new LinkedList();
        try {
            outResult = s.executeQuery("select  * from APP.VARIABLES where APP.variables.famille_variable= '" + famille + "'");
            System.out.println("select  * from APP.VARIABLES where APP.variables.famille_variable= '" + famille + "'");

            while (outResult.next()) {
//    System.out.println(outResult.getString(2));
                String num = outResult.getString(1);
                String value = outResult.getString(2);
                String fam = outResult.getString(3);
                par.add(new Variable(num, value, fam));
        //System.out.println("num:"+num+" val:"+value+" fam"+fam);
            }
            outResult.close();

        } catch (Throwable e) {
            insertError(e + "", "getparm autoout");
        }
        return par;
    }

    public String getTxtFamille(String famille) {
        String resFamille = "";
        try {
            System.out.println("select  * from APP.VARIABLES where APP.variables.famille_variable= '" + famille + "'");
            outResult = s.executeQuery("select  * from APP.VARIABLES where APP.variables.famille_variable= '" + famille + "'");

            while (outResult.next()) {
//    System.out.println(outResult.getString(2));

                String num = outResult.getString(1);
                String value = outResult.getString(2);
                resFamille += num + ";" + value + ">>";
            }
            outResult.close();

        } catch (Throwable e) {
            insertError(e + "", "  getparm autoout");
        }

        System.out.println(" exit " + resFamille);
        return resFamille;
    }

    boolean sdcDataExist() {
        try {
            outResult = s.executeQuery("select * from APP.INVOICE WHERE EXTERNAL_DATA LIKE '%SDC%'");
            while (outResult.next()) {
                return true;
            }
        } catch (Throwable e) {

            insertError(e + "", "isaha");
        }
        return false;
    }

    Tarif[] getTarif() {

        Tarif[] tar = new Tarif[15];
        try {

            outResult = s.executeQuery("select COUNT(ID_TARIF) from APP.TARIF ");
            while (outResult.next()) {
                tar = new Tarif[outResult.getInt(1)];
            }
            outResult.close();

            outResult = s.executeQuery("select  * from APP.TARIF ");
            int i = 0;
            while (outResult.next()) {

                String ID_TARIF = outResult.getString("ID_TARIF");
                String DES_TARIF = outResult.getString("DES_TARIF");
                String FONT_TARIF = outResult.getString("FONT_TARIF");
                String VAR_TARIF = outResult.getString("VAR_TARIF");
                double COEF_TARIF = outResult.getDouble("COEF_TARIF");
                int b = outResult.getInt("B_TARIF");
                int g = outResult.getInt("G_TARIF");
                int r = outResult.getInt("R_TARIF");
                String observe = outResult.getString("OBSERVE");
                String smart = outResult.getString("SMART");

                tar[i] = new Tarif(ID_TARIF, VAR_TARIF, DES_TARIF, COEF_TARIF, FONT_TARIF, r, g, b, observe, smart);
                i++;
            }
        } catch (Throwable e) {

            if (("" + e).contains("OBSERVE")) {
                boolean exist = hasColumn(outResult, "OBSERVE");
                if (!exist) {
                    try {
                        s.execute(" ALTER TABLE TARIF ADD COLUMN OBSERVE VARCHAR(50) DEFAULT 'NO'");
                        s.execute(" ALTER TABLE TARIF ADD COLUMN SMART VARCHAR(50) DEFAULT 'NO'");

                        return getTarif();
                    } catch (SQLException ex) {
                        insertError(ex + "", "add column observe");
                    }
                }
            }
            insertError(e + "", "getparm autoout");
        }

        return tar;
    }

    LinkedList<Tarif> getAllTarif() {

        LinkedList<Tarif> tar = new LinkedList<Tarif>();
        try {
            outResult = s.executeQuery("select  * from APP.TARIF ");
            int i = 0;
            while (outResult.next()) {

                String ID_TARIF = outResult.getString("ID_TARIF");
                String DES_TARIF = outResult.getString("DES_TARIF");
                String FONT_TARIF = outResult.getString("FONT_TARIF");
                String VAR_TARIF = outResult.getString("VAR_TARIF");
                double COEF_TARIF = outResult.getDouble("COEF_TARIF");
                int b = outResult.getInt("B_TARIF");
                int g = outResult.getInt("G_TARIF");
                int r = outResult.getInt("R_TARIF");
                String observe = outResult.getString("OBSERVE");
                String smart = outResult.getString("SMART");

                tar.add(new Tarif(ID_TARIF, VAR_TARIF, DES_TARIF, COEF_TARIF, FONT_TARIF, r, g, b, observe, smart));
                i++;
            }
        } catch (Throwable e) {

            insertError(e + "", "gettar autoout");
        }

        return tar;
    }

    void updateParameter(LinkedList<Variable> par) {

        for (int i = 0; i < par.size(); i++) {

            Variable v = par.get(i);
            //System.out.println(" update APP.VARIABLES set value_variable ='" + v.value + "',value2_variable ='" + v.value2 + "' where nom_variable='" + v.nom + "' AND famille_variable='" + v.famille + "'");
            try {

                s.execute(" update APP.VARIABLES set value_variable ='" + v.value + "'  where nom_variable='" + v.nom + v.value2 + "' AND famille_variable='" + v.famille + "'");
            } catch (SQLException ex) {
                insertError(ex + "", "updateparam" + v.nom);
            }
        }
    }

    public void getLot(String s1) {
        try {

            lot = new LinkedList();
            System.out.println("select app.lot.id_product,name_product,id_LotS,date_exp,qty_start,"
                    + "qty_live,lot.bon_livraison,bon_livraison.NUM_CLIENT,id_lot,"
                    + "lot.revient from APP.LOT ,APP.PRODUCT,bon_livraison where app.lot.id_product = app.product.id_product and bon_livraison.bon_livraison=lot.bon_livraison and app.lot.code_lot<>'-1'" + s1);

            outResult = s.executeQuery("select app.lot.id_product,name_product,id_LotS,date_exp,qty_start,"
                    + "qty_live,lot.bon_livraison,bon_livraison.NUM_CLIENT,id_lot,"
                    + "lot.revient from APP.LOT ,APP.PRODUCT,bon_livraison where app.lot.id_product = app.product.id_product and bon_livraison.bon_livraison=lot.bon_livraison and app.lot.code_lot<>'-1'" + s1);

            while (outResult.next()) {
                int productCode = outResult.getInt(1);
                String name = outResult.getString(2);
                String id_LotS = outResult.getString(3);
                String dateExp = outResult.getString(4);
                int qtyStart = outResult.getInt(5);
                int qtyLive = outResult.getInt(6);
                String bon_livraison = outResult.getString(7);
                String code = outResult.getString(8);
                int dateDoc = outResult.getInt(9);
                double pr = outResult.getDouble(10);
                double prb = 0;//outResult.getDouble(11);
                lot.add(new Lot("" + productCode, "" + name, "" + code, "" + id_LotS, "" + dateExp, "" + dateDoc, "" + qtyStart, "" + qtyLive, "" + bon_livraison, "" + pr, "" + prb));
            }

            //  Close the resultSet 
            outResult.close();

            outResult = s.executeQuery("select app.lot.id_product,name_product,id_LotS,date_exp,qty_start,"
                    + "qty_live,lot.bon_livraison,bon_mvt_in.NUM_CLIENT,id_lot,"
                    + "LOT.revient,code from APP.LOT ,APP.PRODUCT,bon_mvt_in where app.lot.id_product = app.product.id_product and bon_mvt_in.bon_mvt_in=lot.bon_livraison and app.lot.code_lot<>'-1'" + s1);

            while (outResult.next()) {
                int productCode = outResult.getInt(1);
                String name = outResult.getString(2);
                String id_LotS = outResult.getString(3);
                String dateExp = outResult.getString(4);
                int qtyStart = outResult.getInt(5);
                int qtyLive = outResult.getInt(6);
                String bon_livraison = outResult.getString(7);
                String code = outResult.getString(8);
                int dateDoc = outResult.getInt(9);
                double pr = outResult.getDouble(10);
                double prb = 0;//outResult.getDouble(11);
//            String frss = outResult.getString(11);
                Lot l = (new Lot("" + productCode, "" + name, "" + code, "" + id_LotS, "" + dateExp, "" + dateDoc, "" + qtyStart, "" + qtyLive, "" + bon_livraison, "" + pr, "" + prb));
                //l.price=pr;
//            l.frss = frss;
//                lotL.add(l); 
                lot.add(l);
            }
        } catch (Throwable e) {

            /*       Catch all exceptions and pass them to 
             **       the exception reporting method             */
            System.out.println(" . .lot . exception thrown:" + e);
            insertError(e + "", " V lot");
        }
    }
boolean insertLang(String var) {
        try { 
            
       PreparedStatement     psInsert = conn.prepareStatement("insert into APP.LANGUE"
                    + "(VARIABLE,KIN,FRA,ANG) "
                    + " values (?,?,?,?)");

            psInsert.setString(1, var);
            psInsert.setString(2, var);
            psInsert.setString(3, var);
            psInsert.setString(4, var); 

            psInsert.executeUpdate();
            psInsert.close();
            return true;
        } catch (SQLException ex) {
            insertError( ex + "", "insertVariable");
            return false;
        }
    }   
 
    public String l(String lang, String var) {
        String value = null;
        try {
            // System.out.println(" select  * from APP.LANGUE where variable= '"+var+"'");
            outResult = s.executeQuery(" select  * from APP.LANGUE where variable= '" + var + "'");
            //System.out.println("  select  * from APP.LANGUE where variable= '"+var+"'");
            while (outResult.next()) {
                value = outResult.getString(lang);
                // System.out.println(value);
            }
             if (value == null || value.equals("")) { 
           JOptionPane.showMessageDialog(null, 
           "LANG MISSING  " + var, "SYSTEM ERROR ", JOptionPane.WARNING_MESSAGE);
           System.err.println(var); 
            insertLang(var); 
        }
            
            
            
            
        } catch (Throwable e) {
            insertError(e + "", "langue");
        }

        return value;
    }

    public SortedSet<Lot> getLotSortedSet(int id) {
        SortedSet<Lot> sc = new TreeSet();
        try {
            outResult = s.executeQuery("select LOT.ID_PRODUCT,LOT.ID_LOT,LOT.ID_LOTS,LOT.DATE_EXP,LOT.QTY_START,LOT.QTY_LIVE,LOT.BON_LIVRAISON,LOT.REVIENT,LOT.CODE_LOT from APP.LOT  where  APP.LOT.ID_PRODUCT=" + id + " and qty_live>0 order by id_lot ");

            while (outResult.next()) {
                Lot l = new Lot(id, "", "", outResult.getString("ID_LOTS"), outResult.getString("DATE_EXP"), outResult.getInt("ID_LOT"), outResult.getInt("QTY_START"), outResult.getInt("QTY_LIVE"),
                        outResult.getString("BON_LIVRAISON"));
                l.revient = outResult.getDouble("REVIENT");
                sc.add(l);
            }
        } catch (Throwable e) {
            insertError(e + "", " getLotSortedSet");
        }
        return sc;
    }

    public SortedSet<Lot> getLotSortedSetV4(int id, String depot) {
        SortedSet<Lot> sc = new TreeSet();
        LinkedList<Lot> st = new LinkedList<Lot>();
        try {

            String sql = "select LOT.ID_PRODUCT,LOT.ID_LOT,LOT.ID_LOTS,LOT.DATE_EXP,LOT.QTY_START,LOT.QTY_LIVE,"
                    + "LOT.BON_LIVRAISON,LOT.REVIENT,LOT.CODE_LOT from APP.LOT  where  APP.LOT.ID_PRODUCT=" + id + ""
                    + "  and qty_live>0 " + depot + "   order by id_lot ";

            System.out.println(sql);
            outResult = s.executeQuery(sql);
            while (outResult.next()) {
                Lot l = new Lot(id, "", "", outResult.getString("ID_LOTS"), outResult.getString("DATE_EXP"), outResult.getInt("ID_LOT"), outResult.getInt("QTY_START"), outResult.getInt("QTY_LIVE"),
                        outResult.getString("BON_LIVRAISON"));
                l.revient = outResult.getDouble("REVIENT");
                st.add(l);
            }
            outResult.close();

            for (int i = 0; i < st.size(); i++) {
                int max = 0;
                Lot stock = st.get(i);
                outResult = s.executeQuery("select SUM(QUANTITE) from APP.SDC_URUTONDE  where APP.SDC_URUTONDE.LIST_ID_PRODUCT=" + id + " AND CODE_UNI='" + stock.code + "' AND NUM_LOT='" + stock.id_LotS + "' AND BON_LIVRAISON='" + stock.bon_livraison + "' ");
                System.out.println("select SUM(QUANTITE) from APP.SDC_URUTONDE  where APP.SDC_URUTONDE.LIST_ID_PRODUCT=" + id + " AND CODE_UNI='" + stock.code + "' AND NUM_LOT='" + stock.id_LotS + "' AND BON_LIVRAISON='" + stock.bon_livraison + "' ");

                while (outResult.next()) {
                    max = outResult.getInt(1);
                }
                if (max > 0) {
                    st.get(i).qtyLive = stock.qtyLive - max;
                    sc.add(st.get(i));
                } else {
                    sc.add(st.get(i));
                }
            }
        } catch (Throwable e) {
            insertError(e + "", " getLotSortedSet");
        }

        return sc;
    }

    public SortedSet<Lot> getLotSortedSetV6(int id) {
        SortedSet<Lot> sc = new TreeSet();
        LinkedList<Lot> st = new LinkedList<Lot>();
        try {
            outResult = s.executeQuery("select LOT.ID_PRODUCT,LOT.ID_LOT,LOT.ID_LOTS,LOT.DATE_EXP,"
                    + "LOT.QTY_START,LOT.QTY_LIVE,LOT.BON_LIVRAISON,LOT.REVIENT,LOT.CODE_LOT "
                    + "from APP.LOT  where  APP.LOT.ID_PRODUCT=" + id + " and qty_live>0 order by id_lot ");
            System.out.println("select LOT.ID_PRODUCT,LOT.ID_LOT,LOT.ID_LOTS,LOT.DATE_EXP,LOT.QTY_START,LOT.QTY_LIVE,LOT.BON_LIVRAISON,LOT.REVIENT,LOT.CODE_LOT from APP.LOT  where  APP.LOT.ID_PRODUCT=" + id + " and qty_live>0 order by id_lot ");

//        System.out.println("select LOT.ID_PRODUCT,LOT.ID_LOT,LOT.ID_LOTS,LOT.DATE_EXP,LOT.QTY_START,(LOT.QTY_LIVE-SUM(QUANTITE)),LOT.BON_LIVRAISON,LOT.REVIENT,LOT.CODE_LOT from APP.LOT,SDC_URUTONDE  where APP.LOT.ID_PRODUCT="+id+" AND CODE_UNI=CODE_LOT AND ID_LOTS=NUM_LOT AND LOT.BON_LIVRAISON=SDC_URUTONDE.BON_LIVRAISON AND qty_live>0 GROUP BY LOT.ID_PRODUCT,LOT.ID_LOT,LOT.ID_LOTS,LOT.DATE_EXP,LOT.QTY_START,LOT.QTY_LIVE,LOT.BON_LIVRAISON,LOT.REVIENT,LOT.CODE_LOT order by id_lot ");
            while (outResult.next()) {
                Lot l = new Lot(id, "", "", outResult.getString("ID_LOTS"), outResult.getString("DATE_EXP"), outResult.getInt("ID_LOT"), outResult.getDouble("QTY_START"), outResult.getDouble("QTY_LIVE"),
                        outResult.getString("BON_LIVRAISON"));
                l.revient = outResult.getDouble("REVIENT");
                st.add(l);
            }
            outResult.close();

            for (int i = 0; i < st.size(); i++) {
                int max = 0;
                Lot stock = st.get(i);
                outResult = s.executeQuery("select SUM(QUANTITE) from APP.SDC_URUTONDE  where APP.SDC_URUTONDE.LIST_ID_PRODUCT=" + id + " AND CODE_UNI='" + stock.code + "' AND NUM_LOT='" + stock.id_LotS + "' AND BON_LIVRAISON='" + stock.bon_livraison + "' ");
                System.out.println("select SUM(QUANTITE) from APP.SDC_URUTONDE  where APP.SDC_URUTONDE.LIST_ID_PRODUCT=" + id + " AND CODE_UNI='" + stock.code + "' AND NUM_LOT='" + stock.id_LotS + "' AND BON_LIVRAISON='" + stock.bon_livraison + "' ");

                while (outResult.next()) {
                    max = outResult.getInt(1);
                }
                if (max > 0) {
                    st.get(i).qtyLive = stock.qtyLive - max;
                    sc.add(st.get(i));
                } else {
                    sc.add(st.get(i));
                }
            }
        } catch (Throwable e) {
            insertError(e + "", " getLotSortedSet");
        }

        return sc;
    }

    public SortedSet<List> getListSortedSet(String codeu) {
        SortedSet<List> sc = new TreeSet();
        try {
            outResult = s.executeQuery(" select id_invoice,code_uni,num_lot,quantite,price,list.prix_revient,key_invoice,id_product,name_product from APP.list,app.product where code_uni='" + codeu + "' and list.code_uni=product.code ");

            while (outResult.next()) {
                int id_inv = outResult.getInt(1);
                double pr = outResult.getDouble(5);
                int qty = outResult.getInt(4);
                String lott = outResult.getString(3);
                String code = outResult.getString(2);
                String name = outResult.getString(8);
                double prev = outResult.getDouble(6);
                String key_inv = outResult.getString(7);
                List l = new List(id_inv, pr, qty, lott, code, name, prev);
                sc.add(l);
            }
        } catch (Throwable e) {
            insertError(e + "", " getsorted");
        }
        return sc;
    }

    public LinkedList getProductList(int id_invoice) {
        LinkedList<String> prod = new LinkedList();
        try {
            // System.out.println(" select  * from APP.LANGUE where variable= '"+var+"'");
            outResult = s.executeQuery(" select code_uni from APP.list where id_invoice= " + id_invoice + " ");
            //System.out.println("  select  * from APP.LANGUE where variable= '"+var+"'");
            while (outResult.next()) {
                String code = outResult.getString(1);
                prod.add(code);
            }
        } catch (Throwable e) {
            insertError(e + "", "invoiceList");
        }
        return prod;
    }

    public LinkedList getInvoiceList(int id_invoice) {
        LinkedList<List> invoiceList = new LinkedList<List>();

        try {
            // System.out.println(" select  * from APP.LANGUE where variable= '"+var+"'");
            outResult = s.executeQuery(" select id_invoice,code_uni,num_lot,quantite,price,list.prix_revient,key_invoice,id_product,name_product,list.tva"
                    + ",original_price,list.date_exp,list.bon_livraison from APP.list,app.product "
                    + " where id_invoice= " + id_invoice + " and list.code_uni=product.code and bon_livraison!='ANNULE'");

            while (outResult.next()) {
                int id_inv = outResult.getInt(1);
                double pr = outResult.getDouble(5);
                int qty = outResult.getInt(4);
                String lott = outResult.getString(3);
                String code = outResult.getString(2);
                String name = outResult.getString(9);
                double prev = outResult.getDouble(6);
                String key_inv = outResult.getString(7);
                int idpro = outResult.getInt(8);
                double tva = outResult.getDouble(10);
                List ll = new List(id_invoice, pr, qty, lott, code, name, prev);
                ll.id_pro = idpro;
                ll.tva = tva;
                ll.DATE_EXP=outResult.getString("DATE_EXP");
                ll.BON_LIVRAISON=outResult.getString("BON_LIVRAISON");
                ll.product = new Product(idpro, name, code, qty, pr, tva, code, "", "", this);
                ll.product.originalPrice = outResult.getDouble(11);
                if (ll.product.originalPrice != ll.product.currentPrice) {
                    ll.product.productName += "# Discount -" + (int) (100 - Math.abs((ll.product.currentPrice * 100) / ll.product.originalPrice)) + "%";
                }
                System.out.println(ll.product.currentPrice + " ------------------------------------------------- " + ll.product.originalPrice);
                invoiceList.add(ll);
            }
        } catch (Throwable e) {
            insertError(e + "", "invoiceList");
        }
        return invoiceList;
    }

    public int retour(int id_invoice, String nom, RRA_CLIENT rc, String date1, String client, double cash, double credit) {
        String keyIdInvoice = "" + new Date() + "" + nom + ip;
        LinkedList<List> invoiceList = getInvoiceList(id_invoice);
        LinkedList<List> retourListSelected = new LinkedList();
        LinkedList<Product> prod2 = new LinkedList<Product>();
        LinkedList<Lot> toAnnule = new LinkedList();

        int totale = 0;
        String printStringE = "";
        String printStringW = "";

        LinkedList< JCheckBox> jcheck = new LinkedList();
        JPanel pane = new JPanel();
        int div = invoiceList.size() / 5;
        pane.setLayout(new GridLayout(5, div));
        for (int i = 0; i < invoiceList.size(); i++) {
            List lis = invoiceList.get(i);
            JCheckBox now = new JCheckBox(lis.code + "*" + lis.name + "*" + lis.qty);
            jcheck.add(now);
            pane.add(now);
        }
        JOptionPane.showMessageDialog(null, pane, l(lang,"V_HITAMO"), JOptionPane.INFORMATION_MESSAGE);

        for (int i = 0; i < invoiceList.size(); i++) {
            if (jcheck.get(i).isSelected()) {
                List res = invoiceList.get(i);

            //int avant=res.qty;
            //res.qty=(int)getIn(JOptionPane.showInputDialog(" Confirme la Quantité ", "" +res.qty));
//                    getIn( " Confirme la Quantité "+res.code, "" +res.qty); 
                if (res.qty > 0) {
                    res.choix = "NOT ANNULE";
                }

                printStringE = printStringE + "\n  " + res.qty + "  " + res.code + " " + res.name + "**" + (int) (res.qty * res.price);
                printStringW = printStringW + "(" + res.code + "-" + res.qty + "-" + (int) (res.qty * res.price) + ")";
//            res.tva=tvaProd;
                // JOptionPane.showMessageDialog(null,"FACTURE N° "+n+" ANNULEE","",JOptionPane.PLAIN_MESSAGE);
                retourListSelected.add(res);

//            totale=totale+(int)(res.qty*res.price);
//            tva=tva+(int)(res.qty*res.tva);  
                res.product.currentPrice = (res.price);
                prod2.add(res.product);
                Lot sort = new Lot(res.product.productCode, res.product.productName, res.product.code, res.lot, "", 0, res.qty, res.qty, "");
                sort.prodLot = new Product(res.product.productCode, res.product.productName, res.product.code, res.qty, res.price,
                        res.tva, res.product.code, "", "", this);
                sort.prodLot.originalPrice = res.product.originalPrice;
//System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++ "+sort.prodLot.originalPrice);
                toAnnule.add(sort);

            }//     System.out.println(retourList.get(i));
        }

        String printStringUp = "    REBA NEZA KO IYI FACTURE ARIYO YA RETOUR  ";
        boolean done = true;
        boolean done1 = false;
        Client infoClient = null;
        String num_affiliation = "";
        String nom_client = "";
        String numero_quitance = "";
        int percentage = 0;
        String prenom_client = "";
        String mrc = "";
        String tin2 = "";

        try {
            // Clients Societe
            outResult = s.executeQuery(""
                    + "select num_client,numero_affilie,numero_quitance,date,client.percentage,nom_client,prenom_client"
                    + ",invoice.key_invoice,invoice.tin,invoice.mrc from  APP.INVOICE,app.credit,client "
                    + "where invoice.id_invoice=credit.id_invoice  "
                    + " and client.num_affiliation=credit.numero_affilie and  APP.INVOICE.id_invoice = " + id_invoice);

            while (outResult.next()) {
                client = outResult.getString("num_client");
                num_affiliation = outResult.getString("numero_affilie");
                nom_client = outResult.getString("nom_client");
                numero_quitance = outResult.getString("numero_quitance");
                date1 = outResult.getString("DATE");
                percentage = outResult.getInt("percentage");
                prenom_client = outResult.getString("prenom_client");
                tin2 = outResult.getString("tin");
                mrc = outResult.getString("mrc");
                done1 = true;
                done = false;
                infoClient = new Client(num_affiliation, nom_client, prenom_client, percentage, 0, "" + client, "", "", "", "" + num_affiliation);

            }
            outResult.close();
            if (infoClient == null) {

                outResult = s.executeQuery("select num_client,numero_affilie,nom_client,prenom_client,"
                        + "numero_quitance,client_rama.percentage,employeur,invoice.key_invoice,invoice.tin,invoice.mrc"
                        + " from APP.INVOICE,APP.CREDIT,APP.CLIENT_RAMA "
                        + "where APP.INVOICE.id_invoice =  " + id_invoice + "   "
                        + "and APP.INVOICE.id_invoice = APP.credit.id_invoice and APP.client_RAMA.num_affiliation = APP.credit.numero_affilie  order by app.invoice.id_invoice");

                System.out.println("select num_client,numero_affilie,nom_client,prenom_client,numero_quitance,client_rama.percentage,employeur,invoice.key_invoice,invoice.tin"
                        + " from APP.INVOICE,APP.CREDIT,APP.CLIENT_RAMA "
                        + "where APP.INVOICE.id_invoice =  " + id_invoice + "   "
                        + "and APP.INVOICE.id_invoice = APP.credit.id_invoice and APP.client_RAMA.num_affiliation = APP.credit.numero_affilie  order by app.invoice.id_invoice");

                while (outResult.next()) {
                    client = outResult.getString(1);
                    num_affiliation = outResult.getString(2);
                    nom_client = outResult.getString(3);
                    prenom_client = outResult.getString(4);
                    numero_quitance = outResult.getString(5);
                    percentage = outResult.getInt(6);
                    keyIdInvoice = outResult.getString(8);
                    tin2 = outResult.getString("tin");
                    mrc = outResult.getString("mrc");
                    infoClient = new Client(num_affiliation, nom_client, prenom_client, percentage, 0, "" + client, "", "", "", "" + num_affiliation);

                }
                outResult.close();

            }
            if (done) {
                printStringUp = printStringUp + "\n    FACTURE N°    : " + id_invoice;
                printStringE = printStringE + "\n  ";
                printStringE = printStringE + "\n  TOTAL : " + (int) totale;
            } else {
                printStringUp = printStringUp + "\n    FACTURE N°    : " + id_invoice;
                printStringUp = printStringUp + "\n    " + date1;
                printStringUp = printStringUp + "\n    -------------------------------------------   ";
                printStringUp = printStringUp + "\n    Assureur            : " + client;
                printStringUp = printStringUp + "\n    N° Affiliation      : " + num_affiliation;
                printStringUp = printStringUp + "\n    Nom et Prenom : " + nom_client + " " + prenom_client;
                //   printStringUp=printStringUp+ "\n    Departement     : "+c.EMPLOYEUR;
                printStringUp = printStringUp + "\n    N° Quittance     : " + numero_quitance;
                printStringUp = printStringUp + "\n    -------------------------------------------   ";
                printStringE = printStringE + "\n  ";

                printStringE = printStringE + "\n  TOTAL en RWF: " + totale;

                printStringE = printStringE + "\n  " + client + " " + (100 - percentage) + " % : " + (totale - (int) (totale * percentage * 0.01));

                printStringE = printStringE + "\n  ADHERENT  " + percentage + " % : " + (int) (totale * percentage * 0.01);
//totale=(totale);

            }
            //  Close the resultSet  and date ='"+d+"'
            outResult.close();

            if (done || done1) {
//        if((total>totale) && (totale>0))
//        {
                int n = JOptionPane.showConfirmDialog(null,
                        printStringUp + "\n " + printStringE
                        + "\n MONTANT RETOUR " + totale,
                        "RETOUR ????? ", JOptionPane.YES_NO_OPTION);

                if (n == 0) {
                    insertAnnuleListV4(toAnnule, id_invoice, keyIdInvoice);
                    insertRefundListV4(toAnnule, id_invoice, keyIdInvoice);

                    RRA_PACK rp = Main.tax.getRpack4(0, prod2,
                            getRRA("rtype"), "R", "", mrc);
                    if (rp != null) {
                        int id_refund = -1;
                        rp.ClientTin = tin2;
                        String toSend = rc.getSaleString2(rp);
                        String data = rc.sendRequest("SEND RECEIPT", toSend, "" + keyIdInvoice, "TIME", " ", getRRA("rtype"), "R", id_invoice);
                        System.err.println("data:*******" + data);

                        if (data != null && !data.contains("ERROR")) {
                            String spp[] = data.split("#");
                            try {
                                id_refund = Integer.parseInt(spp[0]);
                                //id_refund=getMaxValue("MAX(ID_REFUND)","'"+keyIdInvoice+"'", "REFUND", "KEY_INVOICE");
                            } catch (NumberFormatException e) {
                                insertError("" + e, "refund list not");
                                deleteRefundList(keyIdInvoice);
                                id_refund = -1;
                            }
                            if (id_refund != -1) {
                                Annule deleted = new Annule(id_invoice, num_affiliation, nom + " -R", client, printStringW, date1, "RETOUR");

                                if (retourMse(retourListSelected, true)) {
                                    String rais = (JOptionPane.showInputDialog(null, " REASON"));
                                    double totalOr = cash + credit;
                                    int pourceCASH = (int) (cash * 100 / totalOr);
                                    int pourceCredit = 100 - pourceCASH;

                                    double CASH = rp.getTotal() * pourceCASH / 100.0;
                                    double CREDIT = rp.getTotal() * pourceCASH / 100.0;

                                    String time_rec_mrc = insertRefund(deleted, rais, rp, keyIdInvoice, id_refund, id_invoice, CASH, CREDIT);
                                    insertDeleted(deleted, rais);

                                    String resp = getExternalDataV4("EXTERNAL_DATA", id_refund, "REFUND", "ID_REFUND");
                                    if (resp != null) {
                                        resp = resp.replaceAll(",,", ",#,#");
                                        String[] sp = resp.split(",");
                                        LinkedList<String> Clientinfo = Leclient(infoClient);
                                        int margin = 5;
                                        String marginVar = getRRA("margin");
                                        try {
                                            margin = Integer.parseInt(marginVar);
                                        } catch (NumberFormatException ex) {
                                            System.err.println(marginVar + "   marginVar   " + ex);
                                        }
                                        new RRA_PRINT2(getRRA("company"), getString("bp"), getRRA("City"), getRRA("tin"), "" + rp.ClientTin,
                                                (rp.getTotal()), (rp.getTotalA()), (rp.getTotalB()), (rp.getTotalTaxes()),
                                                (rp.getTotalTaxes()), (rp.getTotal()), sp[0], sp[1] + "/" + sp[2] + "  " + sp[3],
                                                sp[6], sp[5], id_refund, rp.MRC, rp.getProductList, getRRA("rtype"), "R",
                                                getRRA("version"), -1, id_invoice, Clientinfo, time_rec_mrc, sp[4], nom, numero_quitance, s, "", margin, 0, 0);

                                        JOptionPane.showMessageDialog(null, l(lang,"V_PRODUITSUR") + id_invoice + l(lang,"V_RETOURNER"), "", JOptionPane.INFORMATION_MESSAGE);
                                    } else {
                                        JOptionPane.showMessageDialog(null, l(lang,"V_UPDATE_FAIL") + id_invoice + " ", l(lang,"V_IKIBAZO"), JOptionPane.PLAIN_MESSAGE);
                                    }

                                } else {
                                    JOptionPane.showMessageDialog(null, l(lang,"V_ANNULER_FACTURE") + id_invoice + l(lang,"V_BIRANZE"), l(lang,"V_IKIBAZO"), JOptionPane.ERROR_MESSAGE);
                                }

                            } else {
                                updateListAnnule(id_invoice);
                            }
                        }
                    }
                } else {
                    totale = 0;
                }
//        }
//        else
//        {
//            JOptionPane.showMessageDialog(null,"FACTURE N° "+id_invoice+" TOTAL NTIHAGIJE "+total+ " VS RETOUR "+totale,"",JOptionPane.WARNING_MESSAGE);
//        }
            } else {
                JOptionPane.showMessageDialog(null, l(lang,"V_FACTURE_NUMERO") + id_invoice + l(lang,"V_NTIBAHO"), "", JOptionPane.PLAIN_MESSAGE);
            }
        } catch (Throwable e) {
            insertError(e + "", "retour ");
        }
        return totale;
    }

    boolean JCheckBoxZirasa(LinkedList< JCheckBox> jcheck1, LinkedList< Lot> lotZose) {
        for (int i = 0; i < jcheck1.size(); i++) {
            if (jcheck1.get(i).isSelected() == true && lotZose.get(i).aSortir == 0) {
                return false;
            }
            if (!jcheck1.get(i).isSelected() == true && lotZose.get(i).aSortir != 0) {
                return false;
            }
        }
        return true;
    }

    public boolean sortStock(LinkedList<Lot> list) {
        String createString1 = "";
        try {
            for (int i = 0; i < list.size(); i++) {
                Lot l = list.get(i);
                createString1 = " update APP.LOT set QTY_LIVE=QTY_LIVE-" + l.aSortir + " "
                        + "where BON_LIVRAISON='" + l.bon_livraison + "' AND ID_LOTS='" + l.id_LotS
                        + "'  and date_exp= '" + l.dateExp + "'  and ID_PRODUCT=" + l.prodLot.productCode + "";
                System.out.println(createString1);
                s.execute(createString1);
                if (l.aSortir == l.qtyLive) {
                    PreparedStatement psInsert2 = conn.prepareStatement("insert into APP.USED_LOT(ID_PRODUCT,ID_LOT,ID_LOTS,DATE_EXP,QTY_START,QTY_LIVE,BON_LIVRAISON,REVIENT,CODE_LOT)  values (?,?,?,?,?,?,?,?,?)");
                    psInsert2.setInt(1, l.prodLot.productCode);
                    psInsert2.setInt(2, l.id_lot);
                    psInsert2.setString(3, l.id_LotS);
                    psInsert2.setString(4, l.dateExp);
                    psInsert2.setInt(5, l.qtyStart);
                    psInsert2.setInt(6, l.qtyLive);
                    psInsert2.setString(7, l.bon_livraison);
                    psInsert2.setDouble(8, l.revient);
                    psInsert2.setString(9, l.prodLot.code);
                    psInsert2.executeUpdate();
                    psInsert2.close();

                    String sqlToDelete = " delete from lot "
                            + "where BON_LIVRAISON='" + l.bon_livraison + "' AND ID_LOTS='" + l.id_LotS
                            + "'  and date_exp= '" + l.dateExp + "'  and ID_PRODUCT=" + l.prodLot.productCode + "";
                    System.out.println(sqlToDelete);
                    s.execute(sqlToDelete);
                }

            }
        } catch (Throwable e) {
            insertError(createString1, " sortStock");
        }
        return true;
    }

    public boolean sortStock2(LinkedList<Lot> list) {
        String createString1 = "";
        try {
            for (int i = 0; i < list.size(); i++) {
                Lot l = list.get(i);
                createString1 = " update APP.LOT set QTY_LIVE=QTY_LIVE-" + l.aSortir2 + " "
                        + "where BON_LIVRAISON='" + l.bon_livraison + "' AND ID_LOTS='" + l.id_LotS
                        + "'  and date_exp= '" + l.dateExp + "'  and ID_PRODUCT=" + l.prodLot.productCode + "";
                System.out.println(createString1);
                s.execute(createString1);

                if (l.aSortir2 == l.qtyLive) {
                    PreparedStatement psInsert2 = conn.prepareStatement("insert into APP.USED_LOT(ID_PRODUCT,ID_LOT,ID_LOTS,DATE_EXP,QTY_START,QTY_LIVE,BON_LIVRAISON,REVIENT,CODE_LOT)  values (?,?,?,?,?,?,?,?,?)");
                    psInsert2.setInt(1, l.prodLot.productCode);
                    psInsert2.setInt(2, l.id_lot);
                    psInsert2.setString(3, l.id_LotS);
                    psInsert2.setString(4, l.dateExp);
                    psInsert2.setDouble(5, l.qtyStart2);
                    psInsert2.setDouble(6, l.qtyLive2);
                    psInsert2.setString(7, l.bon_livraison);
                    psInsert2.setDouble(8, l.revient);
                    psInsert2.setString(9, l.prodLot.code);
                    psInsert2.executeUpdate();
                    psInsert2.close();

                    String sqlToDelete = " delete from lot "
                            + "where BON_LIVRAISON='" + l.bon_livraison + "' AND ID_LOTS='" + l.id_LotS
                            + "'  and date_exp= '" + l.dateExp + "'  and ID_PRODUCT=" + l.prodLot.productCode + "";
                    System.out.println(sqlToDelete);
                    s.execute(sqlToDelete);
                }

            }
        } catch (Throwable e) {
            insertError(createString1, " sortStock");
        }
        return true;
    }

    int insertInvoiceList(LinkedList<Lot> LotSortir, Invoice invo, Credit c) {

        String keyIdInvoice = "" + new Date() + "" + invo.id_employe + ip;
        try {
            if (LotSortir.size() > 0) {

                psInsert = conn.prepareStatement("insert into APP.INVOICE (NUM_CLIENT,DATE ,TOTAL,EMPLOYE,TVA,REDUCTION,KEY_INVOICE)  values (?,?,?,?,?,?,?)");

                psInsert.setString(1, invo.id_client);
                psInsert.setString(2, invo.date);
                psInsert.setDouble(3, invo.tot);
                psInsert.setString(4, invo.id_employe);
                psInsert.setDouble(5, invo.tva);
                psInsert.setDouble(6, invo.REDUCTION);
                psInsert.setString(7, keyIdInvoice);
                psInsert.executeUpdate();
                psInsert.close();

                psInsert = conn.prepareStatement("insert into APP.LIST (CODE_UNI,NUM_LOT,QUANTITE,PRICE,prix_revient,KEY_INVOICE,ID_INVOICE,LIST_ID_PRODUCT)  values (?,?,?,?,?,?,?,?)");

                for (int j = 0; j < LotSortir.size(); j++) // ecrire les produits
                {
                    Lot l = LotSortir.get(j);
                    psInsert.setString(1, l.prodLot.code);
                    psInsert.setString(2, l.id_LotS);
                    psInsert.setInt(3, l.aSortir);
                    psInsert.setDouble(4, l.prodLot.currentPrice);
                    psInsert.setDouble(5, l.revient);
                    psInsert.setString(6, keyIdInvoice);
                    psInsert.setInt(7, -1);
                    psInsert.setInt(8, l.prodLot.productCode);
                    psInsert.executeUpdate();
                }
                psInsert.close();

                if (!invo.id_client.equals("CASHNORM") || (invo.id_client.equals("CASHNORM") && c.NUMERO_AFFILIE.equals("DIVERS"))) {
                    psInsert = conn.prepareStatement("insert into APP.CREDIT (NUMERO_AFFILIE ,NUMERO_QUITANCE,SOLDE,PAYED,CAUSE,KEY_INVOICE,ID_INVOICE)  values (?,?,?,?,?,?,?)");

                    psInsert.setString(1, c.NUMERO_AFFILIE);
                    psInsert.setString(2, c.NUMERO_QUITANCE);
                    psInsert.setDouble(3, c.SOLDE);
                    psInsert.setDouble(4, c.PAYED);
                    psInsert.setString(5, c.RAISON);
                    psInsert.setString(6, keyIdInvoice);
                    psInsert.setInt(7, -1);
                    psInsert.executeUpdate();
                    psInsert.close();
                }
                // Release the resources (clean up )
                int fact = getIdFacture(keyIdInvoice);
                System.out.println(fact + "key invoice:" + keyIdInvoice);
                //  update list, credit
                if (fact != -1) {
                    String listUpdateSql = " update APP.LIST  SET ID_INVOICE=" + fact + " where  key_invoice= '" + keyIdInvoice + "' and id_invoice=" + -1 + "";
                    s.execute(listUpdateSql);
                    String creditUpdateSql = " update APP.credit  SET ID_INVOICE=" + fact + " where  key_invoice= '" + keyIdInvoice + "' and id_invoice=" + -1 + "";
                    s.execute(creditUpdateSql);
                    return fact;
                }
            }
        } catch (Throwable e) {
            writeError(e + " KEY " + keyIdInvoice, "doFinishVente FAIL");
            insertError(e + "", "doFinishVente");
        }
        return -1;
    }

    public boolean checkStock(LinkedList<Lot> list) {
        try {

            for (int i = 0; i < list.size(); i++) {
                Lot l = list.get(i);
                String select = "select qty_live from APP.LOT  where  "
                        + "APP.LOT.ID_PRODUCT=" + l.productCode + " and  ID_LOTS ='" + l.id_LotS + "' "
                        + "and DATE_EXP='" + l.dateExp + "' and BON_LIVRAISON='" + l.bon_livraison + "' ";
                System.out.println(select);
                outResult = s.executeQuery(select);

                int reba = 0;
                while (outResult.next()) {
                    System.out.println(l.aSortir + "  VS " + outResult.getInt(1));
                    if (outResult.getInt(1) < l.aSortir) {
                        return false;
                    }

                    reba++;
                }
                System.out.println(l.aSortir + "  VS " + reba);
                if (reba == 0) {
                    return false;
                }
            }
        } catch (Throwable e) {
            insertError(e + "", " checkStock");
        }

        return true;
    }

    public boolean checkStock2(LinkedList<Lot> list) {
        try {
            for (int i = 0; i < list.size(); i++) {
                Lot l = list.get(i);
                String select = "select qty_live from APP.LOT  where  "
                        + "APP.LOT.ID_PRODUCT=" + l.productCode + " and  ID_LOTS ='" + l.id_LotS + "' "
                        + "and DATE_EXP='" + l.dateExp + "' and BON_LIVRAISON='" + l.bon_livraison + "' ";
                System.out.println(select);
                outResult = s.executeQuery(select);
                int reba = 0;
                while (outResult.next()) {
                    System.out.println(l.aSortir2 + "  VS " + outResult.getInt(1));
                    if (outResult.getDouble(1) < l.aSortir2) {
                        return false;
                    }
                    reba++;
                }
                System.out.println(l.aSortir2 + "  VS " + reba);
                if (reba == 0) {
                    return false;
                }
            }
        } catch (Throwable e) {
            insertError(e + "", " checkStock");
        }

        return true;
    }

    public static int curikaDate(String dat) {
        if (dat != null) {
            if (dat.length() == 5) {
                dat = "0" + dat;
            }
            if (dat.length() == 6) {
                try {
                    return Integer.parseInt("" + dat.substring(4) + "" + dat.substring(2, 4) + "" + dat.substring(0, 2));
                } catch (NumberFormatException e) {
                    System.err.println(dat + " not a number Date " + e);
                }
            } else {
                System.err.println(dat + " size Date");
            }
        } else {
            System.err.println(dat + " null Date");
        }
        return -1;
    }

    void writeError(String desi, String ERR) {
        try {
            String file = "ERROR/" + datez + ".txt";
            File f = new File(file);
            PrintWriter sorti = new PrintWriter(new FileWriter(f));
            sorti.println(desi + ERR);
            sorti.close();
            JOptionPane.showMessageDialog(null, "  " + desi, l(lang,"V_IKIBAZO"), JOptionPane.WARNING_MESSAGE);
        } catch (Throwable ex) {
            JOptionPane.showMessageDialog(null, "  " + ex, l(lang,"V_CONTACT"), JOptionPane.WARNING_MESSAGE);    //FUNGA FATAL ERROR
        }
    }

    LinkedList<List> getPatientFile(String client, String heure, String status) {
        LinkedList<Invoice> facture = new LinkedList();
        try {
            System.out.println("select * from APP.CREDIT,APP.INVOICE where " + client
                    + "  and APP.INVOICE.id_invoice = APP.credit.id_invoice "
                    + heure + " " + status + " ORDER BY HEURE ");

            outResult = s.executeQuery("select * from APP.CREDIT,APP.INVOICE where " + client
                    + "  and APP.INVOICE.id_invoice = APP.credit.id_invoice "
                    + heure + " " + status + " ORDER BY HEURE ");
            while (outResult.next()) {
                int id_invoice = outResult.getInt(1);

                facture.add(new Invoice(id_invoice, "", "", "", 0, "", 0, 0));

            }
            outResult.close();

        } catch (Throwable e) {
            /*  Catch all exceptions and pass them to
             **  the exception reporting method             */
            System.out.println(" . . inv . exception thrown:");
            insertError(e + "", " V reimpression facture");
        }

        LinkedList<List> li = new LinkedList();
        for (int i = 0; i < facture.size(); i++) {
            li.addAll(listIdInvoice(facture.get(i).id_invoice));
        }
        return li;
    }

    public LinkedList<List> listIdInvoice(int id_invoice) {
        LinkedList<List> li = new LinkedList();
        try {
            outResult = s.executeQuery("select  APP.INVOICE.id_invoice,date,num_lot,quantite,list.price,name_product"
                    + " from APP.INVOICE,list,app.PRODUCT where  "
                    + "APP.INVOICE.id_invoice = " + id_invoice + "   and APP.INVOICE.id_invoice = APP.LIST.id_invoice AND APP.PRODUCT.CODE=APP.LIST.CODE_UNI ");
            //  Loop through the ResultSet and print the data
            while (outResult.next()) {
                // int id_invoice=outResult.getInt(1);
                String date = outResult.getString(2);
                String loti = outResult.getString(3);
                int qte = outResult.getInt(4);
                double price = outResult.getDouble(5);
                String name = outResult.getString(6);
                li.add(new List(id_invoice, price, qte, loti, date, name, 0));

            }
            //  Close the resultSet
            outResult.close();

            //  Beginning of the primary catch block: uses errorPrint method
        } catch (Throwable e) {
            insertError(e + "  list tva ", "tva ");
        }
        return li;
    }

    public LinkedList<Client> getClientV(String table, String assurance) {
        Client c;
        LinkedList<Client> client = new LinkedList();

        try {
            if (assurance.equals("SOCIETE")) {
                outResult = s.executeQuery(""
                        + "select * from APP.CLIENT where  ASSURANCE!='RAMA' AND ASSURANCE!='CORAR'"
                        + " AND ASSURANCE!='MMI' AND ASSURANCE!='AAR' AND ASSURANCE!='SORAS' ORDER BY NOM_CLIENT");
                while (outResult.next()) {
                    String NUM_AFFILIATIONa = outResult.getString(1);
                    String NOM_CLIENTa = outResult.getString(2);
                    String PRENOM_CLIENTa = outResult.getString(3);
                    int PERCENTAGEa = outResult.getInt(4);
                    int DATE_EXPa = outResult.getInt(5);
                    String ASSURANCEa = outResult.getString(6);
                    String EMPLOYEURa = outResult.getString(7);
                    String SECTEURa = outResult.getString(8);
                    String VISAa = outResult.getString(9);
                    String CODE = outResult.getString(10);
                    String AGE = outResult.getString(11);
                    String SEXE = outResult.getString(12);
                    String LINK = outResult.getString(13);
                    String BENEFICIAIRE = outResult.getString(14);
                    c = new Client(NUM_AFFILIATIONa, NOM_CLIENTa, PRENOM_CLIENTa, PERCENTAGEa, DATE_EXPa, ASSURANCEa, EMPLOYEURa, SECTEURa, VISAa, CODE, AGE, SEXE, LINK, BENEFICIAIRE);
                    client.add(c);
                }
            } else {
                outResult = s.executeQuery(""
                        + "select * from APP.CLIENT" + table + "  where   ASSURANCE='" + assurance + "' ORDER BY NOM_CLIENT");
                while (outResult.next()) {
                    String NUM_AFFILIATIONa = outResult.getString(1);
                    String NOM_CLIENTa = outResult.getString(2);
                    String PRENOM_CLIENTa = outResult.getString(3);
                    int PERCENTAGEa = outResult.getInt(4);
                    int DATE_EXPa = outResult.getInt(5);
                    String ASSURANCEa = outResult.getString(6);
                    String EMPLOYEURa = outResult.getString(7);
                    String SECTEURa = outResult.getString(8);
                    String VISAa = outResult.getString(9);
                    String CODE = outResult.getString(10);
                    String AGE = outResult.getString(11);
                    String SEXE = outResult.getString(12);
                    String LINK = outResult.getString(13);
                    String BENEFICIAIRE = outResult.getString(14);
                    c = new Client(NUM_AFFILIATIONa, NOM_CLIENTa, PRENOM_CLIENTa, PERCENTAGEa, DATE_EXPa, ASSURANCEa, EMPLOYEURa, SECTEURa, VISAa, CODE, AGE, SEXE, LINK, BENEFICIAIRE);
                    client.add(c);
                }
            }
            outResult.close();
        } catch (Throwable e) {
            insertError(e + "", "getclintV");
        }

        return client;
    }

    void insertTracking(String desi, String operation, int qty, String type) {
        try {
            psInsert = conn.prepareStatement("insert into APP.TRACKING_CLOUD(ID_EMPLOYE,OPERATION,LIGNE_ISHYIGA,TYPE)  values (?,?,?,?)");

            psInsert.setString(1, desi);
            psInsert.setString(2, operation);
            psInsert.setInt(3, qty);
            psInsert.setString(4, type);
            psInsert.executeUpdate();
            psInsert.close();
        } catch (SQLException ex) {
            insertError(ex + "", "insertTracking1");
        }
    }

    void insertTracking(String desi, String operation, int qty, String type, String received, String last, double done, int line) {
        try {
            psInsert = conn.prepareStatement("insert into APP.TRACKING_CLOUD(ID_EMPLOYE,OPERATION,LIGNE_ISHYIGA,TYPE,RECEIVED,LAST_SYNCHRO,DONE_SEC,LIGNE_CLOUD)  values (?,?,?,?,?,?,?,?)");

            psInsert.setString(1, desi);
            psInsert.setString(2, operation);
            psInsert.setInt(3, qty);
            psInsert.setString(4, type);
            psInsert.setString(5, received);
            psInsert.setString(6, last);
            psInsert.setDouble(7, done);
            psInsert.setInt(8, line);

            psInsert.executeUpdate();
            psInsert.close();
        } catch (SQLException ex) {
            insertError(ex + "", "insertMissed");
        }
    }

    void insertInAutorisation(Autorisation auto) {
        try {
            psInsert = conn.prepareStatement("insert into APP.AUTORISATION(CODE,QUANTITE,PRICE,NUM_AFFILIATION,QUITANCE,EMPLOYE,ASSURANCE,ETAT,REPONSE)  values (?,?,?,?,?,?,?,?,?)");

            psInsert.setString(1, auto.CODE);
            psInsert.setInt(2, auto.QUANTITE);
            psInsert.setDouble(3, auto.PRICE);
            psInsert.setString(4, auto.NUM_AFFILITION);
            psInsert.setString(5, auto.NUM_QUITANCE);
            psInsert.setString(6, auto.EMPLOYE);
            psInsert.setString(7, auto.ASSURANCE);
            psInsert.setString(8, auto.ETAT);
            psInsert.setString(9, auto.REPONSE);
            psInsert.executeUpdate();
            psInsert.close();
        } catch (Throwable ex) {
            insertError(ex + "", "insertAutorisation");
        }

    }

    public static double getIn(String in) {
        double productPrice = 0;
        try {
            if (in.equals("")) {
                productPrice = 0;
            } else {
                Double in_int = new Double(in);
                productPrice = in_int.doubleValue();
            }
        } catch (NumberFormatException e) {
            productPrice = 0;
        }
        return productPrice;
    }

    public double getTvaRate(String rate) {

        try {
            outResult = s.executeQuery("select  * from APP.VARIABLES "
                    + "where APP.variables.famille_variable= 'RRA' AND NOM_VARIABLE LIKE '%taxrate%' "
                    + "AND VALUE_VARIABLE='" + rate + "'");
            while (outResult.next()) {
                return Double.parseDouble(outResult.getString(4)) / 100.0;
            }
        } catch (Throwable e) {
            insertError(e + "", "getparamFam");
        }
        return 0;
    }

    public void updateExternal(int id, String data) {

        try {
            s.execute("update app.invoice set external_data='" + data + "' where id_invoice=" + id + "");
        } catch (SQLException e) {
            insertError(e + " ", "external data");
        }

    }

    int doFinishVenteV4(Invoice invoicePrint, Credit c, double money, double change, String depot) {
//     String keyIdInvoice=""+new Date()+""+invoicePrint.id_employe+ip;
//     System.out.println("key invoice:"+keyIdInvoice+ip); 

        LinkedList<Product> productPrint = invoicePrint.getProductListDB();
        LinkedList< JCheckBox> jcheck = new LinkedList();
        LinkedList< Lot> LotSortir = new LinkedList();
        LinkedList< Lot> Lotzose = new LinkedList();

        JPanel pane = new JPanel();
        pane.setLayout(new GridLayout(1, productPrint.size()));
        pane.setPreferredSize(new Dimension(productPrint.size() * 200, 300));

        for (int j = 0; j < productPrint.size(); j++) // ecrire les produits
        {
            Product p = productPrint.get(j);
            int quantity = p.qty();

            Iterator<Lot> isc = getLotSortedSetV4(p.productCode, depot).iterator();
            JPanel paneP = new JPanel();
            paneP.add(new JLabel("" + p.productName));
            paneP.add(new JLabel("QTE VOULUE: " + quantity));
            while (isc.hasNext()) {
                Lot l = isc.next();
                int live = l.qtyLive;
                JCheckBox now = new JCheckBox(" " + quantity + " - " + l);
                if (live > quantity) {
                    l.aSortir = quantity;
                    l.prodLot = p;
                    if (l.aSortir != 0) {
                        LotSortir.add(l);
                        now.setSelected(true);
                    }
                    quantity = 0;
                } else {
                    quantity = quantity - live;
                    l.aSortir = live;
                    l.prodLot = p;
                    if (l.aSortir != 0) {
                        LotSortir.add(l);
                        now.setSelected(true);
                        now.setText(" " + live + " - " + l);
                    }
                }
                Lotzose.add(l);
                jcheck.add(now);
                paneP.add(now);
            }
            pane.add(paneP);
        }

        JOptionPane.showMessageDialog(null, pane, l(lang,"V_HITAMOLOT"), JOptionPane.INFORMATION_MESSAGE);
        // pane=new JPanel();  
        pane.setLayout(new GridLayout(1, productPrint.size()));
        pane.setPreferredSize(new Dimension(productPrint.size() * 200, 300));
        boolean ntizahinduste = JCheckBoxZirasa(jcheck, Lotzose);
        // LotSortir=new LinkedList();
        boolean wahisemoFaux = true;

        if (!ntizahinduste) {
            pane = new JPanel();
            pane.setLayout(new GridLayout(1, productPrint.size()));
            pane.setPreferredSize(new Dimension(productPrint.size() * 200, 300));

            LotSortir = new LinkedList();
            for (int j = 0; j < productPrint.size(); j++) // ecrire les produits
            {
                Product p = productPrint.get(j);
                int quantity = p.qty();
                JPanel paneP = new JPanel();
                paneP.add(new JLabel("" + p.productName));
                paneP.add(new JLabel("QTE VOULUE: " + quantity));
                int v = 0;
                System.out.println(p);
                while (v < Lotzose.size()) {
                    Lot l = Lotzose.get(v);
                    if (jcheck.get(v).isSelected() == true && l.productCode == p.productCode) {
                        JLabel nowLabel = new JLabel(" " + quantity + " - " + l);
                        int live = l.qtyLive;
                        if (live > quantity) {
                            l.aSortir = quantity;
                            l.prodLot = p;
                            if (l.aSortir != 0) {
                                LotSortir.add(l);
                            }
                            quantity = 0;
                        } else {
                            quantity = quantity - live;
                            l.aSortir = live;
                            l.prodLot = p;
                            if (l.aSortir != 0) {
                                LotSortir.add(l);
                                nowLabel.setText(" " + quantity + " - " + l);
                            }
                        }

                        System.err.println(nowLabel.getText());
                        paneP.add(nowLabel);
                    }
                    v++;
                }
                pane.add(paneP);

                if (quantity != 0) {
                    JOptionPane.showMessageDialog(null, l(lang,"V_STOCKINSUFFISANT") +"\n :" + p.productName, l(lang,"V_SORTSTOCK"), JOptionPane.INFORMATION_MESSAGE);
                    wahisemoFaux = false;
                    break;
                }
            }
        }
        if (wahisemoFaux) {
            int n = JOptionPane.showConfirmDialog(null, pane, l(lang,"V_CONFIRMOUTPUT"), JOptionPane.YES_NO_OPTION);
            if (n == 0) {
                ntizahinduste = true;
            } else {
                ntizahinduste = false;
            }
        }
        System.out.println("---------3----------" + ntizahinduste);

       ///////////////////////////////////////////////////////////////////////////////////////////////
        Validator val = new Validator(invoicePrint, this);

        if (val.valid && wahisemoFaux && ntizahinduste) {
            if (checkStock(LotSortir)) {
                System.out.println(Main.tax + "---------checkStock----------" + wahisemoFaux);
                if (Main.tax != null && Main.tax.MRC2 != null) {
                    String data = null;
                    int fact = -1;
                    String key_fagiture = insertFagitureV4(LotSortir, invoicePrint, c);
                    if (key_fagiture != null) {
                        data = Main.tax.InvoiceSDCV7(invoicePrint, key_fagiture, c.PAYED,
                                Main.tax.getRRA("rtype"), "S", "", "");
                        if (data != null && !data.contains("AN ERROR MSG")) {
                            String sp[] = data.split("#");
                            try {
                                invoicePrint.id_invoice = Integer.parseInt(sp[0]);
                            } catch (NumberFormatException e) {
                                insertError("" + e, "SDC ID_INVOICE");
//                             deleteListSDC(key_fagiture); 
                                return -1;
                            }
                            if (invoicePrint.id_invoice != -1) {
                                String reference = "";

                                if (c.NUMERO_QUITANCE != null) {
                                    reference = ",reference='" + c.NUMERO_QUITANCE + "'";
                                }

                                fact = insertInvoiceListV5(LotSortir, invoicePrint, c, key_fagiture, reference);
                                invoicePrint.totA = invoicePack.getTotalA();
                                invoicePrint.tot = invoicePack.getTotal();
                                invoicePrint.totB = invoicePack.tvaPriceB;

                                if ((!invoicePrint.id_client.equals("RAMA") && !invoicePrint.id_client.equals("MMI") && invoicePack != null) || (invoicePrint.id_client.equals("MMI") && getString("defaultmmi").equals("EPSON"))) {
                                    Main.tax.photoCopyS(fact, "" + getRRA("rtype") + "S", false, money, change);
                                }

                                return fact;
                            }
                        } else if (data.contains("ERROR")) {
                            int key = getIdFacture(key_fagiture);
                            if (key == -1) {
                                deleteListSDC(key_fagiture);
                            }
                            String[] msg = data.split("#");
                            JOptionPane.showMessageDialog(null, "" + msg[0], "" + msg[1], JOptionPane.WARNING_MESSAGE);
                            return -1;
                        }
                    } else {
                        return -1;
                    }

                } else {
                    JOptionPane.showMessageDialog(null, l(lang,"V_MRCFOUND"), "ATTENTION", JOptionPane.ERROR_MESSAGE);
                }

            } else {
                JOptionPane.showMessageDialog(null, l(lang,"V_STOCKDEJA"), l(lang,"V_CHECKINGERROR"), JOptionPane.INFORMATION_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(null, l(lang,"V_ORDREDESORTIE"), l(lang,"V_ACCESSREFU"), JOptionPane.WARNING_MESSAGE);
        }

        return -1;
    }

    boolean insertSmartAssurance(SMART_MATRIX rt, Connection conn) {
        try {
            PreparedStatement psInsert2 = conn.prepareStatement("insert into APP.SMART_MATRIX"
                    + " (ID_INSURANCE,MAXIMUM_ITEM_INVOICE ,DES_INSURANCE,SCHEME_INSURANCE"
                    + "   ,FINGER_INSURANCE,TARIF_INSURANCE,MAXIMUM_AMOUNT_INVOICE)  values (?,?,?,?,?,?,?)");

            psInsert2.setString(1, rt.ID_INSURANCE);
            psInsert2.setInt(2, rt.MAXIMUM_ITEM_INVOICE);
            psInsert2.setString(3, rt.DES_INSURANCE);
            psInsert2.setString(4, rt.SCHEME_INSURANCE);
            psInsert2.setString(5, rt.FINGER_INSURANCE);
            psInsert2.setString(6, rt.TARIF_INSURANCE);
            psInsert2.setDouble(7, rt.MAXIMUM_AMOUNT_INVOICE);
            psInsert2.executeUpdate();
            psInsert2.close();

            return true;
        } catch (Throwable e) {
            insertError(e + "", "insertSmartAssurance");
        }
        return false;
    }

    boolean insertTrackSmart(SMART_TRACKING rt, Connection conn) {
        try {
            PreparedStatement psInsert = conn.prepareStatement("insert into APP.SMART_TRACKING_COMM"
                    + " (SENT_TRACKING,RECEIVED_TRACKING ,KEY_INVOICE,EMPLOYE)  values (?,?,?,?)");

            psInsert.setString(1, rt.SENT_TRACKING);
            psInsert.setString(2, rt.RECEIVED_TRACKING);
            psInsert.setString(3, rt.KEY_INVOICE);
            psInsert.setString(4, rt.EMPLOYE);
            psInsert.executeUpdate();
            psInsert.close();
            return true;
        } catch (Throwable e) {
            writeError(e + " KEY " + rt.KEY_INVOICE, "insertTrackRRA FAIL");
            //insertError(e+"","insertTrackRRA");
        }
        return false;
    }

    String doFinishVenteV4Silencieux(Invoice invoicePrint, Credit c, String waitress) {
        LinkedList<Product> productPrint = invoicePrint.getProductListDB();
        LinkedList< Lot> LotSortir = new LinkedList();

        for (int j = 0; j < productPrint.size(); j++) // ecrire les produits
        {
            Product p = productPrint.get(j);
            int quantity = p.qty();

            Iterator<Lot> isc = getLotSortedSetV4(p.productCode, "").iterator();
            while (isc.hasNext()) {
                Lot l = isc.next();
                int live = l.qtyLive;
                if (live > quantity) {
                    l.aSortir = quantity;
                    l.prodLot = p;
                    if (l.aSortir != 0) {
                        LotSortir.add(l);
                    }
                    quantity = 0;
                } else {
                    quantity = quantity - live;
                    l.aSortir = live;
                    l.prodLot = p;
                    if (l.aSortir != 0) {
                        LotSortir.add(l);
                    }
                }
            }
            if (quantity != 0) {
                return "STOCK INSUFFISANT :" + p.productName;
            }
        }

        Validator val = new Validator(invoicePrint, this);

        if (val.valid) {
            if (checkStock(LotSortir)) {
                if (Main.tax != null && Main.tax.MRC2 != null) {
                    String data;
                    int fact;
                    String key_fagiture = insertFagitureV4(LotSortir, invoicePrint, c);
                    if (key_fagiture != null) {
                        data = Main.tax.InvoiceSDCV7(invoicePrint, key_fagiture, c.PAYED,
                                Main.tax.getRRA("rtype"), "S", "", "MOBILE");

                        System.out.println("data doFinishVenteV4Silencieux " + data);

                        if (data != null && !data.contains("AN ERROR MSG")) {
                            String sp[] = data.split("#");
                            try {
                                invoicePrint.id_invoice = Integer.parseInt(sp[0]);
                            } catch (NumberFormatException e) {
                                insertError("" + e, "SDC ID_INVOICE");
//                             deleteListSDC(key_fagiture); 
                                return "" + e + " SDC ID_INVOICE";
                            }
                            if (invoicePrint.id_invoice != -1) {
                                String reference = "";
                                if (c.NUMERO_QUITANCE != null) {
                                    reference = ",reference='" + c.NUMERO_QUITANCE + "'";
                                }

                                fact = insertInvoiceListV5(LotSortir, invoicePrint, c, key_fagiture, reference);
                                invoicePrint.totA = invoicePack.getTotalA();
                                invoicePrint.tot = invoicePack.getTotal();
                                invoicePrint.totB = invoicePack.tvaPriceB;

                                return fact + "::OK::" + Main.tax.photoCopySilencieux(fact, "" + getRRA("rtype") + "S", waitress);

                            }
                        } else if (data.contains("ERROR")) {
                            int key = getIdFacture(key_fagiture);
                            if (key == -1) {
                                deleteListSDC(key_fagiture);
                            }
                            String[] msg = data.split("#");
                            // JOptionPane.showMessageDialog(null, ""+msg[0], ""+msg[1], JOptionPane.WARNING_MESSAGE);
                            return data;
                        }
                    } else {
                        return key_fagiture;
                    }

                } else {
                    JOptionPane.showMessageDialog(null, l(lang,"V_MRCFOUND"), "ATTENTION", JOptionPane.ERROR_MESSAGE);
                }

            } else {
                JOptionPane.showMessageDialog(null, l(lang,"V_STOCKDEJA"), l(lang,"V_CHECKINGERROR"), JOptionPane.INFORMATION_MESSAGE);
            }
        } else {
            return "INVALID SUMS ";
        }
        return "INTERNAL ERROR OCCURED";
    }

    public Invoice getInvoiceSDC(int ID_INVOICE, String sql) {
        Invoice invo = null;
        try {
            outResult = s.executeQuery("select * from APP.INVOICE where   ID_INVOICE=" + ID_INVOICE + " " + sql);
            System.out.println("select * from APP.INVOICE where   ID_INVOICE=" + ID_INVOICE + " " + sql);
            while (outResult.next()) {
                invo = new Invoice(ID_INVOICE, outResult.getString("EMPLOYE"), outResult.getString("DATE"),
                        outResult.getString("NUM_CLIENT"), outResult.getDouble("TOTAL"),
                        outResult.getString("HEURE"), outResult.getDouble("TVA"), 0);
                invo.key = outResult.getString("KEY_INVOICE");

            }
            outResult.close();

            outResult = s.executeQuery("select * from APP.SDC_FAGITURE where   KEY_INVOICE='" + invo.key + "'");
            while (outResult.next()) {
                invo.id_client = outResult.getString("NUM_CLIENT");
                invo.id_employe = outResult.getString("EMPLOYE");
                invo.date = outResult.getString("DATE");
                invo.tot = outResult.getDouble("TOTAL");
                invo.tva = outResult.getDouble("TVA");
                invo.CODE_BAR = outResult.getString("NUMERO_AFFILIE");
            }
            outResult.close();
            invo.client = getClient(invo.CODE_BAR, invo.id_client);
            return invo;
        } catch (Throwable e) {
            /*       Catch all exceptions and pass them to
             **       the exception reporting method             */
            System.out.println(" . .invexception thrown:");
            insertError(e + "", " V inv ");
        }
        return null;
    }

    public Credit getCredit(String key) {
        try {
            outResult = s.executeQuery("select * from APP.SDC_FAGITURE where   KEY_INVOICE='" + key + "'");
            while (outResult.next()) {
                Credit c2 = new Credit(0, outResult.getString("NUMERO_AFFILIE"),
                        outResult.getString("NUMERO_QUITANCE"), outResult.getDouble("SOLDE"),
                        outResult.getDouble("PAYED"));
                return c2;
            }
            outResult.close();
        } catch (Throwable e) {
            /*       Catch all exceptions and pass them to
             **       the exception reporting method             */
            System.out.println(" . .invexception thrown:");
            insertError(e + "", " V inv ");
        }
        return null;
    }

    public LinkedList<Lot> getProductListSDC(String key) {
        LinkedList<Lot> LIST = new LinkedList();
        try {
            outResult = s.executeQuery("select * from APP.SDC_URUTONDE,APP.LOT where KEY_INVOICE='" + key + "' "
                    + " AND CODE_LOT=CODE_UNI AND APP.LOT.BON_LIVRAISON=APP.SDC_URUTONDE.BON_LIVRAISON AND"
                    + " ID_LOTS=NUM_LOT ");

            while (outResult.next()) {
                Product prodlot = new Product(outResult.getInt("LIST_ID_PRODUCT"), outResult.getString("NAME_PRODUCT"), outResult.getString("CODE_UNI"), outResult.getDouble("PRICE"), outResult.getDouble("TVA"), outResult.getInt("QUANTITE"));
                prodlot.prix_revient = outResult.getDouble(5);
                prodlot.originalPrice = outResult.getDouble("ORIGINAL_PRICE");

                Lot l = new Lot(outResult.getInt("LIST_ID_PRODUCT"), outResult.getString("NAME_PRODUCT"),
                        outResult.getString("CODE_UNI"), outResult.getString("NUM_LOT"), outResult.getString("DATE_EXP"), 1, outResult.getInt("QTY_START"), outResult.getInt("QTY_LIVE"), outResult.getString(9));

                l.prodLot = prodlot;
                l.aSortir = outResult.getInt("QUANTITE");
                LIST.add(l);
            }
            outResult.close();

        } catch (Throwable e) {
            /*       Catch all exceptions and pass them to
             **       the exception reporting method             */
            System.out.println(" . .invexception thrown:");
            insertError(e + "", " V inv ");
        }

        return LIST;

    }

    public int doFinishVenteV7(int id_invoice, double money, double change) {
        Invoice invo = getInvoiceSDC(id_invoice, " AND NUM_CLIENT IS NULL ");
        if (invo != null) {
            Credit c2 = getCredit(invo.key);
            c2.ID_INVOICE = id_invoice;
            LinkedList< Lot> LotSortir = getProductListSDC(invo.key);
            for (int i = 0; i < LotSortir.size(); i++) {
                Product p = LotSortir.get(i).prodLot;
                invo.add(p);
            }

            if (checkStock(LotSortir)) {
                if (Main.tax != null && Main.tax.MRC2 != null) {
                    System.out.println("HAPA:" + LotSortir.size());
                    invoicePack = Main.tax.getRpack4(0, invo.getProductList(),
                            "N", "S", "", Main.tax.MRC2.MRC);
                    insertInvoiceListV5(LotSortir, invo, c2, invo.key, "");
                    Main.tax.photoCopyS(id_invoice, "" + getRRA("rtype") + "S", false, money, change);
                    return id_invoice;
                } else {
                    JOptionPane.showMessageDialog(null, l(lang,"V_MRCFOUND"), "ATTENTION", JOptionPane.ERROR_MESSAGE);
                }
            } else {
                JOptionPane.showMessageDialog(null, l(lang,"V_STOCKDEJA"), l(lang,"V_CHECKINGERROR"), JOptionPane.INFORMATION_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(null, l(lang,"V_INVALIDINVOICE"), l(lang,"V_INVOICE_ERROR"), JOptionPane.INFORMATION_MESSAGE);
        }

        return -1;
    }

    public int getRes(String num) {

        LinkedList< String> des = new LinkedList();
        try {
            String sql = "  select * from APP.RESERVATION WHERE  STATUT ='OCCUPIED' and ETAT = 'ACTIF'"
                    + " and num_client='" + num + "' "
                    + " order by num_room";
            System.out.println(sql);
            ResultSet outResult2 = s.executeQuery(sql);

            while (outResult2.next()) {
                des.add("RESER: " + outResult2.getInt("NUM_RESERVATION") + " ROOM" + outResult2.getInt("NUM_ROOM"));
            }
            outResult2.close();
        } catch (Throwable e) {
            insertError(e + " ", " getRoom");
        }

        String CHOIX = (String) JOptionPane.showInputDialog(null, " Faites votre choix ", "RESERVATION ",
                JOptionPane.QUESTION_MESSAGE, null, des.toArray(), "");

        String[] sp = CHOIX.split(" ");

        return Integer.parseInt(sp[1]);

    }

    public int doFinishVenteV5(Invoice invoicePrint, Credit c, Main main,
            int room_number, double money, double change, String depot) {
//     String keyIdInvoice=""+new Date()+""+invoicePrint.id_employe+ip;
//     System.out.println("key invoice:"+keyIdInvoice+ip); 

        LinkedList<Product> productPrint = invoicePrint.getProductListDB();
        LinkedList< JCheckBox> jcheck = new LinkedList();
        LinkedList< Lot> LotSortir = new LinkedList();
        LinkedList< Lot> Lotzose = new LinkedList();

        JPanel pane = new JPanel();
        pane.setLayout(new GridLayout(1, productPrint.size()));
        pane.setPreferredSize(new Dimension(productPrint.size() * 200, 300));

        for (int j = 0; j < productPrint.size(); j++) // ecrire les produits
        {
            Product p = productPrint.get(j);
            System.out.println("getQty:" + p.qty);
            int quantity = p.qty();

            Iterator<Lot> isc = getLotSortedSetV4(p.productCode, depot).iterator();
            JPanel paneP = new JPanel();
            paneP.add(new JLabel("" + p.productName));
            paneP.add(new JLabel("QTE VOULUE: " + quantity));
            while (isc.hasNext()) {
                Lot l = isc.next();
                int live = l.qtyLive;
                JCheckBox now = new JCheckBox(" " + quantity + " - " + l);
                if (live > quantity) {
                    l.aSortir = quantity;
                    l.prodLot = p;
                    if (l.aSortir != 0) {
                        LotSortir.add(l);
                        now.setSelected(true);
                    }
                    quantity = 0;
                } else {
                    quantity = quantity - live;
                    l.aSortir = live;
                    l.prodLot = p;
                    if (l.aSortir != 0) {
                        LotSortir.add(l);
                        now.setSelected(true);
                        now.setText(" " + live + " - " + l);
                    }
                }
                Lotzose.add(l);
                jcheck.add(now);
                paneP.add(now);
            }
            pane.add(paneP);
        }

        JOptionPane.showMessageDialog(null, pane, l(lang,"V_HITAMOLOT"), JOptionPane.INFORMATION_MESSAGE);
        // pane=new JPanel();  
        pane.setLayout(new GridLayout(1, productPrint.size()));
        pane.setPreferredSize(new Dimension(productPrint.size() * 200, 300));
        boolean ntizahinduste = JCheckBoxZirasa(jcheck, Lotzose);
        // LotSortir=new LinkedList();
        boolean wahisemoFaux = true;
        System.out.println("---------2----------");
        if (!ntizahinduste) {
            pane = new JPanel();
            pane.setLayout(new GridLayout(1, productPrint.size()));
            pane.setPreferredSize(new Dimension(productPrint.size() * 200, 300));

            LotSortir = new LinkedList();
            for (int j = 0; j < productPrint.size(); j++) // ecrire les produits
            {
                Product p = productPrint.get(j);
                int quantity = p.qty();
                JPanel paneP = new JPanel();
                paneP.add(new JLabel("" + p.productName));
                paneP.add(new JLabel("QTE VOULUE: " + quantity));
                int v = 0;
                System.out.println(p);
                while (v < Lotzose.size()) {
                    Lot l = Lotzose.get(v);
                    if (jcheck.get(v).isSelected() == true && l.productCode == p.productCode) {
                        JLabel nowLabel = new JLabel(" " + quantity + " - " + l);
                        int live = l.qtyLive;
                        if (live > quantity) {
                            l.aSortir = quantity;
                            l.prodLot = p;
                            if (l.aSortir != 0) {
                                LotSortir.add(l);
                            }
                            quantity = 0;
                        } else {
                            quantity = quantity - live;
                            l.aSortir = live;
                            l.prodLot = p;
                            if (l.aSortir != 0) {
                                LotSortir.add(l);
                                nowLabel.setText(" " + quantity + " - " + l);
                            }
                        }

                        System.err.println(nowLabel.getText());
                        paneP.add(nowLabel);
                    }
                    v++;
                }
                pane.add(paneP);

                if (quantity != 0) {
                    JOptionPane.showMessageDialog(null, l(lang,"V_STOCKINSUFFISANT") + " \n :" + p.productName, l(lang,"V_SORTSTOCK"), JOptionPane.INFORMATION_MESSAGE);
                    wahisemoFaux = false;
                    break;
                }
            }
        }
        if (wahisemoFaux) {
            int n = JOptionPane.showConfirmDialog(null, pane, l(lang,"V_CONFIRMOUTPUT"), JOptionPane.YES_NO_OPTION);
            if (n == 0) {
                ntizahinduste = true;
            } else {
                ntizahinduste = false;
            }
        }
        System.out.println("---------3----------" + ntizahinduste);
        if (wahisemoFaux && ntizahinduste) {
            if (checkStock(LotSortir)) {
                System.out.println(main.tax + "---------checkStock----------" + wahisemoFaux);
                if (main.tax != null && main.tax.MRC2 != null) {
                    String data = null;
                    int fact = -1;
                    String key_fagiture = insertFagitureV4(LotSortir, invoicePrint, c);
                    if (key_fagiture != null) {
                        System.out.println("-------------------" + Main.tax.getRRA("rtype"));
                        data = main.tax.InvoiceSDCV7(invoicePrint, key_fagiture, c.PAYED,
                                main.tax.getRRA("rtype"), "S", "", "");

                        if (data != null && !data.contains("AN ERROR MSG")) {
                            String sp[] = data.split("#");
                            try {
                                invoicePrint.id_invoice = Integer.parseInt(sp[0]);
                            } catch (NumberFormatException e) {
                                insertError("" + e, "SDC ID_INVOICE");
                                deleteListSDC(key_fagiture);
                                return -1;
                            }
                            if (invoicePrint.id_invoice != -1) {
                                fact = insertInvoiceListV5(LotSortir, invoicePrint, c, key_fagiture, ", reference='" + room_number + "'");
                                Main.tax.photoCopyS(fact, "NS", false, money, change);
                                return fact;
                            }
                        } else if (data.contains("ERROR")) {
                            int key = getIdFacture(key_fagiture);
                            if (key == -1) {
                                deleteListSDC(key_fagiture);
                            }
                            String[] msg = data.split("#");
                            JOptionPane.showMessageDialog(null, "" + msg[0], "" + msg[1], JOptionPane.WARNING_MESSAGE);
                            return -1;
                        }
                    } else {
                        return -1;
                    }

                } else {
                    JOptionPane.showMessageDialog(null, l(lang,"V_MRCFOUND"), "ATTENTION", JOptionPane.ERROR_MESSAGE);
                }

            } else {
                JOptionPane.showMessageDialog(null, l(lang,"V_STOCKDEJA"), l(lang,"V_CHECKINGERROR"), JOptionPane.INFORMATION_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(null, l(lang,"V_ORDREDESORTIE"), l(lang,"V_ACCESSREFU"), JOptionPane.WARNING_MESSAGE);
        }

        return -1;
    }

    public int dofinishProformaV7(Invoice current, String refence, String name, Main main, String mode, String Ext_Key) {

        System.out.println("dofinishprof");
        if (main.tax.rraClient.isRRA) {
            String key_fagiture = keyFinishProforma(current, refence, name);
            if (key_fagiture != null) {
                String data = null;
                data = main.tax.InvoiceSDCV7(current, key_fagiture, 0, "P", "S", "", "MOBILE");

                if (data != null && !data.contains("AN ERROR MSG")) {
                    String sp[] = data.split("#");
                    try {
                        current.id_invoice = Integer.parseInt(sp[0]);
                    } catch (NumberFormatException e) {
                        insertError("" + e, "SDC ID_INVOICE");
                    }
                    if (current.id_invoice != -1) {
                        System.out.println(current.id_invoice);
                        insertProformaV5(current, name, key_fagiture, refence, Ext_Key);
                    }
                } else if (data.contains("ERROR")) {
                    int key = getIdProforma(key_fagiture);
                    if (key == -1) {
                        deleteListSDC(key_fagiture);
                    }
                    String[] msg = data.split("#");
                    JOptionPane.showMessageDialog(null, "" + msg[0], "" + msg[1], JOptionPane.WARNING_MESSAGE);
                }
            }
        } else {
            current.id_invoice = doFinishProforma(current, refence, name, Ext_Key);
        }
        return current.id_invoice;
    }

    public int doFinishVenteV6(Invoice invoicePrint, Credit c, Main main) {

        LinkedList<Product> productPrint = invoicePrint.getProductList();
        LinkedList< Lot> LotSortir = new LinkedList();

        for (int j = 0; j < productPrint.size(); j++) // ecrire les produits
        {
            Product p = productPrint.get(j);
            double quantity = p.qtyDouble;

            Iterator<Lot> isc = getLotSortedSetV6(p.productCode).iterator();
            while (isc.hasNext()) {
                Lot l = isc.next();
                double live = l.qtyLive2;
                System.out.println(live + "  v   q " + quantity);
                if (live > quantity) {
                    l.aSortir2 = quantity;
                    l.prodLot = p;
                    if (l.aSortir2 != 0) {
                        LotSortir.add(l);
                    }
                    quantity = 0;
                    System.out.println(live + "  live>quantity " + quantity);
                } else {
                    quantity = quantity - live;
                    l.aSortir2 = live;
                    l.prodLot = p;
                    if (l.aSortir2 != 0) {
                        LotSortir.add(l);
                    }
                    System.out.println(live + "  else " + quantity);
                }
            }
        }

        if (true) {
            if (checkStock2(LotSortir)) {
                System.out.println(main.tax + "------v6---checkStock----------");
                if (main.tax != null && main.tax.MRC2 != null) {
                    String data = null;
                    int fact = -1;
                    String key_fagiture = insertFagitureV6(LotSortir, invoicePrint, c);
                    System.out.println(key_fagiture + "------v6---insertFagitureV6----------");
                    if (key_fagiture != null) {
                        System.out.println("--------v6-----------" + Main.tax.getRRA("rtype"));
                        data = main.tax.InvoiceSDCV6(invoicePrint, key_fagiture, c.PAYED,
                                main.tax.getRRA("rtype"), "S", "");

                        if (data != null && !data.contains("AN ERROR MSG")) {
                            String sp[] = data.split("#");
                            try {
                                invoicePrint.id_invoice = Integer.parseInt(sp[0]);
                            } catch (NumberFormatException e) {
                                insertError("" + e, "SDC ID_INVOICE");
                                deleteListSDC(key_fagiture);
                                return -1;
                            }
                            if (invoicePrint.id_invoice != -1) {
                                fact = insertInvoiceListV6(LotSortir, invoicePrint, c, key_fagiture, " ");
                                return fact;
                            }
                        } else if (data.contains("ERROR")) {
                            int key = getIdFacture(key_fagiture);
                            if (key == -1) {
                                deleteListSDC(key_fagiture);
                            }
                            String[] msg = data.split("#");
                            JOptionPane.showMessageDialog(null, "" + msg[0], "" + msg[1], JOptionPane.WARNING_MESSAGE);
                            return -1;
                        }
                    } else {
                        return -1;
                    }

                } else {
                    JOptionPane.showMessageDialog(null, l(lang,"V_MRCFOUND"), "ATTENTION", JOptionPane.ERROR_MESSAGE);
                }

            } else {
                JOptionPane.showMessageDialog(null, l(lang,"V_STOCKDEJA"), l(lang,"V_CHECKINGERROR"), JOptionPane.INFORMATION_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(null, l(lang,"V_ORDREDESORTIE"), l(lang,"V_ACCESSREFU"), JOptionPane.WARNING_MESSAGE);
        }

        return -1;
    }

    String insertFagitureV4(LinkedList<Lot> LotSortir, Invoice invo, Credit c) {
        String keyIdInvoice = "" + new Date() + "" + invo.id_employe + ip;
        try {
            if (LotSortir.size() > 0) {
                psInsert = conn.prepareStatement("insert into APP.SDC_FAGITURE (NUM_CLIENT,DATE ,TOTAL,EMPLOYE,TVA,REDUCTION,KEY_INVOICE,NUMERO_AFFILIE,NUMERO_QUITANCE,SOLDE,PAYED)"
                        + "  values (?,?,?,?,?,?,?,?,?,?,?)");
                psInsert.setString(1, invo.id_client);
                psInsert.setString(2, invo.date);
                psInsert.setDouble(3, invo.tot);
                psInsert.setString(4, invo.id_employe);
                psInsert.setDouble(5, invo.tva);
                psInsert.setDouble(6, invo.REDUCTION);
                psInsert.setString(7, keyIdInvoice);
                psInsert.setString(8, c.NUMERO_AFFILIE);
                psInsert.setString(9, c.NUMERO_QUITANCE);
                psInsert.setDouble(10, c.SOLDE);
                psInsert.setDouble(11, c.PAYED);
                psInsert.executeUpdate();
                psInsert.close();

                psInsert = conn.prepareStatement("insert into APP.SDC_URUTONDE (CODE_UNI,NUM_LOT,QUANTITE,PRICE,prix_revient,KEY_INVOICE,LIST_ID_PRODUCT,"
                        + "TVA,BON_LIVRAISON,NAME_PRODUCT,ORIGINAL_PRICE)  values (?,?,?,?,?,?,?,?,?,?,?)");

                for (int j = 0; j < LotSortir.size(); j++) // ecrire les produits
                {
                    Lot l = LotSortir.get(j);
                    psInsert.setString(1, l.prodLot.code);
                    psInsert.setString(2, l.id_LotS);
                    psInsert.setInt(3, l.aSortir);
                    psInsert.setDouble(4, l.prodLot.currentPrice);
                    psInsert.setDouble(5, l.revient);
                    psInsert.setString(6, keyIdInvoice);
                    psInsert.setInt(7, l.prodLot.productCode);
                    psInsert.setDouble(8, l.prodLot.tva);
                    psInsert.setString(9, l.bon_livraison);
                    psInsert.setString(10, l.prodLot.productName);
                    psInsert.setDouble(11, l.prodLot.originalPrice);
                    psInsert.executeUpdate();
                }
                psInsert.close();

                System.out.println("key invoice:" + keyIdInvoice);

                if (Main.tax.smartPatient != null) {
                    updateSmartTracking2("KEY_INVOICE='" + keyIdInvoice + "'", Main.tax.smartPatient.global_id,
                            " AND (KEY_INVOICE is null or KEY_INVOICE='')", s);
                }
                return keyIdInvoice;
            }
        } catch (Throwable e) {
            writeError(e + " KEY " + keyIdInvoice, "doFinishVente FAIL");
            insertError(e + "", "doFinishVente");
        }
        return null;
    }

    String insertFagitureV6(LinkedList<Lot> LotSortir, Invoice invo, Credit c) {
        String keyIdInvoice = "" + new Date() + "" + invo.id_employe + ip;
        System.out.println(keyIdInvoice + "   size  " + LotSortir.size());
        try {
            if (LotSortir.size() > 0) {
                psInsert = conn.prepareStatement("insert into APP.SDC_FAGITURE "
                        + "(NUM_CLIENT,DATE ,TOTAL,EMPLOYE,TVA,REDUCTION,KEY_INVOICE)  values (?,?,?,?,?,?,?)");

                psInsert.setString(1, invo.id_client);
                psInsert.setString(2, invo.date);
                psInsert.setDouble(3, invo.tot);
                psInsert.setString(4, invo.id_employe);
                psInsert.setDouble(5, invo.tva);
                psInsert.setDouble(6, invo.REDUCTION);
                psInsert.setString(7, keyIdInvoice);
                psInsert.executeUpdate();
                psInsert.close();

                psInsert = conn.prepareStatement("insert into APP.SDC_URUTONDE"
                        + " (CODE_UNI,NUM_LOT,QUANTITE,PRICE,prix_revient,KEY_INVOICE,LIST_ID_PRODUCT,"
                        + "TVA,BON_LIVRAISON,NAME_PRODUCT,ORIGINAL_PRICE)  values (?,?,?,?,?,?,?,?,?,?,?)");

                for (int j = 0; j < LotSortir.size(); j++) // ecrire les produits
                {
                    Lot l = LotSortir.get(j);
                    psInsert.setString(1, l.prodLot.code);
                    psInsert.setString(2, l.id_LotS);
                    psInsert.setDouble(3, l.aSortir2);
                    psInsert.setDouble(4, l.prodLot.currentPrice);
                    psInsert.setDouble(5, l.revient);
                    psInsert.setString(6, keyIdInvoice);
                    psInsert.setInt(7, l.prodLot.productCode);
                    psInsert.setDouble(8, l.prodLot.tva);
                    psInsert.setString(9, l.bon_livraison);
                    psInsert.setString(10, l.prodLot.productName);
                    psInsert.setDouble(11, l.prodLot.originalPrice);
                    psInsert.executeUpdate();
                }
                psInsert.close();

                System.out.println("key invoice:" + keyIdInvoice);
                return keyIdInvoice;
            }
        } catch (Throwable e) {
            writeError(e + " KEY " + keyIdInvoice, "doFinishVente FAIL");
            insertError(e + "", "doFinishVente");
        }
        return null;
    }

    public void updateTosend(String key, String data) {

        try {
            s.execute("update app.SDC_FAGITURE set TOSEND='" + data + "' where KEY_INVOICE='" + key + "'");
        } catch (SQLException e) {
            insertError(e + " ", "tosend data");
        }
    }

    int insertInvoiceListV5(LinkedList<Lot> LotSortir, Invoice invo,
            Credit c, String key, String refence) {
        int fact = getIdFacture(key);
        double red = 0;
        try {
            if (LotSortir.size() > 0 && fact != -1 && fact == invo.id_invoice) {
                if (sortStock(LotSortir)) {
                    String listUpdateSql = " update APP.INVOICE  SET NUM_CLIENT='" + invo.id_client + "', DATE='" + invo.date + "'"
                            + " , TOTAL=" + RRA_PRINT2.setSansVirgule(invoicePack.getTotal()) + " , EMPLOYE='" + invo.id_employe + "'"
                            + " ,TVA=" + RRA_PRINT2.setSansVirgule(invoicePack.getTotalTaxes()) + " , REDUCTION=" + RRA_PRINT2.setSansVirgule(invo.REDUCTION) + ""
                            + ", TOTAL_A=" + RRA_PRINT2.setSansVirgule(invoicePack.getTotalA()) + ", TOTAL_B=" + RRA_PRINT2.setSansVirgule(invoicePack.getTotalB()) + ""
                            + " , TOTAL_C=" + RRA_PRINT2.setSansVirgule(invoicePack.getTotalD()) + ", TOTAL_D=" + RRA_PRINT2.setSansVirgule(invoicePack.getTotalD()) + ""
                            + " , TAXE_A=" + RRA_PRINT2.setSansVirgule(invoicePack.tvaPriceA) + ", TAXE_B=" + RRA_PRINT2.setSansVirgule(invoicePack.tvaPriceB) + ""
                            + " , TAXE_C=" + RRA_PRINT2.setSansVirgule(invoicePack.tvaPriceC) + ", TAXE_D=" + RRA_PRINT2.setSansVirgule(invoicePack.tvaPriceD) + ""
                            + " , MRC='" + invoicePack.MRC + "', TIN='" + invoicePack.ClientTin + "', CASH=" + RRA_PRINT2.setSansVirgule(c.PAYED) + ""
                            + " , CREDIT=" + RRA_PRINT2.setSansVirgule(c.SOLDE) + ""
                            + refence + "   where  key_invoice= '" + key + "' and id_invoice=" + fact + "";
                    System.out.println(listUpdateSql);
                    s.execute(listUpdateSql);

                    psInsert = conn.prepareStatement("insert into APP.LIST (CODE_UNI,NUM_LOT,QUANTITE,PRICE,prix_revient,KEY_INVOICE,ID_INVOICE,"
                            + "LIST_ID_PRODUCT,TVA,ORIGINAL_PRICE,BON_LIVRAISON,DATE_EXP)  values (?,?,?,?,?,?,?,?,?,?,?,?)");

                    for (int j = 0; j < LotSortir.size(); j++) // ecrire les produits
                    {
                        Lot l = LotSortir.get(j);

                        psInsert.setString(1, l.prodLot.code);
                        psInsert.setString(2, l.id_LotS);
                        psInsert.setInt(3, l.aSortir);
                        psInsert.setDouble(4, l.prodLot.currentPrice);
                        psInsert.setDouble(5, l.revient);
                        psInsert.setString(6, key);
                        psInsert.setInt(7, invo.id_invoice);
                        psInsert.setInt(8, l.prodLot.productCode);
                        psInsert.setDouble(9, l.prodLot.tva);
                        psInsert.setDouble(10, l.prodLot.originalPrice);
                        psInsert.setString(11, l.bon_livraison);
                        psInsert.setString(12, l.dateExp);
                        psInsert.executeUpdate();
                        red += ((l.prodLot.originalPrice - l.prodLot.currentPrice) * l.aSortir);
                    }
                    psInsert.close();
                    s.execute("update APP.INVOICE  SET REDUCTION=" + red + " where  key_invoice= '" + key + "' and id_invoice=" + fact + "");

                    if (!invo.id_client.equals("CASHNORM") || (invo.id_client.equals("CASHNORM") && c.NUMERO_AFFILIE.equals("DIVERS"))) {
                        psInsert = conn.prepareStatement("insert into APP.CREDIT (NUMERO_AFFILIE ,NUMERO_QUITANCE,SOLDE,PAYED,CAUSE,KEY_INVOICE,ID_INVOICE)  values (?,?,?,?,?,?,?)");

                        psInsert.setString(1, c.NUMERO_AFFILIE);
                        psInsert.setString(2, c.NUMERO_QUITANCE);
                        psInsert.setDouble(3, c.SOLDE);
                        psInsert.setDouble(4, c.PAYED);
                        psInsert.setString(5, c.RAISON);
                        psInsert.setString(6, key);
                        psInsert.setInt(7, invo.id_invoice);
                        psInsert.executeUpdate();
                        psInsert.close();
                    }

                    String time_rec_mrc = "";

                    outResult = s.executeQuery("select  heure from APP.invoice WHERE  id_invoice=" + invo.id_invoice + "");
                    while (outResult.next()) {
                        time_rec_mrc = ("" + outResult.getTimestamp(1));
                    }
                    //System.out.println("ttttttttttttttt   "+time_rec_mrc);      
                    outResult.close();

                    String creditUpdateSql = " delete from APP.SDC_URUTONDE  where  key_invoice= '" + key + "' ";
                    System.out.println("IYI:" + creditUpdateSql);
                    s.execute(creditUpdateSql);
                    creditUpdateSql = " delete from APP.SDC_FAGITURE  where  key_invoice= '" + key + "' ";
                    s.execute(creditUpdateSql);

                    return fact;
                } else {
                    JOptionPane.showMessageDialog(null, l(lang,"V_STOCKNTISOHOTSE"), l(lang,"V_SORTSTOCK"), JOptionPane.INFORMATION_MESSAGE);
                    return -1;
                }
            } else {
                JOptionPane.showMessageDialog(null, l(lang,"V_SDCFACTURE"), l(lang,"V_ERRORINVOICE"), JOptionPane.INFORMATION_MESSAGE);
                return -1;
            }
        } catch (Throwable e) {
            writeError(e + " KEY " + key, "doFinishVente FAIL");
            insertError(e + "", "doFinishVente");
        }
        return -1;
    }

    int insertInvoiceListV6(LinkedList<Lot> LotSortir, Invoice invo,
            Credit c, String key, String refence) {
        int fact = getIdFacture(key);
        double red = 0;
        try {
            if (LotSortir.size() > 0 && fact != -1 && fact == invo.id_invoice) {
                if (sortStock2(LotSortir)) {
                    String listUpdateSql = " update APP.INVOICE  SET NUM_CLIENT='" + invo.id_client + "', DATE='" + invo.date + "'"
                            + " , TOTAL=" + RRA_PRINT2.setSansVirgule(invoicePack.getTotal()) + " , EMPLOYE='" + invo.id_employe + "'"
                            + " ,TVA=" + RRA_PRINT2.setSansVirgule(invoicePack.getTotalTaxes()) + " , REDUCTION=" + RRA_PRINT2.setSansVirgule(invo.REDUCTION) + ""
                            + ", TOTAL_A=" + RRA_PRINT2.setSansVirgule(invoicePack.getTotalA()) + ", TOTAL_B=" + RRA_PRINT2.setSansVirgule(invoicePack.getTotalB()) + ""
                            + " , TOTAL_C=" + RRA_PRINT2.setSansVirgule(invoicePack.getTotalD()) + ", TOTAL_D=" + RRA_PRINT2.setSansVirgule(invoicePack.getTotalD()) + ""
                            + " , TAXE_A=" + RRA_PRINT2.setSansVirgule(invoicePack.tvaPriceA) + ", TAXE_B=" + RRA_PRINT2.setSansVirgule(invoicePack.tvaPriceB) + ""
                            + " , TAXE_C=" + RRA_PRINT2.setSansVirgule(invoicePack.tvaPriceC) + ", TAXE_D=" + RRA_PRINT2.setSansVirgule(invoicePack.tvaPriceD) + ""
                            + " , MRC='" + invoicePack.MRC + "', TIN='" + invoicePack.ClientTin + "', CASH=" + RRA_PRINT2.setSansVirgule(c.PAYED) + ", CREDIT=" + RRA_PRINT2.setSansVirgule(c.SOLDE) + ""
                            + refence + "   where  key_invoice= '" + key + "' and id_invoice=" + fact + "";
                    System.out.println(listUpdateSql);

                    s.execute(listUpdateSql);

                    psInsert = conn.prepareStatement("insert into APP.LIST (CODE_UNI,NUM_LOT,QUANTITE,PRICE,prix_revient,KEY_INVOICE,ID_INVOICE,"
                            + "LIST_ID_PRODUCT,TVA,ORIGINAL_PRICE,BON_LIVRAISON)  values (?,?,?,?,?,?,?,?,?,?,?)");

                    for (int j = 0; j < LotSortir.size(); j++) // ecrire les produits
                    {
                        Lot l = LotSortir.get(j);

                        psInsert.setString(1, l.prodLot.code);
                        psInsert.setString(2, l.id_LotS);
                        psInsert.setDouble(3, l.aSortir2);
                        psInsert.setDouble(4, l.prodLot.currentPrice);
                        psInsert.setDouble(5, l.revient);
                        psInsert.setString(6, key);
                        psInsert.setInt(7, invo.id_invoice);
                        psInsert.setInt(8, l.prodLot.productCode);
                        psInsert.setDouble(9, l.prodLot.tva);
                        psInsert.setDouble(10, l.prodLot.originalPrice);
                        psInsert.setString(11, l.bon_livraison);
                        psInsert.executeUpdate();
                        red += ((l.prodLot.originalPrice - l.prodLot.currentPrice) * l.aSortir);
                    }
                    psInsert.close();
                    s.execute("update APP.INVOICE  SET REDUCTION=" + red + " where  key_invoice= '" + key + "' and id_invoice=" + fact + "");

                    if (!invo.id_client.equals("CASHNORM") || (invo.id_client.equals("CASHNORM") && c.NUMERO_AFFILIE.equals("DIVERS"))) {
                        psInsert = conn.prepareStatement("insert into APP.CREDIT (NUMERO_AFFILIE ,NUMERO_QUITANCE,SOLDE,PAYED,CAUSE,KEY_INVOICE,ID_INVOICE)  values (?,?,?,?,?,?,?)");

                        psInsert.setString(1, c.NUMERO_AFFILIE);
                        psInsert.setString(2, c.NUMERO_QUITANCE);
                        psInsert.setDouble(3, c.SOLDE);
                        psInsert.setDouble(4, c.PAYED);
                        psInsert.setString(5, c.RAISON);
                        psInsert.setString(6, key);
                        psInsert.setInt(7, invo.id_invoice);
                        psInsert.executeUpdate();
                        psInsert.close();
                    }

                    String time_rec_mrc = "";

                    outResult = s.executeQuery("select  heure from APP.invoice WHERE  id_invoice=" + invo.id_invoice + "");
                    while (outResult.next()) {
                        time_rec_mrc = ("" + outResult.getTimestamp(1));
                    }
                    //System.out.println("ttttttttttttttt   "+time_rec_mrc);      
                    outResult.close();

                    String creditUpdateSql = " delete from APP.SDC_URUTONDE  where  key_invoice= '" + key + "' ";
                    s.execute(creditUpdateSql);
                    System.err.println(creditUpdateSql);
                    return fact;
                } else {
                    JOptionPane.showMessageDialog(null, l(lang,"V_STOCKNTISOHOTSE"), l(lang,"V_SORTSTOCK"), JOptionPane.INFORMATION_MESSAGE);
                    return -1;
                }
            } else {
                JOptionPane.showMessageDialog(null, l(lang,"V_SDCFACTURE"), l(lang,"V_ERRORINVOICE"), JOptionPane.INFORMATION_MESSAGE);
                return -1;
            }
        } catch (Throwable e) {
            writeError(e + " KEY " + key, "doFinishVente FAIL");
            insertError(e + "", "doFinishVente");
        }
        return -1;
    }

    public String getExternalDataV4(String colonne, int id_invoice, String table, String colonne2) {

        try {
            System.out.println("select  " + colonne + " from APP." + table + " WHERE  " + colonne2 + "=" + id_invoice + "");

            outResult = s.executeQuery("select  " + colonne + " from APP." + table + " WHERE  " + colonne2 + "=" + id_invoice + "");
            while (outResult.next()) {
                return (outResult.getString(1));
            }

            outResult.close();
        } catch (Throwable e) {
            insertError(e + "", "getIdfacture");
        }
        return null;

    }

    int insertAnnuleListV4(LinkedList<Lot> LotSortir, int id_invoice, String key) {
        int fact = getIdFacture(key);

        try {
            if (LotSortir.size() > 0) {
                psInsert = conn.prepareStatement("insert into APP.ANNULE_LIST (CODE_UNI,NUM_LOT,QUANTITE,PRICE,prix_revient,KEY_INVOICE,ID_INVOICE,LIST_ID_PRODUCT,TVA,NAME_PRODUCT)  values (?,?,?,?,?,?,?,?,?,?)");

                for (int j = 0; j < LotSortir.size(); j++) // ecrire les produits
                {
                    Lot l = LotSortir.get(j);

                    psInsert.setString(1, l.prodLot.code);
                    psInsert.setString(2, l.id_LotS);
                    psInsert.setInt(3, l.qtyLive);
                    psInsert.setDouble(4, l.prodLot.currentPrice);
                    psInsert.setDouble(5, l.revient);
                    psInsert.setString(6, key);
                    psInsert.setInt(7, id_invoice);
                    psInsert.setInt(8, l.prodLot.productCode);
                    psInsert.setDouble(9, l.prodLot.tva);
                    psInsert.setString(10, l.prodLot.productName);
                    psInsert.executeUpdate();
                }
                psInsert.close();
                return fact;
            } else {
                JOptionPane.showMessageDialog(null, l(lang,"V_NO_PRODUCT_FOUND"), l(lang,"V_INVOICE_ERROR"), JOptionPane.INFORMATION_MESSAGE);
                return -1;
            }
        } catch (Throwable e) {
            writeError(e + " KEY " + key, "doFinishVente FAIL");
            insertError(e + "", "doFinishVente");
        }
        return -1;
    }

    int insertRefundListV4(LinkedList<Lot> LotSortir, int id_invoice, String key) {
        int fact = getIdFacture(key);

        try {
            if (LotSortir.size() > 0) {
                psInsert = conn.prepareStatement("insert into APP.REFUND_LIST (CODE_UNI,NUM_LOT,QUANTITE,PRICE,prix_revient,KEY_INVOICE,"
                        + "ID_INVOICE,LIST_ID_PRODUCT,TVA,NAME_PRODUCT,ID_REFUND,ORIGINAL_PRICE)  values (?,?,?,?,?,?,?,?,?,?,?,?)");

                for (int j = 0; j < LotSortir.size(); j++) // ecrire les produits
                {
                    Lot l = LotSortir.get(j);

                    psInsert.setString(1, l.prodLot.code);
                    psInsert.setString(2, l.id_LotS);
                    psInsert.setInt(3, l.qtyLive);
                    psInsert.setDouble(4, l.prodLot.currentPrice);
                    psInsert.setDouble(5, l.revient);
                    psInsert.setString(6, key);
                    psInsert.setInt(7, id_invoice);
                    psInsert.setInt(8, l.prodLot.productCode);
                    psInsert.setDouble(9, l.prodLot.tva);

                    if (l.prodLot.productName != null && l.prodLot.productName.contains("#")) {
                        l.prodLot.productName = l.prodLot.productName.split("#")[0];
                    }
                    psInsert.setString(10, l.prodLot.productName);
                    psInsert.setInt(11, -1);
                    psInsert.setDouble(12, l.prodLot.originalPrice);
                    psInsert.executeUpdate();

                }
                psInsert.close();
                return fact;
            } else {
                JOptionPane.showMessageDialog(null, "V_NO_PRODUCT_FOUND", l(lang,"V_INVOICE_ERROR"), JOptionPane.INFORMATION_MESSAGE);
                return -1;
            }
        } catch (Throwable e) {
            writeError(e + " KEY " + key, "insertRefundList FAIL");
            insertError(e + "", "insertRefundList");
        }
        return -1;
    }

    public void updateListAnnule(int id) {
        try {
            Statement sD = conn.createStatement();
            String createString1 = " update app.annule_list set id_invoice=-1 where id_invoice=" + id;
            sD.execute(createString1);
        } catch (SQLException ex) {
            Logger.getLogger(Db.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void deleteListSDC(String key) {
        try {
            Statement sD = conn.createStatement();
            System.out.println(" delete from sdc_fagiture  where key_invoice='" + key + "'");
            String createString1 = " delete from sdc_fagiture  where key_invoice='" + key + "'";
            sD.execute(createString1);
            createString1 = " delete from sdc_urutonde  where key_invoice='" + key + "'";
            sD.execute(createString1);
        } catch (SQLException ex) {
            insertError("" + ex, "DELET SDC");
        }
    }

    public void deleteRefundList(String key) {
        try {
            Statement sD = conn.createStatement();
            String createString1 = " delete from refund_list  where key_invoice='" + key + "' and id_refund=-1";
            sD.execute(createString1);
        } catch (SQLException ex) {
            insertError("" + ex, "DELET SDC");
        }
    }

    int insertProformaV5(Invoice invo, String c, String key, String ref, String Ext_Key) {
        int fact = getIdProforma(key);
        System.out.println("Ext_KeyExt_KeyExt_KeyExt_Key   " + Ext_Key);
        System.out.println(fact);
        System.out.println(invo.id_client);
        Client client = new Client("", c, ref, 0, 0, invo.id_client, "", "", "", "");
        LinkedList<String> Clientinfo = Leclient(client);
        try {
            if (fact == invo.id_invoice) {
                String listUpdateSql = " update APP.bon_proforma  SET NUM_CLIENT='" + invo.id_client + "', DATE='" + invo.date + "'"
                        + " , TOTAL=" + invo.tot + " , EMPLOYE='" + invo.id_employe + "' ,TVA=" + invo.tva + ""
                        + " , REDUCTION=" + invo.REDUCTION + ", REFERENCE='" + ref + "', NOM_CLIENT='" + c + "'"
                        + ", TOTAL_A=" + RRA_PRINT2.setSansVirgule(invoicePack.getTotalA()) + ", TOTAL_B=" + RRA_PRINT2.setSansVirgule(invoicePack.getTotalB()) + ""
                        + " , TOTAL_C=" + RRA_PRINT2.setSansVirgule(invoicePack.getTotalD()) + ", TOTAL_D=" + RRA_PRINT2.setSansVirgule(invoicePack.getTotalD()) + ""
                        + " , TAXE_A=" + RRA_PRINT2.setSansVirgule(invoicePack.tvaPriceA) + ", TAXE_B=" + RRA_PRINT2.setSansVirgule(invoicePack.tvaPriceB) + ""
                        + " , TAXE_C=" + RRA_PRINT2.setSansVirgule(invoicePack.tvaPriceC) + ", TAXE_D=" + RRA_PRINT2.setSansVirgule(invoicePack.tvaPriceD) + ""
                        + " , MRC='" + invoicePack.MRC + "', TIN='" + invoicePack.ClientTin + "', EXT_KEY='" + Ext_Key + "'"
                        + ""
                        + " where  key_invoice= '" + key + "' and ID_PROFORMA=" + fact + "";
                System.out.println(listUpdateSql);
                s.execute(listUpdateSql);

                listUpdateSql = " update APP.LIST_PROFORMA  SET ID_PROFORMA=" + fact + " where  key_invoice= '" + key + "' and ID_PROFORMA=-1";
                s.execute(listUpdateSql);

                String resp = getExternalDataV4("EXTERNAL_DATA", fact, "BON_PROFORMA", "ID_PROFORMA");
                String[] sp = resp.split(",");
             ////>SDC001000638,8,36,NS,02/09/2013 09:31:09,Q3CYMKAPARLNCBXR,AF4VYRUIXYVJVUWHKYZGE2RTDY
//             RRA_PACK rp=getRpack3(invo.getProductListDB());
//             RRA_PACK rp= getRpack3(0,invo.getProductList(),
//             ""+isaha,  "P","S");

                String PRINT = (String) JOptionPane.showInputDialog(null, l(lang, "V_OPTIONS"), " OPTIONS IMPRESSION",
                        JOptionPane.QUESTION_MESSAGE, null, new String[]{"EPSON", "A4"}, "EPSON");
                if (PRINT.equals("EPSON")) {
                        int margin = 5;
                    String marginVar = getRRA("margin");
                    try {
                        margin = Integer.parseInt(marginVar);
                    } catch (NumberFormatException ex) {
                        System.err.println(marginVar + "   marginVar   " + ex);
                    }

                    RRA_PRINT2 printer = new RRA_PRINT2(getRRA("company"), getString("bp"), getRRA("City"), getRRA("tin"), "" + invoicePack.ClientTin,
                            invoicePack.getTotal(), invoicePack.getTotalA(), invoicePack.getTotalB(), invoicePack.getTotalTaxes(), invoicePack.getTotalTaxes(),
                            invoicePack.getTotal(), sp[0], sp[1] + "/" + sp[2] + "  " + sp[3],
                            sp[6], sp[5], invo.id_invoice, invoicePack.MRC, invo.getProductList(), "P", "S", getRRA("version"), 1, 0, Clientinfo, Main.tax.getNowTime(getMRCtimeProfo(fact)), sp[4], invo.id_employe, "", s, "" + c, margin, 0, 0);
                } 
                else 
                {
                    RRA_PRINT2 r = Main.tax.printRRAa4("" + "", "P", "S", invo.getProductList(),
                    invoicePack, invo.id_invoice);
//double [] ibiciro= new double[]{r.TOTAL,r.TOTAL_A_EX,r.TOTAL_B_18,r.TOTAL_TAX,r.TOTAL_TAX_B,0,0};
            double[] ibiciro = new double[]{r.TOTAL_A_EX, r.TOTAL_B_18, r.TOTAL_TAX_B, r.TOTAL_TAX, r.TOTAL, 0, 0};
            PrintVars convertInvoiceVars = convertInvoiceVars(invo);
                    System.out.println("Hano nahageze convert");
                    lotList=new LinkedList();
            //PrintTiers convertInvoiceTiers = convertInvoiceTiers(invo.client,"");
                    for(int i=0;i<invo.getProductList().size();i++)
                    {
                        System.out.println(i);
                        Product p=invo.getProductList().get(i);
                        System.out.println(p.code+":"+p.productName+"::"+p.qty+":::"+p.currentPrice+":-:"+p.tva);
                        Lot lo = new Lot(p.code, p.productName, "", "", "" + p.qty,
                        "" + p.currentPrice, "" + p.tva, ""+p.currentPrice, "" + (p.currentPrice * p.qty), "", "");
                
                        lotList.add(lo);
                        
                    }
                    System.out.println(lotList);
            PrintV3 printer = new PrintV3(this, img1, convertInvoiceTiers(client, ""), convertInvoiceVars, lotList, ibiciro,
                    new String[]{"", "", "", "", "", "", "", "", "", "", "", ""}, r, 1, rraLogo);


                }
                
                
                
// printer =new  RRA_PRINT2(getRRA("company"), getString("bp"), getRRA("City"),getRRA("tin"), ""+invoicePack.ClientTin,
//invoicePack.getTotal(),invoicePack.getTotalA(),invoicePack.getTotalB(), invoicePack.getTotalTaxes(), invoicePack.getTotalTaxes(),
//invoicePack.getTotal() , sp[0],sp[1]+"/"+sp[2]+"  "+sp[3],
//sp[6],sp[5],invo.id_invoice,invoicePack.MRC,invo.getProductList()
//,"P","S",getRRA("version"),1,0,Clientinfo,Main.tax.getNowTime(getMRCtimeProfo( fact)),sp[4]
//        ,invo.id_employe,"",s,""+c); 
                //
//if(printer.byempurimwe)
//{
//    updateExternal(id_invoice, response+"#PRINTED");
//} 
                return fact;

            } else {
                deleteListProforma(key);
                JOptionPane.showMessageDialog(null, l(lang,"V_SDCPROFORMA"), l(lang,"V_ERRORPROFORMA"), JOptionPane.INFORMATION_MESSAGE);
                return -1;
            }
        } catch (Throwable e) {
            writeError(e + " KEY " + key, "doFinishPROFORMA FAIL");
            insertError(e + "", "doFinishPROFORMA");
        }
        return -1;
    }

    public String getMRCtimeProfo(int fact) {
        String date_rec_mrc = "";
        try {

            outResult = s.executeQuery("select  heure from APP.bon_proforma WHERE ID_PROFORMA=" + fact);
            while (outResult.next()) {
                date_rec_mrc = (outResult.getString(1));
            }
            outResult.close();
        } catch (SQLException ex) {
            insertError("" + ex, "DELETE LIST PROFORMA");
        }

        return date_rec_mrc;
    }

    public void deleteListProforma(String key) {
        try {
            Statement sD = conn.createStatement();
            String creditUpdateSql = " delete from APP.LIST_PROFORMA  where  key_invoice= '" + key + "' ";
            s.execute(creditUpdateSql);
        } catch (SQLException ex) {
            insertError("" + ex, "DELETE LIST PROFORMA");
        }
    }

    public int getMaxValue(String colonne, String id_invoice, String table, String colonne2) {

        try {
            outResult = s.executeQuery("select  " + colonne + " from APP." + table + " WHERE  " + colonne2 + "=" + id_invoice + "");
            while (outResult.next()) {
                return (outResult.getInt(1));
            }

            outResult.close();
        } catch (Throwable e) {
            insertError(e + "", "getIdfacture");
        }
        return -1;

    }

    public static String setVirgule(double frw) {
        BigDecimal big = new BigDecimal(frw);
        String getBigString = big.toString();
        //       System.out.println(getBigString);
        getBigString = getBigString.replace('.', ';');
        // System.out.println(getBigString);
        String[] sp = getBigString.split(";");

        String decimal = "";
        if (sp.length > 1) {
            decimal = sp[1];
        }

        if (decimal.equals("") || (decimal.length() == 1 && decimal.equals("0"))) {
            decimal = ".00";
        } else if (decimal.length() == 1 && !decimal.equals("0")) {
            decimal = "." + decimal + "0";
        } else {
            decimal = "." + decimal.substring(0, 2);
        }

        String toret = setVirgule(sp[0]) + decimal;

        return toret;
    }

    static String setVirgule(String frw) {
        String setString = "";
        int k = 0;
        for (int i = (frw.length() - 1); i >= 0; i--) {
            k++;
            if ((k - 1) % 3 == 0 && (k - 1) != 0) {
                setString += ",";
            }
            setString += frw.charAt(i);
        }
        return inverse(setString);
    }

    static String inverse(String frw) {
        String l = "";
        for (int i = (frw.length() - 1); i >= 0; i--) {
            l += frw.charAt(i);
        }
        return l;
    }

    public LinkedList<String> Leclient(Client infoClient) {
        if (infoClient != null) {
            LinkedList<String> Clientinfo = new LinkedList<String>();

            Clientinfo.add(infoClient.NUM_AFFILIATION);
            Clientinfo.add(infoClient.NOM_CLIENT);
            Clientinfo.add(infoClient.PRENOM_CLIENT);
            Clientinfo.add(infoClient.ASSURANCE);
            Clientinfo.add("" + infoClient.PERCENTAGE);
            Clientinfo.add(infoClient.EMPLOYEUR);
            return Clientinfo;
        }
        return null;
    }

    void printRetardRRA(int ID_INVOICE, String choix) {
        RRA_RECORD l = null;

        if (!choix.contains("R")) {
            l = Main.tax.getInvoice(ID_INVOICE, choix, "INVOICE");
        } else {
            l = Main.tax.getInvoice(ID_INVOICE, choix, "REFUND");
        }
        String line = "select  date_exp,code_uni,num_lot,quantite ,list.price,name_product,PRODUCT.TVA"
                + ", original_price from APP.PRODUCT,APP.LIST where "
                + "  APP.LIST.id_invoice = " + ID_INVOICE + ""
                + " and  APP.PRODUCT.CODE=APP.LIST.CODE_UNI AND BON_LIVRAISON!='ANNULE' order"
                + " by  name_product";

        int negatif = 1;
        if (choix.contains("R")) {
            negatif = -1;
            line = "select  date_exp,code_uni,num_lot,quantite ,REFUND_list.price,APP.PRODUCT.name_product,PRODUCT.TVA"
                    + ", original_price from APP.PRODUCT,APP.REFUND_LIST where "
                    + "  APP.REFUND_LIST.id_REFUND = " + ID_INVOICE + ""
                    + " and  APP.PRODUCT.CODE=APP.REFUND_LIST.CODE_UNI  order"
                    + " by  name_product";
        }
        LinkedList<Product> prod = getInvoiceList(l, line);
        l.CODEBAR = "+ID_INVOICE;" + ID_INVOICE + codeBar + "+DATE;" + (new Date());
        String Rtype = choix.substring(0, 1), Ttype = choix.substring(1, 2);
        RRA_PACK rp = Main.tax.getRpack4(ID_INVOICE, prod, Rtype, Ttype, Main.tax.getNowTime(l.HEURE), l.MRC);

        if (rp != null) {
            RRA_PRINT2 r = Main.tax.printRRAa4(l.EXTERNAL_DATA + l.COPY_DATA, Rtype, Ttype, rp.getProductList,
                    rp, l.id_ref);
//double [] ibiciro= new double[]{r.TOTAL,r.TOTAL_A_EX,r.TOTAL_B_18,r.TOTAL_TAX,r.TOTAL_TAX_B,0,0};
            double[] ibiciro = new double[]{r.TOTAL_A_EX, r.TOTAL_B_18, r.TOTAL_TAX_B, r.TOTAL_TAX, r.TOTAL, 0, 0};
            
            l.ID=ID_INVOICE;
            PrintVars convertInvoiceVars = convertInvoiceVars(l);

            PrintTiers convertInvoiceTiers = convertInvoiceTiers(l.umukiliya, l.TIN);

            PrintV3 printer = new PrintV3(
        this, img1, convertInvoiceTiers(l.umukiliya, l.TIN), 
        convertInvoiceVars, lotList, ibiciro,
        new String[]{"", "", "", "", "", "", "", "", "", "", "", ""}, 
        r, negatif, rraLogo);

            if (choix.contains("R")) {
                PrintRefundBook rf_book = new PrintRefundBook(this, img1,
                        convertInvoiceTiers, convertInvoiceVars, lotList, ibiciro,
                        new String[]{"", "",
                            "", "", "", "", "", "", "", "", "", ""}, r, l.raison);
            }
        }
    }

    LinkedList<Product> getInvoiceList(RRA_RECORD l, String line) {
        LinkedList<Product> prod = new LinkedList<Product>();
        lotList = new LinkedList<Lot>();
        String date1 = "";
        String code = "";
        String lot1 = "";
        int quantite = 0;
        double price = 0;
        double tva = 0;
        String desi = "";

        double totalTva = 0;
        double total = 0;

        codeBar = " +MONNAIE_INVOICE;FRW"
                + "+DATE;" + l.DATE + "+DATA;";
        try {
            outResult = s.executeQuery(line);
            while (outResult.next()) {
                date1 = outResult.getString(1);
                code = outResult.getString(2);
                lot1 = outResult.getString(3);
                quantite = outResult.getInt(4);
                price = outResult.getDouble(5);
                desi = outResult.getString(6);
                tva = outResult.getDouble(7);
                double orig = outResult.getDouble(8);
                double prix = ((quantite * price));
                double tvU = (price - (price / (1 + tva)));
                double tvT = (tvU * quantite);
                totalTva = totalTva + tvT;
                total = total + (prix);
                String puht = "" + (price - tvU);
                String tvu = "" + (tvU);
                String putt = "" + (price);
                String ptt = "" + (price * quantite);

                if (orig != price) {
                    desi += " (discount. -" + (int) (100 - Math.abs((price * 100) / orig)) + "%)";
                }
                Product pro = new Product(0, desi, code, quantite, price, tva, code, "", "", null);
                pro.originalPrice = orig;
                prod.add(pro);
                Lot lo = new Lot(code, desi, lot1, date1, "" + quantite,
                        "" + orig, "" + Main.tax.getRate(tva), putt, "" + ptt, "", "");
                codeBar = codeBar + "(" + code + ";" + lot1 + ";" + date1 + ";" + quantite + ";" + "" + (int) price + ")";

                lotList.add(lo);
                codeBar = codeBar + "(" + code + ";" + lot1 + ";" + date1 + ";" + quantite + ";" + "" + (int) price + ")";
            }
            outResult.close();
        } catch (Throwable e) {
            insertError(e + "", "getInvoiceList");
        }
        return prod;
    }

    PrintVars convertInvoiceVars(RRA_RECORD i) {
        String modeIm = "FACTURERRA";
        return new PrintVars(i.DATE, 
                "CASH : " + RRA_PRINT2.setVirgule(i.CASH,1,",")  +
                " CREDIT : " + RRA_PRINT2.setVirgule(i.CREDIT,1,","), modeIm, "FACTURE ", "" + i.ID, "FRW", i.CODEBAR,
                1);
    }
    
    PrintVars convertInvoiceVars(Invoice i) {
        String modeIm = "FACTURERRA";
        return new PrintVars(i.date, "", modeIm, "PROFORMA ", "" + i.id_invoice, "FRW", "",
                1);
    }
    

    PrintTiers convertInvoiceTiers(Client i, String tax) { 
        if (i.NUM_AFFILIATION.equals("DIVERS")) { 
            return new PrintTiers(i.NUM_AFFILIATION, "CASHNORM", i.PRENOM_CLIENT, i.SECTEUR, i.EMPLOYEUR,
                    tax);
        } 

        return new PrintTiers(i.NUM_AFFILIATION, i.NOM_CLIENT, i.PRENOM_CLIENT, i.SECTEUR, i.EMPLOYEUR,
                tax);
    }

    LinkedList<Product> InStockProduct(String s1) {
        try {
            if (s1.contains("RAMA")) {
                System.out.println(s1);
                outResult = s.executeQuery("select DISTINCT APP.LOT.id_product,name_product,code," + s1 + ", tva,code_bar,observation,code_soc_RAMA from APP.PRODUCT,APP.LOT where APP.LOT.ID_PRODUCT=APP.PRODUCT.ID_PRODUCT AND status!='TEMP' and status!='REJECTED' order by NAME_PRODUCT");

                allProduct = new LinkedList();
                //  Loop through the ResultSet and print the data
                while (outResult.next()) {
                    int productCode = outResult.getInt(1);
                    String productName = outResult.getString(2);
                    String code = outResult.getString(3);
                    double currentPrice = outResult.getDouble(4);
                    double tva = outResult.getDouble(5);
                    String codeBar = outResult.getString(6);
                    String observation = outResult.getString(7);
                    String codeSoc = outResult.getString(8);
                    Product p = new Product(productCode, productName, code, 1, currentPrice, tva, codeBar, observation, codeSoc, this);
                    // if(p.currentPrice>0)
                    allProduct.add(p);

                    // System.out.println(p);
                }
                //  Close the resultSet
                outResult.close();
            } else {
                outResult = s.executeQuery("select DISTINCT APP.LOT.id_product,name_product,code," + s1 + ", tva,code_bar,observation from APP.PRODUCT,APP.LOT WHERE APP.LOT.ID_PRODUCT=APP.PRODUCT.ID_PRODUCT AND status!='TEMP' and status!='REJECTED' order by NAME_PRODUCT");
                //  System.out.println(s1);
                allProduct = new LinkedList();
                //  Loop through the ResultSet and print the data
                while (outResult.next()) {
                    int productCode = outResult.getInt(1);
                    String productName = outResult.getString(2);
                    String code = outResult.getString(3);
                    double currentPrice = outResult.getDouble(4);
                    double tva = outResult.getDouble(5);
                    String codeBar = outResult.getString(6);
                    String observation = outResult.getString(7);
                    // String codeSoc  =  outResult.getString(8);
                    Product p = new Product(productCode, productName, code, 1, currentPrice, tva, codeBar, observation, code, this);
                    //  if(p.currentPrice>0)
                    allProduct.add(p);
                }
                //  Close the resultSet
                outResult.close();
            }

            //  Beginning of the primary catch block: uses errorPrint method
        } catch (Throwable e) {
            insertError(e + "", "doworkOther");
        }

        return allProduct;
    }

    public LinkedList GetMRC(String sql) {
        LinkedList<MRC> allmrc = new LinkedList<MRC>();
        try {

            outResult = s.executeQuery("  select  * from APP.SDC_MRC " + sql + " ORDER BY MRC");
            //System.out.println("  select  * from APP.LANGUE where variable= '"+var+"'");
            while (outResult.next()) {
                allmrc.add(new MRC(outResult.getString(1), outResult.getString(2),
                        outResult.getString(3), outResult.getString(4),
                        outResult.getString(5), outResult.getString(5), outResult.getString(6)));
            }
        } catch (Throwable e) {
            insertError(e + "", "getMrc");
        }
        return allmrc;
    }

    public LinkedList<RRA_PRODUCT> getPLU(String hera, String geza, double[] taxes, String periode) {
        LinkedList<RRA_PRODUCT> prod = new LinkedList();
        String sql = "select min (id_invoice),max (id_invoice) from APP.INVOICE where"
                + " invoice.HEURE >'" + hera + "' AND invoice.HEURE <'" + geza + "' " + periode;
        try {
            int min_id_invoice = 0;
            int max_id_invoice = 0;
            System.err.println(sql);
            outResult = s.executeQuery(sql);
            while (outResult.next()) {
                min_id_invoice = outResult.getInt(1) - 1;
                max_id_invoice = outResult.getInt(2) + 1;
            }

            sql = "select  code_uni,SUM(quantite) as quantite,list.tva,price,name_product from APP.LIST,APP.PRODUCT  "
                    + " WHERE ID_INVOICE>" + min_id_invoice + " and  ID_INVOICE<" + max_id_invoice + " "
                    + " and code_uni=code GROUP BY  name_product,CODE_UNI,PRICE,list.tva order by name_product ";

            outResult = s.executeQuery(sql);
            RRA_PRODUCT rp = null;
            while (outResult.next()) {

                String cat = "A";
                double tva = outResult.getDouble(3);
                System.out.println(tva + ":FECKE:" + taxes[1]);
                if (tva == taxes[0]) {
                    cat = "A-EX";
                } else if (tva == taxes[1]) {
                    cat = "B";
                } else if (tva == taxes[2]) {
                    cat = "C";
                } else if (tva == taxes[3]) {
                    cat = "D";
                }
                rp = new RRA_PRODUCT(outResult.getString(1), outResult.getString(5), outResult.getDouble(4),
                        tva, outResult.getInt(2), cat, 0);
                prod.add(rp);

            }
            for (int i = 0; i < prod.size(); i++) {

                RRA_PRODUCT rpp = prod.get(i);
                sql = "select SUM(qty_live)   from APP.LOT  "
                        + " WHERE   code_lot='" + rpp.productCode + "'   ";

                outResult = s.executeQuery(sql);
                while (outResult.next()) {
                    prod.get(i).onStock = outResult.getInt(1);
                }
            }
        } catch (SQLException e) {
            System.err.println(sql);
            insertError("PLU", " " + e);
        }
        return prod;
    }

    public int count(String table, String col, String contain, String periode) {
        String sql = "select  count(" + col + ") from APP." + table + " where "
                + " " + contain + " and " + periode;
        try {
            outResult = s.executeQuery(sql);
            while (outResult.next()) {
                return outResult.getInt(1);
            }
        } catch (SQLException e) {
            System.err.println(sql);
            insertError("count", " " + e);
        }
        return 0;
    }

    public double sum(String table, String col, String contain, String periode) {
        String sql = "select  sum(" + col + ") from APP." + table + " where "
                + " " + contain + " and " + periode;
        System.out.println(sql);
        try {
            outResult = s.executeQuery(sql);
            while (outResult.next()) {
                return outResult.getDouble(1);
            }
        } catch (SQLException e) {
            System.err.println(sql);
            insertError("sum", " " + e);
        }
        return 0;
    }

    public LinkedList<String> lesClients() {
        LinkedList<String> clien = new LinkedList();
        try {
            outResult = s.executeQuery("select NOM_VARIABLE from APP.VARIABLES WHERE FAMILLE_VARIABLE='CLIENT' order by NOM_VARIABLE ");

            while (outResult.next()) {
                clien.add(outResult.getString(1));
            }
    //  Close the resultSet
            outResult.close();

        } catch (Throwable e) {

            insertError(e + "", " V getlot");
        }

        return clien;

    }

    static String startTime() {

        Date toDay = new Date();

        String year = "" + (2000 + (toDay.getYear() - 100));//garantie de un millenaire, a changer muri 3000

        String mounth;

        if ((toDay.getMonth() + 1) < 10) {
            mounth = "0" + (toDay.getMonth() + 1);
        } else {
            mounth = "" + (toDay.getMonth() + 1);
        }

        return year + "-" + mounth + "-01" + " 00:00:00";

    }

    static String EndTime() {

        Date toDay = new Date();

        int y = (2000 + (toDay.getYear() - 100));//garantie de un millenaire, a changer muri 3000

        String year = "" + y;
        String mounth;
        String day;

        if ((toDay.getMonth() + 1) < 10) {
            mounth = "0" + (toDay.getMonth() + 1);
        } else {
            mounth = "" + (toDay.getMonth() + 1);
        }
        day = "" + getMois(mounth, y);

        return year + "-" + mounth + "-" + day + " 23:59:59";

    }

    static String getMois(String s1, int year) {

        String m = "";

        if (s1.equals("01")) {
            m = "31";
        }

        if (s1.equals("03")) {
            m = "31";
        }

        if (s1.equals("04")) {
            m = "30";
        }

        if (s1.equals("05")) {
            m = "31";
        }

        if (s1.equals("06")) {
            m = "30";
        }

        if (s1.equals("07")) {
            m = "31";
        }

        if (s1.equals("08")) {
            m = "31";
        }

        if (s1.equals("09")) {
            m = "30";
        }

        if (s1.equals("10")) {
            m = "31";
        }

        if (s1.equals("11")) {
            m = "30";
        }

        if (s1.equals("12")) {
            m = "31";
        }

        if (s1.equals("02")) {

            if (year % 4 == 0) {
                m = "29";
            } else {
                m = "28";
            }

        }
        return m;

    }

    public static String kohereza(String url, String valuetosend) throws Exception {

        URL siteUrl = new URL(url);
        HttpURLConnection connection = (HttpURLConnection) siteUrl.openConnection();
        connection.setRequestMethod("POST");
        connection.setDoOutput(true);
        connection.setDoInput(true);
        DataOutputStream out = new DataOutputStream(connection.getOutputStream());
        System.out.println(" URL  " + url);
        System.out.println(" START  " + new Date());
        out.writeBytes(valuetosend);
        out.flush();
        System.out.println("FLUSH  " + new Date());
        out.close();
        // Thread.sleep(1000);
        BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        //Thread.sleep(1000);
        String responsesrver = in.readLine();
        String line;
        String line2 = "";
        while ((line = in.readLine()) != null) {
            line2 += "   " + (in.readLine() + '\n');
            System.out.println(line);
        }
        System.out.println(responsesrver + " " + new Date() + " respo  " + line2);
        in.close();
        return responsesrver;
    }

    public String getNameEmploye(int id, int carte) {
        try {
            String sql = "select NOM_EMPLOYE from APP.EMPLOYE WHERE ID_EMPLOYE=" + id + " AND CATRE_INDETITE=" + carte;
            System.out.println(sql);
            outResult = s.executeQuery(sql);

            while (outResult.next()) {
                return outResult.getString(1);
            }
            outResult.close();
        } catch (Throwable e) {
            System.out.println("getNameEmploye" + e);
        }
        return "ACCESS DENIED";
    }

    public String getStatus(String table, String list) {
        String newStatus = "";
 //GETSTATUS#PROFORMA#1#1;Admin;2015-09-01 05:57:10;AOS123000>>2#2;Admin;2015-09-01 05:57:10;AOS123000>>
        //3#3;Admin;2015-09-01 05:57:10;AOS123000>>4#4;Admin;2015-09-01 05:57:10;AOS123000>>5#5;Admin;2015-09-01 05:57:10;AOS123000>>  

        String[] listStatus = list.split(">>");

        for (int i = 0; i < listStatus.length; i++) {
            String[] ext = listStatus[i].split("#");
            try {
                String sql = "select STATUS from APP." + table + " WHERE EXT_KEY='" + ext[1] + "'";
                System.out.println(sql);
                outResult = s.executeQuery(sql);
                while (outResult.next()) {
                    newStatus += ext[0] + ";" + outResult.getString(1) + ">>";
                }
                outResult.close();
            } catch (Throwable e) {
                System.out.println("getNameEmploye" + e);
            }
        }
        System.out.println("newStatus   " + newStatus);
        return newStatus;
    }

    public String getStringBlob(Blob Sent_track, boolean vect) {
        if (vect) {
            lines = new Vector<String>();
        }
        String allsent = "";

        try {
            InputStream sentStream = Sent_track.getBinaryStream();
            BufferedReader in = new BufferedReader(new InputStreamReader(sentStream));
            String sent;
            while ((sent = in.readLine()) != null) {
                allsent += sent + "\n";
                if (vect) {
                    lines.add(sent);
                }
            }

            sentStream.close();
        } catch (Exception e) {
            insertError(e + "", " getStringBlob ");
            return "";
        }

        return allsent;

    }

    public SMART_TRACKING getSmartTrack(int external_id, String global_id) {
        try {
            lines=new Vector<String>();
            outResult = s.executeQuery("select * from APP.SMART_TRACKING_COMM where  "
                    + " EXTERNAL_ID=" + external_id + " and  GLOBAL_ID='" + global_id + "'");
            System.out.println("select * from APP.SMART_TRACKING_COMM where  "
                    + " EXTERNAL_ID=" + external_id + " and  GLOBAL_ID='" + global_id + "'");
            while (outResult.next()) {
                SMART_TRACKING s = new SMART_TRACKING(outResult.getInt("ID_TRACKING"), outResult.getString("HEURE_TRACKING"),
                        getStringBlob(outResult.getBlob("SENT_TRACKING"), true), getStringBlob(outResult.getBlob("RECEIVED_TRACKING"), false),
                        outResult.getString("KEY_INVOICE"), outResult.getString("EMPLOYE"), outResult.getString("GLOBAL_ID"),
                        outResult.getString("NUM_AFFILIATION"), external_id);

                return s;
            }
            outResult.close();
        } catch (Throwable e) {
//      Catch all exceptions and pass them to the exception reporting method             /
            System.out.println(" . .invexception thrown:");
            insertError(e + "", " V getSmartTrack ");
        }
        return null;
    }

    public SMART_MATRIX getInsuranceScheme(String REASON, SmartPatient card,Statement state, Connection conn) {
        System.out.println("select * from APP.SMART_MATRIX where  "
                + " REASON='" + REASON + "'  ");

try {
    outResult = s.executeQuery("select * from APP.SMART_MATRIX where  "
            + " REASON='" + REASON + "'  ");
    while (outResult.next()) {
        return new SMART_MATRIX(outResult.getInt("MAXIMUM_ITEM_INVOICE"), outResult.getString("ID_INSURANCE"),
                    outResult.getString("DES_INSURANCE"), outResult.getString("SCHEME_INSURANCE"),
                outResult.getString("FINGER_INSURANCE"), outResult.getString("TARIF_INSURANCE"),
                outResult.getString("REASON"), outResult.getDouble("MAXIMUM_AMOUNT_INVOICE"));
    }
    outResult.close();
    smart.MySQLConnector.insertErrorMySQL2(card,REASON+"-REASON is missing  in Smart",  state,   conn);
    int n = JOptionPane.showConfirmDialog(null,
                                    " REASON "+REASON+" NOT FOUND IN SMART MATRIX " 
                                    + "\n DO YOU WANT TO PROCEED MANUALLY ??? ",
                                    " " + REASON  , JOptionPane.YES_NO_OPTION);
            
                                    if (n == 0) {
   
                                        
 LinkedList < String > les_ID_INSURANCE=new LinkedList<String>(); 
 outResult = s.executeQuery("select DISTINCT ID_INSURANCE from APP.SMART_MATRIX   ");
 while (outResult.next()) { les_ID_INSURANCE.add(outResult.getString(1)); }                                       
 LinkedList < String > les_DES_INSURANCE=new LinkedList<String>(); 
 outResult = s.executeQuery("select DISTINCT DES_INSURANCE from APP.SMART_MATRIX   ");
 while (outResult.next()) { les_DES_INSURANCE.add(outResult.getString(1)); } 
 LinkedList < String > les_SCHEME_INSURANCE=new LinkedList<String>(); 
 outResult = s.executeQuery("select DISTINCT SCHEME_INSURANCE from APP.SMART_MATRIX   ");
 while (outResult.next()) { les_SCHEME_INSURANCE.add(outResult.getString(1)); } 
 
 String ID_INSURANCE     =(String) JOptionPane.showInputDialog(null, "SELECT", "ID_INSURANCE",
                           JOptionPane.QUESTION_MESSAGE, null, les_ID_INSURANCE.toArray(), l(lang, "V_MUSHYA"));  ;                                     
 String DES_INSURANCE    =(String) JOptionPane.showInputDialog(null, "SELECT", "DES_INSURANCE",
                           JOptionPane.QUESTION_MESSAGE, null, les_DES_INSURANCE.toArray(), l(lang, "V_MUSHYA"));                                          
 String SCHEME_INSURANCE =(String) JOptionPane.showInputDialog(null, "SELECT", "SCHEME_INSURANCE",
                           JOptionPane.QUESTION_MESSAGE, null, les_SCHEME_INSURANCE.toArray(), l(lang, "V_MUSHYA"));      
   
  if (ID_INSURANCE !=null && DES_INSURANCE  !=null && SCHEME_INSURANCE !=null) {
      return new  SMART_MATRIX(50, ID_INSURANCE,DES_INSURANCE, SCHEME_INSURANCE,"Yes", " ",REASON,  100000);
  }       

        }
        else
        {  return null; } 
            
        } catch (Throwable e) {
//      Catch all exceptions and pass them to the exception reporting method             /
            System.out.println(" . .invexception thrown:");
            insertError(e + "", " V inv ");
        }
        
  
        
        return null;
    }

    boolean insertTrackSmart(SMART_TRACKING rt) {
        try {
            PreparedStatement psInsert = conn.prepareStatement("insert into APP.SMART_TRACKING_COMM"
                    + " (SENT_TRACKING,RECEIVED_TRACKING ,KEY_INVOICE,EMPLOYE,GLOBAL_ID,NUM_AFFILIATION,EXTERNAL_ID)  values (?,?,?,?,?,?,?)");

            byte[] buff = rt.RECEIVED_TRACKING.getBytes();
            Blob rblob = new SerialBlob(buff);

            byte[] sbuff;
            sbuff = rt.SENT_TRACKING.getBytes();
            Blob sblob = new SerialBlob(sbuff);

            psInsert.setBlob(1, sblob);
            psInsert.setBlob(2, rblob);
            psInsert.setString(3, rt.KEY_INVOICE);
            psInsert.setString(4, rt.EMPLOYE);
            psInsert.setString(5, rt.GLOBAL_ID);
            psInsert.setString(6, rt.NUM_AFFILIATION);
            psInsert.setInt(7, rt.EXTERNAL_ID);
            psInsert.executeUpdate();
            psInsert.close();
            return true;
        } catch (Throwable e) {
            writeError(e + " KEY " + rt.KEY_INVOICE, "insertTrackRRA FAIL");
            //insertError(e+"","insertTrackRRA");
        }
        return false;
    }

    static void updateSmartTracking2(String colonne, String global_id,
            String condition, Statement stat) {
        try {
            String sql = (" update APP.SMART_TRACKING_COMM set " + colonne + " where GLOBAL_ID='" + global_id + "' "
                    + condition);
            System.out.println(sql);
            stat.execute(sql);

        } catch (Throwable e) {
            System.err.println(e + " updateSmart");
        }
    }

    void updateSmartBLOB2(String value, String global_id, String condition) {
        try {
            byte[] sbuff;
            sbuff = value.getBytes();
            Blob sblob = new SerialBlob(sbuff);
            PreparedStatement pstmt = conn.prepareStatement(" update APP.SMART_TRACKING_COMM set SENT_TRACKING=? where GLOBAL_ID='" + global_id + "' " + condition);

            String sql = (" update APP.SMART_TRACKING_COMM set SENT_TRACKING=? where GLOBAL_ID='" + global_id + "' " + condition);
            System.out.println("" + value);
            pstmt.setBlob(1, sblob);
            pstmt.executeUpdate();
            pstmt.close();

        } catch (Throwable e) {
            insertError(e + "", "updateSmart");
        }
    }

    public static LinkedList<Lot> getStock(Statement state) {
        LinkedList<Lot> stock = new LinkedList();
        try {

            String sql1 = ("select app.lot.id_product, code_lot,id_LotS,date_exp,qty_start,"
                    + "qty_live,lot.bon_livraison,bon_livraison.NUM_CLIENT,id_lot,"
                    + "lot.revient from APP.LOT ,bon_livraison where"
                    + "   bon_livraison.bon_livraison=lot.bon_livraison and app.lot.code_lot<>'-1'");

            System.out.println(sql1);
            ResultSet outResult = state.executeQuery(sql1);

            while (outResult.next()) {
                int productCode = outResult.getInt(1);
                String name = outResult.getString(2);
                String id_LotS = outResult.getString(3);
                String dateExp = outResult.getString(4);
                int qtyStart = outResult.getInt(5);
                int qtyLive = outResult.getInt(6);
                String bon_livraison = outResult.getString(7);
                String code = outResult.getString(8);
                int dateDoc = outResult.getInt(9);
                double pr = outResult.getDouble(10);
                double prb = 0;//outResult.getDouble(11);
                stock.add(new Lot("" + productCode, "" + name, "" + code, "" + id_LotS, "" + dateExp, "" + dateDoc, "" + qtyStart, "" + qtyLive, "" + bon_livraison, "" + pr, "" + prb));
            }
            //  Close the resultSet 
            outResult.close();

            String sql2 = ("select app.lot.id_product,code_lot,id_LotS,date_exp,qty_start,"
                    + "qty_live,lot.bon_livraison,bon_mvt_in.NUM_CLIENT,id_lot,"
                    + "LOT.revient,'' from APP.LOT  ,bon_mvt_in where  bon_mvt_in.bon_mvt_in=lot.bon_livraison "
                    + "and app.lot.code_lot<>'-1'");

            outResult = state.executeQuery(sql2);
            System.out.println(sql2);

            while (outResult.next()) {
                int productCode = outResult.getInt(1);
                String name = outResult.getString(2);
                String id_LotS = outResult.getString(3);
                String dateExp = outResult.getString(4);
                int qtyStart = outResult.getInt(5);
                int qtyLive = outResult.getInt(6);
                String bon_livraison = outResult.getString(7);
                String code = outResult.getString(8);
                int dateDoc = outResult.getInt(9);
                double pr = outResult.getDouble(10);
                double prb = 0;//outResult.getDouble(11);
                String frss = outResult.getString(11);

                Lot l = (new Lot("" + productCode, "" + name, "" + code, "" + id_LotS, "" + dateExp, "" + dateDoc, "" + qtyStart, "" + qtyLive, "" + bon_livraison, "" + pr, "" + prb));

                l.frss = frss;
                stock.add(l);
            }

        } catch (Throwable e) {

            System.out.println(" . .lot . exception thrown:" + e);
            //writeError(e+" KEY "+(new Date())+" V lot"+e,"insertTrackRRA FAIL");

        }

        return stock;
    }
    public LinkedList allActesConsultations(String sConsu) {
    LinkedList<Actes> allact = new LinkedList(); 
    try {
           
        outResult = s.executeQuery("select id_actes,code_actes,name_actes,prix, tva,fam_category,observation "
                + " from CLINIC_ACTES where fam_category='" + sConsu + "' order by NAME_ACTES");
        System.out.println("select id_actes,code_actes,name_actes,prix, tva,fam_category,observation "
                + " from CLINIC_ACTES where fam_category='" + sConsu + "' order by NAME_ACTES");

        //  Loop through the ResultSet and print the data
        while (outResult.next()) {
            int productCode = outResult.getInt(1);
            String code = outResult.getString(2);
            String productName = outResult.getString(3);
            double currentPrice = outResult.getDouble(4);
            double tva = outResult.getDouble(5);
            String famcat = outResult.getString(6);
            String observation = outResult.getString(7); 
            Actes p = new Actes(code, productName, tva, currentPrice, famcat, observation); 
            p.ID_ACTES = productCode;

            if (p.PRIX > 0) 
            {
                allact.add(p);
}
        }
        //  Close the resultSet
        outResult.close();

        //  Beginning of the primary catch block: uses errorPrint method
    } catch (Throwable e) {
        insertError(e + "", "doworkOther");
    }
    return allact;
}
    public int  getIdtable(String col,String table,String keyInvoice)
  {
      
      try
      {
          outResult = s.executeQuery("select  "+col+" from APP."+table+" WHERE KEY_INVOICE='"+keyInvoice+"'");
          while (outResult.next())
          {
            return(outResult.getInt(1));
          }
          
          outResult.close();
      }
      catch (Throwable e)       
      {
        insertError(e+"","getIdfacture");
      }
     return -1;
      
  }
    
   public LinkedList<Visite> getAllVisiteOpen(String NUM_AFFILIATION)
    { 
        LinkedList<Visite> visite = new LinkedList();
        Visite v=null; 

        try {
            {
                outResult = s.executeQuery("select * from APP.CLINIC_VISITE WHERE "
                        + "NUM_AFFILIATION'="+NUM_AFFILIATION+"' AND ETAT_ACTUEL='OPEN'   ORDER BY ID_VISITE");
                while (outResult.next()) 
                {
                    int id_visite = outResult.getInt(1);
                    String date=outResult.getString(2);
                    String NUM_PATIENT = outResult.getString(3); 
                    String ASSURANCE=outResult.getString(5);
                    String HEURE_IN = outResult.getString(6);
                    String HEURE_OUT = outResult.getString(7); 
                    String ETAT_ACT = outResult.getString(8);
                    String ETAT_SORT = outResult.getString(9);
                    String NUMERO_QUITANCE = outResult.getString(10); 
                    String UMURWAZA = outResult.getString(11); 
                    String TYPE_VISITE = outResult.getString(12);
                    String EMPLOYE = outResult.getString(13); 
                    String VUVUZELA = outResult.getString(14);
                    int TOTAL=outResult.getInt(15);
                    int DEPO=outResult.getInt(16);
                    
                    v=new Visite(NUM_PATIENT, NUM_AFFILIATION, ETAT_ACT, ETAT_SORT,NUMERO_QUITANCE, UMURWAZA, TYPE_VISITE, EMPLOYE, VUVUZELA, ASSURANCE);
                    v.ID_VISITE=id_visite; 
                    v.POURCENTAGE=outResult.getInt("POURCENTAGE");
                    v.NUMERO_BON=outResult.getString("NUMERO_QUITANCE");
                    visite.add(v);
                }
            }
            outResult.close();
        } catch (Throwable e) {
            insertError(e + "", "getclintV");
        }

        return visite;
    }

}
