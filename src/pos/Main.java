
package pos;

/*
Interface Ishyiga copyright 2007 Kimenyi Aimable Gestion des ventes et du stock
 */  

import clinic.Interface_Patient;
import com.toedter.calendar.JCalendar;
import gnu.io.CommPortIdentifier;
import gnu.io.PortInUseException;
import gnu.io.SerialPort;
import gnu.io.UnsupportedCommOperationException;
import java.awt.*;
import java.awt.event.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.*;
import java.sql.Connection;
import java.sql.Statement;
import java.util.*;  
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*; 
import rra.RRA_PACK;
import rra.RRA_PRINT2;
import smart.*;

public class Main extends JFrame {

    static Enumeration portList;
    static Font police;
    static CommPortIdentifier portId;
    static String messageString = " ";
    static SerialPort serialPort;
    static OutputStream outputStream;
    static boolean outputBufferEmptyFlag = false;
    static String defaultPort;
    static String special;
    private static boolean Display;
    static public String stock;
    private String nom,  var = "PRIX", sqlFamille = "";
      private int v;
    final int maxCode;
    final int LENTH = 1000;
    final int WIDT = 1150;
    private JLabel labelProduit = new JLabel(" ITEMS ");
    public double TOTALPRICE2,TVAPRICE2;
    private int k = 1;
    private JButton sendButton,removeButton,addButton, addButtonZOOM, getStockButtonZOOM, getStockButton,
            askButton,cashButton, creditButton, scanButton,stockButton, proformaButton,smartButton;
    private JButton venteButton, autreTarif,actButton ,annuleButton,repriceButtton,aboutButton,
            t1, t2, t3, t4, t5, t6, t7, t8, t9, t10,newClient, optionsButton, newTest,
            unVedette,deuxVedette,troisVedette,quatreVedette, cinqVedette, sixVedette, septVedette, huitVedette
            , b1, b2, b3, b4, b5, b6, b7, b8, b9, b10, b11, b12;
    String currentScan = "";
    String IJISHO = "FACTURE";
    String ss = "";
    int comp = 0; 
    private int i1, i2, i3, i4, i5, i6, i7, i8, i9, i10, i11, i12;
    private static JFrame frame;							   // cette fenetre
    private JList allProductList = new JList();        // un JList qui affiche tous tout les produits
    private JList allProductListZOOM = new JList();        // un JList qui affiche tous tout les produits
    private JList saleJList = new JList();             // un JList qui affiche juste vente en cours
    private JList assList = new JList();
    private JTextField texttoBESent = new JTextField();// la ou on ecrie les produits et leurs quantites
    private JTextField search = new JTextField();
    private JTextField searchZOOM = new JTextField();
    private JTextField searchPatient = new JTextField();
    private JTextArea total = new JTextArea();        // Gere le prix Total
    JTextArea textArea = new JTextArea();
    private JScrollPane scrollPaneProductList;     	// si il ya trop des produits qui depasse le Jpanel
    private JScrollPane scrollPaneallSale;   			// si il ya trop des vente qui depasse le cadre
    private JPanel salePane;                      	//les panels qui composent la fenetre
    private JPanel southPane;
    private JPanel pricePane;
    private JPanel centerPane;
    static public Cashier cashier; 							// Caisse courant
    private Properties properties = new Properties();
    private int today;
    private int virgule = 1;
    int voirClient = 0;
    Client currentClient = null;
    String serv, dataBase;
    public static Employe umukozi;
    Proforma pro;
    Tarif[] tarif;
    String color = "noColor";
    static String dev;
    public static String lang = "FRA";
    public static String SDCLANG="FRA";
    String IMPR;
    String nameProf = "", refPro = "";
    
    String live_Tarif = "CASHNORM";
    
    public static String observe_Tarif= "NO";    
    public static String allow_smart  = "NO";
    
    String ordonnancez = "";
//    String   keyInvoice="";
//    int id_proforma;
    double ifashwePrice, previent, amountReduit, amountPerProduct, totReduction, ancienTotal;
    double reduction;
    JRadioButton radioSpecial = new JRadioButton("SPECIAL");
    JRadioButton PRESCRIPTION = new JRadioButton("PRESCRIPTION");
    //////VARIABLE EASTcLINIC///
    String st="" ;
    String list="";
    boolean isClinic = false; 
    private javax.swing.JLabel PATIENT_FILE;
    private javax.swing.JLabel LESCLIENTS;
    private JButton ADD_DATA, EDIT_DATA;
    private JList lesClients = new JList();
    private JList lesFactures = new JList();
    private JCalendar dateIn ; 
    private JCalendar dateOut ;
    private JButton checkFactures = new JButton(" VISUALISE ");
    double amount1,amount2,amount3,amount4;
    static public Taxes tax=null;
    
    Pane2 panel = new  Pane2();
    String TIME="";
    Main main;
    boolean vedetteImage=false; 
    LinkedList <Proforma> toTransProforma2;
    LinkedList <String>    toTransProformaAbakozi;
    
    String selected=""; 
     String depotSql="";
     Display_Client dc;
     boolean sendDisplay=false;
     static public final double ARRONDI=0.00500000000;
     static public String versionIshyiga="5.1.0";
     
     static boolean isSpecial=false;
     
    /******************************************************************0.00050000000;**************
    le constructeur qui cree un frame
     ********************************************************************************/
  public  Main(Employe umukozi, Cashier c, String serv, String dataBase, String color) {
      System.out.println("umukozi2");
      
      this.color = color;
        this.dataBase = dataBase;
        this.serv = serv;
        frame = this;
        v = 0;
        stock = "CASHNORM";
        this.umukozi = umukozi;
        
        System.out.println("umukozi.BP_EMPLOYE   "+umukozi.BP_EMPLOYE);
         
    if(umukozi.BP_EMPLOYE!=null && umukozi.BP_EMPLOYE.contains("#"))
            { depotSql=" AND ID_LOTS  like '%" +umukozi.BP_EMPLOYE + "%'" ; } 
        this.lang = umukozi.LANGUE;        
        nom = umukozi.NOM_EMPLOYE;
        cashier = c;
        cashier.db.lang = Main.lang;
        cashier.db.nom_empl = umukozi.NOM_EMPLOYE;
        maxCode = cashier.allProduct.size();
        System.err.println("umukozi1");
        Main.SDCLANG=cashier.db.getString("sdclang");
        String setIm=cashier.db.getString("vedetteImage");
        
        final SwingWorker<String , String> worker = new SwingWorker<String, String>() 
{
@Override protected String doInBackground() throws Exception {  
      CommCloud smartMatrix = new CommCloud(cashier.db.conn);
 return  " "; 
}
@Override protected void done() { } 
@Override  protected void process(java.util.List<String> chunks) {  }   };
             worker.execute();  
             worker.addPropertyChangeListener(new PropertyChangeListener() {
@Override public void propertyChange(PropertyChangeEvent evt) {
if (evt.getPropertyName().equals("progress")) {  } } });   

        
        System.out.println(setIm+" umukozi1 "+Boolean.parseBoolean(setIm));
        if(dev==null)
        {
            dev="RWF";
        } 
        
        dateIn = new JCalendar(); 
        dateIn.putClientProperty("JCalendar.footerStyle", "Today");
        dateOut = new JCalendar();
        if(isClinic)
            IJISHO="CLINIC";
//        tax=new Taxes(serv,serv, dataBase);
//        tax.rraClient.isRRA=isRRA1;
        
//        writeLot(); 
        
        showProduct();
        tarif = cashier.db.getTarif();
        System.out.println("*******************************tetetetete");
        draw(); 
        
        //setVedette(1); 
        cashier.basketStart(stock); 
        if(tax.rraClient.isRRA == false && c.db.sdcDataExist())
{
            
String pwd=JOptionPane.showInputDialog( "SDC  "+l(lang, "V_NOT")+"  "+l(lang, "V_SUPPORTED")+" PWD");
if(pwd!=null && pwd.equals("MACINYA"))
{
    
}
else
{
 System.exit(0);   
}



}
        String datedoc = cashier.date;
        String d = "" + datedoc.substring(4) + "" + datedoc.substring(2, 4) + "" + datedoc.substring(0, 2);
        Properties prop = System.getProperties();

        try {
            Integer hhh = new Integer(d);
            today = hhh.intValue();

            JOptionPane.showMessageDialog(null, "        " + l(lang, "V_HEUREDEPART") + " : " + new Date().toLocaleString() + "\n                " + l(lang, "V_CAISSEMONTANT") + "  : " + Cashier.yose2 + " " + cashier.db.devise + "\n                                " + l(lang, "V_BONTRAVAIL") + "\n                             ISHYIGA V5.0.2 !!!!!!", "   " + l(lang, "V_BIENVENUE") + " " + nom + " !!!!! ", JOptionPane.PLAIN_MESSAGE);
//JOptionPane.showMessageDialog(null, "    " + prop.getProperty("java.class.path", null) + "\n          Caisse Yawe irimo Amafaranga : " + Cashier.yose + " RWF\n                              Akazi Keza !!", " Murakazaneza " + nom, JOptionPane.PLAIN_MESSAGE);
        } catch (Throwable ex) {
            JOptionPane.showMessageDialog(null, l(lang, "V_TODAY") + "  " + today, "  " + l(lang, "V_DATE"), JOptionPane.PLAIN_MESSAGE);
        }
        if (Display) {
            boolean portFound = false;


            portList = CommPortIdentifier.getPortIdentifiers();
//System.out.println(portList.hasMoreElements());
            while (portList.hasMoreElements()) {
                portId = (CommPortIdentifier) portList.nextElement();
                if (portId.getPortType() == CommPortIdentifier.PORT_SERIAL) {

                    if (portId.getName().equals(defaultPort)) {
                        System.out.println("Found port " + defaultPort);

                        portFound = true;
                        try {
                            serialPort =
                                    (SerialPort) portId.open("PriceDisplay", 2000);

                        } catch (PortInUseException e) {
                            System.out.println("Port in use.");
                            JOptionPane.showMessageDialog(null, " IO " + e, l(lang, "V_ERROR"), JOptionPane.PLAIN_MESSAGE);
                            continue;
                        }
                        try {
                            outputStream = serialPort.getOutputStream();
                        } catch (IOException e) {
                            JOptionPane.showMessageDialog(null, " IO " + e, l(lang, "V_ERROR"), JOptionPane.PLAIN_MESSAGE);
                        }
                        try {
                            serialPort.setSerialPortParams(9600,
                                    SerialPort.DATABITS_8,
                                    SerialPort.STOPBITS_1,
                                    SerialPort.PARITY_NONE);
                        } catch (UnsupportedCommOperationException e) {
                        }
                        try {
                            serialPort.notifyOnOutputEmpty(true);
                        } catch (Exception e) {
                            JOptionPane.showMessageDialog(null, " exc " + e, " Error setting event notification ", JOptionPane.PLAIN_MESSAGE);
                            System.out.println("Error setting event notification");
                            System.out.println(e.toString());
//                            System.exit(-1);
                        }
                    }
                }
            }

            if (!portFound) {
                System.out.println("port " + defaultPort + " not found.");
                JOptionPane.showMessageDialog(null, "port " + defaultPort + " not found.", "", JOptionPane.PLAIN_MESSAGE);
            }
            try {
                Ikaze();
            } catch (Exception ex) {
                cashier.db.insertError("main ", ""+ex);
            }
        }
        
        



        
    }

    public void Ikaze() {
        try {
            outputStream.write(26);
            outputStream.write((getString("nom")).getBytes());
            //outputStream.write(("IKAZE MU ISHYIGA "+ getString("t1")).getBytes()); 
            outputStream.write(10);
            outputStream.write(13);
            String cash = " " + stock;
            if (cash.length() < 20) {
                for (int i = cash.length(); i < 19; i++) {
                    cash = cash + " ";
                }
            }
            outputStream.write((cash + " ").getBytes());
        } catch (IOException ex) {
            cashier.db.insertError("Ikaze ", ""+ex);
        }
    }

    String getString(String s) {
        String ret = " ";
        //System.out.println("stateMysql value:"+stateMysql);
        Variable var = cashier.db.getParam("RUN", "MAIN", s + "MAIN");

        if (var != null) {
            ret = var.value2;
        }
        return ret;
    }

    String getString2(String s) {
        String ret = " ";
        //System.out.println("stateMysql value:"+stateMysql);
        Variable var = cashier.db.getParam("RUN", "MAIN", s + "MAIN");

        if (var != null) {
            ret = var.value;
        }
        return ret;
    }

// on dessine les composants
    public void draw() {
        police = new Font(cashier.db.getString("font"), Integer.parseInt(cashier.db.getString("style")), Integer.parseInt(cashier.db.getString("length")));

        Container content = getContentPane();
        if (isClinic) {
            content.add(eastPaneClinic(390), BorderLayout.EAST);
            content.add(eastPane(380), BorderLayout.CENTER);
            content.add(westPane(350), BorderLayout.WEST);
            ADD_DATA();
        } else {
            content.add(eastPane(370), BorderLayout.EAST);
            content.add(centerPane(70), BorderLayout.CENTER);
            content.add(westPane(500), BorderLayout.WEST);            
            b1();
            b2();
            b3();
            b4();
            b5();
            b6();
            b7();
            b8();
            b9();
            b10();
            b11();
            b12();
            setVedette(1);
        }

        allProductListEcoute(); 
        assListEcoute();
        annule();
        cash();
     smart();
        Credit();       // Prendre l argent
        sendProduct();  // Ajout manuel
        addProduct();   // Ajout preselectionne
        addProductZOOM();
        removeProduct();// enlever un produit
        stockManager();
        un();
        deux();
        trois();
        quatre();
        cinq();
        six();
        sept();
        huit();
        lesClients();
        ecoute();
        superSearchAff();
        actualise();
        retour();
       // search();
        t1();
        t2();
        t3();
        t4();
        t5();
        t6();
        t7();
        t8();
        t9();
        t10();

        getStock();
        getStockZOOM();
        optionsButton();
        aboutIshyiga();
        //Test();
        reprice();
        newClient();
        scancash();
        scanGros();
        scanFrame();
        scanAdd();
        scanOne();
        scanRemove();
        scanCredit();
        superSearchIt();
        Proforma();
        searchZOOMIT();
        setTitle(nom + "'s Ishyiga  " + l(lang, "V_SESSION") + "  + " + l(lang, "V_TARIF") + stock + " +MODE: " + IJISHO);

        enableEvents(WindowEvent.WINDOW_CLOSING);
        Image im = Toolkit.getDefaultToolkit().getImage("logo.png");
        this.setIconImage(im);
        enableEvents(WindowEvent.WINDOW_CLOSING);
        //on fixe  les composant del'interface
        setSize(WIDT, LENTH);
        setVisible(true);

    }
// AFFICHE MAJ le stock

    public void showProduct() {
        
        allProductList.setListData(cashier.allProduct.toArray()); 
        String table = "";
        if (stock.equals("RAMA")) {
            table = "_RAMA";

        }
        if (isClinic) {
            cashier.allClient = cashier.db.getClientV(table, stock);
            lesClients.setListData(cashier.allClient.toArray()); 
        } 
    }

    public JPanel eastPaneClinic(int w) {

        JPanel CommonWestPane = new JPanel();
        CommonWestPane.setPreferredSize(new Dimension(w, 700));
        //CommonWestPane.setLayout(new GridLayout(2,1)); 

        JPanel clientPane = new JPanel();
        clientPane.setPreferredSize(new Dimension(w, 550));
//       clientPane.setLayout(new GridLayout(3,1)); 

        PATIENT_FILE = new javax.swing.JLabel();
        LESCLIENTS = new javax.swing.JLabel();
        JLabel shaka = new javax.swing.JLabel();
        
        ADD_DATA = new JButton("ADD");
        ADD_DATA.setFont(police);
        EDIT_DATA = new JButton("EDIT");
        EDIT_DATA.setFont(police);
        EDIT_DATA.setBackground(new Color(255, 255, 205));

        Font policea = new Font("GungsuhChe", Font.BOLD, 26);
        PATIENT_FILE.setFont(policea); // NOI18N
        PATIENT_FILE.setText(" PATIENT FILE");

        policea = new Font("GungsuhChe", Font.BOLD, 18);
        LESCLIENTS.setFont(policea); // NOI18N
        LESCLIENTS.setText(" PATIENT ");

        // shaka.setFont(policea); // NOI18N
        shaka.setText(" SEARCH : ");
        searchPatient.setPreferredSize(new Dimension(140, 30));
        searchPatient.setFont(police);
//         f;
        JScrollPane scrollPaneClientList = new JScrollPane(lesClients);
        scrollPaneClientList.setPreferredSize(new Dimension(w - 50, 400));
        lesClients.setBackground(Color.PINK);

        JScrollPane scrollPaneReservationList = new JScrollPane(lesFactures);
        scrollPaneReservationList.setPreferredSize(new Dimension(w - 50, 170));
        lesFactures.setBackground(new Color(255, 255, 180));
        
        JPanel NorthPane3 = new JPanel();
        NorthPane3.setLayout(new GridLayout(1, 3));
        NorthPane3.add(ADD_DATA);
        NorthPane3.add(EDIT_DATA);
        NorthPane3.add(LESCLIENTS);

        clientPane.add(NorthPane3, BorderLayout.NORTH);
        clientPane.add(scrollPaneClientList, BorderLayout.CENTER);

        JPanel paneC = new JPanel();
        //   paneC.setPreferredSize(new Dimension(w, 20)); 
        paneC.add(shaka, BorderLayout.WEST);
        paneC.add(searchPatient, BorderLayout.EAST);
        clientPane.add(paneC, BorderLayout.SOUTH);
        clientPane.add(new JLabel());

        CommonWestPane.add(clientPane);
        //CommonWestPane.add(panelSouth); 
//         CommonWestPane.add(ReservationPane);  

        return CommonWestPane;
    }

    /**************************************************************************
    AFFICHE  du ////////////// stock
     **************************************************************************/
    public JPanel westPane(int w) {

        JPanel commonWestPane = new JPanel();
        commonWestPane.setPreferredSize(new Dimension(w, 700));

        JPanel NorthPane = new JPanel();
        NorthPane.setSize(new Dimension(w, 30));

        addButton = new JButton(l(lang, "V_SELL"));
        addButton.setBackground(Color.orange);
        addButton.setFont(police);

        getStockButton = new JButton(l(lang, "V_STOCK"));
        getStockButton.setBackground(Color.GREEN);
        getStockButton.setFont(police);

        addButtonZOOM = new JButton(l(lang, "V_SELL"));
        addButtonZOOM.setBackground(Color.orange);
        addButton.setFont(police);

        getStockButtonZOOM = new JButton(l(lang, "V_STOCK"));
        getStockButtonZOOM.setBackground(Color.GREEN);
        getStockButtonZOOM.setFont(police);

        askButton = new JButton("ASK");
        askButton.setBackground(Color.CYAN);
        askButton.setFont(police);

        labelProduit = new JLabel(l(lang, "V_ITEMS"));
        labelProduit.setFont(police);

        //  NorthPane.add(askButton,BorderLayout.WEST);
        if (isClinic) {
            NorthPane.add(PRESCRIPTION, BorderLayout.WEST);
        }
        NorthPane.add(getStockButton, BorderLayout.WEST);
        NorthPane.add(labelProduit, BorderLayout.WEST);
        NorthPane.add(addButton, BorderLayout.EAST);

        commonWestPane.add(NorthPane, BorderLayout.NORTH);

        centerPane = new JPanel();
        centerPane.setSize(new Dimension(w, 420));

        scrollPaneProductList = new JScrollPane(allProductList);
        scrollPaneProductList.setPreferredSize(new Dimension(w - 10, 410));
        allProductList.setBackground(Color.PINK);
        allProductList.setFont(police);


        centerPane.add(scrollPaneProductList, BorderLayout.SOUTH);

        commonWestPane.add(centerPane, BorderLayout.CENTER);


        southPane = new JPanel();
        southPane.setLayout(new GridLayout(2, 1));
        southPane.setSize(new Dimension(w, 250));

        JPanel paneN = new JPanel();
        paneN.setPreferredSize(new Dimension(w, 40));

        texttoBESent.setPreferredSize(new Dimension(80, 30));
        sendButton = new JButton(l(lang, "V_SELL"));
        sendButton.setBackground(Color.orange);
        texttoBESent.setFont(police);
        sendButton.setFont(police);
        radioSpecial.setFont(police);

        paneN.add(radioSpecial, BorderLayout.WEST);
//        paneN.add(texttoBESent, BorderLayout.CENTER);
//        paneN.add(sendButton, BorderLayout.EAST);

        JPanel paneC = new JPanel();
        paneC.setPreferredSize(new Dimension(w, 20));

        JLabel searchButton = new JLabel(l(lang, "V_SEARCH"));
        search.setPreferredSize(new Dimension(140, 30));
        search.setFont(police);
        searchButton.setFont(police);


        paneC.add(searchButton, BorderLayout.WEST);
        paneC.add(search, BorderLayout.EAST);


        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(4, 2));
        panel.setPreferredSize(new Dimension(w - 100, 120));

        unVedette = new JButton(getString("unVedette"));
        unVedette.setBackground(Color.green);
        unVedette.setFont(police);

        deuxVedette = new JButton(getString("deuxVedette"));
        deuxVedette.setBackground(Color.yellow);
        deuxVedette.setFont(police);

        troisVedette = new JButton(getString("troisVedette"));
        troisVedette.setBackground(Color.yellow);
        troisVedette.setFont(police);

        quatreVedette = new JButton(getString("quatreVedette"));
        quatreVedette.setBackground(Color.yellow);
        quatreVedette.setFont(police);

        cinqVedette = new JButton(getString("cinqVedette"));
        cinqVedette.setFont(police);
        //cinqVedette.setBackground(Color.pink);

        sixVedette = new JButton(getString("sixVedette"));
        sixVedette.setFont(police);
        //sixVedette.setBackground(Color.pink);

        septVedette = new JButton(getString("septVedette"));
        septVedette.setFont(police);
        //septVedette.setBackground(Color.pink);

        huitVedette = new JButton(getString("huitVedette"));
        huitVedette.setBackground(Color.green);
        huitVedette.setFont(police);

        panel.add(unVedette);
        panel.add(deuxVedette);
        panel.add(troisVedette);
        panel.add(quatreVedette);
        panel.add(cinqVedette);
        panel.add(sixVedette);
        panel.add(septVedette);
        panel.add(huitVedette);


        JPanel paneD = new JPanel();
        // paneD.setPreferredSize(new Dimension(400, 20));
        paneD.setLayout(new GridLayout(2, 1));

        paneD.add(paneC);
        paneD.add(paneN);
        

        //  southPane.add(paneN, BorderLayout.NORTH);

        southPane.add(paneD);
        southPane.add(panel, BorderLayout.SOUTH);

        commonWestPane.add(southPane, BorderLayout.SOUTH);
        return commonWestPane;

    }

    public JPanel westPaneZoom() {

        JPanel commonWestPane = new JPanel();
        commonWestPane.setPreferredSize(new Dimension(800, 500));
        JPanel NorthPane = new JPanel();
        NorthPane.setSize(new Dimension(400, 30));

        //  NorthPane.add(askButton,BorderLayout.WEST);
        JLabel lp = new JLabel(l(lang, "V_ITEMS"));
        lp.setFont(police);
        NorthPane.add(getStockButtonZOOM, BorderLayout.WEST);
        NorthPane.add(lp, BorderLayout.WEST);
        NorthPane.add(addButtonZOOM, BorderLayout.EAST);
        getStockButtonZOOM.setFont(police);
        addButtonZOOM.setFont(police);

        commonWestPane.add(NorthPane, BorderLayout.NORTH);

        JPanel centerPane = new JPanel();
        centerPane.setSize(new Dimension(800, 420));
        // allProductListZOOM.setFont(new Font("Helvetica", Font.BOLD, 20));
        allProductListZOOM.setFont(police);
        scrollPaneProductList = new JScrollPane(allProductListZOOM);
        scrollPaneProductList.setPreferredSize(new Dimension(790, 410));
        allProductListZOOM.setBackground(Color.WHITE);
        scrollPaneProductList.setFont(police);
        centerPane.add(scrollPaneProductList, BorderLayout.SOUTH);
        commonWestPane.add(centerPane, BorderLayout.CENTER);

        JPanel southPane = new JPanel();
        southPane.setLayout(new GridLayout(2, 1));
        southPane.setSize(new Dimension(400, 250));

        JPanel paneN = new JPanel();
        paneN.setPreferredSize(new Dimension(400, 40));

        searchZOOM.setPreferredSize(new Dimension(140, 30));
        searchZOOM.setFont(police);
        paneN.add(searchZOOM, BorderLayout.EAST);

        southPane.add(paneN);
        commonWestPane.add(southPane, BorderLayout.SOUTH);
        return commonWestPane;

    }

    public void actionPerformed(ActionEvent e) {
        IMPR = e.getActionCommand();

    }

    public JPanel RadioButton1() {
//Create the radio buttons.
        String defMMI=getString("defaultmmi");
        
        JRadioButton EPSON = new JRadioButton("EPSON");
        EPSON.setMnemonic(KeyEvent.VK_P);
        EPSON.setActionCommand("EPSON");
        
        JRadioButton A5 = new JRadioButton("A5");
        A5.setMnemonic(KeyEvent.VK_L);
        A5.setActionCommand("A5");
        if(defMMI==null || defMMI.equals("EPSON"))
        {
            IMPR = "EPSON";
            EPSON.setSelected(true);
        }
        else
        {
            IMPR = "A5";
            A5.setSelected(true);
        }

        ButtonGroup group = new ButtonGroup();
        group.add(A5);
        group.add(EPSON);
        

//Register a listener for the radio buttons.
        EPSON.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                IMPR = ae.getActionCommand();
            }
        });

        A5.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                IMPR = ae.getActionCommand();
            }
        });

//Put the radio buttons in a column in a panel.
        JPanel radioPanel = new JPanel(new GridLayout(0, 1));
        radioPanel.add(A5);
        radioPanel.add(EPSON);        
        add(radioPanel, BorderLayout.LINE_START);
//  setBorder(BorderFactory.createEmptyBorder(20,20,20,20));
        return radioPanel;
    }

    /**************************************************************************
    AFFICHE du Vedettes
     **************************************************************************/
    public JPanel centerPane(int w2) {

        b1 = new JButton();
        b1.setFont(police);
        //  b1.setFont(new Font("Helvetica", Font.PLAIN, 20));
        b2 = new JButton();
        b2.setFont(police);
        //  b2.setFont(new Font("Helvetica", Font.PLAIN, 20));
        b2.setBackground(Color.orange);
        b3 = new JButton();
        b3.setFont(police);
        b3.setBackground(Color.yellow);
        //  b3.setFont(new Font("Helvetica", Font.PLAIN, 20));
        b4 = new JButton();
        b4.setBackground(Color.pink);
        b4.setFont(police);
        //  b4.setFont(new Font("Helvetica", Font.PLAIN, 20));
        b5 = new JButton();
        b5.setBackground(Color.green);
        b5.setFont(police);
        //  b5.setFont(new Font("Helvetica", Font.PLAIN, 20));
        b6 = new JButton();
        b6.setBackground(new Color(250, 220, 100));
        b6.setFont(police);
        //  b6.setFont(new Font("Helvetica", Font.PLAIN, 20));
        b7 = new JButton();
        b7.setBackground(Color.gray);
        b7.setFont(police);
        //  b7.setFont(new Font("Helvetica", Font.PLAIN, 20));
        b8 = new JButton();
        b8.setFont(police);
        //  b8.setFont(new Font("Helvetica", Font.PLAIN, 20));
        b9 = new JButton();
        b9.setBackground(Color.orange);
        b9.setFont(police);
        //  b9.setFont(new Font("Helvetica", Font.PLAIN, 20));
        b10 = new JButton();
        b10.setBackground(Color.yellow);
        b10.setFont(police);
        //  b10.setFont(new Font("Helvetica", Font.PLAIN, 20));
        b11 = new JButton();
        b11.setBackground(Color.pink);
        b11.setFont(police);
        // b11.setFont(new Font("Helvetica", Font.PLAIN, 20));
        b12 = new JButton();
        b12.setBackground(Color.green);
        b12.setFont(police);
        // b12.setFont(new Font("Helvetica", Font.PLAIN, 20));



        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(8, 1));
        panel.setBackground(Color.yellow);
        panel.setPreferredSize(new Dimension(w2, 400));
        panel.setFont(police);
        //panel.setFont(new Font("Helvetica", Font.PLAIN, 40));
        panel.add(b1);
        panel.add(b2);
        panel.add(b3);
        panel.add(b4);
        panel.add(b5);
        panel.add(b6);
        panel.add(b7);
        panel.add(b8);
//        panel.add(b9);
//        panel.add(b10);
//        panel.add(b11);
//        panel.add(b12);

        return panel;
    }

    /***********************************************************************
    AFFICHE fenetre des achats BASKET
     ***********************************************************************/
    public JPanel eastPane(int w) {

        JPanel commonEasttPane = new JPanel();
        commonEasttPane.setPreferredSize(new Dimension(w, 700));

        JPanel upPane = new JPanel();
        upPane.setPreferredSize(new Dimension(w, 30));

        JLabel items = new JLabel(l(lang, "V_BASKET"));
        items.setFont(police);

        annuleButton = new JButton(l(lang, "V_ALL"));
        annuleButton.setBackground(Color.red);
        annuleButton.setFont(police);


        removeButton = new JButton(l(lang, "V_ONE"));
        removeButton.setBackground(Color.MAGENTA);
        removeButton.setFont(police);

        repriceButtton = new JButton(l(lang, "V_REDUCTION"));
        repriceButtton.setBackground(Color.GREEN);
        repriceButtton.setFont(police);

        aboutButton = new JButton("?");
        aboutButton.setBackground(Color.white);
        aboutButton.setFont(police);

        upPane.add(annuleButton, BorderLayout.WEST);
        upPane.add(items, BorderLayout.CENTER);
        upPane.add(removeButton, BorderLayout.EAST);
        upPane.add(repriceButtton, BorderLayout.EAST);


        commonEasttPane.add(upPane, BorderLayout.NORTH);


        salePane = new JPanel();
        salePane.setPreferredSize(new Dimension(w, 400));

        scrollPaneallSale = new JScrollPane(saleJList);
        scrollPaneallSale.setPreferredSize(new Dimension(w - 10, 330));
        saleJList.setBackground(Color.WHITE);
        saleJList.setFont(police);

        pricePane = new JPanel();
        pricePane.setPreferredSize(new Dimension(w, 70));

        JLabel totalLabel = new JLabel(l(lang, "V_TOTAL"));
//        police = new Font("Arial", Font.BOLD, 15);
        totalLabel.setFont(police);

        //total.setFont(new Font("Helvetica", Font.PLAIN, 40));
        total.setFont(new Font(police.getName(), police.getStyle(), 30));
        total.setText(0 + " " + dev);

        pricePane.add(totalLabel, BorderLayout.WEST);
        pricePane.add(total, BorderLayout.EAST);

        salePane.add(scrollPaneallSale, BorderLayout.NORTH);
        salePane.add(pricePane, BorderLayout.CENTER);


        commonEasttPane.add(salePane, BorderLayout.CENTER);

        JPanel DownPane = new JPanel();
        DownPane.setPreferredSize(new Dimension(w, 350));

        JPanel money = new JPanel();
        money.setPreferredSize(new Dimension(w+20, 50));

        cashButton = new JButton(l(lang, "V_CASH"));
        cashButton.setBackground(Color.GREEN);
        cashButton.setFont(police);

        venteButton = new JButton(l(lang, "V_RETOUR"));
        venteButton.setBackground(Color.YELLOW);
        venteButton.setFont(police);

        creditButton = new JButton(l(lang, "V_CREDIT"));
        creditButton.setBackground(Color.red);
        creditButton.setFont(police);

        proformaButton = new JButton("PROFORMA");
        proformaButton.setBackground(new java.awt.Color(203, 215, 253));
        proformaButton.setFont(police);

        smartButton = new JButton("CARD");
        smartButton.setBackground(new java.awt.Color(167,167,167));
        smartButton.setFont(police);
        
        {
        if(MySQLConnector.imAliveCard)
        { smartButton.setBackground(Color.green);}
        else
        {
          smartButton.setBackground(Color.orange);  
        }
        }
        money.add(cashButton, BorderLayout.WEST);
        money.add(creditButton, BorderLayout.EAST);
//        money.add(venteButton, BorderLayout.EAST);
        money.add(proformaButton, BorderLayout.EAST);
        money.add(smartButton, BorderLayout.EAST);

        DownPane.add(money, BorderLayout.NORTH); 
        
        Tarif tt1 = tarif[0];
        t1 = new JButton(tt1.DES_TARIF);
        t1.setBackground(new Color(tt1.r, tt1.g, tt1.b));
        t1.setFont(police);
        Tarif tt2 = tarif[1];
        t2 = new JButton(tt2.DES_TARIF);
        t2.setBackground(new Color(tt2.r, tt2.g, tt2.b));
        t2.setFont(police);
        Tarif tt3 = tarif[2];
        t3 = new JButton(tt3.DES_TARIF);
        t3.setBackground(new Color(tt3.r, tt3.g, tt3.b));
        t3.setFont(police);
        Tarif tt4 = tarif[3];
        t4 = new JButton(tt4.DES_TARIF);
        t4.setBackground(new Color(tt4.r, tt4.g, tt4.b));
        t4.setFont(police);
        Tarif tt5 = tarif[4];
        t5 = new JButton(tt5.DES_TARIF);
        t5.setBackground(new Color(tt5.r, tt5.g, tt5.b));
        t5.setFont(police);
        Tarif tt6 = tarif[5];
        t6 = new JButton(tt6.DES_TARIF);
        t6.setBackground(new Color(tt6.r, tt6.g, tt6.b));
        t6.setFont(police);
        Tarif tt7 = tarif[6];
        t7 = new JButton(tt7.DES_TARIF);
        t7.setBackground(new Color(tt7.r, tt7.g, tt7.b));
        t7.setFont(police);
        Tarif tt8 = tarif[7];
        t8 = new JButton(tt8.DES_TARIF);
        t8.setBackground(new Color(tt8.r, tt8.g, tt8.b));
        t8.setFont(police);
        Tarif tt9 = tarif[8];
        t9 = new JButton(tt9.DES_TARIF);
        t9.setBackground(new Color(tt9.r, tt9.g, tt9.b));
        t9.setFont(police);
        Tarif tt10 = tarif[9];
        t10 = new JButton(tt10.DES_TARIF);
        t10.setBackground(new Color(tt10.r, tt10.g, tt10.b));
        t10.setFont(police);
        
        
        JPanel paneChoixM = new JPanel();
        paneChoixM.setLayout(new GridLayout(1, 2));
        paneChoixM.setBackground(Color.yellow);
        paneChoixM.setPreferredSize(new Dimension(w, 100));
        
        
        JPanel paneChoix = new JPanel();
        
        if(!getString("nom").contains("PHARMACIE"))
        {
            paneChoix.setLayout(new GridLayout(5, 2));
            paneChoix.setBackground(Color.yellow);
            paneChoix.setPreferredSize(new Dimension(w, 100));            
            
            paneChoix.add(t1);
            paneChoix.add(t2);
            paneChoix.add(t3);
            paneChoix.add(t4);
            paneChoix.add(t5);
            paneChoix.add(t6);
            paneChoix.add(t7);
            paneChoix.add(t8);
            paneChoix.add(t9);
            paneChoix.add(t10);
            
            DownPane.add(paneChoix, BorderLayout.CENTER);
        }
        else
        {
            paneChoix.setLayout(new GridLayout(5, 1));
            paneChoix.setBackground(Color.yellow);
            paneChoix.setPreferredSize(new Dimension(w/2, 100));
            
            paneChoix.add(t1);
            paneChoix.add(t2);
            paneChoix.add(t3);
            paneChoix.add(t4);
            paneChoix.add(t5);
            paneChoixM.add(paneChoix);
            
            Tarif [] tarif2 = new Tarif [tarif.length-5];
        System.out.println("-------------------------");
        System.out.println(tarif.length);
        for(int i=5 ;i<tarif.length;i++)
        {
            tarif2[i-5]=tarif[i];
           // System.out.println(tarif2[i-5]);
        }
        System.out.println(tarif2.length);
        if(tarif2!=null)
        {
        assList.setListData(tarif2); 
        assList.setCellRenderer(new MyListCellThing(tarif2,police));
        } 
            JScrollPane   assListScro = new JScrollPane(assList);
            assListScro.setPreferredSize(new Dimension(w - 10, 412));
//            assList.setBackground(Color.WHITE);
            assList.setFont(police); 
            paneChoixM.add(assListScro);
            
            DownPane.add(paneChoixM, BorderLayout.CENTER);
        }  


        JPanel data = new JPanel();
        data.setPreferredSize(new Dimension(w, 80));

        scanButton = new JButton(l(lang, "V_SCAN"));
        scanButton.setBackground(Color.green);
        scanButton.setFont(police);

        actButton = new JButton(l(lang, "V_GUHINDURA"));
        actButton.setBackground(Color.ORANGE);
        actButton.setFont(police);

        stockButton = new JButton(l(lang, "V_DATA"));
        stockButton.setBackground(Color.yellow);
        stockButton.setFont(police);
        
        autreTarif = new JButton("TARIF");
        autreTarif.setFont(police);

        JPanel sPane = new JPanel();
        sPane.setSize(new Dimension(w / 3, 50));

        sPane.add(stockButton, BorderLayout.WEST);
        sPane.add(scanButton, BorderLayout.CENTER);
        sPane.add(actButton, BorderLayout.EAST);        
//        sPane.add(venteButton, BorderLayout.EAST);  //RETURN BUTTON KURI HOME PAGE
//        sPane.add(autreTarif, BorderLayout.EAST);


        data.add(sPane, BorderLayout.NORTH);

        JPanel paneNew = new JPanel();
        paneNew.setPreferredSize(new Dimension(w, 30));
        paneNew.setLayout(new GridLayout(1, 2));

        newClient = new JButton(l(lang, "V_CLIENT"));
        newClient.setFont(police);

        optionsButton = new JButton(l(lang, "V_OPTIONS"));
        optionsButton.setBackground(Color.pink);
        optionsButton.setFont(police);

        newTest = new JButton("TEST");
        newTest.setBackground(Color.ORANGE);

        paneNew.add(newClient);
        paneNew.add(optionsButton);
        paneNew.add(aboutButton);
        //paneNew.add(newTest);

        data.add(paneNew, BorderLayout.SOUTH);

        DownPane.add(data, BorderLayout.SOUTH);

        commonEasttPane.add(DownPane, BorderLayout.SOUTH);

        return commonEasttPane;

    }
    
    public void assListEcoute() {
        assList.addMouseListener(new MouseListener() {

            public void actionPerformed(ActionEvent ae) {
            } 
            public void mouseExited(MouseEvent ae) {
            } 
            public void mouseClicked(MouseEvent e) {

                Tarif tarifa =(Tarif)(assList.getSelectedValue());
                    live_Tarif = tarifa.DES_TARIF;
                    observe_Tarif=tarifa.OBSERVE;
                    if(tarifa.SMART!=null)
                    {
                        allow_smart=tarifa.SMART;
                    }
                    
                    umukozi = cashier.db.getName(umukozi.ID_EMPLOYE);
                    if (cashier.current.getProductList().size() < 1 || (stock.equals(live_Tarif) && cashier.current.getProductList().size() >= 1)) {                        
                        v = 1;
                        stock = tarifa.DES_TARIF;
                        live_Tarif = tarifa.DES_TARIF;
                        var = tarifa.VAR_TARIF;
                        cashier.stock(var, sqlFamille);
                        setTitle(nom + "'s Ishyiga " + l(lang, "V_SESSION") + " + Tarif " + stock + " +MODE: " + IJISHO);
                        showProduct();
                        if (color.equals("OK")) {
                            allProductList.setBackground(new Color(tarifa.r, tarifa.g, tarifa.b));
                            assList.setBackground(new Color(tarifa.r, tarifa.g, tarifa.b));
                        }
                    } else {
                        JOptionPane.showMessageDialog(frame, l(lang, "V_RANGIZA") + "  " + l(lang, "V_LE") + " " + l(lang, "V_FACTURE") + " " + l(lang, "V_PROCESS") + " :" + stock, l(lang, "V_ERROR"), JOptionPane.WARNING_MESSAGE);
                    }
            }

            ;  
    public void mousePressed(MouseEvent e) {
            }

            public void mouseReleased(MouseEvent e) {
            }

            public void mouseEntered(MouseEvent e) {
            }
        });
    }

    public void newClient() {
        newClient.addActionListener(
                new ActionListener() {

public void actionPerformed(ActionEvent ae) {
    if (ae.getSource() == newClient) {
//        int n = JOptionPane.showConfirmDialog(frame, l(lang, "V_MUSHYA") + " = YES,   " + l(lang, "V_GUKOSORA") + " = NON", l(lang, "V_CHOICE"), JOptionPane.YES_NO_OPTION);
 String nAff  = (String)JOptionPane.showInputDialog(null, l(lang, "V_CHOICE"), " ",
                    JOptionPane.QUESTION_MESSAGE, null, new String [] { 
l(lang, "V_HISTORIQUE"),l(lang, "V_GUKOSORA"),l(lang, "V_MUSHYA") }, l(lang, "V_HISTORIQUE")); 
 
if(nAff.equals("HISTORIQUE"))
{ 
    VoirFichier.doOneCredit(stock,cashier.db.s); 
}
else if(nAff.equals(l(lang, "V_MUSHYA")))
{
   nAff = (JOptionPane.showInputDialog(frame, l(lang, "V_SCAN") + "  " + l(lang, "V_IDENTITE")));
            if (!(nAff == null || nAff.length() < 32)) {
                String nom1 = nAff.substring(31, nAff.length());
                String prenom = "";
                String nom = "";
                String mode = "miniscule";

                for (int i = 0; i < nom1.length(); i++) {
                    char c = nom1.charAt(i);
                    int hash = ("" + c).hashCode();
                    if ((hash >= 97 && hash <= 122)) {
                        if (i == 0) {
                            mode = "majiscule";
                        }
                        nom = nom + c;
                    } else {
                        prenom = prenom + c;
                    }
                }
                if (mode.equals("majiscule")) {
                    prenom = (nom.charAt(nom.length() - 1) + prenom).toUpperCase();
                    nom = (nom.substring(0, nom.length() - 1)).toUpperCase();
                } else {
                    nom = prenom.charAt(prenom.length() - 1) + nom;
                    prenom = prenom.substring(0, prenom.length() - 1);
                    String tmt = nom;
                    nom = prenom.toUpperCase();
                    prenom = tmt.toUpperCase();
                }
                String naissance = nAff.substring(2, 6);
                String sexe = nAff.substring(7, 8);
                String code = nAff.substring(9, 21);

                new PersonInterface(cashier.db, nom, prenom, code, naissance, sexe,"","",0,stock);
            } else if (stock.equals("RAMA")) {
                new ClientRamaGui(cashier.db, "", "", "", "", "");
            } else {
                new PersonInterface(cashier.db, "", "", "", "", "","","",0,stock);
            }
        } else {

  nAff = (JOptionPane.showInputDialog(frame, l(lang, "V_NUMERO") + ", " + l(lang, "V_NAMEORSURNAME")));
            Client c = null;
            String db = "";


            if (stock.equals("RAMA")) {
                db = "_RAMA";
            }
            if (stock.equals("CASHNORM")) {
                db = "UNI";
            }
            if (nAff != null) {
                c = cashier.db.getClient(nAff.toUpperCase(), db);
            }
            if (c == null && nAff != null) {
                c = (Client) JOptionPane.showInputDialog(null, l(lang, "V_OPTIONS"), l(lang, "V_CLIENT"), JOptionPane.QUESTION_MESSAGE, null, cashier.db.getClientV(nAff.toUpperCase(), db, stock), nom);
            }
            if (c != null) {
                if (stock.equals("RAMA")) {
                    new ClientRamaGui(cashier.db, c);
                } else {
                    new PersonInterface(cashier.db, c);
                }
            } else {
                JOptionPane.showMessageDialog(null, l(lang, "V_ERROR") + " " + l(lang, "V_NUMAFFILIATION"), l(lang, "V_ERROR"), JOptionPane.PLAIN_MESSAGE);
            }
        }
    }
}
});
    }

    public void Test() {
        newTest.addActionListener(
                new ActionListener() {

                    public void actionPerformed(ActionEvent ae) {
                        if (ae.getSource() == newTest) {
                            new Test(cashier.db);
                        }
                    }
                });
    }

    public void uploadPre(String donne) {
        donne = donne.replaceAll(";;", ";-;");
        String[] tubigabanye = donne.split(":::");
        String catA = "";
        String catB = "";
        System.out.println("tubi:" + tubigabanye.length);

        for (int i = 0; i < tubigabanye.length; i++) {
            catA = tubigabanye[i];
            catB = tubigabanye[i + 1];
            i = i + 2;
        }
        System.out.println("rimwe:" + catA);
        System.out.println("kabi:" + catB);
        //dukate info za mbere.
        StringTokenizer st1 = new StringTokenizer(catA);
        String num_cl = st1.nextToken(";");
        String ass = st1.nextToken(";");
        String dr = st1.nextToken(";");
        String quit = st1.nextToken(";");

        if (stock.contains("RAMA")) {
            currentClient = cashier.db.getClientA("" + num_cl, "_RAMA", ass);
        } else {
            currentClient = cashier.db.getClientA("" + num_cl, "", ass);
        }
//        ordonnance = "" + quit + "-" + dr;


        //dukate info z'imiti
        String[] umuti = catB.split("#");
        System.out.println("kabi:" + umuti.length);
        for (int i = 0; i < umuti.length; i++) {
            StringTokenizer st2 = new StringTokenizer(umuti[i]);
            String code = st2.nextToken(";");
            String qty = st2.nextToken(";");
            String dose = st2.nextToken(";");
            Product tubonye = null;

            if (stock.equals("CASHNORM")) {
                tubonye = cashier.db.getProduct(code, "", "PRIX,");
            } else {
                tubonye = cashier.db.getProduct(code, "", "PRIX_" + stock + ",");
            }

            addProductLot(tubonye, Integer.parseInt(qty));
        } 
    }

public void optionsButton() {
optionsButton.addActionListener(
    new ActionListener() {

        public void actionPerformed(ActionEvent ae) {
            if (ae.getSource() == optionsButton && umukozi.PWD_EMPLOYE.contains("OPTIONS")) {
//                            String nAff = (String) JOptionPane.showInputDialog(null, l(lang, "V_OPTIONS"), " ",
//                                    JOptionPane.QUESTION_MESSAGE, null, new String[]{"PRESCRIPTION","IMPRIMER FACTURE", "TICKET DE CAISSE", "ANNULER"}, nom);

                String nAff = (String) JOptionPane.showInputDialog(null, l(lang, "V_OPTIONS"), " ",
                        JOptionPane.QUESTION_MESSAGE, null,
                        new String[]{
                            "" + l(lang, "V_PRINT") + " " + l(lang, "V_FACTURE"),
                            "" + l(lang, "V_VIEW") + " " + l(lang, "V_FACTURE"),
                            "" + l(lang, "V_REFUND") + " " + l(lang, "V_FACTURE"),
                            "TICKET DE CAISSE",
                            "DEPOT CAISSE",
                            "RETRAIT CAISSE",
                            "" + l(lang, "V_COPY") + " " + l(lang, "V_FACTURE"),
                            "A5", "SYNC FAILED","PUSH SMART"}, nom);

                if (nAff.equals("PRESCRIPTION")) {
                    String ch = (String) JOptionPane.showInputDialog(null, l(lang, "V_OPTIONS"), l(lang, "V_IMPRESSION"),
                            JOptionPane.QUESTION_MESSAGE, null, new String[]{"CODEBAR", "CLOUD"}, nom);
                    if (ch.equals("CODEBAR")) {

//                                    String donne = JOptionPane.showInputDialog("SCANNER VOTRE PRESCRIPTION","SCANN");
                        String donne = JOptionPane.showInputDialog("SCANNER VOTRE PRESCRIPTION", "SRS/0004020055/04;SORAS;KANIMBA;1:::AACIF01;10;3/J#ALGIP01;2;3/J#PAIDO01;3;OK#:::");
                        donne = donne.replaceAll(";;", ";-;");
                        uploadPre(donne.toUpperCase());
                    } else if (ch.equals("CLOUD")) {
                        CommCloud product_Cloud = new CommCloud("http://www.ishyiga" + getString("extension") + "/test_cloud/download_prescription.php?soc=" + cashier.db.dbName + "&user=" + umukozi.NOM_EMPLOYE, "");
                        System.out.println("" + product_Cloud.sendlot);

                        String[] presZose = product_Cloud.sendlot.split(">>>");
                        String[] control = new String[presZose.length];
                        for (int i = 0; i < presZose.length; i++) {
                            String test = presZose[i];
                            System.out.println("TT:" + test);

                            StringTokenizer st1 = new StringTokenizer(test);
                            String num_cl = st1.nextToken(";");
                            String ass = st1.nextToken(";");
                            String doct = st1.nextToken(";");
                            String rest = st1.nextToken(";");
                            control[i] = i + "-" + num_cl + "-" + ass + "-" + doct;
                        }
                        String inv = (String) JOptionPane.showInputDialog(null, l(lang, "V_OPTIONS"), l(lang, "V_IMPRESSION"),
                                JOptionPane.QUESTION_MESSAGE, null, control, "");
                        if (inv != null) {
                            StringTokenizer st1 = new StringTokenizer(inv);
                            String t = st1.nextToken("-");
                            st1.nextToken("-");
                            System.out.println("SSS:" + presZose[Integer.parseInt(t)]);
                            uploadPre(presZose[Integer.parseInt(t)]);

                        }
                    }
                } 
                
                else if (nAff.equals("SYNC FAILED")) {
                    String ch = (String) JOptionPane.showInputDialog(null, l(lang, "V_OPTIONS"), l(lang, "V_IMPRESSION"),
                            JOptionPane.QUESTION_MESSAGE, null, new String[]{l(lang, "V_PARNUMERO"), l(lang, "V_PARHEURE")}, nom);
                    boolean doIt = false;
                    int id_invoice = 0;
                    Invoice inv = null;
                   
                    if (ch.equals(l(lang, "V_PARHEURE"))) {//PAR HEURE
                        inv = (Invoice) JOptionPane.showInputDialog(null, l(lang, "V_OPTIONS"), l(lang, "V_IMPRESSION"),
                                JOptionPane.QUESTION_MESSAGE, null, cashier.db.printByHeure(ch), "");
                        id_invoice = inv.id_invoice;
                        doIt = true;
                    }
                    if (ch.equals(l(lang, "V_PARNUMERO"))) {//PAR NUMERO
                        id_invoice = getQuantity(l(lang, "V_IDINVOICE") + " ", " ", 0);
                        doIt = true;
                    }
                    if (doIt) 
                    {
                        cashier.db.doFinishVenteV7(id_invoice,0,0);
                    }
//                        cashier.db.printA5(id_invoice, id_client, umukozi, type);
                    
                }
                
                else if (nAff.equals("" + l(lang, "V_VIEW") + " " + l(lang, "V_FACTURE"))) {

                    String NS = (String) JOptionPane.showInputDialog(null, l(lang, "V_OPTIONS"), l(lang, "V_IMPRESSION"),
                            JOptionPane.QUESTION_MESSAGE, null, new String[]{"NS", "NR", "CS", "CR", "TS", "TR"}, "NS");

                    String ch = (String) JOptionPane.showInputDialog(null, l(lang, "V_OPTIONS"), l(lang, "V_IMPRESSION"),
                            JOptionPane.QUESTION_MESSAGE, null, new String[]{l(lang,"V_PARGROUPE"), l(lang, "V_PARNUMERO"), l(lang, "V_PARHEURE")}, nom);
                    boolean doIt = false;
                    int ID_INVOICE = 0;
                    if (ch.equals(l(lang,"V_PARGROUPE"))) {
                        Invoice inv = (Invoice) JOptionPane.showInputDialog(null, l(lang, "V_OPTIONS"), l(lang, "V_IMPRESSION"),
                                JOptionPane.QUESTION_MESSAGE, null, cashier.db.printByChoice(ch, NS), "");
                        if (inv != null && inv.id_invoice >= 1) {
                            ID_INVOICE = inv.id_invoice;
                            doIt = true;
                        }
                    }
                    if (ch.equals(l(lang, "V_PARHEURE"))) {
                        Invoice inv = (Invoice) JOptionPane.showInputDialog(null, l(lang, "V_OPTIONS"), l(lang, "V_IMPRESSION"),
                                JOptionPane.QUESTION_MESSAGE, null, cashier.db.printByChoice(ch, NS), "");
                        if (inv != null && inv.id_invoice >= 1) {
                            ID_INVOICE = inv.id_invoice;
                            doIt = true;
                        }
                    }
                    if (ch.equals(l(lang, "V_PARNUMERO"))) {
                        ID_INVOICE = getQuantity(l(lang, "V_IDINVOICE") + " ", " ", 0);
                        doIt = true;
                    } 
                    if (doIt) { 
                                  tax.printViewInvoice(ID_INVOICE);
                    }
                }
                
                else if (nAff.equals("" + l(lang, "V_COPY") + " " + l(lang, "V_FACTURE"))) {
                    String NS = (String) JOptionPane.showInputDialog(null, l(lang, "V_OPTIONS"), l(lang, "V_IMPRESSION"),
                            JOptionPane.QUESTION_MESSAGE, null, new String[]{"NS", "NR"}, "NS");

                    String ch = (String) JOptionPane.showInputDialog(null, l(lang, "V_OPTIONS"), l(lang, "V_IMPRESSION"),
                            JOptionPane.QUESTION_MESSAGE, null, new String[]{l(lang,"V_PARGROUPE"), l(lang, "V_PARNUMERO"), l(lang, "V_PARHEURE")}, nom);
                    boolean doIt = false;
                    int qtyCode = 0;
                    if (ch.equals(l(lang,"V_PARGROUPE"))) {
                        Invoice inv = (Invoice) JOptionPane.showInputDialog(null, l(lang, "V_OPTIONS"), l(lang, "V_IMPRESSION"),
                                JOptionPane.QUESTION_MESSAGE, null, cashier.db.printByChoice(ch, NS), "");
                        qtyCode = inv.id_invoice;
                        doIt = true;
                    }
                    if (ch.equals(l(lang, "V_PARHEURE"))) {
                        Invoice inv = (Invoice) JOptionPane.showInputDialog(null, l(lang, "V_OPTIONS"), l(lang, "V_IMPRESSION"),
                                JOptionPane.QUESTION_MESSAGE, null, cashier.db.printByChoice(ch, NS), "");
                        qtyCode = inv.id_invoice;
                        doIt = true;
                    }
                    if (ch.equals(l(lang, "V_PARNUMERO"))) {
                        qtyCode = getQuantity(l(lang, "V_IDINVOICE") + " ", " ", 0);
                        doIt = true;
                    }

                    if (doIt && NS != null) {
                        int max = cashier.db.getMaxInvoice(NS);
                        int ref = cashier.db.getRefInvoice(qtyCode);


                        if (NS.equals("NS") && (ref != -1 || max != qtyCode)) {
                            JOptionPane.showMessageDialog(frame, l(lang,"V_LASTINVOICE") + NS, " NO COPY ", JOptionPane.WARNING_MESSAGE);

                        } else if (NS.equals("NR") && (max != qtyCode)) {
                            JOptionPane.showMessageDialog(frame, l(lang,"V_LASTINVOICE") + NS, " NO COPY ", JOptionPane.WARNING_MESSAGE);

                        } else {
                            cashier.db.printRetardRRACOPY2(qtyCode, "", umukozi, tax.rraClient, NS);
                        }

                    }

                } 
                else if (nAff.equals("A5")) {
                    String ch = (String) JOptionPane.showInputDialog(null, l(lang, "V_OPTIONS"), l(lang, "V_IMPRESSION"),
                            JOptionPane.QUESTION_MESSAGE, null, new String[]{l(lang,"V_PARGROUPE"), l(lang, "V_PARNUMERO"), l(lang, "V_PARHEURE")}, nom);
                    boolean doIt = false;
                    int id_invoice = 0;
                    Invoice inv = null;
                    String id_client = "RAMA";
                    if (ch.equals(l(lang,"V_PARGROUPE"))) {
                        inv = (Invoice) JOptionPane.showInputDialog(null, l(lang, "V_OPTIONS"), l(lang, "V_IMPRESSION"),
                                JOptionPane.QUESTION_MESSAGE, null, cashier.db.printByChoice(ch, "NS"), "");
                        id_invoice = inv.id_invoice;
                        id_client = inv.id_client;
                        doIt = true;
                    }
                    if (ch.equals(l(lang, "V_PARHEURE"))) {
                        inv = (Invoice) JOptionPane.showInputDialog(null, l(lang, "V_OPTIONS"), l(lang, "V_IMPRESSION"),
                                JOptionPane.QUESTION_MESSAGE, null, cashier.db.printByChoice(ch, "NS"), "");
                        id_invoice = inv.id_invoice;
                        id_client = inv.id_client;
                        doIt = true;
                    }
                    if (ch.equals(l(lang, "V_PARNUMERO"))) {
                        id_invoice = getQuantity(l(lang, "V_IDINVOICE") + " ", " ", 0);

                        id_client = (String) JOptionPane.showInputDialog(null, l(lang, "V_OPTIONS"), l(lang, "V_IMPRESSION"),
                                JOptionPane.QUESTION_MESSAGE, null, new String[]{"RAMA", "MMI"}, "");
                        doIt = true;
                    }
                    if (doIt) {
                        String type = getString("fact");
                        if (id_client.equals("MMI")) {
                            type = "FACTUREMMI";
                        }
                        cashier.db.printA5(id_invoice, id_client, umukozi, type);
                    }
                } else if (nAff.equals("" + l(lang, "V_PRINT") + " " + l(lang, "V_FACTURE"))) {

                    String NS = (String) JOptionPane.showInputDialog(null, l(lang, "V_OPTIONS"), l(lang, "V_IMPRESSION"),
                            JOptionPane.QUESTION_MESSAGE, null, new String[]{"NS", "NR", "CS", "CR", "TS", "TR"}, "NS");

                    String ch = (String) JOptionPane.showInputDialog(null, l(lang, "V_OPTIONS"), l(lang, "V_IMPRESSION"),
                            JOptionPane.QUESTION_MESSAGE, null, new String[]{l(lang,"V_PARGROUPE"), l(lang, "V_PARNUMERO"), l(lang, "V_PARHEURE")}, nom);
                    boolean doIt = false;
                    int ID_INVOICE = 0;
                    if (ch.equals(l(lang,"V_PARGROUPE"))) {
                        Invoice inv = (Invoice) JOptionPane.showInputDialog(null, l(lang, "V_OPTIONS"), l(lang, "V_IMPRESSION"),
                                JOptionPane.QUESTION_MESSAGE, null, cashier.db.printByChoice(ch, NS), "");
                        if (inv != null && inv.id_invoice >= 1) {
                            ID_INVOICE = inv.id_invoice;
                            doIt = true;
                        }
                    }
                    if (ch.equals(l(lang, "V_PARHEURE"))) {
                        Invoice inv = (Invoice) JOptionPane.showInputDialog(null, l(lang, "V_OPTIONS"), l(lang, "V_IMPRESSION"),
                                JOptionPane.QUESTION_MESSAGE, null, cashier.db.printByChoice(ch, NS), "");
                        if (inv != null && inv.id_invoice >= 1) {
                            ID_INVOICE = inv.id_invoice;
                            doIt = true;
                        }
                    }
                    if (ch.equals(l(lang, "V_PARNUMERO"))) {
                        ID_INVOICE = getQuantity(l(lang, "V_IDINVOICE") + " ", " ", 0);
                        doIt = true;
                    } 
                    if (doIt) {
                        LinkedList<String> printR = new LinkedList();
                            String PRINT = (String) JOptionPane.showInputDialog(null, l(lang, "V_OPTIONS"), " OPTIONS IMPRESSION",
                                    JOptionPane.QUESTION_MESSAGE, null, new String[]{"EPSON", "A4"}, "EPSON");
                            if (PRINT.equals("EPSON")) {
                                if (NS != null && NS.contains("R")) {
                                    tax.photoCopyR(ID_INVOICE, NS);
                                } else {
                                    tax.photoCopyS(ID_INVOICE, NS,true,0,0);
                                }
                            } else {
                                cashier.db.printRetardRRA(ID_INVOICE, NS);
                            }
                    }
                } else if (nAff.equals("TICKET DE CAISSE") && (umukozi.PWD_EMPLOYE.contains("TICKET"))) {
                    cashier.db.emp();
                    String[] emp = new String[cashier.db.allEmploye.size()];
                    for (int i = 0; i < emp.length; i++) {
                        emp[i] = ((Employe) cashier.db.allEmploye.get(i)).NOM_EMPLOYE;
                    }

                    String nom = (String) JOptionPane.showInputDialog(null, l(lang, "V_CHOICE"), l(lang, "V_NOM"),
                            JOptionPane.QUESTION_MESSAGE, null, emp, "");

                    String Debut = JOptionPane.showInputDialog(l(lang, "V_DATEDEBUT"), cashier.db.startTime());
                    String FIN = JOptionPane.showInputDialog(l(lang, "V_DATEFIN"), cashier.db.EndTime());
                    //     String FIN = Debut + " 23:59:59";
                    //Debut = Debut + " 00:00:00";

                    int CashierYose = cashier.db.CashierYose(nom, Debut, FIN);
                    
int refundYose = cashier.db.refundYose(nom, Debut, FIN);

String sto = "\n----CASHIER TICKET-------\n CASHIER : " + nom + "  \n NOW TIME    :    "+(new Date()).toLocaleString()+ " ";
sto = sto + "\n START TIME :  " + Debut ;
sto = sto + "\n END TIME   :  " + FIN;
String sto1 = "\n-----------------------";String sto2 = "\n----------------------- ";
sto1 = sto1 + "\n INVOICE  :  ";
sto2 = sto2 + "\n" + RRA_PRINT2.setVirgule(CashierYose, 1, ",") ; 
sto1 = sto1 + "\n-----------------------";sto2 = sto2 + "\n----------------------- ";
sto1 = sto1 + "\n REFUND  :  ";
sto2 = sto2 + "\n" + RRA_PRINT2.setVirgule(refundYose, 1, ",") ;
sto1 = sto1 + "\n-----------------------";sto2 = sto2 + "\n----------------------- ";
sto1 = sto1 + "\n CASH :  ";
sto2 = sto2 + "\n" + RRA_PRINT2.setVirgule((CashierYose-refundYose), 1, ",")  ;
sto1 = sto1 + "\n ";
sto2 = sto2 + "\n ";
sto1 = sto1 + "\n           THANK YOU   " + nom + "";
sto2 = sto2 + "\n "; 

                    int n = JOptionPane.showConfirmDialog(frame, "CREDIT LISTS", " PRINT", JOptionPane.WARNING_MESSAGE);

                    if (n == 0) {
                        String bons = cashier.db.CashierBon(nom, Debut, FIN);
                        String tot = cashier.db.CashierTot(nom, Debut, FIN);
                        sto1 = sto1 + "\n" + bons;
                        sto2 = sto2 + "\n" + tot;
                    }



                    PrintCommand(sto, sto1, sto2, "", "");

                } else if (nAff.equals("" + l(lang, "V_REFUND") + " " + l(lang, "V_FACTURE")) && umukozi.PWD_EMPLOYE.contains("ANNULE")) {
                    int qtyCode = getQuantity(l(lang, "V_IDINVOICE") + " ", " ", 0);
                    String RAIS = "";//JOptionPane.showInputDialog("IMPAMVU ","CL. YABYANZE");
//                                if(isRRA)
                    cashier.db.annuleRRA(qtyCode, nom, RAIS, tax.rraClient);
//                                else
//                                    cashier.db.annule(qtyCode, nom, RAIS);
                } else if (nAff.equals("DEPOT CAISSE")) {
                    int DEPOSIT = getQuantity("DEPOSIT ", " ", 0);
                    if (DEPOSIT < 0) {
                        JOptionPane.showConfirmDialog(null, "ENTER POSITIVE AMMOUNT PLS : " + DEPOSIT, "CAISSE", JOptionPane.YES_NO_OPTION);
                    } else if (DEPOSIT > 0) {
                        Cashier.yose2 = Cashier.yose2 + DEPOSIT;
                        cashier.db.insertLogin2(nom, "DEPOSIT", DEPOSIT);
                    }

                } else if (nAff.equals("RETRAIT CAISSE")) {
                    int RETRAIT = getQuantity(" WITHDRAW ", " ", 0);
                    if (RETRAIT < 0) {
                        JOptionPane.showConfirmDialog(null, "ENTER POSITIVE AMMOUNT PLS : " + RETRAIT, "CAISSE", JOptionPane.YES_NO_OPTION);
                    } else if ((Cashier.yose2 - RETRAIT) > 0) {
                        Cashier.yose2 = Cashier.yose2 - RETRAIT;
                        cashier.db.insertLogin2(nom, "RETRAIT", RETRAIT);
                    } else {
                        JOptionPane.showConfirmDialog(null, "NOT ENOUGH MONEY : " + RETRAIT, "CAISSE", JOptionPane.YES_NO_OPTION);
                    }
                }
                
                else if(nAff.equals("PUSH SMART"))
                {
                    
                    String flag = (String) JOptionPane.showInputDialog(null, l(lang, "V_OPTIONS"), " ",
                        JOptionPane.QUESTION_MESSAGE, null,
                        new String[]{
                            "2",
                            "5"}, "CHOOSE FLAG");
                    
                    if(flag!=null)
                    { 
                    SmartExchange toPush=getPendingSmart(flag);
                    
                    if(toPush!=null)
                    {
                        SMART_TRACKING track=cashier.db.getSmartTrack(toPush.getID(), toPush.getGLOBAL_ID());
                        
                  //      JOptionPane.showMessageDialog(null, new JScrollPane(new JList(lines)));
                         
                        int n = JOptionPane.showConfirmDialog(frame,new JScrollPane(new JList(cashier.db.lines)),
                                            " \n " + l(lang, "V_FICHE") + nAff, JOptionPane.YES_NO_OPTION);
                        if(n==0)
                        {
                            cashier.smartConnection.updateExchange(track.EXTERNAL_ID, track.GLOBAL_ID,track.SENT_TRACKING,flag);
                            cashier.smartConnection.updateFlag7(track.GLOBAL_ID, "2"," and id="+track.EXTERNAL_ID+" and progress_flag="+flag+"");
                            JOptionPane.showMessageDialog(frame, " SMART DATA PUSHED SUCCESSFULLY!!  ", l(lang, "V_ATTENTION"), JOptionPane.INFORMATION_MESSAGE);
                 //   JOptionPane.showMessageDialog(frame, " SMART NOT SUPPOORTED VERSION!!  ", l(lang, "V_ATTENTION"), JOptionPane.INFORMATION_MESSAGE);
                    
                            
                        } 
                        else
                        {
                           JOptionPane.showMessageDialog(frame, " NOT PUSH DONE ", l(lang, "V_ATTENTION"), JOptionPane.WARNING_MESSAGE);
                    
                        }
                        }
                    
                    else 
                    {
                        JOptionPane.showMessageDialog(frame, " NOT PATIENT SELECTED  ", l(lang, "V_ATTENTION"), JOptionPane.WARNING_MESSAGE);
                    } 
                    
                    
                    }
                    
                }
                
                else {
                    JOptionPane.showConfirmDialog(null, l(lang, "V_AUTORISATION"), l(lang, "V_PERMISSION"), JOptionPane.YES_NO_OPTION);
                }
            }
            else {
                    JOptionPane.showConfirmDialog(null, l(lang, "V_AUTORISATION"), l(lang, "V_PERMISSION"), JOptionPane.YES_NO_OPTION);
        }
        }
    });
}

    public void t1() {
        t1.addActionListener(
                new ActionListener() {

                    public void actionPerformed(ActionEvent ae) {
                        if (ae.getSource() == t1) {
                            Tarif tarifa = tarif[0];
                            live_Tarif = tarifa.DES_TARIF;
                            umukozi = cashier.db.getName(umukozi.ID_EMPLOYE);
                            if (cashier.current.getProductList().size() < 1 || (stock.equals(live_Tarif) && cashier.current.getProductList().size() >= 1)) {
                                v = 0;
                                // cashier.stock(""); 
                                var = tarifa.VAR_TARIF;
                                cashier.stock(var, sqlFamille);
                                allProductList.setBackground(Color.pink);
                                // allProductList.setBackground(new Color(tarifa.r,tarifa.g,tarifa.b));
                                stock = "CASHNORM";
                                live_Tarif = tarifa.DES_TARIF;
                                observe_Tarif=tarifa.OBSERVE;
                                allow_smart=tarifa.SMART;
                                setTitle(nom + "'s Ishyiga " + l(lang, "V_SESSION") + " + " + l(lang, "V_TARIF") + "  " + stock + " +MODE: " + IJISHO);
                                showProduct();
                                allProductList.setBackground(Color.pink);
                            } else {
                                JOptionPane.showMessageDialog(frame, l(lang, "V_RANGIZA") + "  " + l(lang, "V_LE") + " " + l(lang, "V_FACTURE") + " " + l(lang, "V_PROCESS") + " :" + stock, l(lang, "V_ERROR"), JOptionPane.WARNING_MESSAGE);
                            }

                        }
                    }
                });
    }

    public void t2() {
        t2.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                if (ae.getSource() == t2) {
                    Tarif tarifa = tarif[1];
                    live_Tarif = tarifa.DES_TARIF;
                    observe_Tarif=tarifa.OBSERVE;
                    allow_smart=tarifa.SMART;
                    umukozi = cashier.db.getName(umukozi.ID_EMPLOYE);
                    
                    if (cashier.current.getProductList().size() < 1 || (stock.equals(live_Tarif) && cashier.current.getProductList().size() >= 1)) {
                        v = 1;
                        stock = tarifa.DES_TARIF;
                        live_Tarif = tarifa.DES_TARIF;
                        var = tarifa.VAR_TARIF;
                        cashier.stock(var, sqlFamille);
                        setTitle(nom + "'s Ishyiga " + l(lang, "V_SESSION") + " + Tarif " + stock + " +MODE: " + IJISHO);
                        showProduct();
                        if (color.equals("OK")) {
                            allProductList.setBackground(new Color(tarifa.r, tarifa.g, tarifa.b));
                        }
                    } else {
                        JOptionPane.showMessageDialog(frame, l(lang, "V_RANGIZA") + "  " + l(lang, "V_LE") + " " + l(lang, "V_FACTURE") + " " + l(lang, "V_PROCESS") + " :" + stock, l(lang, "V_ERROR"), JOptionPane.WARNING_MESSAGE);
                    }
                }
            }
        });
    }

    public void t3() {
        t3.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                if (ae.getSource() == t3) {
                    tarif = cashier.db.getTarif(); 
                    Tarif tarifa = tarif[2];
                    live_Tarif = tarifa.DES_TARIF;
                    observe_Tarif=tarifa.OBSERVE;
                    allow_smart=tarifa.SMART;
                    umukozi = cashier.db.getName(umukozi.ID_EMPLOYE);
                    if (cashier.current.getProductList().size() < 1 || (stock.equals(live_Tarif) && cashier.current.getProductList().size() >= 1)) {
                        v = 1;
                        stock = tarifa.DES_TARIF;
                        live_Tarif = tarifa.DES_TARIF;
                        var = tarifa.VAR_TARIF;
                        cashier.stock(var, sqlFamille);
                        setTitle(nom + "'s Ishyiga " + l(lang, "V_SESSION") + " + Tarif " + stock);
                        showProduct();
                        System.out.println("size prod4:" + new Date());
                        if (color.equals("OK")) {
                            allProductList.setBackground(new Color(tarifa.r, tarifa.g, tarifa.b));
                        }
                    } else {
                        JOptionPane.showMessageDialog(frame, l(lang, "V_RANGIZA") + "  " + l(lang, "V_LE") + " " + l(lang, "V_FACTURE") + " " + l(lang, "V_PROCESS") + " :" + stock, l(lang, "V_ERROR"), JOptionPane.WARNING_MESSAGE);
                    }
                }
            }
        });
    }

    public void t4() {
        t4.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                if (ae.getSource() == t4) {
                    tarif = cashier.db.getTarif();
                    Tarif tarifa = tarif[3];
                    live_Tarif = tarifa.DES_TARIF;
                    observe_Tarif=tarifa.OBSERVE;
                    allow_smart=tarifa.SMART;
                    umukozi = cashier.db.getName(umukozi.ID_EMPLOYE);
                    if (cashier.current.getProductList().size() < 1 || (stock.equals(live_Tarif) && cashier.current.getProductList().size() >= 1)) {
                        v = 1;
                        stock = tarifa.DES_TARIF;
                        live_Tarif = tarifa.DES_TARIF;
                        var = tarifa.VAR_TARIF;
                        cashier.stock(var, sqlFamille);
                        setTitle(nom + "'s Ishyiga " + l(lang, "V_SESSION") + " + Tarif " + stock + " +MODE: " + IJISHO);
                        showProduct();
                        if (color.equals("OK")) {
                            allProductList.setBackground(new Color(tarifa.r, tarifa.g, tarifa.b));
                        }
                    } else {
                        JOptionPane.showMessageDialog(frame, l(lang, "V_RANGIZA") + "  " + l(lang, "V_LE") + " " + l(lang, "V_FACTURE") + " " + l(lang, "V_PROCESS") + " :" + stock, l(lang, "V_ERROR"), JOptionPane.WARNING_MESSAGE);
                    }
                }
            }
        });
    }

    public void t5() {
        t5.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                if (ae.getSource() == t5) {
                    Tarif tarifa = tarif[4];
                    live_Tarif = tarifa.DES_TARIF;
                    observe_Tarif=tarifa.OBSERVE;
                    allow_smart=tarifa.SMART;
                    umukozi = cashier.db.getName(umukozi.ID_EMPLOYE);
                    if (cashier.current.getProductList().size() < 1 || (stock.equals(live_Tarif) && cashier.current.getProductList().size() >= 1)) {
                        v = 1;
                        stock = tarifa.DES_TARIF;
                        live_Tarif = tarifa.DES_TARIF;
                        var = tarifa.VAR_TARIF;
                        cashier.stock(var, sqlFamille);
                        setTitle(nom + "'s Ishyiga " + l(lang, "V_SESSION") + " + Tarif " + stock + " +MODE: " + IJISHO);
                        showProduct();
                        if (color.equals("OK")) {
                            allProductList.setBackground(new Color(tarifa.r, tarifa.g, tarifa.b));
                        }
                    } else {
                        JOptionPane.showMessageDialog(frame, l(lang, "V_RANGIZA") + "  " + l(lang, "V_LE") + " " + l(lang, "V_FACTURE") + " " + l(lang, "V_PROCESS") + " :" + stock, l(lang, "V_ERROR"), JOptionPane.WARNING_MESSAGE);
                    }
                }
            }
        });
    }

    public void t6() {
        t6.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                if (ae.getSource() == t6) {
                    Tarif tarifa = tarif[5];
                    live_Tarif = tarifa.DES_TARIF;
                    observe_Tarif=tarifa.OBSERVE;
                    allow_smart=tarifa.SMART;
                    umukozi = cashier.db.getName(umukozi.ID_EMPLOYE);
                    if (cashier.current.getProductList().size() < 1 || (stock.equals(live_Tarif) && cashier.current.getProductList().size() >= 1)) {
                        v = 1;
                        stock = tarifa.DES_TARIF;
                        live_Tarif = tarifa.DES_TARIF;
                        var = tarifa.VAR_TARIF;
                        cashier.stock(var, sqlFamille);
                        setTitle(nom + "'s Ishyiga " + l(lang, "V_SESSION") + " + Tarif " + stock + " +MODE: " + IJISHO);
                        showProduct();
                        if (color.equals("OK")) {
                            allProductList.setBackground(new Color(tarifa.r, tarifa.g, tarifa.b));
                        }
                    } else {
                        JOptionPane.showMessageDialog(frame, l(lang, "V_RANGIZA") + "  " + l(lang, "V_LE") + " " + l(lang, "V_FACTURE") + " " + l(lang, "V_PROCESS") + " :" + stock, l(lang, "V_ERROR"), JOptionPane.WARNING_MESSAGE);
                    }
                }
            }
        });
    }

    public void t7() {
        t7.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                if (ae.getSource() == t7) {
                    Tarif tarifa = tarif[6];
                    live_Tarif = tarifa.DES_TARIF;
                    observe_Tarif=tarifa.OBSERVE;
                    allow_smart=tarifa.SMART;
                    umukozi = cashier.db.getName(umukozi.ID_EMPLOYE);
                    if (cashier.current.getProductList().size() < 1 || (stock.equals(live_Tarif) && cashier.current.getProductList().size() >= 1)) {
                        v = 1;
                        stock = tarifa.DES_TARIF;
                        live_Tarif = tarifa.DES_TARIF;
                        var = tarifa.VAR_TARIF;
                        cashier.stock(var, sqlFamille);
                        setTitle(nom + "'s Ishyiga " + l(lang, "V_SESSION") + " + Tarif " + stock + " +MODE: " + IJISHO);
                        showProduct();
                        if (color.equals("OK")) {
                            allProductList.setBackground(new Color(tarifa.r, tarifa.g, tarifa.b));
                        }
                    } else {
                        JOptionPane.showMessageDialog(frame, l(lang, "V_RANGIZA") + "  " + l(lang, "V_LE") + " " + l(lang, "V_FACTURE") + " " + l(lang, "V_PROCESS") + " :" + stock, l(lang, "V_ERROR"), JOptionPane.WARNING_MESSAGE);
                    }
                }
            }
        });
    }

    public void t8() {
        t8.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                if (ae.getSource() == t8) {
                    Tarif tarifa = tarif[7];
                    live_Tarif = tarifa.DES_TARIF;
                    observe_Tarif=tarifa.OBSERVE;
                    allow_smart=tarifa.SMART;
                    umukozi = cashier.db.getName(umukozi.ID_EMPLOYE);
                    if (cashier.current.getProductList().size() < 1 || (stock.equals(live_Tarif) && cashier.current.getProductList().size() >= 1)) {
                        v = 1;
                        stock = tarifa.DES_TARIF;
                        live_Tarif = tarifa.DES_TARIF;
                        var = tarifa.VAR_TARIF;
                        cashier.stock(var, sqlFamille);
                        setTitle(nom + "'s Ishyiga " + l(lang, "V_SESSION") + " + Tarif " + stock + " +MODE: " + IJISHO);
                        showProduct();
                        if (color.equals("OK")) {
                            allProductList.setBackground(new Color(tarifa.r, tarifa.g, tarifa.b));
                        }
                    } else {
                        JOptionPane.showMessageDialog(frame, l(lang, "V_RANGIZA") + "  " + l(lang, "V_LE") + " " + l(lang, "V_FACTURE") + " " + l(lang, "V_PROCESS") + " :" + stock, l(lang, "V_ERROR"), JOptionPane.WARNING_MESSAGE);
                    }
                }
            }
        });
    }

    public void t9() {
        t9.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                if (ae.getSource() == t9) {
                    Tarif tarifa = tarif[8];
                    live_Tarif = tarifa.DES_TARIF;
                    observe_Tarif=tarifa.OBSERVE;
                    allow_smart=tarifa.SMART;
                    umukozi = cashier.db.getName(umukozi.ID_EMPLOYE);
                    if (cashier.current.getProductList().size() < 1 || (stock.equals(live_Tarif) && cashier.current.getProductList().size() >= 1)) {
                        v = 1;
                        stock = tarifa.DES_TARIF;
                        live_Tarif = tarifa.DES_TARIF;
                        var = tarifa.VAR_TARIF;
                        cashier.stock(var, sqlFamille);
                        setTitle(nom + "'s Ishyiga " + l(lang, "V_SESSION") + " + Tarif " + stock + " +MODE: " + IJISHO);
                        showProduct();
                        if (color.equals("OK")) {
                            allProductList.setBackground(new Color(tarifa.r, tarifa.g, tarifa.b));
                        }
                    } else {
                        JOptionPane.showMessageDialog(frame, l(lang, "V_RANGIZA") + "  " + l(lang, "V_LE") + " " + l(lang, "V_FACTURE") + " " + l(lang, "V_PROCESS") + " :" + stock, l(lang, "V_ERROR"), JOptionPane.WARNING_MESSAGE);
                    }
                }
            }
        });
    }

    public void t10() {
        t10.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                if (ae.getSource() == t10) {
                    Tarif tarifa = tarif[9];
                    live_Tarif = tarifa.DES_TARIF;
                    observe_Tarif=tarifa.OBSERVE;
                    allow_smart=tarifa.SMART;
                    umukozi = cashier.db.getName(umukozi.ID_EMPLOYE);
                    if (cashier.current.getProductList().size() < 1 || (stock.equals(live_Tarif) && cashier.current.getProductList().size() >= 1)) {
                        v = 1;
                        stock = tarifa.DES_TARIF;
                        live_Tarif = tarifa.DES_TARIF;
                        var = tarifa.VAR_TARIF;
                        cashier.stock(var, sqlFamille);
                        setTitle(nom + "'s Ishyiga " + l(lang, "V_SESSION") + " + Tarif " + stock + " +MODE: " + IJISHO);
                        if (stock.equals("DON")) {
                        }
                        showProduct();
                        if (color.equals("OK")) {
                            allProductList.setBackground(new Color(tarifa.r, tarifa.g, tarifa.b));
                        }
                    } else {
                        JOptionPane.showMessageDialog(frame, l(lang, "V_RANGIZA") + "  " + l(lang, "V_LE") + " " + l(lang, "V_FACTURE") + " " + l(lang, "V_PROCESS") + " :" + stock, l(lang, "V_ERROR"), JOptionPane.WARNING_MESSAGE);
                    }
                }
            }
        });
    }

    /***************************************************************************************************
    AJOUT un produit dans le panier par unite
     **************************************************************************************************/
    void addProductLot(Product p, int qtyCode) {
        
        if (p.check(cashier.howManyProductInSale(p.productCode(), qtyCode)) && qtyCode != -1) {
           
            Product pi = p.clone(qtyCode);
            if (Display) {
                displayUmuti(p, qtyCode, false);
            }
            
            if(cashier.db.dbName.contains("bralirwa") && p.INTERACTION.contains("TRANS"))
            {
            cashier.current.add(pi); 
             Product proc=cashier.db.getProduct("TRANS01", "", "PRIX,");
             proc.qty=qtyCode;
              cashier.current.add(proc);
            }
            else
            {
            cashier.current.add(pi); 
                
            }
            setData();
        } else {
            if (p.qtyLive != -1) {
                JOptionPane.showMessageDialog(frame, p.productName + " " + l(lang, "V_HASIGAYE") + " " + p.qtyLive, l(lang, "V_ERROR"), JOptionPane.WARNING_MESSAGE);
            }
        }
    }

    void addProductProforma(Product p, int qtyCode) {
        if (qtyCode != -1) {
            
             
            
            int qtyLive = cashier.db.getQuantitePerime2(p.productCode,depotSql);
            if (qtyLive < qtyCode) {
                JOptionPane.showMessageDialog(frame, p.productName + " " + l(lang, "V_HASIGAYE") + " " + qtyLive, l(lang, "V_ERROR"), JOptionPane.WARNING_MESSAGE);
            }
            
            Product pi = p.clone(qtyCode);
            if (Display) {
                displayUmuti(p, qtyCode, false);
            }
            cashier.current.add(pi);
              setData();
        } else {
            if (p.qtyLive != -1) {
                JOptionPane.showMessageDialog(frame, p.productName + " " + l(lang, "V_HASIGAYE") + " " + p.qtyLive, l(lang, "V_ERROR"), JOptionPane.WARNING_MESSAGE);
            }
        }
    }

    void addProductPrescription(Product p) {

        JPanel pane = new JPanel();
        pane.setLayout(new GridLayout(3, 1));
        pane.add(new JLabel("AVAILABILITY : " + p.AVAILABILITY));
        JLabel j = new JLabel(p.LASTTIME);
        j.setBackground(p.availColor);
        pane.add(j);
        pane.setBackground(p.availColor);
        JOptionPane.showConfirmDialog(null, pane);
        int qte = getIn(p.productName + "  QTY ??", "  ", 0);
         
        Product pi = p.clone(qte);
        if (Display) {
            displayUmuti(p, qte, false);
        }
        String[] dose = p.DOSAGE.split(";");
        String getDose = (String) JOptionPane.showInputDialog(null, "DOSAGE", " ",
                JOptionPane.QUESTION_MESSAGE, null,
                dose, "");
        if (getDose != null) {
            pi.DOSAGE = getDose;
        } else {
            pi.DOSAGE = JOptionPane.showInputDialog(frame, "DOSAGE " + p.DOSAGE, p.DOSAGE);
        } 
        cashier.current.add(pi); setData(); 
    }

    public void getStock() {
        getStockButton.addActionListener(
                new ActionListener() {

                    public void actionPerformed(ActionEvent ae) {
                        if (ae.getSource() == getStockButton && allProductList.getSelectedValue() != null) {



                            Product p = (Product) allProductList.getSelectedValue();

                            if (p != null) {
                                int stock = 0;

                                String depotSql="";
                                if(umukozi.BP_EMPLOYE!=null && umukozi.BP_EMPLOYE.contains("#"))
                                        { depotSql=" AND ID_LOTS  like '%" +umukozi.BP_EMPLOYE + "%'" ; }  
                                
                                stock = cashier.db.getQuantitePerime2(p.productCode,depotSql);


                                if (stock >= 0) {
                                    JOptionPane.showMessageDialog(frame, p.productName + "  " + l(lang, "V_HASIGAYE") + " " + stock, l(lang, "V_STOCK"), JOptionPane.WARNING_MESSAGE);
                                }
                            }
                        }
                    }
                });
    }

    public void getStockZOOM() {
        getStockButtonZOOM.addActionListener(
                new ActionListener() {

                    public void actionPerformed(ActionEvent ae) {
                        if (ae.getSource() == getStockButtonZOOM && allProductListZOOM.getSelectedValue() != null) {
                            Product p = (Product) allProductListZOOM.getSelectedValue();

                            if (p != null) {
                                int stock = 0;
                                
                                 
                                stock = cashier.db.getQuantitePerime2(p.productCode,depotSql);


                                if (stock >= 0) {
                                    JOptionPane.showMessageDialog(frame, p.productName + "  " + l(lang, "V_HASIGAYE") + " " + stock, l(lang, "V_STOCK"), JOptionPane.WARNING_MESSAGE);
                                }
                            }
                        }
                    }
                });
    }

    public void addProduct() {
        addButton.addActionListener(
                new ActionListener() {

                    public void actionPerformed(ActionEvent ae) {
                        if (ae.getSource() == addButton && allProductList.getSelectedValue() != null) {
                            Product p = (Product) allProductList.getSelectedValue();
                            boolean niSawa = true;

                            if (p != null) {
                                if (!isClinic) {
                                    niSawa = true;
                                } else if (currentClient != null) {
                                    niSawa = (cashier.compatible(p, currentClient));
                                } else {
                                    JOptionPane.showMessageDialog(frame, p.productCode + l(lang,"V_SELECTCLIENT"), l(lang, "V_ERROR"), JOptionPane.WARNING_MESSAGE);
                                    niSawa = false;
                                }
                            } else {
                                JOptionPane.showMessageDialog(frame, p.productCode + l(lang, "V_STOCKCOURRANT"), l(lang, "V_ERROR"), JOptionPane.WARNING_MESSAGE);
                                niSawa = false;
                            }

                            if (niSawa) {
                                if (PRESCRIPTION.isSelected()) {
                                    cashButton.setBackground(Color.magenta);
                                    cashButton.setText("SEND PRE.");
                                    addProductPrescription(p);
                                    System.out.println("ndi prescription");
                                } else {
                                    cashButton.setBackground(Color.GREEN);
                                    cashButton.setText("CASH"); 
                                    ////////RRA
            boolean sdcConnected=true; 
            if(tax.rraClient.isRRA)
            {
                sdcConnected=false;
                if(cashier.current.getProductList().size()>0)
                    sdcConnected=true;
            
            if(!sdcConnected)
            {
                String data =  tax.rraClient.sendRequest("ID REQUEST END INVOICE"," "," ",TIME," ","N","S",0);  
                
                if(data!=null && data.length()>4 && !data.contains("NOT ACCESSIBLE")&& !data.contains("ERROR"))
                {
                    sdcConnected=true;
                    JOptionPane.showMessageDialog(null, l(lang,"V_RRACONNECTION")+data);
                }
                else
                {
                    JOptionPane.showMessageDialog(null, l(lang,"V_RRANOTCONNECTED")+"\n"+ l(lang,"V_VERIFIER") +"\n"+data,"ATTENTION",JOptionPane.WARNING_MESSAGE ); 
                }
            }
            } 
            if(sdcConnected)
            {
            int qte = getQuantity(p.productName + " " + l(lang, "V_ZINGAHE") + " ??", "  ", 0);
            if (IJISHO.contains("PROFORMA")) 
            {
                addProductProforma(p, qte);
            } else 
            {
                addProductLot(p, qte);
            }}}
                            }
                        }
                    }
                });
    }

    public void addProductZOOM() {
        addButtonZOOM.addActionListener(
                new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent ae) {
                        if (ae.getSource() == addButtonZOOM && allProductListZOOM.getSelectedValue() != null) {

                            Product p = (Product) allProductListZOOM.getSelectedValue();
                            if (p != null) {
                                int qte = getQuantity(p.productName + " " + l(lang, "V_ZINGAHE") + " ??", "  ", 0);
 try
     {
         
     String money = JOptionPane.showInputDialog(null,cashier.db.l(cashier.db.lang, "V_PRIXVENTE")); 
     
     Double money3=new Double(money);
     double money4= money3.doubleValue();
     
     p.currentPrice=money4;
     
     if(money4>=0)
     { addProductLot(p, qte);
      cashier.db.insertChangePrice(umukozi.NOM_EMPLOYE,p.productName+" "+stock+" SPECIAL ",money4);
      
     }
    } 
    catch(Throwable ex)
    {
    JOptionPane.showMessageDialog(null,cashier.db. l(cashier.db.lang, "V_ERROR")+" "+cashier.db. l(cashier.db.lang, "V_DANS")+" "+cashier.db. l(cashier.db.lang, "V_INYANDIKO")," Itariki ",JOptionPane.PLAIN_MESSAGE);
    }  
 
                               
                            } else {
                                JOptionPane.showMessageDialog(frame, p.productCode + l(lang, "V_STOCKCOURRANT"), l(lang, "V_ERROR"), JOptionPane.WARNING_MESSAGE);
                            }
                        }
                    }
                });
    }

    /**********************************************************************************************
    Retour Mannuel
     **********************************************************************************************/
    public void retour() {
        venteButton.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                if (ae.getSource() == venteButton && v != 0) {
                    JOptionPane.showMessageDialog(frame, "" + l(lang, "V_CLICK") + "  " + l(lang, "V_CREDIT"), "" + l(lang, "V_NI") + "  " + l(lang, "V_CREDIT"), JOptionPane.PLAIN_MESSAGE);
                }

                if (ae.getSource() == venteButton && v == 0) {
                    //= getQuantity(""+l(lang, "V_NUMERO")+"  "+l(lang, "V_FACTURE"), l(lang, "V_INT"), 0);

                    Invoice inv = (Invoice) JOptionPane.showInputDialog(null, l(lang, "V_OPTIONS"), l(lang, "V_IMPRESSION"),
                            JOptionPane.QUESTION_MESSAGE, null, cashier.db.printByChoice("PAR HEURE","S"), "");
                    int facture = inv.id_invoice;

                    TOTALPRICE2 = cashier.db.getTotal(facture); 
                    //int kuramo = getQuantity(l(lang, "V_RETOURPRODUIT"), l(lang, "V_INT"), 0);


                    //int retour = cashier.db.retour(facture, kuramo, nom);
                    int retour = cashier.db.retour(facture, nom,tax.rraClient,inv.date,inv.id_client,inv.CASH,inv.CREDIT);
                    if (retour > 0) 
                    {
//                        JOptionPane.showMessageDialog(frame, l(lang, "V_FACTURE") +" "+facture+" "+ l(lang, "V_ANNULEES"), l(lang, "V_MERCI"), JOptionPane.PLAIN_MESSAGE);
//                         
                        
                        
//                        totalprice = totalprice - retour;
//                        cashier.current.id_client = "CASHNORM";
//
//                        int money = getAmafaranga(l(lang, "V_TOTAL") + " : " + RRA_PRINT2.setVirgule(totalprice), l(lang, "V_INT"), 0);
//                        int rest = money - totalprice;
//                        // Si c est pas assez d argent
//                        boolean done = true;
//                        if (rest >= 0 && rest < 5000) {
//                            done = false;
//                        }
//                        while (money != -1 && done) {
//                            if (money >= 0 && rest < 0) {
//                                money = getAmafaranga(money + " " + l(lang, "V_NTIHAGIJE") + "  ... " + l(lang, "V_TOTAL") + " : " + RRA_PRINT2.setVirgule(totalprice), l(lang, "V_INT"), 0);
//                            }
//                            if (rest >= 5000) {
//                                money = getAmafaranga(money + " " + l(lang, "V_NIMENSHI") + " ...  " + l(lang, "V_TOTAL") + " : " + RRA_PRINT2.setVirgule(totalprice), l(lang, "V_INT"), 0);
//                            }
//
//                            rest = money - totalprice;
//                            if (rest >= 0 && rest < 5000) {
//                                done = false;
//                            }
//                        }
//
//                        if (rest >= 0 && money != -1 && rest < 5000) {
//
//                            // Mise a jour du stock , du prix et du panier
//
////        cashier.upDateStock();
//                            String EstInStock = cashier.estDansStock();
//                            if (EstInStock.equals("")) {
//                                cashier.current.total();
//                                cashier.current.payed = totalprice;
//                                cashier.current.tot = totalprice;
//                                Cashier.yose = Cashier.yose + totalprice;
//                                cashier.current.tva = tvaPrice;
//                                PrintR(money, rest, tvaPrice, retour);
//                                tangira(rest);
//
//                                /////////////////////////////////////////////////
//
//
//                                JOptionPane.showMessageDialog(frame, l(lang, "V_SUBIZA") + rest, l(lang, "V_MERCI"), JOptionPane.PLAIN_MESSAGE);
//                            } else {
//                                JOptionPane.showMessageDialog(frame, EstInStock + " " + l(lang, "V_PRODUCTISTAKEN"), l(lang, "V_ATTENTION") + " !!! ", JOptionPane.QUESTION_MESSAGE);
//                            }
//                        }
                    }
                }
            }
        });
    }
    
    public void ADD_DATA() {
        ADD_DATA.addActionListener(
                new ActionListener() {

                    public void actionPerformed(ActionEvent ae) {
                        if (ae.getSource() == ADD_DATA) { 
                                new clinic.Interface_Patient(cashier);  
                        }

                    }
                });
    }
    

    public void addSentProduct(String sentText) {
        int s = cashier.allProduct.size();
        Product p = null;
     //   String productCode = 0;
        try {
            if (sentText != null && sentText.equals("")) {
                JOptionPane.showMessageDialog(frame, sentText + l(lang, "V_NTABWO") + "  " + l(lang, "V_CODE"), l(lang, "V_ERROR"), JOptionPane.WARNING_MESSAGE);
            } else {
                Integer in_int = new Integer(sentText);
             //   productCode = in_int.intValue();
                for (int i = 0; i < s; i++) {

                    Product p1 = cashier.allProduct.get(i);
//                    if (p1.productCode == productCode) 
//                    {
//                        p = p1;
//                    }
                }
            }
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(frame, sentText + l(lang, "V_NTABWO") + "  " + l(lang, "V_CODE"), l(lang, "V_ERROR"), JOptionPane.WARNING_MESSAGE);
        }


        // on recupere le produit correspondant



        if (p != null) {
            int qtyCode = getQuantity(p.productName + " " + l(lang, "V_ZINGAHE"), l(lang, "V_INT"), 0);

            if (p.check(cashier.howManyProductInSale(p.productCode(), qtyCode)) && qtyCode != -1) {
             
                Product pi = p.clone(qtyCode);

                // On ajoute le produit dans le panier
                cashier.current.add(pi);
                setData();
            } else {
                JOptionPane.showMessageDialog(frame, p.productName + " " + l(lang, "V_HASIGAYE") + " " + p.qtyLive, l(lang, "V_ERROR"), JOptionPane.WARNING_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(frame,  l(lang, "V_STOCKCOURRANT"), l(lang, "V_ERROR"), JOptionPane.WARNING_MESSAGE);
        }
    }

    /**********************************************************************************************
    AJOUT Mannuel
     **********************************************************************************************/
    public void sendProduct() {
        sendButton.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                if (ae.getSource() == sendButton) {
                    // On recupere le code du produit
                    String sentText = texttoBESent.getText();
                    addSentProduct(sentText);
                }
            }
        });
    }

    /**********************************************************************************************
    GESTION DU STOCK OUVERTURE DU FENETRE BUTTON
     **********************************************************************************************/
    public void stockManager() {
        actButton.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                if (ae.getSource() == actButton) {

                    if (umukozi.PWD_EMPLOYE.contains("CHANGER_PRIX")) {
                        new Data(cashier, stock, var, today);
                    } else {
                        JOptionPane.showConfirmDialog(null, l(lang, "V_AUTORISATION"), l(lang, "V_PERMISSION"), JOptionPane.YES_NO_OPTION);
                    }



                }
            }
        });

    }

    /**********************************************************************************************
    ACTUALIZE le stock
     **********************************************************************************************/
    public void actualise() {
        stockButton.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent ae) {
                        if (ae.getSource() == stockButton && (umukozi.PWD_EMPLOYE.contains("DATA")||umukozi.LEVEL_EMPLOYE>10)) {

                            new VoirFichier(umukozi, serv, dataBase, tax);

                        } else {
                            JOptionPane.showConfirmDialog(null, l(lang, "V_AUTORISATION"), l(lang, "V_PERMISSION"), JOptionPane.YES_NO_OPTION);
                        }
                    }
                });

    }

    LinkedList<String> trouve(LinkedList<Product> product, String find) {

        LinkedList<String> trouve = new LinkedList();
        LinkedList<String> trouver = new LinkedList();
        Product p1 = null;
        double p2 = 1000000;
        int p3 = -1;
        String resString = "";

        StringTokenizer st1 = new StringTokenizer(find);
        while (st1.hasMoreTokens()) {
            trouve.add(st1.nextToken(" "));
        }

        for (int i = 0; i < cashier.allProduct.size(); i++) {
            Product p = cashier.allProduct.get(i);
            int j = 0;
            while (j < trouve.size() && (p.productName.toUpperCase()).contains(trouve.get(j).toUpperCase())) {
                j++;
            }
            
            if (j == trouve.size()) {
                resString = resString + "\n CODE = " + p.productCode + " NAME : " + p.productName + " PRICE =" + p.currentPrice;
                p1 = p;
                if (p.currentPrice < p2) {
                    p3 = p.productCode;
                    p2 = p.currentPrice;
                }
            }

        }
        trouver.add(resString);
        trouver.add("" + p3);
        return trouver;
    }
    
 void superSearch ( String find) { 
LinkedList<Product> res = new LinkedList();
String [] splited =find.split(" ");
//System.out.println(find+cashier.allProduct.size());
for (int i = 0; i < cashier.allProduct.size(); i++) {
Product p = cashier.allProduct.get(i);
int trouve=0;
for(int j=0;j<splited.length;j++)
{
    String findo= splited[j];
if (p.contains(findo)) 
{ 
trouve++;
} 
else {
        break;
    }
}
if(trouve==splited.length) {
        res.add(p);
    }
}  
allProductList.setListData(res.toArray()); 
    }
    
    void superSearchPatient(String find) {

        LinkedList<Client> res = new LinkedList();
        find = find.replaceAll(" ", "");
        for (int i = 0; i < cashier.allClient.size(); i++) {
            Client p = cashier.allClient.get(i);
            if (p.NOM_CLIENT.toUpperCase().contains(find)) {
                res.add(p);
            }
        }
        int c = res.size();
        Client[] control = new Client[c];
        for (int i = 0; i < c; i++) {
            Client p = res.get(i);
            control[i] = p;
        }
        lesClients.setListData(control);

    }

    void superSearchZOOM(String find) {

        LinkedList<Product> res = new LinkedList();
        find = find.replaceAll(" ", "");
        for (int i = 0; i < cashier.db.allProductall.size(); i++) {
            Product p = cashier.db.allProductall.get(i);
            if (p.productName.toUpperCase().contains(find)) {
                res.add(p);
            }
        }
        int c = res.size();
        Product[] control = new Product[c];
        for (int i = 0; i < c; i++) {
            Product p = res.get(i);
            control[i] = p;
        }
        allProductListZOOM.setListData(control);

    }

    public void allProductListEcoute() {
        radioSpecial.addMouseListener(new MouseListener() {

            public void actionPerformed(ActionEvent ae) {
            }

            public void mouseExited(MouseEvent ae) {
            }

            public void mouseClicked(MouseEvent e) {

                if (!radioSpecial.isSelected() ) {
                    
                    isSpecial=false;}
                
                if (radioSpecial.isSelected() && umukozi.PWD_EMPLOYE.contains("CHANGER_PRIX")) {
                    
                    isSpecial=true;
                    
                     int nn = JOptionPane.showConfirmDialog(frame,
                             " DCI LIST ?" ,l(lang, "V_FICHE"), JOptionPane.YES_NO_OPTION);
                     
                    if(nn==0)
                    {
                        cashier.db.doworkOtherAll2(var); 
                    }
                    else
                    {
                        cashier.db.doworkOtherAll(var);
                    } 
                    
                    allProductListZOOM.setListData(cashier.db.allProductall.toArray()); 
                    System.out.println(cashier.db.allProductall.size());
                    allProductListZOOM.setBackground(Color.pink);
                    JOptionPane.showConfirmDialog(null, westPaneZoom());
                }
            } 
            ;  
            public void mousePressed(MouseEvent e) {   } 
            public void mouseReleased(MouseEvent e) {  }
            public void mouseEntered(MouseEvent e) {  }
        });
    }

    public void searchZOOMIT() {
        searchZOOM.addKeyListener(
                new KeyListener() {

                    public void actionPerformed(ActionEvent ae) {
                    }

                    public void keyTyped(KeyEvent e) {

                        String cc = "" + e.getKeyChar();
                        String find = (searchZOOM.getText() + cc).toUpperCase();
                        if (cc.hashCode() == 8 && find.length() > 0) {
                            find = (find.substring(0, find.length() - 1));
                        }
                       superSearchZOOM(find);
                    }

                    public void keyPressed(KeyEvent e) {
                    }

                    public void keyReleased(KeyEvent e) {
                    }
                });
    }

    public void superSearchIt() {
        search.addKeyListener(
                new KeyListener() {

                    public void actionPerformed(ActionEvent ae) {
                    }

                    public void keyTyped(KeyEvent e) {

                        String cc = "" + e.getKeyChar();
                        String find = (search.getText() + cc).toUpperCase();
                        if (cc.hashCode() == 8 && find.length() > 0) {
                            find = (find.substring(0, find.length() - 1));
                        }
                        superSearch(find);
                    }

                    public void keyPressed(KeyEvent e) {
                    }

                    public void keyReleased(KeyEvent e) {
                    }
                });

    }

    public void superSearchAff() {
        searchPatient.addKeyListener(
                new KeyListener() {

                    public void actionPerformed(ActionEvent ae) {
                    }

                    public void keyTyped(KeyEvent e) {

                        String cc = "" + e.getKeyChar();
                        String find = (searchPatient.getText() + cc).toUpperCase();
                        if (cc.hashCode() == 8 && find.length() > 0) {
                            find = (find.substring(0, find.length() - 1));
                        }
                        superSearchPatient(find);
                    }

                    public void keyPressed(KeyEvent e) {
                    }

                    public void keyReleased(KeyEvent e) {
                    }
                });

    }
//
//    /**********************************************************************************************
//    VOIR Fiche de stock
//     **********************************************************************************************/
//    public void search() {
//        searchButton.addActionListener(
//                new ActionListener() {
//
//                    public void actionPerformed(ActionEvent ae) {
//                        if (ae.getSource() == searchButton && search.getText().length() > 2) {
//                            //Soie sertie nÃ‚Â°2 + aig. ronde 1 mg 2 ce 
//
//
//                            String s = search.getText();
//                            LinkedList<String> trouve = trouve(cashier.allProduct, s);
//                            if (!trouve.get(1).equals("-1")) {
//                                String sentText = JOptionPane.showInputDialog(frame, trouve.getFirst(), trouve.get(1));
//
//                                if (sentText != null) {
//                                    addSentProduct(sentText);
//                                }
//                            } else {
//                                JOptionPane.showConfirmDialog(frame, s + " " + l(lang, "V_NTAWE"), l(lang, "V_IBISA") + s, JOptionPane.YES_NO_OPTION);
//                            }
//                        }
//                    }
//                });
//    }

    /**********************************************************************************************
    GESTION des produits VEDETTES BUTTON
     **********************************************************************************************/
    public void b1() {
        b1.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                if (ae.getSource() == b1) {
                    
                    boolean sdcConnected=true; 
            if(tax.rraClient.isRRA)
            {
                sdcConnected=false;
                if(cashier.current.getProductList().size()>0)
                    sdcConnected=true;
            
            if(!sdcConnected)
            {
               // String data =  tax.rraClient.sendRequest("ID REQUEST"," "," ",TIME," ","N","S",0);  
                String data =  Main.tax.rraClient.sendRequest("ID REQUEST END INVOICE"," "," ",""," ","N","S",0);
                if(data!=null && data.length()>4 && !data.contains("NOT ACCESSIBLE")&& !data.contains("ERROR"))
                {
                    sdcConnected=true;
                    JOptionPane.showMessageDialog(null, l(lang,"V_RRACONNECTION")+data);
                }
                else
                {
                    JOptionPane.showMessageDialog(null, l(lang,"V_RRANOTCONNECTED")+"\n"+ l(lang,"V_VERIFIER") +"\n"+data,"ATTENTION",JOptionPane.WARNING_MESSAGE ); 
                }
            }
            
            } 
            if(sdcConnected)
            {
                addOneVedette(i1);
            }
                }
            }
        });
    }

    public void b2() {
        b2.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                if (ae.getSource() == b2) {
                    boolean sdcConnected=true; 
            if(tax.rraClient.isRRA)
            {
                sdcConnected=false;
                if(cashier.current.getProductList().size()>0)
                    sdcConnected=true;
            
            if(!sdcConnected)
            {
String data =  tax.rraClient.sendRequest("ID REQUEST"," "," ",TIME," ","N","S",0);  
                
                if(data!=null && data.length()>4 && !data.contains("NOT ACCESSIBLE")&& !data.contains("ERROR"))
                {
                    sdcConnected=true;
                    JOptionPane.showMessageDialog(null, l(lang,"V_RRACONNECTION")+data);
                }
                else
                {
                    JOptionPane.showMessageDialog(null, l(lang,"V_RRANOTCONNECTED")+" \n"+  l(lang,"V_VERIFIER") +"\n " +data,"ATTENTION",JOptionPane.WARNING_MESSAGE ); 
                }
            }
            
            } 
            if(sdcConnected)
            {
                addOneVedette(i2);
            }
                }
            }
        });
    }

    public void b3() {
        b3.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                if (ae.getSource() == b3) {
                    boolean sdcConnected=true; 
            if(tax.rraClient.isRRA)
            {
                sdcConnected=false;
                if(cashier.current.getProductList().size()>0)
                    sdcConnected=true;
            
            if(!sdcConnected)
            {
                String data =  tax.rraClient.sendRequest("ID REQUEST"," "," ",TIME," ","N","S",0);  
                
                if(data!=null && data.length()>4 && !data.contains("NOT ACCESSIBLE")&& !data.contains("ERROR"))
                {
                    sdcConnected=true;
                    JOptionPane.showMessageDialog(null, l(lang,"V_RRACONNECTION")+data);
                }
                else
                {
                    JOptionPane.showMessageDialog(null, l(lang,"V_RRANOTCONNECTED")+" \n"+  l(lang,"V_VERIFIER") +"\n " +data,"ATTENTION",JOptionPane.WARNING_MESSAGE ); 
                }
            }
            
            } 
            if(sdcConnected)
            {
                addOneVedette(i3);
            }
                }
            }
        });
    }

    public void b4() {
        b4.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                if (ae.getSource() == b4) {
                    boolean sdcConnected=true; 
            if(tax.rraClient.isRRA)
            {
                sdcConnected=false;
                if(cashier.current.getProductList().size()>0)
                    sdcConnected=true;
            
            if(!sdcConnected)
            {
                String data =  tax.rraClient.sendRequest("ID REQUEST"," "," ",TIME," ","N","S",0);  
                
                if(data!=null && data.length()>4 && !data.contains("NOT ACCESSIBLE")&& !data.contains("ERROR"))
                {
                    sdcConnected=true;
                    JOptionPane.showMessageDialog(null, l(lang,"V_RRACONNECTION")+data);
                }
                else
                {
                    JOptionPane.showMessageDialog(null, l(lang,"V_RRANOTCONNECTED")+" \n"+  l(lang,"V_VERIFIER") +"\n " +data,"ATTENTION",JOptionPane.WARNING_MESSAGE ); 
                }
            }
            
            } 
            if(sdcConnected)
            {
                addOneVedette(i4);
            }
                }
            }
        });
    }

    public void b5() {
        b5.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                if (ae.getSource() == b5) {
                    boolean sdcConnected=true; 
            if(tax.rraClient.isRRA)
            {
                sdcConnected=false;
                if(cashier.current.getProductList().size()>0)
                    sdcConnected=true;
            
            if(!sdcConnected)
            {
                String data =  tax.rraClient.sendRequest("ID REQUEST"," "," ",TIME," ","N","S",0);  
                
                if(data!=null && data.length()>4 && !data.contains("NOT ACCESSIBLE")&& !data.contains("ERROR"))
                {
                    sdcConnected=true;
                    JOptionPane.showMessageDialog(null, l(lang,"V_RRACONNECTION")+data);
                }
                else
                {
                    JOptionPane.showMessageDialog(null, l(lang,"V_RRANOTCONNECTED")+" \n"+  l(lang,"V_VERIFIER") +"\n " +data,"ATTENTION",JOptionPane.WARNING_MESSAGE ); 
                }
            }
            
            } 
            if(sdcConnected)
            {
                addOneVedette(i5);
            }
                }
            }
        });
    }

    public void b6() {
        b6.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                if (ae.getSource() == b6) {
                    boolean sdcConnected=true; 
            if(tax.rraClient.isRRA)
            {
                sdcConnected=false;
                if(cashier.current.getProductList().size()>0)
                    sdcConnected=true;
            
            if(!sdcConnected)
            {
                String data =  tax.rraClient.sendRequest("ID REQUEST"," "," ",TIME," ","N","S",0);  
                
                if(data!=null && data.length()>4 && !data.contains("NOT ACCESSIBLE")&& !data.contains("ERROR"))
                {
                    sdcConnected=true;
                    JOptionPane.showMessageDialog(null, l(lang,"V_RRACONNECTION")+data);
                }
                else
                {
                    JOptionPane.showMessageDialog(null, l(lang,"V_RRANOTCONNECTED")+" \n"+  l(lang,"V_VERIFIER") +"\n " +data,"ATTENTION",JOptionPane.WARNING_MESSAGE ); 
                }
            }            
            } 
            if(sdcConnected)
            {
                addOneVedette(i6);
            }
                }
            }
        });
    }

    public void b7() {
        b7.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                if (ae.getSource() == b7) {
                    boolean sdcConnected=true; 
            if(tax.rraClient.isRRA)
            {
                sdcConnected=false;
                if(cashier.current.getProductList().size()>0)
                    sdcConnected=true;
            
            if(!sdcConnected)
            {
                String data =  tax.rraClient.sendRequest("ID REQUEST"," "," ",TIME," ","N","S",0);  
                
                if(data!=null && data.length()>4 && !data.contains("NOT ACCESSIBLE")&& !data.contains("ERROR"))
                {
                    sdcConnected=true;
                    JOptionPane.showMessageDialog(null, l(lang,"V_RRACONNECTION")+data);
                }
                else
                {
                    JOptionPane.showMessageDialog(null, l(lang,"V_RRANOTCONNECTED")+" \n"+  l(lang,"V_VERIFIER") +"\n " +data,"ATTENTION",JOptionPane.WARNING_MESSAGE ); 
                }
            }
            
            } 
            if(sdcConnected)
            {
                addOneVedette(i7);
            }
                }
            }
        });
    }

    public void b8() {
        b8.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                if (ae.getSource() == b8) {
                    boolean sdcConnected=true; 
            if(tax.rraClient.isRRA)
            {
                sdcConnected=false;
                if(cashier.current.getProductList().size()>0)
                    sdcConnected=true;
            
            if(!sdcConnected)
            {
                String data =  tax.rraClient.sendRequest("ID REQUEST"," "," ",TIME," ","N","S",0);  
                
                if(data!=null && data.length()>4 && !data.contains("NOT ACCESSIBLE")&& !data.contains("ERROR"))
                {
                    sdcConnected=true;
                    JOptionPane.showMessageDialog(null, l(lang,"V_RRACONNECTION")+data);
                }
                else
                {
                    JOptionPane.showMessageDialog(null, l(lang,"V_RRANOTCONNECTED")+" \n"+  l(lang,"V_VERIFIER") +"\n " +data,"ATTENTION",JOptionPane.WARNING_MESSAGE ); 
                }
            }
            
            } 
            if(sdcConnected)
            {
                addOneVedette(i8);
            }
                }
            }
        });
    }

    public void b9() {
        b9.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                if (ae.getSource() == b9) {
                    boolean sdcConnected=true; 
            if(tax.rraClient.isRRA)
            {
                sdcConnected=false;
                if(cashier.current.getProductList().size()>0)
                    sdcConnected=true;
            
            if(!sdcConnected)
            {
                String data =  tax.rraClient.sendRequest("ID REQUEST"," "," ",TIME," ","N","S",0);  
                
                if(data!=null && data.length()>4 && !data.contains("NOT ACCESSIBLE")&& !data.contains("ERROR"))
                {
                    sdcConnected=true;
                    JOptionPane.showMessageDialog(null, l(lang,"V_RRACONNECTION")+data);
                }
                else
                {
                    JOptionPane.showMessageDialog(null, l(lang,"V_RRANOTCONNECTED")+" \n"+  l(lang,"V_VERIFIER") +"\n " +data,"ATTENTION",JOptionPane.WARNING_MESSAGE ); 
                }
            }
            
            } 
            if(sdcConnected)
            {
                addOneVedette(i9);
            }
                }
            }
        });
    }

    public void b10() {
        b10.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                if (ae.getSource() == b10) {
                    boolean sdcConnected=true; 
            if(tax.rraClient.isRRA)
            {
                sdcConnected=false;
                if(cashier.current.getProductList().size()>0)
                    sdcConnected=true;
            
            if(!sdcConnected)
            {
                String data =  tax.rraClient.sendRequest("ID REQUEST"," "," ",TIME," ","N","S",0);  
                
                if(data!=null && data.length()>4 && !data.contains("NOT ACCESSIBLE")&& !data.contains("ERROR"))
                {
                    sdcConnected=true;
                    JOptionPane.showMessageDialog(null, l(lang,"V_RRACONNECTION")+data);
                }
                else
                {
                    JOptionPane.showMessageDialog(null, l(lang,"V_RRANOTCONNECTED")+" \n"+  l(lang,"V_VERIFIER") +"\n " +data,"ATTENTION",JOptionPane.WARNING_MESSAGE ); 
                }
            }
            
            } 
            if(sdcConnected)
            {
                addOneVedette(i10);
            }
                }
            }
        });
    }

    public void b11() {
        b11.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                if (ae.getSource() == b11) {
                    boolean sdcConnected=true; 
            if(tax.rraClient.isRRA)
            {
                sdcConnected=false;
                if(cashier.current.getProductList().size()>0)
                    sdcConnected=true;
            
            if(!sdcConnected)
            {
                String data =  tax.rraClient.sendRequest("ID REQUEST"," "," ",TIME," ","N","S",0);  
                
                if(data!=null && data.length()>4 && !data.contains("NOT ACCESSIBLE")&& !data.contains("ERROR"))
                {
                    sdcConnected=true;
                    JOptionPane.showMessageDialog(null, l(lang,"V_RRACONNECTION")+data);
                }
                else
                {
                    JOptionPane.showMessageDialog(null, l(lang,"V_RRANOTCONNECTED")+" \n"+  l(lang,"V_VERIFIER") +"\n " +data,"ATTENTION",JOptionPane.WARNING_MESSAGE ); 
                }
            }
            
            } 
            if(sdcConnected)
            {
                addOneVedette(i11);
            }
                }
            }
        });
    }

    public void b12() {
        b12.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                if (ae.getSource() == b12) {
                    boolean sdcConnected=true; 
            if(tax.rraClient.isRRA)
            {
                sdcConnected=false;
                if(cashier.current.getProductList().size()>0)
                    sdcConnected=true;
            
            if(!sdcConnected)
            {
                String data =  tax.rraClient.sendRequest("ID REQUEST"," "," ",TIME," ","N","S",0);  
                
                if(data!=null && data.length()>4 && !data.contains("NOT ACCESSIBLE")&& !data.contains("ERROR"))
                {
                   sdcConnected=true;
                    JOptionPane.showMessageDialog(null, l(lang,"V_RRACONNECTION")+data);
                }
                else
                {
                    JOptionPane.showMessageDialog(null, l(lang,"V_RRANOTCONNECTED")+" \n"+  l(lang,"V_VERIFIER") +"\n " +data,"ATTENTION",JOptionPane.WARNING_MESSAGE ); 
                }
            }
            
            } 
            if(sdcConnected)
            {
                addOneVedette(i12);
            }
                }
            }
        });
    }

    public void un() {
        unVedette.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                if (ae.getSource() == unVedette) {
                    if (isClinic) {
                        sqlFamille = " ";
                        cashier.stock(var, sqlFamille);
                        showProduct();
                        labelProduit.setText(getString("unVedette"));
                    } else {
                        setVedette(1);
                    }
                }
            }
        });
    }

    public void deux() {
        deuxVedette.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                if (ae.getSource() == deuxVedette) {
                    if (isClinic) {
                        sqlFamille = " AND FAMILLE LIKE '%" + getString2("deuxVedette") + "%'";
                        cashier.stock(var, sqlFamille);
                        showProduct();
                        labelProduit.setText(getString("deuxVedette"));
                    } else {
                        setVedette(2);
                    }

                }
            }
        });
    }

    public void trois() {
        troisVedette.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                if (ae.getSource() == troisVedette) {
                    if (isClinic) {
                        sqlFamille = " AND FAMILLE LIKE '%" + getString2("troisVedette") + "%'";
                        cashier.stock(var, sqlFamille);
                        showProduct();
                        labelProduit.setText(getString("troisVedette"));
                    } else {
                        setVedette(3);
                    }

                }
            }
        });
    }

    public void quatre() {
        quatreVedette.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                if (ae.getSource() == quatreVedette) {
                    if (isClinic) {
                        sqlFamille = " AND FAMILLE LIKE '%" + getString2("quatreVedette") + "%'";
                        cashier.stock(var, sqlFamille);
                        showProduct();
                        labelProduit.setText(getString("quatreVedette"));

                    } else {
                        setVedette(4);
                    }
                }
            }
        });
    }

    public void cinq() {
        cinqVedette.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                if (ae.getSource() == cinqVedette) {
                    if (isClinic) {
                        sqlFamille = " AND FAMILLE LIKE '%" + getString2("cinqVedette") + "%'";
                        cashier.stock(var, sqlFamille);
                        showProduct();
                        labelProduit.setText(getString("cinqVedette"));

                    } else {
                        setVedette(5);
                    }
                }
            }
        });
    }

    public void six() {
        sixVedette.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                if (ae.getSource() == sixVedette) {
                    if (isClinic) {
                        sqlFamille = " AND FAMILLE LIKE '%" + getString2("sixVedette") + "%'";
                        cashier.stock(var, sqlFamille);
                        showProduct();
                        labelProduit.setText(getString("sixVedette"));
                           sixVedette.setBackground(Color.orange);
                    } else {
                        setVedette(6);
                    }
                }
            }
        });
    }

    public void sept() {
        septVedette.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                if (ae.getSource() == septVedette) {
                    if (isClinic) {
                        sqlFamille = " AND FAMILLE LIKE '%" + getString2("septVedette") + "%'";
                        cashier.stock(var, sqlFamille);
                        showProduct();
                        labelProduit.setText(getString("septVedette"));

                    } else {
                        setVedette(7);
                    }
                }
            }
        });
    }

    public void huit() {
        huitVedette.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                if (ae.getSource() == huitVedette) {
                    if (isClinic) {

                        sqlFamille = " AND FAMILLE LIKE '%" + getString2("huitVedette") + "%'";
                        cashier.stock(var, sqlFamille);
                        showProduct();
                        labelProduit.setText(getString("huitVedette"));

                    } else {
                        showProduct();
                    }
                }
                setVedette(8);
            }
        });
    }

    public void displayUmuti(Product p, int qte, boolean vedette) {
        String umuti = p.productName;
        System.out.println(umuti + ":umuti length:" + umuti.length());
        if (umuti.length() > 20) {
            umuti = p.productName.substring(0, 19);
        } else if (umuti.length() < 20) {
            for (int i = umuti.length(); i < 19; i++) {
                umuti = umuti + " ";
            }
        }


        if (Display) {
            try {


                String igiciro;
                if (vedette == true) {
                    igiciro = "" + qte + " = " + RRA_PRINT2.setVirgule((qte * p.currentPrice),1, ",") + " " + dev;
                } else {
                    igiciro = "" + qte + " = " + RRA_PRINT2.setVirgule((qte * p.salePrice()),1, ",") + " " + dev;
                }

                if (igiciro.length() < 20) {
                    for (int i = igiciro.length(); i < 19; i++) {
                        igiciro = igiciro + " ";
                    }
                }

                System.out.println(umuti);
                outputStream.write(26);
                outputStream.write((umuti).getBytes());
                outputStream.write(10);
                outputStream.write(13);
                outputStream.write((igiciro + " ").getBytes());
                //Thread.sleep(100); 
            } catch (Exception ex) {
                System.out.println("" + ex);
            }
        }
    }

    public void displayMsg(String up, String down) {
        if (Display) {
            try {
                if (up.length() < 20) {
                    for (int i = up.length(); i < 19; i++) {
                        up = up + " ";
                    }
                }
                if (down.length() < 20) {
                    for (int i = down.length(); i < 19; i++) {
                        down = down + " ";
                    }
                }

                outputStream.write(26);
                outputStream.write((up).getBytes());
                outputStream.write(10);
                outputStream.write(13);
                outputStream.write((down).getBytes());
                //Thread.sleep(100); 
            } catch (Exception ex) {
                System.out.println("" + ex);
            }
        }
    }

    /**********************************************************************************************
    AJOUT dans le BASKET par son code
     **********************************************************************************************/
    public void addOneVedette(int pr) {
        Product pp = cashier.getProductVedette(pr);

        Product p = (pp).clone(pp.qty);
        int qte = pp.qty;
        if (p != null && p.currentPrice > 0) {
            if (p.check(cashier.howManyProductInSale(p.productCode(), qte))) {
               
                displayUmuti(p, qte, true);
                
                 if(cashier.db.dbName.contains("bralirwa") && p.INTERACTION.contains("TRANS"))
            {
             cashier.current.add(p); 
             Product proc=cashier.db.getProduct("TRANS01", "", "PRIX,");
             proc.qty=qte;
              cashier.current.add(proc);
            }
              else  
                 {
                     cashier.current.add(p);
                 } 
                
                
                setData();
            } else {
                JOptionPane.showMessageDialog(frame, p.productName + " " + l(lang, "V_HASIGAYE") + " " + p.qtyLive, l(lang, "V_ERROR"), JOptionPane.WARNING_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(frame, pr + " " + l(lang, "V_NTIBONEKA"), l(lang, "V_ERROR"), JOptionPane.WARNING_MESSAGE);
        }

    }

    /**********************************************************************************************
    AJOUT dans le BASKET par son code
     **********************************************************************************************/
    
    public void addOne(int pr) {
        int s = cashier.allProduct.size();
        Product p = null;
        for (int i = 0; i < s; i++) {

            Product p1 = cashier.allProduct.get(i);
            if (p1.productCode == pr) {
                p = p1.clone(1);
            }
        }


        if (p.check(cashier.howManyProductInSale(p.productCode(), 1))) {
            
            cashier.current.add(p); setData();
 
        } else {
            JOptionPane.showMessageDialog(frame, p.productName + " " + l(lang, "V_HASIGAYE") + " " + p.qtyLive, l(lang, "V_ERROR"), JOptionPane.WARNING_MESSAGE);
        }
    }

    /**********************************************************************************************
    CHANGEMENT DE li Interface
     **********************************************************************************************/
    public void setVedette(int k) {
        this.k = k;
        Vedette[] c = new Vedette[13];
        int j = 1;
        for (int i = 0; i < cashier.vedette.size(); i++) {
            Vedette v2 = (Vedette) cashier.vedette.get(i);
            if (v2.cat == k) {
                c[j] = v2 ;
                j++;
            }
        } 
        
if(vedetteImage)
{ 
 b1.setText("<html>  "+c[1].img()+"   </html>"); 
 System.out.println("<html>  "+c[1].img()+"   </html>");
 b1.setToolTipText(cashier.getProductVedetteName(c[1].ID_PRODUCT));
 b1.setBackground(Color.white);
  b2.setText("<html>  "+c[2].img()+"   </html>"); 
 b2.setToolTipText(cashier.getProductVedetteName(c[2].ID_PRODUCT));
  b2.setBackground(Color.white);
  b3.setText("<html>  "+c[3].img()+"   </html>"); 
 b3.setToolTipText(cashier.getProductVedetteName(c[3].ID_PRODUCT));
  b3.setBackground(Color.white);
  b4.setText("<html>  "+c[4].img()+"   </html>"); 
 b4.setToolTipText(cashier.getProductVedetteName(c[4].ID_PRODUCT));
  b4.setBackground(Color.white);
  b5.setText("<html>  "+c[5].img()+"   </html>"); 
 b5.setToolTipText(cashier.getProductVedetteName(c[5].ID_PRODUCT));
  b5.setBackground(Color.white);
  b6.setText("<html>  "+c[6].img()+"   </html>"); 
 b6.setToolTipText(cashier.getProductVedetteName(c[6].ID_PRODUCT));
  b6.setBackground(Color.white);
  b7.setText("<html>  "+c[7].img()+"   </html>"); 
 b7.setToolTipText(cashier.getProductVedetteName(c[7].ID_PRODUCT));
  b8.setText("<html>  "+c[8].img()+"   </html>"); 
 b8.setToolTipText(cashier.getProductVedetteName(c[8].ID_PRODUCT));
  b9.setText("<html>  "+c[9].img()+"   </html>"); 
 b9.setToolTipText(cashier.getProductVedetteName(c[9].ID_PRODUCT));
  b10.setText("<html>  "+c[10].img()+"   </html>"); 
 b10.setToolTipText(cashier.getProductVedetteName(c[10].ID_PRODUCT));
  b11.setText("<html>  "+c[11].img()+"   </html>"); 
 b11.setToolTipText(cashier.getProductVedetteName(c[11].ID_PRODUCT));
   b12.setText("<html>  "+c[12].img()+"   </html>"); 
 b12.setToolTipText(cashier.getProductVedetteName(c[12].ID_PRODUCT)); 

 
 b7.setBackground(Color.white);
 b8.setBackground(Color.white);
 b9.setBackground(Color.white);
 b10.setBackground(Color.white);
 b11.setBackground(Color.white);
 b12.setBackground(Color.white); 
 
}
        else
        {
      
        b1.setText("<html><h3><b> " + cashier.getProductVedetteName(c[1].ID_PRODUCT) + "</h3> </html>");
        b2.setText("<html><h3><b> " + cashier.getProductVedetteName(c[2].ID_PRODUCT) + "</h3> </html>");
        b3.setText("<html><h3><b> " + cashier.getProductVedetteName(c[3].ID_PRODUCT) + "</h3> </html>");
        b4.setText("<html><h3><b> " + cashier.getProductVedetteName(c[4].ID_PRODUCT) + "</h3> </html>");
        b5.setText("<html><h3><b> " + cashier.getProductVedetteName(c[5].ID_PRODUCT) + "</h3> </html>");
        b6.setText("<html><h3><b> " + cashier.getProductVedetteName(c[6].ID_PRODUCT) + "</h3> </html>");
        b7.setText("<html><h3><b> " + cashier.getProductVedetteName(c[7].ID_PRODUCT) + "</h3> </html>");
        b8.setText("<html><h3><b> " + cashier.getProductVedetteName(c[8].ID_PRODUCT) + "</h3> </html>");
        b9.setText("<html><h3><b> " + cashier.getProductVedetteName(c[9].ID_PRODUCT) + "</h3> </html>");
        b10.setText("<html><h3><b> " + cashier.getProductVedetteName(c[10].ID_PRODUCT) + "</h3> </html>");
        b11.setText("<html><h3><b> " + cashier.getProductVedetteName(c[11].ID_PRODUCT) + "</h3> </html>");
        b12.setText("<html><h3><b> " + cashier.getProductVedetteName(c[12].ID_PRODUCT) + "</h3> </html>");

        }
 
        i1 =  c[1].ID_PRODUCT ;
        i2 =  c[2].ID_PRODUCT ;
        i3 =  c[3].ID_PRODUCT ;
        i4 =  c[4].ID_PRODUCT ;
        i5 =  c[5].ID_PRODUCT ;
        i6 =  c[6].ID_PRODUCT ;
        i7 =  c[7].ID_PRODUCT ;
        i8 =  c[8].ID_PRODUCT ;
        i9 =  c[9].ID_PRODUCT ;
        i10 =  c[10].ID_PRODUCT ;
        i11 =  c[11].ID_PRODUCT ;
        i12 =  c[12].ID_PRODUCT ;
    }


    /***********************************************************************************************
    ENLEVE un produit dans le panier BUTTON
     ***********************************************************************************************/
    public void removeProduct() {

        removeButton.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                if (ae.getSource() == removeButton && saleJList.getSelectedValue() != null) {

                    String r = (String) saleJList.getSelectedValue();
                    Product p = cashier.makeProductRemove(r); 
                    //On enleve par le Code du Produit
                    cashier.current.remove(p); setData();
 

                }
            }
        });

    }

    /***********************************************************************************************
    ANNULE BUTTON
     ***********************************************************************************************/
    public void annule() {

        annuleButton.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                if (ae.getSource() == annuleButton) {

                    cashier.basketStart(stock);
                    tangira(0);


                }
            }
        });

    }

    /***********************************************************************************************
    CHANGEMENT DE PRIX 
     ***********************************************************************************************/
    public void reprice() {
        repriceButtton.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                if (ae.getSource() == repriceButtton && cashier.current.getProductList().size() > 0 && (umukozi.PWD_EMPLOYE.contains("REDUCTION"))) {
                    System.out.println("size list:" + cashier.current.getProductList().size());
                    String nAff = (String) JOptionPane.showInputDialog(null, l(lang, "V_OPTIONS"), " ",
                            JOptionPane.QUESTION_MESSAGE, null, new String[]{"PRODUCT", "TOTAL", "CHANGE PV"}, nom);
                    if (nAff.equals("PRODUCT") && (umukozi.PWD_EMPLOYE.contains("REDUCTION"))) {
                        if (saleJList.getSelectedValue() == null) {
                            JOptionPane.showMessageDialog(null, l(lang, "V_SELECT") + " " + l(lang, "V_UN") + " " + l(lang, "V_ITEMS") + " " + l(lang, "V_DANS") + " " + l(lang, "V_LE") + " " + l(lang, "V_BASKET"), l(lang, "V_ATTENTION"), JOptionPane.WARNING_MESSAGE);
                        } else {
                            String r = (String) saleJList.getSelectedValue();
                            Product p = cashier.makeProductRemove(r); 
                            
                            JPanel pw=new JPanel();
                            JPasswordField passwordField = new JPasswordField(10);
                            JLabel label = new JLabel(l(lang, "V_PWD"));
                            pw.add(label);pw.add(passwordField);
                            passwordField.setText("");
                            JOptionPane.showConfirmDialog(null, pw, l(lang, "V_CONFIRM"), NORMAL);
                            String pwd=passwordField.getText();
                           
                            
//                         String pwd = JOptionPane.showInputDialog(l(lang, "V_CONFIRM") + " " + l(lang, "V_PWD"), " ");
                            

                            if (pwd.equals(getString("pwd"))) {
                                int n = 1;
                                ifashwePrice = getIn(JOptionPane.showInputDialog(l(lang, "V_CONFIRM") + " " + l(lang, "V_PRIXVENTE") + ", PR:" + p.prix_revient + " " + cashier.db.devise, "" + p.currentPrice));
                                if (ifashwePrice <= p.prix_revient) {
                                    n = JOptionPane.showConfirmDialog(frame, ifashwePrice + l(lang, "V_INFERIEUR") + p.prix_revient,
                                            "" + l(lang, "V_IGIHOMBO"), JOptionPane.YES_NO_OPTION);

                            if (n == 0) { 
                                ancienTotal = TOTALPRICE2; 
                                
cashier.current.getProductList().get(saleJList.getSelectedIndex()).productName += 
        "# Discount -"+(int)(100-((ifashwePrice*100)/cashier.current.getProductList().get(saleJList.getSelectedIndex()).currentPrice))+"%";

cashier.current.getProductList().get(saleJList.getSelectedIndex()).currentPrice = ifashwePrice;
setData(); 
totReduction = ancienTotal - TOTALPRICE2;  
} else {
                                        JOptionPane.showMessageDialog(null, l(lang, "V_AUCUN") + " " + l(lang, "V_REDUCTION") + " " + l(lang, "V_FAITE"), l(lang, "V_ATTENTION"), JOptionPane.WARNING_MESSAGE);

                                    }
} else if (ifashwePrice < p.currentPrice || (dataBase.toUpperCase().contains("PHARMEDICA"))) {
    ancienTotal = TOTALPRICE2;
   cashier.current.getProductList().get(saleJList.getSelectedIndex()).productName += 
        "# Discount -"+(int)(100-((ifashwePrice*100)/cashier.current.getProductList().get(saleJList.getSelectedIndex()).currentPrice))+"%";
    cashier.current.getProductList().get(saleJList.getSelectedIndex()).currentPrice = ifashwePrice;
    setData();
    totReduction = ancienTotal - TOTALPRICE2;
                                   
} else {
    JOptionPane.showMessageDialog(null, l(lang, "V_REDUCTION") + " " + l(lang, "V_NIMENSHI") + " " + l(lang, "V_LE") + " " + l(lang, "V_PRIXVENTE"), l(lang, "V_ATTENTION"), JOptionPane.WARNING_MESSAGE);
}
}
}
                    } else if (nAff.equals("TOTAL") && (umukozi.PWD_EMPLOYE.contains("REDUCTION"))) {
                        if (TOTALPRICE2 > 0) {
                            
                            JPanel pw=new JPanel();
                            JPasswordField passwordField = new JPasswordField(10);
                            JLabel label = new JLabel(l(lang, "V_PWD"));
                            pw.add(label);pw.add(passwordField);
                            passwordField.setText("");
                            JOptionPane.showConfirmDialog(null, pw, l(lang, "V_CONFIRM"), NORMAL);
                            String pwd=passwordField.getText();
                            
//                            String pwd = JOptionPane.showInputDialog(l(lang, "V_CONFIRM") + " " + l(lang, "V_PWD"), " ");
                            System.out.println("***---***" + getString("pwd"));
                            if (pwd.equals(getString("pwd"))) {
                                int n = 1;
                                reduction = getAmafaranga(l(lang, "V_CONFIRM") + "  " + l(lang, "V_PERCENTAGE") + " " + l(lang, "V_DE") + " " + l(lang, "V_REDUCTION"), l(lang, "V_INT"), 0);
                                if (reduction > 100) {
                                    reduction = getAmafaranga(l(lang, "V_CONFIRM") + " " + l(lang, "V_PERCENTAGE"), l(lang, "V_INT"), 0);
                                } else if (reduction <= 100 && reduction >= 0) {
                                    amountReduit = ((TOTALPRICE2 * reduction) / 100);
                                    System.out.println("AMOUNT REDUIT if:" + amountReduit);
                                    //totalprice=(int)(totalprice-amountReduit);
                                    System.out.println("TOTAL REDUIT if:" + TOTALPRICE2);
                                    amountPerProduct = amountReduit / cashier.current.getProductList().size();
                                    
                                  
for(int i=0;i<cashier.current.getProductList().size();i++ )
{
cashier.current.getProductList().get(i).productName += 
"# discount -"+reduction+"%";   
double rd=(100.0-reduction)/100;
System.out.println(rd);
cashier.current.getProductList().get(i).currentPrice = cashier.current.getProductList().get(i).currentPrice * rd ;
}
                                    ancienTotal = TOTALPRICE2;
                                    setData(); 
                                    totReduction = ancienTotal - TOTALPRICE2; 
                                } else {
                                    JOptionPane.showMessageDialog(null, l(lang, "V_AUCUN") + " " + l(lang, "V_REDUCTION") + " " + l(lang, "V_FAITE"), l(lang, "V_ATTENTION"), JOptionPane.INFORMATION_MESSAGE);

                                }
                            }
                        }
                    } else if (nAff.equals("CHANGE PV") && (umukozi.PWD_EMPLOYE.contains("CHANGER_PRIX"))) {
                        if (saleJList.getSelectedValue() == null) {
                            JOptionPane.showMessageDialog(null, l(lang, "V_SELECT") + " " + l(lang, "V_LE") + " " + l(lang, "V_ITEMS") + " " + l(lang, "V_DANS") + " " + l(lang, "V_BASKET"), l(lang, "V_ATTENTION"), JOptionPane.WARNING_MESSAGE);
                        } else {
                            String r = (String) saleJList.getSelectedValue();
                            Product p = cashier.makeProductRemove(r);
                            
                            JPanel pw=new JPanel();
                            JPasswordField passwordField = new JPasswordField(10);
                            JLabel label = new JLabel(l(lang, "V_PWD"));
                            pw.add(label);pw.add(passwordField);
                            passwordField.setText("");
                            JOptionPane.showConfirmDialog(null, pw, l(lang, "V_CONFIRM"), NORMAL);
                            String pwd=passwordField.getText();

//                            String pwd = JOptionPane.showInputDialog(l(lang, "V_CONFIRM") + " " + l(lang, "V_PWD"), " ");

                            if (pwd.equals(getString("pwd"))) {
                                int n = 1;
                                ifashwePrice = getIn(JOptionPane.showInputDialog(l(lang, "V_CONFIRM") + " " + l(lang, "V_PRIXVENTE") + " , PR: " + p.prix_revient + " " + cashier.db.devise, "" + p.currentPrice));

                                ancienTotal = TOTALPRICE2; 
                                cashier.current.getProductList().get(saleJList.getSelectedIndex()).currentPrice = ifashwePrice;
                                totReduction = 0;  
                               
                                if (stock.contains("CASHNORM")) {
                                    cashier.db.changePrice(p.productCode, ifashwePrice, p.tva, "PRIX", nom, p.productName + " " + stock);
                                } else {
                                    cashier.db.changePrice(p.productCode, ifashwePrice, p.tva, "PRIX_" + stock, nom, p.productName + " " + stock);
                                } 
                            }
                        }
                    } else {
                        JOptionPane.showMessageDialog(null, l(lang, "V_PERMISSION") + " ", l(lang, "V_ATTENTION"), JOptionPane.WARNING_MESSAGE);
                    }
                }
            }
        });

    }

    public void ecoute() {

        scanButton.addKeyListener(
                new KeyListener() {

                    public void actionPerformed(ActionEvent ae) {
                    }

                    public void keyTyped(KeyEvent e) {

                        char ck = e.getKeyChar();
                        String cc = "" + ck;
                        if (ck == ' ') {
                            if (cc.hashCode() == 10) {
                                Product p = null;
                                for (int z = 0; z < cashier.allProduct.size(); z++) {
                                    Product j = cashier.allProduct.get(z);
                                    if (j.codeBar.equals(ss)) {
                                        p = j;
                                        break;
                                    }
                                }
                                if (p != null) {
                                    int qtyCode = 1;// getIn(p.productName+"  zingahe","  INT",0);
                                    if (p.check(cashier.howManyProductInSale(p.productCode(), qtyCode)) && qtyCode != -1) {
                                       
                                        Product pi = p.clone(qtyCode);
                                        cashier.current.add(pi); setData();
                                    } else {
                                        JOptionPane.showMessageDialog(frame, p.productName + " " + l(lang, "V_HASIGAYE") + " " + p.qtyLive, l(lang, "V_ERROR"), JOptionPane.WARNING_MESSAGE);
                                    }
                                } else {
                                    JOptionPane.showMessageDialog(frame, currentScan + l(lang, "V_STOCKCOURRANT"), l(lang, "V_ERROR"), JOptionPane.WARNING_MESSAGE);
                                }

                                ss = "";

                            } else {
                                ss = ss + ck;
                            }
                        }

                    }

                    public void keyPressed(KeyEvent e) {
                    }

                    public void keyReleased(KeyEvent e) {
                    }
                });

    }


    public void Proforma() {
        proformaButton.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                if (ae.getSource() == proformaButton) {
                    if (cashier.current.getProductList().size() > 0 && IJISHO.contains("PROFORMA")) {
                        if (IJISHO.equals("PROFORMA")) {
                            
String nAff  ;
String refer  ;

if(!l(lang, "V_REFERENCE").contains("CHAMBRE"))
{nAff = (JOptionPane.showInputDialog(frame, l(lang, "V_NUMERO") + ", " + l(lang, "V_NAMEORSURNAME")));
 refer = (JOptionPane.showInputDialog(frame, l(lang, "V_REFERENCE") + "  " + nAff));
}
else
{
    
    nAff = (JOptionPane.showInputDialog(frame, l(lang, "V_NUMERO") + ", " + l(lang, "V_NAMEORSURNAME")));
    Client  c = (Client) JOptionPane.showInputDialog(null, " " + l(lang, "V_OPTIONS"), l(lang, "V_CLIENT")
            , JOptionPane.QUESTION_MESSAGE, null, cashier.db.getClientV(nAff.toUpperCase(), "", stock), nom); 
    nAff=c.NUM_AFFILIATION;
    refer = ""+cashier.db.getRes(c.NUM_AFFILIATION);  
}  
                            int n = JOptionPane.showConfirmDialog(frame,
                                    "  NOM_CLIENT     =" + nAff
                                    + "\n " + l(lang, "V_REFERENCE") + "   =" + refer,
                                    " " + l(lang, "V_FICHE") + " PROFORMA", JOptionPane.YES_NO_OPTION);

                            if (n == 0) {
                                cashier.current.id_client = stock;
                                cashier.current.tot = TOTALPRICE2;
                                cashier.current.tva = TVAPRICE2;
                                cashier.current.total();
                                EndProforma(nAff, (int)cashier.current.tot, refer, 
                                        (int)cashier.current.tva,"","");
                                IJISHO = "FACTURE";
                                proformaButton.setBackground(new java.awt.Color(203, 215, 253));
                                setTitle(nom + "'s Ishyiga " + l(lang, "V_SESSION") + " +  "
                                        + l(lang, "V_TARIF") + " : " + stock + " " + l(lang, "V_MODE") + " : " + IJISHO);
                                tangira(0);

                            }

                        } else if (IJISHO.equals("PROFORMA-OPEN")) {
                            String nAff = (JOptionPane.showInputDialog(frame, l(lang, "V_NUMERO") + ", " + l(lang, "V_NAMEORSURNAME"), "" + pro.nom_client));
                            String refer = (JOptionPane.showInputDialog(frame, l(lang, "V_REFERENCE") + "  " + nAff, "" + pro.reference));

                            int n = JOptionPane.showConfirmDialog(frame,
                                    "  NOM_CLIENT     =" + nAff
                                    + "\n "+l(lang, "V_REFERENCE")+"   =" + refer,
                                    " " + l(lang, "V_FICHE") + " PROFORMA", JOptionPane.YES_NO_OPTION);

                            if (n == 0) {
                                cashier.current.id_client = stock;
                                cashier.current.tot = TOTALPRICE2;
                                cashier.current.tva = TVAPRICE2;
                                cashier.current.total();
                                EndProforma(nAff, (int)cashier.current.tot, refer, 
                                        (int)cashier.current.tva,"","");
                                IJISHO = "FACTURE";
                                proformaButton.setBackground(new java.awt.Color(203, 215, 253));
                                setTitle(nom + "'s Ishyiga " + l(lang, "V_SESSION") + " +  "
                                        + l(lang, "V_TARIF") + " : " + stock + " " + l(lang, "V_MODE") + " : " + IJISHO);
                                tangira(0);

                            }

                        }
                        
                        else {
                            JOptionPane.showMessageDialog(frame, stock + l(lang, "V_NUMAFFILIATION") + " " + l(lang, "V_DIFFERENT") + "  " + l(lang, "V_TARIF"), l(lang, "V_ERROR"), JOptionPane.PLAIN_MESSAGE);
                        }

                        //                        cashier.current.payed = p;
                        //                        Cashier.yose = Cashier.yose + p;
} else if (!IJISHO.contains("PROFORMA")) {
String s = (String) JOptionPane.showInputDialog(null, l(lang, "V_OPTIONS"), l(lang, "V_PROFORMA"),
JOptionPane.QUESTION_MESSAGE, null,
new String[]{l(lang, "V_MUSHYA"),  
             l(lang, "V_PRO_FA"), 
             l(lang, "V_DELETE"),
             l(lang, "V_OPEN")
},           l(lang, "V_MUSHYA"));
    if (s != null) {
                            
if (s.equals(l(lang, "V_DELETE"))  ) 
{
    String numAff = (JOptionPane.showInputDialog(frame, l(lang, "V_NUMERO") + ", " + l(lang, "V_NAMEORSURNAME")));
    Proforma inv = (Proforma) JOptionPane.showInputDialog(null, l(lang, "V_OPTIONS"), l(lang, "V_IMPRESSION"),
            JOptionPane.QUESTION_MESSAGE, null, cashier.db.Proformat(numAff), "");
    pro = inv;  
    proformaButton.setBackground(Color.WHITE);
    setTitle(nom + "'s Ishyiga " + l(lang, "V_SESSION") + " + Tarif " + stock + " +MODE: " + IJISHO);
    toTransProforma2=new LinkedList();
    toTransProforma2.add(inv);
    cashier.db.updateProformat(toTransProforma2,"DELETED"); 
}                            
if (s.equals(l(lang, "V_MUSHYA")) && setT("", "PROFORMA", stock)) 
{
    proformaButton.setBackground(Color.magenta);
    setTitle(nom + "'s Ishyiga " + l(lang, "V_SESSION") + " + Tarif " + stock + " +MODE: " + IJISHO);
}
else if (s.equals(l(lang, "V_OPEN")) && setT("", "PROFORMA-OPEN", stock)) 
{
    String numAff = (JOptionPane.showInputDialog(frame, l(lang, "V_NUMERO") + ", " + l(lang, "V_NAMEORSURNAME")));
    Proforma inv = (Proforma) JOptionPane.showInputDialog(null, l(lang, "V_OPTIONS"), l(lang, "V_IMPRESSION"),
            JOptionPane.QUESTION_MESSAGE, null, cashier.db.Proformat(numAff), "");
    pro = inv; 
    proformaButton.setBackground(Color.WHITE);
    setTitle(nom + "'s Ishyiga " + l(lang, "V_SESSION") + " + Tarif " + stock + " +MODE: " + IJISHO);

    LinkedList<List> l = cashier.db.listProforma(inv.id_Proforma);
    for (int i = 0; i < l.size(); i++) {
        List ll = l.get(i);
        Product p = new Product(ll.id_pro, ll.lot, ll.code, ll.qty, ll.price, ll.tva, "", "", ll.code, cashier.db, 0);
        p.qty = ll.qty;
        cashier.current.add(p);
    }
    setData();
}
else if (s.equals(l(lang, "V_PRO_FA")) && setT("", "FACTURE", stock)) 
{
String numAff = (JOptionPane.showInputDialog(frame, l(lang, "V_NUMERO") + ", " + 
        l(lang, "V_NAMEORSURNAME")));
Proforma [] currentOpenProforma = cashier.db.Proformat(numAff);
LinkedList < JCheckBox> jcheck=new LinkedList();
JPanel pane=new JPanel(); 
int div=currentOpenProforma.length/5; 
pane.setLayout(new GridLayout(5,div)); 
    for(int i=0;i<currentOpenProforma.length;i++)
    {
    Proforma  ret=currentOpenProforma[i];
    JCheckBox now=new JCheckBox(""+ret);
    jcheck.add(now);
    pane.add(now); 
    }   
JOptionPane.showMessageDialog(null, pane,l(lang,"V_SELECT_PROFORMA")+tarif,
        JOptionPane.INFORMATION_MESSAGE);  
  toTransProforma2= new LinkedList();
  toTransProformaAbakozi= new LinkedList();
for(int i=0;i<currentOpenProforma.length;i++)
{
    if(jcheck.get(i).isSelected())
    { toTransProforma2.add(currentOpenProforma[i]); 
      toTransProformaAbakozi.add(currentOpenProforma[i].num_Client);} 
} 

proformaButton.setBackground(Color.WHITE);
setTitle(nom + "'s Ishyiga " + l(lang, "V_SESSION") + " + Tarif " + stock + " +MODE: " + IJISHO);

for(int k=0;k<toTransProforma2.size();k++)
{
LinkedList<List> l = cashier.db.listProforma(toTransProforma2.get(k)
        .id_Proforma);
for (int i = 0; i < l.size(); i++) {
   List ll = l.get(i);
   Product p = new Product(ll.id_pro, ll.lot, ll.code, ll.qty, ll.price, ll.tva, "", "", ll.code, cashier.db, 0);
   p.qty = ll.qty;
   cashier.current.add(p);
}
} 
setData();
                            }
                      }

                    }
                }
            }
        });

    }

    boolean setT(String tiers, String ijisho, String tarif) {
        int n = JOptionPane.showConfirmDialog(frame,
                " Tier  : " + tiers
                + "\n TARIF : " + tarif
                + "\n MODE  : " + ijisho,
                "CHANGEMENT OPERATION", JOptionPane.YES_NO_OPTION);
        if (n == 0) {
            this.IJISHO = ijisho;
            setTitle(nom + "'s Ishyiga " + l(lang, "V_SESSION") + " +  "
                    + l(lang, "V_TARIF") + " : " + tarif + " " + l(lang, "V_MODE") + " : " + IJISHO);

            return true;
        }
        return false;
    }

    /***********************************************************************************************
    //PAIEMENT BUTTON CASH  
     ***********************************************************************************************/
    public void scancash() {

        cashButton.addKeyListener(
                new KeyListener() {

                    public void actionPerformed(ActionEvent ae) {
                    }

                    public void keyTyped(KeyEvent e) {
                        char ck = e.getKeyChar();
                        String cc = "" + ck;
                        System.out.println("HASH CODE:" + cc.hashCode());

                        if (cc.hashCode() == 10) {
                            Product p = null;
                            for (int z = 0; z < cashier.allProduct.size(); z++) {
                                Product j = cashier.allProduct.get(z);
                                if (j.codeBar.equals(ss)) {
                                    p = j;
                                    break;
                                }
                            }
//                            System.out.println(p.codeBar);
                            if (p != null) {
                                int qte = getQuantity(p.productName + " " + l(lang, "V_ZINGAHE") + " ??", "  ", 0);
                                addProductLot(p, qte);
                            } else {
                                JOptionPane.showMessageDialog(frame, currentScan + l(lang, "V_STOCKCOURRANT"), l(lang, "V_ERROR"), JOptionPane.WARNING_MESSAGE);
                            }
                            ss = "";
                        } else {
                            ss = ss + ck;
                        }
                        System.out.println("ee" + ss);
                    }

                    public void keyPressed(KeyEvent e) {
                    }

                    public void keyReleased(KeyEvent e) {
                    }
                });
    }

    public void scanCredit() {
        creditButton.addKeyListener(
                new KeyListener() {

                    public void actionPerformed(ActionEvent ae) {
                    }

                    public void keyTyped(KeyEvent e) {
                        char ck = e.getKeyChar();
                        String cc = "" + ck;

                        if (cc.hashCode() == 10) {
                            Product p = null;
                            for (int z = 0; z < cashier.allProduct.size(); z++) {
                                Product j = cashier.allProduct.get(z);
                                if (j.codeBar.equals(ss)) {
                                    p = j;
                                    break;
                                }
                            }
                            if (p != null) {
                                int qte = getQuantity(p.productName + " " + l(lang, "V_ZINGAHE") + " ??", "  ", 0);
                                addProductLot(p, qte);
                            } else {
                                JOptionPane.showMessageDialog(frame, currentScan + l(lang, "V_STOCKCOURRANT"), l(lang, "V_ERROR"), JOptionPane.WARNING_MESSAGE);
                            }
                            ss = "";
                        } else {
                            ss = ss + ck;
                        }
                    }

                    public void keyPressed(KeyEvent e) {
                    }

                    public void keyReleased(KeyEvent e) {
                    }
                });
    }

    public void scanGros() {

        t1.addKeyListener(
                new KeyListener() {

                    public void actionPerformed(ActionEvent ae) {
                    }

                    public void keyTyped(KeyEvent e) {
                        char ck = e.getKeyChar();
                        String cc = "" + ck;
                        //System.out.println(cc.hashCode());
                        //if(ck==' ') 
                        if (cc.hashCode() == 10) {
                            Product p = null;
                            for (int z = 0; z < cashier.allProduct.size(); z++) {
                                Product j = cashier.allProduct.get(z);
                                if (j.codeBar.equals(ss)) {
                                    p = j;
                                    break;
                                }
                            }
                            if (p != null) {
                                addProductLot(p, 1);
                            } else {
                                JOptionPane.showMessageDialog(frame, currentScan + l(lang, "V_STOCKCOURRANT"), l(lang, "V_ERROR"), JOptionPane.WARNING_MESSAGE);
                            }
                            ss = "";
                        } else {
                            ss = ss + ck;
                        }
                    }

                    public void keyPressed(KeyEvent e) {
                    }

                    public void keyReleased(KeyEvent e) {
                    }
                });
    }
    
    public void scanFrame() {

        getStockButton.addKeyListener(
                new KeyListener() {

                    public void actionPerformed(ActionEvent ae) {
                    }

                    public void keyTyped(KeyEvent e) {
                        char ck = e.getKeyChar();
                        String cc = "" + ck;
                        //System.out.println(cc.hashCode());
                        //if(ck==' ') 
                        if (cc.hashCode() == 10) {
                            Product p = null;
                            for (int z = 0; z < cashier.allProduct.size(); z++) {
                                Product j = cashier.allProduct.get(z);
                                if (j.codeBar.equals(ss)) {
                                    p = j;
                                    break;
                                }
                            }
                            if (p != null) {
                                int qte = getQuantity(p.productName + " " + l(lang, "V_ZINGAHE") + " ??", "  ", 0);
                                addProductLot(p, qte);
                            } else {
                                JOptionPane.showMessageDialog(frame, currentScan + l(lang, "V_STOCKCOURRANT"), l(lang, "V_ERROR"), JOptionPane.WARNING_MESSAGE);
                            }
                            ss = "";
                        } else {
                            ss = ss + ck;
                        }
                    }

                    public void keyPressed(KeyEvent e) {
                    }

                    public void keyReleased(KeyEvent e) {
                    }
                });
    }
    public void scanOne() {

        annuleButton.addKeyListener(
                new KeyListener() {

                    public void actionPerformed(ActionEvent ae) {
                    }

                    public void keyTyped(KeyEvent e) {
                        char ck = e.getKeyChar();
                        String cc = "" + ck;
                        //System.out.println(cc.hashCode());
                        //if(ck==' ') 
                        if (cc.hashCode() == 10) {
                            Product p = null;
                            for (int z = 0; z < cashier.allProduct.size(); z++) {
                                Product j = cashier.allProduct.get(z);
                                if (j.codeBar.equals(ss)) {
                                    p = j;
                                    break;
                                }
                            }
                            if (p != null) {
                                int qte = getQuantity(p.productName + " " + l(lang, "V_ZINGAHE") + " ??", "  ", 0);
                                addProductLot(p, qte);
                            } else {
                                JOptionPane.showMessageDialog(frame, currentScan + l(lang, "V_STOCKCOURRANT"), l(lang, "V_ERROR"), JOptionPane.WARNING_MESSAGE);
                            }
                            ss = "";
                        } else {
                            ss = ss + ck;
                        }
                    }

                    public void keyPressed(KeyEvent e) {
                    }

                    public void keyReleased(KeyEvent e) {
                    }
                });
    }
    public void scanRemove() {

        removeButton.addKeyListener(
                new KeyListener() {

                    public void actionPerformed(ActionEvent ae) {
                    }

                    public void keyTyped(KeyEvent e) {
                        char ck = e.getKeyChar();
                        String cc = "" + ck;
                        //System.out.println(cc.hashCode());
                        //if(ck==' ') 
                        if (cc.hashCode() == 10) {
                            Product p = null;
                            for (int z = 0; z < cashier.allProduct.size(); z++) {
                                Product j = cashier.allProduct.get(z);
                                if (j.codeBar.equals(ss)) {
                                    p = j;
                                    break;
                                }
                            }
                            if (p != null) {
                                int qte = getQuantity(p.productName + " " + l(lang, "V_ZINGAHE") + " ??", "  ", 0);
                                addProductLot(p, qte);
                            } else {
                                JOptionPane.showMessageDialog(frame, currentScan + l(lang, "V_STOCKCOURRANT"), l(lang, "V_ERROR"), JOptionPane.WARNING_MESSAGE);
                            }
                            ss = "";
                        } else {
                            ss = ss + ck;
                        }
                    }

                    public void keyPressed(KeyEvent e) {
                    }

                    public void keyReleased(KeyEvent e) {
                    }
                });
    }
    
    public void scanAdd() {

        addButton.addKeyListener(
                new KeyListener() {

                    public void actionPerformed(ActionEvent ae) {
                    }

                    public void keyTyped(KeyEvent e) {
                        char ck = e.getKeyChar();
                        String cc = "" + ck;
                        //System.out.println(cc.hashCode());
                        //if(ck==' ') 
                        if (cc.hashCode() == 10) {
                            Product p = null;
                            for (int z = 0; z < cashier.allProduct.size(); z++) {
                                Product j = cashier.allProduct.get(z);
                                if (j.codeBar.equals(ss)) {
                                    p = j;
                                    break;
                                }
                            }
                            if (p != null) {
                                int qte = getQuantity(p.productName + " " + l(lang, "V_ZINGAHE") + " ??", "  ", 0);
                                addProductLot(p, qte);
                            } else {
                                JOptionPane.showMessageDialog(frame, currentScan + l(lang, "V_STOCKCOURRANT"), l(lang, "V_ERROR"), JOptionPane.WARNING_MESSAGE);
                            }
                            ss = "";
                        } else {
                            ss = ss + ck;
                        }
                    }

                    public void keyPressed(KeyEvent e) {
                    }

                    public void keyReleased(KeyEvent e) {
                    }
                });
    }

    public void cash() {
        cashButton.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                if (ae.getSource() == cashButton && v != 0 && !PRESCRIPTION.isSelected()) {
                    JOptionPane.showMessageDialog(frame, "" + l(lang, "V_CLICK") + "  " + l(lang, "V_CREDIT"), "" + l(lang, "V_NI") + "  " + l(lang, "V_CREDIT"), JOptionPane.PLAIN_MESSAGE);
                }

                if (ae.getSource() == cashButton && PRESCRIPTION.isSelected()) {
                    try {
                        if (lesClients.getSelectedValue() != null) {
                            cashier.current.client = (Client) lesClients.getSelectedValue();
                            cashier.current.id_client = cashier.current.client.ASSURANCE;
                            cashier.current.OBSERVATION_INVOICE = (JOptionPane.showInputDialog(frame, " OBSERVATION"));
                            cashier.PRESCRIPTIONEND(stock,tax.smartPatient);

                            if (cashier.current.id_invoice != -1) {
                                cashier.basketStart(stock);
                                tangira(0);
                            }
                        }
                    } catch (Throwable e) {
                        cashier.db.insertError(" facture igize ikibazo " + e, cashier.factNum + "print");
                    }
                    tangira(0);
                }  
                else if (ae.getSource() == cashButton && v == 0 && TOTALPRICE2 > 0 
                        && ( IJISHO.contains("FACTURE")|| IJISHO.equals("PRO_FA"))) 
                {
                    //    if(stock.equals("CASHREMISE"))
                    cashier.current.id_client = "CASHNORM";

                    displayMsg(l(lang, "V_TOTAL"), RRA_PRINT2.setVirgule(TOTALPRICE2,1, ",") + " " + dev);

                    double money = getAmafaranga(l(lang, "V_TOTAL") + " : " + RRA_PRINT2.setVirgule(TOTALPRICE2,1, ","), " INT", 0);
                    double rest = money -TOTALPRICE2;
                    // Si c est pas assez d argent
                    boolean done = true;
                    if (rest >= 0 && rest < 50000) {
                        done = false;
                    }
                    while (money != -1 && done) {
                        if (money >= 0 && rest < 0) {
                            money = getAmafaranga(money + " " + l(lang, "V_NTIHAGIJE") + "  ... " + l(lang, "V_TOTAL") + " : " + RRA_PRINT2.setVirgule(TOTALPRICE2,1, ","), l(lang, "V_INT"), 0);
                        }
                        if (rest >= 50000) {
                            money = getAmafaranga(money + " " + l(lang, "V_NIMENSHI") + " ...  " + l(lang, "V_TOTAL") + " : " + RRA_PRINT2.setVirgule(TOTALPRICE2,1, ","), l(lang, "V_INT"), 0);
                        }

                        rest = money -TOTALPRICE2;
                        if (rest >= 0 && rest < 50000) {
                            done = false;
                        }
                    }

                    if (rest >= 0 && money != -1 && rest < 50000) {
                        String EstInStock = cashier.estDansStock();
                        if (EstInStock.equals("")) {
                            cashier.current.total();
                            cashier.current.payed = TOTALPRICE2;
                            cashier.current.tot = TOTALPRICE2;
                            Cashier.yose2 = Cashier.yose2 +TOTALPRICE2;
                            cashier.current.tva = TVAPRICE2;                           
                            cashier.current.client=new Client("DIVERS", "", "", 100, 0, "CASHNORM", "", "", "", "");
                            
                            
                            String reference="CASHNORM";
                            
                            if(IJISHO.equals("PRO_FA"))
                            {  
                                
cashier.db.updateProformat(toTransProforma2,"FACTURE"); 
 Proforma proforma=toTransProforma2.getFirst();
 reference=cashier.current.id_employe+" "+proforma.nom_client+" "+proforma.reference;

cashier.current.id_employe=   (String) JOptionPane.showInputDialog(null, l(lang, "V_OPTIONS"), " ",
 JOptionPane.QUESTION_MESSAGE, null, toTransProformaAbakozi.toArray() , nom);    
  }
PrintRRA(money, rest,TVAPRICE2,reference);
                        } else {
                            JOptionPane.showMessageDialog(frame, EstInStock + " " + l(lang, "V_PRODUCTISTAKEN"), l(lang, "V_ATTENTION") + " !!! ", JOptionPane.QUESTION_MESSAGE);
                        }
                    }
                } else if (!IJISHO.contains("FACTURE") && cashier.current.getProductList().size() == 0 && setT("", "FACTURE", stock)) {
                    proformaButton.setBackground(new java.awt.Color(203, 215, 253));
                }
            }
        });
    }

    public void smart() {
        smartButton.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
     
                
 System.out.println(ae.getSource() == smartButton); 
 System.out.println(cashier.current.getProductList().size() > 0); 
 System.out.println(IJISHO.contains("FACTURE")); 
 System.out.println(!stock.equals("CASHNORM")); 
 System.out.println(!allow_smart.equalsIgnoreCase("NO")); 
 
                if (ae.getSource() == smartButton && cashier.current.getProductList().size() > 0
                    && IJISHO.contains("FACTURE") && !stock.equals("CASHNORM") 
                    && !allow_smart.equalsIgnoreCase("NO")
                    ) 
                  //   if (ae.getSource() == smartButton && cashier.current.getProductList().size() > 0) 
                    
                {
                    try {
                        
                        //SendLotCloud product_Cloud = new CommCloud(cashier.db.connMysql);
                        //product_Cloud.GET_MATRIX();
                        if(cashier.smartConnection!=null) 
                        {
                        System.out.println(!stock.equals("11 CASHNORM")); 
                        SmartExchange exchangePatient = getPendingSmart("1");
                        System.out.println(!stock.equals("22 CASHNORM")); 
                        if (exchangePatient != null) {
                        //LinkedList<String> list = cashier.db.getFichier("forwardedcarddata.xml");
                            //SmartFwd card=new SmartFwd(list) ;                        
                            //SmartPatient card=new SmartXML("forwardedcarddata.xml",true).getPatient();

                            SmartPatient card = exchangePatient.getEXCHANGE_PATIENT();
                            System.out.println(!stock.equals("22 CASHNORM"));
                            JOptionPane.showMessageDialog(null, card, "CARD RECORD", JOptionPane.WARNING_MESSAGE);

                            String db = "";
                            
                            if(card.Type2==0) {
                                card.percentage=(int) Double.parseDouble(card.Amount_Required);
                            }

                            if (stock.equals("RAMA")) {
                                db = "_RAMA";
                            }
 
String nAff = card.medicalaid_number;
Client c = cashier.db.getClientAff(nAff, db, "");//" AND ASSURANCE='" + stock + "'");
card =cashier.smartConnection.getClientCopay( card); 
String loc=cashier.smartConnection.getPracticeNumber();
SMART_MATRIX sm2=null;
if(card!=null)
{
    sm2=getCardInfo2(card.medicalaid_code+card.medicalaid_plan,
        card,cashier.smartConnection.stateMysql,
        cashier.smartConnection.connMysql); 
}


if(sm2!=null)
{
if (c == null) {
//    c = new Client(card.patient_id, card.patient_forenames, card.patient_surname, 
//        perc, card.expiryToInt(card.medicalaid_expiry), "", sm.SCHEME_INSURANCE, 
//        card.medicalaid_plan, "0", card.medicalaid_code);
    int n = JOptionPane.showConfirmDialog(frame, " " + l(lang, "V_URASHAKA") + "  " + l(lang, "V_KUMUSHYIRAMO") + "", nAff + " " + l(lang, "V_NTAWE"), JOptionPane.YES_NO_OPTION);
    if (n == 0) { 

if (stock.equals("RAMA")) {
    new ClientRamaGui(cashier.db, card.patient_forenames, card.patient_surname, card.medicalaid_number,
            card.patient_dob, "");
} else {
    new PersonInterface(cashier.db, card.patient_forenames, card.patient_surname, 
            card.medicalaid_number, card.patient_dob, card.getGender(card.patient_gender), 
            "" + card.expiryToInt(card.medicalaid_expiry), sm2.SCHEME_INSURANCE,card.percentage,sm2.ID_INSURANCE);
}
                                }
                            } else {
                                System.out.println(c.ASSURANCE);
                                c.DATE_EXP= card.expiryToInt(card.medicalaid_expiry);
                                card =cashier.smartConnection.getClientCopay( card);
                                c.PERCENTAGE=card.percentage; 
                                String DBB="CLIENT";
                                cashier.db.upDateClient(c,DBB,c.NUM_AFFILIATION);
                                c = cashier.db.getClientAff(nAff, db, "");//, " AND ASSURANCE='" + stock + "'");
      
                                
                                if ((c.DATE_EXP < today && !card.card_validitystatus.equals("1"))|| (!card.card_validitystatus.equals("1"))) {
                                    
                                    int n = JOptionPane.showConfirmDialog(frame, "" + l(lang, "V_URASHAKA") + "  " + l(lang, "V_KONGERA") + " \n"+c.DATE_EXP+" : "+today, nAff + "  " + l(lang, "V_EXPIRER"), JOptionPane.WARNING_MESSAGE);

                                    if (n == 0) {
                                        if (stock.equals("RAMA")) {
                                            new ClientRamaGui(cashier.db, c);
                                        } else {
                                            new PersonInterface(cashier.db, c);
                                        }
                                    }
                                    c = null;
                                } else {
                                    int n = JOptionPane.showConfirmDialog(frame,
                                            "  NOM_CLIENT     =" + c.NOM_CLIENT
                                            + "\n PRENOM CLIENT  =" + c.PRENOM_CLIENT
                                            + "\n NUM_AFFILIATION=" + c.NUM_AFFILIATION
                                            + "\n BENEFICIAIRE =" + c.BENEFICIAIRE
                                            + "\n LIEN AVEC ADHERENT=" + c.LIEN
                                            + "\n PERCENTAGE     =" + c.PERCENTAGE
                                            + "\n ASSURANCE        =" + c.ASSURANCE
                                            + "\n DPT        =" + c.EMPLOYEUR,
                                            " " + l(lang, "V_FICHE") + nAff, JOptionPane.YES_NO_OPTION);
                                    if (n == 0 && cashier.smartConnection!=null) {
                                        card.PracticeNumber=loc;
                                        Main.tax.setSmartPatient(card); 
                                        cashier.db.insertTrackSmart(new SMART_TRACKING(0, "","",exchangePatient.toString2(), "", umukozi.NOM_EMPLOYE,card.global_id,card.medicalaid_number,exchangePatient.getID()));
                                         
                                        cashier.smartConnection.updateFlag7(card.global_id, "5"," and progress_flag=1");
                                        System.out.println(Main.tax.smartPatient);
                                        if (finishCredit(c,card,exchangePatient.toString2())) {
                                            System.out.println("chapa3");
                                            cashier.smartConnection.updateFlag7(card.global_id, "2"," and progress_flag=5"); 
                                            cashier.smartConnection.updateExchange(exchangePatient.getID(),card.global_id,Taxes.smartXMLForward,"5");
                                        }
                                    }
                                    
                                }
                            }
}
                            
                        } else {
                            JOptionPane.showMessageDialog(frame, " NOT PATIENT SELECT OR CARD NOT FORWARDED ", l(lang, "V_ATTENTION"), JOptionPane.WARNING_MESSAGE);
                        }
                    }
                        else {
                            cashier.db.insertError("smart connection ", "THERE IS NO CONNECTION TO SMART DATABASE");
                            JOptionPane.showMessageDialog(frame, " THERE IS NO CONNECTION TO SMART DATABASE \n CONTACT YOUR SYSTEM ADMNINISTRATOR ", l(lang, "V_ATTENTION"), JOptionPane.ERROR_MESSAGE);

                        }

                    } catch (Exception ex) {
                        Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                else
                {
                    JOptionPane.showMessageDialog(frame, stock+" NOT ALLOWED TO USE SMART TECHNOLOGY ", l(lang, "V_ATTENTION"), JOptionPane.WARNING_MESSAGE);
                        
                }
            }
        });
    }
    
    
    SmartExchange getPendingSmart(String flag) {
        LinkedList<SmartExchange> pending = cashier.smartConnection.getPendingClient(flag);
        if (pending != null) {
            
            LinkedList< JRadioButton> jcheck = new LinkedList();
            JPanel pane = new JPanel();
            int div = pending.size();
            pane.setLayout(new GridLayout(pending.size()+1, div));
            for (int i = 0; i < pending.size(); i++) {
                SmartExchange ret = pending.get(i);
                JRadioButton now = new JRadioButton("" + ret);
                jcheck.add(now);
                pane.add(now);
            }
            JOptionPane.showMessageDialog(null, pane, l(lang, "V_NUMAFFILIATION")+" "+l(lang, "V_NAMEORSURNAME"), JOptionPane.INFORMATION_MESSAGE);
            for (int i = 0; i < pending.size(); i++) {
                if (jcheck.get(i).isSelected()) {
                    SmartExchange ok = pending.get(i);
                    return ok;
                }
            }
            //cashier.addAll(database.getLivProduct(toTrans));
            //setData();
        }
        return null;
    }

    Client[] checkClient(String name, String db) { 
        
        LinkedList<Client> clii = new LinkedList();
        String resString = "";
        String nom1 = "";
        String prenom1 = "";
        StringTokenizer st1 = new StringTokenizer(name);
        
        if (st1.hasMoreTokens()) {
            nom1 = st1.nextToken(" ");
        }
        if (st1.hasMoreTokens()) {
            prenom1 = st1.nextToken(" ");
        }
        String sql = "";
        if (stock.equals("SORAS") || stock.equals("CORAR") || stock.equals(("MMI")) || stock.equals(("RAMA"))) {
            sql = "and assurance='" + stock + "'";
        }


        clii = cashier.db.getOldClient("select * from APP.CLIENT" + db + " where  NOM_CLIENT like '%" + nom1 + "%' " + sql);

        Client[] cli = new Client[clii.size()];
        for (int h = 0; h < clii.size(); h++) {
            cli[h] = clii.get(h);
        }
        return cli;
    }

    /***********************************************************************************************
    PAIEMENT BUTTON CREDIT
     ***********************************************************************************************/
    public void Credit() {        
        creditButton.addActionListener(new ActionListener() { 
            public void actionPerformed(ActionEvent ae) {
                if (ae.getSource() == creditButton && cashier.current.getProductList().size() > 0 && PRESCRIPTION.isSelected()) {
                    JOptionPane.showMessageDialog(frame, "" + l(lang, "V_CLICK") + "  SEND PRESCRIPTION", "" + l(lang, "V_NI") + "  PRESCRIPTION", JOptionPane.PLAIN_MESSAGE);
                }
                //                        else if (ae.getSource() == creditButton && cashier.current.getProductList().size() > 0 && IJISHO.contains("FACTURE")) 
                //                        {
                //                            JOptionPane.showMessageDialog(frame,l(lang, "V_PERMISSION")+ " : CREDIT IN CASHNORM  ",l(lang, "V_ERROR"), JOptionPane.WARNING_MESSAGE);
                //                        }
                else if (ae.getSource() == creditButton 
                        && cashier.current.getProductList().size() > 0 
                        && IJISHO.contains("FACTURE")) {
                    
                    int nn=0;
                    if(!allow_smart.equalsIgnoreCase("NO"))
                    {
                        nn = JOptionPane.showConfirmDialog(frame,stock+" INSURANCE USES SMART\n DO YOU WANT TO PROCEED?" ,l(lang, "V_FICHE"), JOptionPane.YES_NO_OPTION);
                    }
                    if(nn==0)
                    {
                    Client c = null;
                    boolean scan = false;
                    String nAff = "";

                    if (stock.equals("CASHNORM")) {
                        nAff = (JOptionPane.showInputDialog(l(lang, "V_NUMERO") + ", " + l(lang, "V_NAMEORSURNAME"), "DIVERS"));
                    } else if (!stock.equals("CASHNORM") && currentClient == null) {
                        nAff = (JOptionPane.showInputDialog(frame, l(lang, "V_NUMERO") + ", " + l(lang, "V_NAMEORSURNAME")));
                    } 
                    if (currentClient == null) {
                        String db = "";
                        if (stock.equals("RAMA")) {
                            db = "_RAMA"; }

                        /////////////////////
                        if (nAff != null && db.contains("RAMA") && nAff.contains(" ")) {
                            System.out.println(nAff + " SIZE:" + nAff.length());
                            String code = "";

                            for (int i = 0; i < nAff.length(); i++) {
                                String j = "" + nAff.charAt(i);
                                if (j.equals(" ")) {
                                    break;
                                }
                                code += j;
                            }
                            
                            Client[] k = cashier.db.getClientScann(code, db);

                            if (k.length > 0) {
                                c = (Client) JOptionPane.showInputDialog(null, " " + l(lang, "V_OPTIONS"), l(lang, "V_CLIENT"), JOptionPane.QUESTION_MESSAGE, null, k, nom);
                                scan = true;
                            }
                        }
 
                        String name = "";

                        if (c == null && nAff != null && !scan) {
                            c = (Client) JOptionPane.showInputDialog(null, " " + l(lang, "V_OPTIONS"), l(lang, "V_CLIENT"), JOptionPane.QUESTION_MESSAGE, null, cashier.db.getClientV(nAff.toUpperCase(), db, stock), nom);
                        }

                        if (c == null) {
                            int n = JOptionPane.showConfirmDialog(frame, " " + l(lang, "V_URASHAKA") + "  " + l(lang, "V_KUMUSHYIRAMO") + "", nAff + " " + l(lang, "V_NTAWE"), JOptionPane.YES_NO_OPTION);
                            if (n == 0) {
                                if (stock.equals("RAMA")) {

                                    new ClientRamaGui(cashier.db, name, "", nAff, "", "");
                                } else {
                                    new PersonInterface(cashier.db, nom, nAff, "", "", "","","",0,stock);
                                }
                            }
                        }
                        if (c != null && c.VISA.contains("N")) {
                            JOptionPane.showMessageDialog(null, l(lang, "V_CLIENT") + " " + c.NOM_CLIENT + " " + c.PRENOM_CLIENT + " " + l(lang, "V_NEGATIF"), l(lang, "V_SORRY"), JOptionPane.INFORMATION_MESSAGE);
                            c = null;
                        } else if (c != null && c.DATE_EXP < today) {
                            int n = JOptionPane.showConfirmDialog(frame, "" + l(lang, "V_URASHAKA") + "  " + l(lang, "V_KONGERA") + " ", nAff + "  " + l(lang, "V_EXPIRER"), JOptionPane.WARNING_MESSAGE);

                            if (n == 0) {
                                if (stock.equals("RAMA")) {
                                    new ClientRamaGui(cashier.db, c);
                                } else {
                                    new PersonInterface(cashier.db, c);
                                }
                            }

                            c = null;
                        }
                    } else {
                        c = currentClient;
                    }
String ordonnance ="" ;
                    if (c != null) {
                        if (c.ASSURANCE.equals("CASHNORM") && c.NUM_AFFILIATION.equals("DIVERS")) {
                            nAff = (JOptionPane.showInputDialog(l(lang, "V_NAMEORSURNAME"), " "));
                            if (nAff != null) {
                                c.NOM_CLIENT = nAff;
                                c.PRENOM_CLIENT = "";
                                ordonnance = nAff;
                            }
                        }
                        Variable va = cashier.db.getParama("RUN", "blockMAIN");
                        if (va == null) {
                            va = new Variable("", "", "", "");
                        }

                        if (!va.value.contains(c.ASSURANCE)) {
                            if (c.ASSURANCE.equals(stock) || stock.equals("CHAMBRE")
                                    || stock.equals("SOCIETE") || 
                                    (stock.equals("CASHNORM"))) {

//String ID_CLIENT = cashier.db.getString( "id_client");
//String url = cashier.db.getString( "syncurl");
//String PWD = (JOptionPane.showInputDialog("CLIENT PWD", ""));
//String NAME_TIER= c.ASSURANCE;
//
//System.out.println(PWD +"  "+(PWD.length())+"  "+ (!NAME_TIER.equals("MMI"))+"  "+url+"  "+url.contains("http"));
//
//
//
//if(PWD!=null && PWD.length()==4 && !NAME_TIER.equals("MMI") && 
//   url  !=null && url.contains("http") )
//{
// 
//String USER = "ALL";
//String PACKAGE = "POS";   
//String CODES="";
// 
//            for (int i = 0; i < cashier.current.getProductList().size(); i++) {
//
//                Product p2 = cashier.current.getProductList().get(i); 
//                //GYNO-02;6;2193::MICON01;2;476::>>2014-02-03 09:44:04>>ALG000001101>>GOOD#
//                CODES+=p2.code+";"+p2.qty+";"+((int)p2.currentPrice)+"::";
//            }
//    
// CODES+= ">>NOW>>"+ID_CLIENT;
//  
//    String GET="ID_CLIENT="+ID_CLIENT+"&NAME_TIER="+NAME_TIER+"&PWD="+PWD+"&USER="+USER+"&CODES="+CODES+"&PACKAGE="+
//            PACKAGE+"&ID_TIER="+c.NUM_AFFILIATION;
//     GET=GET.replaceAll(" ", "+");
//  String patientFile="";
//    try {
//        patientFile = Db.kohereza (url+"/getPatientFile.php?"+GET,"");
//    } catch (Exception ex) {
//        Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
//    }
//  System.out.println(patientFile) ;    
//  
//  if(patientFile!= null && patientFile.contains("#"))
//  { 
//   JOptionPane.showConfirmDialog(null,new InteractionPane(patientFile,cashier.db));  
//  }
//  else
//  {
//   JOptionPane.showConfirmDialog(null,patientFile);    
//  }
//  
//}                            

                                int n = JOptionPane.showConfirmDialog(frame,
                                        "  NOM_CLIENT     =" + c.NOM_CLIENT
                                        + "\n PRENOM CLIENT  =" + c.PRENOM_CLIENT
                                        + "\n NUM_AFFILIATION=" + c.NUM_AFFILIATION
                                        + "\n BENEFICIAIRE =" + c.BENEFICIAIRE
                                        + "\n LIEN AVEC ADHERENT=" + c.LIEN
                                        + "\n PERCENTAGE     =" + c.PERCENTAGE
                                        + "\n ASSURANCE        =" + c.ASSURANCE
                                        + "\n DPT        =" + c.EMPLOYEUR,
                                        " " + l(lang, "V_FICHE") + nAff, JOptionPane.YES_NO_OPTION);
                                if (n == 0) {
                                    finishCredit(c,null,"");
                                }
                            } else {
                                JOptionPane.showMessageDialog(frame, stock + l(lang, "V_NUMAFFILIATION") + " " + l(lang, "V_DIFFERENT") + "  " + l(lang, "V_TARIF") + c.ASSURANCE, l(lang, "V_ERROR"), JOptionPane.PLAIN_MESSAGE);
                            }
                        } else {
                            JOptionPane.showMessageDialog(frame, l(lang, "V_ASSURANCE") + " " + c.ASSURANCE + " " + l(lang, "V _NOTACCEPT"), l(lang, "V_ERROR"), JOptionPane.WARNING_MESSAGE);
                        }

                    }
                }
                    else
                    {
                        JOptionPane.showMessageDialog(frame, "PLEASE USE CARD BUTTON. ", l(lang, "V_FICHE"), JOptionPane.WARNING_MESSAGE);
                    }
            }else if (!IJISHO.contains("FACTURE") && cashier.current.getProductList().size() == 0 && setT("", "FACTURE", stock)) {
                    proformaButton.setBackground(new java.awt.Color(203, 215, 253));
                }
            }
        });
    }

    
    public boolean finishCredit(Client c, SmartPatient patient,String readSmart) {
        String ordonnance = "";
        
                     if(live_Tarif.equals("MMI")|| live_Tarif.equals("RAMA"))
  {

   ordonnance = (JOptionPane.showInputDialog(frame, l(lang, "V_NUMERO") + " " + l(lang, "V_ORDONNANCE") + "  " + c.ASSURANCE, "" + ordonnance));
   ordonnance=ordonnance+"-"+ (JOptionPane.showInputDialog(frame, l(lang,"V_PRESCRIBER")+c.ASSURANCE));
   ordonnance=ordonnance+"-..";
   ordonnance=ordonnance+"-"+ (JOptionPane.showInputDialog(frame, l(lang,"V_HEALTH")+c.ASSURANCE,"P"));
   ordonnance=ordonnance+"-..";
   ordonnance=ordonnance+"-"+ (JOptionPane.showInputDialog(frame, l(lang,"V_NUMERO_DU_MEDECIN"),c.NOM_CLIENT));
   ordonnance= JOptionPane.showInputDialog(frame, " CONFIRME OU CHANGE SANS TOUCHER LES -  ",ordonnance);
    
  }
     else   if (!l(lang, "V_REFERENCE").contains("CHAMBRE")) {
            ordonnance = (JOptionPane.showInputDialog(frame, l(lang, "V_NUMERO") + " " + l(lang, "V_ORDONNANCE") + "  " + c.ASSURANCE, "" + ordonnance));
        } else {
            ordonnance = "" + cashier.db.getRes(c.NUM_AFFILIATION);
        }                                  // (JOptionPane.showInputDialog(frame, " Impamvu y'uburwayi "));
  
        double p = (((TOTALPRICE2 * c.PERCENTAGE) * 0.01));
 
        double ideni = 0;
        double money = 0;
        double rest = 0;
 
        if(patient!=null)
            {
                System.out.println("type:"+patient.Type2);
                if(patient.Type2==2 || patient.Type2==0)
                {
                    ideni = p;
                    displayMsg("TOTAL", RRA_PRINT2.setVirgule(p, 1,   ",") + " " + dev);
                    money = getAmafaranga(l(lang, "V_TOTAL") + " : " + RRA_PRINT2.setVirgule(p, 1,   ","), cashier.db.devise, 0);
            
                
                    if(((TOTALPRICE2 - p))>Double.parseDouble(patient.amount)||
                            Double.parseDouble(patient.amount)==0)
                    {
                        JOptionPane.showMessageDialog(frame, "YOU HAVE EXCEEDED YOUR INSURANCE AMOUNT, "
                                + "\n YOU ARE ALLOWED: "+RRA_PRINT2.setVirgule(patient.amount,",")+", "
                                + "\n PLEASE RECHECK YOUR INVOICE " + patient.Type2, 
                                l(lang, "V_ATTENTION"), JOptionPane.WARNING_MESSAGE);
                       // cashier.smartConnection.updateFlag2(patient.global_id, "1"," and progress_flag=5");
                        return false;
                    }
                }
                else if(patient.Type2==1)
                {
                    double amount_req=Double.parseDouble(patient.Amount_Required);
                    double diff=TOTALPRICE2-amount_req;
                    double cash=TOTALPRICE2-Math.min(amount_req, Double.parseDouble(patient.amount));
                    
                    if(cash<=0)
                    {
                        cash=0;
                        ideni=TOTALPRICE2-cash;
                        money = getAmafaranga(RRA_PRINT2.setVirgule(0,1, ",") + " " + l(lang, "V_ECRIRE") + " " + l(lang, "V_PAYED") + " : ", l(lang, "V_INT"), 0);
                        p = cash;
                        System.out.println(ideni + "" + diff);
                        c.PERCENTAGE = (int) (((double) ideni / (double) TOTALPRICE2) * 100);
                    }
                    else if(cash>0)
                    {
                        ideni=TOTALPRICE2-cash;
                        money = getAmafaranga(RRA_PRINT2.setVirgule((int) (cash),1, ",") + " " + l(lang, "V_ECRIRE") + " " + l(lang, "V_PAYED") + " : ", l(lang, "V_INT"), 0);
                        //money = ideni;
                        p = cash;
                        System.out.println(ideni + "" + diff);
                        c.PERCENTAGE = (int) (((double) ideni / (double) TOTALPRICE2) * 100); 
                    }
                }
                else
                {
                    JOptionPane.showMessageDialog(frame, "UNDEFINED CLIENT TYPE " + patient.Type2, l(lang, "V_ATTENTION"), JOptionPane.WARNING_MESSAGE);
               //     cashier.smartConnection.updateFlag2(patient.global_id, "9"," and progress_flag=5");
                    return false;
                }
            }
        
        else
        {
            if (c.ASSURANCE.equals("PARTICULIER")) {
                ideni = getAmafaranga((int) TOTALPRICE2 + " " + l(lang, "V_ECRIRE") + " " + l(lang, "V_PAYED") + " : ", l(lang, "V_INT"), 0);
                money = ideni;
                p = ideni;
                System.out.println(ideni + "" + TOTALPRICE2);
                c.PERCENTAGE = (int) (((double) ideni / (double) TOTALPRICE2) * 100);
            } else {
                ideni = p;
                displayMsg("TOTAL", RRA_PRINT2.setVirgule(p, 1,   ",") + " " + dev);
                money = getAmafaranga(l(lang, "V_TOTAL") + " : " + RRA_PRINT2.setVirgule(p, 1,   ","), cashier.db.devise, 0);
            }
    }
            
        rest = money - p;
        // Si c est pas assez d argent
        boolean done = true;
        if (rest >= 0 && rest < 50000) {
            done = false;
        }
        while (money != -1 && done) {

            if (money >= 0 && rest < 0) {
                money = getAmafaranga(money + " " + l(lang, "V_NTIHAGIJE") + "  ... " + l(lang, "V_TOTAL") + " : " + RRA_PRINT2.setVirgule(p, 1,  ","), l(lang, "V_INT"), 0);
            }
            if (rest >= 50000) {
                money = getAmafaranga(money + " " + l(lang, "V_NIMENSHI") + " ...  " + l(lang, "V_TOTAL") + " : " + RRA_PRINT2.setVirgule(p, 1,   ","), l(lang, "V_INT"), 0);
            }

            rest = money - p;
            if (rest >= 0 && rest < 50000) {
                done = false;
            }
        }

        if (rest >= 0 && money != -1 && rest < 50000) {
            
            System.out.println("money:"+money+":p:"+p+":percent:"+c.PERCENTAGE+":rest:"+rest);

            cashier.current.id_client = c.ASSURANCE;

            // Mise a jour du stock , du prix et du panier
//cashier.upDateStock();
            String EstInStock = cashier.estDansStock();
            if (EstInStock.equals("")) {
                cashier.current.tot = TOTALPRICE2;
                cashier.current.tva = TVAPRICE2;
                cashier.current.total();
                cashier.current.payed = p;
                Cashier.yose2 = Cashier.yose2 + p;
                cashier.current.client = c;

                if (ordonnance == null) {
                    ordonnance = "";
                }

                if (stock.equals("RAMA")) {
               PrintM2(c, ordonnance, TVAPRICE2, getString("fact"), rest,money);
                } else if (stock.equals("MMI")) {
                    JOptionPane.showMessageDialog(frame, RadioButton1(), null, JOptionPane.INFORMATION_MESSAGE);
                    System.out.println("IMPR:" + IMPR);
                    if (IMPR.equals("A5")) {
                        PrintM2(c, ordonnance, TVAPRICE2, "FACTUREMMI", rest,money);
                    } else if (IMPR.equals("EPSON")) {
                        Print(c, p, money, rest, TVAPRICE2, ordonnance);
                    }
                } else {
                    Print(c, p, money, rest, TVAPRICE2, ordonnance);
                }
             if(Main.tax.smartPatient!=null)
                {
                    cashier.db.updateSmartBLOB2(Taxes.smartXMLForward, Main.tax.smartPatient.global_id," AND KEY_INVOICE='"+Main.tax.key_invoice+"'");
                    Main.tax.smartPatient=null;
                }
             return true;
                //tangira(rest);
//total.setText(totalprice+ "  RWF ");
            } else {
                JOptionPane.showMessageDialog(frame, EstInStock + " " + l(lang, "V_PRODUCTISTAKEN"), l(lang, "V_ATTENTION"), JOptionPane.QUESTION_MESSAGE);
                return false;
            }
        } else {
            JOptionPane.showMessageDialog(frame, l(lang, "V_FACTURE") + "  " + l(lang, "V_ANNULEES"), l(lang, "V_ERROR"), JOptionPane.PLAIN_MESSAGE);
            cashier.basketStart(stock);
            tangira(rest);
            return false;
        }

    }

    public void setData()
    {
            TOTALPRICE2 = 0;TVAPRICE2=0;
            
            // Mise a jour d interface et du prix
            int c = cashier.current.getProductList().size();
            String[] control = new String[c];

            double tot=0;
            double tva=0;
 st="FACTURE#"+umukozi.NOM_EMPLOYE+"#5#"+umukozi.NOM_EMPLOYE+"#"+new Date()+"#" ;
 list="";
 
            for (int i = 0; i < c; i++) {

                Product p2 = cashier.current.getProductList().get(i);
                control[i] = p2.toSell(); 
                double taxable =((p2.salePrice()) / (1 + p2.tva)); 
                tot = tot + (p2.salePrice()); 
                tva = tva + (taxable * p2.tva); 
                 list+=""+p2.productCode+";"+p2.qty+";"+p2.currentPrice+";"+p2.productName+"::";
            }
            TVAPRICE2 =  (tva);
            TOTALPRICE2= tot; 
            
            if(TOTALPRICE2>1000000000)
            { 
            total.setFont(new Font(police.getName(), police.getStyle(), 15)); 
            total.setText(RRA_PRINT2.setVirgule( TOTALPRICE2,1 ,",") + " "+dev);  
            }
            else
            {
            total.setFont(new Font(police.getName(), police.getStyle(), 30)); 
            total.setText(RRA_PRINT2.setVirgule( TOTALPRICE2,1 ,",") + " "+dev); 
            
            }
            
            saleJList.setListData(control);
    }
    public void tangira(double rest) {
        saleJList.setListData(new String[0]);
        TOTALPRICE2 = 0;TVAPRICE2=0;
        reduction = 0;
        totReduction = 0;
        cashier.current.REDUCTION = 0;
        total.setText(RRA_PRINT2.setVirgule(rest,1, ",") + " " + dev);
        if (rest != 0) {
            if (Display) {
                try {
                    outputStream.write(26);
                    outputStream.write((l(lang, "V_RENDU") + " " + RRA_PRINT2.setVirgule(rest,1, ",")  + " " + dev).getBytes());
                    outputStream.write(10);
                    outputStream.write(13);
                    outputStream.write((l(lang, "V_MERCI") + "!!         ").getBytes());
                    //outputStream.write(("IKAZE MU ISHYIGA "+ getString("t1")).getBytes()); 

                } catch (Exception ex) {
                    System.out.println("" + ex);
                }
            }
            JOptionPane.showMessageDialog(frame, l(lang, "V_SUBIZA") + " " + RRA_PRINT2.setVirgule(rest,1, ","), l(lang, "V_MERCI"), JOptionPane.PLAIN_MESSAGE);
        }

        if (Display) {
            try {
//    outputStream.write(26);
//    outputStream.write(("IKAZE MURI "+ getString("t1")).getBytes()); 
//       
//               outputStream.write(26);
//outputStream.write(("IKAZE MU ISHYIGA ").getBytes()); 
////outputStream.write(("IKAZE MU ISHYIGA "+ getString("t1")).getBytes()); 
//                      outputStream.write(10);
//                       outputStream.write(13);
//  outputStream.write((" "+ stock).getBytes()); 
                Ikaze();

            } catch (Exception ex) {
                System.out.println("" + ex);
            }
        }
    }

    public void PrintM2(Client c, String quittance, double tva, String type, double rest,double money) {
         
        int aff = 0;
        try { 
            aff = (int) ((TOTALPRICE2 * (c.PERCENTAGE / 100.0)) + 0.5);
            System.out.println("************************************************************************");
            System.out.println("affilie:" + aff);

            cashier.current.footer = new String[]{"", "", "", "", "", "", "", "", "", ""};
            cashier.current.REDUCTION = (int) totReduction;
            try {
                System.out.println("tot-aff:" + (int) ((TOTALPRICE2 - aff)) + " total:" + TOTALPRICE2);
                cashier.BASKETEND(stock, new Credit(cashier.factNum, c.NUM_AFFILIATION, quittance,((TOTALPRICE2 - aff)), aff, "M"),money,rest,depotSql);
                if(sendDisplay)
                {
                 dc.sendRequest(st+list)  ; 
                }
                //cashier.db.doFinishCredit(new Credit(cashier.factNum, c.NUM_AFFILIATION, quittance, (int)((totalprice - aff)+0.5), aff, "M")); 
            } catch (Throwable e) {
                cashier.db.insertError(" facture igize ikibazo " + e, cashier.factNum + "print");
            }

            if (cashier.current.id_invoice != -1) {
                
                cashier.db.printA5(cashier.current.id_invoice, stock, umukozi,type); 
                cashier.basketStart(stock);
                tangira(rest);
            } 
        } catch (Throwable t) { // Anything else
            cashier.db.insertError(t + " erreur dans l'impression ", cashier.factNum + "print");
            cashier.basketStart(stock);
        }
    }
    
        public void PrintRRA(double money, double rest, double tva,String reference) 
        {
            try {
                
            cashier.BASKETEND(stock, new Credit(0, "CASHNORM", reference, 0,TOTALPRICE2, "M",money),
                    money,rest,depotSql);
            
if(sendDisplay)
{ dc.sendRequest(st+list)  ;  }
        } catch (Throwable e) {
            cashier.db.insertError(" facture igize ikibazo " + e, cashier.factNum + "print");
        }
            
        if (cashier.current.id_invoice != -1) {
            cashier.basketStart(stock);
           // PrintCommand(printStringUp, printStringE, printStringW, down, merci);
            tangira(rest);
        }
    }
 
   
    public void Print(Client c, double affilier, double money, double rest , double tva,String quittance) 
    {
        cashier.current.REDUCTION = (int) totReduction;
        String raison = "M";

        try {
            
            cashier.BASKETEND(stock, 
                    new Credit(cashier.factNum, c.NUM_AFFILIATION, quittance,((TOTALPRICE2 - affilier)), 
                    affilier, raison),money,rest,depotSql);

//cashier.db.doFinishCredit(new Credit(cashier.factNum, c.NUM_AFFILIATION, quittance, (int)((totalprice - affilier)+0.5), affilier, raison));
        } catch (Throwable e) {
            cashier.db.insertError(" facture igize ikibazo " + e, cashier.factNum + "print");

        }
   
        String down = "";
        System.out.println("nahageze print");

        if (cashier.current.id_invoice != -1) {
            cashier.basketStart(stock); 
            tangira(rest);
        }
    }
  public String PrintSilencieux
  (String name, double affilier, double money,
   double rest , double tva,String quittance,String waitress) 
    {
        cashier.current.tot=affilier;
        cashier.current.tva=tva;
        cashier.current.REDUCTION = (int) totReduction;
        String raison = "M";
        
 System.out.println("PrintSilencieux");
        try {
        String answer=  cashier.BASKETENDSILENCIEUX( 
                    new Credit(cashier.factNum, name, quittance,((TOTALPRICE2 - affilier)), 
                    affilier, raison),waitress);
            cashier.basketStart(stock); 
            tangira(rest);
 return answer;
//cashier.db.doFinishCredit(new Credit(cashier.factNum, c.NUM_AFFILIATION, quittance, (int)((totalprice - affilier)+0.5), affilier, raison));
        } catch (Throwable e) {
            return (" facture igize ikibazo " + e+ cashier.factNum + "print");

        }
    
    }

    public int EndProforma (String NAME_CLIENT, int money, String quittance, int tva,String mode,String ext_key) {
        System.out.println("nahageze print1:");
 

 LinkedList<Product> listProduct=cashier.current.getProductListDB();
 
        for (int i = 0; i < listProduct.size(); i++) {
            Product p = listProduct.get(i);
        System.out.println(p.productName+"   "+p.qty+"  "+p.currentPrice);
        } 
        
          setData();
          
          cashier.current.tot=TOTALPRICE2;
          cashier.current.tva=TVAPRICE2; 
        cashier.current.REDUCTION = totReduction;
int id=-1;
try {   
cashier.current.id_invoice = cashier.db.dofinishProformaV7(
        cashier.current, 
        quittance, NAME_CLIENT,this,mode,ext_key);
id=cashier.current.id_invoice;
System.out.println("out "+id ); 
} catch (Throwable e) {
    cashier.db.insertError(" facture igize ikibazo " + e, cashier.current.id_invoice + "print");
    }   
       
if(!tax.rraClient.isRRA)
{            
    
Client client=new Client("", NAME_CLIENT, quittance, 0, 0, cashier.current.id_client, "", "", "", ""); 

LinkedList<String> Clientinfo=cashier.db.Leclient(client);
RRA_PACK  invoicePack=Main.tax.getRpack4(0,listProduct ,
 "P", "S","",Main.tax.MRC2.MRC);   
 
        int margin=5;
        String marginVar=cashier.db.getRRA("margin");
        try
        { margin=Integer.parseInt(marginVar); }
        catch (NumberFormatException ex)
        {System.err.println(marginVar +"   marginVar   "+ex); }
 RRA_PRINT2 printer =new  RRA_PRINT2
(cashier.db.getRRA("company"), getString("bp"), cashier.db.getRRA("City"),
     cashier.db.getRRA("tin"), ""+invoicePack.ClientTin,
invoicePack.getTotal(),invoicePack.getTotalA(),
invoicePack.getTotalB(), invoicePack.getTotalTaxes(),
invoicePack.getTotalTaxes(),invoicePack.getTotal() ,
"",""+"/"+""+"  "+"","","",
cashier.current.id_invoice,invoicePack.MRC,listProduct
,"P","S",cashier.db.getRRA("version"),1,0,Clientinfo,
        Main.tax.getNowTime(cashier.db.getMRCtimeProfo(id)),"",
         cashier.current.id_employe
         ,quittance,cashier.db.s,""+NAME_CLIENT,margin,0,0);  
}
cashier.basketStart(stock); 
    return id;
}

    private void PrintCommand  (String printStringUp, String printStringE, String printStringW, String down, String merci) {
        System.out.println("ok:" + printStringUp + "  :" + printStringE + "  " + printStringW + " " + down + " " + merci);
        PrintJob pjob = getToolkit().getPrintJob(Main.this, "Ishyiga ", properties);
        if (pjob != null) {
            Graphics pg = pjob.getGraphics();


            if (pg != null) {
                System.out.println("cmdprint");
                printLongString(pjob, pg, printStringUp, printStringE, printStringW, down, merci);
                pg.dispose();
            }
            pjob.end();
        }
        System.out.println("nahageze cmdprint");

    }
    // I'm assuming a one-inch margin on all
    // four sides. This could be done better.
    private int margin = 0;

    // Print string to graphics via printjob
    // Does not deal with word wrap or tabs
    private void printLongString(PrintJob pjob, Graphics pg, String printStringUp, String printStringE, String printStringW, String down, String merci) {
        try {
            margin = Integer.parseInt(getString("marginEpson"));
            System.out.println("marginEpson we:" + margin);

        } catch (Throwable e) {
            cashier.db.insertError("marginEpson", "" + e);
        }
        System.out.println("page num we:");
        int pageNum = 1;
        int linesForThisPage = 0;
        int linesForThisJob = 0;
        // Note: String is immutable so won't change while printing.
        if (!(pg instanceof PrintGraphics)) {
            throw new IllegalArgumentException("Graphics context not PrintGraphics");
        }


        StringReader sr = new StringReader(printStringE);
        LineNumberReader lnr = new LineNumberReader(sr);

        StringReader sr1 = new StringReader(printStringW);
        LineNumberReader lnr1 = new LineNumberReader(sr1);

        StringReader sr2 = new StringReader(printStringUp);
        LineNumberReader lnr2 = new LineNumberReader(sr2);


        String nextLine;
        String nextLineE;
        String nextLineW;

        int pageHeight = pjob.getPageDimension().height - margin;
        Font helv = new Font("Helvetica", Font.PLAIN, Integer.parseInt(getString("boldEpson")));
        //have to set the font to get any output
        pg.setFont(helv);
        FontMetrics fm = pg.getFontMetrics(helv);
        int fontHeight = fm.getHeight();
        int fontDescent = fm.getDescent();
        int curHeight = margin;


        try {
            do {
                nextLine = lnr2.readLine();
                if (nextLine != null) {
                    if ((curHeight + fontHeight) > pageHeight) {
                        // New Page
                        //System.out.println(linesForThisPage + " lines printed for page " + pageNum);
                        if (linesForThisPage == 0) {
                            //System.out.println( "Font is too big for pages of this size; aborting...");
                            break;
                        }
                        pageNum++;
                        linesForThisPage = 0;
                        pg.dispose();
                        pg = pjob.getGraphics();
                        if (pg != null) {
                            pg.setFont(helv);
                        }
                        curHeight = 0;
                    }
                    curHeight += fontHeight;
                    if (pg != null) {
                        if (linesForThisPage == 2) {
                            //  pg.drawString("Ammoxyciline 200 mg ", margin, curHeight - fontDescent);
                            //pg.drawString("10.000 ", margin+170, curHeight - fontDescent);
                        }
                        pg.drawString(nextLine, margin, curHeight - fontDescent);

                        linesForThisPage++;
                        linesForThisJob++;

                    } else {
                        //System.out.println("pg null");
                    }
                }
                System.out.println("weeeeeeeeeeeeeeeeeeee");
            } while (nextLine != null);

            do {
                nextLineE = lnr.readLine();
                nextLineW = lnr1.readLine();

                System.out.println("nextline" + nextLineE + "  w:" + nextLineW);

                if (nextLineW != null) {
                    if ((curHeight + fontHeight) > pageHeight) {
                        // New Page
                        //System.out.println(linesForThisPage + " lines printed for page " + pageNum);
                        if (linesForThisPage == 0) {
                            //System.out.println( "Font is too big for pages of this size; aborting...");
                            break;
                        }
                        pageNum++;
                        linesForThisPage = 0;
                        pg.dispose();
                        pg = pjob.getGraphics();
                        if (pg != null) {
                            pg.setFont(helv);
                        }
                        curHeight = 0;
                    }
                    curHeight += fontHeight;
                    if (pg != null) {

                        int size = 35;
                        int west = 240;
                        try {
                            size = Integer.parseInt(getString("size"));
                            west = Integer.parseInt(getString("west"));
                        } catch (Throwable t) { // Anything else
                            cashier.db.insertError(t + "", "print j");
                        }

                        printInt(pg, nextLineW, west, curHeight - fontDescent);//dessiner les prix sur la facture epson.
                        if (nextLineE.length() > size) {
                            //nextLineE = nextLineE.substring(0, 34) ;
                            pg.drawString(nextLineE.substring(0, (size - 1)), margin, curHeight - fontDescent);
                            curHeight += fontHeight;
                            nextLineE = "       " + nextLineE.substring((size - 1), nextLineE.length());

                        }
                        pg.drawString(nextLineE, margin, curHeight - fontDescent);

                        // pg.drawString(nextLineW, margin+170+(5-nextLineW.length()), curHeight - fontDescent); 
                        linesForThisPage++;
                        linesForThisJob++;

                    } else {
                        //System.out.println("pg null");
                    }
                }
            } while (nextLineE != null);

            helv = new Font("Helvetica", Font.BOLD, Integer.parseInt(getString("merciBold")));
            //have to set the font to get any output
            pg.setFont(helv);
            pg.drawString(merci, margin + Integer.parseInt(getString("merciX")), curHeight + Integer.parseInt(getString("merciY")));

            helv = new Font("Helvetica", Font.BOLD, 7);
            //have to set the font to get any output
            pg.setFont(helv);
            pg.drawString(down, margin + 10, curHeight + 20);

        } catch (EOFException eof) {
            cashier.db.insertError(eof + "", "print");
        } catch (Throwable t) { // Anything else
            cashier.db.insertError(t + "", "print");
        }

        System.out.println("koko?");
    }

    public void printInt(Graphics pg, String s, int w, int h) {

        int back = 0;

        for (int i = s.length() - 1; i >= 0; i--) {
            if (s.charAt(i) == ' ') {
                back += 2;
            } else {
                back += 5;
            }

            pg.drawString("" + s.charAt(i), w - back, h);

        }

    }

    /**********************************************************************************************
    EXIT POUR deconnecte l'usager qui quitte MAJ DE LA BASE DE DONNES
     **********************************************************************************************/
    @Override
    protected void processWindowEvent(WindowEvent evt) {
        if (WindowEvent.WINDOW_CLOSING == evt.getID()) {
            //default icon, custom title
            int n = JOptionPane.showConfirmDialog(frame, l(lang, "V_EXITISHYIGA"), l(lang, "V_ATTENTION"), JOptionPane.YES_NO_OPTION);
            if (n == 0) {
                //                try {
                //                    LinkedList<String> list = cashier.db.getFichier("log.txt");
                //                    AutoOut.backUpDatabase(cashier.db.connMysql, list.getFirst());

                //
                //                } catch (Throwable ex) {
                //                    cashier.db.insertError(ex + "", "back up");
                //
                //                }
                saleJList.setListData(new String[0]);

                try {
                    //cashier.db.doFinishSales(cashier.todaySales);
                    //cashier.db.doFinishCredit(cashier.credit);
                    cashier.db.insertLogin2(nom, "OUT", Cashier.yose2);

                    String sto = " UMUKESIYE : " + nom + " ARARANGIJE ";
                    sto = sto + "\n IGIHE :  " + new Date().toLocaleString();
                    String sto1 = "\n-----------------------";
                    String sto2 = "\n----------------------- ";
                    //sto1=sto1+"\n";  sto2=sto2+"\n ";
                    sto1 = sto1 + "\n MWAKOREYE AMAFARANGA :  ";
                    sto2 = sto2 + "\n" + RRA_PRINT2.setVirgule(Cashier.yose2,1, ",");
                    sto1 = sto1 + "\n-----------------------";
                    sto2 = sto2 + "\n----------------------- ";
                    sto1 = sto1 + "\n ";
                    sto2 = sto2 + "\n ";
                    sto1 = sto1 + "\n           IBIHE BYIZA";
                    sto2 = sto2 + "\n ";
                    //sto1=sto1+"\n";  sto2=sto2+"\n ";
                    // sto1=sto1+"\n";
                    if (umukozi.PWD_EMPLOYE.contains("TICKET")) 
                    {
                        PrintCommand(sto, sto1, sto2, "", "");
                        JOptionPane.showMessageDialog(frame, "CASHIER AMMOUNT : "+RRA_PRINT2.setVirgule(Cashier.yose2,1, ",")+"FRW", l(lang, "V_MERCI"), JOptionPane.INFORMATION_MESSAGE);
                   }
           
                    JOptionPane.showMessageDialog(frame, l(lang, "V_MERCI") + " " + l(lang, "V_USE"), l(lang, "V_MERCI"), JOptionPane.INFORMATION_MESSAGE);

                    System.out.println("DIS" + Display);
                    if (Display) {
                        try {
                            // outputStream.write(("IKAZE MURI "+ getString("t1")).getBytes());

                            //                             outputStream.write(26);
                            //outputStream.write(("IKAZE MU ISHYIGA ").getBytes()); 
                            ////outputStream.write(("IKAZE MU ISHYIGA "+ getString("t1")).getBytes()); 
                            //                      outputStream.write(10);
                            //                       outputStream.write(13);
//  outputStream.write((" "+ stock).getBytes()); 
                            Ikaze();
                        } catch (Exception ex) {
                            System.out.println("ERROR:" + ex);
                        }
                    }
                    if (Display) {
                        serialPort.close();
                    }

                    cashier.db.closeConnection();

                } 
                catch (Throwable e) {
                    System.out.println("ER:" + e);
                    cashier.db.insertError(e + "", "processWindowEvent");
                }
                // JOptionPane.showMessageDialog(frame,Cashier.yose+" RWF","UFITE MURI CAISSE ",JOptionPane.PLAIN_MESSAGE);

                System.exit(0);
            }
        }
    }

    public static double getAmafaranga(String s, String s1, int i) {
        double entier = -1;

        String in = JOptionPane.showInputDialog(null, s + s1);
        boolean done = true;

        while (in != null && i < 3 && done == true) {
            try {
                if (in.equals("")) {
                    i++;
                    in = JOptionPane.showInputDialog(null, s + " : ENTER A NUMBER", "1");
                } else {
                    Double in_int = new Double(in);
                    entier = in_int.doubleValue();
                    done = false;
                    if (entier < 0) {
                        entier = -1;
                    }
                }

            } catch (NumberFormatException e) {
                i++;
                in = JOptionPane.showInputDialog(null, s + " = " + in + " NOT A NUMBER ; ENTER A NUMBER ");
                done = true;
            }
        }
        if (i == 3) {
            JOptionPane.showMessageDialog(frame, " TOO MANY TRY "," WE HAVE TO EXIT. ", JOptionPane.PLAIN_MESSAGE);
        }

        return entier;
    }

    public static int getQuantity(String s, String s1, int i) {
        int entier = -1;

        String in = JOptionPane.showInputDialog(null, s + s1);
        boolean done = true;

        while (in != null && i < 3 && done == true) {
            try {
                if (in.equals("")) {
                    i++;
                    in = JOptionPane.showInputDialog(null, s + " : ENTER A NUMBER ", "1");
                } else {
                    Integer in_int = new Integer(in);
                    entier = in_int.intValue();
                    done = false;
                    if (entier < 0 || entier == 0) {
                        entier = -1;
                    }
                }

            } catch (NumberFormatException e) {
                i++;
                in = JOptionPane.showInputDialog(null, s + " = " + in + " NOT A NUMBER ; ENTER A NUMBER ");
                done = true;
            }
        }
        if (i == 3) {
            JOptionPane.showMessageDialog(frame, " TOO MANY TRY ", " WE HAVE TO EXIT ", JOptionPane.PLAIN_MESSAGE);
        }

        return entier;
    }

    public static int getIn(String s, String s1, int i) {
        int entier = -1;

        String in = JOptionPane.showInputDialog(null, s + s1);
        boolean done = true;

        while (in != null && i < 3 && done == true) {
            try {
                if (in.equals("")) {
                    i++;
                    in = JOptionPane.showInputDialog(null, s + " Enter a number ", "1");
                } else {
                    Integer in_int = new Integer(in);
                    entier = in_int.intValue();
                    done = false;
                    if (entier < 0) {
                        entier = -1;
                    }
                }

            } catch (NumberFormatException e) {
                i++;
                in = JOptionPane.showInputDialog(null, s + " = " + in + "= Not a number; Enter a number ");
                done = true;
            }
        }
        if (i == 3) {
            JOptionPane.showMessageDialog(frame, " Too many try ", " Sorry we have to Exit ", JOptionPane.PLAIN_MESSAGE);
        }

        return entier;
    }

        public static double getIn(String in) {
            double productPrice = 0;
            try {
                if (in.equals("")) {
                    productPrice = 0;
                } else {
                    Double in_int = new Double(in);
                    productPrice = in_int.doubleValue();
                }
            } catch (NumberFormatException e) {
                productPrice = 0;
            }
            return productPrice;
        }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public void aboutIshyiga() {
        aboutButton.addActionListener(
                new ActionListener() {

                    public void actionPerformed(ActionEvent ae) {
                        if (ae.getSource() == aboutButton) {
                            String c = (String) JOptionPane.showInputDialog(null, " Faites votre choix  ! !", "A PROPOS ",
                                    JOptionPane.QUESTION_MESSAGE, null, new String[]{"VERSION","ASK ONLINE", "CALCULETTE", "A PROPOS", "INFO WEB", "GUIDE"}, "");

                            if (c == null) {
                            } else if (c.equals("VERSION")) {
        
 panel=new Pane2();
 JPanel js=new JPanel();
 js.setLayout(new GridLayout(5,1,5,20));//
 js.add(new JLabel(" SOFTWARE NAME : ISHYIGA POS"));
 js.add(new JLabel(" VERSION : ISHYIGA POS CAISSE "+versionIshyiga ));
 js.add(new JLabel("ALGORITHM INCORPORATION RDB 101909872 "));
js.add(new JLabel(" COPYRIGTH  :RWC-10000331  RDB Ã‚Â©"));
js.add(new JLabel("INTELLECTUAL PROPERTY LAW Art. 195, 197")); 
JPanel pp=new JPanel();
pp.setLayout(new GridLayout(1,2,5,20));//
pp.setPreferredSize(new Dimension(600,200)); //400=largeur350=longeur
pp.add(panel); 
pp.add(js);


final SwingWorker<String , String> worker = new SwingWorker<String, String>() 
{
@Override protected String doInBackground() throws Exception { 
      panel.win7();       
 return  " "; 
}
@Override protected void done() { } 
@Override  protected void process(java.util.List<String> chunks) {  }   };
             worker.execute();  
             worker.addPropertyChangeListener(new PropertyChangeListener() {
@Override public void propertyChange(PropertyChangeEvent evt) {
if (evt.getPropertyName().equals("progress")) {  } } }); 
   
JOptionPane.showMessageDialog(null,pp ,
                "VERSION INFORMATION", JOptionPane.INFORMATION_MESSAGE);
worker.cancel(true);

    }else if (c.equals("ASK ONLINE")) {
                                new Askquestions(cashier.db, umukozi);
                            } else if (c.equals("CALCULETTE")) {
                                new Calculatrice();
                            } else if (c.equals("GUIDE")) //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                            {
                                new OpenFileUsingJFileChooser2();

                            } /////////////////////////
                            else if (c.equals("A PROPOS")) //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                            {
                                new AboutIshyigaGUI();

                            } /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                            else if (c.equals("INFO WEB")) {
                                //               JOptionPane.showConfirmDialog(null,  "Le lien sur le web n'est pas encore disponible !!!","Erreur!",JOptionPane.YES_NO_OPTION);
                                try {
                                    String url = "www.ishyiga" + cashier.db.getString("extension") + "";
                                    Process p = Runtime.getRuntime().exec("\"C:\\Program Files\\Internet Explorer\\iexplore.exe\" " + url);
                                } catch (IOException ex) {
                                    JOptionPane.showMessageDialog(null, l(lang," V_INTERNE_CONNECTION ") + ex, "CONNEXION !", JOptionPane.WARNING_MESSAGE);
                                }
                            }
                        }
                    }
                });
    }
    
    private void writeLot() 
{ 
      Thread  thread= new Thread(new Backup());
        thread.start(); 
}
    
    public  class Backup implements Runnable
{
    @Override
    public void run(){
        
   String  files="STOCK/STOCK_DU_"+cashier.current.date+".txt";   
    File file = new File(files);
  
    {
        try
        {
        LinkedList<Lot> stock=Db.getStock( cashier.db.conn.createStatement()); 
        System.out.println("not exist"+files+"**"+stock.size());  
            PrintWriter sorti=new PrintWriter(new FileWriter(file),true);  
            for(int j=0;j<stock.size();j++)
            {
                Lot C  = stock.get(j);   
                
                String factureSage=""+C.u1+";"+C.u3+";"+C.u2+";"+C.u4+";"+C.u5+";"+C.u6+";"+C.u7+";"+C.u8+";"
                        +C.u9+";"+C.u0+";"+C.u11; 
                sorti.println(factureSage); 
            } 
            sorti.close();
        
//            aos.zamura2("INVOICE"); 
            //aos.zamura ("REFUND" ) ;
            //aos.zamuraItems(cashier.db.getString("syncurl"),cashier.db.connMysql.createStatement());  
//            aos.zamuraStock();  
        } 
        catch (Throwable e) 
        { cashier.db.insertError(e+"","WRITELOT"); }
      }
    }
}  

     String l(String lang, String var) {
        return cashier.db.l(lang, var);
    }

    public void lesClients() {
        lesClients.addMouseListener(new MouseListener() {

            public void actionPerformed(ActionEvent ae) {
            }

            public void mouseExited(MouseEvent ae) {
            }

            public void mouseClicked(MouseEvent e) {
                if (lesClients.getSelectedValue() != null) {
    currentClient=(Client)lesClients.getSelectedValue();
    String client= "  credit.numero_affilie='"+currentClient.NUM_AFFILIATION+"'";
//    System.out.println("njnj "+client);
    String heure = " and invoice.HEURE >'"+akiraDateSql(dateIn.getDate(), "00:00:00")+"' AND invoice.HEURE <'"+
    akiraDateSql(dateOut.getDate(), "23:59:00")+" ' ";   
    String status= "";
    lesFactures.setListData(cashier.db.getPatientFile (client,heure, status).toArray());
                }
            }

            ;  
    public void mousePressed(MouseEvent e) {
            }

            public void mouseReleased(MouseEvent e) {
            }

            public void mouseEntered(MouseEvent e) {
            }
        });
    }
 public JPanel RadioButton() {
//Create the radio buttons.
JRadioButton ALL = new JRadioButton("ALL");
ALL.setMnemonic(KeyEvent.VK_P);
ALL.setActionCommand("ALL"); 
ALL.setSelected(true);
JRadioButton OPEN = new JRadioButton("OPENED");
OPEN.setMnemonic(KeyEvent.VK_L);
OPEN.setActionCommand("OPEN"); 

JRadioButton CLOSED = new JRadioButton("CLOSED");
CLOSED.setMnemonic(KeyEvent.VK_L);
CLOSED.setActionCommand("CLOSED"); 
 

ButtonGroup group = new ButtonGroup();
group.add(ALL);
group.add(OPEN);
group.add(CLOSED);

//Register a listener for the radio buttons.
OPEN.addActionListener(new ActionListener()  {
            public void actionPerformed(ActionEvent ae) {
                IMPR=ae.getActionCommand();
            }
                
            });

CLOSED.addActionListener(new ActionListener()  {
            public void actionPerformed(ActionEvent ae) {
                IMPR=ae.getActionCommand();
            }  
            });
ALL.addActionListener(new ActionListener()  {
            public void actionPerformed(ActionEvent ae) {
            
   if(lesClients.getSelectedValue()!=null)
   {
     //  System.out.println("njnj ");
    Client cli=(Client)lesClients.getSelectedValue();
    String client= "  credit.numero_affilie='"+cli.NUM_AFFILIATION+"'";
    String heure = " and invoice.HEURE >'"+akiraDateSql(dateIn.getDate(), "00:00:00")+"' AND invoice.HEURE <'"+
            akiraDateSql(dateOut.getDate(), "23:59:00")+" ' ";   
    String status= "";
   lesFactures.setListData(cashier.db.getPatientFile (client,heure, status).toArray());
   }
            }  
            });
//Put the radio buttons in a column in a panel.
JPanel radioPanel = new JPanel(new GridLayout(1, 3));
radioPanel.add(ALL); 
radioPanel.add(OPEN); 
radioPanel.add(CLOSED);

add(radioPanel, BorderLayout.LINE_START);
//  setBorder(BorderFactory.createEmptyBorder(20,20,20,20));
return radioPanel;
} 

    String akiraDateSql(Date d, String heure) {
        String day = "";
        String mm = "";
        String year = "";

        if (d.getDate() < 10) {
            day = "0" + d.getDate();
        } else {
            day = "" + d.getDate();
        }
        if ((d.getMonth() + 1) < 10) {
            mm = "0" + (d.getMonth() + 1);
        } else {
            mm = "" + (d.getMonth() + 1);
        }
        if ((d.getYear() - 100) < 10) {
            year = "0" + (d.getYear() - 100);
        } else {
            year = "" + (d.getYear() - 100);
        }
        String checkin = "20" + year + "-" + mm + "-" + day + " " + heure;
        System.out.println(checkin);
        return checkin;
    }

    SMART_MATRIX getCardInfo2(String reason, SmartPatient card,Statement s, Connection conn)
    {
        SMART_MATRIX sm=cashier.db.getInsuranceScheme (reason,card,s,conn);
        
        String assurance=null;
if(sm!=null) { assurance =sm.DES_INSURANCE; }

if(sm==null)
{  return null;  } 
else if(!(assurance!=null &&  assurance.contains(live_Tarif)  || live_Tarif.equals("SOCIETE")))
{
JOptionPane.showMessageDialog(frame, assurance+
" CARD TARIF DIFFERENT FROM CURRENT TARIF "+live_Tarif, l(lang, "V_ATTENTION"), JOptionPane.WARNING_MESSAGE);
return null;}
else if(TOTALPRICE2>sm.MAXIMUM_AMOUNT_INVOICE)
{
JOptionPane.showMessageDialog(frame, sm.MAXIMUM_AMOUNT_INVOICE+
" CARD MAXIMUM_AMOUNT_INVOICE > FROM CURRENT TOTAL  "+TOTALPRICE2, l(lang, "V_ATTENTION"), JOptionPane.WARNING_MESSAGE);
return null;}
else if(cashier.current.getProductList().size()>sm.MAXIMUM_ITEM_INVOICE)
{
JOptionPane.showMessageDialog(frame, sm.MAXIMUM_ITEM_INVOICE+
" CARD MAXIMUM_ITEM_INVOICE > FROM CURRENT  LENGTH "+TOTALPRICE2, l(lang, "V_ATTENTION"), JOptionPane.WARNING_MESSAGE);
return null;}

sm.ID_INSURANCE=live_Tarif;

return sm;
    }
    
    public static void main(String[] arghs) {
        StartServer d;
        try {
            d = new StartServer();
//            System.out.println(d.ip);
            int id = getQuantity("IDENTIFICATION", " :", 0);
//            int id=1;
            if (id != -1) {

                JPanel pw = new JPanel();
                JPasswordField passwordField = new JPasswordField(10);
                JLabel label = new JLabel(" PASSWORD ");
                pw.add(label);
                pw.add(passwordField);
                passwordField.setText("");
                JOptionPane.showConfirmDialog(null, pw);
                Integer in_int = new Integer(passwordField.getText());
                int carte = in_int.intValue();
//                int carte = 1;

                LinkedList<String> give = new LinkedList();

                File productFile = new File("log.txt");
                String ligne = null;
                BufferedReader entree = new BufferedReader(new FileReader(productFile));
                try {
                    ligne = entree.readLine();
                } catch (IOException e) {
                    System.out.println(e);
                }
                int i = 0;
                while (ligne != null) {
                    // System.out.println(i+ligne);
                    give.add(ligne);
                    i++;

                    try {
                        ligne = entree.readLine();
                    } catch (IOException e) {
                        JOptionPane.showMessageDialog(null, "QUESTION ", "CHECK LOG FILE", JOptionPane.PLAIN_MESSAGE);
                    }
                }
                entree.close();

   //JOptionPane.showMessageDialog(null," cashier"," in ",JOptionPane.PLAIN_MESSAGE);

                double frw = getAmafaranga("DEPOSIT ", " : INT", 0);
                // String dataBase  = (JOptionPane.showInputDialog(frame, " dataBase ","score"));
//String serv="localhost";

                String dataBase = give.get(2);
                String serv = give.get(1);
                defaultPort = give.get(4);
                special = give.get(6);
                dev = " " + give.get(5) + " ";
                
                String smartdb="",smartserver="";
                try
                {
                    smartserver=give.get(13);
                    smartdb=give.get(14); 
                }
                catch(Exception ee)
                {
                    smartdb="NOT AVAILABLE";
                    smartserver="NOT AVAILABLE"; 
                }
                
//                isRRA1= Boolean.parseBoolean(give.get(6));
                System.out.println("default com:" + defaultPort);
                
                if (defaultPort.contains("COM")) {
                    Display = true;
                } else {
                    Display = false;
                }
 //JOptionPane.showMessageDialog(null," cashier"," in 3 ",JOptionPane.PLAIN_MESSAGE);
                // String serv  = (JOptionPane.showInputDialog(frame, " Server ","192.168.0.100"));
                // String serv="localhost";

                if (carte != -1 && frw != -1) 
                {
                //     JOptionPane.showMessageDialog(null," cashier"," in 4 ",JOptionPane.PLAIN_MESSAGE);
                    tax=new Taxes(serv,serv, dataBase);
                 //    JOptionPane.showMessageDialog(null," cashier"," in 5 ",JOptionPane.PLAIN_MESSAGE);
                    Cashier c = new Cashier(id, serv, dataBase, dev,smartserver,smartdb);
                   // c.smartConnection= new MySQLConnector(smartserver,   smartdb, c.db);
                    c.cashierId=""+id;
                    c.db.ip = d.ip;
                    c.db.special = special; 

                //         JOptionPane.showMessageDialog(null," cashier"," in ",JOptionPane.PLAIN_MESSAGE);

                    if (c != null && c.db.check_db(id, carte)) {
                        Employe n = c.db.getName(id);
                        Cashier.yose2 = frw;
                        c.db.insertLogin2(n.NOM_EMPLOYE, "IN", frw);
                        //JOptionPane.showMessageDialog(null," Main"," in ",JOptionPane.PLAIN_MESSAGE);
                        new Main(n, c, serv, dataBase, give.get(3));
                    } else {
                        JOptionPane.showMessageDialog(null, "ACCESS DENIED (PASSWORD)","ERROR", JOptionPane.PLAIN_MESSAGE);
                    }
                }
            }
        } catch (Throwable t) {
            JOptionPane.showMessageDialog(null, "SERVER IS OFFLINE" + t, "ERROR", JOptionPane.PLAIN_MESSAGE);
        }
    }
}
