package pos;

import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Toolkit;
import javax.swing.JLabel;
import javax.swing.JFrame;
import javax.swing.ImageIcon;

public class AboutIshyigaGUI extends JFrame {
private ImageIcon aboutIshyigaIcon = new ImageIcon("image/About-Ishyiga.png");
String title="------------------------------------------- [ A propos du logiciel Ishyiga ] --------------------------------------- " ;

public AboutIshyigaGUI() {

this.setTitle(title); 
Image im = Toolkit.getDefaultToolkit().getImage("logo.png");
    this.setIconImage(im);  
this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
this.setSize(890, 460);  
this.setResizable(false);
        
setLayout(new GridLayout(1,0,0,0));
add(new JLabel(aboutIshyigaIcon));
this.setVisible(true);
}

public static void main(String[] args) {

    AboutIshyigaGUI about=new AboutIshyigaGUI();

}
}