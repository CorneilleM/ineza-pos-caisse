
package pos;

import java.awt.*;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Date;
import java.util.LinkedList;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author Kimenyi
 */
public class PrintDoc extends JFrame {


int total,tvaPrice,solde,choix;

String Reference;
private Properties properties = new Properties();
LinkedList <Credit> credit;
Assurance ass;
LinkedList <Variable> par;
private int margin;
int pageNum = 0;
String ink=""; 
Db db;
PrintDoc(LinkedList <Credit> credit,Assurance client,String server,String dataBase)
{
//System.out.println(client.date);
  
    ink = JOptionPane.showInputDialog(null, " Numero Facture "," ");
 this.db= new Db(server,dataBase);
 //System.out.println("server:"+server+":"+dataBase);
db.read();
  par=db.getParam("PRINT");
 margin=getValue("margin",par);
 this.credit=credit; 
 this.ass=client;
 this.total=0;
 this.solde=0;
 this.tvaPrice=0;

PrintCommand();
}

private void PrintCommand()
{
 PrintJob pjob = getToolkit().getPrintJob(this, "Ishyiga", properties);
         if (pjob != null) {

     
        Graphics pg = pjob.getGraphics();

        if (pg != null) {
           if(ass.sigle.contains("CLIENT"))
            printLongStringDebit(pjob, pg);
           else if(ass.sigle.equals("CASHNORM"))
             printLongStringCash(pjob, pg);
            else
            printLongStringCredit(pjob, pg);
           pg.dispose();
        }
        pjob.end();
      }
}

 int getValue(String s,LinkedList <Variable> par)
 {
     int ret=0;
//System.out.println("par size:"+par.size());
     for(int i=0;i<par.size();i++)
     {
         String sa=par.get(i).nom;
         
            if(sa.equals(s))
            {
               // System.out.println("sa:"+sa);
Double val=new Double (par.get(i).value);
//System.out.println("val:"+val);
ret = val.intValue();
//System.out.println("ret:"+ret);
            } 
      //  System.out.println(s+"   "+sa); 
     } 
     return ret;
 }
public Graphics header(Graphics pg,int pageH,int pageW)
{

    return pg;
}
private void printLongStringDebit (PrintJob pjob, Graphics pg) {

   

    // Note: String is immutable so won't change while printing.

    if (!(pg instanceof PrintGraphics)) {
      throw new IllegalArgumentException ("Graphics context not PrintGraphics");
    }

    int pageH = pjob.getPageDimension().height - margin;
    int pageW = pjob.getPageDimension().width - margin;


if (pg != null)
  {
 int down =pageH-120;
//////////////////////////////////////////////////////////////////////////////////////////////////////

    int curHeight = margin;

    /// ENTETE
       ////Toolkit toolkit = getToolkit();
        // create the image using the toolkit
      //Image  img = toolkit.createImage("uni.gif");
     // pg.drawImage(img, 0, 0, this );
Font helv = new Font("Helvetica", Font.BOLD, 10);
//have to set the font to get any output
pg.setFont (helv);
// Toolkit toolkit = getToolkit();
//Image img =  toolkit.createImage("uni.gif");
String date=ass.date;

pg.drawString("Kigali, le "+date.substring(0, 2)+"/"+date.substring(2, 4)+"/"+date.substring(4, 6), pageW - getValue("dateX",par), getValue("dateY",par));
 helv = new Font("Helvetica", Font.BOLD, 25);
pg.setFont (helv);
pg.drawString("PHARMAVIE", getValue("logoX",par), getValue("logoY",par) );
curHeight += 25+getValue("adresseY",par);


 helv = new Font("Helvetica", Font.PLAIN, 10);
//have to set the font to get any output
pg.setFont (helv);
FontMetrics fm = pg.getFontMetrics(helv);
int fontHeight = fm.getHeight();
int fontDescent = fm.getDescent();
   pg.drawString("Plot N° 1 Avenue de la revolution", margin, curHeight );
    curHeight += fontHeight;
    pg.drawString("BP 4781 KIGALI-RWANDA", margin, curHeight );
    curHeight += fontHeight;
    pg.drawString("Tel: 252 570499 ", margin, curHeight );
    curHeight += fontHeight;

    pg.drawString("Mob: 0788302750", margin, curHeight );
    curHeight += fontHeight;
    pg.drawString("N° TVA : 100889636", margin, curHeight);
    curHeight += fontHeight;


    pg.drawRect(getValue("clientX",par), curHeight + getValue("clientY",par),350 , 50);
    Font helv2 = new Font("Helvetica", Font.BOLD, 10);
    pg.setFont (helv2);
    pg.drawString(ass.sigle, getValue("clientX",par)+10, curHeight + getValue("clientY",par)+10);
    helv2 = new Font("Helvetica", Font.ITALIC, 10);
    pg.setFont (helv2);
    pg.drawString(ass.designation, getValue("clientX",par)+10, curHeight + getValue("clientY",par)+20);
    pg.drawString(ass.adresse, getValue("clientX",par)+10, curHeight + getValue("clientY",par)+30);

    curHeight += fontHeight+50;

    int bla=curHeight;

    helv2 = new Font("Helvetica", Font.BOLD, 10);
    pg.setFont (helv2);

    Font helv1 = new Font("Helvetica", Font.BOLD, 15);
    pg.setFont (helv1);
    pg.drawString("Facture : "+ink, margin, bla+25 );

    helv2 = new Font("Helvetica", Font.PLAIN, 10);
    pg.setFont (helv2);
    pg.drawString("Reference "+credit.getFirst().ID_INVOICE+"-"+credit.getLast().ID_INVOICE, margin, bla+40 );

    helv1 = new Font("Helvetica", Font.PLAIN, 10);
    pg.setFont (helv1);
    pg.drawString("Nombre de Bons # "+credit.size(), margin, bla +60  );
    
   helv1 = new Font("Helvetica", Font.BOLD, 10);
                        pg.setFont (helv1);
                        pageNum++;
                        pg.drawString("Page : "+pageNum, pageW/2, pageH-9  );
   curHeight += fontHeight+90;
    String [] entete = {"  BL","  Date","     N° AFF.","     Quittance","     Nom","     Prénom","TOTAL HT","TVA","TOTAL",""};
    int g=entete.length;
    int[] longeur ={0,(int)(0.6*(pageW/g)),(int)(0.7*(pageW/g)),(int)(1.4*(pageW/g)),(int)(1.6*(pageW/g)),(int)(1.6*(pageW/g)),(int)(1.5*(pageW/g)),(int)(0.88*(pageW/g)),(int)(0.65*(pageW/g)),(int)(0.88*(pageW/g))};
    curHeight += 30;



    int w=margin;
    int curHeightLigne=curHeight;

    Font helvLot = new Font("Helvetica", Font.PLAIN, 10);
    pg.setFont (helvLot);
    FontMetrics fmLot = pg.getFontMetrics(helvLot);
    curHeight += 30;
    fontHeight= fmLot.getHeight();
    boolean done=true;

    for(int j=0;j<credit.size();j++)
    {
       helvLot = new Font("Helvetica", Font.PLAIN, 8);
    pg.setFont (helvLot);
    Credit c =credit.get(j);
    w=margin;
    if(done)
    curHeight += fontHeight;
    else
    curHeight += 2*fontHeight;

    done=true;
String now="";
int lon=0;
        for(int i=0;i<9;i++)
        {
           w+=(longeur[i]);

    if(i==0)
    {
        pg.drawString(""+c.ID_INVOICE, w+5, curHeight );
    }
    if(i==1)
    {
        pg.drawString(c.NUM_CLIENT, w+5, curHeight );
    }
    if(i==2)
   {
      now=c.NUMERO_AFFILIE;
      lon=13;
    if(now.length()<lon)
       pg.drawString(now, w+5, curHeight );
    else
    { done=false;
        pg.drawString(now.substring(0, lon-1), w+5, curHeight );
        pg.drawString(now.substring(lon-1, now.length()), w+5, fontHeight+curHeight);

     }


    }
    if(i==3)
    {


         now=c.NUMERO_QUITANCE;
         lon=16;
    if(now.length()<lon)
       pg.drawString(now, w+5, curHeight );
    else
    { done=false;
        pg.drawString(now.substring(0, lon-1), w+5, curHeight );
        pg.drawString(now.substring(lon-1, now.length()), w+5, fontHeight+curHeight);

     }

    }
    if(i==4)
    {
         now=c.NOM;
       lon=18;
    if(now.length()<lon)
       pg.drawString(now, w+5, curHeight );
    else
    { done=false;
      pg.drawString(now.substring(0, lon-1), w+5, curHeight );
      pg.drawString(now.substring(lon-1, now.length()), w+5,fontHeight +curHeight);

     }
    }
    if(i==5)
    {
       now=c.PRENOM;
       lon=14;
    if(now.length()<lon)
       pg.drawString(now, w+5, curHeight );
    else
    { done=false;
        pg.drawString(now.substring(0, lon-1), w+5, curHeight );
        pg.drawString(now.substring(lon-1, now.length()), w+5, fontHeight+curHeight);

     }
    }
          
    if(i==6)
    {
        printInt(pg,c.PAYED-c.tva,w+(longeur[i+1])-10,curHeight);
        solde+=(c.PAYED-c.tva);
    }
    if(i==7)
    {
        printInt(pg,c.tva,w+(longeur[i+1])-10,curHeight);tvaPrice+=c.tva;
    }
    if(i==8)
    {
        printInt(pg,c.PAYED,w+(longeur[i+1])-10,curHeight);
        total+=c.PAYED;
    }

        }
    if((curHeight)>pageH-getValue("longeur",par))
                        {

int z=margin;
int b =pageH-30;
helv1 = new Font("Helvetica", Font.BOLD, 10);
pg.setFont (helv1);

w=margin;
                for(int i=0;i<10;i++)
                {
                w+=(longeur[i]);
                pg.drawLine(w, curHeightLigne-20, w, b);
                pg.drawString(entete[i], w+4, curHeightLigne-5 );
                }
                pg.drawLine(margin, b, w, b);
                pg.drawRect(margin, curHeightLigne-20 ,w-margin , 25);

                            pg.dispose();
                            pg = pjob.getGraphics();
                        if (pg != null) {
                        pg.setFont(helv);
                        }
                            curHeight = margin;
                            helv = new Font("Helvetica", Font.BOLD, 25);
                            helv1 = new Font("Helvetica", Font.PLAIN, 15);
                            pg.setFont (helv1);
                            pg.drawString(ass.designation+"              Facture : "+ink, margin, margin+25 );

                            helv1 = new Font("Helvetica", Font.BOLD, 10);
                            pg.setFont (helv1);
                            pageNum++;
                            pg.drawString("Page : "+pageNum, pageW/2, pageH-9  );
                            curHeight += fontHeight+50;

                            curHeightLigne =curHeight;
                            curHeight += 10;
                            done=true;  
                        }

    }

   helv2 = new Font("Helvetica", Font.BOLD, 10);
                pg.setFont (helv2);
                w=margin;
                for(int i=0;i<10;i++)
                {
                w+=(longeur[i]);
                pg.drawLine(w, curHeightLigne-20, w, down);
                pg.drawString(entete[i], w+4, curHeightLigne-5 );
                }
                pg.drawLine(margin, down, w, down);
                pg.drawRect(margin, curHeightLigne-20 ,w-margin , 25);


                pg.drawRect(margin+280, down+5, 270       , 40     );

                pg.drawLine(margin+370, down+5, margin+370, down+45);
                pg.drawLine(margin+460, down+5, margin+460, down+45);


                pg.drawString(" TOTAL HT RWF" , margin+285, down+15);
                pg.drawString(" TOTAL TVA RWF", margin+375, down+15);
                pg.drawString(" TOTAL TTC RWF", margin+465, down+15);

                pg.drawLine(margin+280, down+25, margin+550, down+25);

                helv2 = new Font("Helvetica", Font.PLAIN, 10);
                pg.setFont (helv2);

                printInt(pg,(int)((total-tvaPrice)),margin+365, down+35);
                printInt(pg,(int)(tvaPrice)    ,margin+425, down+35);
                printInt(pg,(int)(total)           ,margin+545, down+35);


                helv2 = new Font("Helvetica", Font.BOLD, 10);
                pg.setFont (helv2);
                int compte=pageH-20;
                pg.drawRect(margin+20, down+50,500 , 40);
                helv2 = new Font("Helvetica", Font.BOLD, 7);
                pg.setFont (helv2);
               pg.drawString("Conditions de règlement: Nos Factures sont payables endéans les 30 jours à partir de la date de réception.", margin+100,down+60 );
                pg.drawString("      Passé ce délai la direction se rèserve le droit de calculer des intèrêts de 1,25 % par mois de retard ", margin+100,down+70 );
                 pg.drawString("      Le paiement se fait à nos caisses contre remise d'un reçu ou via le compte bancaire ci-dessous ", margin+100,down+80 );

                helv2 = new Font("Helvetica", Font.BOLD, 10);
                pg.setFont (helv2);
                pg.drawString("", margin, compte);
                pg.drawString("", margin+100, compte);

    //cashier.basketStart("KIPHARMA");


  }

}

 private void printLongStringCash (PrintJob pjob, Graphics pg) {

   
    // Note: String is immutable so won't change while printing.

    if (!(pg instanceof PrintGraphics)) {
      throw new IllegalArgumentException ("Graphics context not PrintGraphics");
    }

    int pageH = pjob.getPageDimension().height - margin;
    int pageW = pjob.getPageDimension().width - margin;

int curHeight = margin;

if (pg != null)
  {

            BufferedInputStream bis = null;
            try {
                int down = pageH - 120;
                curHeight = margin;
                Font helv = new Font("Helvetica", Font.BOLD, getValue("cdate", par));
                pg.setFont(helv);
                 Image img=null;
                bis = new BufferedInputStream(new FileInputStream(getString("logo")));
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                try {
                    int ch;
                    while ((ch = bis.read()) != -1) {
                        baos.write(ch);
                        System.err.println(ch);
                    }
                     System.err.println(img);
                     img = Toolkit.getDefaultToolkit().createImage(baos.toByteArray());
                     System.err.println(img.getWidth(rootPane));
                } catch (IOException exception) {
                    System.err.println("Error loading: ");
                }
               String date = ass.date;
                pg.drawString("Kigali, le " + date.substring(0, 2) + "/" + date.substring(2, 4) + "/" + date.substring(4, 6), pageW - getValue("dateX", par), getValue("dateY", par));
                helv = new Font("Helvetica", Font.BOLD, 25);
                pg.setFont(helv);

                 Thread t = new Thread();
    t.start();
                try {
                    t.sleep(5000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(PrintDoc.class.getName()).log(Level.SEVERE, null, ex);
                }
                System.out.println( pg.drawImage(img, getValue("logoX", par), getValue("logoY", par), 200, 50, rootPane));

                curHeight += 25 + getValue("adresseY", par);
                helv = new Font("Helvetica", Font.PLAIN, getValue("cadresse", par));
//have to set the font to get any output
                pg.setFont(helv);
                FontMetrics fm = pg.getFontMetrics(helv);
                int fontHeight = fm.getHeight();
                int fontDescent = fm.getDescent();
                  pg.drawString("Plot N° 1 Avenue de la revolution BP 4781 KIGALI-RWANDA", margin, curHeight );
    curHeight += fontHeight;
    pg.drawString("Tel: 252 570499 ", margin, curHeight );
    curHeight += fontHeight;

    pg.drawString("Mob: 0788302750", margin, curHeight );
    curHeight += fontHeight;
    pg.drawString("N° TVA : 100889636", margin, curHeight);
    curHeight += fontHeight;

                pg.drawRect(getValue("clientX", par), getValue("clientY", par), 250, 50);
                Font helv2 = new Font("Helvetica", Font.BOLD, getValue("cclient", par));
                pg.setFont(helv2);
                pg.drawString(ass.sigle, getValue("clientX", par) + 10,  getValue("clientY", par) + 10);
                helv2 = new Font("Helvetica", Font.ITALIC, getValue("cclient", par));
                pg.setFont(helv2);
                pg.drawString(ass.designation, getValue("clientX", par) + 10, getValue("clientY", par) + 25);
                pg.drawString(ass.adresse, getValue("clientX", par) + 10,  getValue("clientY", par) + 40);
                curHeight += fontHeight + 30;
                int bla = curHeight;
                Font helv1 = new Font("Helvetica", Font.BOLD, getValue("cfacture", par));
                pg.setFont(helv1);
                pg.drawString("Facture : " + ink, margin, bla + 25);
                helv2 = new Font("Helvetica", Font.PLAIN, getValue("cfacture", par));
                pg.setFont(helv2);
                pg.drawString("Reference " + credit.getFirst().ID_INVOICE + "-" + credit.getLast().ID_INVOICE, margin, bla + 40);
                helv1 = new Font("Helvetica", Font.PLAIN, 10);
                pg.setFont(helv1);
                pg.drawString("Nombre de Bons # " + credit.size(), margin, bla + 60);
                helv1 = new Font("Helvetica", Font.BOLD, 10);
                pg.setFont(helv1);
                pageNum++;
                pg.drawString("Page : " + pageNum, pageW / 2, pageH - 9);
                curHeight += fontHeight + 90;
                bla = curHeight;
                helv2 = new Font("Helvetica", Font.BOLD, 10);
                pg.setFont(helv2);
    String [] entete = {"BL","Date","Employe","Heure","Payé HT","Tva","Total",""};
    int g=entete.length;
    int[] longeur ={0,(int)(0.6*(pageW/g)),(int)(0.7*(pageW/g)),(int)(1.4*(pageW/g)),(int)(1.5*(pageW/g)),(int)(0.88*(pageW/g)),(int)(0.65*(pageW/g)),(int)(0.88*(pageW/g))};

                int w = margin;
                int curHeightLigne = curHeight;
                Font helvLot = new Font("Helvetica", Font.PLAIN, 10);
                pg.setFont(helvLot);
                FontMetrics fmLot = pg.getFontMetrics(helvLot);
                curHeight += getValue("ligne", par);
                fontHeight = fmLot.getHeight();
                boolean done = true;


                        curHeight += 30;

    for(int j=0;j<credit.size();j++)
    {
         helvLot = new Font("Helvetica", Font.PLAIN, getValue("cgenerale", par));
                    pg.setFont(helvLot);

    Credit c =credit.get(j);
    w=margin;
    if(done)
    curHeight += fontHeight;
    else
    curHeight += 2*fontHeight;

    done=true;
String now="";
int lon=0;
        for(int i=0;i<7;i++)
        {
           w+=(longeur[i]);

    if(i==0)
    {
        pg.drawString(""+c.ID_INVOICE, w+5, curHeight );
    }
    if(i==1)
    {
        pg.drawString(c.NUM_CLIENT, w+5, curHeight );
    }
    if(i==2)
   {
      now=c.NUMERO_AFFILIE;
      lon=16;
    if(now.length()<lon)
       pg.drawString(now, w+5, curHeight );
    else
    { done=false;
        pg.drawString(now.substring(0, lon-1), w+5, curHeight );
        pg.drawString(now.substring(lon-1, now.length()), w+5, fontHeight+curHeight);

     }


    }
    if(i==3)
    {
         now=c.NUMERO_QUITANCE;
         lon=16;
    if(now.length()<lon)
       pg.drawString(now, w+5, curHeight );
    else
    { done=false;
        pg.drawString(now.substring(0, lon-1), w+5, curHeight );
        pg.drawString(now.substring(lon-1, now.length()), w+5, fontHeight+curHeight);

     }

    }
   if(i==4)
    {
        printInt(pg,c.PAYED-c.tva,w+(longeur[i+1])-10,curHeight);
        solde+=(c.PAYED-c.tva);
    }
    if(i==5)
    {
        printInt(pg,c.tva,w+(longeur[i+1])-10,curHeight);tvaPrice+=c.tva;
    }
    if(i==6)
    {
        printInt(pg,c.PAYED,w+(longeur[i+1])-10,curHeight);
        total+=c.PAYED;
    }

        }
    if ((curHeight) > pageH - getValue("longeur", par)) {
                        int z = margin;
                        int b = pageH - 30;
                
helv1 = new Font("Helvetica", Font.BOLD, 10);
pg.setFont (helv1);

w=margin;
                for(int i=0;i<8;i++)
                {
                w+=(longeur[i]);
                pg.drawLine(w, curHeightLigne-20, w, b);
                pg.drawString(entete[i], w+4, curHeightLigne-5 );
                }
                pg.drawLine(margin, b, w, b);
                pg.drawRect(margin, curHeightLigne-20 ,w-margin , 25);

                            pg.dispose();
                            pg = pjob.getGraphics();
                        if (pg != null) {
                        pg.setFont(helv);
                        }
                            curHeight = margin;
                            helv = new Font("Helvetica", Font.BOLD, 25);
                            helv1 = new Font("Helvetica", Font.PLAIN, 15);
                            pg.setFont (helv1);
                            pg.drawString(ass.designation+"              Facture : "+ink, margin, margin+25 );

                            helv1 = new Font("Helvetica", Font.BOLD, 10);
                            pg.setFont (helv1);
                            pageNum++;
                            pg.drawString("Page : "+pageNum, pageW/2, pageH-9  );
                            curHeight += fontHeight+50;

                            curHeightLigne =curHeight;
                            curHeight += 10;
done=true;


                        }

    }

   helv2 = new Font("Helvetica", Font.BOLD, 10);
                pg.setFont (helv2);
                w=margin;
                for(int i=0;i<8;i++)
                {
                w+=(longeur[i]);
                pg.drawLine(w, curHeightLigne-20, w, down);
                pg.drawString(entete[i], w+4, curHeightLigne-5 );
                }
                pg.drawLine(margin, down, w, down);
                pg.drawRect(margin, curHeightLigne-20 ,w-margin , 25);


                pg.drawRect(margin+280, down+5, 270       , 40     );

                pg.drawLine(margin+370, down+5, margin+370, down+45);
                pg.drawLine(margin+460, down+5, margin+460, down+45);


                pg.drawString(" TOTAL HT RWF" , margin+285, down+15);
                pg.drawString(" TOTAL TVA RWF", margin+375, down+15);
                pg.drawString(" TOTAL TTC RWF", margin+465, down+15);

                pg.drawLine(margin+280, down+25, margin+550, down+25);

                helv2 = new Font("Helvetica", Font.PLAIN, 10);
                pg.setFont (helv2);

                printInt(pg,(int)((total-tvaPrice)),margin+365, down+35);
                printInt(pg,(int)(tvaPrice)    ,margin+425, down+35);
                printInt(pg,(int)(total)           ,margin+545, down+35);


                helv2 = new Font("Helvetica", Font.BOLD, 10);
                pg.setFont (helv2);
                int compte=pageH-20;
                pg.drawRect(margin+20, down+50,500 , 40);
                helv2 = new Font("Helvetica", Font.BOLD, 7);
                pg.setFont (helv2);
            //  pg.drawString("Conditions de règlement: Nos Factures sont payables endéans les 30 jours à partir de la date de réception.", margin+100,down+60 );
         pg.drawString("                                         printed on "+new Date().toLocaleString(), margin+100,down+70 );
           //      pg.drawString("      Le paiement se fait à nos caisses contre remise d'un reçu ou via le compte bancaire ci-dessous ", margin+100,down+80 );
  helv2 = new Font("Helvetica", Font.BOLD, 10);
                pg.setFont (helv2);
                pg.drawString("BK 0040-0285955-32/RWF", margin, compte);
                pg.drawString("BCR 00010-0007883-03-65/RWF", margin+430, compte);
}  catch (FileNotFoundException ex) {
                Logger.getLogger(PrintDoc.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                try {
                    bis.close();
                } catch (IOException ex) {
                    Logger.getLogger(PrintDoc.class.getName()).log(Level.SEVERE, null, ex);
                }
  }
}}




private void printLongStringCredit (PrintJob pjob, Graphics pg) {

 

   if (!(pg instanceof PrintGraphics)) {
      throw new IllegalArgumentException ("Graphics context not PrintGraphics");
    }

    int pageH = pjob.getPageDimension().height - margin;
    int pageW = pjob.getPageDimension().width - margin;

int curHeight = margin;

if (pg != null)
  {
            BufferedInputStream bis = null;
            try {
                int down = pageH - 120;
                curHeight = margin;
                Font helv = new Font("Helvetica", Font.BOLD, getValue("cdate", par));
                pg.setFont(helv);
                 Image img=null;
                bis = new BufferedInputStream(new FileInputStream(getString("logo")));
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                try {
                    int ch;
                    while ((ch = bis.read()) != -1) {
                        baos.write(ch);
                        System.err.println(ch);
                    }
                     System.err.println(img);
                     img = Toolkit.getDefaultToolkit().createImage(baos.toByteArray());
                     System.err.println(img.getWidth(rootPane));
                } catch (IOException exception) {
                    System.err.println("Error loading: ");
                }
               String date = ass.date;
               if(date.length()==6)
               {
                   String k=date.substring(2,4); 
                   System.out.println("association:"+k);
                   if(k.equals("02"))
                   {
                       int m=Integer.parseInt(date.substring(4,6));
                       if(m%4==0)
                           date=29+k+m;                       
                   }
                   
               }
                pg.drawString("Kigali, le " + date.substring(0, 2) + "/" + date.substring(2, 4) + "/" + date.substring(4, 6), pageW - getValue("dateX", par), getValue("dateY", par));
                helv = new Font("Helvetica", Font.BOLD, 25);
                pg.setFont(helv);

                 Thread t = new Thread();
                 // JOptionPane.showMessageDialog(null," start"," OK ",JOptionPane.PLAIN_MESSAGE);

    t.start();
    t.sleep(10000);

                System.out.println( pg.drawImage(img, getValue("logoX", par), getValue("logoY", par), 200, 50, rootPane));
               
                curHeight += 25 + getValue("adresseY", par);
                helv = new Font("Helvetica", Font.PLAIN, getValue("cadresse", par));
//have to set the font to get any output
                pg.setFont(helv);
                FontMetrics fm = pg.getFontMetrics(helv);
                int fontHeight = fm.getHeight();
                int fontDescent = fm.getDescent();

               // JOptionPane.showMessageDialog(null," PRINT"," OK ",JOptionPane.PLAIN_MESSAGE);
 String in  = (String)JOptionPane.showInputDialog(null, "Fait votre choix", db.l(db.lang,"V_REMISE"), JOptionPane.QUESTION_MESSAGE, null, new String [] {"0","5","10","15","20","25","30"}, "");

                    Integer in_int=new Integer(in);
                    double percentage =(double)( 100-in_int.intValue())/100;
          pg.drawString("Plot N° 1 Avenue de la revolution ", margin, curHeight );
    curHeight += fontHeight;
    pg.drawString("BP 4781 KIGALI-RWANDA", margin, curHeight );
    curHeight += fontHeight;
    pg.drawString("Tel: 252 570499 ", margin, curHeight );
    curHeight += fontHeight;

    pg.drawString("Mob: 0788302750", margin, curHeight );
    curHeight += fontHeight;
    pg.drawString("N° TVA : 100889636", margin, curHeight);
    curHeight += fontHeight;
    pg.drawRect(getValue("clientX", par), getValue("clientY", par), 250, 50);
                Font helv2 = new Font("Helvetica", Font.BOLD, getValue("cclient", par));
                pg.setFont(helv2);
                pg.drawString(ass.sigle, getValue("clientX", par) + 10,  getValue("clientY", par) + 10);
                helv2 = new Font("Helvetica", Font.ITALIC, getValue("cclient", par));
                pg.setFont(helv2);
                pg.drawString(ass.designation, getValue("clientX", par) + 10, getValue("clientY", par) + 25);
                pg.drawString(ass.adresse, getValue("clientX", par) + 10,  getValue("clientY", par) + 40);
                curHeight += fontHeight + 30;
                int bla = curHeight;
                Font helv1 = new Font("Helvetica", Font.BOLD, getValue("cfacture", par));
                pg.setFont(helv1);



                pg.drawString("Facture : " + ink+"/PHVIE/2012", margin, bla + 25);
                helv2 = new Font("Helvetica", Font.PLAIN, getValue("cfacture", par));
                pg.setFont(helv2);
                pg.drawString("Reference " + credit.getFirst().ID_INVOICE + "-" + credit.getLast().ID_INVOICE, margin, bla + 40);
                helv1 = new Font("Helvetica", Font.PLAIN, 10);
                pg.setFont(helv1);
                pg.drawString("Nombre de Bons # " + credit.size(), margin, bla + 60);
                helv1 = new Font("Helvetica", Font.BOLD, 10);
                pg.setFont(helv1);
                pageNum++;
                pg.drawString("Page : " + pageNum, pageW / 2, pageH - 9);
                curHeight += fontHeight + 90;
                bla = curHeight;
                helv2 = new Font("Helvetica", Font.BOLD, 10);
                pg.setFont(helv2);
                String[] entete = {" BL", " DATE", "    N° AFF.", "QUITTANCE", "        NOM", "       PRENOM", "TOTAL HT", " TVA", "  TOTAL", ""};
                int g = entete.length;
                int[] longeur = {0, (int) (0.6*(pageW/g)), (int) (0.7*(pageW/g)), (int) (((double)getValue("affilieL", par)/10)*(pageW/g)), (int) (((double)getValue("quittanceL", par)/10)*(pageW/g)),(int) (1.53*(pageW/g)), (int) (1.5*(pageW/g)), (int) (0.95*(pageW/g)), (int) (0.65*(pageW/g)), (int) (0.88*(pageW/g))};
                int w = margin;
                int curHeightLigne = curHeight;
                Font helvLot = new Font("Helvetica", Font.PLAIN, 10);
                pg.setFont(helvLot);
                FontMetrics fmLot = pg.getFontMetrics(helvLot);
                curHeight += getValue("ligne", par);
                fontHeight = fmLot.getHeight();
                boolean done = true;

               

                    for (int j = 0; j < credit.size(); j++) {
                    helvLot = new Font("Helvetica", Font.PLAIN, getValue("cgenerale", par));
                    pg.setFont(helvLot);
                    Credit c = credit.get(j);
                    w = margin;
                    if (done) {
                    curHeight += fontHeight;
                    } 
                    else
                    {
                        curHeight += 2 * fontHeight;
                    }
                    done = true;
                    String now = "";
                    int lon = 0;
                    for (int i = 0; i < 9; i++) {
                         w += (longeur[i]);
                       
                        if (i == 0) {
                            pg.drawString("" + c.ID_INVOICE, w + 5, curHeight);
                        }
                        if (i == 1) {
                            now = c.NUM_CLIENT;
                             if (now.length() <13) {
                                pg.drawString(now, w + 5, curHeight);
                            } else {
                                done = false;
                                pg.drawString(now.substring(0, lon - 1), w + 5, curHeight);
                                pg.drawString(now.substring(lon - 1, now.length()), w + 5, fontHeight + curHeight);
                            }
                        }
                        if (i == 2) {
                            now = c.NUMERO_AFFILIE;
                            lon =  getValue("affilie", par);
                            System.out.println("now length:"+now.length()+" aff:"+lon);
                            if (now.length() <lon) {
                                pg.drawString(now, w + 5, curHeight);
                            } else {
                                done = false;
                                pg.drawString(now.substring(0, lon - 1), w + 5, curHeight);
                                pg.drawString(now.substring(lon - 1, now.length()), w + 5, fontHeight + curHeight);
                            }
                        }
                        if (i == 3) {
                            now = c.NUMERO_QUITANCE;
                            lon = getValue("quittance", par);
                            if (now.length() <lon ) {
                                pg.drawString(now, w + 5, curHeight);
                            } else {
                                done = false;
                                pg.drawString(now.substring(0, lon - 1), w + 5, curHeight);
                                pg.drawString(now.substring(lon - 1, now.length()), w + 5, fontHeight + curHeight);
                            }
                        }
                        if (i == 4) {
                            now = c.NOM;
                            lon = getValue("nom", par) ;
                            if (now.length() < lon) {
                                pg.drawString(now, w + 5, curHeight);
                            } else {
                                done = false;
                                pg.drawString(now.substring(0, lon - 1), w + 5, curHeight);
                                pg.drawString(now.substring(lon - 1, now.length()), w + 5, fontHeight + curHeight);
                            }
                        }
                        if (i == 5)
                        {
                            now = c.PRENOM;
                            lon = getValue("prenom", par);
                            if (now.length() < lon) {
                                pg.drawString(now, w + 5, curHeight);
                            } 
                            else
                            {
                                done = false;
                                pg.drawString(now.substring(0, lon - 1), w + 5, curHeight);
                                pg.drawString(now.substring(lon - 1, now.length()), w + 5, fontHeight + curHeight);
                            }
                        }
                        if (i == 6) 
                        {
                            printInt(pg, (int)((c.SOLDE - c.tva)*percentage), w + (longeur[i + 1]) - 10, curHeight);
                            solde +=  (int)((c.SOLDE - c.tva)*percentage);
                        }
                        if (i == 7) {
                            printInt(pg,  (int)(( c.tva)*percentage), w + (longeur[i + 1]) - 10, curHeight);
                            tvaPrice += (int)(( c.tva)*percentage);
                        }
                        if (i == 8) {
                            printInt(pg, (int)((c.SOLDE)*percentage) , w + (longeur[i + 1]) - 10, curHeight);
                            total += (int)((c.SOLDE)*percentage);
                        }
                    }
                    if ((curHeight) > pageH - getValue("longeur", par)) {
                        int z = margin;
                        int b = pageH - 30;
                        helv1 = new Font("Helvetica", Font.BOLD, getValue("ctitle", par));
                        pg.setFont(helv1);
                        w = margin;
                        for (int i = 0; i < 10; i++) {
                            w += (longeur[i]);
                            pg.drawLine(w, curHeightLigne - 20, w, b);
                            pg.drawString(entete[i], w + 2, curHeightLigne - 5);
                        }
                        pg.drawLine(margin, b, w, b);
                        pg.drawRect(margin, curHeightLigne - 20, w - margin, 25);
                        pg.dispose();
                        pg = pjob.getGraphics();
                        if (pg != null) {
                            pg.setFont(helv);
                        }
                        curHeight = margin;
                        helv = new Font("Helvetica", Font.BOLD, getValue("cclient", par));
                        helv1 = new Font("Helvetica", Font.PLAIN, getValue("cfacture", par));
                        pg.setFont(helv1);
                        pg.drawString(ass.designation + "              Facture : " +ink, margin, margin + 25);
                        helv1 = new Font("Helvetica", Font.BOLD, 10);
                        pg.setFont(helv1);
                        pageNum++;
                        pg.drawString("Page : " + pageNum, pageW / 2, pageH - 9);
                        curHeight += fontHeight + 50;
                        curHeightLigne = curHeight;
                        curHeight += 10;
                        done = true;
                    }
                }
                helv2 = new Font("Helvetica", Font.BOLD, getValue("ctotal", par));
                pg.setFont(helv2);
                w = margin;
                for (int i = 0; i < 10; i++) {
                    w += (longeur[i]);
                    pg.drawLine(w, curHeightLigne - 20, w, down);
                    pg.drawString(entete[i], w + 4, curHeightLigne - 5);
                }
                pg.drawLine(margin, down, w, down);
                pg.drawRect(margin, curHeightLigne - 20, w- margin, 25);

                pg.drawRect(margin + 280, down + 5, 292, 40);

                pg.drawLine(margin + 370, down + 5, margin + 370, down + 45);
                pg.drawLine(margin + 460, down + 5, margin + 460, down + 45);

                pg.drawString(" TOTAL HT RWF", margin + 285, down + 15);
                pg.drawString(" TOTAL TVA RWF", margin + 375, down + 15);
                
                pg.drawString(" TOTAL TTC RWF", margin + 480, down + 15);
                
                pg.drawLine(margin + 280, down + 25, w, down + 25);
                helv2 = new Font("Helvetica", Font.PLAIN, getValue("cglobal", par));
                pg.setFont(helv2);
                printInt(pg, (int) ((total-tvaPrice)), margin + 365, down + 35);
                printInt(pg, (int) (tvaPrice), margin + 425, down + 35);
                printInt(pg, (int) (total), pageW - margin - margin , down + 35);
                helv2 = new Font("Helvetica", Font.BOLD, 10);
                pg.setFont(helv2);
                int compte = pageH - 20;
                pg.drawRect(margin, down + 50, pageW - margin - margin + 2, 40);
        //        helv2 = new Font("Helvetica", Font.BOLD, 7);
      //          pg.setFont(helv2);
    //            pg.drawString("Conditions de règlement: Nos Factures sont payables endéans les 30 jours à partir de la date de réception.", margin + 100, down + 60);
  //              pg.drawString("      Passé ce délai la direction se rèserve le droit de calculer des intèrêts de 1,25 % par mois de retard ", margin + 100, down + 70);
//                pg.drawString("      Le paiement se fait à nos caisses contre remise d'un reçu ou via le compte bancaire ci-dessous ", margin + 100, down + 80);
                helv2 = new Font("Helvetica", Font.BOLD, 10);
                pg.setFont(helv2);
                pg.drawString("", margin, compte);
                pg.drawString("", margin + 390, compte);

                //cashier.basketStart("KIPHARMA");
            } catch (InterruptedException ex) {
                JOptionPane.showMessageDialog(null," PRINT",""+ex,JOptionPane.PLAIN_MESSAGE);

            } catch (FileNotFoundException ex) {
              JOptionPane.showMessageDialog(null," PRINT",""+ex,JOptionPane.PLAIN_MESSAGE);
            } finally {
                try {
                    bis.close();
                } catch (IOException ex) {
                   JOptionPane.showMessageDialog(null," PRINT",""+ex,JOptionPane.PLAIN_MESSAGE);
                }
            }

    //cashier.basketStart("KIPHARMA");

    
  }

}

 public static String setVirgule(double frw)
    {      
            BigDecimal big=new BigDecimal(frw);
            String getBigString=big.toString();
     //       System.out.println(getBigString);
  getBigString=getBigString.replace('.', ';');
 // System.out.println(getBigString);
         String[]sp=getBigString.split(";");  
          
          String decimal="";
          if(sp.length>1)
          {
              decimal=sp[1];
          }
            
            if(decimal.equals("") || ( decimal.length()==1 && decimal.equals("0")) )
            {
                decimal=".00";
            }
            else if (decimal.length()==1 && !decimal.equals("0") )
            {
                decimal="."+decimal+"0";
            }
            else
            {
                decimal="."+decimal.substring(0, 2);
            }
             
            
            String toret=setVirgule(sp[0])+decimal;            
            
            return toret; 
}
public static String setVirgule(String frw)
   {
       String setString="";
       int k=0;
       for(int i=(frw.length()-1);i>=0;i--)
       {
           k++;
           if((k-1)%3==0 && (k-1)!=0)
           {
               setString+=",";
           }
           setString+=frw.charAt(i);
       }
       return inverse(setString);
   }
   
   static String inverse(String frw)
   {
       String l="";
       for(int i=(frw.length()-1);i>=0;i--)
       {
           l+=frw.charAt(i);
       }
       return l;
   }


  public void printInt(Graphics pg,double nbre,int w,int h)
  {
  String s=setVirgule(nbre);

  int back=0;

  for(int i=s.length()-1;i>=0;i--)
  {
      if(s.charAt(i)==' ')
      back+=2;
      else
       back+=5;

      pg.drawString(""+s.charAt(i),w-back,h);

  }






  }
  String setVirgule(int frw)
        {

String setString = ""+frw;
int l =setString.length();



if(l>3 && l<=6)
setString= setString.substring(0,l-3)+" "+setString.substring(l-3) ;
if(l>6)
{

String s1 = setString.substring(0,l-3);
int sl=s1.length();
setString=s1.substring(0,sl-3)+" "+s1.substring(sl-3)+" "+setString.substring(l-3)  ;

}

return setString;
}
  
  
  String getString(String s)
{
String ret=" ";
 Variable var= db.getParam("PRINT","",s);
 if(var!=null)
     ret=var.value;
     return ret;
}
public static void main(String[] arghs)
{
    //new PrintDoc();
}



}
