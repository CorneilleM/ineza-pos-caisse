/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pos;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import javax.swing.*;

/**
 *
 * @author ADMIN
 */
public class VariableInterface extends JFrame
{
    private  JTextField
        NOM_VARIABLE ,
	VALUE_VARIABLE  ,
	FAMILLE_VARIABLE  ,
	VALUE2_VARIABLE;
    
    private JLabel
        NOM_VARIABLEL,
	VALUE_VARIABLEL  ,
	FAMILLE_VARIABLEL  ,
	VALUE2_VARIABLEL;
    
    private JButton send;
    private Db db;
    String  what;
    private Variable c;
   JPanel mainInsurance = new JPanel();
    
    
VariableInterface( Db db )
{
this.db=db;
what="";
this.c=new Variable( "","","","");
makeFrame();
send();
draw(); 
}

VariableInterface(Db db,Variable c)
{
    what="UPDATE";
    this.db=db;
    this.c=c;
    makeFrame();
    send();
    draw();


}
    public void makeFrame()
{

        this.NOM_VARIABLE = new JTextField();
	this.VALUE_VARIABLE  = new JTextField();
	this.FAMILLE_VARIABLE  = new JTextField();
	this.VALUE2_VARIABLE = new JTextField(); 

        NOM_VARIABLE.setText(""+c.nom);
	VALUE_VARIABLE.setText(c.value);
	FAMILLE_VARIABLE.setText(c.famille);
	VALUE2_VARIABLE.setText(c.value2); 

        this.NOM_VARIABLEL=new JLabel("NOM VARIABLE ");
	this.VALUE_VARIABLEL=new JLabel("VALUE VARIABLE ");
	this.FAMILLE_VARIABLEL=new JLabel("FAMILLE VARIABLE ");
	this.VALUE2_VARIABLEL=new JLabel("VALUE2 VARIABLE "); 
         this.send=new JButton("ENVOYER");

}
    
    public void draw()
{
    setTitle("Variable for Ishyiga"); 
    this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE); 
    
    //on fixe  les composant del'interface
    Container content = getContentPane();


    if(what.equals("UPDATE"))
    {
     send.setText("UPDATE");
     send.setBackground(Color.GREEN);
    }

     westPane(content);

     setSize(500,450);
     setVisible(true);

}
    
    public JPanel westPane(Container content )
{
JPanel Pane  = new JPanel(); 
JPanel panel = new JPanel();
panel.setLayout(new GridLayout(4,2,5,20));//
panel.setPreferredSize(new Dimension(450,300)); //400=largeur350=longeur
 

panel.add(NOM_VARIABLEL);panel.add(NOM_VARIABLE);
panel.add(VALUE_VARIABLEL);panel.add(VALUE_VARIABLE);
panel.add(FAMILLE_VARIABLEL);panel.add(FAMILLE_VARIABLE);
panel.add(VALUE2_VARIABLEL);panel.add(VALUE2_VARIABLE); 
Pane.add(panel,BorderLayout.CENTER);


JPanel face = new JPanel();
face.setLayout(new GridLayout(1,4));
face.setPreferredSize(new Dimension(150,70)); 
face.add(send);
mainInsurance.add(panel);
mainInsurance.add(face);
content.add(mainInsurance);
return mainInsurance;
 
}
    
    /**********************************************************************************************
AJOUT Mannuel
**********************************************************************************************/
public void send()
{

	send.addActionListener(new ActionListener() {
	public void actionPerformed(ActionEvent ae)
	{
    if(ae.getSource()==send  )
    {

	String NOM_VARIABL= NOM_VARIABLE.getText();
	String valeur = VALUE_VARIABLE.getText();
	String famille = FAMILLE_VARIABLE.getText(); 
	String valeur2 = VALUE2_VARIABLE.getText(); 

Variable p=new Variable(NOM_VARIABL,valeur,famille, valeur2);
if(what.equals("UPDATE"))
db.upDateVariable(p,NOM_VARIABL);
else
db.insertVariable(p);
    }
        }});
        
} 

@Override
protected void processWindowEvent(WindowEvent evt)
{
   if(WindowEvent.WINDOW_CLOSING == evt.getID())
   {
	   this.dispose();
			//default icon, custom title
//JOptionPane.showMessageDialog(this,"RIRAFUNGIRA RIMWE N'ISHYIGA"," IKIBAZO",JOptionPane.PLAIN_MESSAGE);
        			//this.setDefaultCloseOperation(HIDE_ON_CLOSE);


	}

}
    
    public static void main(String[] args) 
    {
        new VariableInterface(null); 
        
        // TODO code application logic here
    }

}
