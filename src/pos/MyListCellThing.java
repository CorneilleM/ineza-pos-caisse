/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pos;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

/**
 *
 * @author Kimenyi
 */
public class MyListCellThing extends JButton implements ListCellRenderer {
Tarif [] indexes;
Font police;
    public MyListCellThing(Tarif [] indexes,Font police) {
        this.indexes=indexes;
        this.police=police;
        setOpaque(true);        
    } 
    public Component getListCellRendererComponent(JList list, Object value, int index,
            boolean isSelected, boolean cellHasFocus) { 
        // Assumes the stuff in the list has a pretty toString  
         setText(value.toString());
         setFont(police);         
         Tarif tt10 = indexes[index ];
         if(index==0)
             this.setPreferredSize(new Dimension(0,19));
         else
             this.setPreferredSize(new Dimension(0,20));
         setBackground(new Color(tt10.r, tt10.g, tt10.b)); 
        return this;
    }
}