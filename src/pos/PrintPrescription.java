package pos;

 
import com.barcodelib.barcode.DataMatrix;
import java.awt.*; 
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
/**
 *
 * @author Kimenyi
 */
public class PrintPrescription extends JFrame {
 
private Properties properties = new Properties(); 

  PrintPrescription(Invoice invoicePrint,Image img,String [] info)  
{   PrintJob pjob = getToolkit().getPrintJob(this, "Ishyiga ", properties);
    if (pjob != null) { 
    Graphics pg = pjob.getGraphics(); 
        if (pg != null) { 
        printCommande(invoicePrint,pjob, pg,img,info);
        pg.dispose();
        }
        pjob.end();
        }  
} 
private void printCommande (Invoice inv,PrintJob pjob, Graphics pg,Image img,String [] info) {
if (!(pg instanceof PrintGraphics)) {
throw new IllegalArgumentException ("Graphics context not PrintGraphics");
} 
int W=pjob.getPageDimension().width/2;
int H=pjob.getPageDimension().height/2;
System.out.println("west:"+W);

System.out.println("heigh:"+H);
 
int longeurPhoto=50;
int largeurPhoto=150;
int y=15; 
int ligne=6;
int pas=15;
int marginData= 15; 
int sizeObservation=40;
int sizeName=32;
int xQte=215;
int xDosage=240; 

if (pg != null)
{ 
    
    
    try 
    {
        DataMatrix barcode = new DataMatrix(); 
        barcode.setData(inv.CODE_BAR);
        int dim=50; 
        
        pg.drawImage(barcode.renderBarcode(),W-75, 35,dim,dim, rootPane);
        System.err.println("down "+inv.CODE_BAR);
      
        Thread.sleep(5000);
    } 
    catch (Exception ex) {
        Logger.getLogger(Print.class.getName()).log(Level.SEVERE, null, ex);
                
        System.err.println(ex);
    } 
    
    Font helv = new Font("Helvetica", Font.BOLD,11);
pg.setFont(helv);   
pg.drawImage(img,marginData,y, largeurPhoto,longeurPhoto,rootPane);  
System.out.println("heigh:"+H);
pg.drawRect(marginData-4,y-4, W-25,H);  
pg.drawString(" "+info[0], marginData, ligne*pas);ligne++;
pg.drawString(" "+info[1] + " "+inv.client.PRENOM_CLIENT, marginData, ligne*pas);ligne++;
pg.drawString(" "+info[2]+" ", marginData,ligne*pas);ligne++;
System.out.println("heigh:"+H);
helv = new Font("Helvetica", Font.PLAIN,10);
pg.setFont(helv);
pg.drawString("     ---------------------------------------------------------------------- ", marginData,
         ligne*pas);ligne++;
pg.drawString("Patient id: "+inv.client.NUM_AFFILIATION, marginData, ligne*pas);ligne++;
pg.drawString("Names: "+inv.client.NOM_CLIENT + " "+inv.client.PRENOM_CLIENT, marginData, ligne*pas);ligne++;
pg.drawString("Assurance: "+inv.client.ASSURANCE+" ", marginData,ligne*pas);ligne++;
pg.drawString("Date: "+inv.date+"  DOCTEUR: "+inv.id_employe, marginData  ,ligne*pas);ligne++;
System.out.println("heigh:"+H);
pg.drawString("     ---------------------------------------------------------------------- ", marginData,
         ligne*pas);ligne++;
         
helv = new Font("Helvetica", Font.BOLD,11);
pg.setFont(helv);      
pg.drawString( " DESIGNATION ", marginData, ligne*pas);
pg.drawString(("QTE "), xQte, ligne*pas);
pg.drawString((" DOSAGE "), xDosage, ligne*pas);
ligne++;
for (int i = 0; i < inv.getProductListDB().size(); i++) {
    Product p =inv.getProductListDB().get(i); 

helv = new Font("Lucida Handwriting", Font.PLAIN,9);
pg.setFont(helv);

if(p.productName.length()<sizeName)
{
pg.drawString(p.productName+" ", marginData, ligne*pas);
pg.drawString((p.qty()+"  "), xQte, ligne*pas);
pg.drawString((p.DOSAGE+"  "), xDosage, ligne*pas);
}
else
{
pg.drawString(p.productName.substring(0, sizeName) +" ", marginData,ligne*pas);
pg.drawString((p.qty()+"  "), xQte, ligne*pas);
pg.drawString(p.DOSAGE+"  ", xDosage,ligne*pas);
ligne++;
pg.drawString(p.productName.substring(sizeName+1,p.productName.length()), marginData, ligne*pas);
 }
ligne++;
}
ligne=23;
 helv = new Font("Helvetica", Font.PLAIN,11);
pg.setFont(helv);
pg.drawString("     ---------------------------------------------------------------------- "
, marginData, ligne*pas); ligne++;
 
System.out.println("heigh:"+H);
if(inv.OBSERVATION_INVOICE.length()<sizeObservation)
{
pg.drawString("OBS.: "+inv.OBSERVATION_INVOICE, marginData, ligne*pas);  
}
else
{
pg.drawString("OBS.: "+inv.OBSERVATION_INVOICE.substring(0, sizeObservation), marginData, ligne*pas); 
ligne++;
pg.drawString(inv.OBSERVATION_INVOICE.substring(sizeObservation+1,inv.OBSERVATION_INVOICE.length()), marginData, ligne*pas); 
}

} 
}  
}

 