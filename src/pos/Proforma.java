/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pos;

/**
 *
 * @author ADMIN
 */
public class Proforma 
{
    public int id_Proforma;
    double total,reduction,tva;
    public String num_Client,nom_client,date,employe,heure,reference,keyinvoice,list;

    public Proforma(int id_Proforma,String num_Client, String nom_client, String date,
            double total, double reduction, double tva, String employe, String heure, 
            String reference,String key)
    {
        this.id_Proforma = id_Proforma;
        this.total = total;
        this.reduction = reduction;
        this.tva = tva;
        this.num_Client = num_Client;
        this.nom_client = nom_client;
        this.date = date;
        this.employe = employe;
        this.heure = heure;
        this.reference = reference;
        this.keyinvoice=key;
    }
    
     public String toString()
    {
    return id_Proforma+"  "+nom_client+"  "+total;
    }
    
    

}
