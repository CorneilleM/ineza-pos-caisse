/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pos;
  
import java.io.IOException; 
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList; 
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import rra.RRA_CLIENT;
import rra.RRA_PACK;
import rra.RRA_PRINT2;
import rra.RRA_RECORD;
import rra.SDC_CONTROLLER;
import smart.*;

/**
 *
 * @author Kimenyi
 */
public class Taxes {
String driver = "org.apache.derby.jdbc.NetWorkDriver";
String dbName; 
String server;
String connectionURL ;
ResultSet outResult ;
Connection conn = null;
Statement s;
PreparedStatement psInsert; 
MRC MRC2;
public Db db;
public static String TIN,SDC_IP;
double amount1=0;
double amount2=0;    
static RRA_PACK invoicePack=null;
public RRA_CLIENT rraClient=null;
public static double tvaRateA, tvaRateB, tvaRateC, tvaRateD; 
SmartPatient smartPatient;
String key_invoice="";
public static String smartXMLForward="";
Taxes(String serverSDC,String server,String dbName)
{ 
this.server=server;
this.dbName=dbName;
connectionURL = "jdbc:derby://"+server+":1527/" + dbName + ";create=false";
read();
 
}
private void read() 
{

    String SDCServer  ;
    try {
        conn = DriverManager.getConnection(connectionURL);
        s = conn.createStatement();
        //s1 = conn.createStatement();
        System.out.println("connected to 11  " + connectionURL);

        this.TIN = getRRA("tin");
        String mac = getMac();
        if (mac == null || (mac != null && mac.length() < 3)) 
        {
            mac = Db.ip;
        }

        SDCServer = getSDCIP(mac);
//  String mac=AutoOut.ip; 
        this.MRC2 =  GetMRC(mac);

        if (SDCServer == null || (SDCServer != null && SDCServer.length() < 4)) 
        {
            SDCServer = server;
        }

        if (MRC2 == null) 
        {
            SDCServer = server;
            JOptionPane.showMessageDialog(null, "NO MRC FOUND FOR:" + mac, "ATTENTION", JOptionPane.ERROR_MESSAGE);
        }
        this.rraClient = new RRA_CLIENT(SDCServer, dbName, new SDC_CONTROLLER(s, conn));
    } catch (Throwable e) {
        System.out.println(e + "errrr to  " + connectionURL);
        insertError(e + " rrrrr", "Read");
    }
    tvaRateA= getTvaRate("A");
tvaRateB= getTvaRate("B");
tvaRateC= getTvaRate("C");
tvaRateD= getTvaRate("D"); 
}

    public void setSmartPatient(SmartPatient smartPatient) {
        this.smartPatient = smartPatient;
    }


void insertError(String desi,String ERR)
    { 
        try {
            psInsert = conn.prepareStatement("insert into APP.ERROR(DESI,USERNAME)  values (?,?)");

            if(desi.length()>160) {
                desi=desi.substring(0, 155);
            }
             if(ERR.length()>160) {
                ERR=ERR.substring(0, 155);
            }  
            
            psInsert.setString(1,desi );
            psInsert.setString(2, ERR);  
            psInsert.executeUpdate();
            psInsert.close();
            JOptionPane.showMessageDialog(null,"  "+desi, db.l(db.lang,"V_IKIBAZO"),JOptionPane.PLAIN_MESSAGE); 
            
        } catch (Throwable ex) {
         JOptionPane.showMessageDialog(null,"  "+ex, db.l(db.lang,"V_IKIBAZO"),JOptionPane.PLAIN_MESSAGE);   
        }
            
                    
          }

  String getRRA(  String nom)
{ 
try
{
outResult = s.executeQuery("select  * from APP.VARIABLES where APP.variables.nom_variable= '"+nom+"RRA'   ");
 while (outResult.next())
{
 return outResult.getString(4); 
}}
catch (Throwable e){
      insertError(e+"","getparm autoout");
 }
return  "";
}
 public static String getRRA2(  String nom, Statement state)
{ 
try
{
ResultSet outResult = state.executeQuery("select  * from APP.VARIABLES where APP.variables.nom_variable= '"+nom+"RRA'   ");
 while (outResult.next())
{
 return outResult.getString(4); 
}}
catch (Throwable e){
    System.err.println(e+" getparm autoout");
 }
return  "";
}
  //int id,String table,
static public String getNowTime(String heure)
{
    //2013-12-18 09:33:50.263
    //0123456789012345678 
    if(heure!=null && heure.length()>20)
    {String heureInvoice=heure.substring(8, 10)+"/"+heure.substring(5, 7)+"/"+heure.substring(0, 4)
            +" "+heure.substring(11, 13)+":"+heure.substring(14, 16)+":"+heure.substring(17, 19);
return heureInvoice;
    }
    else {
        return "NOT A TIME"+heure;
    }
}
 String InvoiceSDCV7(Invoice invo,String key,double money,
         String rtype,String ttype,String tin_cl,String mode)
 {
//     System.err.println("sdc************");  
//     System.err.println("sdc************"); 
//     
////            String time= getNowTime();
            System.err.println(rtype +" *****Taxes**InvoiceSDCV4rrrrrrrrr*******"+ttype); 
 RRA_PACK rp= getRpack4(0,invo.getProductList() ,
             rtype,   ttype,"",MRC2.MRC);  
 if(MRC2!=null)
     {
         if( mode!=null && (!mode.equals("MOBILE")))
         {
             tin_cl = JOptionPane.showInputDialog("CLIENT TIN NUMBER (OPTIONAL) ", ""+tin_cl);
         }
     if(tin_cl==null)
     {
         int n = JOptionPane.showConfirmDialog(null,"VOULEZ VOUS TERMINER CE FACTURE "," ATTENTION ",JOptionPane.YES_NO_OPTION);
        
         if(n==0)
         {
             tin_cl="";
             rp.ClientTin=tin_cl;
         }
         else
         {
             return "AN ERROR MSG : WRONG TAX RATE # PRODUCT ERROR ";     
         }
     }
     else
     {
         int i=0;
         while(tin_cl.length()!=9 && i<3 && tin_cl.length()>=1)
         {
             tin_cl = JOptionPane.showInputDialog("INVALID TIN : "+tin_cl, "");               
             i++;
         }
         
         if(tin_cl.length()!=9 && i>=3)
         {
             rp.ClientTin="";
         }
         else
         {
             rp.ClientTin=tin_cl;
         }  
     }
     System.out.println("ahhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh");
     if(rp!=null)
     {
         System.out.println("fimbooooooooooooooooooooo");
         Db.invoicePack=rp;
         
    String toSend=rraClient.getSaleString2(rp); 
      System.err.println("++++++++++++++++++  "+toSend);
    updateTosend(key, "SEND RECEIPT#"+toSend+"#"+invo.id_employe+"#"+key+"@TIME@"+tin_cl);
    String data =rraClient.sendRequest("SEND RECEIPT",toSend,key,"TIME",tin_cl,rtype,ttype,0);
    
     System.err.println("data:*******"+data);
     if(data!=null && !data.contains("AN ERROR MSG"))
     {
         return data;
     } 
     return data;
     }
     else {
         return "AN ERROR MSG : WRONG TAX RATE # PRODUCT ERROR ";
     }
     }
 else {
         return "AN ERROR MSG : NO MRC FOUND # MRC ERROR ";
 }
 }
 
  String InvoiceSDCV6(Invoice invo,String key,double money,
         String rtype,String ttype,String tin_cl)
 { 
            System.err.println(rtype +" *****rrrrrrrrrrrrrrrrrrrrr*******"+ttype); 
 RRA_PACK rp= getRpack4(0,invo.getProductList() ,
             rtype,   ttype,"",MRC2.MRC);  
 if(MRC2!=null)
     {

     
     if(rp!=null)
     {
         Db.invoicePack=rp;
         
    String toSend=rraClient.getSaleString2(rp); 
      System.err.println("++++++++++++++++++  "+toSend);
    updateTosend(key, "SEND RECEIPT#"+toSend+"#"+invo.id_employe+"#"+key+"@TIME@"+tin_cl);
    String data =rraClient.sendRequest("SEND RECEIPT",toSend,key,"TIME",tin_cl,rtype,ttype,0);
    
     System.err.println("data:*******"+data);
     if(data!=null && !data.contains("AN ERROR MSG"))
     {
         return data;
     } 
     return data;
     }
     else {
             return "AN ERROR MSG : WRONG TAX RATE # PRODUCT ERROR ";
         }
     }
 else {
         return "AN ERROR MSG : NO MRC FOUND # MRC ERROR ";
     }
      
 }
 
  public static  RRA_PACK getRpack4(int id, LinkedList <Product> getProductList,
         String rtype, String ttype,String date_time_mrc,String MRC)
{
double totalpriceA, totalpriceB, totalpriceC, totalpriceD,
          tvaPriceA, tvaPriceB, tvaPriceC, tvaPriceD;
           totalpriceA=0; totalpriceB=0; totalpriceC=0; totalpriceD=0;
          tvaPriceA=0; tvaPriceB=0; tvaPriceC=0; tvaPriceD=0;
    
   for (int i = 0; i < getProductList.size(); i++) {

                Product p2 =  getProductList.get(i); 
//                System.out.println(p2.currentPrice+"pricee"+p2.tva+"eeeeeeee:"+p2.qty);
                double taxable =((p2.salePrice()) / (1 + p2.tva));
                
                
//                System.out.println(taxable+"taxabl:"+p2.qty);
//                if(taxable<0)
//                    arr=-0.5;
//                else
//                    arr=0.5;
                 
                if(p2.tva==tvaRateA)
                {tvaPriceA+=(taxable * p2.tva);
                totalpriceA+=(p2.salePrice());
                getProductList.get(i).classe="A-EX";
                }
                else if(p2.tva==tvaRateD)
                {tvaPriceD+=(taxable * p2.tva);
                totalpriceD+=(p2.salePrice());
                getProductList.get(i).classe="D";
                }
                else if(p2.tva==tvaRateB)
                {tvaPriceB+=((taxable * p2.tva));
                totalpriceB+=(p2.salePrice());
                getProductList.get(i).classe="B";
                }
                else if(p2.tva==tvaRateC)
                {tvaPriceC+=(taxable * p2.tva);
                totalpriceC+=(p2.salePrice());
                getProductList.get(i).classe="C";
                }
                else
                {
                    return null;
                }
   }
    if(MRC==null)
   {
        JOptionPane.showMessageDialog(null, "NO MRC FOUND  ","ATTENTION",JOptionPane.ERROR_MESSAGE);  
 return null;
   }
   System.err.println(totalpriceA +" *****rettu*******"+tvaPriceB);
  return new RRA_PACK(id, 
          totalpriceA, totalpriceB, totalpriceC, totalpriceD,
          tvaPriceA, tvaPriceB, tvaPriceC, tvaPriceD,
          tvaRateA*100, tvaRateB*100, tvaRateC*100, tvaRateD*100,
          rtype,  ttype,MRC,TIN,getProductList,date_time_mrc);
}
  String getRate(double tva)
{  
                if( tva==tvaRateA)
                { 
                 return "A";
                }
                else if( tva==tvaRateD)
                { 
                 return "D";
                }
                else if( tva==tvaRateB)
                { 
                return "B";
                }
                else if( tva==tvaRateC)
                { 
                return "C";
                }
                else
                {
                    return "";
                }
  
}
  
  
public void updateTosend(String key,String data)
{ 
    
    try
     {
         s.execute("update app.SDC_FAGITURE set TOSEND='"+data+"' where KEY_INVOICE='"+key+"'");
     }
     catch(SQLException e)
     {
         insertError(e+" ","tosend data");
     }
     
} 
public double getTvaRate( String rate) {
        
        try {
outResult = s.executeQuery("select  * from APP.VARIABLES "
        + "where APP.variables.famille_variable= 'RRA' AND NOM_VARIABLE LIKE '%taxrate%' "
        + "AND VALUE_VARIABLE='"+rate+"'");
     while (outResult.next()) { 
               return Double.parseDouble(outResult.getString(4))/100.0;
            }
        } catch (Throwable e) {
            insertError(e + "", "getparamFam");
        }
        return 0;
    }
  
  
public  LinkedList <List >    printByChoice(String choix, String rtype)
{
    LinkedList <List >    facture = new LinkedList();
try
    {
if (choix.equals("PAR GROUPE"))                                      
    {
    String [] control= clients();
    String client = (String) JOptionPane.showInputDialog(null, "Fait votre choix", "Impression ",
    JOptionPane.QUESTION_MESSAGE, null, control, "");
    
    String sql="select * from APP.INVOICE where invoice.HEURE >'2012-01-01 00:00:00' "
            + "AND  invoice.tarif='"+client+"' AND COMPTABILISE!='ANNULE'  and"
            + "  (external_data like '%"+rtype+"%' or copy_data like '%"+rtype+"%' ) ORDER BY HEURE ";
    String id="ID_INVOICE";
    if(rtype.contains("R"))
    {  sql=  "select  *     from "
        + " APP.REFUND    where  tarif='"+client+"' "
        + "   and (external_data like '%"+rtype+"%' or copy_data like '%"+rtype+"%' )  ";
    
    id="id_refund";
    }
    System.out.println(sql);
    outResult = s.executeQuery(sql); 
    
    
    
    
     while (outResult.next())
    {
        int id_invoice=  outResult.getInt(id); 
        String clientw =outResult.getString("num_client");
        String z=outResult.getString("date");
        int total=outResult.getInt("total"); 
        facture.add(new List (  id_invoice,0,      total, outResult.getString("NUM_FACT"),  clientw)); 
      }
    }
    if (choix.equals("PAR HEURE"))                                      
    {
    String  Debut = JOptionPane.showInputDialog("ENTREZ LE TEMPS du debut",getRRA("debut"));
    String FIN = JOptionPane.showInputDialog   ("ENTREZ LE TEMPS de la fin ",getRRA("fin")); 
    
    outResult = s.executeQuery("select * from APP.INVOICE where  invoice.HEURE >'"+Debut+"' AND invoice.HEURE <'"+FIN+" ' AND COMPTABILISE!='ANNULE' ORDER BY HEURE ");
       while (outResult.next())
    {
        int id_invoice=  outResult.getInt(1); 
        String clientw =outResult.getString(2);
        String z=outResult.getString(3);
        int total=outResult.getInt(4);
        String id_employe=  outResult.getString(5);
        String time =""+outResult.getTimestamp(6);
        int tva=outResult.getInt(7); 
        double reduction=outResult.getDouble(9);
  facture.add(new List (  id_invoice,0,      total, outResult.getString("NUM_FACT"),  clientw)); 
       }
    }
 outResult.close(); 
    } 
catch (Throwable e)  {
/*  Catch all exceptions and pass them to
**  the exception reporting method             */
System.out.println(" . . inv . exception thrown:");
insertError(e+""," V reimpression facture");
}  
  return facture  ;                                 
}
 String [] clients ()
{
    String [] control ;
    LinkedList<String> clien  =new LinkedList();
    try
    {
    outResult = s.executeQuery("select NOM_VARIABLE from APP.VARIABLES WHERE FAMILLE_VARIABLE='CLIENT' ");
    while (outResult.next())
    {
      clien.add(outResult.getString(1));
    }

    //  Close the resultSet
    outResult.close();

      }  catch (Throwable e)  {

   insertError(e+""," clients");
    }
    control= new String [clien.size()];
    for(int i=0;i<clien.size();i++) {
        control[i]=clien.get(i);
    }
return control;

}
 public void photoCopyR(int id_invoice, String Operation) 
{
 LinkedList <Product> prod2=new LinkedList();
   
  String external_data="external_data"; 
  String heure="heure";
  String time="";
  String employe="";
  int id_ref=0;
 String tin="";
 String mrc="";
  if(Operation.contains("C"))
  {
      external_data="COPY_data";
      heure="heure_copy";
  }
  
String sql="select  num_client,code_uni ,quantite,refund_list.price,refund_list.original_price,"
        + "name_product,refund_list.tva, "+external_data+", "+heure+",refund.id_invoice,refund.tin"
        + ",refund.mrc,employe from APP.REFUND ,APP.refund_list  where APP.REFUND.id_REFUND = " +
        id_invoice+" and APP.REFUND.id_REFUND = APP.refund_list.id_REFUND "
        + "   and "+external_data+" like '%,"+Operation+",%' order by  name_product";
  System.out.println(sql);
try {
outResult = s.executeQuery(sql);
       
 
while (outResult.next()) {
String    client = outResult.getString(1); 
String   code = outResult.getString(2); 
int   quantite = outResult.getInt(3);
double  price = outResult.getDouble(4);
double  orig = outResult.getDouble(5);
String     desi = outResult.getString(6); 

if(price!=orig) {
        desi += "# Discount -"+(int)(100-Math.abs((price*100)/orig))+"%";
    }
//
//System.out.println(desi+"      "+price+" **************************************** "+orig+"    "+Math.abs((price*100)/orig)+"      "+(100-Math.abs((price*100)/orig)));
double  tva = outResult.getDouble(7); 
external_data=outResult.getString(8);
time=outResult.getString(9); 
id_ref=outResult.getInt(10);
tin=outResult.getString("tin"); 
mrc=outResult.getString("mrc"); 
employe=outResult.getString("employe"); 

Product pro=new Product(0, desi, code, quantite, price, tva, code, "", "", null);
 
pro.originalPrice=orig;
prod2.add(pro);
} 

outResult.close();   

 } catch (SQLException ex) {
Logger.getLogger(Db.class.getName()).log(Level.SEVERE, null, ex);
}
 LinkedList<String> infoClient=Leclient(getClient(id_ref));
 String Rtype=Operation.substring(0, 1), Ttype=Operation.substring(1, 2); 
 RRA_PACK rp = getRpack4(id_invoice, prod2, Rtype, Ttype, getNowTime(time),mrc); 
 if(rp!=null)
 {
     rp.ClientTin=tin;
     printRRA(external_data,Rtype, Ttype, rp.getProductList, rp, id_invoice,id_ref,infoClient,employe,"","",0,0,0,0);
 } 
 
}
 
public void photoCopyS(int id_invoice, String Operation,boolean second,double money,double change) 
{
 LinkedList <Product> prod2=new LinkedList();
   
  String external_data="external_data"; 
  String date_time_mrc=""; 
  String tin="";  String mrc="";
   String heure="heure";
   String employe="";
   String quitance="";
   String nom_client="";
   String    client="";
   double cash=0;
   double credit=0;
  
  
  if(Operation.contains("C"))
  {
      external_data="COPY_data";
      heure="heure_copy";
  }
  
  
String sql=""
+ "select  num_client, code_uni ,quantite,list.price,list.original_price,"
+ "name_product,list.tva, "+external_data+",  "+heure+",invoice.tin,invoice.mrc,employe,"
        + " name_client,cash,credit,DESIGNATION_FRSS "
+ "from APP.PRODUCT,APP.INVOICE ,APP.LIST "
+ " where     "
+ "  APP.INVOICE.id_invoice = " + id_invoice
+ " and APP.INVOICE.id_invoice = APP.LIST.id_invoice  "
+ " AND APP.PRODUCT.CODE=APP.LIST.CODE_UNI and "+external_data+" like '%,"+Operation+",%'"
+ " order by  name_product";
  System.out.println(sql);
try {
outResult = s.executeQuery(sql); 
 
while (outResult.next()) {
client = outResult.getString(1); 
String   code = outResult.getString(2); 
int   quantite = outResult.getInt(3);
double  price = outResult.getDouble(4);
double  orig = outResult.getDouble(5);
String     desi = outResult.getString(6); 
if(price!=orig) {
desi += "# Discount -"+(int)(100-Math.abs((price*100)/orig))+"%";
    }
double  tva = outResult.getDouble(7); 
external_data=outResult.getString(8);
date_time_mrc=outResult.getString(9); 
tin=outResult.getString("tin");
mrc=outResult.getString("mrc");
employe=outResult.getString("employe");
nom_client=outResult.getString("name_client");
cash=outResult.getDouble("cash");
credit=outResult.getDouble("credit");

if(Main.isSpecial) 
{
desi=outResult.getString("DESIGNATION_FRSS");

}

Product pro=new Product(0, desi, code, quantite, price, tva, code, "", "", null);
pro.originalPrice=orig;
prod2.add(pro);
} 
outResult.close();  


outResult = s.executeQuery("select APP.CREDIT.NUMERO_QUITANCE FROM APP.CREDIT WHERE APP.CREDIT.ID_INVOICE="+id_invoice+""); 

while (outResult.next()) 
{
  quitance = outResult.getString(1); 
}
outResult.close();  

 } catch (SQLException ex) {
Logger.getLogger(Db.class.getName()).log(Level.SEVERE, null, ex);
}

String Rtype=Operation.substring(0, 1), Ttype=Operation.substring(1, 2); 
LinkedList<String> infoClient=Leclient(getClient(id_invoice));
System.out.println(" Operation Operation   "+Operation);
 RRA_PACK rp = getRpack4(id_invoice, prod2, Rtype, Ttype, getNowTime(date_time_mrc),mrc); 
if(rp!=null)
{
    
rp.ClientTin=tin;
System.out.println("SECOND:"+second+"  CLIENT ");
System.out.println(" photoCopyS CLIENT "+client);
System.out.println("  CLIENT2 "+nom_client);

if(nom_client==null) {
        nom_client="";
    }

if(  second && client!=null && client.equals("CASHNORM") && nom_client.length()<3)
{
    try{
    int nn = JOptionPane.showConfirmDialog(null,  " VOULEZ-VOUS METTRE LE NOM SUR LE FACTURE? ", " NOM DU CLIENT ", JOptionPane.YES_NO_OPTION);
    if (nn == 0) 
    {
        String izina= (JOptionPane.showInputDialog(Main.cashier.db.l(Main.cashier.db.lang, "V_NAMEORSURNAME")," "));
        if(izina==null)
        {
            nom_client="";
        }
        else
        {
            nom_client=izina;
            s.execute("UPDATE APP.INVOICE SET NAME_CLIENT='"+izina+"' WHERE ID_INVOICE="+id_invoice+"");
        }
        
    }
    }
    catch(Exception ex)
    {
        nom_client="";
    }
}

printRRA(external_data,Rtype, Ttype, rp.getProductList, rp, id_invoice,0,infoClient,employe,
        quitance,nom_client,money,change,cash,credit);

}  
}

public void printViewInvoice(int id_invoice) 
{  
        String printStringUp = "   INVOICE VIEW ";
        String printStringE = "";
        
                
                
  String external_data="external_data"; 
  String date_time_mrc=""; 
  String tin="";  String mrc="";
   String heure="heure";
   String employe="";
   String quitance="";
   String nom_client="";
   String    client="";
   double cash=0;
   double credit=0;
   
String sql=""
+ "select  num_client, code_uni ,quantite,list.price,list.original_price,"
+ "name_product,list.tva, "+external_data+",  "+heure+",invoice.tin,invoice.mrc,employe,name_client,cash,credit "
+ "from APP.PRODUCT,APP.INVOICE ,APP.LIST "
+ " where     "
+ "  APP.INVOICE.id_invoice = " + id_invoice
+ " and APP.INVOICE.id_invoice = APP.LIST.id_invoice  " 
        + " AND APP.PRODUCT.CODE=APP.LIST.CODE_UNI  "
+ " order by  name_product";
  System.out.println(sql);
try {
outResult = s.executeQuery(sql); 
 
while (outResult.next()) {
client = outResult.getString(1); 
String   code = outResult.getString(2); 
int   quantite = outResult.getInt(3);
double  price = outResult.getDouble(4);
double  orig = outResult.getDouble(5);
String     desi = outResult.getString(6); 
if(price!=orig) {
desi += "# Discount -"+(int)(100-Math.abs((price*100)/orig))+"%";
    }
double  tva = outResult.getDouble(7); 
external_data=outResult.getString(8);
date_time_mrc=outResult.getString(9); 
tin=outResult.getString("tin");
mrc=outResult.getString("mrc");
employe=outResult.getString("employe");
nom_client=outResult.getString("name_client");
cash=outResult.getDouble("cash");
credit=outResult.getDouble("credit");
Product pro=new Product(0, desi, code, quantite, price, tva, code, "", "", null);
pro.originalPrice=orig;

  printStringE = printStringE + "\n  " + quantite + "  " + desi + "    " + (int) (quantite * price);
                  

} 
outResult.close();  


outResult = s.executeQuery("select APP.CREDIT.NUMERO_QUITANCE FROM APP.CREDIT WHERE APP.CREDIT.ID_INVOICE="+id_invoice+""); 

while (outResult.next()) 
{
  quitance = outResult.getString(1); 
}
outResult.close();  

 } catch (SQLException ex) {
Logger.getLogger(Db.class.getName()).log(Level.SEVERE, null, ex);
} 

Client infoClient= (getClient(id_invoice));

printStringUp = printStringUp + "\n    ID     : " + id_invoice;
printStringUp = printStringUp + "\n    " + date_time_mrc;
printStringUp = printStringUp + "\n    -------------------------------------------   ";
printStringUp = printStringUp + "\n    Group            : " + client; 
printStringUp = printStringUp + "\n    CODE            : " + infoClient.NUM_AFFILIATION; 
printStringUp = printStringUp + "\n    Name            : " + infoClient.NOM_CLIENT + infoClient.PRENOM_CLIENT; 
 
printStringUp = printStringUp + "\n    Ref     : " + quitance;
printStringUp = printStringUp + "\n    -------------------------------------------   ";
  
printStringE = printStringE + "\n  "; 
printStringE = printStringE + "\n  TOTAL en RWF: " + (cash+credit); 
printStringE = printStringE + "\n  CREDIT " + credit;
printStringE = printStringE + "\n  CASH  " +cash;
printStringE = printStringE + "\n  CLICK NO TO PRINT  " ;


//System.out.println(printStringUp
//+ "\n" + printStringE);
 
 int n=JOptionPane.showConfirmDialog(null,
printStringUp
+ "\n" + printStringE,
" INVOICE VIEW ", JOptionPane.YES_NO_OPTION);
 
 if(n!=0)
 {
     photoCopyS(id_invoice, "NS",true,0,0);                                
 }
}

public String photoCopySilencieux(int id_invoice, String Operation,String waitress) 
{
 LinkedList <Product> prod2=new LinkedList();
   
  String external_data="external_data"; 
  String date_time_mrc=""; 
  String tin="";  String mrc="";
   String heure="heure";
   String employe="";
   String quitance="";
   String nom_client="";
   String    client="";
  
  if(Operation.contains("C"))
  {
      external_data="COPY_data";
      heure="heure_copy";
  }
  
  
String sql=""
+ "select  num_client, code_uni ,quantite,list.price,list.original_price,"
+ "name_product,list.tva, "+external_data+",  "+heure+",invoice.tin,invoice.mrc,employe,name_client "
+ "from APP.PRODUCT,APP.INVOICE ,APP.LIST "
+ " where     "
+ "  APP.INVOICE.id_invoice = " + id_invoice
+ " and APP.INVOICE.id_invoice = APP.LIST.id_invoice  "
+ " AND APP.PRODUCT.CODE=APP.LIST.CODE_UNI and "+external_data+" like '%,"+Operation+",%'"
+ " order by  name_product";
  System.out.println(sql);
try {
outResult = s.executeQuery(sql); 
 
while (outResult.next()) {
client = outResult.getString(1); 
String   code = outResult.getString(2); 
int   quantite = outResult.getInt(3);
double  price = outResult.getDouble(4);
double  orig = outResult.getDouble(5);
String     desi = outResult.getString(6); 
if(price!=orig) {
desi += "# Discount -"+(int)(100-Math.abs((price*100)/orig))+"%";
    }
double  tva = outResult.getDouble(7); 
external_data=outResult.getString(8);
date_time_mrc=outResult.getString(9); 
tin=outResult.getString("tin");
mrc=outResult.getString("mrc");
employe=outResult.getString("employe");
nom_client=outResult.getString("name_client");
Product pro=new Product(0, desi, code, quantite, price, tva, code, "", "", null);
pro.originalPrice=orig;
prod2.add(pro);
} 
outResult.close();  


outResult = s.executeQuery("select APP.CREDIT.NUMERO_QUITANCE FROM APP.CREDIT WHERE APP.CREDIT.ID_INVOICE="+id_invoice+""); 

while (outResult.next()) 
{
  quitance = outResult.getString(1); 
}
outResult.close();  

 } catch (SQLException ex) {
Logger.getLogger(Db.class.getName()).log(Level.SEVERE, null, ex);
}

String Rtype=Operation.substring(0, 1), Ttype=Operation.substring(1, 2);

LinkedList<String> infoClient=Leclient(getClient(id_invoice));

System.out.println(" Operation Operation   "+Operation);
 RRA_PACK rp = getRpack4(id_invoice, prod2, Rtype, Ttype, getNowTime(date_time_mrc),mrc); 
 
if(rp!=null)
{
    
rp.ClientTin=tin; 
 
if(nom_client==null) {
        nom_client="";
    }
 
printRRA(external_data,Rtype, Ttype, rp.getProductList, rp, id_invoice,0,infoClient,employe,quitance,
        "Info: "+waitress,0,0,0,0);
}  
return external_data;
}
public static boolean printRRA3(String data,String rtype,String ttype,LinkedList<Product> prod2,
        RRA_PACK rp,int id_invoice,int id_ref,LinkedList<String>infoClient,
        String umukozi,String quitance,String nom_client,double money,double change,
        double cash,double credit,Statement state,String STATUS,SmartPatient smartPatient,String key_invoice )
{
    
    
    //0 SDC001000638,
    //1 465,
    //2 743,
    //3 NS,
    //4 17/12/2013 18:02:39,
    //5 AVR6PYZDNGFEICTD,
    //6 2NPRWVP2X7YCR2GQ5NOA2G7W7A
    int neg=1;
    if(ttype.equals("R")) {
        neg=-1;
    }
    data = data.replaceAll(",,", ",#,#");
    String[] sp = data.split(",");
     int margin=5;
        String marginVar= "5";
        try
        { margin=Integer.parseInt(marginVar); }
        catch (NumberFormatException ex)
        {System.err.println(marginVar +"   marginVar   "+ex); }
    
    if(sp.length!=7)
    {
        new RRA_PRINT2(getRRA2("company",state),getRRA2("bp",state),getRRA2("City",state), getRRA2("tin",state), ""+rp.ClientTin,
                (rp.getTotal()),  (rp.getTotalA()), (rp.getTotalB()), rp.getTotalTaxes(), rp.getTotalTaxes(),
                rp.getTotal(), ""  ,  "/"  + "  "  ,"","", id_invoice,  rp.MRC, prod2,rtype,
                ttype,getRRA2("version",state),neg,id_ref,infoClient,rp.MRCtime, "",
                umukozi+" STATUS: "+STATUS,quitance,Main.cashier.db.s,nom_client,margin,money,change);
        
        
        
        
        return false;        
    }
    else
    {   
   new RRA_PRINT2(getRRA2("company",state),getRRA2("bp",state),getRRA2("City",state), getRRA2("tin",state), ""+rp.ClientTin,
   (rp.getTotal()),  (rp.getTotalA()), (rp.getTotalB()), rp.getTotalTaxes(), rp.getTotalTaxes(),
   rp.getTotal(),  sp[0], sp[1] + "/" + sp[2] + "  " + sp[3],
   sp[6],sp[5], id_invoice,  rp.MRC, prod2,rtype,
   ttype,getRRA2("version",state),neg,id_ref,infoClient,rp.MRCtime, sp[4],umukozi+" STATUS: "+STATUS,
           quitance,Main.cashier.db.s,
           nom_client,margin,money,change);
    return true;
    }
}
 boolean printRRA(String data,String rtype,String ttype,LinkedList<Product> prod2,
        RRA_PACK rp,int id_invoice,int id_ref,LinkedList<String>infoClient,
        String umukozi,String quitance,String nom_client,double money,double change,double cash,double credit)
{
    System.out.println("patient:"+smartPatient+"");
    System.out.println("condition:"+(!Main.allow_smart.equalsIgnoreCase("NO")));
    if((!Main.allow_smart.equalsIgnoreCase("NO")) && smartPatient!=null )
    {         
smartXMLForward=smartXml( prod2 ,  rp,  id_invoice,infoClient.get(3),infoClient.get(2),infoClient.get(0),
        infoClient.get(1),"2000-01-01",cash,credit,smartPatient,s);
       System.out.println("yahageze");
     
Db.updateSmartTracking2(" ID_INVOICE="+id_invoice+"" , smartPatient.global_id,
        " AND  KEY_INVOICE='"+key_invoice+"'",s);
        System.out.println( smartXMLForward);
    } 
    
    //0 SDC001000638,
    //1 465,
    //2 743,
    //3 NS,
    //4 17/12/2013 18:02:39,
    //5 AVR6PYZDNGFEICTD,
    //6 2NPRWVP2X7YCR2GQ5NOA2G7W7A
    int neg=1;
    if(ttype.equals("R")) {
        neg=-1;
    }
    data = data.replaceAll(",,", ",#,#");
    String[] sp = data.split(",");
     int margin=5;
        String marginVar= getRRA("margin");
        try
        { margin=Integer.parseInt(marginVar); }
        catch (NumberFormatException ex)
        {System.err.println(marginVar +"   marginVar   "+ex); }
     
                System.out.println("marginVar  "+marginVar);
    if(sp.length!=7)
    {
        new RRA_PRINT2(getRRA("company"),getString("bp"),getRRA("City"), getRRA("tin"), ""+rp.ClientTin,
                (rp.getTotal()),  (rp.getTotalA()), (rp.getTotalB()), rp.getTotalTaxes(), rp.getTotalTaxes(),
                rp.getTotal(), ""  ,  "/"  + "  "  ,"","", id_invoice,  rp.MRC, prod2,rtype,
                ttype,getRRA("version"),neg,id_ref,infoClient,rp.MRCtime, "",
                umukozi,quitance,Main.cashier.db.s,nom_client,margin,money,change);
        
        
        
        
        return false;        
    }
    else
    {   
   new RRA_PRINT2(getRRA("company"),getString("bp"),getRRA("City"), getRRA("tin"), ""+rp.ClientTin,
   (rp.getTotal()),  (rp.getTotalA()), (rp.getTotalB()), rp.getTotalTaxes(), rp.getTotalTaxes(),
   rp.getTotal(),  sp[0], sp[1] + "/" + sp[2] + "  " + sp[3],
   sp[6],sp[5], id_invoice,  rp.MRC, prod2,rtype,
   ttype,getRRA("version"),neg,id_ref,infoClient,rp.MRCtime, sp[4],umukozi,quitance,Main.cashier.db.s,
           nom_client,margin,money,change);
    return true;
    }
}

RRA_PRINT2 printRRAa4(String data,String rtype,String ttype,LinkedList<Product> prod2
        ,RRA_PACK rp,int id_invoice)
{
    data = data.replaceAll(",,", ",#,#");
    String[] sp = data.split(",");
      int margin=5;
        String marginVar= getRRA("margin");
        try
        { margin=Integer.parseInt(marginVar); }
        catch (NumberFormatException ex)
        {System.err.println(marginVar +"   marginVar   "+ex); }
    if(sp.length!=7)
    {
        
        return   new RRA_PRINT2(getRRA("company"), getRRA("bp"), getRRA("City"), getRRA("tin"), "00000",
                (rp.getTotal()), (rp.getTotalA()), (rp.getTotalB()), rp.getTotalTaxes(), rp.getTotalTaxes(), 
                rp.getTotal(),    "", "" + "/" + "  " , "", "", id_invoice,rp.MRC,  rtype, ttype,
                getRRA("version"),rp.MRCtime, "","",Main.cashier.db.s,margin); 
    }
    else
    {    
        
        
        return   new RRA_PRINT2(getRRA("company"), getRRA("bp"), getRRA("City"), getRRA("tin"), "00000",
                (rp.getTotal()), (rp.getTotalA()), (rp.getTotalB()), rp.getTotalTaxes(), rp.getTotalTaxes(), 
                rp.getTotal(),    sp[0], sp[1] + "/" + sp[2] + "  " + sp[3],sp[6], sp[5], id_invoice, rp.MRC, 
                rtype, ttype,getRRA("version"),rp.MRCtime, sp[4],"",Main.cashier.db.s,margin);
        
    } 
}

static String smartXml(LinkedList<Product> prod2 ,RRA_PACK rp,int id_invoice,String Scheme_Code,String Scheme_Plan,String First_Name,String Surname,
String Date_Of_Birth,double cash,double credit,SmartPatient smartpatient,Statement stat) 
{ 
String practice="";
if(smartpatient!=null)
{
    practice=smartpatient.PracticeNumber;
}
    
    String Start_Date=rp.MRCtime.substring(0, 11).replaceAll("/", "-");
    String Start_Time=rp.MRCtime.substring(10); 
    System.out.println("mrc time:"+Start_Date);
            
       String doXmlServices=  "" ;  
            
  for (int i = 0; i < prod2.size(); i++) {
                    Product p = (Product) prod2.get(i);

                    String name = p.productName;
                    System.out.println("name::"+name);
                    
                    
 String doXmlDignostic = SmartFwdXml.doXmlDignostic( "SP","ISHYIGA",p.code,"Medication"
         ,"BUP",p.code,p.productName,p.qty,
         p.salePrice());


String doXmlService    = SmartFwdXml.doXmlService(practice,id_invoice,"INV0"+id_invoice,  Start_Date,  Start_Time,
        "SH",  "REASON" ,  doXmlDignostic,i+1)  ;
  doXmlServices+=doXmlService;                  
}
    int pool_number=1;
try
{
    pool_number= (Integer.parseInt(smartpatient.pool_number));
}
catch (Exception NumberFormatException)
{ 
  System.out.println(NumberFormatException+" NumberFormatException "+pool_number);
}
  
  
  return SmartFwdXml.doXmlClaim(practice,id_invoice,Start_Date,Start_Time,pool_number , doXmlServices,  Scheme_Code,  Scheme_Plan,  First_Name,  Surname,
        Date_Of_Birth,getRRA2("company",stat),cash,credit,smartpatient,
        prod2.size(),  rp.getTotal())  ; 
}
String getString(String s)
{
    String ret=" ";
    Variable var= getParam("RUN","MAIN",s+"MAIN");
    if(var!=null) {
        ret=var.value2;
    }
    return ret;
} 
Variable getParam(String famille,String mode, String nom)
{
  // System.out.println("select  * from APP.VARIABLES where APP.variables.nom_variable= '"+nom+"'   and APP.variables.famille_variable= '"+famille+"' ");
    Variable par = null;
try
{
    
                System.out.println("nom variable:"+nom+"famille"+famille);
outResult = s.executeQuery("select  * from APP.VARIABLES where APP.variables.nom_variable= '"+nom+"'  and APP.variables.famille_variable= '"+famille+"' ");
//System.out.println("select  * from APP.VARIABLES where APP.variables.nom_variable= '"+nom+"'  and APP.variables.famille_variable= '"+famille+"' ");

while (outResult.next())
{
  //  System.out.println(outResult.getString(2));
        String num =outResult.getString(1);
        String value =outResult.getString(2);
        String fam =outResult.getString(3);
        String value2 =outResult.getString(4);
        par=(new Variable(num,value,fam,value2));
}}
catch (Throwable e){
      insertError(e+"","getparm autoout");
 }
return     par;
}

String getMac()
   {
       try {
                Process p = Runtime.getRuntime().exec("getmac /fo csv /nh");
                java.io.BufferedReader in = new java.io.BufferedReader(new  java.io.InputStreamReader(p.getInputStream()));
                String line;
                line = in.readLine();        
                String[] result = line.split(",");
                String res=result[0].replace('"', ' ').trim();
                System.out.println(res);
                return res;
        } catch (IOException ex) {
            insertError(ex+""," getMac");
            System.out.println("getmac:"+ex);
        }
       return "Disabled";
       
   }

public MRC GetMRC(String Mac)
{
     MRC allmrc;
    try
    {
        
        outResult = s.executeQuery("  select  * from APP.SDC_MRC where APP.SDC_MRC.MAC_ADDRESS= '"+Mac+"'   ");
        //System.out.println("  select  * from APP.LANGUE where variable= '"+var+"'");
        while (outResult.next())
        {
            allmrc=(new MRC(outResult.getString(1), outResult.getString(2), 
                    outResult.getString(3), outResult.getString(4), 
                    outResult.getString(5), outResult.getString(5),outResult.getString(8)));
            return  allmrc;
        } 
    }
    catch (Throwable e)
    {
        insertError(e+"","getMrc");
    }
    return null;
    
}

//String getMRC(String Mac)
//{
//    try
//    {
//        outResult = s.executeQuery("select  MRC from APP.SDC_MRC where APP.SDC_MRC.MAC_ADDRESS= '"+Mac+"'   ");
//        while (outResult.next())
//        {
//            return outResult.getString(1); 
//        }
//    }
//    catch (Throwable e)
//    {
//        insertError(e+"","getMRC");
//    }
//    return null;
//}

String getSDCIP(String Mac)
{
    try
    {
        outResult = s.executeQuery("select  SDC_IP from APP.SDC_MRC where APP.SDC_MRC.MAC_ADDRESS= '"+Mac+"'   ");
        while (outResult.next())
        {
            return outResult.getString(1); 
        }
    }
    catch (Throwable e)
    {
        insertError(e+"","getMRC");
    }
    return null;
}

Client getClient(int id_invoice)
{
    Client infoClient= null;
    String client="";
    String   num_affiliation="";
    String  nom_client="";  
    String   numero_quitance  ="";
    int percentage=0;
    String prenom_client="";
    String employeur="";    
    double solde=0,payed=0;
    double perc=0; 
    
    try
{
// Clients Societe
    
    String sql="" +
             "select num_client,numero_affilie,numero_quitance,client.percentage,nom_client,prenom_client,"
         + "invoice.key_invoice,client.employeur,invoice.cash,invoice.credit from  APP.INVOICE,app.credit,client "
         + "where invoice.id_invoice=credit.id_invoice  " +
             " and client.num_affiliation=credit.numero_affilie and  APP.INVOICE.id_invoice = "+ id_invoice ;
    System.out.println(sql);
 outResult = s.executeQuery(sql );
 
       
    while (outResult.next())
    { 
        client=outResult.getString("num_client");
        num_affiliation=outResult.getString("numero_affilie");
        nom_client=outResult.getString("nom_client"); 
        numero_quitance  =outResult.getString("numero_quitance"); 
        percentage=outResult.getInt("percentage");
        prenom_client=outResult.getString("prenom_client");
        employeur=outResult.getString("employeur");
        solde=outResult.getDouble("credit");
        payed=outResult.getDouble("cash");
        System.out.println("solde:"+solde+" payed:"+payed);
        perc = (((double) solde / (double) (payed+solde)) * 100);
        key_invoice=outResult.getString("key_invoice");
        System.out.println("perc:"+perc);        
        infoClient=new Client(num_affiliation, nom_client, prenom_client,percentage, 0, ""+client, ""+employeur, "", "", ""+num_affiliation);
        if(perc>0)
        {
            infoClient.PERCENTAGE_PART=perc;
        }
        return infoClient;
    }
    outResult.close();
    
    if(infoClient==null)
    {
           
    outResult = s.executeQuery("select num_client,numero_affilie,nom_client,prenom_client,numero_quitance,client_RAMA.percentage,employeur,invoice.key_invoice"
            + " from APP.INVOICE,APP.CREDIT,APP.CLIENT_RAMA "
            + "where APP.INVOICE.id_invoice =  " + id_invoice + "   "
            + "and APP.INVOICE.id_invoice = APP.credit.id_invoice and APP.client_RAMA.num_affiliation = APP.credit.numero_affilie  order by app.invoice.id_invoice");

            System.out.println("select num_client,numero_affilie,nom_client,prenom_client,numero_quitance,client_RAMA.percentage,employeur"
            + " from APP.INVOICE,APP.CREDIT,APP.CLIENT_RAMA "
            + "where APP.INVOICE.id_invoice =  " + id_invoice + "   "
            + "and APP.INVOICE.id_invoice = APP.credit.id_invoice and APP.client_RAMA.num_affiliation = APP.credit.numero_affilie  order by app.invoice.id_invoice");

            while (outResult.next()) {
                client = outResult.getString(1);
                num_affiliation = outResult.getString(2);
                nom_client = outResult.getString(3);
                prenom_client = outResult.getString(4);
                numero_quitance = outResult.getString(5);
                percentage = outResult.getInt(6);
                employeur = outResult.getString(7);
                key_invoice=outResult.getString("key_invoice");
                infoClient=new Client(num_affiliation, nom_client, prenom_client,percentage, 0, ""+client, ""+employeur, "", "", ""+num_affiliation);
    
            }
            outResult.close(); 
            if(infoClient==null)
            {
                outResult = s.executeQuery("select name_client,key_invoice from APP.INVOICE "
                        + "where APP.INVOICE.id_invoice =  " + id_invoice + "   ");
                System.out.println("select name_client from APP.INVOICE "
                        + "where APP.INVOICE.id_invoice =  " + id_invoice + "   ");
                
                while (outResult.next()) 
                {
                    client = outResult.getString(1); 
                    key_invoice=outResult.getString("key_invoice");
                }
                return new Client("DIVERS", "", " "+client,0, 0, ""+"", ""+"", "", "", ""+"");
            }
            else
            {                
            return infoClient;
            }
    }    
    }
    catch (Throwable e)  
    {
        insertError(e+"","retour ");
    }
    return new Client("DIVERS", "", "CLIENT DIVERS",0, 0, ""+"", ""+"", "", "", ""+"");
}
public LinkedList<String> Leclient(Client infoClient)
{
    LinkedList<String> Clientinfo=new LinkedList<String>();
    if(infoClient!=null)
    {
        Clientinfo.add(infoClient.NUM_AFFILIATION);
        Clientinfo.add(infoClient.NOM_CLIENT);
        Clientinfo.add(infoClient.PRENOM_CLIENT);
        Clientinfo.add(infoClient.ASSURANCE);
        Clientinfo.add(""+infoClient.PERCENTAGE);
        Clientinfo.add(infoClient.EMPLOYEUR); 
        Clientinfo.add(""+infoClient.PERCENTAGE_PART);
        return Clientinfo;
    }
          
    return null;
}
String getRtype(String EXT,String COPY)
{ 
    String RTYPE="";
     if(EXT.contains("NS"))
       {
       if(COPY.contains("CS")) {
               RTYPE="NS + CS";
           }
       else {
               RTYPE="NS";
           }
       }
       else if(EXT.contains("NR"))
       {
       if(COPY.contains("CR")) {
               RTYPE="NR + CR";
           }
       else {
               RTYPE="NR";
           }
       } 
       else if(EXT.contains("TS"))
       {RTYPE="TS";}
       else if(EXT.contains("TR"))
       {RTYPE="TR";}
       else if(EXT.contains("PS"))
       {RTYPE="PS";}  
     
return RTYPE;

}
public RRA_RECORD getInvoice (int ID_INVOICE,String CHOIX,String TABLE )
{ 
try
{  
outResult =  s.executeQuery("select * from APP."+TABLE+" where   ID_"+TABLE+"="+ID_INVOICE+"");
  RRA_RECORD r=null;
  int refInv=0;
while ( outResult.next())
{
  String EXT=outResult.getString("EXTERNAL_DATA");
  String COPY=outResult.getString("COPY_DATA");  
  String RTYPE=getRtype(  EXT,  COPY);
r= new RRA_RECORD
(outResult.getInt(1) ,RTYPE,outResult.getString("NUM_CLIENT") , outResult.getString("EMPLOYE") ,
 outResult.getString("HEURE") , EXT , COPY   ,outResult.getString("MRC") ,outResult.getString("TIN") ,
 outResult.getDouble("TOTAL") , outResult.getDouble("TVA")   , outResult.getDouble("TOTAL_A")  , 
 outResult.getDouble("TOTAL_B")  ,  outResult.getDouble("TOTAL_C")  , outResult.getDouble("TOTAL_D")   ,
 outResult.getDouble("TAXE_A") , outResult.getDouble("TAXE_B") , outResult.getDouble("TAXE_C")   , 
 outResult.getDouble("TAXE_D"),outResult.getString("DATE"), outResult.getDouble("CASH"), outResult.getDouble("CREDIT") )   ;  
r.MRC=outResult.getString("MRC");
if(TABLE.equals("REFUND"))
{
    r.raison=outResult.getString("raison");
    refInv=outResult.getInt("id_invoice");
    r.id_ref=refInv;
    System.out.println("ndayifashe  "+r.raison);
} 
}

if(r==null)
{
JOptionPane.showMessageDialog(null, ID_INVOICE+db.l(db.lang,"V_NTIBAHO"));

}
 outResult.close();
 if(TABLE.equals("REFUND")) {
        r.umukiliya=   Main.tax.getClient(refInv);
    } 
 else {
        r.umukiliya=   Main.tax.getClient(ID_INVOICE);
    } 
 
 return r;
}  catch (Throwable e)  {
/*       Catch all exceptions and pass them to
**       the exception reporting method             */
System.out.println(" . .invexception thrown:");
JOptionPane.showMessageDialog(null, ID_INVOICE+db.l(db.lang,"V_NTIBAHO"));

 insertError(e+""," V inv ");
}
return null;
}
  public static void main(String[] arghs) {
      
      System.out.println(getNowTime("2013-12-18 09:33:50.263"));
  }  
}
