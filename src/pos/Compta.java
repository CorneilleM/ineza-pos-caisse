/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pos;

import java.sql.Date;

/**
 *
 * @author Kimenyi Mabilisi
 */
public class Compta {

    String livre,libele,intitule,date,cpte;
    int compte;
    double montant_cash,montant_credit,tva,valeur;
    double TOTAL_A,TOTAL_B,TOTAL_C,TOTAL_D,TAXE_A,TAXE_B,TAXE_C,TAXE_D;

    Compta(String livre,String libele,String intitule,int compte,double montant_cash,double montant_credit,String date,String cpte,double tva,double TOTAL_A,double TOTAL_B,double TOTAL_C,double TOTAL_D,double TAXE_A,double TAXE_B,double TAXE_C,double TAXE_D)
    {
    this.libele=libele;
    this.livre=livre;
    this.compte=compte;
    this.intitule=intitule;
    this.montant_cash=montant_cash;
    this.montant_credit=montant_credit;
    this.date=date;
    this.cpte=cpte;
    this.tva=tva;
    this.TOTAL_A=TOTAL_A;
    this.TOTAL_B=TOTAL_B;
    this.TOTAL_C=TOTAL_C;
    this.TOTAL_D=TOTAL_D;
    this.TAXE_A=TAXE_A;
    this.TAXE_B=TAXE_B;
    this.TAXE_C=TAXE_C;
    this.TAXE_D=TAXE_D;
    
    }

}
