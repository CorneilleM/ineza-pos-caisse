package pos;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author aimable
 */

/*
Interface Stock copyright 2007 Kimenyi Aimable Gestion des ventes et du stock
*/
 
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import javax.swing.JFrame;
import javax.swing.filechooser.FileNameExtensionFilter;

public  class Data extends JFrame
{
    private  String nom;                                // le nom du Caissier
    static int counterInvoice;
    final int LENTH=770;
    final int WIDT =800;
    private  JButton priceButton;                       // le boutton qui envoie le selection
    private  JButton qtyButton; 
    private  JButton autoButton;// le boutton qui donne l'autorisation
    private  JButton addButton,saveButton,vedetteButton;                        // le botton qui ajoute les produits
    private  JList  allProductList=new JList();        // un JList qui affiche tous tout les produits
    private  JScrollPane  scrollPaneProductList;     	// si il ya trop des produits qui depasse le Jpanel
    private  JPanel productPane ;
    private Cashier cashier; // Caisse courant
    private String stock;
    private String var;
    private JLabel searchL;
    private JTextField search;
    private JButton inStock;
    private JButton allProd;
    private JButton sommeil;
    private JButton okprod;
    Font police;
    int today;
    LinkedList <Product> allPro=new LinkedList<Product>();

/********************************************************************************
le constructeur qui cree un frame
********************************************************************************/
 Data(Cashier cashier,String stock,String var,int today) 
{ 
police = new Font(cashier.db.getString("font"), Integer.parseInt(cashier.db.getString("style")),Integer.parseInt(cashier.db.getString("length")));
this.cashier= cashier; 
this.var=var;
this.stock=stock;
this.today=today;
counterInvoice=1; 
nom=cashier.cashierName; 
showProduct();
draw(); 
AllPRoduct();
InStockPRoduct();
InSommeilPRoduct();
InCurrentPRoduct();
searchZOOMIT();
Autorisation();

} 
// on dessine les composants
public void draw() 
{
    setTitle(nom+"'s Stock Session");
    
    enableEvents(WindowEvent.WINDOW_CLOSING);
    Image im = Toolkit.getDefaultToolkit().getImage("logo.png");
    enableEvents(WindowEvent.WINDOW_CLOSING);

    //on fixe  les composant del'interface
    Container content = getContentPane();
    westPane(content);
    eastPane(content);

            priceProduct();  // Ajout manuel
            addProduct();   // Ajout preselectionne
            qtyProduct();
            ved();

     setSize(WIDT,LENTH);
     setVisible(true);

}

// Methode qui Montre tout le stock
void showProduct()
{
     allPro = cashier.db.doworkOtherData(var," where status!='TEMP' and status!='REJECTED' ");
    
    int c=allPro.size();
    
    Product [] control= new Product [c];
    for(int i=0;i<c;i++)
    {
        //Product p= cashier.allProduct.get(i);
        control[i]=allPro.get(i);
    }
    allProductList.setListData(control);
}

/**************************************************************************
affiche du stock
**************************************************************************/

public void westPane(Container content) {
    productPane = new JPanel();        
    productPane.setPreferredSize(new Dimension(600, 700));

    JLabel out = new JLabel(stock + "*******" + cashier.db.l(cashier.db.lang, "V_DESIGNATION") + "*******&&******" + cashier.db.l(cashier.db.lang, "V_PRIXVENTE"));
    out.setFont(police);
    
    JPanel paneC = new JPanel(); 

    search = new JTextField();        
    searchL = new JLabel(cashier.db.l(cashier.db.lang, "V_SEARCH"));
    search.setPreferredSize(new Dimension(250, 30));
    search.setFont(police);
    searchL.setFont(police);  
    
    inStock=new JButton("PRODUCT IN STOCK");    
    allProd=new JButton("ALL PRODUCT");
    
    sommeil=new JButton("SOMMEIL");    
    okprod=new JButton("CURRENT");
    
    
    paneC.add(searchL);
    paneC.add(search);  

    scrollPaneProductList = new JScrollPane(allProductList);
    scrollPaneProductList.setPreferredSize(new Dimension(550, 600));
    allProductList.setFont(police);

    allProductList.setBackground(Color.pink);
    
    JPanel paneUnder = new JPanel(); 
    
    
    paneUnder.add(allProd);
    paneUnder.add(sommeil);    
    paneUnder.add(okprod);
    paneUnder.add(inStock);

    productPane.add(out, BorderLayout.NORTH);
    productPane.add(paneC, BorderLayout.NORTH);
    productPane.add(scrollPaneProductList, BorderLayout.SOUTH); 
    productPane.add(paneUnder, BorderLayout.SOUTH); 
    
    

    content.add(productPane, BorderLayout.WEST);
    //content.add(paneC, BorderLayout.SOUTH); 
}
/***********************************************************************
fenetre des achats
***********************************************************************/
public void eastPane(  Container content )
{
    JPanel panel = new JPanel();
    panel.setLayout(new GridLayout(4,1));
    qtyButton=new JButton(cashier.db.l(cashier.db.lang, "V_CODEBAR"));
    qtyButton.setBackground(Color.yellow);
    //qtyButton.setFont(new Font("Helvetica", Font.PLAIN, 20));
    qtyButton.setFont(police);
    priceButton= new JButton(cashier.db.l(cashier.db.lang, "V_PRICE"));
    priceButton.setBackground(Color.red);
    //priceButton.setFont(new Font("Helvetica", Font.PLAIN, 20));
    priceButton.setFont(police);
    
    autoButton=new JButton("AUTORISATION");
    autoButton.setBackground(Color.ORANGE);
    //qtyButton.setFont(new Font("Helvetica", Font.PLAIN, 20));
    autoButton.setFont(police);
    
    addButton= new JButton("IBISHYA"); 
    addButton.setFont(new Font("Helvetica", Font.PLAIN, 20));
    
    saveButton=new JButton("Save");
    saveButton.setBackground(Color.orange);
    saveButton.setFont(new Font("Helvetica", Font.PLAIN, 20));
    
    vedetteButton=new JButton(cashier.db.l(cashier.db.lang, "V_VEDETTE"));
    vedetteButton.setBackground(Color.green);
    vedetteButton.setFont(police);
    //vedetteButton.setFont(new Font("Helvetica", Font.PLAIN, 20));
    
   
    panel.setPreferredSize(new Dimension (180,300));
    
    
    panel.add( autoButton);
    panel.add( qtyButton);
    panel.add( priceButton);
   // panel.add( saveButton);
    panel.add( vedetteButton);

    content.add(panel, BorderLayout.EAST);


}
/**********************************************************************************************
Ajout Mannuel  verifier les entrees
**********************************************************************************************/
public void ved()
{
    vedetteButton.addActionListener(new ActionListener() {
    public void actionPerformed(ActionEvent ae)
    {

        if(ae.getSource()==vedetteButton && allProductList.getSelectedValue()!=null   )
        {
        Product p = (Product )allProductList.getSelectedValue();
        int c = p.productCode;
       
    
    int ino = getIn(cashier.db.l(cashier.db.lang, "V_CATEGORIE"),cashier.db.l(cashier.db.lang, "V_INT"),0);
    if(ino!=-1 && 0<ino && ino<10)
    {
    int out = getIn(cashier.db.l(cashier.db.lang, "V_POSITION"),cashier.db.l(cashier.db.lang, "V_INT"),0);
    String pathImage="";//JOptionPane.showInputDialog("IMAGE PATH");
    
    if(out!=-1 && 0<=out && out<12)
    {
        Vedette v;
       
        for(int i=0;i<cashier.vedette.size();i++)
        {
            v=cashier.vedette.get(i);
        if(v.cat==ino && v.num==out)
        {
            int qte = getIn(cashier.db.l(cashier.db.lang, "V_MULTIPLE"),cashier.db.l(cashier.db.lang, "V_INT"),0);
        
       JFileChooser chooser = new JFileChooser();
chooser.setCurrentDirectory(new java.io.File("."));
chooser.setDialogTitle("choosertitle");
FileNameExtensionFilter filter = new FileNameExtensionFilter(
        "JPG & GIF Images", "jpg", "gif");
    chooser.setFileFilter(filter);
//chooser.setAcceptAllFileFilterUsed(false);

if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
  System.out.println("getCurrentDirectory(): " + chooser.getCurrentDirectory());
  System.out.println("getSelectedFile() : " + chooser.getSelectedFile());
  
  pathImage="file:///"+chooser.getSelectedFile();
  
} else {
  System.out.println("No Selection ");
}     
        v.ID_PRODUCT=c;
        v.qte=qte;
        v.path=pathImage;
        cashier.db.fixVedette(v);
        Product inter=cashier.db.getProduct(c);
        inter.qty=qte;
        cashier.allProductVedette.add(inter);
        
        
        }
        }
       
    }
    }}}});
}
/**********************************************************************************************
Ajout Mannuel  verifier les entrees
**********************************************************************************************/
public void priceProduct()
{
    priceButton.addActionListener(new ActionListener() {
    public void actionPerformed(ActionEvent ae)
    {

        if(ae.getSource()==priceButton && allProductList.getSelectedValue()!=null   )
        { 
      Product p = (Product )allProductList.getSelectedValue();
        int c = p.productCode; 
     try
     {
         
     String money = JOptionPane.showInputDialog(null,cashier.db.l(cashier.db.lang, "V_PRIXVENTE"));
     
     
     Double money3=new Double(money);
     double money4= money3.doubleValue();
     if(money4>=0)
     {
     double tvaa=0.0;
     if(stock.equals("CASHNORM"))
     { 
    cashier.db.changePrice(c,money4,tvaa,stock,nom,p.productName+" "+stock);
     }

     else if(stock.contains("RAMA"))
     {
       
     String tva = JOptionPane.showInputDialog(null, "CODE RAMA ");
     
     String observation = JOptionPane.showInputDialog(null, "OBSERVATION RAMA ",p.observation);
    
    cashier.db.changePriceRama(c,money4,tva,stock,nom,p.productName+" "+stock,observation);
     }
   else
             cashier.db.changePrice(c,money4,tvaa,var,nom,p.productName+" "+stock);
        
         JOptionPane.showMessageDialog(null,stock," INPUT ",JOptionPane.PLAIN_MESSAGE);

     } }
    
    catch(Throwable ex)
    {
    JOptionPane.showMessageDialog(null,cashier.db. l(cashier.db.lang, "V_IKIBAZO")+" "+cashier.db. l(cashier.db.lang, "V_DANS")+" "+cashier.db. l(cashier.db.lang, "V_INYANDIKO")," Itariki ",JOptionPane.PLAIN_MESSAGE);
    }  
        }}});
}  


/***************************************************************************************************
Ajoute un produit dans le panier
**************************************************************************************************/
public void addProduct()
{  addButton.addActionListener(
new ActionListener()
{  public void actionPerformed(ActionEvent ae)
{  if(ae.getSource()==addButton  )
{

    String n = JOptionPane.showInputDialog(null, " IZINA: s");
    if(n!=null && n.length()!=0)
    {
    int cat = getIn( "  cat"," INT",0);
    if(cat!=-1)
    {
    String t = JOptionPane.showInputDialog(null, " type: s");
    if(t!=null && t.length()!=0)
    {
    int ino = getIn( " Cyaranguwe "," INT",0);
    if(ino!=-1)
    {
    int out = getIn( " Kizagurishwa "," INT",0);
    while(ino>=out && out!=-1 )
          out = getIn( " Kizagurishwa in >=out "," INT",0);
    if(out!=-1)
    {
     
        
    int quantite =getIn( " Umubare "," INT",0);
    if(quantite!=-1)
    {

    int id = cashier.allProduct.size()+2;

    
    }}}}}}
    
    }}}); 
}

public void qtyProduct()
{  qtyButton.addActionListener(
new ActionListener()
{  public void actionPerformed(ActionEvent ae)
{
if(ae.getSource()==qtyButton && allProductList.getSelectedValue()!=null )
{
  Product p = (Product )allProductList.getSelectedValue();
        int c = p.productCode;
String in = JOptionPane.showInputDialog(p.productName+" Scannez Maintenant "," Scann");

if(!in.equals(""))
{
cashier.db.upDateCodeBar(in, c);

}
}}});
}  

public void Autorisation() 
{
autoButton.addActionListener(
        new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                if (ae.getSource() == autoButton && allProductList.getSelectedValue()!= null) 
                {
                    Product p=(Product)allProductList.getSelectedValue();
                    int qte = getIn(p.productName + " "+cashier.db.l(cashier.db.lang, "V_ZINGAHE")+" ??", "  ", 0);
                    String money = JOptionPane.showInputDialog(cashier.db.l(cashier.db.lang, "V_PRIXVENTE"),""+p.currentPrice);  
                    
                    String db = "";
                    if (stock.equals("RAMA")) 
                    {
                        db = "_RAMA";
                    }
                            
                    String nAff = (JOptionPane.showInputDialog(cashier.db.l(cashier.db.lang, "V_NUMERO")+", "+cashier.db.l(cashier.db.lang, "V_NAMEORSURNAME")," ")); 
                    Client c = (Client) JOptionPane.showInputDialog(null, " "+cashier.db.l(cashier.db.lang, "V_OPTIONS"),cashier.db.l(cashier.db.lang, "V_CLIENT") , JOptionPane.QUESTION_MESSAGE, null,cashier.db.getClientV(nAff.toUpperCase(),db,stock), nom);
                    if (c == null) 
                     {
                            int n = JOptionPane.showConfirmDialog(null,  " " + cashier.db.l(cashier.db.lang, "V_URASHAKA") + "  " + cashier.db.l(cashier.db.lang, "V_KUMUSHYIRAMO") + "", nAff + " "+cashier.db.l(cashier.db.lang, "V_NTAWE"), JOptionPane.YES_NO_OPTION);
                            if (n == 0) {
                                if (stock.equals("RAMA")) { 
                                  new ClientRamaGui(cashier.db, nom,"" , nAff, "", "");
                                } else {
                                    new PersonInterface(cashier.db, nom, nAff, "", "", "", "", "",  0,"");
                                }
                            }
                     }
                     else
                     {
                         if(c.DATE_EXP < today)
                         {
                             int n = JOptionPane.showConfirmDialog(null, ""+cashier.db.l(cashier.db.lang, "V_URASHAKA")+"  "+cashier.db.l(cashier.db.lang, "V_KONGERA")+" ", nAff +"  "+ cashier.db.l(cashier.db.lang, "V_EXPIRER"), JOptionPane.WARNING_MESSAGE);

                        if (n == 0) {
                            if (stock.equals("RAMA")) {
                                new ClientRamaGui(cashier.db, c);
                            } else {
                                new PersonInterface(cashier.db, c);
                            }
                        }

                        c = null;
                             
                         }
                         else
                         {
                             int n = JOptionPane.showConfirmDialog(null,
                                    "  NOM_CLIENT     =" + c.NOM_CLIENT
                                    + "\n PRENOM CLIENT  =" + c.PRENOM_CLIENT
                                    + "\n NUM_AFFILIATION=" + c.NUM_AFFILIATION
                                    +"\n BENEFICIAIRE ="+c.BENEFICIAIRE
                                    +"\n LIEN AVEC ADHERENT="+c.LIEN
                                    + "\n PERCENTAGE     =" + c.PERCENTAGE
                                    + "\n ASSURANCE        =" + c.ASSURANCE
                                    + "\n DPT        =" + c.EMPLOYEUR,
                                    " "+cashier.db.l(cashier.db.lang, "V_FICHE") + nAff, JOptionPane.YES_NO_OPTION);
                            if (n == 0) 
                            {
                                String ordonnance = (JOptionPane.showInputDialog(null,cashier.db.l(cashier.db.lang, "V_NUMERO")+" "+cashier.db.l(cashier.db.lang, "V_ORDONNANCE")+ "  "+ c.ASSURANCE,""));
                                String tosend=p.code+";"+qte+";"+money+";"+c.NUM_AFFILIATION+";"+ordonnance+";"+nom+";"+stock+"#"; 
                                System.out.println(tosend);
                                cashier.db.insertTracking(nom, "GUSABA AUTORISATION "+stock+" FROM SETUP/AUTORISATION ",1, "ISHYIGA");
    
                                CommCloud product_Cloud=new CommCloud("http://www.ishyiga"+cashier.db.getString("extension")+"/test_cloud/upload_list_autorisation.php?soc="+cashier.db.dbName+"&user="+nom,"list="+tosend);   
                                String response=product_Cloud.sendlot;
                                
                                if(response.contains("UPLOAD SUCCESSF"))
                                {
                                    cashier.db.insertTracking(nom, "CLOUD RESPONSE "+response,0, "CLOUD");   
                                    Autorisation auto=new Autorisation(p.code,qte, Double.parseDouble(money), c.NUM_AFFILIATION, ordonnance, nom, stock,"START","NOT_YET");
                                    cashier.db.insertInAutorisation(auto);
                                    JOptionPane.showMessageDialog(null, cashier.db.l(cashier.db.lang,"V_AUTORISATION_ENVOIYER")); 
                                }
                                
                            } 
                         }
                          
                     }
                }
            }
        });
}

public void AllPRoduct() 
{
allProd.addActionListener(
        new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                if (ae.getSource() == allProd) 
                {
                    showProduct();
                }
            }
        });
}
public void InStockPRoduct() 
{
inStock.addActionListener(
        new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                if (ae.getSource() == inStock) 
                { 
                    allPro = cashier.db.InStockProduct(var);
                    int c=allPro.size();
                    
                    Product [] control= new Product [c];
                    for(int i=0;i<c;i++)
                    {
                        control[i]=allPro.get(i);
                    }
                    System.out.println("settinga:");
                    allProductList.setListData(control);
                }
            }
        });
}

public void InSommeilPRoduct() 
{
sommeil.addActionListener(
        new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                if (ae.getSource() == sommeil) 
                { 
                    allPro = cashier.db.doworkOtherData(var," where status='SOMMEIL'");
                    int c=allPro.size();
                    
                    Product [] control= new Product [c];
                    for(int i=0;i<c;i++)
                    {
                        control[i]=allPro.get(i);
                    }
                    System.out.println("settinga:");
                    allProductList.setListData(control);
                }
            }
        });
}

public void InCurrentPRoduct() 
{
okprod.addActionListener(
        new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                if (ae.getSource() == okprod) 
                { 
                    allPro = cashier.db.doworkOtherData(var," where status='OK' AND "+var+" >0 ");
                    int c=allPro.size();
                    
                    Product [] control= new Product [c];
                    for(int i=0;i<c;i++)
                    {
                        control[i]=allPro.get(i);
                    }
                    System.out.println("settinga:");
                    allProductList.setListData(control);
                }
            }
        });
}


public  int getIn (String s,String s1,int i)
{
    int entier=-1;

    String in = JOptionPane.showInputDialog(null, s+s1);
   boolean done =true;
    
    while(in!=null && i<3&& done==true)
    try
    {   if(in.equals(""))
        {
        i++;
       in = JOptionPane.showInputDialog(null, s+" Enter a number ");
        }
        else
        {
        Integer in_int=new Integer(in);
        entier = in_int.intValue();
        done=false;
        }
        
     }
    catch (NumberFormatException e)
    {
        i++;
        in = JOptionPane.showInputDialog(null, s+" ="+in+ cashier.db.l(cashier.db.lang,"V_NOT_A _NUMBER") +";"+ cashier.db.l(cashier.db.lang,"V_NUMBER"));
        done=true;
    }
     if(i==3)
        {
        JOptionPane.showMessageDialog(this,"Too many try"," Sorry we have to Exit ",JOptionPane.PLAIN_MESSAGE);
        
        }

    return entier;
}

public void searchZOOMIT() { 
        search.addKeyListener(
                new KeyListener()     {
                    public void actionPerformed(ActionEvent ae) {
                    }
                    public void keyTyped(KeyEvent e) { 
                        
                        String cc=""+e.getKeyChar();
                         String find=(search.getText()+ cc).toUpperCase();
                        if(cc.hashCode()==8 && find.length()>0 )
                            find=(find.substring(0, find.length()-1)); 
                                  superSearchZOOM(find);
                            } 
                    public void keyPressed(KeyEvent e) { 
                    } 
                    public void keyReleased(KeyEvent e) {
                    }
                }); 
    } 

void superSearchZOOM ( String find) {

LinkedList<Product> res=  new LinkedList();
find=find.replaceAll(" ","");
for (int i = 0; i < allPro.size(); i++) {
Product p = allPro.get(i);
if (p.productName.toUpperCase().contains(find)) { 
res.add(p);
}}
int c = res.size();
Product[] control = new Product[c];
for (int i = 0; i < c; i++) {
Product p = res.get(i);
control[i] = p;
}
allProductList.setListData(control);

}




    @Override
protected void processWindowEvent(WindowEvent evt)
{
   if(WindowEvent.WINDOW_CLOSING == evt.getID())
   {
	 
         this.dispose();
			//default icon, custom title
//JOptionPane.showMessageDialog(this,"RIRAFUNGIRA RIMWE N'ISHYIGA"," IKIBAZO",JOptionPane.PLAIN_MESSAGE);
        			//this.setDefaultCloseOperation(HIDE_ON_CLOSE);

			
	}

}}