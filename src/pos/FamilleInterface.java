
package pos;
 
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.util.LinkedList;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;


public class FamilleInterface extends JFrame
{
      JTextField CODE_FAMILLE;
      JTextField DESIGNATION_FAMILLE;
      JTextField TARIF1;
      JTextField TARIF2;
      JTextField TARIF3;
      JTextField TARIF4; 
      JTextField B;
      JTextField FONT;
      JTextField OBSERVE;
      
      
     JList  FAMILLE_CATEGORIE,SMART;         
  String code_fam,des_fam,tarif1,tarif2,tarif3,tarif4;
  int i,j=0;
JScrollPane jScrollPanel;

  String [] title; 
  String [][] data=new String[6][6];
  String []donne=new String [10];
  JTable jTable;


Container content;
static JList famille=new JList();
JLabel CODE_FAMILLEL,DESIGNATION_FAMILLEL,TARIF1L,TARIF2L,TARIF3L,TARIF4L,BL,FONTL,OBSERVEL,SMARTL;
JPanel mainFamille = new JPanel();

 JButton deleteJournal;
 JButton saveButton;
 JButton viewFamille;
 JButton updateFamille;
 JButton deleteFamille;

Font police;
Color color;


/******************************************************************************************
     * VARIABLE DE JOURNAL WESTPANE
     ****************************************************************************************/
Db db;
LinkedList <Tarif> familleList;
JButton familleButton; 
public FamilleInterface(Db db)
{  
this.db=db;
this.title = new String [] {"CODE","DESIGNATION","VAR_TARIF","COEF_TARIF","R_TARIF","G_TARIF","B_TARIF","FONT","OBSERVE","SMART"};
this.jTable=new JTable();
 
content = getContentPane();
//Set the title to the Comptabilitée frame
this.setTitle("SAISIR FAMILLE");
//End the process when clicking on Close
enableEvents(WindowEvent.WINDOW_CLOSING);
//Make the Comptabilitée frame not resizable
this.setResizable(false);
//Define the size of the Comptabilité;here, 500 pixels of width and 650 pixels of height
this.setSize(600,850);
setVisible(true);
table();
makeFrameFamille();
insertFamille();
updateFamille();
famJTable();

}
public JPanel makeFrameFamille()
{
CODE_FAMILLEL = new JLabel("CODE :");
police = new Font("Rockwell", Font.BOLD, 14);
CODE_FAMILLEL.setFont(police);
CODE_FAMILLEL.setBackground(Color.PINK);
CODE_FAMILLE = new JTextField("");
police = new Font("Consolas", Font.BOLD, 16);
CODE_FAMILLE.setFont(police);
CODE_FAMILLE.setForeground(Color.BLUE);
color= new Color(175,215,255);//(r,g,b)
CODE_FAMILLE.setBackground(color); 

////    JScrollPane scFamille = new JScrollPane(famille);
////    scFamille.setPreferredSize(new Dimension(200,50));
//    police=new Font("Consolas",Font.BOLD,14);

//    famille.setFont(police);
//    color= new Color(255,145,200);//(r,g,b)
//    famille.setBackground(color);
//*****************************************************************************************************************************************************
DESIGNATION_FAMILLEL = new JLabel("DESIGNATION :");
police = new Font("Rockwell", Font.BOLD, 14);
DESIGNATION_FAMILLEL.setFont(police);
DESIGNATION_FAMILLEL.setBackground(Color.PINK);
DESIGNATION_FAMILLE = new JTextField("");
police = new Font("Consolas", Font.BOLD, 16);
DESIGNATION_FAMILLE.setFont(police);
DESIGNATION_FAMILLE.setForeground(Color.BLUE);
color= new Color(175,215,255);//(r,g,b)
DESIGNATION_FAMILLE.setBackground(color);

    TARIF1L = new JLabel("VAR_TARIF :");
    police = new Font("Rockwell", Font.BOLD, 14);
   TARIF1L.setFont(police);
   TARIF1L.setBackground(Color.PINK);
   TARIF1 = new JTextField("");
    police = new Font("Consolas", Font.BOLD, 16);
    TARIF1.setFont(police);
    TARIF1.setForeground(Color.BLUE);
    color= new Color(175,215,255);//(r,g,b)
    TARIF1.setBackground(color);

    TARIF2L = new JLabel("COEF_TARIF :");
    police = new Font("Rockwell", Font.BOLD, 14);
    TARIF2L.setFont(police);
     TARIF2 = new JTextField("");
    police = new Font("Consolas", Font.BOLD, 16);
    TARIF2.setFont(police);
   TARIF2.setForeground(Color.BLUE);
    color= new Color(175,215,255);//(r,g,b)
    TARIF2.setBackground(color);
    TARIF3L = new JLabel("R_TARIF :");
    police = new Font("Rockwell", Font.BOLD, 14);
    TARIF3L.setFont(police);

    TARIF3 = new JTextField("");
    police = new Font("Consolas", Font.BOLD, 16);
     TARIF3.setFont(police);
    TARIF3.setForeground(Color.BLUE);
    color= new Color(175,215,255);//(r,g,b)
    TARIF3.setBackground(color);

    TARIF4L = new JLabel("G_TARIF :");
    police = new Font("Rockwell", Font.BOLD, 14);
   TARIF4L.setFont(police);

  TARIF4 = new JTextField("");
    police = new Font("Consolas", Font.BOLD, 16);
     TARIF4.setFont(police);
    TARIF4.setForeground(Color.BLUE);
    color= new Color(175,215,255);//(r,g,b)
   TARIF4.setBackground(color);
   
   BL = new JLabel("B_TARIF :");
    police = new Font("Rockwell", Font.BOLD, 14);
   BL.setFont(police);
   
   B = new JTextField("");
    police = new Font("Consolas", Font.BOLD, 16);
     B.setFont(police);
    B.setForeground(Color.BLUE);
    color= new Color(175,215,255);//(r,g,b)
   B.setBackground(color);
   
   FONTL = new JLabel("FONT TARIF :");
    police = new Font("Rockwell", Font.BOLD, 14);
   FONTL.setFont(police);
   
   FONT = new JTextField("");
    police = new Font("Consolas", Font.BOLD, 16);
     FONT.setFont(police);
    FONT.setForeground(Color.BLUE);
    color= new Color(175,215,255);//(r,g,b)
   FONT.setBackground(color);
   
   OBSERVEL = new JLabel("ALLOW OBSERVATION :");
    police = new Font("Rockwell", Font.BOLD, 14);
   OBSERVEL.setFont(police);
   
   SMARTL = new JLabel("SMART ALLOWED :");
    police = new Font("Rockwell", Font.BOLD, 14);
   SMARTL.setFont(police);
   
   OBSERVE = new JTextField("");
    police = new Font("Consolas", Font.BOLD, 16);
     OBSERVE.setFont(police);
    OBSERVE.setForeground(Color.BLUE);
    color= new Color(175,215,255);//(r,g,b)
   OBSERVE.setBackground(color); 
   
FAMILLE_CATEGORIE=new JList();
FAMILLE_CATEGORIE.setListData(new String []{"NO","YES"});
FAMILLE_CATEGORIE.setForeground(Color.BLUE);

SMART=new JList();
SMART.setListData(new String []{"NO","YES"});
SMART.setForeground(Color.BLUE);


saveButton = new JButton("SAVE");
police = new Font("Rockwell", Font.BOLD, 14);
saveButton.setFont(police);
color= new Color(55,155,255);//(r,g,b)
saveButton.setBackground(color);
viewFamille = new JButton("VIEW");
police = new Font("Rockwell", Font.BOLD, 14);
viewFamille.setFont(police);
color= new Color(255,145,200);//(r,g,b)
viewFamille.setBackground(color);

updateFamille = new JButton("UPDATE");
police = new Font("Rockwell", Font.BOLD, 14);
updateFamille.setBackground(Color.LIGHT_GRAY);
updateFamille.setFont(police);

deleteFamille = new JButton("DELETE");
police = new Font("Rockwell", Font.BOLD, 14);
deleteFamille.setFont(police);

    JPanel panelTable=new JPanel();
    panelTable.setLayout(new GridLayout(1,1));//
    panelTable.setPreferredSize(new Dimension(550,150));
    panelTable.add(new JScrollPane(jTable));

    JPanel panelFamille=new JPanel();
    panelFamille.setLayout(new GridLayout(10,2,5,20));//
    panelFamille.setPreferredSize(new Dimension(450,450));


    panelFamille.add(CODE_FAMILLEL);
    panelFamille.add(CODE_FAMILLE);

    panelFamille.add(DESIGNATION_FAMILLEL);
    panelFamille.add(DESIGNATION_FAMILLE);
    panelFamille.add(TARIF1L);
    panelFamille.add(TARIF1);
    panelFamille.add(TARIF2L);
    panelFamille.add(TARIF2);
    panelFamille.add(TARIF3L);
    panelFamille.add(TARIF3);
    panelFamille.add(TARIF4L);
    panelFamille.add(TARIF4);
    
    panelFamille.add(BL);
    panelFamille.add(B);
    panelFamille.add(FONTL);
    panelFamille.add(FONT);
    panelFamille.add(new JLabel(" ALLOW OBSERVATION "));
    panelFamille.add(FAMILLE_CATEGORIE); 
    panelFamille.add(SMARTL);
    panelFamille.add(SMART);
    
    

    JPanel panelFamille2 = new JPanel();
    panelFamille2.setLayout(new GridLayout(1,4));
    panelFamille2.setPreferredSize(new Dimension(450, 40));

    panelFamille2.add(saveButton);
    panelFamille2.add(viewFamille);
    panelFamille2.add(updateFamille);
    panelFamille2.add(deleteFamille);

    mainFamille.add(panelFamille);
    mainFamille.add(panelTable);
    mainFamille.add(panelFamille2);

    content.add(mainFamille);
    return mainFamille;
}

public void famJTable()
{

jTable.addMouseListener(new MouseListener() {
public void actionPerformed(ActionEvent ae)
{
}
public void mouseExited(MouseEvent ae)
{ 
}
public void mouseClicked(MouseEvent e)
{
String p ="";
 if( jTable.getSelectedRow()!=-1 )
{ 
Tarif family=familleList.get(jTable.getSelectedRow()); 
if(family!=null)
{CODE_FAMILLE.setText(family.ID_TARIF);
 DESIGNATION_FAMILLE.setText(family.DES_TARIF);
 TARIF1.setText(""+family.VAR_TARIF);
 TARIF2.setText(""+family.COEF_TARIF);
 TARIF3.setText(""+family.r);
 TARIF4.setText(""+family.g); 
 B.setText(""+family.b);
 FONT.setText(""+family.FONT_TARIF);
 FAMILLE_CATEGORIE.setSelectedValue(family.OBSERVE, true);
 SMART.setSelectedValue(family.OBSERVE, true);
 CODE_FAMILLE.setEditable(false);
}
    } 
};  
    public void mousePressed(MouseEvent e)
             {
     }; 
    public void mouseReleased(MouseEvent e)
             { }; 
    public void mouseEntered(MouseEvent e)
{  }
    }
    );

}


public void table ()
{
     familleList=db.getAllTarif(); 
      donne[0]=code_fam;
      donne[1]=des_fam;
      donne[2]=tarif1;
      donne[3]=tarif2;
      donne[4]=tarif3;
      donne[5]=tarif4;
      donne[6]="B_TARIFF";
      donne[7]="FONT";
      donne[8]="OBSERVATION";
      donne[9]="SMART";
      

 data=new String[familleList.size()][10];
for(i=0;i<familleList.size();i++)
{
    
Tarif family=familleList.get(i);
data[i][0]=family.ID_TARIF;
data[i][1]=family.DES_TARIF;
data[i][2]=""+family.VAR_TARIF;
data[i][3]=""+family.COEF_TARIF;
data[i][4]=""+family.r;
data[i][5]=""+family.g; 
data[i][6]=""+family.b;
data[i][7]=""+family.FONT_TARIF;
data[i][8]=""+family.OBSERVE;
data[i][9]=""+family.SMART;

}  


jTable= new javax.swing.JTable(); 
            jTable.setModel(new javax.swing.table.DefaultTableModel(data, 
           title
                    )); 
            jTable.setAutoResizeMode(jTable.AUTO_RESIZE_ALL_COLUMNS);
            jTable.getColumnModel().getColumn(1).setPreferredWidth(200);//.setWidth(200);
           // jTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            jTable.doLayout();
            jTable.validate();  
}
 
public void insertFamille()
{
saveButton.addActionListener(new ActionListener() {
public void actionPerformed(ActionEvent ae)
{
if(ae.getSource()==saveButton ) 
{
    db.insertTarif(new Tarif(CODE_FAMILLE.getText(),(TARIF1.getText()),DESIGNATION_FAMILLE.getText(),
        Double.parseDouble(( TARIF2.getText())),( FONT.getText()),
Integer.parseInt(TARIF3.getText()),Integer.parseInt(TARIF4.getText()),Integer.parseInt(B.getText())
        ,(String)FAMILLE_CATEGORIE.getSelectedValue(),(String)SMART.getSelectedValue())); 
    
    CODE_FAMILLE.setEditable(true);

    table();

}
}
}
);
}

public void updateFamille()
{
updateFamille .addActionListener(new ActionListener() {
public void actionPerformed(ActionEvent ae)
{
if(ae.getSource()==updateFamille  )

{
db.upDateTarif(new Tarif(CODE_FAMILLE.getText(),(TARIF1.getText()),DESIGNATION_FAMILLE.getText(),
        Double.parseDouble(( TARIF2.getText())),( FONT.getText()),
Integer.parseInt(TARIF3.getText()),Integer.parseInt(TARIF4.getText()),Integer.parseInt(B.getText())
        ,(String)FAMILLE_CATEGORIE.getSelectedValue(),(String)SMART.getSelectedValue())); 

table();
}
}
}
);
}

public static void main(String[] args) { 
    

}
}