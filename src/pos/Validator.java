/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
*/
package pos;

import rra.RRA_PRINT2;

/**
*
* @author Kimenyi
*/
public class Validator {

Invoice inv;
Db db;
boolean valid=false;
Validator(Invoice inv, Db db) 
{
    this.inv = inv;
    this.db = db;
    valid=verifyList();
    if(valid) {
        valid=verifyTaxB();
    }
}

private boolean verifyList() 
{
    double totalList = 0;
    String err = "";
    for (int i = 0; i < inv.getProductListDB().size(); i++) 
    {
        Product p = inv.getProductListDB().get(i);
        totalList += (p.qty * p.currentPrice);
        err += ";" + p.code + "-" + p.qty + "-" + p.currentPrice;
    }
    System.out.println(inv.tot+":VALIDATOR:"+totalList);
    System.out.println(inv.tot+":errr:"+err);
    if (inv.tot != totalList) 
    {
        db.insertError("verifyList", inv.tot + ";" + inv.id_invoice + ";" + err);
        return false;
    }
    else {
        return true;
    }
}

private boolean verifyTaxB() 
{
    double totalTaxB = 0;
    String err = "";
    for (int i = 0; i < inv.getProductListDB().size(); i++) 
    {
        Product p2 = inv.getProductListDB().get(i);
        double taxable = ((p2.salePrice()) / (1 + p2.tva));
        totalTaxB = totalTaxB + (taxable * p2.tva);
        err += ";" + p2.code + "-" + p2.qty + "-" + p2.currentPrice + "-" + p2.tva;        
    }
    System.out.println(inv.tva+":VALIDATOR:"+totalTaxB+":::"+(inv.tva-totalTaxB));
    
    if (inv.tva != totalTaxB ) 
    {
        if(inv.tva>totalTaxB && (inv.tva-totalTaxB)<0.0001)
        {
            return true;
        }
        else if(inv.tva<totalTaxB && (totalTaxB-inv.tva)<0.0001)
        {
            return true;
        }
        db.insertError("verifyTaxBB", inv.tva + ";" + inv.id_invoice + ";" + err);
        return false;
    }
    return true;
}

void verifyTaxBarrondi() 
{
        double totalTaxB = 0;
    String err = "";
    for (int i = 0; i < inv.getProductListDB().size(); i++) 
    {
        Product p2 = inv.getProductListDB().get(i);
        double taxable = ((p2.salePrice()) / (1 + p2.tva));
        totalTaxB = totalTaxB + (taxable * p2.tva);
        err += ";" + p2.code + "-" + p2.qty + "-" + p2.currentPrice + "-" + p2.tva;
    }
   
    String tb = RRA_PRINT2.setVirgule(totalTaxB, 1 , ",");
    if (tb != null || tb.equals(""+totalTaxB)) 
    {
        db.insertError("verifyTaxBarrondi", inv.totB + ";" + tb + ";" + totalTaxB);
    }
}
}
