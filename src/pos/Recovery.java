/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pos;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Kimenyi
 */
public class Recovery {

    ResultSet outResult;
    Connection conn = null;
    Statement s;
    public Db db;
    PreparedStatement psInsert;

    public Recovery(String server, String dbName) {
        read(server, dbName);
String ch = (String) JOptionPane.showInputDialog(null, "Fait votre choix", " ",
                                    JOptionPane.QUESTION_MESSAGE, null, new String[]{"DownLoad", "Upload", }, "");

if(ch.equals("DownLoad"))
        {  downLoadEmploye();
        downLoadFrss();
        downLoadClient();
        downLoadClientRama();
        downLoadVedette();
        downLoadVariables(); 
       downLoadProduct();
        downLoadLot();
        }
else if(ch.equals("Upload"))
upLoad();

    }

    public void read(String server, String dbName) {
        try {
            conn = DriverManager.getConnection("jdbc:derby://" + server + ":1527/" + dbName + ";create=true");
            s = conn.createStatement();
        } catch (Throwable e) {
            insertError(e + " echec", "Read Recovery");
        }
    }

    public void downLoadEmploye() {
        String file = "RECOVERY/EMPLOYE.txt";
        File f = new File(file);
        try {
            PrintWriter sorti = new PrintWriter(new FileWriter(f));
            outResult = s.executeQuery("select * from EMPLOYE ");
            String recoveryHeaderLine = "INSERT INTO APP.EMPLOYE (ID_EMPLOYE, NOM_EMPLOYE, PRENOM_EMPLOYE, BP_EMPLOYE, CATRE_INDETITE, LEVEL_EMPLOYE, TITRE_EMPLOYE, PWD_EMPLOYE, TEL_EMPLOYE) VALUES";
            sorti.println(recoveryHeaderLine);

            while (outResult.next()) {
                String recoveryLine = "(" + outResult.getInt(1) + ",'" + outResult.getString(2) + "','" + outResult.getString(3) + "','" + outResult.getString(4) + "'," + outResult.getInt(5) + "," + outResult.getInt(6) + ",'" + outResult.getString(7) + "','" + outResult.getString(8) + "'," + outResult.getInt(9) + ")";
                sorti.println(recoveryLine);
            }
            //  Close the resultSet
            outResult.close();
            sorti.close();
            //  Beginning of the primary catch block: uses errorPrint method
        } catch (Throwable e) {
            insertError(e + "", " downLoadEmploye");
        }
    }

    public void downLoadFrss() {
        String file = "RECOVERY/FRSS.txt";
        File f = new File(file);

        try {
            PrintWriter sorti = new PrintWriter(new FileWriter(f));

            outResult = s.executeQuery("select * from FRSS ");

            String recoveryHeaderLine = "INSERT INTO APP.FRSS (NUM_AFFILIATION, NOM_CLIENT, PRENOM_CLIENT, PERCENTAGE, DATE_EXP, ASSURANCE, EMPLOYEUR, SECTEUR, VISA, CODE) VALUES";
            sorti.println(recoveryHeaderLine);

            while (outResult.next()) {

                String recoveryLine = "('" + outResult.getString(1) + "','" + outResult.getString(2) + "','" + outResult.getString(3) + "'," + outResult.getInt(4) + "," + outResult.getInt(5) + ",'" + outResult.getString(6) + "','" + outResult.getString(7) + "','" + outResult.getString(8) + "','" + outResult.getString(9) + "','" + outResult.getString(10) + "')";
                sorti.println(recoveryLine);
            }
            //  Close the resultSet
            outResult.close();
            sorti.close();
            //  Beginning of the primary catch block: uses errorPrint method
        } catch (Throwable e) {
            insertError(e + "", " downLoadFrss");
        }

    }

    public void downLoadClient() {
        String file = "RECOVERY/CLIENT.txt";
        File f = new File(file);

        try {
            PrintWriter sorti = new PrintWriter(new FileWriter(f));

            outResult = s.executeQuery("select * from CLIENT ");

            String recoveryHeaderLine = "INSERT INTO APP.CLIENT (NUM_AFFILIATION, NOM_CLIENT, PRENOM_CLIENT, PERCENTAGE, DATE_EXP, ASSURANCE, EMPLOYEUR, SECTEUR, VISA, CODE) VALUES";
            sorti.println(recoveryHeaderLine);

            while (outResult.next()) {
                String recoveryLine = "('" + outResult.getString(1) + "','" + outResult.getString(2) + "','" + outResult.getString(3) + "'," + outResult.getInt(4) + "," + outResult.getInt(5) + ",'" + outResult.getString(6) + "','" + outResult.getString(7) + "','" + outResult.getString(8) + "','" + outResult.getString(9) + "','" + outResult.getString(10) + "')";
                sorti.println(recoveryLine);
            }
            //  Close the resultSet
            outResult.close();
            sorti.close();
            //  Beginning of the primary catch block: uses errorPrint method
        } catch (Throwable e) {
            insertError(e + "", " downLoadClient");
        }

    }

    public void downLoadClientRama() {
        String file = "RECOVERY/CLIENT_RAMA.txt";
        File f = new File(file);

        try {
            PrintWriter sorti = new PrintWriter(new FileWriter(f));

            outResult = s.executeQuery("select * from CLIENT_RAMA ");

            String recoveryHeaderLine = "INSERT INTO APP.CLIENT_RAMA (NUM_AFFILIATION, NOM_CLIENT, PRENOM_CLIENT, PERCENTAGE, DATE_EXP, ASSURANCE, EMPLOYEUR, SECTEUR, VISA, CODE) VALUES";
            sorti.println(recoveryHeaderLine);

            while (outResult.next()) {
                String recoveryLine = "('" + outResult.getString(1) + "','" + outResult.getString(2) + "','" + outResult.getString(3) + "'," + outResult.getInt(4) + "," + outResult.getInt(5) + ",'" + outResult.getString(6) + "','" + outResult.getString(7) + "','" + outResult.getString(8) + "','" + outResult.getString(9) + "','" + outResult.getString(10) + "')";
                sorti.println(recoveryLine);
            }
            //  Close the resultSet
            outResult.close();
            sorti.close();
            //  Beginning of the primary catch block: uses errorPrint method
        } catch (Throwable e) {
            insertError(e + "", " downLoadClientRama");
        } 
    }

    public void downLoadLot() {
        String file = "RECOVERY/STOCK.txt";
        File f = new File(file);

        try {
            PrintWriter sorti = new PrintWriter(new FileWriter(f));

            outResult = s.executeQuery("select * from LOT ");

            String recoveryHeaderLine = "INSERT INTO APP.LOT (ID_PRODUCT, ID_LOT, ID_LOTS, DATE_EXP, QTY_START, QTY_LIVE, BON_LIVRAISON) VALUES";
            sorti.println(recoveryHeaderLine);
            while (outResult.next()) {
                String recoveryLine = "(" + outResult.getInt(1) + "," + outResult.getInt(2) + ",'" + outResult.getString(3) + "','" + outResult.getString(4) + "'," + outResult.getInt(5) + "," + outResult.getInt(6) + ",'BLA01')";
                sorti.println(recoveryLine);
            }
//  Close the resultSet
            outResult.close();
            sorti.close();
//  Beginning of the primary catch block: uses errorPrint method
        } catch (Throwable e) {
            insertError(e + "", " downLoadLot");
        }
    }

    public void downLoadProduct() {
        String file = "RECOVERY/PRODUCT.txt";
        File f = new File(file);

        try {
            PrintWriter sorti = new PrintWriter(new FileWriter(f));

            outResult = s.executeQuery("select * from PRODUCT order by NAME_PRODUCT");

            String recoveryHeaderLine = "INSERT INTO APP.PRODUCT (ID_PRODUCT, NAME_PRODUCT, CODE, PRIX, PRIX_SOCIETE, PRIX_RAMA, PRIX_CORAR, PRIX_MMI, PRIX_SORAS, PRIX_VAN, TVA, OBSERVATION, CODE_BAR, CODE_SOC_RAMA, CODE_SOC_MMI, CODE_SOC_SORAS, CODE_SOC_CORAR, PRIX_REVIENT, COEF, FAMILLE, MAXIMUM, CODE_FRSS, DESIGNATION_FRSS, CONDITIONNEMENT, PRIX_AAR, PRIX_FARG) VALUES ";
            sorti.println(recoveryHeaderLine);

            while (outResult.next()) {
                String recoveryLine = "(" + outResult.getInt(1) + ",'" + outResult.getString(2).replace("'", "`") + "','" + outResult.getString(3) + "'," + outResult.getDouble(4) + "," + outResult.getDouble(5) + "," + outResult.getDouble(6) + "," + outResult.getDouble(7) + "," + outResult.getDouble(8) + "," + outResult.getDouble(9) + "," + outResult.getDouble(10) + "," + outResult.getDouble(11) + ",'" + outResult.getString(12) + "','" + outResult.getString(13) + "','" + outResult.getString(14) + "','" + outResult.getString(15) + "','" + outResult.getString(16) + "','" + outResult.getString(17) + "'," + outResult.getDouble(18) + "," + outResult.getDouble(19) + ",'" + outResult.getString(20) + "'," + outResult.getInt(21) + ",'" + outResult.getString(22) + "','" + outResult.getString(23) + "'," + outResult.getInt(24) + "," + outResult.getDouble(25) + "," + outResult.getDouble(26) + ")";
                sorti.println(recoveryLine);
            }
            //  Close the resultSet
            outResult.close();
            sorti.close();
            //  Beginning of the primary catch block: uses errorPrint method
        } catch (Throwable e) {
            insertError(e + "", " downLoadProduct");
        }
    }

    public void downLoadVariables() {
        String file = "RECOVERY/VARIABLES.txt";
        File f = new File(file);

        try {
            PrintWriter sorti = new PrintWriter(new FileWriter(f));

            outResult = s.executeQuery("select * from VARIABLES ");
            //INSERT INTO APP.VARIABLES (NOM_VARIABLE, VALUE_VARIABLE, FAMILLE_VARIABLE, VALUE2_VARIABLE) VALUES 
            String recoveryHeaderLine = "INSERT INTO APP.VARIABLES (NOM_VARIABLE, VALUE_VARIABLE, FAMILLE_VARIABLE, VALUE2_VARIABLE) VALUES ";
            sorti.println(recoveryHeaderLine);

            while (outResult.next()) {
                String recoveryLine = "('" + outResult.getString(1) + "','" + outResult.getString(2) + "','" + outResult.getString(3) + "','" + outResult.getString(4) + "')";
                sorti.println(recoveryLine);
            }
            //  Close the resultSet
            outResult.close();
            sorti.close();
            //  Beginning of the primary catch block: uses errorPrint method
        } catch (Throwable e) {
            insertError(e + "", " downLoadVariables");
        }

    }

    public void downLoadVedette() {
        String file = "RECOVERY/VEDETTE.txt";
        File f = new File(file);

        try {
            PrintWriter sorti = new PrintWriter(new FileWriter(f));
            outResult = s.executeQuery("select * from VEDETTE ");

            String recoveryHeaderLine = "INSERT INTO APP.VEDETTE (ID_VEDETTE, TYPE_VEDETTE, CAT_VEDETTE, NUM_VEDETTE, ID_PRODUCT, QTE) VALUES ";
            sorti.println(recoveryHeaderLine);

            while (outResult.next()) {
                String recoveryLine = "(" + outResult.getInt(1) + ",'" + outResult.getString(2) + "'," + outResult.getInt(3) + "," + outResult.getInt(4) + "," + outResult.getInt(5) + "," + outResult.getInt(6) + ")";
                sorti.println(recoveryLine);
            }
//  Close the resultSet
            outResult.close();
            sorti.close();
//  Beginning of the primary catch block: uses errorPrint method
        } catch (Throwable e) {
            insertError(e + "", " downLoadVedette");
        }
    }

    boolean upLoadRecovery( LinkedList<String> data,String file) {
        
        LinkedList<String> errorList=new LinkedList();
        boolean succes = true; 
            String insertStatement = data.getFirst();
            for (int i = 1; i < data.size(); i++) { 
            try {
                s.execute(insertStatement + data.get(i));
                
            } catch (SQLException ex) {
                errorList.add("ligne "+i+"  "+ex);
                //insertError(ex + "", " upLoadRecovery");
                    succes = false;
            } 
            } 
        File f = new File(file+"ERROR");

        try {
            PrintWriter sorti = new PrintWriter(new FileWriter(f)); 
            for (int i = 0; i < errorList.size(); i++)
            {
            sorti.println(errorList.get(i));
            }
    sorti.close();
//  Beginning of the primary catch block: uses errorPrint method
        } catch (Throwable e) {
            insertError(e + "", " downLoadError");
        } 
        return succes;
    }
public void upLoad()
{
  try {
LinkedList<String> employe = getFichier("RECOVERY/EMPLOYE.txt");
LinkedList<String> frss = getFichier("RECOVERY/FRSS.txt");
LinkedList<String> client = getFichier("RECOVERY/CLIENT.txt");
LinkedList<String> client_rama = getFichier("RECOVERY/CLIENT_RAMA.txt");  
LinkedList<String> PRODUCT = getFichier("RECOVERY/PRODUCT.txt");
LinkedList<String> VARIABLES = getFichier("RECOVERY/VARIABLES.txt");
LinkedList<String> VEDETTE = getFichier("RECOVERY/VEDETTE.txt");
LinkedList<String> STOCK = getFichier("RECOVERY/STOCK.txt");


System.out.println("RECOVERY/PRODUCT.txt"+upLoadRecovery(PRODUCT,"RECOVERY/PRODUCT.txt"));
System.out.println("RECOVERY/VARIABLES.txt"+upLoadRecovery(VARIABLES,"RECOVERY/VARIABLES.txt"));
System.out.println("RECOVERY/VEDETTE.txt"+upLoadRecovery(VEDETTE,"RECOVERY/VEDETTE.txt"));
System.out.println(upLoadRecovery(employe,"RECOVERY/EMPLOYE.txt")+"RECOVERY/EMPLOYE.txt"+employe.size()); 
System.out.println("RECOVERY/FRSS.txt"+upLoadRecovery(frss,"RECOVERY/FRSS.txt"));
System.out.println(upLoadRecovery(client,"RECOVERY/CLIENT.txt"));
System.out.println(upLoadRecovery(client_rama,"RECOVERY/CLIENT_RAMA.txt")); 
System.out.println(upLoadRecovery(STOCK,"RECOVERY/STOCK.txt")); 
            
             } catch (Throwable e) {
            insertError(e + "", " upLoadRecovery");
        }
    
}      
    
    LinkedList<String> getFichier(String fichier) throws FileNotFoundException, IOException {
        LinkedList<String> give = new LinkedList();

        File productFile = new File(fichier);
        String ligne = null;
        BufferedReader entree = new BufferedReader(new FileReader(productFile));
        try {
            ligne = entree.readLine();
        } catch (IOException e) {
            System.out.println(e);
        }
        int i = 0;
        while (ligne != null) {
            // System.out.println(i+ligne);
            give.add(ligne);
            i++;
            try {
                ligne = entree.readLine();
            } catch (IOException e) {
                insertError(e + "", "getFichier To recover");
            }
        }
        entree.close();
        return give;
    }

    void insertError(String desi, String ERR) {
        try {
            psInsert = conn.prepareStatement("insert into APP.ERROR(DESI,USERNAME)  values (?,?)");
            if (desi.length() > 160) {
                desi = desi.substring(0, 155);
            }
            if (ERR.length() > 160) {
                ERR = ERR.substring(0, 155);
            }
            psInsert.setString(1, desi);
            psInsert.setString(2, ERR);
            psInsert.executeUpdate();
            psInsert.close();
            JOptionPane.showMessageDialog(null, " insertError  " + desi, db.l(db.lang,"V_IKIBAZO"), JOptionPane.PLAIN_MESSAGE);
        } catch (Throwable ex) {
            JOptionPane.showMessageDialog(null, " insertError " + ex, db.l(db.lang,"V_IKIBAZO"), JOptionPane.PLAIN_MESSAGE);
            ;
        }
    }

    public static void main(String[] arghs) {
        new Recovery("localhost", "DIVINE");
    }
}
