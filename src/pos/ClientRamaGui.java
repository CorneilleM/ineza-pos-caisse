

package pos; 
import java.awt.*;
import java.awt.event.*;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import javax.swing.JFrame; 
import javax.swing.text.MaskFormatter;
/**
 *
 * @author aimable
 */
public class ClientRamaGui extends JFrame{

private  JTextField
                    NUM_AFFILIATION,
                    /*CODE,*/
                    NOM_CLIENT,
                    PRENOM_CLIENT,
                    PERCENTAGE,
                    DATE_EXP,
                    EMPLOYEUR,
                    SECTEUR,SEXE,NAISSANCE,BENEFICIAIRE ;
//ASSURANCE,

private JLabel      SEXEL,NAISSANCEL,
                    NUM_AFFILIATIONL,
                    CODEL,
                    NOM_CLIENTL,
                    PRENOM_CLIENTL,
                    PERCENTAGEL,
                    DATE_EXPL,
                    ASSURANCEL,
                    EMPLOYEURL,
                    SECTEURL,
                    LIEN,BENEFICIAIREL,NUML
                    ;

private JButton send;
private Db db;
private Client c ;
Font police;


JList  lienList=new JList(new String  [] {"LUI  MEME","CONJOINT ","ENFANT ","",""});
JList sexeList=new JList(new String[]{"MALE","FEMALE"});
String soc,what ;

ClientRamaGui( Db db, String nom ,String prenom ,String code,String naissance  ,String sexe  )
{
police = new Font(db.getString("font"), Integer.parseInt(db.getString("style")),Integer.parseInt(db.getString("length")));
this.db=db;
what="";
//if(code.equals(""))
String numaffcode=""+(Integer.parseInt(db.getParama("RUN" , "compteurClient").value)+1);

makeFrame(" ",code,  nom ,  prenom ,  numaffcode,  naissance  ,  sexe,""  );
// makeFrame(stock,nAff);
send();
draw();
lienList.setSelectedIndex(0);


}
ClientRamaGui(Db db,Client c )
{
    police = new Font(db.getString("font"), Integer.parseInt(db.getString("style")),Integer.parseInt(db.getString("length")));
    what="UPDATE";
    this.db=db;
    this.c=c;
    makeFrame(c.ASSURANCE,c.NUM_AFFILIATION,  c.NOM_CLIENT ,  c.PRENOM_CLIENT ,  c.CODE,  c.AGE  ,  c.SEXE,c.BENEFICIAIRE  );
    send();
    draw(); 
}

public void makeFrame(String stock,String nA,String nom ,String prenom ,String code,String naissance,String sexe,String BENEFICIAIRES )
{
   //  this.CODE= new JTextField(); CODE.setText(code);
     this.NUML=new JLabel();NUML.setText(code);
     
    this.NUM_AFFILIATION= new JTextField();NUM_AFFILIATION.setText(nA);
    this.NOM_CLIENT= new JTextField();NOM_CLIENT.setText(nom);
    this.PRENOM_CLIENT= new JTextField();PRENOM_CLIENT.setText(prenom);
     this.BENEFICIAIRE= new JTextField();BENEFICIAIRE.setText(BENEFICIAIRES);
     this.SEXE= new JTextField();SEXE.setText(sexe);
     this.NAISSANCE= new JTextField();NAISSANCE.setText(naissance);
     
     try {
    MaskFormatter date = new MaskFormatter("##-##-####");
    NAISSANCE = new JFormattedTextField(date);
    police = new Font("Consolas", Font.BOLD, 14);
    NAISSANCE.setFont(police);
    NAISSANCE.setForeground(Color.BLUE); 
    NAISSANCE.setBackground(new Color(175,215,255));
    } 
     catch (ParseException e) {
    e.printStackTrace();
    }
     
     this.PERCENTAGE= new JTextField();
    this.DATE_EXP= new JTextField();
    DATE_EXP.setText("311217");
    this.EMPLOYEUR= new JTextField();
    this.SECTEUR = new JTextField();
    this.CODEL=new JLabel(db. l(db.lang,  "V_CODE"));
    CODEL.setFont(police);
    NUML.setFont(police);     
    this.NUM_AFFILIATIONL=new JLabel(db.l(db.lang, "V_NUMAFFILIATION")+":");
    NUM_AFFILIATION.setFont(police);
    NUM_AFFILIATIONL.setFont(police);
    this.NOM_CLIENTL=new JLabel(db.l(db.lang, "V_ADHERENT")+":");
    NOM_CLIENT.setFont(police);
    NOM_CLIENTL.setFont(police);
    this.PRENOM_CLIENTL=new JLabel(db.l(db.lang, "V_PRENOM")+":");
    PRENOM_CLIENT.setFont(police);
    PRENOM_CLIENTL.setFont(police);
    this.PERCENTAGEL=new JLabel(db.l(db.lang, "V_PERCENTAGE")+":");
    PERCENTAGE.setFont(police);
    PERCENTAGEL.setFont(police);
    this.DATE_EXPL=new JLabel(db. l(db.lang,  "V_DATEEXP")+":");
    DATE_EXP.setFont(police);
    DATE_EXPL.setFont(police);
    this.ASSURANCEL=new JLabel(db. l(db.lang,  "V_ASSURANCE")+":");
    ASSURANCEL.setFont(police);    
    this.EMPLOYEURL=new JLabel(db. l(db.lang,  "V_DPT")+":");
    EMPLOYEUR.setFont(police);
    EMPLOYEURL.setFont(police);
    this.SECTEURL=new JLabel(db. l(db.lang,  "V_LIEU")+":");
    SECTEUR.setFont(police);
    SECTEURL.setFont(police);
    this.SEXEL=new JLabel(db. l(db.lang,  "V_SEXE")+":");
    SEXEL.setFont(police);
    SEXE.setFont(police);
    sexeList.setBackground(Color.PINK);
    sexeList.setFont(police);
    this.NAISSANCEL=new JLabel(db. l(db.lang,  "V_DATENAISSANCE")+":");
    NAISSANCE.setFont(police);
    NAISSANCEL.setFont(police);
    this.LIEN=new JLabel(db. l(db.lang,  "V_LIEN")+":");
    LIEN.setFont(police);
    lienList.setFont(police);
    this.BENEFICIAIREL=new JLabel(db. l(db.lang,  "V_BENEFICIAIRE")+":");
    BENEFICIAIRE.setFont(police);
    BENEFICIAIREL.setFont(police);
    this.send=new JButton("ENVOYER");
        soc=stock;
 PERCENTAGE.setText(""+15);

}

public void draw()
{
    setTitle("Register a new Client for Ishyiga");
    Image im = Toolkit.getDefaultToolkit().getImage("logo.png");
    this.setIconImage(im); 

    this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);


    //on fixe  les composant del'interface
    Container content = getContentPane();


    if(what.equals("UPDATE"))
      UpDatePane(content);
      else
      westPane(content);

     setSize(600,600);
     setVisible(true);

}
public  int getIn (String in)
{
                      int productCode=-1;
    try
    {
        if(in.equals(""))
        {

JOptionPane.showMessageDialog(null,db. l(db.lang, "V_INVALIDPERCENTAGE"),in,JOptionPane.PLAIN_MESSAGE);
        productCode=-1;
        }
        else
        {
        Integer in_int=new Integer(in);
        productCode = in_int.intValue();
        if(productCode<0 || productCode>100)
             {

JOptionPane.showMessageDialog(null, db. l(db.lang, "V_IKIBAZO")+" "+db. l(db.lang, "V_PERCENTAGE"),""+productCode,JOptionPane.PLAIN_MESSAGE);
        productCode=-1;
        }


        }
         }
    catch (NumberFormatException e)
    {
     JOptionPane.showMessageDialog(null,in,db. l(db.lang, "V_NOTPERCENTAGE"),JOptionPane.PLAIN_MESSAGE);

    productCode=-1;
    }
    return productCode;
} 
/**************************************************************************
affiche du stock
**************************************************************************/

public void westPane(Container content )
{
JPanel Pane  = new JPanel();

Pane.setPreferredSize(new Dimension (600,700));

JPanel panel = new JPanel();
panel.setLayout(new GridLayout(14,2));
panel.setPreferredSize(new Dimension (400,500));


String code=""+(Integer.parseInt(db.getParama("RUN" , "compteurClient").value)+1);
//CODE.setText(code);
NUML.setText(code);
  panel.add(CODEL);panel.add(NUML);//panel.add(CODE);
// if(!soc.equals("CASHREMISE"))   {

 //}

panel.add(NOM_CLIENTL);panel.add(NOM_CLIENT);
panel.add(PRENOM_CLIENTL);panel.add(PRENOM_CLIENT);
JScrollPane S =new JScrollPane(lienList);
S.setPreferredSize( new Dimension (40,40) );
//lienList.setSelectedValue(c.LIEN,true);

//societeJList.setBackground(Color.pink);
//panel.add(S);

panel.add(LIEN);panel.add(S);
panel.add(BENEFICIAIREL);panel.add(BENEFICIAIRE);
panel.add(NAISSANCEL);panel.add(NAISSANCE);
panel.add(SEXEL);panel.add(sexeList);
panel.add(SECTEURL);panel.add(SECTEUR);
panel.add(ASSURANCEL);
 panel.add(new JLabel("RAMA"));
  panel.add(NUM_AFFILIATIONL);panel.add(NUM_AFFILIATION);
 panel.add(PERCENTAGEL);PERCENTAGE.setText(""+15);panel.add(PERCENTAGE);
 panel.add(DATE_EXPL);panel.add(DATE_EXP);
panel.add(EMPLOYEURL);panel.add(EMPLOYEUR);



Pane.add(panel,BorderLayout.CENTER);
Pane.add(send,BorderLayout.SOUTH);

content.add(Pane, BorderLayout.WEST);

}
public void UpDatePane(Container content )
{
JPanel Pane  = new JPanel();

Pane.setPreferredSize(new Dimension (600,700));

JPanel panel = new JPanel();
panel.setLayout(new GridLayout(14,2));
panel.setPreferredSize(new Dimension (400,500));
//CODE.setText(c.CODE);
NUML.setText(c.CODE);
NUM_AFFILIATION.setText(c.NUM_AFFILIATION);
NOM_CLIENT.setText(c.NOM_CLIENT);
PRENOM_CLIENT.setText(c.PRENOM_CLIENT);
PERCENTAGE.setText(""+15);
DATE_EXP.setText("");
 BENEFICIAIRE.setText(c.BENEFICIAIRE);
 EMPLOYEUR.setText(c.EMPLOYEUR);
SECTEUR.setText(c.SECTEUR);
NAISSANCE.setText(c.AGE);
SEXE.setText(c.SEXE);
sexeList.setSelectedValue(c.SEXE, true);
lienList.setSelectedValue(c.LIEN,true);
panel.add(NUM_AFFILIATIONL);panel.add(new JLabel(c.NUM_AFFILIATION));
 //}
 panel.add(CODEL);panel.add(new JLabel(c.CODE));
panel.add(NOM_CLIENTL);panel.add(NOM_CLIENT);
panel.add(PRENOM_CLIENTL);panel.add(PRENOM_CLIENT);
JScrollPane S =new JScrollPane(lienList);
S.setPreferredSize( new Dimension (40,40) );
panel.add(LIEN);panel.add(S);
panel.add(BENEFICIAIREL);panel.add(BENEFICIAIRE);
panel.add(NAISSANCEL);panel.add(NAISSANCE);
panel.add(SEXEL);panel.add(sexeList);
panel.add(PERCENTAGEL);panel.add(PERCENTAGE);
panel.add(DATE_EXPL);panel.add(DATE_EXP); 
panel.add(ASSURANCEL); 

panel.add(new JLabel("RAMA")); 

panel.add(EMPLOYEURL);panel.add(EMPLOYEUR);
panel.add(SECTEURL);panel.add(SECTEUR);

send.setText("UPDATE");
Pane.add(panel,BorderLayout.CENTER);

Pane.add(send,BorderLayout.SOUTH);

content.add(Pane, BorderLayout.WEST);

}
 


/**********************************************************************************************
AJOUT Mannuel
**********************************************************************************************/
public void send() {

send.addActionListener(new ActionListener() {

    public void actionPerformed(ActionEvent ae) {
        if (ae.getSource() == send) {

            String DBB = "CLIENT_RAMA";
            String NUM_AFFILIATIONa = NUM_AFFILIATION.getText().toUpperCase();
            String NAISS=NAISSANCE.getText();
            String GENDER="" + sexeList.getSelectedValue();

            String nAff = "'" + NUM_AFFILIATIONa + "'";

            String NOM_CLIENTa = NOM_CLIENT.getText().toUpperCase();
            String PRENOM_CLIENTa = PRENOM_CLIENT.getText().toUpperCase();

            int PERCENTAGEa = getIn(PERCENTAGE.getText());

//PERCENTAGEa=15;
            String datedoc = DATE_EXP.getText();

            int dated = 0;



            String ASSURANCEa = "";


            ASSURANCEa = "RAMA";

            String EMPLOYEURa = EMPLOYEUR.getText().toUpperCase();
            String SECTEURa = SECTEUR.getText().toUpperCase();
            try {
                if ((datedoc.length() == 6)) {
                    String createString1 = "" + datedoc.substring(4) + "" + datedoc.substring(2, 4) + "" + datedoc.substring(0, 2);
                    Integer hhh = new Integer(createString1);
                    dated = hhh.intValue();

                    int DATE_EXPa = dated;
                    if (soc.equals("CASHREMISE")) {
                        DBB = "CLIENT_UNIPHARMA";
                    }

                    if (!(PERCENTAGEa == -1)) {
                        if (!NUM_AFFILIATIONa.equals("") || soc.equals("CASHREMISE")) {
                            if (!NOM_CLIENTa.equals("") && !PRENOM_CLIENTa.equals("") && !SECTEURa.equals("")) {
                                if (!ASSURANCEa.equals("") && !NAISS.contains(" ")&& GENDER!=null && !GENDER.equals("null") ) {
                                    String link = (String) (lienList.getSelectedValue());
                                    boolean dop = true;
                                    if (what.equals("UPDATE")) {
                                        dop = db.upDateClientR(new Client(NUM_AFFILIATIONa, NOM_CLIENTa, PRENOM_CLIENTa, PERCENTAGEa, DATE_EXPa, ASSURANCEa, EMPLOYEURa, SECTEURa, "O", NUML.getText(), NAISS, "" + sexeList.getSelectedValue(), link, BENEFICIAIRE.getText()), DBB, NUM_AFFILIATIONa);
                                        db.insertClientR_tracking(c);
                                   
                                    } else {
                                        dop = db.insertClientR(new Client(NUM_AFFILIATIONa, NOM_CLIENTa, PRENOM_CLIENTa, PERCENTAGEa, DATE_EXPa, ASSURANCEa, EMPLOYEURa, SECTEURa, "O", NUML.getText(), NAISS, "" + sexeList.getSelectedValue(), link, BENEFICIAIRE.getText()));
                                    }
                                    if (dop) {
                                        JOptionPane.showMessageDialog(null, db.l(db.lang, "V_CLIENT") + " " + db.l(db.lang, "V_ARINJIYE") + " " + db.l(db.lang, "V_NTAKIBAZO"), db.l(db.lang, "V_MERCI"), JOptionPane.INFORMATION_MESSAGE);
                                    }
                                } else {
                                    JOptionPane.showMessageDialog(null, db.l(db.lang, "V_IKIBAZO") + " " + db.l(db.lang, "V_DANS") + " " + db.l(db.lang, "V_INYANDIKO"), db.l(db.lang, "V_IKIBAZO"), JOptionPane.WARNING_MESSAGE);
                                }
                            } else {
                                JOptionPane.showMessageDialog(null, db.l(db.lang, "V_IKIBAZO") + " " + db.l(db.lang, "V_DANS") + " " + db.l(db.lang, "V_INYANDIKO"), db.l(db.lang, "V_DESIGNATION"), JOptionPane.WARNING_MESSAGE);
                            }
                        } else {
                            JOptionPane.showMessageDialog(null, db.l(db.lang, "V_IKIBAZO") + " " + db.l(db.lang, "V_DANS") + " " + db.l(db.lang, "V_INYANDIKO"), db.l(db.lang, "V_NUMAFFILIATION"), JOptionPane.WARNING_MESSAGE);
                        }
                    }

                } else {
                    JOptionPane.showMessageDialog(null, datedoc.length() + " " + db.l(db.lang, "V_FORMAT") + "   311215  ", db.l(db.lang, "V_DATE"), JOptionPane.WARNING_MESSAGE);
                }

            } catch (Throwable ex) {
                JOptionPane.showMessageDialog(null, ex, db.l(db.lang, "V_IKIBAZO"), JOptionPane.WARNING_MESSAGE);
            }
        }
    }
});

}
 @Override
protected void processWindowEvent(WindowEvent evt)
{
   if(WindowEvent.WINDOW_CLOSING == evt.getID())
   {
	   this.dispose();
			//default icon, custom title
//JOptionPane.showMessageDialog(this,"RIRAFUNGIRA RIMWE N'ISHYIGA"," IKIBAZO",JOptionPane.PLAIN_MESSAGE);
        			//this.setDefaultCloseOperation(HIDE_ON_CLOSE);


	}

}


public static void main(String[] arghs)
{
        try {
            StartServer d = new StartServer();
            Db db = new Db("localhost","ishyiga");
            db.read();

            //new PersonInterface(db, "", "RAMA");

        } catch (Throwable ex) {
            Logger.getLogger(PersonInterface.class.getName()).log(Level.SEVERE, null, ex);
        }

}
}
