/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pos;

/**
 *
 * @author Kimenyi
 */
import java.io.FileInputStream;
import java.io.IOException;
import javax.print.Doc;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintException;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.SimpleDoc;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.Copies;

public class PrintImage {

    /** Creates a new instance of PrintImage */
    static public void main(String args[]) throws Exception {
    try {
      PrintRequestAttributeSet pras = new HashPrintRequestAttributeSet();
      pras.add(new Copies(1));

      PrintService pss[] = PrintServiceLookup.lookupPrintServices(DocFlavor.INPUT_STREAM.GIF, pras);

      if (pss.length == 0)
        throw new RuntimeException("No printer services available.");

      PrintService ps = pss[0];
      System.out.println("Printing to " + ps);

      DocPrintJob job = ps.createPrintJob();

      FileInputStream fin = new FileInputStream("C:\\Users\\Kimenyi\\Desktop\\Ishyiga SoftWear\\Ishyiga for Unipharma\\Unipharma\\uni.gif");
      Doc doc = new SimpleDoc(fin, DocFlavor.INPUT_STREAM.GIF, null);
 
      job.print(doc, pras);

      fin.close();
    } catch (IOException ie) {
      ie.printStackTrace();
    } catch (PrintException pe) {
      pe.printStackTrace();
    }
  }

}