/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pos;

/**
 *
 * @author Kimenyi
 */ 
import java.awt.Color;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.util.LinkedList;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author Kimenyi Aimable
 */
public class Map1 extends JFrame{
private JButton send,run;
JPanel header;
JLabel nom_variableheader       ,valeurheader,   tailleheader,   X_header    ,      Y_header,     boldheader;
JLabel logoL,
       dateL,
       rueL,
       adresseL,
       localiteL,
       telL,
       faxL,
       emailL,
       tvaL,
       clientL,
       factureL,
       referenceL;

               JTextField logoV,     logoT ,logoX,     logoY,     logoB,
                          dateV,     dateT ,dateX,     dateY,     dateB,
                          rueV ,       rueT,rueX ,      rueY,      rueB,
                          adresseV,adresseT,adresseX,adresseY, adresseB,
                          localiteV,localiteT,localiteX,localiteY, localiteB,
                          telV,        telT,   telX,    telY,       telB,
                          faxV,        faxT,   faxX,    faxY,       faxB,
                          emailV,    emailT,  emailX,  emailY,    emailB,
                          tvaV,        tvaT,    tvaX,    tvaY,      tvaB,
                                    clientT, clientX, clientY,  clientB,
                          factureV,factureT, factureX,factureY,factureB,
                          referenceV,referenceT,referenceX,referenceY,referenceB;
 JLabel villeL,clientLargeurL,clientLongeurL,entete1XL,entete2XL,ligne1XL,ligne2XL;
 JTextField ville,clientLargeur,clientLongeur,entete1X,entete2X,ligne1X,ligne2X;

     JPanel     enteteManager;
     JLabel     enteteTailleE  ,    enteteBoldE,     tailleLigneE,  boldLigneE,longeurLigneE, marginE,lengthE;
     JTextField enteteTaille  ,     enteteBold,      tailleLigne ,  boldLigne ,longeurLigne , margin ,length;

     JPanel entete;
     JLabel nom_variableentete       ,valeurentete,   tailleentete,   largeurentete    ,      dimendionentete,     boldentete, variableentete, centrageentete;

     JLabel entete_1, entete_2, entete_3, entete_4,entete_5,entete_6,entete_7,entete_8,entete_9, entete_10;

     JTextField
                                 entete_1Valeur, entete_1Variable, entete_1Largeur, entete_1Dimension, entete_1Centrage,
                                 entete_2Valeur,entete_2Variable,entete_2Largeur,entete_2Dimension,entete_2Centrage,
                                 entete_3Valeur, entete_3Variable, entete_3Largeur, entete_3Dimension, entete_3Centrage,
                                 entete_4Valeur,entete_4Variable,entete_4Largeur,entete_4Dimension,entete_4Centrage,
                                 entete_5Valeur,entete_5Variable,entete_5Largeur,entete_5Dimension,entete_5Centrage,
                                 entete_6Valeur,entete_6Variable,entete_6Largeur,entete_6Dimension,entete_6Centrage,
                                 entete_7Valeur,entete_7Variable,entete_7Largeur,entete_7Dimension,entete_7Centrage,
                                 entete_8Valeur,entete_8Variable,entete_8Largeur,entete_8Dimension,entete_8Centrage,
                                 entete_9Valeur,entete_9Variable,entete_9Largeur,entete_9Dimension,entete_9Centrage,
                                 entete_10Valeur,entete_10Variable,entete_10Largeur,entete_10Dimension,entete_10Centrage;


     JPanel prixManager;
     JLabel  prixTailleP  ,     prixBoldP,     tailleLigneP,  boldLigneP,longeurLigneP, marginP,lengthP;
     JTextField prixTaille  ,     prixBold,     taillePrix,  boldPrix,longeurPrix, marginPrix,lengthPrix;
     JPanel prix;
     JLabel nom_variableP       ,valeurP,   tailleP,   largeurP    ,      dimendionP,     boldP, variableP, centrageP;
     JLabel prix_1, prix_2, prix_3, prix_4;
     JTextField
                                 prix_1Valeur, prix_1Variable, prix_1Largeur, prix_1Dimension, prix_1Centrage,
                                 prix_2Valeur,prix_2Variable,prix_2Largeur,prix_2Dimension,prix_2Centrage,
                                 prix_3Valeur, prix_3Variable, prix_3Largeur, prix_3Dimension, prix_3Centrage,
                                 prix_4Valeur, prix_4Variable, prix_4Largeur, prix_4Dimension, prix_4Centrage;

JPanel footerManager;
JLabel    marginfooterL,lengthfooterL;
JTextField  marginfooter    ,lengthfooter;

JPanel footer;
JLabel footerLabel, nom_variableF       ,valeurF,   tailleF,   X_F    ,      Y_F,     boldF;
JLabel footer_1,
       footer_2,
       footer_3,
       footer_4,
       footer_5,
       footer_6,
       footer_7,
       footer_8,
       footer_9
      ,footer_10,
      footer_11,footer_12;


               JTextField

                         footer_1Nom, footer_1Valeur,footer_1Taille,footer_1X,footer_1Y,footer_1Bold,
                         footer_2Nom, footer_2Valeur,footer_2Taille,footer_2X,footer_2Y,footer_2Bold,
                         footer_3Nom, footer_3Valeur,footer_3Taille,footer_3X,footer_3Y,footer_3Bold,
                         footer_4Nom, footer_4Valeur,footer_4Taille,footer_4X,footer_4Y,footer_4Bold,
                         footer_5Nom, footer_5Valeur,footer_5Taille,footer_5X,footer_5Y,footer_5Bold,
                         footer_6Nom, footer_6Valeur,footer_6Taille,footer_6X,footer_6Y,footer_6Bold,
                         footer_7Nom, footer_7Valeur,footer_7Taille,footer_7X,footer_7Y,footer_7Bold,
                         footer_8Nom, footer_8Valeur,footer_8Taille,footer_8X,footer_8Y,footer_8Bold,
                         footer_9Nom, footer_9Valeur,footer_9Taille,footer_9X,footer_9Y,footer_9Bold,
                         footer_10Nom, footer_10Valeur,footer_10Taille,footer_10X,footer_10Y,footer_10Bold,
                         footer_11Nom, footer_11Valeur,footer_11Taille,footer_11X,footer_11Y,footer_11Bold,
                         footer_12Nom, footer_12Valeur,footer_12Taille,footer_12X,footer_12Y,footer_12Bold;


LinkedList<Product>pro;
Db db;
String mode;
 Map1(String server,String mode,String dbName)
    {
this.db= new Db(server,dbName);

 db.read();

this.mode=mode;
draw();

setTitle("ISHYIGA Mis en Page "+mode);



enableEvents(WindowEvent.WINDOW_CLOSING);
Container content = getContentPane();
eastPane(content);
send();

setSize(900,700);
setVisible(true);

    }

 public void draw()
 {

this.logoV= new JTextField();logoV.setText(getValue("logoV",mode));


this.dateV= new JTextField();dateV.setText(getValue("dateV",mode));
this.rueV= new JTextField();rueV.setText(getValue("rueV",mode));
this.adresseV= new JTextField();adresseV.setText(getValue("adresseV",mode));
this.localiteV= new JTextField();localiteV.setText(getValue("localiteV",mode));
this.telV= new JTextField();telV.setText(getValue("telV",mode));
this.faxV= new JTextField();faxV.setText(getValue("faxV",mode));
this.emailV= new JTextField();emailV.setText(getValue("emailV",mode));
this.tvaV= new JTextField();tvaV.setText(getValue("tvaV",mode));
this.clientT= new JTextField();clientT.setText(getValue("clientT",mode));
this.factureT= new JTextField();factureT.setText(getValue("factureT",mode));
this.referenceT= new JTextField();referenceT.setText(getValue("referenceT",mode));
this.logoT= new JTextField();logoT.setText(getValue("logoT",mode));
this.dateT= new JTextField();dateT.setText(getValue("dateT",mode));
this.rueT= new JTextField();rueT.setText(getValue("rueT",mode));
this.adresseT= new JTextField();adresseT.setText(getValue("adresseT",mode));
this.localiteT= new JTextField();localiteT.setText(getValue("localiteT",mode));
this.telT= new JTextField();telT.setText(getValue("telT",mode));
this.faxT= new JTextField();faxT.setText(getValue("faxT",mode));
this.emailT= new JTextField();emailT.setText(getValue("emailT",mode));
this.tvaT= new JTextField();tvaT.setText(getValue("tvaT",mode));
this.clientX= new JTextField();clientX.setText(getValue("clientX",mode));
this.factureX= new JTextField();factureX.setText(getValue("factureX",mode));
this.referenceX= new JTextField();referenceX.setText(getValue("referenceX",mode));
this.logoX= new JTextField();logoX.setText(getValue("logoX",mode));
this.dateX= new JTextField();dateX.setText(getValue("dateX",mode));
this.rueX= new JTextField();rueX.setText(getValue("rueX",mode));
this.adresseX= new JTextField();adresseX.setText(getValue("adresseX",mode));
this.localiteX= new JTextField();localiteX.setText(getValue("localiteX",mode));
this.telX= new JTextField();telX.setText(getValue("telX",mode));
this.faxX= new JTextField();faxX.setText(getValue("faxX",mode));
this.emailX= new JTextField();emailX.setText(getValue("emailX",mode));
this.tvaX= new JTextField();tvaX.setText(getValue("tvaX",mode));
this.clientY= new JTextField();clientY.setText(getValue("clientY",mode));
this.factureY= new JTextField();factureY.setText(getValue("factureY",mode));
this.referenceY= new JTextField();referenceY.setText(getValue("referenceY",mode));
this.logoY= new JTextField();logoY.setText(getValue("logoY",mode));
this.dateY= new JTextField();dateY.setText(getValue("dateY",mode));
this.rueY= new JTextField();rueY.setText(getValue("rueY",mode));
this.adresseY= new JTextField();adresseY.setText(getValue("adresseY",mode));
this.localiteY= new JTextField();localiteY.setText(getValue("localiteY",mode));
this.telY= new JTextField();telY.setText(getValue("telY",mode));
this.faxY= new JTextField();faxY.setText(getValue("faxY",mode));
this.emailY= new JTextField();emailY.setText(getValue("emailY",mode));
this.tvaY= new JTextField();tvaY.setText(getValue("tvaY",mode));
this.clientB= new JTextField();clientB.setText(getValue("clientB",mode));
this.factureB= new JTextField();factureB.setText(getValue("factureB",mode));
this.referenceB= new JTextField();referenceB.setText(getValue("referenceB",mode));
this.logoB= new JTextField();logoB.setText(getValue("logoB",mode));
this.dateB= new JTextField();dateB.setText(getValue("dateB",mode));
this.rueB= new JTextField();rueB.setText(getValue("rueB",mode));
this.adresseB= new JTextField();adresseB.setText(getValue("adresseB",mode));
this.localiteB= new JTextField();localiteB.setText(getValue("localiteB",mode));
this.telB= new JTextField();telB.setText(getValue("telB",mode));
this.faxB= new JTextField();faxB.setText(getValue("faxB",mode));
this.emailB= new JTextField();emailB.setText(getValue("emailB",mode));
this.tvaB= new JTextField();tvaB.setText(getValue("tvaB",mode));
this.factureV= new JTextField();factureV.setText(getValue("factureV",mode));
this.referenceV= new JTextField();referenceV.setText(getValue("referenceV",mode));


this.enteteTaille= new JTextField();enteteTaille.setText(getValue("enteteTaille",mode));
this.enteteBold= new JTextField();enteteBold.setText(getValue("enteteBold",mode));
this.tailleLigne= new JTextField();tailleLigne.setText(getValue("tailleLigne",mode));
this.boldLigne= new JTextField();boldLigne.setText(getValue("boldLigne",mode));
this.longeurLigne= new JTextField();longeurLigne.setText(getValue("longeurLigne",mode));
this.margin= new JTextField();margin.setText(getValue("margin",mode));
this.length= new JTextField();length.setText(getValue("length",mode));
this.entete_1Valeur= new JTextField();entete_1Valeur.setText(getValue("entete_1Valeur",mode));
this.entete_2Valeur= new JTextField();entete_2Valeur.setText(getValue("entete_2Valeur",mode));
this.entete_3Valeur= new JTextField();entete_3Valeur.setText(getValue("entete_3Valeur",mode));
this.entete_4Valeur= new JTextField();entete_4Valeur.setText(getValue("entete_4Valeur",mode));
this.entete_5Valeur= new JTextField();entete_5Valeur.setText(getValue("entete_5Valeur",mode));
this.entete_6Valeur= new JTextField();entete_6Valeur.setText(getValue("entete_6Valeur",mode));
this.entete_7Valeur= new JTextField();entete_7Valeur.setText(getValue("entete_7Valeur",mode));
this.entete_8Valeur= new JTextField();entete_8Valeur.setText(getValue("entete_8Valeur",mode));
this.entete_9Valeur= new JTextField();entete_9Valeur.setText(getValue("entete_9Valeur",mode));
this.entete_10Valeur= new JTextField();entete_10Valeur.setText(getValue("entete_10Valeur",mode));

this.entete_1Variable= new JTextField();entete_1Variable.setText(getValue("entete_1Variable",mode));
this.entete_2Variable= new JTextField();entete_2Variable.setText(getValue("entete_2Variable",mode));
this.entete_3Variable= new JTextField();entete_3Variable.setText(getValue("entete_3Variable",mode));
this.entete_4Variable= new JTextField();entete_4Variable.setText(getValue("entete_4Variable",mode));
this.entete_5Variable= new JTextField();entete_5Variable.setText(getValue("entete_5Variable",mode));
this.entete_6Variable= new JTextField();entete_6Variable.setText(getValue("entete_6Variable",mode));
this.entete_7Variable= new JTextField();entete_7Variable.setText(getValue("entete_7Variable",mode));
this.entete_8Variable= new JTextField();entete_8Variable.setText(getValue("entete_8Variable",mode));
this.entete_9Variable= new JTextField();entete_9Variable.setText(getValue("entete_9Variable",mode));
this.entete_10Variable= new JTextField();entete_10Variable.setText(getValue("entete_10Variable",mode));
this.entete_1Largeur= new JTextField();entete_1Largeur.setText(getValue("entete_1Largeur",mode));
this.entete_2Largeur= new JTextField();entete_2Largeur.setText(getValue("entete_2Largeur",mode));
this.entete_3Largeur= new JTextField();entete_3Largeur.setText(getValue("entete_3Largeur",mode));
this.entete_4Largeur= new JTextField();entete_4Largeur.setText(getValue("entete_4Largeur",mode));
this.entete_5Largeur= new JTextField();entete_5Largeur.setText(getValue("entete_5Largeur",mode));
this.entete_6Largeur= new JTextField();entete_6Largeur.setText(getValue("entete_6Largeur",mode));
this.entete_7Largeur= new JTextField();entete_7Largeur.setText(getValue("entete_7Largeur",mode));
this.entete_8Largeur= new JTextField();entete_8Largeur.setText(getValue("entete_8Largeur",mode));
this.entete_9Largeur= new JTextField();entete_9Largeur.setText(getValue("entete_9Largeur",mode));
this.entete_10Largeur= new JTextField();entete_10Largeur.setText(getValue("entete_10Largeur",mode));
this.entete_1Dimension= new JTextField();entete_1Dimension.setText(getValue("entete_1Dimension",mode));
this.entete_2Dimension= new JTextField();entete_2Dimension.setText(getValue("entete_2Dimension",mode));
this.entete_3Dimension= new JTextField();entete_3Dimension.setText(getValue("entete_3Dimension",mode));
this.entete_4Dimension= new JTextField();entete_4Dimension.setText(getValue("entete_4Dimension",mode));
this.entete_5Dimension= new JTextField();entete_5Dimension.setText(getValue("entete_5Dimension",mode));
this.entete_6Dimension= new JTextField();entete_6Dimension.setText(getValue("entete_6Dimension",mode));
this.entete_7Dimension= new JTextField();entete_7Dimension.setText(getValue("entete_7Dimension",mode));
this.entete_8Dimension= new JTextField();entete_8Dimension.setText(getValue("entete_8Dimension",mode));
this.entete_9Dimension= new JTextField();entete_9Dimension.setText(getValue("entete_9Dimension",mode));
this.entete_10Dimension= new JTextField();entete_10Dimension.setText(getValue("entete_10Dimension",mode));
this.entete_1Centrage= new JTextField();entete_1Centrage.setText(getValue("entete_1Centrage",mode));
this.entete_2Centrage= new JTextField();entete_2Centrage.setText(getValue("entete_2Centrage",mode));
this.entete_3Centrage= new JTextField();entete_3Centrage.setText(getValue("entete_3Centrage",mode));
this.entete_4Centrage= new JTextField();entete_4Centrage.setText(getValue("entete_4Centrage",mode));
this.entete_5Centrage= new JTextField();entete_5Centrage.setText(getValue("entete_5Centrage",mode));
this.entete_6Centrage= new JTextField();entete_6Centrage.setText(getValue("entete_6Centrage",mode));
this.entete_7Centrage= new JTextField();entete_7Centrage.setText(getValue("entete_7Centrage",mode));
this.entete_8Centrage= new JTextField();entete_8Centrage.setText(getValue("entete_8Centrage",mode));
this.entete_9Centrage= new JTextField();entete_9Centrage.setText(getValue("entete_9Centrage",mode));
this.entete_10Centrage= new JTextField();entete_10Centrage.setText(getValue("entete_10Centrage",mode));
this.prixBold= new JTextField();prixBold.setText(getValue("prixBold",mode));
this.taillePrix= new JTextField();taillePrix.setText(getValue("taillePrix",mode));
this.boldPrix= new JTextField();boldPrix.setText(getValue("boldPrix",mode));
this.longeurPrix= new JTextField();longeurPrix.setText(getValue("longeurPrix",mode));

this.prix_1Variable= new JTextField();prix_1Variable.setText(getValue("prix_1Variable",mode));
this.prix_2Variable= new JTextField();prix_2Variable.setText(getValue("prix_2Variable",mode));
this.prix_3Variable= new JTextField();prix_3Variable.setText(getValue("prix_3Variable",mode));
this.prix_4Variable= new JTextField();prix_4Variable.setText(getValue("prix_4Variable",mode));

this.prix_1Valeur= new JTextField();prix_1Valeur.setText(getValue("prix_1Valeur",mode));
this.prix_2Valeur= new JTextField();prix_2Valeur.setText(getValue("prix_2Valeur",mode));
this.prix_3Valeur= new JTextField();prix_3Valeur.setText(getValue("prix_3Valeur",mode));
this.prix_4Valeur= new JTextField();prix_4Valeur.setText(getValue("prix_4Valeur",mode));

this.prix_1Largeur= new JTextField();prix_1Largeur.setText(getValue("prix_1Largeur",mode));
this.prix_2Largeur= new JTextField();prix_2Largeur.setText(getValue("prix_2Largeur",mode));
this.prix_3Largeur= new JTextField();prix_3Largeur.setText(getValue("prix_3Largeur",mode));
this.prix_4Largeur= new JTextField();prix_4Largeur.setText(getValue("prix_4Largeur",mode));
this.prix_1Dimension= new JTextField();prix_1Dimension.setText(getValue("prix_1Dimension",mode));
this.prix_2Dimension= new JTextField();prix_2Dimension.setText(getValue("prix_2Dimension",mode));
this.prix_3Dimension= new JTextField();prix_3Dimension.setText(getValue("prix_3Dimension",mode));
this.prix_4Dimension= new JTextField();prix_4Dimension.setText(getValue("prix_4Dimension",mode));
this.prix_1Centrage= new JTextField();prix_1Centrage.setText(getValue("prix_1Centrage",mode));
this.prix_2Centrage= new JTextField();prix_2Centrage.setText(getValue("prix_2Centrage",mode));
this.prix_3Centrage= new JTextField();prix_3Centrage.setText(getValue("prix_3Centrage",mode));
this.prix_4Centrage= new JTextField();prix_4Centrage.setText(getValue("prix_4Centrage",mode));
this.lengthPrix= new JTextField();lengthPrix.setText(getValue("lengthPrix",mode));
this.marginPrix= new JTextField();marginPrix.setText(getValue("marginPrix",mode));
this.prixTaille= new JTextField();prixTaille.setText(getValue("prixTaille",mode));
this.prixBold= new JTextField();prixBold.setText(getValue("prixBold",mode));
this.taillePrix= new JTextField();taillePrix.setText(getValue("taillePrix",mode));
this.boldPrix= new JTextField();boldPrix.setText(getValue("boldPrix",mode));
this.longeurPrix= new JTextField();longeurPrix.setText(getValue("longeurPrix",mode));


this.marginfooter= new JTextField();marginfooter.setText(getValue("marginfooter",mode));
this.lengthfooter= new JTextField();lengthfooter.setText(getValue("lengthfooter",mode));
this.footer_1Nom= new JTextField();footer_1Nom.setText(getValue("footer_1Nom",mode));
this.footer_2Nom= new JTextField();footer_2Nom.setText(getValue("footer_2Nom",mode));
this.footer_3Nom= new JTextField();footer_3Nom.setText(getValue("footer_3Nom",mode));
this.footer_4Nom= new JTextField();footer_4Nom.setText(getValue("footer_4Nom",mode));
this.footer_5Nom= new JTextField();footer_5Nom.setText(getValue("footer_5Nom",mode));
this.footer_6Nom= new JTextField();footer_6Nom.setText(getValue("footer_6Nom",mode));
this.footer_7Nom= new JTextField();footer_7Nom.setText(getValue("footer_7Nom",mode));
this.footer_8Nom= new JTextField();footer_8Nom.setText(getValue("footer_8Nom",mode));

this.footer_9Nom= new JTextField();footer_9Nom.setText(getValue("footer_9Nom",mode));
this.footer_10Nom= new JTextField();footer_10Nom.setText(getValue("footer_10Nom",mode));
this.footer_11Nom= new JTextField();footer_11Nom.setText(getValue("footer_11Nom",mode));
this.footer_12Nom= new JTextField();footer_12Nom.setText(getValue("footer_12Nom",mode));

this.footer_1Valeur= new JTextField();footer_1Valeur.setText(getValue("footer_1Valeur",mode));
this.footer_2Valeur= new JTextField();footer_2Valeur.setText(getValue("footer_2Valeur",mode));
this.footer_3Valeur= new JTextField();footer_3Valeur.setText(getValue("footer_3Valeur",mode));
this.footer_4Valeur= new JTextField();footer_4Valeur.setText(getValue("footer_4Valeur",mode));
this.footer_5Valeur= new JTextField();footer_5Valeur.setText(getValue("footer_5Valeur",mode));
this.footer_6Valeur= new JTextField();footer_6Valeur.setText(getValue("footer_6Valeur",mode));
this.footer_7Valeur= new JTextField();footer_7Valeur.setText(getValue("footer_7Valeur",mode));
this.footer_8Valeur= new JTextField();footer_8Valeur.setText(getValue("footer_8Valeur",mode));
this.footer_9Valeur= new JTextField();footer_9Valeur.setText(getValue("footer_9Valeur",mode));
this.footer_10Valeur= new JTextField();footer_10Valeur.setText(getValue("footer_10Valeur",mode));
this.footer_11Valeur= new JTextField();footer_11Valeur.setText(getValue("footer_11Valeur",mode));
this.footer_12Valeur= new JTextField();footer_12Valeur.setText(getValue("footer_12Valeur",mode));


this.footer_1Taille= new JTextField();footer_1Taille.setText(getValue("footer_1Taille",mode));
this.footer_2Taille= new JTextField();footer_2Taille.setText(getValue("footer_2Taille",mode));
this.footer_3Taille= new JTextField();footer_3Taille.setText(getValue("footer_3Taille",mode));
this.footer_4Taille= new JTextField();footer_4Taille.setText(getValue("footer_4Taille",mode));
this.footer_5Taille= new JTextField();footer_5Taille.setText(getValue("footer_5Taille",mode));
this.footer_6Taille= new JTextField();footer_6Taille.setText(getValue("footer_6Taille",mode));
this.footer_7Taille= new JTextField();footer_7Taille.setText(getValue("footer_7Taille",mode));
this.footer_8Taille= new JTextField();footer_8Taille.setText(getValue("footer_8Taille",mode));
this.footer_9Taille= new JTextField();footer_9Taille.setText(getValue("footer_9Taille",mode));
this.footer_10Taille= new JTextField();footer_10Taille.setText(getValue("footer_10Taille",mode));
this.footer_11Taille= new JTextField();footer_11Taille.setText(getValue("footer_11Taille",mode));
this.footer_12Taille= new JTextField();footer_12Taille.setText(getValue("footer_12Taille",mode));

this.footer_1X= new JTextField();footer_1X.setText(getValue("footer_1X",mode));
this.footer_2X= new JTextField();footer_2X.setText(getValue("footer_2X",mode));
this.footer_3X= new JTextField();footer_3X.setText(getValue("footer_3X",mode));
this.footer_4X= new JTextField();footer_4X.setText(getValue("footer_4X",mode));
this.footer_5X= new JTextField();footer_5X.setText(getValue("footer_5X",mode));
this.footer_6X= new JTextField();footer_6X.setText(getValue("footer_6X",mode));
this.footer_7X= new JTextField();footer_7X.setText(getValue("footer_7X",mode));
this.footer_8X= new JTextField();footer_8X.setText(getValue("footer_8X",mode));
this.footer_9X= new JTextField();footer_9X.setText(getValue("footer_9X",mode));
this.footer_10X= new JTextField();footer_10X.setText(getValue("footer_10X",mode));
this.footer_11X= new JTextField();footer_11X.setText(getValue("footer_11X",mode));
this.footer_12X= new JTextField();footer_12X.setText(getValue("footer_12X",mode));

this.footer_1Y= new JTextField();footer_1Y.setText(getValue("footer_1Y",mode));
this.footer_2Y= new JTextField();footer_2Y.setText(getValue("footer_2Y",mode));
this.footer_3Y= new JTextField();footer_3Y.setText(getValue("footer_3Y",mode));
this.footer_4Y= new JTextField();footer_4Y.setText(getValue("footer_4Y",mode));
this.footer_5Y= new JTextField();footer_5Y.setText(getValue("footer_5Y",mode));
this.footer_6Y= new JTextField();footer_6Y.setText(getValue("footer_6Y",mode));
this.footer_7Y= new JTextField();footer_7Y.setText(getValue("footer_7Y",mode));
this.footer_8Y= new JTextField();footer_8Y.setText(getValue("footer_8Y",mode));
this.footer_9Y= new JTextField();footer_9Y.setText(getValue("footer_9Y",mode));
this.footer_10Y= new JTextField();footer_10Y.setText(getValue("footer_10Y",mode));
this.footer_11Y= new JTextField();footer_11Y.setText(getValue("footer_11Y",mode));
this.footer_12Y= new JTextField();footer_12Y.setText(getValue("footer_12Y",mode));

this.footer_1Bold= new JTextField();footer_1Bold.setText(getValue("footer_1Bold",mode));
this.footer_2Bold= new JTextField();footer_2Bold.setText(getValue("footer_2Bold",mode));
this.footer_3Bold= new JTextField();footer_3Bold.setText(getValue("footer_3Bold",mode));
this.footer_4Bold= new JTextField();footer_4Bold.setText(getValue("footer_4Bold",mode));
this.footer_5Bold= new JTextField();footer_5Bold.setText(getValue("footer_5Bold",mode));
this.footer_6Bold= new JTextField();footer_6Bold.setText(getValue("footer_6Bold",mode));
this.footer_7Bold= new JTextField();footer_7Bold.setText(getValue("footer_7Bold",mode));
this.footer_8Bold= new JTextField();footer_8Bold.setText(getValue("footer_8Bold",mode));
this.footer_9Bold= new JTextField();footer_9Bold.setText(getValue("footer_9Bold",mode));
this.footer_10Bold= new JTextField();footer_10Bold.setText(getValue("footer_10Bold",mode));
this.footer_11Bold= new JTextField();footer_11Bold.setText(getValue("footer_11Bold",mode));
this.footer_12Bold= new JTextField();footer_12Bold.setText(getValue("footer_12Bold",mode));

this.clientLargeur= new JTextField();clientLargeur.setText(getValue("clientLargeur",mode));
this.clientLongeur= new JTextField();clientLongeur.setText(getValue("clientLongeur",mode));
this.entete1X= new JTextField();entete1X.setText(getValue("entete1X",mode));
this.entete2X= new JTextField();entete2X.setText(getValue("entete2X",mode));
this.ligne1X= new JTextField();ligne1X.setText(getValue("ligne1X",mode));
this.ligne2X= new JTextField();ligne2X.setText(getValue("ligne2X",mode));
this.ville= new JTextField();ville.setText(getValue("ville",mode));


this.send=new JButton("Update");
send.setBackground(Color.GREEN);

this.run=new JButton(">>> RUN >>>");
run.setBackground(Color.RED);


this.villeL=new JLabel("ville");
this.clientLargeurL=new JLabel("clientLargeur");
this.clientLongeurL=new JLabel("clientLongeurl");
this.entete1XL=new JLabel("entete1XL");
this.entete2XL=new JLabel("entete2XL");
this.ligne1XL=new JLabel("ligne1XL");
this.ligne2XL=new JLabel("ligne2XL");
this.nom_variableheader=new JLabel("nom_variableheader");
this.valeurheader=new JLabel("valeurheader");
this.X_header=new JLabel("X_header");
this.Y_header=new JLabel("Y_header");
this.boldheader=new JLabel("boldheader");
this.tailleheader=new JLabel("tailleheader");
this.boldentete=new JLabel("boldentete");
this.variableentete=new JLabel("variableentete");
this.centrageentete=new JLabel("centrageentete");
this.logoL=new JLabel("logoL");
this.dateL=new JLabel("dateL");
this.rueL=new JLabel("rueL");
this.adresseL=new JLabel("adresseL");
this.localiteL=new JLabel("localiteL");
this.telL=new JLabel   ("telL");
this.faxL=new JLabel   ("faxL");
this.emailL=new JLabel ("emailL");
this.tvaL=new JLabel   ("tvaL");
this.clientL=new JLabel("clientL");
this.factureL=new JLabel("factureL");
this.referenceL=new JLabel("referenceL");
this.enteteTailleE=new JLabel("enteteTailleE");
this.enteteBoldE=new JLabel("longeurFin");
this.tailleLigneE=new JLabel("tailleLigneE");
this.boldLigneE=new JLabel("boldLigneE");
this.longeurLigneE=new JLabel("longeurLigneE");
this.marginE=new JLabel("marginE");
this.lengthE=new JLabel("lengthE");
this.nom_variableentete=new JLabel("nom_variableentete");
this.valeurentete=new JLabel("valeurentete");
this.tailleentete=new JLabel("tailleentete");
this.largeurentete=new JLabel("largeurentete");
this.dimendionentete=new JLabel("dimendionentete");
this.entete_1=new JLabel("entete_1");
this.entete_2=new JLabel("entete_2");
this.entete_3=new JLabel("entete_3");
this.entete_4=new JLabel("entete_4");
this.entete_5=new JLabel("entete_5");
this.entete_6=new JLabel("entete_6");
this.entete_7=new JLabel("entete_7");
this.entete_8=new JLabel("entete_8");
this.entete_9=new JLabel("entete_9");
this.entete_10=new JLabel("entete_10");
this.nom_variableP=new JLabel("nom_variableP");
this.valeurP=new JLabel("valeurP");
this.tailleP=new JLabel("tailleP");
this.largeurP=new JLabel("largeurP");
this.boldP=new JLabel("boldP");
this.dimendionP=new JLabel("dimendionP");
this.variableP=new JLabel("variableP");
this.centrageP=new JLabel("centrageP");
this.prixTailleP=new JLabel("prixTailleP");
this.prixBoldP=new JLabel("prixBoldP");
this.tailleLigneP=new JLabel("tailleLigneP");
this.boldLigneP=new JLabel("boldLigneP");
this.longeurLigneP=new JLabel("longeurLigneP");
this.marginP=new JLabel("marginP");
this.lengthP=new JLabel("lengthP");
this.prix_1=new JLabel("prix_1");
this.prix_2=new JLabel("prix_2");
this.prix_3=new JLabel("prix_3");
this.prix_4=new JLabel("prix_4");
this.marginfooterL=new JLabel("marginfooterL");
this.lengthfooterL=new JLabel("lengthfooterL");
this.footerLabel=new JLabel("footerLabel");
this.nom_variableF=new JLabel("nom_variableF");
this.valeurF=new JLabel("valeurF");
this.tailleF=new JLabel("tailleF");
this.X_F=new JLabel("X_F");
this.Y_F=new JLabel("Y_F");
this.boldF=new JLabel("boldF");
this.footer_1=new JLabel("footer_1");
this.footer_2=new JLabel("footer_2");
this.footer_3=new JLabel("footer_3");
this.footer_4=new JLabel("footer_4");
this.footer_5=new JLabel("footer_5");
this.footer_6=new JLabel("footer_6");
this.footer_7=new JLabel("footer_7");
this.footer_8=new JLabel("footer_8");

this.footer_9=new JLabel("footer_9");
this.footer_10=new JLabel("footer_10");
this.footer_11=new JLabel("footer_11");
this.footer_12=new JLabel("footer_12");

 }
 String getValue(String s,String mod)
 {
String ret=" ";
 Variable var= db.getParam("PRINT",mod,s+mod);
 if(var!=null)
     ret=var.value;
     return ret;
 }
  String getValue2(String s,LinkedList <Variable> par)
 {
     String ret="";
     for(int i=0;i<par.size();i++)
     {
         String sa=par.get(i).nom;
            if(sa.equals(s))
           ret=par.get(i).value2;
       }

     return ret;
 }

    public double getDoub(String s)
{
   double d=0;
    try
    {
        Double in_int=new Double(s);
        d = in_int.doubleValue();
        System.out.println(s);
    }
     catch (NumberFormatException e)
    {
      JOptionPane.showMessageDialog(null,s,s+ db.l(db.lang,"V_ERRORPARAM")+e,JOptionPane.PLAIN_MESSAGE);
    }
return d;
}
  void eastPane(  Container content )
{



    JPanel panel = new JPanel();
    JPanel manager = new JPanel();
    manager.setLayout(new GridLayout(5,1));
    panel.setLayout(new GridLayout(4,1));

    header= new JPanel();
    header.setLayout(new GridLayout(13,6));

header.add(nom_variableheader);
header.add(valeurheader);
header.add(tailleheader);
header.add(X_header);
header.add(Y_header);
header.add( boldheader);
header.add(logoL); header.add(logoV);   header.add(logoT);header.add(logoX);    header.add(logoY);    header.add(logoB);
header.add(nom_variableheader); header.add(valeurheader);  header.add(tailleheader);   header.add(X_header);     header.add(Y_header);    header.add( boldheader);
header.add(logoL); header.add(logoV);   header.add(logoT);header.add(logoX);    header.add(logoY);    header.add(logoB);
header.add(       dateL);header.add(dateV);     header.add(dateT );header.add(dateX);     header.add(dateY);     header.add(dateB);
       header.add(rueL); header.add(rueV );     header.add(  rueT);header.add(rueX );    header.add(  rueY);     header.add( rueB);
       header.add(adresseL);  header.add(adresseV); header.add(adresseT);header.add(adresseX);header.add(adresseY); header.add(adresseB);
       header.add(localiteL); header.add(localiteV);header.add(localiteT);header.add(localiteX);header.add(localiteY); header.add(localiteB);
       header.add(telL); header.add(telV);  header.add(      telT); header.add(  telX);  header.add(  telY);      header.add( telB);
       header.add(faxL); header.add(faxV);       header.add( faxT);   header.add(faxX);    header.add(faxY);       header.add(faxB);
       header.add(emailL);   header.add(emailV);  header.add(  emailT);  header.add(emailX);  header.add(emailY);  header.add(  emailB);
       header.add(tvaL);  header.add(tvaV);        header.add(tvaT);header.add(    tvaX);   header.add( tvaY);  header.add(    tvaB);
       header.add(clientL);  header.add(new JTextField() );header.add(clientT);header.add(clientX);header.add(clientX); header.add(clientY);  header.add(clientB);
       header.add(factureL);  header.add(factureV);header.add(factureT); header.add(factureX);header.add(factureY);header.add(factureB);
       header.add(referenceL);header.add(referenceV);     header.add(referenceT);header.add(referenceX);header.add(referenceY);header.add(referenceB);



enteteManager=new JPanel();
enteteManager.setLayout(new GridLayout(2,7));

enteteManager.add(enteteTailleE  );  enteteManager.add(  enteteBoldE);   enteteManager.add(  tailleLigneE); enteteManager.add( boldLigneE); enteteManager.add(longeurLigneE); enteteManager.add( marginE); enteteManager.add(lengthE);
enteteManager.add( enteteTaille  );  enteteManager.add(   enteteBold);   enteteManager.add(   tailleLigne ); enteteManager.add( boldLigne ); enteteManager.add(longeurLigne ); enteteManager.add(margin );enteteManager.add(length);

manager.add(enteteManager);

entete=new JPanel();
entete.setLayout(new GridLayout(11,8));

entete.add(   nom_variableentete       ); entete.add( valeurentete); entete.add( variableentete);   entete.add( largeurentete    );    entete.add(   dimendionentete);  entete.add( centrageentete);

entete.add(entete_1);entete.add(entete_1Valeur); entete.add(entete_1Variable); entete.add(entete_1Largeur); entete.add(entete_1Dimension); entete.add(entete_1Centrage);
entete.add(entete_2);entete.add(entete_2Valeur);entete.add(entete_2Variable);entete.add(entete_2Largeur);entete.add(entete_2Dimension);entete.add(entete_2Centrage);
entete.add(entete_3);entete.add(entete_3Valeur); entete.add(entete_3Variable); entete.add(entete_3Largeur); entete.add(entete_3Dimension); entete.add(entete_3Centrage);
entete.add(entete_4);entete.add(entete_4Valeur);entete.add(entete_4Variable);entete.add(entete_4Largeur);entete.add(entete_4Dimension);entete.add(entete_4Centrage);
entete.add(entete_5);entete.add(entete_5Valeur);entete.add(entete_5Variable);entete.add(entete_5Largeur);entete.add(entete_5Dimension);entete.add(entete_5Centrage);
entete.add(entete_6);entete.add(entete_6Valeur);entete.add(entete_6Variable);entete.add(entete_6Largeur);entete.add(entete_6Dimension);entete.add(entete_6Centrage);
entete.add(entete_7);entete.add(entete_7Valeur);entete.add(entete_7Variable);entete.add(entete_7Largeur);entete.add(entete_7Dimension);entete.add(entete_7Centrage);
entete.add(entete_8);entete.add(entete_8Valeur);entete.add(entete_8Variable);entete.add(entete_8Largeur);entete.add(entete_8Dimension);entete.add(entete_8Centrage);
entete.add(entete_9);entete.add(entete_9Valeur);entete.add(entete_9Variable);entete.add(entete_9Largeur);entete.add(entete_9Dimension);entete.add(entete_9Centrage);
entete.add(entete_10);entete.add(entete_10Valeur);entete.add(entete_10Variable);entete.add(entete_10Largeur);entete.add(entete_10Dimension);entete.add(entete_10Centrage);



prixManager=new JPanel();
prixManager.setLayout(new GridLayout(2,7));
prixManager.add(prixTailleP  );  prixManager.add(   prixBoldP); prixManager.add(    tailleLigneP); prixManager.add(  boldLigneP);prixManager.add(longeurLigneP); prixManager.add( marginP); prixManager.add(lengthP);
prixManager.add( prixTaille  );   prixManager.add(  prixBold);  prixManager.add(   taillePrix); prixManager.add( boldPrix);prixManager.add(longeurPrix);prixManager.add( marginPrix); prixManager.add(lengthPrix);

manager.add(prixManager);
prix=new JPanel();
prix.setLayout(new GridLayout(5,8));
prix.add(nom_variableP      );
prix.add(valeurP);
//prix.add( tailleP);
prix.add(variableP);
prix.add(largeurP    );
prix.add(    dimendionP);
//prix.add(  boldP);

prix.add(centrageP);
prix.add(prix_1);   prix.add( prix_1Valeur);  prix.add(prix_1Variable); prix.add(prix_1Largeur); prix.add(prix_1Dimension); prix.add(prix_1Centrage);
prix.add(prix_2);    prix.add(prix_2Valeur);prix.add(prix_2Variable);prix.add(prix_2Largeur);prix.add(prix_2Dimension);prix.add(prix_2Centrage);
prix.add(prix_3);    prix.add(prix_3Valeur); prix.add(prix_3Variable); prix.add(prix_3Largeur); prix.add(prix_3Dimension); prix.add(prix_3Centrage);
prix.add(prix_4);    prix.add(prix_4Valeur); prix.add(prix_4Variable); prix.add(prix_4Largeur); prix.add(prix_4Dimension); prix.add(prix_4Centrage);



footerManager = new JPanel();
footerManager.setLayout(new GridLayout(2,8));
footerManager.add(marginfooterL);
footerManager.add(lengthfooterL);

footerManager.add(villeL);footerManager.add(clientLargeurL);footerManager.add(clientLongeurL);
footerManager.add(entete1XL);footerManager.add(entete2XL);footerManager.add(ligne1XL);
footerManager.add(ligne2XL);
footerManager.add(marginfooter);
footerManager.add(lengthfooter);

footerManager.add(ville);footerManager.add(clientLargeur);footerManager.add(clientLongeur);
footerManager.add(entete1X);footerManager.add(entete2X);footerManager.add(ligne1X);
footerManager.add(ligne2X);
//footerManager.add(send);


manager.add(footerManager);
 footer=new JPanel();
footer.setLayout(new GridLayout(13,7));
footer.add( footerLabel); footer.add( nom_variableF       ); footer.add(valeurF); footer.add(  tailleF); footer.add(  X_F    );  footer.add(    Y_F); footer.add(    boldF);


footer.add( footer_1); footer.add( footer_1Nom); footer.add( footer_1Valeur);footer.add( footer_1Taille);footer.add( footer_1X);footer.add( footer_1Y);footer.add( footer_1Bold);
footer.add( footer_2); footer.add( footer_2Nom); footer.add( footer_2Valeur);footer.add( footer_2Taille);footer.add( footer_2X);footer.add( footer_2Y);footer.add( footer_2Bold);
footer.add( footer_3); footer.add( footer_3Nom); footer.add( footer_3Valeur);footer.add( footer_3Taille);footer.add( footer_3X);footer.add( footer_3Y);footer.add( footer_3Bold);
footer.add( footer_4); footer.add( footer_4Nom); footer.add( footer_4Valeur);footer.add( footer_4Taille);footer.add( footer_4X);footer.add( footer_4Y);footer.add( footer_4Bold);
footer.add( footer_5); footer.add( footer_5Nom); footer.add( footer_5Valeur);footer.add( footer_5Taille);footer.add( footer_5X);footer.add( footer_5Y);footer.add( footer_5Bold);
footer.add( footer_6); footer.add( footer_6Nom); footer.add( footer_6Valeur);footer.add( footer_6Taille);footer.add( footer_6X);footer.add( footer_6Y);footer.add( footer_6Bold);
footer.add( footer_7); footer.add( footer_7Nom); footer.add( footer_7Valeur);footer.add( footer_7Taille);footer.add( footer_7X);footer.add( footer_7Y);footer.add( footer_7Bold);
footer.add( footer_8); footer.add( footer_8Nom); footer.add( footer_8Valeur);footer.add( footer_8Taille);footer.add( footer_8X);footer.add( footer_8Y);footer.add( footer_8Bold);
footer.add( footer_9); footer.add( footer_9Nom); footer.add( footer_9Valeur);footer.add( footer_9Taille);footer.add( footer_9X);footer.add( footer_9Y);footer.add( footer_9Bold);
footer.add( footer_10); footer.add( footer_10Nom); footer.add( footer_10Valeur);footer.add( footer_10Taille);footer.add( footer_10X);footer.add( footer_10Y);footer.add( footer_10Bold);
footer.add( footer_11); footer.add( footer_11Nom); footer.add( footer_11Valeur);footer.add( footer_11Taille);footer.add( footer_11X);footer.add( footer_11Y);footer.add( footer_11Bold);
footer.add( footer_12); footer.add( footer_12Nom); footer.add( footer_12Valeur);footer.add( footer_12Taille);footer.add( footer_12X);footer.add( footer_12Y);footer.add( footer_12Bold);


manager.add(send);



       panel.add(header);
       panel.add(manager);
       panel.add(entete);
       panel.add(prix);
       panel.add(footer);

       content.add(panel);





    }

public static int getIn (String in)
{
                      int productCode=-1;
    try
    {

        Integer in_int=new Integer(in);
        productCode = in_int.intValue();

        }

    catch (NumberFormatException e)
    {
     JOptionPane.showMessageDialog(null,in," Ntabwo ari  Pourcentage ",JOptionPane.PLAIN_MESSAGE);


    }
    return productCode;
}

public void send()
{

	send.addActionListener(new ActionListener() {
	public void actionPerformed(ActionEvent ae)
	{
    if(ae.getSource()==send )
    {

LinkedList <Variable> param=new LinkedList();
 Double m;
    try
    {

  param.add(new Variable("logoV",(logoV.getText()),"PRINT",mode));
  param.add(new Variable("dateV",(dateV.getText()),"PRINT",mode));
  param.add(new Variable("rueV",(rueV.getText()),"PRINT",mode));
  param.add(new Variable("adresseV",(adresseV.getText()),"PRINT",mode));
  param.add(new Variable("localiteV",(localiteV.getText()),"PRINT",mode));
  param.add(new Variable("telV",(telV.getText()),"PRINT",mode));
  param.add(new Variable("faxV",(faxV.getText()),"PRINT",mode));
  param.add(new Variable("emailV",(emailV.getText()),"PRINT",mode));
  param.add(new Variable("tvaV",(tvaV.getText()),"PRINT",mode));
  param.add(new Variable("clientT",""+getDoub(clientT.getText()),"PRINT",mode));
  param.add(new Variable("factureT",""+getDoub(factureT.getText()),"PRINT",mode));
  param.add(new Variable("referenceT",""+getDoub(referenceT.getText()),"PRINT",mode));
  param.add(new Variable("logoT",""+getDoub(logoT.getText()),"PRINT",mode));
  param.add(new Variable("dateT",""+getDoub(dateT.getText()),"PRINT",mode));
  param.add(new Variable("rueT",""+getDoub(rueT.getText()),"PRINT",mode));
  param.add(new Variable("adresseT",""+getDoub(adresseT.getText()),"PRINT",mode));
  param.add(new Variable("localiteT",""+getDoub(localiteT.getText()),"PRINT",mode));
  param.add(new Variable("telT",""+getDoub(telT.getText()),"PRINT",mode));
  param.add(new Variable("faxT",""+getDoub(faxT.getText()),"PRINT",mode));
  param.add(new Variable("emailT",""+getDoub(emailT.getText()),"PRINT",mode));
  param.add(new Variable("tvaT",""+getDoub(tvaT.getText()),"PRINT",mode));
  param.add(new Variable("clientX",""+getDoub(clientX.getText()),"PRINT",mode));
  param.add(new Variable("factureX",""+getDoub(factureX.getText()),"PRINT",mode));
  param.add(new Variable("referenceX",""+getDoub(referenceX.getText()),"PRINT",mode));
  param.add(new Variable("logoX",""+getDoub(logoX.getText()),"PRINT",mode));
  param.add(new Variable("dateX",""+getDoub(dateX.getText()),"PRINT",mode));
  param.add(new Variable("rueX",""+getDoub(rueX.getText()),"PRINT",mode));
  param.add(new Variable("adresseX",""+getDoub(adresseX.getText()),"PRINT",mode));
  param.add(new Variable("localiteX",""+getDoub(localiteX.getText()),"PRINT",mode));
  param.add(new Variable("telX",""+getDoub(telX.getText()),"PRINT",mode));
  param.add(new Variable("faxX",""+getDoub(faxX.getText()),"PRINT",mode));
  param.add(new Variable("emailX",""+getDoub(emailX.getText()),"PRINT",mode));
  param.add(new Variable("tvaX",""+getDoub(tvaX.getText()),"PRINT",mode));
  param.add(new Variable("clientY",""+getDoub(clientY.getText()),"PRINT",mode));
  param.add(new Variable("factureY",""+getDoub(factureY.getText()),"PRINT",mode));
  param.add(new Variable("referenceY",""+getDoub(referenceY.getText()),"PRINT",mode));
  param.add(new Variable("logoY",""+getDoub(logoY.getText()),"PRINT",mode));
  param.add(new Variable("dateY",""+getDoub(dateY.getText()),"PRINT",mode));
  param.add(new Variable("rueY",""+getDoub(rueY.getText()),"PRINT",mode));
  param.add(new Variable("adresseY",""+getDoub(adresseY.getText()),"PRINT",mode));
  param.add(new Variable("localiteY",""+getDoub(localiteY.getText()),"PRINT",mode));
  param.add(new Variable("telY",""+getDoub(telY.getText()),"PRINT",mode));
  param.add(new Variable("faxY",""+getDoub(faxY.getText()),"PRINT",mode));
  param.add(new Variable("emailY",""+getDoub(emailY.getText()),"PRINT",mode));
  param.add(new Variable("tvaY",""+getDoub(tvaY.getText()),"PRINT",mode));
  param.add(new Variable("clientB",""+getDoub(clientB.getText()),"PRINT",mode));
  param.add(new Variable("factureB",""+getDoub(factureB.getText()),"PRINT",mode));
  param.add(new Variable("referenceB",""+getDoub(referenceB.getText()),"PRINT",mode));
  param.add(new Variable("logoB",""+getDoub(logoB.getText()),"PRINT",mode));
  param.add(new Variable("dateB",""+getDoub(dateB.getText()),"PRINT",mode));
  param.add(new Variable("rueB",""+getDoub(rueB.getText()),"PRINT",mode));
  param.add(new Variable("adresseB",""+getDoub(adresseB.getText()),"PRINT",mode));
  param.add(new Variable("localiteB",""+getDoub(localiteB.getText()),"PRINT",mode));
  param.add(new Variable("telB",""+getDoub(telB.getText()),"PRINT",mode));
  param.add(new Variable("faxB",""+getDoub(faxB.getText()),"PRINT",mode));
  param.add(new Variable("emailB",""+getDoub(emailB.getText()),"PRINT",mode));
  param.add(new Variable("tvaB",""+getDoub(tvaB.getText()),"PRINT",mode));
  param.add(new Variable("enteteTaille",""+getDoub(enteteTaille.getText()),"PRINT",mode));
  param.add(new Variable("enteteBold",""+getDoub(enteteBold.getText()),"PRINT",mode));
  param.add(new Variable("tailleLigne",""+getDoub(tailleLigne.getText()),"PRINT",mode));
  param.add(new Variable("boldLigne",""+getDoub(boldLigne.getText()),"PRINT",mode));
  param.add(new Variable("longeurLigne",""+getDoub(longeurLigne.getText()),"PRINT",mode));
  param.add(new Variable("margin",""+getDoub(margin.getText()),"PRINT",mode));
  param.add(new Variable("length",""+getDoub(length.getText()),"PRINT",mode));
  param.add(new Variable("entete_1Valeur",(entete_1Valeur.getText()),"PRINT",mode));
  param.add(new Variable("entete_2Valeur",(entete_2Valeur.getText()),"PRINT",mode));
  param.add(new Variable("entete_3Valeur",(entete_3Valeur.getText()),"PRINT",mode));
  param.add(new Variable("entete_4Valeur",(entete_4Valeur.getText()),"PRINT",mode));
  param.add(new Variable("entete_5Valeur",(entete_5Valeur.getText()),"PRINT",mode));
  param.add(new Variable("entete_6Valeur",(entete_6Valeur.getText()),"PRINT",mode));
  param.add(new Variable("entete_7Valeur",(entete_7Valeur.getText()),"PRINT",mode));
  param.add(new Variable("entete_8Valeur",(entete_8Valeur.getText()),"PRINT",mode));
  param.add(new Variable("entete_9Valeur",(entete_9Valeur.getText()),"PRINT",mode));
  param.add(new Variable("entete_10Valeur",(entete_10Valeur.getText()),"PRINT",mode));
  param.add(new Variable("entete_1Variable",(entete_1Variable.getText()),"PRINT",mode));
  param.add(new Variable("entete_2Variable",(entete_2Variable.getText()),"PRINT",mode));
  param.add(new Variable("entete_3Variable",(entete_3Variable.getText()),"PRINT",mode));
  param.add(new Variable("entete_4Variable",(entete_4Variable.getText()),"PRINT",mode));
  param.add(new Variable("entete_5Variable",(entete_5Variable.getText()),"PRINT",mode));
  param.add(new Variable("entete_6Variable",(entete_6Variable.getText()),"PRINT",mode));
  param.add(new Variable("entete_7Variable",(entete_7Variable.getText()),"PRINT",mode));
  param.add(new Variable("entete_8Variable",(entete_8Variable.getText()),"PRINT",mode));
  param.add(new Variable("entete_9Variable",(entete_9Variable.getText()),"PRINT",mode));
  param.add(new Variable("entete_10Variable",(entete_10Variable.getText()),"PRINT",mode));
  param.add(new Variable("entete_1Largeur",""+getDoub(entete_1Largeur.getText()),"PRINT",mode));
  param.add(new Variable("entete_2Largeur",""+getDoub(entete_2Largeur.getText()),"PRINT",mode));
  param.add(new Variable("entete_3Largeur",""+getDoub(entete_3Largeur.getText()),"PRINT",mode));
  param.add(new Variable("entete_4Largeur",""+getDoub(entete_4Largeur.getText()),"PRINT",mode));
  param.add(new Variable("entete_5Largeur",""+getDoub(entete_5Largeur.getText()),"PRINT",mode));
  param.add(new Variable("entete_6Largeur",""+getDoub(entete_6Largeur.getText()),"PRINT",mode));
  param.add(new Variable("entete_7Largeur",""+getDoub(entete_7Largeur.getText()),"PRINT",mode));
  param.add(new Variable("entete_8Largeur",""+getDoub(entete_8Largeur.getText()),"PRINT",mode));
  param.add(new Variable("entete_9Largeur",""+getDoub(entete_9Largeur.getText()),"PRINT",mode));
  param.add(new Variable("entete_10Largeur",""+getDoub(entete_10Largeur.getText()),"PRINT",mode));
  param.add(new Variable("entete_1Dimension",""+getDoub(entete_1Dimension.getText()),"PRINT",mode));
  param.add(new Variable("entete_2Dimension",""+getDoub(entete_2Dimension.getText()),"PRINT",mode));
  param.add(new Variable("entete_3Dimension",""+getDoub(entete_3Dimension.getText()),"PRINT",mode));
  param.add(new Variable("entete_4Dimension",""+getDoub(entete_4Dimension.getText()),"PRINT",mode));
  param.add(new Variable("entete_5Dimension",""+getDoub(entete_5Dimension.getText()),"PRINT",mode));
  param.add(new Variable("entete_6Dimension",""+getDoub(entete_6Dimension.getText()),"PRINT",mode));
  param.add(new Variable("entete_7Dimension",""+getDoub(entete_7Dimension.getText()),"PRINT",mode));
  param.add(new Variable("entete_8Dimension",""+getDoub(entete_8Dimension.getText()),"PRINT",mode));
  param.add(new Variable("entete_9Dimension",""+getDoub(entete_9Dimension.getText()),"PRINT",mode));
  param.add(new Variable("entete_10Dimension",""+getDoub(entete_10Dimension.getText()),"PRINT",mode));
  param.add(new Variable("entete_1Centrage",""+getDoub(entete_1Centrage.getText()),"PRINT",mode));
  param.add(new Variable("entete_2Centrage",""+getDoub(entete_2Centrage.getText()),"PRINT",mode));
  param.add(new Variable("entete_3Centrage",""+getDoub(entete_3Centrage.getText()),"PRINT",mode));
  param.add(new Variable("entete_4Centrage",""+getDoub(entete_4Centrage.getText()),"PRINT",mode));
  param.add(new Variable("entete_5Centrage",""+getDoub(entete_5Centrage.getText()),"PRINT",mode));
  param.add(new Variable("entete_6Centrage",""+getDoub(entete_6Centrage.getText()),"PRINT",mode));
  param.add(new Variable("entete_7Centrage",""+getDoub(entete_7Centrage.getText()),"PRINT",mode));
  param.add(new Variable("entete_8Centrage",""+getDoub(entete_8Centrage.getText()),"PRINT",mode));
  param.add(new Variable("entete_9Centrage",""+getDoub(entete_9Centrage.getText()),"PRINT",mode));
  param.add(new Variable("entete_10Centrage",""+getDoub(entete_10Centrage.getText()),"PRINT",mode));
   param.add(new Variable("prixBold",""+getDoub(prixBold.getText()),"PRINT",mode));
  param.add(new Variable("taillePrix",""+getDoub(taillePrix.getText()),"PRINT",mode));
  param.add(new Variable("boldPrix",""+getDoub(boldPrix.getText()),"PRINT",mode));
  param.add(new Variable("longeurPrix",""+getDoub(longeurPrix.getText()),"PRINT",mode));
  param.add(new Variable("prix_1Variable",""+getDoub(prix_1Variable.getText()),"PRINT",mode));
  param.add(new Variable("prix_2Variable",""+getDoub(prix_2Variable.getText()),"PRINT",mode));
  param.add(new Variable("prix_3Variable",""+getDoub(prix_3Variable.getText()),"PRINT",mode));
  param.add(new Variable("prix_4Variable",""+getDoub(prix_4Variable.getText()),"PRINT",mode));
  param.add(new Variable("prix_1Largeur",""+getDoub(prix_1Largeur.getText()),"PRINT",mode));
  param.add(new Variable("prix_2Largeur",""+getDoub(prix_2Largeur.getText()),"PRINT",mode));
  param.add(new Variable("prix_3Largeur",""+getDoub(prix_3Largeur.getText()),"PRINT",mode));
  param.add(new Variable("prix_4Largeur",""+getDoub(prix_4Largeur.getText()),"PRINT",mode));
  param.add(new Variable("prix_1Dimension",""+getDoub(prix_1Dimension.getText()),"PRINT",mode));
  param.add(new Variable("prix_2Dimension",""+getDoub(prix_2Dimension.getText()),"PRINT",mode));
  param.add(new Variable("prix_3Dimension",""+getDoub(prix_3Dimension.getText()),"PRINT",mode));
  param.add(new Variable("prix_4Dimension",""+getDoub(prix_4Dimension.getText()),"PRINT",mode));
  param.add(new Variable("prix_1Centrage",""+getDoub(prix_1Centrage.getText()),"PRINT",mode));
  param.add(new Variable("prix_2Centrage",""+getDoub(prix_2Centrage.getText()),"PRINT",mode));
  param.add(new Variable("prix_3Centrage",""+getDoub(prix_3Centrage.getText()),"PRINT",mode));
  param.add(new Variable("prix_4Centrage",""+getDoub(prix_4Centrage.getText()),"PRINT",mode));
  param.add(new Variable("lengthPrix",""+getDoub(lengthPrix.getText()),"PRINT",mode));
  param.add(new Variable("marginPrix",""+getDoub(marginPrix.getText()),"PRINT",mode));
  param.add(new Variable("prixTaille",""+getDoub(prixTaille.getText()),"PRINT",mode));
  param.add(new Variable("prixBold",""+getDoub(prixBold.getText()),"PRINT",mode));
  param.add(new Variable("taillePrix",""+getDoub(taillePrix.getText()),"PRINT",mode));
  param.add(new Variable("boldPrix",""+getDoub(boldPrix.getText()),"PRINT",mode));
  param.add(new Variable("longeurPrix",""+getDoub(longeurPrix.getText()),"PRINT",mode));
  param.add(new Variable("footer_1Valeur",(footer_1Valeur.getText()),"PRINT",mode));
  param.add(new Variable("footer_2Valeur",(footer_2Valeur.getText()),"PRINT",mode));
  param.add(new Variable("footer_3Valeur",(footer_3Valeur.getText()),"PRINT",mode));
  param.add(new Variable("footer_4Valeur",(footer_4Valeur.getText()),"PRINT",mode));
  param.add(new Variable("footer_5Valeur",(footer_5Valeur.getText()),"PRINT",mode));
  param.add(new Variable("footer_6Valeur",(footer_6Valeur.getText()),"PRINT",mode));
  param.add(new Variable("footer_7Valeur",(footer_7Valeur.getText()),"PRINT",mode));
  param.add(new Variable("footer_8Valeur",(footer_8Valeur.getText()),"PRINT",mode));
  param.add(new Variable("footer_9Valeur",(footer_9Valeur.getText()),"PRINT",mode));
  param.add(new Variable("footer_10Valeur",(footer_10Valeur.getText()),"PRINT",mode));
  param.add(new Variable("footer_11Valeur",(footer_11Valeur.getText()),"PRINT",mode));
  param.add(new Variable("footer_12Valeur",(footer_12Valeur.getText()),"PRINT",mode));

  param.add(new Variable("footer_1Taille",""+getDoub(footer_1Taille.getText()),"PRINT",mode));
  param.add(new Variable("footer_2Taille",""+getDoub(footer_2Taille.getText()),"PRINT",mode));
  param.add(new Variable("footer_3Taille",""+getDoub(footer_3Taille.getText()),"PRINT",mode));
  param.add(new Variable("footer_4Taille",""+getDoub(footer_4Taille.getText()),"PRINT",mode));
  param.add(new Variable("footer_5Taille",""+getDoub(footer_5Taille.getText()),"PRINT",mode));
  param.add(new Variable("footer_6Taille",""+getDoub(footer_6Taille.getText()),"PRINT",mode));
  param.add(new Variable("footer_7Taille",""+getDoub(footer_7Taille.getText()),"PRINT",mode));
  param.add(new Variable("footer_8Taille",""+getDoub(footer_8Taille.getText()),"PRINT",mode));
  param.add(new Variable("footer_9Taille",""+getDoub(footer_9Taille.getText()),"PRINT",mode));
  param.add(new Variable("footer_10Taille",""+getDoub(footer_10Taille.getText()),"PRINT",mode));
  param.add(new Variable("footer_11Taille",""+getDoub(footer_11Taille.getText()),"PRINT",mode));
  param.add(new Variable("footer_12Taille",""+getDoub(footer_12Taille.getText()),"PRINT",mode));

  param.add(new Variable("footer_1X",""+getDoub(footer_1X.getText()),"PRINT",mode));
  param.add(new Variable("footer_2X",""+getDoub(footer_2X.getText()),"PRINT",mode));
  param.add(new Variable("footer_3X",""+getDoub(footer_3X.getText()),"PRINT",mode));
  param.add(new Variable("footer_4X",""+getDoub(footer_4X.getText()),"PRINT",mode));
  param.add(new Variable("footer_5X",""+getDoub(footer_5X.getText()),"PRINT",mode));
  param.add(new Variable("footer_6X",""+getDoub(footer_6X.getText()),"PRINT",mode));
  param.add(new Variable("footer_7X",""+getDoub(footer_7X.getText()),"PRINT",mode));
  param.add(new Variable("footer_8X",""+getDoub(footer_8X.getText()),"PRINT",mode));
  param.add(new Variable("footer_9X",""+getDoub(footer_9X.getText()),"PRINT",mode));
  param.add(new Variable("footer_10X",""+getDoub(footer_10X.getText()),"PRINT",mode));
  param.add(new Variable("footer_11X",""+getDoub(footer_11X.getText()),"PRINT",mode));
  param.add(new Variable("footer_12X",""+getDoub(footer_12X.getText()),"PRINT",mode));


  param.add(new Variable("footer_1Y",""+getDoub(footer_1Y.getText()),"PRINT",mode));
  param.add(new Variable("footer_2Y",""+getDoub(footer_2Y.getText()),"PRINT",mode));
  param.add(new Variable("footer_3Y",""+getDoub(footer_3Y.getText()),"PRINT",mode));
  param.add(new Variable("footer_4Y",""+getDoub(footer_4Y.getText()),"PRINT",mode));
  param.add(new Variable("footer_5Y",""+getDoub(footer_5Y.getText()),"PRINT",mode));
  param.add(new Variable("footer_6Y",""+getDoub(footer_6Y.getText()),"PRINT",mode));
  param.add(new Variable("footer_7Y",""+getDoub(footer_7Y.getText()),"PRINT",mode));
  param.add(new Variable("footer_8Y",""+getDoub(footer_8Y.getText()),"PRINT",mode));
  param.add(new Variable("footer_9Y",""+getDoub(footer_9Y.getText()),"PRINT",mode));
  param.add(new Variable("footer_10Y",""+getDoub(footer_10Y.getText()),"PRINT",mode));
  param.add(new Variable("footer_11Y",""+getDoub(footer_11Y.getText()),"PRINT",mode));
  param.add(new Variable("footer_12Y",""+getDoub(footer_12Y.getText()),"PRINT",mode));



  param.add(new Variable("footer_1Bold",""+getDoub(footer_1Bold.getText()),"PRINT",mode));
  param.add(new Variable("footer_2Bold",""+getDoub(footer_2Bold.getText()),"PRINT",mode));
  param.add(new Variable("footer_3Bold",""+getDoub(footer_3Bold.getText()),"PRINT",mode));
  param.add(new Variable("footer_4Bold",""+getDoub(footer_4Bold.getText()),"PRINT",mode));
  param.add(new Variable("footer_5Bold",""+getDoub(footer_5Bold.getText()),"PRINT",mode));
  param.add(new Variable("footer_6Bold",""+getDoub(footer_6Bold.getText()),"PRINT",mode));
  param.add(new Variable("footer_7Bold",""+getDoub(footer_7Bold.getText()),"PRINT",mode));
param.add(new Variable("footer_8Bold",""+getDoub(footer_8Bold.getText()),"PRINT",mode));
param.add(new Variable("footer_9Bold",""+getDoub(footer_9Bold.getText()),"PRINT",mode));
param.add(new Variable("footer_10Bold",""+getDoub(footer_10Bold.getText()),"PRINT",mode));
param.add(new Variable("footer_11Bold",""+getDoub(footer_11Bold.getText()),"PRINT",mode));
param.add(new Variable("footer_12Bold",""+getDoub(footer_12Bold.getText()),"PRINT",mode));

param.add(new Variable("prix_1Valeur",(prix_1Valeur.getText()),"PRINT",mode));
param.add(new Variable("prix_2Valeur",(prix_2Valeur.getText()),"PRINT",mode));
param.add(new Variable("prix_3Valeur",(prix_3Valeur.getText()),"PRINT",mode));
param.add(new Variable("prix_4Valeur",(prix_4Valeur.getText()),"PRINT",mode));
param.add(new Variable("ville",(ville.getText()),"PRINT",mode));
param.add(new Variable("clientLargeur",""+getDoub(clientLargeur.getText()),"PRINT",mode));
param.add(new Variable("clientLongeur",""+getDoub(clientLongeur.getText()),"PRINT",mode));
param.add(new Variable("entete1X",""+getDoub(entete1X.getText()),"PRINT",mode));
param.add(new Variable("entete2X",""+getDoub(entete2X.getText()),"PRINT",mode));
param.add(new Variable("ligne1X",""+getDoub(ligne1X.getText()),"PRINT",mode));
param.add(new Variable("ligne2X",""+getDoub(ligne2X.getText()),"PRINT",mode));
param.add(new Variable("referenceV", referenceV.getText() ,"PRINT",mode));
param.add(new Variable("factureV", factureV.getText() ,"PRINT",mode));
param.add(new Variable("marginfooter", marginfooter.getText() ,"PRINT",mode));
param.add(new Variable("lengthfooter", lengthfooter.getText() ,"PRINT",mode));

db.updateParameter1(param);
      
    }

    catch(Throwable ex)
    {
    JOptionPane.showMessageDialog(null,ex,db.l(db.lang,"V_IKIBAZO"),JOptionPane.PLAIN_MESSAGE);
    }}}});}
@Override
protected void processWindowEvent(WindowEvent evt)
{
   if(WindowEvent.WINDOW_CLOSING == evt.getID())
   {
      this.dispose();
   }
}
public static void main(String[] arghs)
	   {
       // new Map1("localhost","FACTURE");
       }
}
