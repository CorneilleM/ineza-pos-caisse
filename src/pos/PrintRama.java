/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pos;
import java.awt.*; 
import java.util.LinkedList;
import java.util.Properties;
import java.util.StringTokenizer;
import javax.swing.JFrame;

/**
 *-
 * @author Kimenyigu
 */
public class PrintRama extends JFrame {

LinkedList <Lot> lotList;
int pageNum = 0;
private Properties properties = new Properties();
int margin=0;
int numero;
double total,tva;
Client frss;
String date,reference,mode,agent,bon,medecin,poste,izina;
String [] footer;
Db db;

PrintRama(LinkedList <Lot> lotList,Db db,String mode ,String date,Client frss,
        int numero,int tva,double total,String [] footer,String quittance,String izina,boolean urihe)
{
   /*StringTokenizer st1=new StringTokenizer (quittance);
   bon=st1.nextToken("-");
   medecin=st1.nextToken("-");
   agent=st1.nextToken("-");
   poste=st1.nextToken("-");*/
    this.izina=izina;
    bon=quittance;
this.lotList=lotList;
this.db=db;
this.mode=mode;
this.margin=getValue("margin");
this.date=date;
this.frss=frss;
this.numero=numero;
this.tva=tva;
this.total=total;
this.footer=footer;
PrintCommand();

}
private void PrintCommand()
{
    PrintJob pjob = getToolkit().getPrintJob(this, "Ishyiga ", properties);
    if (pjob != null) {
    Graphics pg = pjob.getGraphics();
    if (pg != null) {

    printCommande(pjob, pg);

    pg.dispose();
    }
    pjob.end();
    }
}
private void printCommande (PrintJob pjob, Graphics pg) {


   if (!(pg instanceof PrintGraphics)) {
      throw new IllegalArgumentException ("Graphics context not PrintGraphics");
    }

    int pageH = pjob.getPageDimension().height - margin;
    int pageW = pjob.getPageDimension().width - margin;

  if (pg != null)
  {


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  HEADER
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
System.out.println("------------------------------------------------------- HEADER");

Font helv = new Font("Helvetica", Font.BOLD, getValue("dateT"));
pg.drawString(getString("ville")+", le " + date.substring(0, 2) + "/" + date.substring(2, 4) + "/" + date.substring(4, 6)+"  à Kigali " , pageW - getValue("dateX"), getValue("dateY"));
helv = new Font("Helvetica", Font.BOLD, getValue("adresseT"));
pg.setFont(helv);
FontMetrics fm = pg.getFontMetrics(helv);
int fontHeight = fm.getHeight();
pg.drawString(getString("adresseV"), getValue("adresseX"),  getValue("adresseY"));
pg.drawString(getString("rueV"), getValue("rueX"),  getValue("rueY"));
pg.drawString(getString("localiteV")+bon, getValue("localiteX"),  getValue("localiteY"));
pg.drawLine(getValue("localiteX"), getValue("localiteY")+2, getValue("localiteX")+80, getValue("localiteY")+2);
pg.drawString(getString("telV"), getValue("telX"), getValue("telY"));
//pg.drawString(getString("emailV")+medecin, getValue("emailX"), getValue("emailY"));
// pg.drawString(getString("tvaV")+poste, getValue("tvaX"),  getValue("tvaY"));
Font helv2 = new Font("Helvetica", Font.BOLD, getValue("clientT"));
pg.setFont(helv2);
pg.drawString("N° Affiliation : "+frss.NUM_AFFILIATION, getValue("clientX") + 10,  getValue("clientY") + 10);
pg.drawString("Adhérent     :   "+frss.NOM_CLIENT+"  "+frss.PRENOM_CLIENT, getValue("clientX" ) + 10, getValue("clientY" ) + 25);
pg.drawString("Bénéficiaire :  "+frss.BENEFICIAIRE+" "+frss.LIEN+"      Age : "+frss.AGE, getValue("clientX" ) + 10,  getValue("clientY" ) + 40);
pg.drawString("Departement :  "+frss.EMPLOYEUR, getValue("clientX") + 300,  getValue("clientY") + 10);
pg.drawString("Lieu d'Affectation :  "+frss.SECTEUR, getValue("clientX") + 300,  getValue("clientY") + 25);
Font helv1 = new Font("Helvetica", Font.BOLD, getValue("factureT"));
pg.setFont(helv1);
pg.drawString(getString("factureV") , getValue("factureX"), getValue("factureY"));
helv2 = new Font("Helvetica", Font.PLAIN, getValue("referenceT"));
pg.setFont(helv2);
pg.drawString(getString("referenceV")+izina+" "+  numero, getValue("referenceX"), getValue("referenceY"));
helv1 = new Font("Helvetica", Font.BOLD, 10);
pg.setFont(helv1);
helv2 = new Font("Helvetica", Font.BOLD, 10);
pg.setFont(helv2);

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  ENTETE
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
        System.out.println("------------------------------------------------------- ENTETE");
            int g = getValue("length");
            String[] entete = new String [g];
            int[] longeur = new int[g+1];
            String[] variable= new String [g];
            int [] dimension=new int[g];
            longeur[0]=0;

            int page=1;
            int y=  getValue("entete"+page+"X");
            for(int k=0;k<g;k++)
            {
            entete[k]=getString("entete_"+(k+1)+"Valeur");
            longeur[k+1]=getValue("entete_"+(k+1)+"Largeur");
            variable[k]=getString("entete_"+(k+1)+"Variable");
            dimension[k]=getValue("entete_"+(k+1)+"Dimension");
 // System.out.println(" d   "   +getValue("entete_"+(k+1)+"Dimension"));
            }
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  BODY
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
System.out.println("------------------------------------------------------- BODY");
            int w = margin;
            int curHeightLigne = getValue("ligne"+page+"X");
            Font helvLot = new Font("Helvetica", Font.PLAIN, 10);
            pg.setFont(helvLot);
            FontMetrics fmLot = pg.getFontMetrics(helvLot);
            fontHeight = fmLot.getHeight();
            boolean done = true;
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////            IMITI
for (int j = 0; j < lotList.size(); j++) {

            helvLot = new Font("Helvetica", Font.PLAIN, getValue("tailleLigne"));
            pg.setFont(helvLot);
            Lot ci = lotList.get(j);
            w = margin;
//NIBA IZINA RYABAYE RIRERIRE
if (done) {
curHeightLigne += fontHeight;
} else {
curHeightLigne += 2 * fontHeight;
}
done = true;
    // UMUTI NIBICIRO
    for (int i = 0; i < g; i++)
    {
    w += (longeur[i]);
    String valeur=ci.vars[i];
    String variab=variable[i];
    int dim      =dimension[i];
    if(variab.equals("int"))
    printInt(pg,setVirgule(valeur), w + (longeur[i + 1]) - 10, curHeightLigne);
    else  if(variab.equals("double"))
    printInt(pg,setVirguleD(valeur), w + (longeur[i + 1]) - 10, curHeightLigne);
    else if (valeur.length() < dim)
    {
    pg.drawString(valeur, w + 5, curHeightLigne);
    }
    else
    {
    done = false;
    pg.drawString(valeur.substring(0, dim - 1)+".", w + 5, curHeightLigne);
    pg.drawString(valeur.substring(dim - 1, valeur.length()), w + 5, fontHeight + curHeightLigne);
    }
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////
//DISPLAY GRAPHICH
///////////////////////////////////////////////////////////////////////////////////////////////////////////
System.out.println(getValue("longeurLigne")+"------------------------------------------------------- DISPLAY GRAPHICH");

            int hauteur = pageH - getValue("longeurLigne");
            helv1 = new Font("Helvetica", Font.BOLD, getValue("enteteTaille"));
            pg.setFont(helv1);
            w = margin;
            if(pageNum>1)
                page=2;
            y=getValue("entete"+page+"X");
            for (int i = 0; i < g; i++) {
            w += (longeur[i]);
            pg.drawLine(w,y, w, hauteur);//ligne zihagaze
            pg.drawString(entete[i], w + 2, y + 15);// amazina
            }
            pg.drawLine(margin, hauteur, w, hauteur);//umurongo wo hasi
            pg.drawRect(margin, y , w - margin, 25); // cadre ya entete

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//PRIX
///////////////////////////////////////////////////////////////////////////////////////////////////////////
System.out.println( "------------------------------------------------------- PRIX");

w = margin;
helv2 = new Font("Helvetica", Font.BOLD, getValue("prixTaille"));
pg.setFont(helv2);
int lp = getValue("lengthPrix");
String[] enteteP = new String [lp+1];
int[] longeurP = new int[lp+1];
String[] variableP= new String [lp];
longeurP[0]=0;

    for(int k=0;k<lp;k++)
    {
    enteteP[k]=getString("prix_"+(k+1)+"Valeur");
    if(enteteP[k].length()>2)
    enteteP[k]=enteteP[k]+" FRW";
    longeurP[k+1]=getValue("prix_"+(k+1)+"Largeur");
    variableP[k]=getString("prix_"+(k+1)+"Variable");
    System.out.println(longeurP[k+1]);
    }
    int Xprix = getValue("longeurPrix");
    int marginP=getValue("marginPrix");
    w =marginP ;
    for (int i = 0; i < lp; i++) {
    w += (longeurP[i]);
    int wid=w + (longeurP[i + 1]) - 20;
    if(i==0)
    printInt(pg,setVirgule( (int)total),  wid, Xprix+15);
    if(i==1)
    printInt(pg,setVirgule((int)tva),  wid, Xprix+15);
    if(i==2)
    printInt(pg,setVirgule( (int)(total-tva)),  wid, Xprix+15);

    pg.drawLine(w,Xprix-20, w, Xprix+20);//ligne zihagaze
    pg.drawString(enteteP[i], w + 2, Xprix - 5);// amazina
    }
     if(lp!=0)
    {
    pg.drawLine(marginP, Xprix, w, Xprix);//umurongo wo hasi
    pg.drawRect(marginP, Xprix-20 , w-marginP , 40); // cadre ya entete

    }
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//FOOTER
///////////////////////////////////////////////////////////////////////////////////////////////////////////
System.out.println( "------------------------------------------------------- FOOTER");
    for(int v=1;v<getValue("lengthfooter");v++)
    { helv2 = new Font("Helvetica", Font.BOLD, getValue("footer_"+v+"Taille" ) );
    pg.setFont(helv2);
    pg.drawString(getString("footer_"+v+"Valeur")+  " "+footer[v], getValue("footer_"+v+"X" ), getValue("footer_"+v+"Y" ));
    }
    helv2 = new Font("Helvetica", Font.BOLD, 8 );
    pg.setFont(helv2);

    int mutX=getValue("faxX");
    int mutY=getValue("faxY");
    pg.drawRect(mutX, mutY-12, getValue("clientLongeur"), getValue("clientLargeur"));

    pg.drawString("          « Spécialité » ifite « Générique » :",mutX, mutY);
    pg.drawString("     Iyo umurwayi ahisemo gufata « Spécialité » mu",mutX, mutY+10);
    pg.drawString("   mwanya wa  « Générique », RAMA yishyura ikurikije ",mutX, mutY+20);
    pg.drawString("                 Igiciro cya « Générique » gusa",mutX, mutY+30);

    pg.drawRect(mutX+370, mutY-12, getValue("clientLongeur")-30, getValue("clientLargeur"));



  }

}

int getValue(String s)
{

int ret=0;
Variable var= db.getParam("PRINT",mode,s+"R"+mode);
 if(var!=null)
 {
Double val=new Double (var.value);
ret = val.intValue();
}

return ret;
}
String getString(String s)
{
String ret=" ";
 Variable var= db.getParam("PRINT",mode,s+"R"+mode);
 if(var!=null)
     ret=var.value;
     return ret;
}
public void printInt(Graphics pg,int nbre,int w,int h)
  {
  String s=setVirgule(nbre);

  int back=0;

  for(int i=s.length()-1;i>=0;i--)
  {
      if(s.charAt(i)==' ')
      back+=2;
      else
       back+=5;

      pg.drawString(""+s.charAt(i),w-back,h);

  }
}
public void printInt(Graphics pg,String s,int w,int h)
  {

  int back=0;

  for(int i=s.length()-1;i>=0;i--)
  {
      if(s.charAt(i)==' ')
      back+=2;
      else
       back+=5;


      pg.drawString(""+s.charAt(i),w-back,h);

  }




  }

String setVirgule(int frw)
        {

String setString = ""+frw;
int l =setString.length();
if(l<2)
   setString = "    "+frw;
else if(l<3)
   setString = "  "+frw;
else if(l<4)
   setString = "  "+frw;

String vir=".";

if(l>3 && l<=6)
setString= setString.substring(0,l-3)+vir+setString.substring(l-3) ;
if(l>6)
{

String s1 = setString.substring(0,l-3);
int sl=s1.length();
setString=s1.substring(0,sl-3)+vir+s1.substring(sl-3)+vir+setString.substring(l-3)  ;

}

return setString;
}
String setVirgule(String setString)
        {


int l =setString.length();



if(l>3 && l<=6)
setString= setString.substring(0,l-3)+" "+setString.substring(l-3) ;
if(l>6)
{

String s1 = setString.substring(0,l-3);
int sl=s1.length();
setString=s1.substring(0,sl-3)+" "+s1.substring(sl-3)+" "+setString.substring(l-3)  ;

}

return setString;
}
  String setVirguleD(String frw)
        {

StringTokenizer st1=new StringTokenizer (frw);

    String entier =st1.nextToken(".");
System.out.println(frw);
    String decimal=st1.nextToken("").replace(".", "");

    if(decimal.length()==1 && decimal.equals("0") )
    decimal=".00";
    else if (decimal.length()==1 && !decimal.equals("0") )
          decimal="."+decimal+"0";
    else
    decimal="."+decimal.substring(0, 2);

String setString = entier+decimal;

int l =setString.length();
if(l<2)
   setString = "    "+frw;
else if(l<3)
   setString = "  "+frw;
else if(l<4)
   setString = "  "+frw;

int up=6;
int ju=up+3;
if(l>up && l<=ju)
{
    setString= setString.substring(0,l-up)+","+setString.substring(l-up) ;
}
else if(l>ju)
{
setString=setString.substring(0,l-9)+","+setString.substring(l-9,l-6)+","+setString.substring(l-6,l)  ;

}

return setString;
}

public static void main(String[] arghs)
{
    //new PrintDoc();
}




}
