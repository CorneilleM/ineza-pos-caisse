/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pos;
import java.awt.*;
import java.awt.event.*;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import javax.swing.text.MaskFormatter;
/**
 *
 * @author aimable
 */
public class PersonInterface extends JFrame{

private  JTextField
                    NUM_AFFILIATION,
                    /*CODE,*/
                    NOM_CLIENT,
                    PRENOM_CLIENT,
                    PERCENTAGE,
                    DATE_EXP,
                    EMPLOYEUR,
                    SECTEUR,SEXE,NAISSANCE ;
//ASSURANCE,

private JLabel      SEXEL,NAISSANCEL,
                    NUM_AFFILIE,
                    NUM_AFFILIATIONL,
                    CODEL,
                    NOM_CLIENTL,
                    PRENOM_CLIENTL,
                    PERCENTAGEL,
                    DATE_EXPL,
                    ASSURANCEL,
                    EMPLOYEURL,
                    SECTEURL,
                    LIEN,
                    NUML;

JList sexeList=new JList(new String[]{"MALE","FEMALE"});
private JButton send;
private Db db;
private Client c ;
Font police;


private  JList  societeJList=new JList();
JList  lienList=new JList(new String  [] {"LUI/ELLE MEME","CONJOINT(E)","ENFANT","","",""});
String soc,what;

PersonInterface( Db db, String nom ,String prenom ,String nAff,String naissance  ,
        String sexe,String exp,String departement,int percentage,String assurance)
{
this.db=db;
police = new Font(db.getString("font"), Integer.parseInt(db.getString("style")),
        Integer.parseInt(db.getString("length")));
what="";
//if(code.equals(""))
String code=""+(Integer.parseInt(db.getParama("RUN" , "compteurClient").value)+1);
System.out.println("code:"+code);
if(exp.length()==5) {
        exp="0"+exp;
    }
makeFrame(departement,nAff,  nom ,  prenom ,  code,  naissance  ,  sexe,  exp,percentage,  assurance);
// makeFrame(stock,nAff);
send();
draw();
lienList.setSelectedIndex(0); 
}
PersonInterface(Db db,Client c)
{
    police = new Font(db.getString("font"), Integer.parseInt(db.getString("style")),Integer.parseInt(db.getString("length")));
    what="UPDATE";
    this.db=db;
    this.c=c;
 makeFrame(c.ASSURANCE,c.NUM_AFFILIATION,  c.NOM_CLIENT ,  c.PRENOM_CLIENT ,  c.CODE,  c.AGE  , 
         c.SEXE,"",c.PERCENTAGE,c.ASSURANCE);
    send();
    draw();


}
private void makeFrame(String stock,String nA,String nom ,String prenom ,String code,String naissance
        ,String sexe,String exp,int percentage,String assurance )
{
    //this.CODE= new JTextField(); CODE.setText(code);  
    this.NUML=new JLabel();NUML.setText(code);
    this.NUM_AFFILIE=new JLabel();
    this.NUM_AFFILIATION= new JTextField(nA);
    this.NOM_CLIENT= new JTextField();NOM_CLIENT.setText(nom);
    this.PRENOM_CLIENT= new JTextField();PRENOM_CLIENT.setText(prenom);
    this.SEXE= new JTextField();SEXE.setText(sexe);
    this.sexeList.setSelectedValue(sexe, true);
    this.NAISSANCE= new JTextField();NAISSANCE.setText(naissance);
    
    try {
    MaskFormatter date = new MaskFormatter("##-##-####");
    NAISSANCE = new JFormattedTextField(date);
    police = new Font("Consolas", Font.BOLD, 14);
    NAISSANCE.setFont(police);
    NAISSANCE.setForeground(Color.BLUE); 
    NAISSANCE.setBackground(new Color(175,215,255));
    }
    catch (ParseException e) {
    e.printStackTrace();
    }
    
    
    this.PERCENTAGE= new JTextField(""+percentage);
    this.DATE_EXP= new JTextField();
    if(exp.length()>3) {
        DATE_EXP.setText(exp);
    }
    else {
        DATE_EXP.setText("311217");
    }
    this.EMPLOYEUR= new JTextField();
    this.EMPLOYEUR.setText(stock);
    this.SECTEUR = new JTextField();
    this.CODEL=new JLabel(db. l(db.lang,  "V_CODE"));
    CODEL.setFont(police);
    NUML.setFont(police); 
    this.NUM_AFFILIATIONL=new JLabel(db.l(db.lang, "V_NUMAFFILIATION")+":");
    NUM_AFFILIATION.setFont(police);
    NUM_AFFILIATIONL.setFont(police);
    this.NOM_CLIENTL=new JLabel(db.l(db.lang, "V_ADHERENT")+":");
    NOM_CLIENT.setFont(police);
    NOM_CLIENTL.setFont(police);
    this.PRENOM_CLIENTL=new JLabel(db.l(db.lang, "V_PRENOM")+":");
    PRENOM_CLIENT.setFont(police);
    PRENOM_CLIENTL.setFont(police);
    this.PERCENTAGEL=new JLabel(db.l(db.lang, "V_PERCENTAGE")+":");
    PERCENTAGE.setFont(police);
    PERCENTAGEL.setFont(police);
    this.DATE_EXPL=new JLabel(db. l(db.lang,  "V_DATEEXP")+":");
    DATE_EXP.setFont(police);
    DATE_EXPL.setFont(police);
    this.ASSURANCEL=new JLabel(db. l(db.lang,  "V_ASSURANCE")+":");
    ASSURANCEL.setFont(police);
    this.EMPLOYEURL=new JLabel(db. l(db.lang,  "V_DPT")+":");
    EMPLOYEUR.setFont(police);
    EMPLOYEURL.setFont(police);
    this.SECTEURL=new JLabel(db. l(db.lang,  "V_TEL")+":");
    SECTEUR.setFont(police);
    SECTEURL.setFont(police);
    this.SEXEL=new JLabel(db. l(db.lang,  "V_SEXE")+":");
    SEXEL.setFont(police);
    sexeList.setBackground(Color.PINK);
    sexeList.setFont(police); 
    this.NAISSANCEL=new JLabel(db. l(db.lang,  "V_DATENAISSANCE")+":");
    NAISSANCE.setFont(police);
    NAISSANCEL.setFont(police);
    this.LIEN=new JLabel(db. l(db.lang,  "V_LIEN")+":");
    LIEN.setFont(police);
    this.send=new JButton("ENVOYER");
    soc=stock;  
    String [] control= db.clients();
    societeJList.setListData(control);
    societeJList.setSelectedValue(assurance,true);
    
    Variable v=db.getParama("CLIENT", assurance);
    PERCENTAGE.setText(v.value2);
    this.ASSURANCEL.setText("ASSURANCE:"+societeJList.getSelectedValue());
    
//    int ind=societeJList.getFirstVisibleIndex();
//    societeJList.ensureIndexIsVisible(ind);
//    System.out.println(ind+"selected::::::::::::::::::::::::"+societeJList.getSelectedIndex());
//    societeJList.scrollRectToVisible(societeJList.getCellBounds(ind,ind+1));
    
    //societeJList.ensureIndexIsVisible(societeJList.getse);
}

private void draw()
{
    setTitle("Register a new Client for Ishyiga");
    Image im = Toolkit.getDefaultToolkit().getImage("logo.png");
    this.setIconImage(im); 

    this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);


    //on fixe  les composant del'interface
    Container content = getContentPane();


    if(what.equals("UPDATE")) {
        UpDatePane(content);
    }
        else {
        westPane(content);
    }

     setSize(600,600);
     setVisible(true);

}
public  int getIn (String in)
{
                      int productCode ;
    try
    {
        if(in.equals(""))
        {

JOptionPane.showMessageDialog(null,db. l(db.lang, "V_INVALIDPERCENTAGE"),in,JOptionPane.PLAIN_MESSAGE);        productCode=-1;
        }
        else
        {
        Integer in_int=new Integer(in);
        productCode = in_int.intValue();
        if(productCode<0 || productCode>100)
             {

JOptionPane.showMessageDialog(null, db. l(db.lang, "V_IKIBAZO")+" "+db. l(db.lang, "V_PERCENTAGE"),""+productCode,JOptionPane.PLAIN_MESSAGE);
        productCode=-1;
        }


        }
         }
    catch (NumberFormatException e)
    {
     JOptionPane.showMessageDialog(null,in,db. l(db.lang, "V_NOTPERCENTAGE"),JOptionPane.PLAIN_MESSAGE);

    productCode=-1;
    }
    return productCode;
}



/**************************************************************************
affiche du stock
**************************************************************************/

public void westPane(Container content )
{
JPanel Pane  = new JPanel();

Pane.setPreferredSize(new Dimension (600,700));

JPanel panel = new JPanel();
panel.setLayout(new GridLayout(12,2));
panel.setPreferredSize(new Dimension (400,500));


  panel.add(CODEL);
  //panel.add(CODE);
  panel.add(NUML); 
// if(!soc.equals("CASHREMISE"))   {

 //}

panel.add(NOM_CLIENTL);panel.add(NOM_CLIENT);
panel.add(PRENOM_CLIENTL);panel.add(PRENOM_CLIENT);
//JScrollPane S =new JScrollPane(lienList);
//S.setPreferredSize( new Dimension (40,40) );

//societeJList.setBackground(Color.pink);
//panel.add(S);

//panel.add(LIEN);panel.add(S);
panel.add(NAISSANCEL);panel.add(NAISSANCE);
panel.add(SEXEL);panel.add(sexeList);
panel.add(SECTEURL);panel.add(SECTEUR);
panel.add(ASSURANCEL);

 JScrollPane    scrollPaneProductList =new JScrollPane(societeJList);
  scrollPaneProductList.setPreferredSize(new Dimension (40,40));
  societeJList.setBackground(Color.pink);
 panel.add(scrollPaneProductList);
  panel.add(NUM_AFFILIATIONL);panel.add(NUM_AFFILIATION);
 panel.add(PERCENTAGEL);panel.add(PERCENTAGE);
 panel.add(DATE_EXPL);panel.add(DATE_EXP);
panel.add(EMPLOYEURL);panel.add(EMPLOYEUR);



Pane.add(panel,BorderLayout.CENTER);
Pane.add(send,BorderLayout.SOUTH);

content.add(Pane, BorderLayout.WEST);

}
public void UpDatePane(Container content )
{
JPanel Pane  = new JPanel();

Pane.setPreferredSize(new Dimension (600,700));

JPanel panel = new JPanel();
panel.setLayout(new GridLayout(11,2));
panel.setPreferredSize(new Dimension (400,500));
//CODE.setText(c.CODE);
NUML.setText(c.CODE);
NUM_AFFILIE.setText(c.NUM_AFFILIATION);
NOM_CLIENT.setText(c.NOM_CLIENT);
PRENOM_CLIENT.setText(c.PRENOM_CLIENT);
PERCENTAGE.setText(""+c.PERCENTAGE);
DATE_EXP.setText("");
//  ASSURANCE.setText(c.ASSURANCE);
 EMPLOYEUR.setText(c.EMPLOYEUR);
SECTEUR.setText(c.SECTEUR);
NAISSANCE.setText(c.AGE);
SEXE.setText(c.SEXE);
sexeList.setSelectedValue(c.SEXE, true);

     panel.add(NUM_AFFILIATIONL);panel.add(NUM_AFFILIE);
 //}
 panel.add(CODEL);panel.add(new JLabel(c.CODE));
panel.add(NOM_CLIENTL);panel.add(NOM_CLIENT);
panel.add(PRENOM_CLIENTL);panel.add(PRENOM_CLIENT);

panel.add(NAISSANCEL);panel.add(NAISSANCE);
panel.add(SEXEL);panel.add(sexeList);
panel.add(PERCENTAGEL);panel.add(PERCENTAGE);
panel.add(DATE_EXPL);panel.add(DATE_EXP);

panel.add(ASSURANCEL);

JScrollPane    scrollPaneProductList =new JScrollPane(societeJList);
scrollPaneProductList.setPreferredSize(new Dimension (40,40));
societeJList.setSelectedValue(c.ASSURANCE,true);
societeJList.setBackground(Color.pink);
panel.add(scrollPaneProductList);



panel.add(EMPLOYEURL);panel.add(EMPLOYEUR);
panel.add(SECTEURL);panel.add(SECTEUR);

send.setText("UPDATE");
Pane.add(panel,BorderLayout.CENTER);

Pane.add(send,BorderLayout.SOUTH);

content.add(Pane, BorderLayout.WEST);

}





/**********************************************************************************************
AJOUT Mannuel
**********************************************************************************************/
private void send()
{

	send.addActionListener(new ActionListener() {
            @Override
	public void actionPerformed(ActionEvent ae)
	{
    if(ae.getSource()==send && (societeJList.getSelectedValue()!= null || soc.equals("SOCIETE")  ))
    {

        String DBB="CLIENT";
        String NUM_AFFILIATIONa;
        String NAISS=NAISSANCE.getText();
        String GENDER="" + sexeList.getSelectedValue(); 
        if(what.equals("UPDATE")) {
            NUM_AFFILIATIONa    =  NUM_AFFILIE.getText().toUpperCase();
        }  
        else {
            NUM_AFFILIATIONa    =  NUM_AFFILIATION.getText().toUpperCase();
        } 
            
    
    String nAff="'"+NUM_AFFILIATIONa+"'";
    
    System.out.println("num d'affiliation:"+NUM_AFFILIATIONa);

    String NOM_CLIENTa =  NOM_CLIENT.getText().toUpperCase();
    String PRENOM_CLIENTa   =  PRENOM_CLIENT.getText().toUpperCase();

    int PERCENTAGEa  =getIn (PERCENTAGE.getText());
    if(societeJList.getSelectedValue().equals("RAMA"))
    {    PERCENTAGEa=15;DBB="CLIENT_RAMA";}

    String datedoc=DATE_EXP.getText();

    int dated ;
    String  ASSURANCEa   ;
    ASSURANCEa= ((String)societeJList.getSelectedValue()).toUpperCase();
    
     System.out.println("ASSURANCE:"+ASSURANCEa);

    String EMPLOYEURa =EMPLOYEUR.getText().toUpperCase();
    String  SECTEURa  = SECTEUR.getText().toUpperCase();
    try
    {
    if( (datedoc.length()==6))
    {
    String createString1=""+datedoc.substring(4)+""+datedoc.substring(2, 4)+""+datedoc.substring(0,2);
    Integer hhh=new Integer (createString1);
    dated = hhh.intValue();

     int DATE_EXPa  =dated;
     if(soc.equals("CASHREMISE"))
     {DBB="CLIENT_UNIPHARMA";  }

        if(!(PERCENTAGEa==-1)) {
            if(!NUM_AFFILIATIONa.equals("") || soc.equals("CASHREMISE")) {
                if(!NOM_CLIENTa.equals("")&& !PRENOM_CLIENTa.equals("") && GENDER!=null && !GENDER.equals("null")) {
                    if(!ASSURANCEa.equals("") && !NAISS.contains(" ") )
                    {
                         if(what.equals("UPDATE"))
                         {
                             db.upDateClient(new Client(NUM_AFFILIATIONa,NOM_CLIENTa ,PRENOM_CLIENTa ,PERCENTAGEa,DATE_EXPa,ASSURANCEa ,EMPLOYEURa,SECTEURa,"",NUML.getText(),NAISSANCE.getText(),""+sexeList.getSelectedValue(),"",""),DBB,NUM_AFFILIE.getText());
                             db.insertClienttracking(c);                         
                         } 
                          else
                         {
                             db.insertClient( new Client(NUM_AFFILIATIONa,NOM_CLIENTa ,PRENOM_CLIENTa ,PERCENTAGEa,DATE_EXPa,ASSURANCEa ,EMPLOYEURa,SECTEURa,"",NUML.getText(),NAISSANCE.getText(),""+sexeList.getSelectedValue(),"",""));
                         }
                    }
                    else {
                        JOptionPane.showMessageDialog(null,db.l(db.lang, "V_IKIBAZO")+" "+db. l(db.lang, "V_DANS")+" "+db. l(db.lang, "V_INYANDIKO"),db. l(db.lang, "V_IKIBAZO"),JOptionPane.WARNING_MESSAGE);
                    }
                }
                 else {
                    JOptionPane.showMessageDialog(null,db. l(db.lang, "V_IKIBAZO")+" "+db. l(db.lang, "V_DANS")+" "+db. l(db.lang, "V_INYANDIKO"),db. l(db.lang, "V_DESIGNATION"),JOptionPane.WARNING_MESSAGE);
                }
            }
             else {
                JOptionPane.showMessageDialog(null,db. l(db.lang, "V_IKIBAZO")+" "+db. l(db.lang, "V_DANS")+" "+db. l(db.lang, "V_INYANDIKO"),db. l(db.lang, "V_NUMAFFILIATION"),JOptionPane.WARNING_MESSAGE);
            }
        }

    } else {
            JOptionPane.showMessageDialog(null,datedoc.length()+" "+db. l(db.lang, "V_FORMAT")+"   311215  ",db. l(db.lang, "V_DATE"),JOptionPane.WARNING_MESSAGE);
        }


    }

    catch(Throwable ex)
    {
   JOptionPane.showMessageDialog(null,ex,db. l(db.lang, "V_IKIBAZO"),JOptionPane.WARNING_MESSAGE);
    }
    }
			}});

}
 @Override
protected void processWindowEvent(WindowEvent evt)
{
   if(WindowEvent.WINDOW_CLOSING == evt.getID())
   {
	   this.dispose();
			//default icon, custom title
//JOptionPane.showMessageDialog(this,"RIRAFUNGIRA RIMWE N'ISHYIGA"," IKIBAZO",JOptionPane.PLAIN_MESSAGE);
        			//this.setDefaultCloseOperation(HIDE_ON_CLOSE);


	}

}


public static void main(String[] arghs)
{
        try {
            StartServer d = new StartServer();
            Db db = new Db("localhost","ishyiga");
            db.read();

            //new PersonInterface(db, "", "RAMA");

        } catch (Throwable ex) {
            Logger.getLogger(PersonInterface.class.getName()).log(Level.SEVERE, null, ex);
        }

}
}
