/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pos;
import java.awt.*;
import java.awt.event.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import javax.swing.JFrame;
/**
 *
 * @author aimable
 */
public class EmployeInterface extends JFrame{


private  JTextField
        ID_EMPLOYE ,
	NOM_EMPLOYE  ,
	PRENOM_EMPLOYE  ,
	BP_EMPLOYE ,
	CATRE_INDETITE ,
	LEVEL_EMPLOYE  ,
	TITRE_EMPLOYE  ,
	EMAIL,
	TEL_EMPLOYE ;
//PWD_EMPLOYE  ,


private JLabel
        LID_EMPLOYE ,
	LNOM_EMPLOYE  ,
	LPRENOM_EMPLOYE  ,
	LBP_EMPLOYE ,
	LCATRE_INDETITE ,
	LLEVEL_EMPLOYE  ,
	LTITRE_EMPLOYE  ,
        EMAILL,
	LTEL_EMPLOYE,LLANGUE  ;

//	LPWD_EMPLOYE  ,

private JList LANGUE;

private JButton send;
private Db db;
private Employe c ;
Font police;
JCheckBox  ANNULE,CHANGE,TICKETCAISSE,EMPLOYE,FACTURE,CREDIT,CAISSE,RAPPORT
        ,REDUCTION,CMD,MVT,MVS,NAME_INV,ZAMURA,INVENTAIRE,ITEM,DATA,OPTIONS;
JCheckBox PERIME_SENDER,PERIME;


String  what;

EmployeInterface( Db db )
{
this.db=db;
what=""; 
this.c=new Employe(db.maxEmp(),0,0,0,"","","","","","","","");
 makeFrame();
    send();
    draw(); 

}
EmployeInterface(Db db,Employe c)
{
    what="UPDATE";
    this.db=db;
    this.c=c;
 makeFrame();
    send();
    draw();


}

public void makeFrame()
{
    police = new Font(db.getString("font"), Integer.parseInt(db.getString("style")),Integer.parseInt(db.getString("length")));


        this.ID_EMPLOYE = new JTextField();
	this.NOM_EMPLOYE  = new JTextField();
	this.PRENOM_EMPLOYE  = new JTextField();
	this.BP_EMPLOYE = new JTextField();
	this.CATRE_INDETITE = new JTextField();
	this.LEVEL_EMPLOYE  = new JTextField();
	this.TITRE_EMPLOYE  = new JTextField();
        this.EMAIL  = new JTextField();
////	this.PWD_EMPLOYE  = new JTextField();
	this.TEL_EMPLOYE  = new JTextField();
        this.LANGUE=new JList(new String  [] {"KINYARWANDA","FRANCAIS","ENGLISH"});

        ID_EMPLOYE.setText(""+c.ID_EMPLOYE);
        ID_EMPLOYE.setFont(police);
	NOM_EMPLOYE.setText(c.NOM_EMPLOYE);
        NOM_EMPLOYE.setFont(police);
	PRENOM_EMPLOYE.setText(c.PRENOM_EMPLOYE);
        PRENOM_EMPLOYE.setFont(police);
	BP_EMPLOYE.setText(c.BP_EMPLOYE);
        BP_EMPLOYE.setFont(police);
	CATRE_INDETITE.setText(""+c.CATRE_INDETITE);
        CATRE_INDETITE.setFont(police);
	LEVEL_EMPLOYE.setText(""+c.LEVEL_EMPLOYE);
        LEVEL_EMPLOYE.setFont(police);
	TITRE_EMPLOYE.setText(c.TITRE_EMPLOYE);
        TITRE_EMPLOYE.setFont(police);
        EMAIL.setText(c.EMAIL);
        EMAIL.setFont(police);
//	PWD_EMPLOYE.setText(c.PWD_EMPLOYE);
//        PWD_EMPLOYE.setFont(police);
	TEL_EMPLOYE.setText(""+c.TEL_EMPLOYE);
        TEL_EMPLOYE.setFont(police);
        LANGUE.setSelectedValue(change(c.LANGUE,false), true);
        LANGUE.setFont(police);

        this.LID_EMPLOYE=new JLabel(db.l(db.lang, "V_NUMERO")+" "+db.l(db.lang, "V_DEL")+" "+db.l(db.lang, "V_EMPLOYE"));
        LID_EMPLOYE.setFont(police);
	this.LNOM_EMPLOYE =new JLabel(db.l(db.lang, "V_NOM"));
        LNOM_EMPLOYE.setFont(police);
	this.LPRENOM_EMPLOYE =new JLabel(db.l(db.lang, "V_PRENOME"));
        LPRENOM_EMPLOYE.setFont(police);
	this.LBP_EMPLOYE=new JLabel(db.l(db.lang, "V_ADDRESSE")); 
        LBP_EMPLOYE.setFont(police);
	this.LCATRE_INDETITE=new JLabel(db.l(db.lang, "V_PWD"));
        LCATRE_INDETITE.setFont(police);
        this.EMAILL=new JLabel("EMAIL");
        EMAILL.setFont(police);
	this.LLEVEL_EMPLOYE =new JLabel(db.l(db.lang, "V_LEVEL")+" "+db.l(db.lang, "V_DEL")+" "+db.l(db.lang, "V_EMPLOYE"));
        LLEVEL_EMPLOYE.setFont(police);
	this.LTITRE_EMPLOYE =new JLabel(db.l(db.lang, "V_TITRE")+" "+db.l(db.lang, "V_DEL")+" "+db.l(db.lang, "V_EMPLOYE"));
        LTITRE_EMPLOYE.setFont(police);
//	this.LPWD_EMPLOYE =new JLabel(db.l(db.lang, "V_ACCESS")+" "+db.l(db.lang, "V_EMPLOYE"));
//        LPWD_EMPLOYE.setFont(police);
	this.LTEL_EMPLOYE=new JLabel(db.l(db.lang, "V_TEL")+" "+db.l(db.lang, "V_DEL")+" "+db.l(db.lang, "V_EMPLOYE"));
        LTEL_EMPLOYE.setFont(police);
        this.LLANGUE=new JLabel(db.l(db.lang, "V_LANGUE"));
        LLANGUE.setFont(police);
         this.send=new JButton("ENVOYER");
         
         
         
         this.ANNULE=new JCheckBox("ANNULE");
         if(c.PWD_EMPLOYE.contains("ANNULE"))
             ANNULE.setSelected(true);
         
         this.CHANGE=new JCheckBox("CHANGER_PRIX");
         if(c.PWD_EMPLOYE.contains("CHANGER_PRIX"))
             CHANGE.setSelected(true);
         
         this.TICKETCAISSE=new JCheckBox("TICKET");
         if(c.PWD_EMPLOYE.contains("TICKET"))
             TICKETCAISSE.setSelected(true);
         
         this.EMPLOYE=new JCheckBox("EMPLOYE");
         if(c.PWD_EMPLOYE.contains("EMPLOYE"))
             EMPLOYE.setSelected(true);
         
         this.FACTURE=new JCheckBox("FACTURE");
         if(c.PWD_EMPLOYE.contains("FACTURE"))
             FACTURE.setSelected(true);
         
         this.CREDIT=new JCheckBox("CREDIT");
         if(c.PWD_EMPLOYE.contains("CREDIT"))
             CREDIT.setSelected(true);
         
         this.CAISSE=new JCheckBox("CAISSE");
         if(c.PWD_EMPLOYE.contains("CAISSE"))
             CAISSE.setSelected(true);
         
         this.RAPPORT=new JCheckBox("RAPPORT"); 
         if(c.PWD_EMPLOYE.contains("RAPPORT"))
             RAPPORT.setSelected(true);
         
         this.REDUCTION=new JCheckBox("REDUCTION");
         if(c.PWD_EMPLOYE.contains("REDUCTION"))
             REDUCTION.setSelected(true);
         
         this.NAME_INV=new JCheckBox("NAME_INVOICE");
         if(c.PWD_EMPLOYE.contains("NAME_INVOICE"))
             NAME_INV.setSelected(true);
         
         this.ZAMURA=new JCheckBox("SALES_UPLOAD");
         if(c.PWD_EMPLOYE.contains("SALES_UPLOAD"))
             ZAMURA.setSelected(true);
         
         this.CMD=new JCheckBox("CMD"); 
         if(c.PWD_EMPLOYE.contains("CMD"))
             CMD.setSelected(true);
         
         this.MVT=new JCheckBox("MVT"); 
         if(c.PWD_EMPLOYE.contains("MVT"))
             MVT.setSelected(true);
         
         this.MVS=new JCheckBox("MVS"); 
         if(c.PWD_EMPLOYE.contains("MVS"))
             MVS.setSelected(true);
         
         this.INVENTAIRE=new JCheckBox("INVENTAIRE"); 
         if(c.PWD_EMPLOYE.contains("INVENTAIRE"))
             INVENTAIRE.setSelected(true);
         
         this.ITEM=new JCheckBox("ITEM"); 
         if(c.PWD_EMPLOYE.contains("ITEM"))
             ITEM.setSelected(true);
         
         this.DATA=new JCheckBox("DATA"); 
         if(c.PWD_EMPLOYE.contains("DATA"))
             DATA.setSelected(true);
         
         this.OPTIONS=new JCheckBox("OPTIONS"); 
         if(c.PWD_EMPLOYE.contains("OPTIONS"))
             OPTIONS.setSelected(true);
         
         this.PERIME=new JCheckBox("PERIMER"); 
         if(c.AUTORISATION.contains("PERIMER"))
             PERIME.setSelected(true);
         
         this.PERIME_SENDER=new JCheckBox("PERIME_SENDER"); 
         if(c.AUTORISATION.contains("PERIME_SENDER"))
             PERIME_SENDER.setSelected(true);
}

public void draw()
{
    setTitle(db.l(db.lang, "V_NEWEMPLOYE"));
    Image im = Toolkit.getDefaultToolkit().getImage("logo.png");
    this.setIconImage(im); 


    this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);


    //on fixe  les composant del'interface
    Container content = getContentPane();


    if(what.equals("UPDATE"))
     send.setText("UPDATE");

     westPane(content);

     setSize(630,750);
     setVisible(true);

}
public  int getIn (String in)
{
                      int productCode=-1;
    try
    {
        if(in.equals(""))
        {

JOptionPane.showMessageDialog(null,db.l(db.lang, "V_IKIBAZO")+" "+db. l(db.lang, "V_INT"),in,JOptionPane.PLAIN_MESSAGE);
        productCode=0;
        }
        else
        {
        Integer in_int=new Integer(in);
        productCode = in_int.intValue();
        if(productCode<0  )
             {

JOptionPane.showMessageDialog(null,db.l(db.lang, "V_IKIBAZO")+" "+db. l(db.lang, "V_DANS")+" "+db. l(db.lang, "V_INT"),db. l(db.lang, "V_IKIBAZO")+""+productCode,JOptionPane.PLAIN_MESSAGE);
        productCode=0;
        }


        }
         }
    catch (NumberFormatException e)
    {
     JOptionPane.showMessageDialog(null,in,db.l(db.lang, "V_NTABWO")+" "+db. l(db.lang, "V_INT")+" ",JOptionPane.PLAIN_MESSAGE);

    productCode=0;
    }
    return productCode;
}



/**************************************************************************
affiche du stock
**************************************************************************/

public void westPane(Container content )
{
JPanel Pane  = new JPanel();

Pane.setPreferredSize(new Dimension (600,700));

JPanel panel = new JPanel();
panel.setLayout(new GridLayout(10,2));
panel.setPreferredSize(new Dimension (400,500));

panel.add(LID_EMPLOYE);panel.add(ID_EMPLOYE);
panel.add(LNOM_EMPLOYE);panel.add(NOM_EMPLOYE);
panel.add(LPRENOM_EMPLOYE);panel.add(PRENOM_EMPLOYE);
panel.add(LBP_EMPLOYE);panel.add(BP_EMPLOYE); 
panel.add(LCATRE_INDETITE);panel.add(CATRE_INDETITE);
panel.add(LLEVEL_EMPLOYE);panel.add(LEVEL_EMPLOYE);
panel.add(LTITRE_EMPLOYE);panel.add(TITRE_EMPLOYE);
panel.add(EMAILL);panel.add( EMAIL);

JScrollPane    scrollPaneProductList =new JScrollPane(LANGUE);
  scrollPaneProductList.setPreferredSize(new Dimension (40,40));
  LANGUE.setBackground(Color.pink);  
panel.add(LLANGUE);panel.add(scrollPaneProductList);
//panel.add(LPWD_EMPLOYE);panel.add(PWD_EMPLOYE);
panel.add(LTEL_EMPLOYE);panel.add(TEL_EMPLOYE);

JPanel panelAuto1 = new JPanel();
panelAuto1.setLayout(new GridLayout(4,4));
panelAuto1.setPreferredSize(new Dimension (600,100));

panelAuto1.add(ANNULE);
panelAuto1.add(CAISSE);
panelAuto1.add(CHANGE);
panelAuto1.add(CMD);
panelAuto1.add(CREDIT);
panelAuto1.add(EMPLOYE);
panelAuto1.add(FACTURE);
panelAuto1.add(MVT);
panelAuto1.add(MVS);
panelAuto1.add(NAME_INV);
panelAuto1.add(RAPPORT);
panelAuto1.add(REDUCTION); 
panelAuto1.add(TICKETCAISSE);
panelAuto1.add(ZAMURA);
panelAuto1.add(INVENTAIRE);
panelAuto1.add(ITEM); 
panelAuto1.add(DATA); 
panelAuto1.add(OPTIONS);
panelAuto1.add(PERIME);
panelAuto1.add(PERIME_SENDER);


Pane.add(panel,BorderLayout.CENTER);
Pane.add(send,BorderLayout.SOUTH);
Pane.add(panelAuto1,BorderLayout.SOUTH);

content.add(Pane, BorderLayout.WEST);

}

/**********************************************************************************************
AJOUT Mannuel
**********************************************************************************************/
public void send()
{
    send.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent ae){
            if(ae.getSource()==send  )
            {
                
                int vID_EMPLOYE =getIn(ID_EMPLOYE.getText());
                String vNOM_EMPLOYE= NOM_EMPLOYE.getText();
                String vPRENOM_EMPLOYE = PRENOM_EMPLOYE.getText();
                String vBP_EMPLOYE = BP_EMPLOYE.getText();
                int vCATRE_INDETITE = getIn(CATRE_INDETITE.getText());
                int vLEVEL_EMPLOYE =getIn(LEVEL_EMPLOYE.getText());
                String vTITRE_EMPLOYE = TITRE_EMPLOYE.getText();
                String MAIL=EMAIL.getText();
//                String vPWD_EMPLOYE = PWD_EMPLOYE.getText();
                int vTEL_EMPLOYE =getIn(TEL_EMPLOYE.getText());
                String vLANGUE=change(""+LANGUE.getSelectedValue(),true);                 
                
                String PWD="";
                String AUTO="";
                if(ANNULE.isSelected())
                PWD=PWD+" ANNULE";  
                if(CHANGE.isSelected())
                PWD=PWD+" CHANGER_PRIX";
                if(CAISSE.isSelected())
                PWD=PWD+" CAISSE";
                if(CREDIT.isSelected())
                PWD=PWD+" CREDIT";
                if(CMD.isSelected())
                PWD=PWD+" CMD";
                if(FACTURE.isSelected())
                PWD=PWD+" FACTURE";
                if(REDUCTION.isSelected())
                PWD=PWD+" REDUCTION"; 
                if(RAPPORT.isSelected())
                PWD=PWD+" RAPPORT";
                if(TICKETCAISSE.isSelected())
                PWD=PWD+" TICKET";
                if(EMPLOYE.isSelected())
                PWD=PWD+" EMPLOYE";
                if(MVT.isSelected())
                PWD=PWD+" MVT";
                if(MVS.isSelected())
                PWD=PWD+" MVS";
                if(NAME_INV.isSelected())
                PWD=PWD+" NAME_INVOICE";
                if(ZAMURA.isSelected())
                PWD=PWD+" SALES_UPLOAD";
                if(INVENTAIRE.isSelected())
                PWD=PWD+" INVENTAIRE";  
                if(ITEM.isSelected())
                PWD=PWD+" ITEM"; 
                
                if(DATA.isSelected())
                PWD=PWD+" DATA"; 
                if(OPTIONS.isSelected())
                PWD=PWD+" OPTIONS";
                                
                if(PERIME.isSelected())
                AUTO=AUTO+" PERIMER"; 
                if(PERIME_SENDER.isSelected())
                AUTO=AUTO+" PERIME_SENDER"; 
                
                Employe p=new Employe(vID_EMPLOYE, vCATRE_INDETITE,vLEVEL_EMPLOYE,vTEL_EMPLOYE, vNOM_EMPLOYE ,vPRENOM_EMPLOYE ,vBP_EMPLOYE ,vTITRE_EMPLOYE,PWD,vLANGUE,AUTO,MAIL);
                if(what.equals("UPDATE"))
                    db.upDateEmp(p,vID_EMPLOYE);
                else
                    db.insertEmp(p);
            }
        }});
    
}

@Override
protected void processWindowEvent(WindowEvent evt)
{
    if(WindowEvent.WINDOW_CLOSING == evt.getID())
   {
	   this.dispose();
			//default icon, custom title
//JOptionPane.showMessageDialog(this,"RIRAFUNGIRA RIMWE N'ISHYIGA"," IKIBAZO",JOptionPane.PLAIN_MESSAGE);
        			//this.setDefaultCloseOperation(HIDE_ON_CLOSE);


	}

}
 public String change(String choice,boolean inv)
 {
     if(choice.equals("KINYARWANDA") && inv)
     {
         return "KIN";
     }
     if(choice.equals("FRANCAIS") && inv)
     {
         return "FRA";
     }
     if(choice.equals("ENGLISH") && inv)
     {
         return "ANG";
     }
     
     if(choice.equals("KIN") && !inv)
     {
         return "KINYARWANDA";
     }
     if(choice.equals("FRA") && !inv)
     {
         return "FRANCAIS";
     }
     if(choice.equals("ANG") && !inv)
     {
         return "ENGLISH";
     }
    return "FRA";
 }


public static void main(String[] arghs)
{
        try {
            StartServer d = new StartServer();
            Db db = new Db("localhost","ishyiga");
            db.read();

            //new PersonInterface(db, "", "RAMA");

        } catch (Throwable ex) {
            Logger.getLogger(PersonInterface.class.getName()).log(Level.SEVERE, null, ex);
        }

}
}
