/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package CRM; 

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.LinkedList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableCellRenderer;
import pos.Db;
import pos.Main;

/**
 *
 * @author Kimenyi
 */
public class InteractionPane extends JPanel {

final int w=600;
final int h=500;  
Db db;
JTable table= new JTable(); 
LinkedList <PatientFileLine> pt = new LinkedList ();
Color [][] s1Color;

public InteractionPane(String reponse,Db db)
{      
 this.db=db;     
 drawFrame (reponse);   
} 
private void drawFrame(String patientFile )
{ 
     JPanel content = this;
     westPane(content,patientFile ); 
     setSize(w,h);
     setVisible(true);

} 
   
public void westPane(JPanel content ,String patientFile )
{
 
JPanel master = new JPanel(); 
master.setPreferredSize(new Dimension (w,h));
   
makeJTables( patientFile);
JScrollPane Scro  =new JScrollPane(table );
Scro.setPreferredSize(new Dimension(w-10,h-10)); 
master.add(Scro);  

content.add(master);

}

private void makeJTables(String patientFile)
{ 
String [] sp=patientFile.split("#");    

  
  for(int i=0;i<sp.length;i++)
  { 
//GYNO-01;2;1300;NOW;ALG000001101;#
//GYNO-02;6;2193;2014-02-03 09:44:04;ALG000001101;>>>GYNO-02 DAKATARIN;MICON01;MICONAZE;MEDIUM;CONTRE ALARGY AND NOT FOR PREGNANT WEMAN#
      
    if (sp[i]!=null && sp[i].contains(">>>")) 
    { 
        String [] LINES=sp[i].split(">>>");
        String [] LINE=LINES[0].split(";"); 
        String [] INTER=LINES[1].split(";");
     //$line[0].';'.$line[1].';'.$line[2].';'.$invoice[1].';'.$invoice[2].
    pt.add(new PatientFileLine(LINE[0],db.getProductDesi(LINE[0]) , 
    LINE[3], LINE[4], Integer.parseInt(LINE[2]), Integer.parseInt(LINE[1]), INTER[3], INTER[4], INTER[2]   ));
   
            
    }
    else
    {
     String [] LINE=sp[i].split(";")  ;
      pt.add(new PatientFileLine(LINE[0], db.getProductDesi(LINE[0]) , 
      LINE[3], LINE[4], Integer.parseInt(LINE[2]), Integer.parseInt(LINE[1]), "", "", ""   ));
      
    }
  
  }    
 
int linge = pt.size();
String[][] s1 = new String[linge][8];
s1Color=new Color [linge][8];
for (int i = 0; i < linge; i++) {
for (int j = 0; j < 8; j++) {
 
    s1Color[i][j]=  Color.white;  
}
}

for (int i = 0; i < linge; i++) { 
PatientFileLine pfl= pt.get(i);   
s1[i]  = pfl.lineArray; 
if(pfl.decision!=null)
{ 
if(pfl.decision.equals("MAJOR"))
{ s1Color[i][5]=  Color.RED; } 
else if(pfl.decision.equals("MEDIUM"))
{ s1Color[i][5]=  Color.yellow; } 
else if(pfl.decision.equals("MINOR")) 
{ s1Color[i][5]=  Color.green; } 
else if(pfl.decision.equals("MINOR"))
{ s1Color[i][5]=  Color.green; } 
}   
}

setTable(new String[]{db.l(Main.lang, "V_CODE"),
db.l(Main.lang, "V_DESIGNATION"),db.l(Main.lang, "V_HEURE"), db.l(Main.lang, "V_QUANTITE"), 
db.l(Main.lang, "V_PRICE"), "DECISION" },s1);
 
System.out.println("SETTABLE ");
 
}
  
 void setTable(String titles[],String [][] arraytoshow)
{  
            MyRenderer myRenderer = new MyRenderer();
            table=new javax.swing.JTable(); 
            table.setModel(new javax.swing.table.DefaultTableModel(arraytoshow, 
            titles
                    ));
            table.setDefaultRenderer(Object.class, myRenderer);
            table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
            table.getColumnModel().getColumn(1).setPreferredWidth(150);//.setWidth(200);
            table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            table.doLayout();
            table.validate();  
             
            changeTable();
            
}
public void changeTable()
{
    table.addMouseListener(new MouseListener() { 
        @Override
    public void mouseClicked(MouseEvent e)
    {
  if( table.getSelectedRow()!=-1 )
    { 
        int roW=table.getSelectedRow();
        int coloM = table.getSelectedColumn(); 
        String selectedcode = (String)table.getValueAt(roW, 0);  
        String selected  = (String)table.getValueAt(roW, coloM);  
        PatientFileLine pfl= pt.get(roW);
   
        String  toDisplay= " \n ITEM   "+pfl.name
           +" \n INTERACT WITH :  "+pfl.designIn
           +" \n LEVEL  "+pfl.decision
           +" \n INTERACTION  "+ pfl.interaction  ; 
        
 int warning=JOptionPane.WARNING_MESSAGE;
 
 
 JOptionPane.showMessageDialog(null, toDisplay  ,selected,warning) ;  
   
    }
  };
        @Override
    public void mousePressed(MouseEvent e){}
        @Override
    public void mouseReleased(MouseEvent e){}
        @Override
    public void mouseEntered(MouseEvent e){}
        @Override
    public void mouseExited(MouseEvent ae){}
    });  
} 
public class MyRenderer extends DefaultTableCellRenderer  
{ 
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean   isSelected, boolean hasFocus, int row, int column) 
{ 
    Component c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column); 

     if(s1Color[row][column]!=null) {
        c.setBackground(s1Color[row][column]);
    }   
      
    return c;
    
}

}



}