/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clinic;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Image;
import java.io.File;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

/**
 *
 * @author Ntezi
 */
public class Patient {

    int ID;
    String ID_ISHYIGA,
            FIRSTNAME,
            LASTNAME,
            SEX, DOB,
            ALLERGIE,
            NID,
            EMAIL,
            PHONE;

    public Patient(String ID_ISHYIGA, String FIRSTNAME, String LASTNAME, String SEX, String DOB, String ALLERGIE, String NID, String EMAIL, String PHONE) {
        this.ID_ISHYIGA = ID_ISHYIGA;
        this.FIRSTNAME = FIRSTNAME;
        this.LASTNAME = LASTNAME;
        this.SEX = SEX;
        this.DOB = DOB;
        this.ALLERGIE = ALLERGIE;
        this.NID = NID;
        this.EMAIL = EMAIL;
        this.PHONE = PHONE;
    }
    
    public String toString() {
        return " | " + FIRSTNAME + "  " + LASTNAME + " ";
    }
}
