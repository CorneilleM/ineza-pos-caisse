                                   /*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clinic; 

import java.awt.*;
import java.awt.event.*;
import java.sql.*;
import java.util.*;

import javax.swing.*;
import pos.*; 
/**
 *
 * @author Ntezi
 */
public class Interface_Patient extends JFrame {

    Container content;
    JTextField ID_ISHYIGA,
            FIRSTNAME,
            LASTNAME,
            DOB,
            NID,EMAIL,PHONE;
    JList   SEX,ASSURANCE;    
    JTextArea ALLERGIE;
    
    JLabel ID_ISHYIGAL,
            FIRSTNAMEL,
            LASTNAMEL,
            DOBL,
            NIDL,EMAILL,PHONEL,
            SEXL,ALLERGIEL,ASSURANCE_Label;
    
    
    
    String photoPath, input;
    JScrollPane assuranceScrol, antecedentScrol, group_sanguinScrol,lienScrol;
    JPanel westPanelGeneral,westPanel, eastPanel, southPanel;
    JButton addButton, checkButton, updateButtom, assurancebutton; 
    
    Font police;
    Statement s;
    PreparedStatement psInsert;
    Connection conn = null; 
//    private ThreadTask _Task = null;
    ResultSet outResult; 
    Cashier cashier;
//    Assurance_Interface ass;
    JLabel nameL, affiliationL, percentageL, dateExpL, departL,affiliePrincipaleL,lien;
    JList name;
    JList  lienList=new JList(new String  [] {"HIMSELF","SPOUSE ","PARENT ","",""});     
    JTextField affiliation, percentage, dateExp, depart,affiliePrincipale;
    JTable tableList,tableList1;
    JScrollPane jScrollPane1, jscroll;
    JButton saveButton, cancelButton, add, Rone, Rall;
    Patient patient;
    LinkedList<Patient_Assurance> insurance = new LinkedList();
    public static JFrame frame;
      
JPanel panel = new JPanel(); 

    public Interface_Patient(Cashier c) {
        this.cashier = c;
        this.conn = c.db.conn;
        initialise();
//        sendUpdated();
        draw();  
    }

    public Interface_Patient(Cashier c, Patient p) {
        this.patient = p;
        this.cashier = c;
        this.conn = c.db.conn;
        initialise(patient);
//        sendUpdated();
         draw();   

    }

    public void initialise() { 
        police = new Font("Verdana", Font.BOLD, 14);
        this.ID_ISHYIGA = new JTextField();
        this.FIRSTNAME = new JTextField();
        this.LASTNAME = new JTextField();
        this.DOB = new JTextField(); 
        this.ALLERGIE = new JTextArea();
        this.NID = new JTextField();
        this.EMAIL = new JTextField();
        this.PHONE = new JTextField(); 
        this.SEX = new JList(); 
        this.ASSURANCE = new JList();

        LinkedList<Variable> control = cashier.db.getParam("CLIENT");
        police = new Font("Verdana", Font.PLAIN, 12);  
        ASSURANCE.setListData(control.toArray());
        ASSURANCE.setSelectedIndex(0);
        ASSURANCE.setFont(police);
        
        SEX.setListData(new String[]{"FEMALE", "MALE"});
        SEX.setBackground(Color.PINK);
        SEX.setFont(police);
         
        police = new Font("Verdana", Font.BOLD, 14);
        this.ID_ISHYIGAL = new JLabel("CODE  : ");
        ID_ISHYIGAL.setFont(police);
        
        this.FIRSTNAMEL = new JLabel("FIRST NAME  : ");
        FIRSTNAMEL.setFont(police);

        this.LASTNAMEL = new JLabel("LAST NAME  : ");
        LASTNAMEL.setFont(police);

        this.SEXL = new JLabel("GENDER  : ");
        SEXL.setFont(police);

        this.DOBL = new JLabel("DATE OF BIRTH  : ");
        DOBL.setFont(police); 

        this.ASSURANCE_Label = new JLabel("INSURANCE");
        ASSURANCE_Label.setFont(police);

        this.NIDL = new JLabel("ID. NUMBER  : ");
        NIDL.setFont(police);

        this.EMAILL = new JLabel("EMAIL  : ");
        EMAILL.setFont(police);

        this.PHONEL = new JLabel("TEL. NUMBER  : ");
        PHONEL.setFont(police);

        this.ALLERGIEL = new JLabel("ALLERGY DETAILS  : ");
        ALLERGIEL.setFont(police); 

        police = new Font("Verdana", Font.PLAIN, 12);
        this.addButton = new JButton("SAVE");
        addButton.setFont(police);

        
        assuranceScrol = new JScrollPane(ASSURANCE);
        
        this.updateButtom = new JButton("UPDATE");
        updateButtom.setFont(police);

        this.assurancebutton = new JButton("INSURANCE");
        assurancebutton.setFont(police);

        police = new Font("Verdana", Font.BOLD, 12);


        nameL = new JLabel("INSURANCE  : ");
        nameL.setFont(police); 
        
        affiliationL = new JLabel("AFF. NUMBER  : ");
        affiliationL.setFont(police);
        
        lien=new JLabel("RELATIONSHIP  : ");
        lien.setFont(police);
        affiliePrincipaleL=new JLabel("PRINCIPAL INSURED  : ");
        affiliePrincipaleL.setFont(police); 
 
        percentageL = new JLabel("PERCENTAGE  : ");
        percentageL.setFont(police);

        dateExpL = new JLabel("EXPIRATION DATE  : ");
        dateExpL.setFont(police);

        departL = new JLabel("DEPARTMENT  : ");
        departL.setFont(police);

        police = new Font("Verdana", Font.BOLD, 12);
        affiliation = new JTextField("");
        affiliation.setFont(police); 
        
        affiliePrincipale = new JTextField("");
        affiliePrincipale.setFont(police);

        percentage = new JTextField("");
        percentage.setFont(police);

        dateExp = new JTextField("");
        dateExp.setFont(police);

        depart = new JTextField("");
        depart.setFont(police);
        
        police = new Font("Verdana", Font.PLAIN, 14);
        cancelButton = new JButton("CANCEL");
        cancelButton.setFont(police);

        Rone = new JButton("ONE");
        Rone.setFont(police);

        Rall = new JButton("ALL");
        Rall.setFont(police);

        saveButton = new JButton("SAVE");
        saveButton.setFont(police);

        add = new JButton("ADD");
        add.setFont(police);
        
        police = new Font("Verdana", Font.PLAIN, 12);
        tableList = new JTable();
        tableList.setFont(police);
        tableList.setBackground(new Color(255, 255, 180));
        tableList.setGridColor(Color.GREEN);
        
        name = new JList();
        assuranceScrol = new JScrollPane(name);
        name.setListData(control.toArray());
        name.setBackground(Color.PINK);
        name.setSelectedIndex(0); 
            
        lienList = new JList();
        lienScrol = new JScrollPane(lienList);
        lienList.setBackground(Color.GREEN);
        lienList.setFont(police);
        lienList.setListData(new String[]{"HIMSELF","SPOUSE ","PARENT ","",""});
        lienList.setSelectedIndex(0);

    }

    public void initialise(Patient p) {

        police = new Font("Verdana", Font.BOLD, 14); 
        this.ID_ISHYIGA = new JTextField("" + p.ID_ISHYIGA);
        this.FIRSTNAME = new JTextField("" + p.FIRSTNAME);
        this.LASTNAME = new JTextField("" + p.LASTNAME);
        this.DOB = new JTextField("" + p.DOB); 
        this.ALLERGIE = new  JTextArea("" + p.ALLERGIE);
        this.NID = new JTextField("" + p.NID);
        this.EMAIL = new JTextField("" + p.EMAIL);
        this.PHONE= new JTextField("" + p.PHONE);
        this.SEX = new JList(); 
        this.ASSURANCE = new JList(); 
        //insurance = cashier.db.getAssurance("" + p.NUM_PATIENT);

        LinkedList<Variable> control = cashier.db.getParam("CLIENT");

        ASSURANCE.setListData(control.toArray());
        ASSURANCE.setSelectedIndex(0);

        SEX.setListData(new String[]{"FEMALE", "MALE"});
        SEX.setSelectedValue(p.SEX, true);
        SEX.setBackground(Color.PINK); 

        police = new Font("Verdana", Font.BOLD, 14);
        
        this.ID_ISHYIGAL = new JLabel("CODE  : ");
        ID_ISHYIGAL.setFont(police);
        
        this.FIRSTNAMEL = new JLabel("FIRST NAME  : ");
        FIRSTNAMEL.setFont(police);

        this.LASTNAMEL = new JLabel("LAST NAME  : ");
        LASTNAMEL.setFont(police);

        this.SEXL = new JLabel("GENDER  : ");
        SEXL.setFont(police);

        this.DOBL = new JLabel("DATE OF BIRTH  : ");
        DOBL.setFont(police); 

        this.ASSURANCE_Label = new JLabel("INSURANCE");
        ASSURANCE_Label.setFont(police);

        this.NIDL = new JLabel("ID. NUMBER  : ");
        NIDL.setFont(police);

        this.EMAILL = new JLabel("EMAIL  : ");
        EMAILL.setFont(police);
        
        this.PHONEL = new JLabel("TEL. NUMBER  : ");
        PHONEL.setFont(police);

        this.ALLERGIEL = new JLabel("ALLERGY DETAILS  : ");
        ALLERGIEL.setFont(police); 

        police = new Font("Verdana", Font.PLAIN, 12);
        this.addButton = new JButton("SAVE");
        addButton.setFont(police);

        
        assuranceScrol = new JScrollPane(ASSURANCE);
        
        this.updateButtom = new JButton("UPDATE");
        updateButtom.setFont(police);

        this.assurancebutton = new JButton("INSURANCE");
        assurancebutton.setFont(police);

        police = new Font("Verdana", Font.BOLD, 12);


        nameL = new JLabel("INSURANCE  : ");
        nameL.setFont(police); 
        
        affiliationL = new JLabel("AFF. NUMBER  : ");
        affiliationL.setFont(police);

        police = new Font("Verdana", Font.PLAIN, 14);
        this.addButton = new JButton("SAVE");
        addButton.setFont(police);
        
        this.checkButton = new JButton("CHECK INFO");
//        checkButton.setFont(police);

        this.updateButtom = new JButton("UPDATE");
        updateButtom.setFont(police);

        this.assurancebutton = new JButton("INSURANCE");
        assurancebutton.setFont(police);

        police = new Font("Verdana", Font.BOLD, 12);


        nameL = new JLabel("INSURANCE");
        nameL.setFont(police);
         
        
        affiliationL = new JLabel("AFF. NUMBER  : ");
        affiliationL.setFont(police);
        
        affiliePrincipaleL = new JLabel("PRINCIPAL INSURED  : ");
        affiliePrincipaleL.setFont(police);
        
        lien = new JLabel("RELATIONSHIP  : ");
        lien.setFont(police);

        percentageL = new JLabel("PERCENTAGE  : ");
        percentageL.setFont(police);

        dateExpL = new JLabel("EXPIRATION DATE  : ");
        dateExpL.setFont(police);

        departL = new JLabel("DEPARTMENT  : ");
        departL.setFont(police); 
         
        affiliation = new JTextField("");
        affiliation.setFont(police);
        
        affiliePrincipale = new JTextField("");
        affiliePrincipale.setFont(police);
        
        
        percentage = new JTextField("");
        percentage.setFont(police);

        dateExp = new JTextField("");
        dateExp.setFont(police);

        depart = new JTextField("");
        depart.setFont(police);
        
        police = new Font("Verdana", Font.PLAIN, 12);
        cancelButton = new JButton("CANCEL");
        cancelButton.setFont(police);

        Rone = new JButton("ONE");
        Rone.setFont(police);

        Rall = new JButton("ALL");
        Rall.setFont(police);

        saveButton = new JButton("SAVE");
        saveButton.setFont(police);

        add = new JButton("ADD");
        add.setFont(police);

        tableList = new JTable();
        tableList.setFont(police);
        tableList.setBackground(new Color(255, 255, 180));
        tableList.setGridColor(Color.GREEN); 

        name = new JList();
        assuranceScrol = new JScrollPane(name);
        name.setListData(control.toArray());
        name.setBackground(Color.PINK);
        name.setSelectedIndex(0);
        
        lienList = new JList();
        lienScrol = new JScrollPane(lienList);
        lienList.setListData(new String[]{"HIMSELF","SPOUSE ","PARENT ","",""});
        lienList.setBackground(Color.GREEN);
        lienList.setSelectedIndex(0);
    }

    public void draw() {
        setTitle("PATIENT FORM ");
        this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE); 
        content = getContentPane(); 
        
        eastPanel(content); 
        southPanel(content);
        
        setSize(500, 600);
        
        send();
        Add();
        removeAll();
        removeOne(); 
        checkAssurance(); 
        setVisible(true);
    }

    void eastPanel(Container content) {
        eastPanel = new JPanel();
        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(9, 2, 5, 20));//
        panel.setPreferredSize(new Dimension(300, 500)); //400=largeur350=longeur 


        panel.add(ID_ISHYIGAL);
        panel.add(ID_ISHYIGA);
        panel.add(FIRSTNAMEL);
        panel.add(FIRSTNAME);
        panel.add(LASTNAMEL);
        panel.add(LASTNAME);
        panel.add(SEXL);
        panel.add(SEX);
        panel.add(DOBL);
        panel.add(DOB); 
        panel.add(ASSURANCE_Label);
        panel.add(assurancebutton);
        panel.add(NIDL);
        panel.add(NID);
        panel.add(EMAILL);
        panel.add(EMAIL);
        panel.add(PHONEL);
        panel.add(PHONE); 
        
        eastPanel.add(panel, BorderLayout.EAST);



//        JPanel face = new JPanel();
//        face.setLayout(new GridLayout(1, 4));
//        face.setPreferredSize(new Dimension(150, 50));
//        face.add(add);
//        face.add(photo);
//        JPanel main = new JPanel();
//        main.add(panel);
//        main.add(face);
        content.add(eastPanel, BorderLayout.WEST);
//
//        JPanel photo_panel = new JPanel();


    } 

    void southPanel(Container content) {

        addButton.setPreferredSize(new Dimension(150, 30));
//        checkButton.setPreferredSize(new Dimension(150, 30));
        updateButtom.setPreferredSize(new Dimension(150, 30));

        southPanel = new JPanel();
//        southPanel.setLayout(new GridLayout(1, 1, 60, 60));
        southPanel.add(addButton);
//        southPanel.add(checkButton);

        southPanel.add(updateButtom);
        content.add(southPanel, BorderLayout.SOUTH);



    }

    /**
     * ********************************************************************************************
     * AJOUT Mannuel
     * ********************************************************************************************
     */
    public void send() {

        addButton.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                if (ae.getSource() == addButton) {
                     
                    System.out.print("Sent"); 

                    Patient info = new Patient(ID_ISHYIGA.getText(),
                            FIRSTNAME.getText(),
                            LASTNAME.getText(),
                            (String) SEX.getSelectedValue(),
                            DOB.getText(), 
                            ALLERGIE.getText(),
                            NID.getText(),
                            EMAIL.getText(),
                            PHONE.getText());  
                    
                    insertInfo(info);
//                    insertAssurance(insurance, NUM_PATIENT.getText());
                    clearPatientForm(); 
                }
            }
        });

    }

//    public void sendUpdated() {
//
//
////        Patient updatedPatient = new Patient(String NUM_PATIENT,String firstname,String lastname,String sex,B,double weight, double taille,String groupe_sanguin,String allergie,String antecedent,String photo = photoPath;,String status);
//        updateButtom.addActionListener(new ActionListener() {
//
//            public void actionPerformed(ActionEvent ae) {
//                String num_patient, firstname, lastname, sex, allergie;
//                if (ae.getSource() == updateButtom) { 
//
////                    Patient updatedPatient = new Patient(String NUM_PATIENT,String firstname,String lastname,String sex,DOB B,double weight, double taille, String groupe_sanguin,String allergie,String antecedent,String photo,String status);
//
//
//                    firstname = FIRSTNAME.getText();
//                    lastname = LASTNAME.getText();
//                    allergie = ALLERGIE.getText();
//                    sex = (String) SEX.getSelectedValue();
//                    
//                    num_patient = patient.NUM_PATIENT;
//
//                    Patient updatedPatient = new Patient(0,num_patient, firstname, lastname, sex, B, weight, taille, groupe_sanguin, allergie, antecedent, photo, status);
//                    
//                    
//                    if (num_patient != null && !num_patient.equals("")) {
//                        if (firstname != null && !firstname.equals("")) {
//                            if (lastname != null && !lastname.equals("")) {
//                                if (day != 0 && !(DOB_YEAR.getText().equals(""))) {
//                                    if (month != 0 && !(DOB_YEAR.getText().equals(""))) {
//                                        if (year != 0 && !(DOB_YEAR.getText().equals(""))) {
//                                            if (month != 0 && !(WEIGHT.getText().equals(""))) {
//                                                if (allergie != null && !allergie.equals("")) {
//                                                    if (taille != 0 && !(TAILLE.getText().equals(""))) {
//                                                        if (groupe_sanguin != null && !groupe_sanguin.equals("")) {
//                                                            if (sex != null && !sex.equals("")) {
//                                                                if (antecedent != null && !antecedent.equals("")) {
//                                                                    if (status != null && !status.equals("")) {
//                                                                        //update(updatedPatient);
//                                                                        clearPatientForm();
//                                                                    } else {
//                                                                        JOptionPane.showMessageDialog(null, "CHECK THE PHOTO", "", JOptionPane.WARNING_MESSAGE);
//                                                                    }
//                                                                } else {
//                                                                    JOptionPane.showMessageDialog(null, "CHECK THE SPECIAL NOTES", "", JOptionPane.WARNING_MESSAGE);
//                                                                }
//                                                            } else {
//                                                                JOptionPane.showMessageDialog(null, "CHECK THE SEX", "", JOptionPane.WARNING_MESSAGE);
//                                                            }
//                                                        } else {
//                                                            JOptionPane.showMessageDialog(null, "CHECK THE BLOOD GROUP ", "", JOptionPane.WARNING_MESSAGE);
//                                                        }
//                                                    } else {
//                                                        JOptionPane.showMessageDialog(null, "CHECK THE HEIGHT", "", JOptionPane.WARNING_MESSAGE);
//                                                    }
//                                                } else {
//                                                    JOptionPane.showMessageDialog(null, "CHECK THE ALLERGY", "", JOptionPane.WARNING_MESSAGE);
//                                                }
//                                            } else {
//                                                JOptionPane.showMessageDialog(null, "CHECK THE WEIGHT", "", JOptionPane.WARNING_MESSAGE);
//                                            }
//                                        } else {
//                                            JOptionPane.showMessageDialog(null, "CHECK THE YEAR", "", JOptionPane.WARNING_MESSAGE);
//                                        }
//                                    } else {
//                                        JOptionPane.showMessageDialog(null, "CHECK THE MONTH", "", JOptionPane.WARNING_MESSAGE);
//                                    }
//                                } else {
//                                    JOptionPane.showMessageDialog(null, "CHECK THE DAY", "", JOptionPane.WARNING_MESSAGE);
//                                }
//                            } else {
//                                JOptionPane.showMessageDialog(null, "CHECK THE LAST NAME", "", JOptionPane.WARNING_MESSAGE);
//                            }
//                        } else {
//                            JOptionPane.showMessageDialog(null, "CHECK THE FIRST NAME", "", JOptionPane.WARNING_MESSAGE);
//                        }
//                    } else {
//                        JOptionPane.showMessageDialog(null, "CHECK THE PATIENT CODE", "", JOptionPane.WARNING_MESSAGE);
//                    }
//
//                }
//            }
//        });
//
//    }

    public void checkAssurance() {

        assurancebutton.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {

                if (ae.getSource() == assurancebutton) {
//                     JOptionPane.showConfirmDialog(null, new Assurance_Interface(cashier),"",JOptionPane.OK_OPTION);

                    int i = JOptionPane.showConfirmDialog(null, makePane());
//                    if(i==1||i==0||i==-1)
//                    {
//                        setVisible(true);
//                    }

                }
            }
        });

    }

    void clearPatientForm() {

        ID_ISHYIGA.setText("");
        FIRSTNAME.setText("");
        LASTNAME.setText("");
        DOB.setText(""); 
        ALLERGIE.setText("");
        NID.setText("");
        EMAIL.setText("");
        PHONE.setText(""); 
        insurance.clear(); 
        setVisible(true);
        this.dispose();
    }

//void get() {
//
//        try {
//            System.out.println("11");
//            outResult = s.executeQuery("select * from APP.CLINIC_PATIENT where FIRSTNAME='" + input + "' ");
//
//            System.out.println("select  * from APP.CLINIC_PATIENT where FIRSTNAME=" + input + " ");
////            File file = new File(outResult);
////            photoPath = file.getPath();
////            ImageIcon im = new ImageIcon(photoPath = file.getPath());
////            Image ii = im.getImage();
////            ii = ii.getScaledInstance(180, 210, WIDTH);
//
//
//
//            while (outResult.next()) {
//
//                NUM_PATIENT.setText(outResult.getString("NUM_PATIENT"));
//                FIRSTNAME.setText(outResult.getString("FIRSTNAME"));
//                LASTNAME.setText(outResult.getString("LASTNAME"));
//                SEX.setSelectedValue(outResult.getString("SEX"), true);
//                DOB_DAY.setText(outResult.getString("DOB_DAY"));
//                DOB_MONTH.setText(outResult.getString("DOB_MONTH"));
//                DOB_YEAR.setText(outResult.getString("DOB_YEAR"));
//                WEIGHT.setText(outResult.getString("WEIGHT"));
//                TAILLE.setText(outResult.getString("TAILLE"));
//                GROUPE_SANGUIN.setSelectedValue(outResult.getString("GROUPE_SANGUIN"), true);
//                ALLERGIE.setText(outResult.getString("ALLERGIE"));
//                ANTECEDENT.setText(outResult.getString("ANTECEDENT"));
//
//                photoPath = outResult.getString("PHOTO");
//                ImageIcon im = new ImageIcon(outResult.getString("PHOTO"));
//                Image ii = im.getImage();
//                ii = ii.getScaledInstance(250, 250, WIDTH);
//                System.out.println("%%");
//                 
//                setVisible(true);
//            }
//        } catch (Throwable e) {
//            System.err.println("11" + e);
//        }
//    }

//    public void update(Patient pa) {
//
//        photoPath = 0+".jpg";
//        try {
////            s = conn.createStatement();
//            System.out.println("ngiye kuri querry wangu");
////            cashier.db.updatePatient(pa, patient.NUM_PATIENT);
////                
//           cashier.db. s.execute(" update APP.CLINIC_PATIENT set "
//                    + "FIRSTNAME ='" + pa.FIRSTNAME + "',"
//                    + "LASTNAME='" + pa.LASTNAME + "',"
//                    + "DOB_DAY =" + pa.BIRTH.DOB_DAY + ","
//                    + "DOB_MONTH=" + pa.BIRTH.DOB_MONTH + ","
//                    + "DOB_YEAR =" + pa.BIRTH.DOB_YEAR + ","
//                    + "WEIGHT=" + pa.WEIGHT + ","
//                    + "ALLERGIE ='" + pa.ALLERGIE + "',"
//                    + "TAILLE=" + pa.TAILLE + ","
//                    + "GROUPE_SANGUIN ='" + pa.GROUPE_SANGUIN + "',"
//                    + "SEX='" + pa.SEX + "',"
//                    + "ANTECEDENT ='" + pa.ANTECEDENT + "',"
//                   
//                    + "PHOTO='" + photoPath+ "', FINGERPRINT_LEFT='"+pa.FINGERPRINT_LEFT+"',"
//                    +" FINGERPRINT_RIGHT='"+pa.FINGERPRINT_RIGHT+"'"
//                   
//                    + " WHERE NUM_PATIENT='" + patient.NUM_PATIENT + "'");
//
//            System.out.println("ngiye kuri querry wangu");
////            updateAss(); 
//            
//        } catch (Exception e1) {
//            System.out.print("Ikibazooooooooo" + e1);
//            cashier.db.insertError("" + e1, "update patient1");
//        } 
//    }

//    public void updateAss() {
//        if (patient != null) {
//            for (int i = 0; i < insurance.size(); i++) {
//                Patient_Assurance pass = insurance.get(i);
//                Patient_Assurance assur = null;
//                try {
//                    
//                    outResult = cashier.db.s.executeQuery("select * from clinic_assurance where num_patient='" + patient.NUM_PATIENT + "' and assurance='" + pass.NOM_ASSURANCE + "' and num_affiliation='" + pass.NUM_AFFILIATION + "' and affilie_principale='"+pass.AFFILIE_PRINCIPALE+"' and lien='"+pass.LIEN+"' and pin='"+pass.PIN+" '");
//                                
//                    while (outResult.next()) {
//                    assur = new Patient_Assurance(outResult.getString(3), outResult.getString(4),outResult.getString(8) ,outResult.getString(9),outResult.getInt(5), outResult.getInt(6), outResult.getString(7),outResult.getString(10));
//                    }
//                    outResult.close();
//                    if (assur != null) 
//                    {
//                        System.out.println(" update APP.CLINIC_ASSURANCE set POURCENTAGE=" + assur.PERCENTAGE + ","
//                                + "AFFILIE_PRINCIPALE ='" + assur.AFFILIE_PRINCIPALE + ","
//                                + "LIEN='" + assur.LIEN + "'," 
//                                + "PIN='" + assur.PIN + "',"
//                                + "DATE_EXP =" + assur.DATE_EXP + ","
//                                + "DEPARTEMENT='" + assur.DEPARTEMENT + "'"
//                                + " WHERE NUM_PATIENT='" + patient.NUM_PATIENT
//                                + "' AND ASSURANCE='" + patient.ASSURANCE
//                                + "' AND NUM_AFFILIATION='" + patient.NUM_AFFILIATION + "' ");
//                        
//                        cashier.db.s.execute(" update APP.CLINIC_ASSURANCE "
//                                + "set POURCENTAGE=" + assur.PERCENTAGE + ","
//                                + "AFFILIE_PRINCIPALE ='" + assur.AFFILIE_PRINCIPALE + "',"
//                                + "LIEN='" + assur.LIEN + "'," 
//                                + "PIN='" + assur.PIN + "'," 
//                                + "DATE_EXP =" + assur.DATE_EXP + ","                     
//                                + "DEPARTEMENT='" + assur.DEPARTEMENT + "'"
//                                + " WHERE NUM_PATIENT='" + patient.NUM_PATIENT
//                                + "' AND ASSURANCE='" + patient.ASSURANCE
//                                + "' AND NUM_AFFILIATION='" + patient.NUM_AFFILIATION + "' ");
//                                       
//                        
//                    } 
//                    else
//                    {
//                        System.out.println("insert into APP.CLINIC_ASSURANCE(NUM_PATIENT,"
//                                + "ASSURANCE,"
//                                + "NUM_AFFILIATION,"
//                                + "POURCENTAGE,"
//                                + "DATE_EXP,"
//                                + "DEPARTEMENT,"
//                                + "AFFILIE_PRINCIPALE,"
//                                + "LIEN,"
//                                + "PIN)  values (?,?,?,?,?,?,?,?,?)");
//                        
//                        psInsert = conn.prepareStatement("insert into APP.CLINIC_ASSURANCE(NUM_PATIENT,"
//                                + "ASSURANCE,"
//                                + "NUM_AFFILIATION,"
//                                + "POURCENTAGE,"
//                                + "DATE_EXP,"
//                                + "DEPARTEMENT,"
//                                + "AFFILIE_PRINCIPALE,"
//                                + "LIEN,"
//                                + "PIN)  values (?,?,?,?,?,?,?,?,?)");
//
//                        psInsert.setString(1, patient.NUM_PATIENT);
//                        psInsert.setString(2, pass.NOM_ASSURANCE);
//                        psInsert.setString(3, pass.NUM_AFFILIATION);
//                        psInsert.setInt(4, pass.PERCENTAGE);
//                        psInsert.setInt(5, pass.DATE_EXP);
//                        psInsert.setString(6, pass.DEPARTEMENT);
//                        psInsert.setString(7, pass.AFFILIE_PRINCIPALE);
//                        psInsert.setString(8, pass.LIEN);
//                        psInsert.setString(9, pass.PIN);
//                        psInsert.executeUpdate();
//                        psInsert.close();
//                    }
//                } catch (Exception e1) {
//                    System.out.print("Ikibazo" + e1);
//                    cashier.db.insertError("" + e1, "update assurance");
//
//                }
//            }
//            JOptionPane.showMessageDialog(null, "PATIENT INFORMATION UPDATED SUCCESSFULLY !!");
//
//        }
//    }

     
     

    @Override
    protected void processWindowEvent(WindowEvent evt) {
        if (WindowEvent.WINDOW_CLOSING == evt.getID()) {
            this.dispose();
        }

    }

    public void insertInfo(Patient info) {
        try {
            psInsert = conn.prepareStatement("insert into APP.CLIENT_ISHYIGA( ID_ISHYIGA,"
                    + "FIRSTNAME,"
                    + "LASTNAME,"
                    + "DOB," 
                    + "SEXE,"
                    + "ALLERGIE,"
                    + "NID,"
                    + "EMAIL,"
                    + "TELEPHONE)"
                    + " values (?,?,?,?,?,?,?,?,?)");

            psInsert.setString(1, info.ID_ISHYIGA);
            psInsert.setString(2, info.FIRSTNAME);
            psInsert.setString(3, info.LASTNAME);
            psInsert.setString(4, info.DOB); 
            psInsert.setString(5, info.SEX); 
            psInsert.setString(6, info.ALLERGIE);
            psInsert.setString(7, info.NID);
            psInsert.setString(8, info.EMAIL);
            psInsert.setString(9, info.PHONE); 
            psInsert.executeUpdate();
            psInsert.close();

            System.out.println("added to db");
        } catch (SQLException ex) {

            System.out.println(ex);
        }
        JOptionPane.showMessageDialog(null, cashier.db.l(cashier.db.lang, "V_CLIENT") + " " + cashier.db.l(cashier.db.lang, "V_ARINJIYE") + " " + cashier.db.l(cashier.db.lang, "V_NTAKIBAZO"), cashier.db.l(cashier.db.lang, "V_MERCI"), JOptionPane.INFORMATION_MESSAGE);

    }

    public JPanel makePane() 
    {
        JPanel mainPane = new JPanel();
        mainPane.setPreferredSize(new Dimension(400, 600));
        JPanel nordPanel = new JPanel();
        JPanel sudPanel = new JPanel();
        sudPanel.setLayout(new GridLayout(2, 1, 5, 10));
        sudPanel.setPreferredSize(new Dimension(400, 450)); 
        
        JPanel centerPanel = new JPanel();
        centerPanel.setLayout(new GridLayout(2, 1, 5, 10));

        JPanel panelProduct = new JPanel();
        panelProduct.setLayout(new GridLayout(8, 2, 5, 10));
        
        panelProduct.setPreferredSize(new Dimension(400,380));        
        police = new Font("Verdana", Font.BOLD, 14);
        panelProduct.setBorder(BorderFactory.createTitledBorder("                    FILL IN ALL THE INSURANCE INFORMATION                            "));
        panelProduct.setFont(police);
        panelProduct.add(nameL);
        panelProduct.add(assuranceScrol); 
        panelProduct.add(affiliationL);
        panelProduct.add(affiliation);
        panelProduct.add(affiliePrincipaleL);
        panelProduct.add(affiliePrincipale);
        panelProduct.add(lien);
        panelProduct.add(lienScrol);
        panelProduct.add(percentageL);
        panelProduct.add(percentage);
        panelProduct.add(dateExpL);
        panelProduct.add(dateExp);
        panelProduct.add(departL);
        panelProduct.add(depart);

 
 
 
        JPanel panelButton1 = new JPanel();
        panelButton1.setLayout(new GridLayout(1, 1, 5, 10));
        panelButton1.setPreferredSize(new Dimension(400,25)); 
        panelButton1.add(Rone);
        panelButton1.add(add);
        panelButton1.add(Rall);

        JPanel panelTable = new JPanel();
        panelTable.setLayout(new GridLayout(1, 1, 20, 5));
        panelTable.setPreferredSize(new Dimension(400, 300));

        tableList = startTable();
        setTable();

        jscroll = new JScrollPane(tableList);
        panelTable.add(jscroll);

        JPanel panelButton = new JPanel();
        panelButton.setLayout(new GridLayout(1, 2, 20, 5));
        panelButton.setPreferredSize(new Dimension(400, 50));

        panelButton.add(saveButton);
        panelButton.add(cancelButton);

        nordPanel.add(panelProduct);

          
       centerPanel.add(panelButton1);

        sudPanel.add(panelTable);
        sudPanel.add(panelButton);


        mainPane.add(nordPanel, BorderLayout.NORTH);
        mainPane.add(panelButton1, BorderLayout.CENTER);
        mainPane.add(sudPanel, BorderLayout.SOUTH);

//        content1.add(mainPane);
        return mainPane;
    }

    private JTable startTable() {
        System.err.println("size:" + insurance.size());

        if (insurance.size() > 0) {
            setTable();
        } else {
            jScrollPane1 = new javax.swing.JScrollPane();
            tableList = new javax.swing.JTable();
            tableList.setBackground(new Color(255, 255, 180));
            tableList.setGridColor(Color.GREEN);
            tableList.setAutoResizeMode(tableList.AUTO_RESIZE_ALL_COLUMNS);
//            tableList.getColumnModel().getColumn(1).setPreferredWidth(50);//.setWidth(200);
     
            String[][] s1 = new String[0][6];
            tableList.setModel(
                    new javax.swing.table.DefaultTableModel(s1, new String[]{"INSURANCE", "AFF. NUMBER","P.INSURED" ,"RELATIONSHIP","%", "EXP.DATE", "DEPARTMENT"}) {

                        Class[] types = new Class[]{java.lang.String.class, java.lang.String.class, java.lang.String.class,
                            java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class};

                        @Override
                        public Class getColumnClass(int columnIndex) {
                            return types[columnIndex];
                        }
                    });
        }
        return tableList;
    }

    void clear() {
        affiliation.setText("");
        percentage.setText("");
        affiliePrincipale.setText("");
        dateExp.setText("");
        depart.setText("");
    }

    void setTable() {
        int linge = insurance.size();
        String[][] s1 = new String[linge][7];
        for (int i = 0; i < linge; i++) {
            Patient_Assurance C = insurance.get(i);

            s1[i][0] = C.NOM_ASSURANCE;
            s1[i][1] = C.NUM_AFFILIATION;
            s1[i][2] = C.AFFILIE_PRINCIPALE;
            s1[i][3] = C.LIEN;
            s1[i][4] = "" + C.PERCENTAGE;
            s1[i][5] = "" + C.DATE_EXP;
            s1[i][6] = "" + C.DEPARTEMENT;
//                    s1[i][6]=""+(com*C.QTY_CMD);
//                    s1[i][7]=""+((C.PRO.PRIX_REVIENT*C.QTY_CMD)-(com*C.QTY_CMD));

        }
        tableList.setModel(new javax.swing.table.DefaultTableModel(s1, new String[]{"INSURANCE", "AFF. NUMBER","P.INSURED ","RELATIONSHIP ", "%", "EXP.DATE", "DEPARTMENT"}));
        tableList.setAutoResizeMode(tableList.AUTO_RESIZE_ALL_COLUMNS);
        tableList.getColumnModel().getColumn(1).setPreferredWidth(140);//.setWidth(200);
        tableList.doLayout();
        tableList.validate();
    }

    void Add() {
        add.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                if (e.getSource() == add) {
                    String assr = "" + ((Variable) name.getSelectedValue()).nom; 
                    String aff = "" + affiliation.getText();
                    String affpr = "" + affiliePrincipale.getText();
                    String link = "" + (String) lienList.getSelectedValue();
                    String depar = "" + depart.getText();

                    int per =Integer.parseInt(percentage.getText());
                    int exp =Integer.parseInt(dateExp.getText());   
                   
                    if (dateExp.getText().length()<3 || dateExp.getText().length()<4){
                    
                     JOptionPane.showMessageDialog(null, "CHECK THE DATE FORMAT ", " NOTIFIED !",JOptionPane.WARNING_MESSAGE);
                    }
                    
                    
                    if (percentage.getText().length()>3){
                    
                     JOptionPane.showMessageDialog(null, "CHECK THE % FORMAT ", " NOTIFIED !",JOptionPane.WARNING_MESSAGE);
                    }
                     

                    if (assr.equals("") || aff.equals("") ||affpr.equals("") ||link.equals("") || depar.equals("") || percentage.getText().equals("") || dateExp.getText().equals("")) {
                    JOptionPane.showMessageDialog(null, " FILL IN ALL THE FIELDS ", " NOTIFIED !",JOptionPane.WARNING_MESSAGE);  
                    } 
                    else {
                        try {
//                          int per = Integer.parseInt(percentage.getText());
//                          int exp = Integer.parseInt(dateExp.getText());
                            Patient_Assurance ass = new Patient_Assurance(assr, aff, affpr,link,per, exp, depar);
                            if (existe(ass)) {
                                insurance.add(new Patient_Assurance(assr, aff,affpr,link, per, exp, depar));
                                setTable();
                                clear();
                            } else {
                                JOptionPane.showMessageDialog(null, "INSURANCE " + ass.NOM_ASSURANCE + " ALREADY EXIST ", "ERROR!", JOptionPane.INFORMATION_MESSAGE);
                            }
                  
                  
                        } catch (Exception ee) {
                            System.out.println("" + ee);
                        }
                    }
                }
            }
        });
    }

    boolean existe(Patient_Assurance ass) {
        for (int i = 0; i < insurance.size(); i++) {
            if (ass.NOM_ASSURANCE.equals(insurance.get(i).NOM_ASSURANCE)) {
                return false;
            }
        }
        return true;
    }

    public void removeOne() {
        Rone.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                if (ae.getSource() == Rone && insurance.size() > 0 && tableList.getSelectedRow() != -1) {
                    Patient_Assurance toRemove = (Patient_Assurance) (insurance.get(tableList.getSelectedRow()));
                    insurance.remove(toRemove);
                    setTable();
                }
            }
        });
    }

    public void removeAll() {

        Rall.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                if (ae.getSource() == Rall && insurance.size() > 0) {
                    insurance = new LinkedList();
                    setTable();
                    clear();
                }
            }
        });

    }

    public void insertAssurance(LinkedList<Patient_Assurance> Assur, String NUM_PAT) {

        for (int i = 0; i < Assur.size(); i++) {
            Patient_Assurance pass = Assur.get(i);
            try {
                psInsert = conn.prepareStatement("insert into APP.CLINIC_ASSURANCE(NUM_PATIENT,"
                        + "ASSURANCE,"
                        + "NUM_AFFILIATION,"
                        + "POURCENTAGE,"
                        + "DATE_EXP,"
                        + "DEPARTEMENT"
                        + "AFFILIE_PRINCIPALE,"
                        + "LIEN,"
                        + ")  values (?,?,?,?,?,?,?,?)");

                psInsert.setString(1, NUM_PAT);
                psInsert.setString(2, pass.NOM_ASSURANCE);
                psInsert.setString(3, pass.NUM_AFFILIATION);
                psInsert.setInt(4, pass.PERCENTAGE);
                psInsert.setInt(5, pass.DATE_EXP);
                psInsert.setString(6, pass.DEPARTEMENT);
                psInsert.setString(7, pass.AFFILIE_PRINCIPALE);
                psInsert.setString(8, pass.LIEN); 

                psInsert.executeUpdate();
                psInsert.close();

                JOptionPane.showMessageDialog(null, cashier.db.l(cashier.db.lang, "V_CLIENT") + " " + cashier.db.l(cashier.db.lang, "V_ARINJIYE") + " " + cashier.db.l(cashier.db.lang, "V_NTAKIBAZO"), cashier.db.l(cashier.db.lang, "V_MERCI"), JOptionPane.INFORMATION_MESSAGE);

                System.out.println("added to db");
            } catch (SQLException ex) {

                System.out.println(ex);
            }

        } 
    } 
}