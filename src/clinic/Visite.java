/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinic; 

/**
 *
 * @author user
 */
public class Visite {
    
     public String DATE,ASSURANCE,NUM_PATIENT,NUM_AFFILIATION,ETAT_ACTUEL,ETAT_SORTIE,NUMERO_QUITANCE,UMURWAZA,TYPE_VISITE,ID_EMPLOYE,VUVUZELA,HEURE_ENTRE,HEURE_SORTIE,NUMERO_BON;
     public int ID_VISITE,POURCENTAGE; 
    
        public Visite (String NUM_PATIENT,String NUM_AFFILIATION,String ETAT_ACTUEL,String ETAT_SORTIE,
                String NUMERO_QUITANCE,String UMURWAZA,String TYPE_VISITE,String ID_EMPLOYE,String VUVUZELA,
                String ASSURANCE)
        {
            this.NUM_PATIENT=NUM_PATIENT;
            this.NUM_AFFILIATION=NUM_AFFILIATION;
            this.ETAT_ACTUEL=ETAT_ACTUEL;
            this.ETAT_SORTIE=ETAT_SORTIE;
            this.NUMERO_QUITANCE=NUMERO_QUITANCE;
            this.UMURWAZA=UMURWAZA;
            this.TYPE_VISITE=TYPE_VISITE;
            this.ID_EMPLOYE=ID_EMPLOYE;
            this.VUVUZELA=VUVUZELA; 
            this.ASSURANCE=ASSURANCE;
        }
        public Visite (String NUM_PATIENT,String NUM_AFFILIATION,String ETAT_ACTUEL,String ETAT_SORTIE,String NUMERO_QUITANCE,String UMURWAZA,String TYPE_VISITE,String ID_EMPLOYE,String VUVUZELA,String ASSURANCE,String HEURE_ENTRE,String HEURE_SORTIE)
        {
            this.NUM_PATIENT=NUM_PATIENT;
            this.NUM_AFFILIATION=NUM_AFFILIATION;
            this.ETAT_ACTUEL=ETAT_ACTUEL;
            this.ETAT_SORTIE=ETAT_SORTIE;
            this.NUMERO_QUITANCE=NUMERO_QUITANCE;
            this.UMURWAZA=UMURWAZA;
            this.TYPE_VISITE=TYPE_VISITE;
            this.ID_EMPLOYE=ID_EMPLOYE;
            this.VUVUZELA=VUVUZELA; 
            this.ASSURANCE=ASSURANCE;
            this.HEURE_ENTRE=HEURE_ENTRE; 
            this.HEURE_SORTIE=HEURE_SORTIE;
        }
        @Override
        public String toString()
        {
            return ID_VISITE+" | "+NUM_PATIENT+" | "+TYPE_VISITE+" | "+ETAT_ACTUEL;
        }
          
    
    
}
