/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinic;

/**
 *
 * @author HP
 */ 
import java.awt.*;
import java.awt.event.*;
import java.sql.*;
import java.util.Date;
import javax.swing.*;
import pos.Cashier;
import pos.Client;

/**
 *
 * @author user
 */
public class Visite_Interface extends JFrame 
{

    JList TYPE_VISITE, LIST_CONSULTATION,LIST_STAFF;
    //JList ETAT_ACTUEL,ETAT_SORTIE;
    JTextField NUM_PATIENT,NUM_AFFILIATION,ASSURANCE,UMURWAZA,NUMERO_QUITANCE;
   // JTextField ID_EMPLOYE;
    JTextArea VUVUZELA;
    JLabel NUM_PATIENTL,NUM_AFFILIATIONL,ASSURANCEL,UMURWAZAL,TYPE_VISITEL,COMMENTL,NUMERO_QUITANCEL;
    
    //JLabel ETAT_ACTUELL,ETAT_SORTIEL,ID_EMPLOYEL;
    JRadioButton radioSpecialist,radioGeneralist,radioInfirmiere,radioSoin,radioConsultation;
    JScrollPane etat_ActuelScroll, etat_SortieScroll,type_VisiteScroll,consultation_scroll,staff_scroll;
    JButton add;
    Statement s;
    PreparedStatement psInsert;
    Connection conn = null;
    JScrollPane vuvuzelaScroll;
    Cashier cashier;
    Client patient;
    int id_vis=1; 
    Container content = getContentPane();
    Font police;

    Visite_Interface(Cashier c,Client patient) {

        this.conn = c.db.conn;
        this.cashier=c;
        this.patient=patient;
        content = getContentPane();
        content.setBackground(new Color(216,233,236));
        initialise();
        send();
        draw();
    }

    public void initialise() {

        this.ASSURANCE = new JTextField(""+patient.ASSURANCE);
        ASSURANCE.setEditable(false);
        this.NUM_AFFILIATION = new JTextField(""+patient.NUM_AFFILIATION);
        NUM_AFFILIATION.setEditable(false);
        this.UMURWAZA = new JTextField();
        this.NUM_PATIENT = new JTextField(""+patient.NOM_CLIENT);
        NUM_PATIENT.setEditable(false);
        this.NUMERO_QUITANCE=new JTextField();
        this.VUVUZELA=new JTextArea();
        vuvuzelaScroll = new JScrollPane(VUVUZELA);
        
//        this.ETAT_ACTUEL = new JList();
//        ETAT_ACTUEL.setListData(new String[]{"LOOP", "OPEN", "CLOSE", "TRANSFER", "HOSPITALIZED"});
//        etat_ActuelScroll = new JScrollPane(ETAT_ACTUEL);
        
//        this.ETAT_SORTIE = new JList();
//        ETAT_SORTIE.setListData(new String[]{"CURED", "HALF CURED", "FLED" ,"DIED"});
//        etat_SortieScroll = new JScrollPane(ETAT_SORTIE);
        this.LIST_CONSULTATION = new JList();
        this.LIST_CONSULTATION.setBackground(new Color(226,222,204));
        this.LIST_CONSULTATION.setListData(cashier.db.allActesConsultations("CONSULTATIONS").toArray());
        //List
        consultation_scroll = new JScrollPane(LIST_CONSULTATION);
        
        this.LIST_STAFF = new JList();
        this.LIST_STAFF.setBackground(new Color(221,232,198));
        this.LIST_STAFF.setListData(cashier.db.allActesConsultations("CONSULTATIONS").toArray());
        staff_scroll = new JScrollPane(LIST_STAFF);
        this.TYPE_VISITE = new JList();
        TYPE_VISITE.setListData(new String[]{"ORDINARY","TRANSFER", "APPOINTMENT", "EMERGENCY", "PRENATAL"});
        type_VisiteScroll = new JScrollPane(TYPE_VISITE);
        
//        this.ETAT_ACTUELL = new JLabel("VISIT STATE");
//        this.ETAT_SORTIEL = new JLabel("OUT STATE");
//        this.ID_EMPLOYEL = new JLabel("EMPLOYE");
        police = new Font("Verdana", Font.BOLD, 14);
       
        this.NUMERO_QUITANCEL=new JLabel("VOUCHER CODE :");
        NUMERO_QUITANCEL.setFont(police);
        this.NUM_PATIENTL = new JLabel("PATIENT CODE :");
        NUM_PATIENTL.setFont(police);
        this.UMURWAZAL = new JLabel("PATIENT CARE :");
        UMURWAZAL.setFont(police);
        this.NUM_AFFILIATIONL = new JLabel("AFFILIATION NUMBER :");
        NUM_AFFILIATIONL.setFont(police);
        this.TYPE_VISITEL = new JLabel("VISIT TYPE ");
        TYPE_VISITEL.setFont(police);
        this.COMMENTL = new JLabel("COMMENT :");  
        COMMENTL.setFont(police);
        this.ASSURANCEL = new JLabel("INSURANCE :");
        ASSURANCEL.setFont(police);

        this.add = new JButton("SEND");

    }

    public void draw() 
    {
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize(); 
    
        setTitle("VISIT'S FORM");
        this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);

        //on fixe  les composant del'interface

        this.setSize(1000, 600);
        this.setLayout(new GridLayout(1, 2, 10, 10));
        this.setBackground(new Color(216,233,236));
        setVisible(true);
        
    //this.setResizable(false);
    
    int left = (dim.width - this.getWidth()) / 2;
    int top = (dim.height - this.getHeight()) / 2;
    this.setLocation(left, top);
    
    
        westPane(content);
        eastPane(content);

    }
void eastPane(Container content) {
        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(10, 2));//
        panel.setBorder(BorderFactory.createTitledBorder("SAISIE VISITE"));
        panel.setBackground(new Color(216,233,236));
//serviceScrol.add(SERVICE);
        panel.add(NUM_PATIENTL);
        panel.add(NUM_PATIENT);
        panel.add(NUM_AFFILIATIONL);
        panel.add(NUM_AFFILIATION);
        panel.add(ASSURANCEL);
        panel.add(ASSURANCE);
        panel.add(NUMERO_QUITANCEL);
        panel.add(NUMERO_QUITANCE);
        panel.add(UMURWAZAL);
        panel.add(UMURWAZA);
        panel.add(TYPE_VISITEL);
        panel.add(type_VisiteScroll); 
        panel.add(COMMENTL);
        panel.add(vuvuzelaScroll);
        panel.add(add);

        content.add(panel,BorderLayout.EAST);
    }
    void westPane(Container content) {
   
        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(2, 1, 5, 10));//
        panel.setBackground(new Color(216,233,236));
        JPanel panelStaff = new JPanel();
        panelStaff.setLayout(new GridLayout(1, 2));
        panelStaff.setBorder(BorderFactory.createTitledBorder("STAFF"));
        panelStaff.setBackground(new Color(194,214,155));
        
        JPanel panelStaffButtons = new JPanel();
        panelStaffButtons.setLayout(new GridLayout(3, 1));
    radioSpecialist= new JRadioButton("SPECIALIST");
    radioSpecialist.setBackground(new Color(194,214,155));
    panelStaffButtons.add(radioSpecialist);
    radioGeneralist= new JRadioButton("GENERALIST");
    radioGeneralist.setBackground(new Color(194,214,155));
    panelStaffButtons.add(radioGeneralist);
    radioInfirmiere= new JRadioButton("INFIRMERIE");
    radioInfirmiere.setBackground(new Color(194,214,155));
    panelStaffButtons.add(radioInfirmiere);
    
        JPanel panelConsultation = new JPanel();
        panelConsultation.setLayout(new GridLayout(1, 2));
        panelConsultation.setBorder(BorderFactory.createTitledBorder("SERVICES"));
        panelConsultation.setBackground(new Color (210,204, 174));

        JPanel panelConsultationButtons = new JPanel();
        panelConsultationButtons.setLayout(new GridLayout(2, 1));
        
    radioConsultation= new JRadioButton("CONSULTATION");
    radioConsultation.setBackground(new Color(210,204, 174));
    panelConsultationButtons.add(radioConsultation);
    radioSoin= new JRadioButton("SOINS INFIRIMIERE");
    radioSoin.setBackground(new Color (210,204, 174));
    panelConsultationButtons.add(radioSoin);
    
        panelStaff.add(panelStaffButtons);
        panelStaff.add(staff_scroll);
        panelConsultation.add(panelConsultationButtons);
        panelConsultation.add(consultation_scroll);
        panel.add(panelStaff);
        panel.add(panelConsultation);
                
    
        content.add(panel,BorderLayout.WEST);
    }
    
    public void radioSpecialist()
{
    radioSpecialist.addMouseListener(new MouseListener() {
    public void actionPerformed(ActionEvent ae) { }
    public void mouseExited(MouseEvent ae){}
    public void mouseClicked(MouseEvent e)
    {
        //if(radioSpecialist.isSelected())
//        n = (String)JOptionPane.showInputDialog(null,"ENTER PASSWORD:", "PASSWORD", 0);
        //JFrame ne= new JFrame();
//        
//       
//           try {
//                
//                if(n.equalsIgnoreCase("gamma"))
//                    {           
//                     System.out.println("nibi kabisa");
//                     
//                    
//                       ne.setSize(750,600);
//                       ne.setVisible(true);
//                       radioSpecialist.doClick();
//                     
//                    }
//               
//                
//                else 
//                {
//                    JOptionPane.showMessageDialog(null,"WRONG PASSWORD","ATTENTION",JOptionPane.PLAIN_MESSAGE);
//                    radioSpecialist.doClick();
//                }
//        }
//           catch (Exception aae) {
//                    System.out.println(aae);}
//        
    };  
    public void mousePressed(MouseEvent e){}
    public void mouseReleased(MouseEvent e){}
    public void mouseEntered(MouseEvent e){}
    }); 
     
    }

    /**
     * ********************************************************************************************
     * AJOUT Mannuel
     * ********************************************************************************************
     */
    public void send() {

        add.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                if (ae.getSource() == add) {

                    if (TYPE_VISITE.getSelectedValue()==null|| NUM_AFFILIATION.getText() == null ||UMURWAZA.getText()==null) {
                        JOptionPane.showMessageDialog(null, "INCOMPLETE FORM",
                                "ERROR", JOptionPane.ERROR_MESSAGE);
                    } else {
                        try {

                           Visite visite = new Visite(NUM_PATIENT.getText(),NUM_AFFILIATION.getText()
                                   ,"OPEN","UNKOWN",NUMERO_QUITANCE.getText(),UMURWAZA.getText(),
                                   (String)TYPE_VISITE.getSelectedValue(),cashier.db.nom_empl,
                                   VUVUZELA.getText(),ASSURANCE.getText());
                                   visite.POURCENTAGE=patient.PERCENTAGE;

                            insertVisite(visite);
                            visite.ID_VISITE=id_vis;
//                            int n=( JOptionPane.showConfirmDialog(null,
//                    "DO YOU WANT TO PASS TO DIAGNOSTIC ?", 
//                    "TITLE", JOptionPane.YES_NO_OPTION));
//            if(n==0)
//            {
//                new Interface_Diagnostic(cashier,patient,visite);
//            }
            
                        } catch (Exception e) {
                            System.out.println(e);
                        }


                    }
                }
            }
        });

    }

    @Override
    protected void processWindowEvent(WindowEvent evt) {
        if (WindowEvent.WINDOW_CLOSING == evt.getID()) {
            this.dispose();
        }

    }

    public void insertVisite(Visite visite) {
        try {
            String keyIdInvoice=""+new Date()+""+cashier.db.nom_empl+cashier.db.ip;
//            String nFrrs=  (JOptionPane.showInputDialog(null, " NUMERO DU BON "+visite.ASSURANCE));

            psInsert = conn.prepareStatement("insert into APP.CLINIC_VISITE(DATE, NUM_PATIENT,NUM_AFFILIATION,ETAT_ACTUEL,ETAT_SORTIE,UMURWAZA,TYPE_VISITE,ID_EMPLOYE,VUVUZELA,ASSURANCE,KEY_INVOICE,NUMERO_QUITANCE,POURCENTAGE)  values (?,?,?,?,?,?,?,?,?,?,?,?,?)");

            psInsert.setString(1,cashier.date);
            psInsert.setString(2, visite.NUM_PATIENT);
            psInsert.setString(3, visite.NUM_AFFILIATION);
            psInsert.setString(4, visite.ETAT_ACTUEL);
            psInsert.setString(5, visite.ETAT_SORTIE); 
            psInsert.setString(6, visite.UMURWAZA);
            psInsert.setString(7, visite.TYPE_VISITE);
            psInsert.setString(8, visite.ID_EMPLOYE);
            psInsert.setString(9, visite.VUVUZELA);  
            psInsert.setString(10, visite.ASSURANCE); 
            psInsert.setString(11, keyIdInvoice); 
//            psInsert.setString(12, nFrrs); 
            psInsert.setString(12,visite.NUMERO_QUITANCE);
            psInsert.setInt(13, visite.POURCENTAGE); 
            psInsert.executeUpdate();
            psInsert.close(); 
            id_vis=cashier.db.getIdtable("ID_VISITE","CLINIC_VISITE",keyIdInvoice);
            JOptionPane.showMessageDialog(null,"VISIT "+id_vis+"  FROM "+patient.NOM_CLIENT+" "
                    + "\n"+cashier.db.l(cashier.db.lang, "V_ARINJIYE")+" "+cashier.db.l(cashier.db.lang, "V_NTAKIBAZO"),cashier.db.l(cashier.db.lang, "V_MERCI"),JOptionPane.INFORMATION_MESSAGE);

        } catch (SQLException ex) {

            System.out.println(ex);
        } 
    }
    
    
    
}
