/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clinic;

/**
 *
 * @author user
 */
public class Actes {

    public String CODE_ACTES, NAME_ACTES, FAMILLE_CATEGORY, OBSERVATION;
    public double TVA, PRIX; 
    public int ID_ACTES;

    public Actes(String CODE_ACTES, String NAME_ACTES, double TVA, double PRIX, String FAMILLE_CATEGORY, String OBSERVATION) {
        this.CODE_ACTES = CODE_ACTES;
        this.NAME_ACTES = NAME_ACTES;
        this.TVA = TVA;
        this.PRIX = PRIX;
        this.FAMILLE_CATEGORY = FAMILLE_CATEGORY;
        this.OBSERVATION = OBSERVATION;
    }

    @Override
    public String toString() {
        return  FAMILLE_CATEGORY+" *** " +NAME_ACTES;
//                +"***" +"("+CODE_ACTES+")"  ;
    }
}
