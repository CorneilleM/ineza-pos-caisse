/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smart;

/**
 *
 * @author Danny
 */
public class SmartTag {
    
    String XmlTagtype="Type";
    String XmlTagamount_required="Amount_Required";
    String XmlTagcard_issuedate="card_issuedate";
    String XmlTagcard_issuername="card_issuername";
    String XmlTagcard_serialnumber="card_serialnumber";
    String XmlTagcard_validitystatus="card_validitystatus";
    String XmlTagpolicy_country="policy_country";
    String XmlTagpatient_dob="patient_dob";
    String XmlTagpatient_surname="patient_surname";
    String XmlTagpatient_forenames="patient_forenames";
    String XmlTagpatient_gender="patient_gender";
    String XmlTagmedicalaid_expiry="medicalaid_expiry";
    String XmlTagmedicalaid_rules="medicalaid_rules";
    String XmlTagmedicalaid_showpools="medicalaid_showpools";
    String XmlTagmedicalaid_number="medicalaid_number";
    String XmlTagmedicalaid_plan="medicalaid_plan";
    String XmlTagmedicalaid_limit="medicalaid_limit";
    String XmlTagglobal_id="global_id";
    String XmlTagmedicalaid_code="medicalaid_code";
    String XmlTagmedicalaid_scheme="medicalaid_scheme";
    String XmlTagpatient_hospitalnumber="patient_hospitalnumber";
    String XmlTagbenefit="Benefits"; 
    String XmlTagNr="Nr";
    String XmlTagamount="Amount"; 
    String XmlTagPreAuthNeeded="PreAuthNeeded";
    String XmlTagClaimable="Claimable"; 
    
}
