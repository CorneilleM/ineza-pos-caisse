/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smart;

/**
 *
 * @author Danny
 */
public class SmartExchange {
    
    int ID;
    String GLOBAL_ID ;
    String MEMBER_NR ; 
    int ADMIT_ID  ;
    int PROGRESS_FLAG ;
    String REJECTION_REASON ;
    int EXCHANGE_TYPE;
    int INOUT_TYPE;
    String LOCATION_ID;
    String SMART_DATE;
    String SMART_FILE;
    String EXCHANGE_DATE;
    String EXCHANGE_FILE;
    String RESULT_DATE;
    String RESULT_FILE;
    SmartPatient EXCHANGE_PATIENT;

    public SmartExchange(int ID, String GLOBAL_ID, String MEMBER_NR, int ADMIT_ID, int PROGRESS_FLAG,
            String REJECTION_REASON, int EXCHANGE_TYPE, int INOUT_TYPE, String LOCATION_ID, String SMART_DATE,
            String SMART_FILE, String EXCHANGE_DATE, String EXCHANGE_FILE, String RESULT_DATE, String RESULT_FILE) {
        this.ID = ID;
        this.GLOBAL_ID = GLOBAL_ID;
        this.MEMBER_NR = MEMBER_NR;
        this.ADMIT_ID = ADMIT_ID;
        this.PROGRESS_FLAG = PROGRESS_FLAG;
        this.REJECTION_REASON = REJECTION_REASON;
        this.EXCHANGE_TYPE = EXCHANGE_TYPE;
        this.INOUT_TYPE = INOUT_TYPE;
        this.LOCATION_ID = LOCATION_ID;
        this.SMART_DATE = SMART_DATE;
        this.SMART_FILE = SMART_FILE;
        this.EXCHANGE_DATE = EXCHANGE_DATE;
        this.EXCHANGE_FILE = EXCHANGE_FILE;
        this.RESULT_DATE = RESULT_DATE;
        this.RESULT_FILE = RESULT_FILE;
        this.EXCHANGE_PATIENT=new SmartXML(SMART_FILE,false).getPatient();
    }

    @Override
    public String toString() {
        return ""+EXCHANGE_PATIENT.medicalaid_number+": "+EXCHANGE_PATIENT.patient_forenames+" "+
                EXCHANGE_PATIENT.patient_surname+"\n"; //To change body of generated methods, choose Tools | Templates.
    } 
    public String toString2() {
        return  ID+">>>"+
         GLOBAL_ID+">>>"+
         MEMBER_NR+">>>"+
         ADMIT_ID+">>>"+
         PROGRESS_FLAG+">>>"+
         REJECTION_REASON+">>>"+
         EXCHANGE_TYPE+">>>"+
         INOUT_TYPE+">>>"+
         LOCATION_ID+">>>"+
         SMART_DATE+">>>"+
         SMART_FILE+">>>"+
         EXCHANGE_DATE+">>>"+
         EXCHANGE_FILE+">>>"+
         RESULT_DATE+">>>"+
         RESULT_FILE+">>>"; //To change body of generated methods, choose Tools | Templates.
    }

    public int getID() {
        return ID;
    } 

    public String getGLOBAL_ID() {
        return GLOBAL_ID;
    }
    
        

    public SmartPatient getEXCHANGE_PATIENT() {
        return EXCHANGE_PATIENT;
    }
    
    
    
    
    
    
}
