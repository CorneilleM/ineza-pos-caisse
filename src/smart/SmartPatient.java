/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smart;

/**
 *
 * @author Danny
 */
public class SmartPatient {
    
    public String PracticeNumber;public int percentage,Type2;
    public String Amount_Required,Receipt,
            
    NHIF_Member_Nr,NHIF_Employer_Code,NHIF_Site_Nr,NHIF_Patient_Relation,
    Admit_Date,Discharge_Date,
    card_issuedate,card_retmasscounter,card_issuername,card_serialnumber,card_validitystatus,card_retcounter,        
    patient_marriagestatus,policy_country,patient_surname,patient_partnerprevioussurname,patient_previoussurname,
    patient_marriagedate,patient_partnerdob,patient_forenames,patient_title,        
    policy_currency,policy_id,patient_language,patient_address,
    employer_fax,employer_cellphone,employer_telephone,patient_cellphone,patient_email,employer_name,
    employer_address, employer_department,patient_telephone,patient_id,patient_birthcountry,
    patient_passportnumber,patient_division,patient_location,patient_birthprovince,patient_idissuedate,
    patient_passportexpirydate,patient_birthcity,patient_subdivision,patient_idcountry,
    patient_passportapplicationdate,patient_passportcountry,patient_passportissuedate,
    patient_passportcity,patient_sublocation,patient_minutia_identifier,
    patient_height,patient_nextofkin,patient_organdonor,patient_livingwill,patient_hospitalnumber, 
    medicalaid_expiry  ,medicalaid_rules,medicalaid_showpools ,medicalaid_plan,
    medicalaid_limit,medicalaid_scheme,medicalaid_groupstatus,nhif_membernumber, medicalaid_regdate,
    global_id,medicalaid_code,dependant_number,amount ,PreAuthNeeded,Claimable,OnceOffVaccinations,medicalaid_number,
    pool_number,patient_gender,patient_dob;
    
    
//    String XmlTagamount_required="Amount_Required";
//    String XmlTagcard_issuedate="card_issuedate";
//    String XmlTagcard_issuername="card_issuername";
//    String XmlTagcard_serialnumber="card_serialnumber";
//    String XmlTagcard_validitystatus="card_validitystatus";
//    String XmlTagpolicy_country="policy_country";
//    String XmlTagpatient_dob="patient_dob";
//    String XmlTagpatient_surname="patient_surname";
//    String XmlTagpatient_forenames="patient_forenames";
//    String XmlTagpatient_gender="patient_gender";
//    String XmlTagmedicalaid_expiry="medicalaid_expiry";
//    String XmlTagmedicalaid_rules="medicalaid_rules";
//    String XmlTagmedicalaid_showpools="medicalaid_showpools";
//    String XmlTagmedicalaid_number="medicalaid_number";
//    String XmlTagmedicalaid_plan="medicalaid_plan";
//    String XmlTagmedicalaid_limit="medicalaid_limit";
//    String XmlTagglobal_id="global_id";
//    String XmlTagmedicalaid_code="medicalaid_code";
//    String XmlTagmedicalaid_scheme="medicalaid_scheme";
//    String XmlTagpatient_hospitalnumber="patient_hospitalnumber";
//    String XmlTagbenefit="Benefits"; 
//    String XmlTagamount="Amount"; 
//    String XmlTagPreAuthNeeded="PreAuthNeeded";
//    String XmlTagClaimable="Claimable";

    public SmartPatient() {
    }  
    public int expiryToInt(String expiry)
{
    //2000-10-12
    //0123456789
    // bug y'abuzukuru 2100 (13/10/2015 11:03 AM)
    String day=expiry.substring(expiry.length()-2, expiry.length());
    String m=expiry.substring(expiry.length()-5, expiry.length()-3);
    String y=expiry.substring(expiry.length()-8, expiry.length()-6);
    
    try
    {
        return Integer.parseInt(y+m+day);
    }
    catch(Exception e)
    {
        return 0;
    } 
}

public String getGender(String gVal)
{
    if(gVal.equals("2"))
    {
        return "FEMALE";
    }
    return "MALE";
}

    public int getType() {
        return Type2;
    }


    
    
    
    @Override
    public String toString()
{
    
    return 
            "Patient Surname   :  "+patient_surname+" \n "+
            "Patient Forenames   :  "+patient_forenames+" \n "+
             "Amount    : "+amount+" \n "+ 
          //  card_issuedate+":card_issuedate"+" \n "+
            //card_issuername+":card_issuername"+" \n "+
            "Card Serial Number   :  "+card_serialnumber+":card_serialnumber"+" \n "+
           // card_validitystatus+":card_validitystatus"+" \n "+
          //  policy_country+":policy_country"+" \n "+
            //patient_dob+":patient_dob"+" \n "+
           "Medicalaid Scheme   : "+medicalaid_scheme+" \n "+
           // patient_gender+":patient_gender"+" \n "+
          //  medicalaid_expiry+":medicalaid_expiry"+" \n "+
           // medicalaid_rules+":medicalaid_rules"+" \n "+
          //  medicalaid_showpools+":medicalaid_showpools"+" \n "+
            "Medicalaid Number   :  "+medicalaid_number+" \n "+
            "Medicalaid Plan  : "+medicalaid_plan+" \n "+
            //medicalaid_limit+":medicalaid_limit"+" \n "+
            //global_id+":global_id"+" \n "+
            "Medicalaid Code    : " +medicalaid_code+" \n "+
           
            "Patient hospital number "+patient_hospitalnumber+" \n "+ 
           
            "PreAuthNeeded : "+PreAuthNeeded+" \n "+
           // Claimable+":Claimable"+" \n"+
            "Pool number : "+pool_number;
    
}
    
}
