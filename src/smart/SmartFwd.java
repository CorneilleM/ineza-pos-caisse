/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package smart;

import java.util.LinkedList;
import javax.swing.JOptionPane;

/**
 *
 * @author HP
 */
public class SmartFwd {
    

String Amount_Required,Receipt,
NHIF_Member_Nr,NHIF_Employer_Code,NHIF_Site_Nr,NHIF_Patient_Relation,
Admit_Date,Discharge_Date,Amount,
card_issuedate,card_retmasscounter,card_issuername,card_serialnumber,card_validitystatus,card_retcounter,
        
patient_marriagestatus,policy_country,patient_surname,patient_partnerprevioussurname,patient_previoussurname,
patient_marriagedate,patient_partnerdob,patient_forenames,patient_dob,patient_title,
        
policy_currency,policy_id,patient_language,patient_address,
employer_fax,employer_cellphone,employer_telephone,patient_cellphone,patient_email,employer_name,
employer_address, employer_department,patient_telephone,patient_id,patient_birthcountry,
patient_passportnumber,patient_division,patient_location,patient_birthprovince,patient_idissuedate,
patient_passportexpirydate,patient_birthcity,patient_subdivision,patient_idcountry,
patient_passportapplicationdate,patient_passportcountry,patient_passportissuedate,
patient_passportcity,patient_sublocation,patient_gender,patient_minutia_identifier,
patient_height,patient_nextofkin,patient_organdonor,patient_livingwill, 
medicalaid_expiry  ,medicalaid_rules,medicalaid_showpools ,medicalaid_number,medicalaid_plan,
medicalaid_limit,medicalaid_groupstatus,nhif_membernumber, medicalaid_regdate,
global_id,medicalaid_code,dependant_number ,PreAuthNeeded,Claimable,OnceOffVaccinations  ;

String XmlTagcard_serialnumber="card_serialnumber";
String XmlTagpatient_surname="patient_surname";
String XmlTagpatient_forenames="patient_forenames";
String XmlTagpatient_partnerdob="patient_partnerdob";
String XmlTagpatient_idissuedate="patient_idissuedate";

public SmartFwd(LinkedList<String> list )
{

String res="";
for(int i=0; i<   list.size();i++)
{
    if(list.get(i).contains(XmlTagcard_serialnumber) )
    { card_serialnumber=extractInfoTag(list.get(i),XmlTagcard_serialnumber); }
     if(list.get(i).contains(XmlTagpatient_surname) )
    { patient_surname=extractInfoTag(list.get(i),XmlTagpatient_surname); }
     if(list.get(i).contains(XmlTagpatient_forenames) )
    { patient_forenames=extractInfoTag(list.get(i),XmlTagpatient_forenames); }
     if(list.get(i).contains(XmlTagpatient_partnerdob) )
    { patient_partnerdob=extractInfoTag(list.get(i),XmlTagpatient_partnerdob); }
     if(list.get(i).contains(XmlTagpatient_idissuedate) )
    { patient_idissuedate=extractInfoTag(list.get(i),XmlTagpatient_idissuedate); } 
}

}

private String extractInfoTag(String line,String tag)
{
    //</Vaccination>
    return line.replaceAll("<"+tag+">", "").replaceAll("</"+tag+">", "");
} 

    @Override
    public String toString()
{
    return card_serialnumber+" \n "+ patient_surname+" \n "+patient_forenames
            +" \n "+patient_partnerdob+" \n "+patient_idissuedate;
}
}
