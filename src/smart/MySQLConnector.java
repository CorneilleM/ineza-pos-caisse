/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smart;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.LinkedList;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import pos.Db;

/**
 *
 * @author Danny
 */
public class MySQLConnector {

    String dbName;
    String connectionURL;
    Db ishyigaDb;

    ResultSet outResult, outResult1;
    public Connection connMysql = null;
    public Statement stateMysql;
    public static boolean imAliveCard=false;
    
    public MySQLConnector(String server, String dataBase, Db db) {
        this.dbName = dataBase;
        this.ishyigaDb = db;
        connectionURL = "jdbc:mysql://" + server + ":3307/" + dataBase + "";//;user=root;password=''";
        System.out.println(connectionURL);
        read();

    }

    private void read() {
        try {
            // Create (if needed) and connect to the database
            connMysql = DriverManager.getConnection(connectionURL, "root", "smart123"); 
            
            String version = connMysql.getMetaData().getDatabaseProductVersion();

            System.out.println(version);
            stateMysql = connMysql.createStatement();
            System.out.println("connection etablie"+ connectionURL);
            imAliveCard=true;
        } catch (Throwable e) {
            System.err.println(e+" connection no etablie TO " + connectionURL);
           
        }

    }

    public void updateFlag7(String global_id, String flag, String condition) {
        try {
            stateMysql = connMysql.createStatement();
            String createString1 = " update EXCHANGE_FILES set PROGRESS_FLAG=" + flag 
                    + " where GLOBAL_ID='" + global_id + "' "+condition;
            stateMysql.execute(createString1);

        } catch (Throwable e) {
            ishyigaDb.insertError("" + e, "Error update flag");
            JOptionPane.showMessageDialog(null, e, "Error update flag  ", JOptionPane.PLAIN_MESSAGE); 
        }

    }
    
    public void updateExchange(int id_exchange,String global_id, String exchange_file,String flag) {
        try {
               
            exchange_file=exchange_file.replaceAll("'", "`");
            stateMysql = connMysql.createStatement();
            String createString1 = " update EXCHANGE_FILES set EXCHANGE_DATE=now(),"
                    + " EXCHANGE_FILE='"+exchange_file+"' where GLOBAL_ID='" + global_id + "'"
                    + "  AND ID="+id_exchange+"";
            
//           String createString1 = " update EXCHANGE_FILES set EXCHANGE_DATE=now(),"
//                    + " EXCHANGE_FILE='"+exchange_file+"' where GLOBAL_ID='" + global_id + "'"
//                    + " AND PROGRESS_FLAG="+flag+" AND ID="+id_exchange+"";
            
            System.out.println(createString1);
            
            stateMysql.execute(createString1);

        } catch (Throwable e) {
            ishyigaDb.insertError("" + e, "Error update Exchange");
            JOptionPane.showMessageDialog(null, e, "Error update exchange  ", JOptionPane.PLAIN_MESSAGE);

        }

    }
     
public String getPracticeNumber() {  
        try {
            String sql="select distinct `Group_Practice_Number` from exchange_locations";
   System.out.println(sql);
            outResult = stateMysql.executeQuery(sql);
            while (outResult.next()) { 
                
                return  outResult.getString(1) ;
            }
            //  Close the resultSet
            outResult.close();
        } catch (Throwable e) {
            ishyigaDb.insertError(e + "", "getPendingClient");
        }

        return "";
    }
 



 public SmartPatient getClientCopay(SmartPatient card) {  
        try {
            String sql="select `Amount`,`Type` from settings_copayments "
                    + " where `Medical_Aid_Plan` ='"+card.medicalaid_plan+"' and `Pool_Nr`='"+card.pool_number+"'";
   System.out.println(sql);
   boolean find=false;
            outResult = stateMysql.executeQuery(sql);
            while (outResult.next()) {  
                card.percentage =outResult.getInt(1) ;
                card.Type2 =outResult.getInt(2) ;
                find=true;
            }
            //  Close the resultSet
            outResult.close();
            
            if(!find) /// by Brown and Allan
            { 
                insertErrorMySQL2(card,"-Copay is missing  in Smart",  stateMysql,   connMysql);
                
                JPanel pw = new JPanel();
                JPasswordField passwordField = new JPasswordField(10);
                JLabel label = new JLabel("CLIENT COPAY IS MISSING,CONTACT CLIENT INSURANCE OR SMART"
                        + "\n  PUT FORCING PASSWORD TO CONTINUE OR CANCEL ");
                pw.add(label);
                pw.add(passwordField);
                passwordField.setText("");
                JOptionPane.showConfirmDialog(null, pw);
                String pwd=ishyigaDb.getString("smartpwd");
                
                if(!pwd.equals("") && pwd.toUpperCase().equals(passwordField.getText().toUpperCase()) )
                {
                    int n = JOptionPane.showConfirmDialog(null,"COPAY WILL BE ASSIGNED TO ZERO \n DO YOU AGREE?", "ATTENTION", JOptionPane.YES_NO_OPTION);
                    if(n==0)
                    {
                        card.percentage =0;
                        card.Type2 =0;
                        
                        return card;
                    }  
                    else
                    {
                        return null;
                    }
                }
                else
                    return null;
               //return new SMART_MATRIX(100000,"","", " "," yes", "","", outResult.getDouble("MAXIMUM_AMOUNT_INVOICE"));
            }
            
            //insertError(  card);
            
        } catch (Throwable e) {
            ishyigaDb.insertError(e + "", "getPendingClient");
        }

        return card;
    }
 
    public LinkedList<SmartExchange> getPendingClient(String flag) { 
        LinkedList<SmartExchange> pending = new LinkedList();
        try {
            String sql="SELECT * FROM EXCHANGE_FILES WHERE PROGRESS_FLAG=" + flag + "";
            System.out.append(sql);
            outResult = stateMysql.executeQuery("SELECT * FROM EXCHANGE_FILES WHERE PROGRESS_FLAG=" + flag + "");
            while (outResult.next()) {
                int id=outResult.getInt("ID");
                String GLOBAL_ID = outResult.getString("GLOBAL_ID");
                String MEMBER_NR = outResult.getString("MEMBER_NR"); 
                int admit_id = outResult.getInt("admit_id");
                int progress_flag = outResult.getInt("progress_flag");
                String rejection_reason = outResult.getString("Rejection_Reason");
                int exchange_type = outResult.getInt("Exchange_Type");
                int InOut_Type = outResult.getInt("InOut_Type");
                String location_id=outResult.getString("Location_ID");
                String Smart_Date = outResult.getString("Smart_Date");
                String Smart_File = outResult.getString("Smart_File");
                
                String Exchange_Date = outResult.getString("Exchange_Date");
                String Exchange_File = outResult.getString("Exchange_File");
                String Result_Date = outResult.getString("Result_Date");
                String Result_File = outResult.getString("Result_File");
                
                
                SmartExchange c = new SmartExchange(id,GLOBAL_ID,MEMBER_NR, admit_id, progress_flag, 
                        rejection_reason, exchange_type, InOut_Type, location_id, Smart_Date, Smart_File,
                        Exchange_Date, Exchange_File, Result_Date, Result_File);
                pending.add(c);

            }
            //  Close the resultSet
            outResult.close();
        } catch (Throwable e) {
            ishyigaDb.insertError(e + "", "getPendingClient");
        }

        return pending;
    }

    public static void insertErrorMySQL2(SmartPatient card,String errorType,Statement s, Connection conn) {
        try { 
            
            String sqll="select distinct (exchange_locations.Group_Practice_Number ) "
                    + "from exchange_locations";
            String IP_address=""; 
             
           ResultSet outResult = s.executeQuery(sqll);
            while (outResult.next()) {
                IP_address=outResult.getString(1);
            } 
                  
String sql=   "INSERT INTO smartlink_errors (SL_Version, Message,  Error, inserted,IP_address) "
 + "VALUES (  ?,?,?,?,?)";
  System.out.println(sql);
   PreparedStatement  psInsert = conn.prepareStatement(sql);
 
            psInsert.setString(1, "ISHYIGA");
            //psInsert.setString(2, " now() ");
            psInsert.setString(2, card.medicalaid_number+errorType);
            psInsert.setString(3, card.medicalaid_plan+card.medicalaid_code);
            LocalDateTime ld=LocalDateTime.now();
            psInsert.setTimestamp(4, Timestamp.valueOf(ld)  );
            psInsert.setString(5,IP_address); 
            
            psInsert.executeUpdate();
            psInsert.close();
// '', CURRENT TIMESTAMP, '"+card.medicalaid_number+"-scheme is missing on Matrix', '"+card.medicalaid_plan+card.medicalaid_code+"'

          JOptionPane.showMessageDialog(null, " NO REASON  " , " SMART MATRIX ", JOptionPane.PLAIN_MESSAGE);   

        } catch (Throwable ex) {
            JOptionPane.showMessageDialog(null, "  " + ex, " Ikibazo ", JOptionPane.PLAIN_MESSAGE);
        }

    }

    public static void main(String[] args) {
        new MySQLConnector("localhost", "smartlink", null);
    }

}
