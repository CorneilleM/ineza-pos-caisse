/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smart;

import java.io.ByteArrayInputStream;
import java.io.File;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * @author Danny
 */
public class SmartXML extends DefaultHandler{ 
    
    SmartPatient smartPatient=new SmartPatient();
    String currentTag;
    String previousTag="";
    SmartTag allTag=new SmartTag();
    int typeCount=0;
    boolean benefit=false;

    public SmartXML(String fname,boolean isFile) {
        readDataFromXML(fname,isFile);
    }
    
    
    
    public void readDataFromXML(String filename,boolean isFile) //throws SAXException, IOException, ParserConfigurationException
    {
        try
        {
        SAXParserFactory factory=SAXParserFactory.newInstance();
        SAXParser saxParser = factory.newSAXParser();
        if(isFile)
            saxParser.parse(new File(filename), this);
        else
            saxParser.parse(new InputSource(new ByteArrayInputStream(filename.getBytes("utf-8"))),this);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }
    
    @Override
    public void startDocument() throws SAXException {
        //System.out.println("start document");    
    }

    @Override
    public void endDocument() throws SAXException {
        //System.out.println("End document"+smartPatient+"55" );
    }

    @Override
    public void startElement(String uri, String localName,
            String qName, Attributes attributes) throws SAXException {
        //System.out.println("start element:"+qName);
        
        if(qName.equalsIgnoreCase("Benefit"))
            benefit=true;
        
        currentTag=qName;     
        
    }

    @Override
    public void endElement(String uri, String localName, 
            String qName) throws SAXException {
//        System.out.println("end element:"+qName);
    } 

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
       // System.out.println(allTag.XmlTagamount_required+":Characters:"+currentTag+"::"+(currentTag.equalsIgnoreCase(allTag.XmlTagamount_required)));
//        if(currentTag)
        //System.out.println(currentTag+":"+previousTag);
        if(currentTag.equalsIgnoreCase(allTag.XmlTagtype) && typeCount==0 )
         { smartPatient.Type2=0; new String(ch, start, length);
         previousTag=currentTag;currentTag="";typeCount++;}
        else if(currentTag.equalsIgnoreCase(allTag.XmlTagamount_required) )
         { smartPatient.Amount_Required=new String(ch, start, length);previousTag=currentTag;currentTag="";}
        else if(currentTag.equalsIgnoreCase(allTag.XmlTagcard_issuedate) )
         { smartPatient.card_issuedate=new String(ch, start, length); previousTag=currentTag;currentTag="";}
        else if(currentTag.equalsIgnoreCase(allTag.XmlTagcard_issuername) )
         { smartPatient.card_issuername=new String(ch, start, length); previousTag=currentTag;currentTag="";}
        else if(currentTag.equalsIgnoreCase(allTag.XmlTagcard_serialnumber) )
         { smartPatient.card_serialnumber=new String(ch, start, length);previousTag=currentTag; currentTag="";}
        else if(currentTag.equalsIgnoreCase(allTag.XmlTagcard_validitystatus) )
         { smartPatient.card_validitystatus=new String(ch, start, length);previousTag=currentTag; currentTag="";}
        else if(currentTag.equalsIgnoreCase(allTag.XmlTagpolicy_country) )
         { smartPatient.policy_country=new String(ch, start, length); previousTag=currentTag;currentTag="";}
        else if(currentTag.equalsIgnoreCase(allTag.XmlTagpatient_dob) )
         { smartPatient.patient_dob=new String(ch, start, length);previousTag=currentTag; currentTag="";}
        else if(currentTag.equalsIgnoreCase(allTag.XmlTagpatient_surname) )
         { smartPatient.patient_surname=new String(ch, start, length); previousTag=currentTag;currentTag="";}
        else if(currentTag.equalsIgnoreCase(allTag.XmlTagpatient_forenames) )
         { smartPatient.patient_forenames=new String(ch, start, length);previousTag=currentTag; currentTag="";}
        else if(currentTag.equalsIgnoreCase(allTag.XmlTagpatient_gender) )
         { String gender=new String(ch, start, length);previousTag=currentTag; currentTag="";
           smartPatient.patient_gender=smartPatient.getGender(gender);}
        else if(currentTag.equalsIgnoreCase(allTag.XmlTagmedicalaid_expiry) )
         { smartPatient.medicalaid_expiry=new String(ch, start, length);previousTag=currentTag; currentTag="";}
        else if(currentTag.equalsIgnoreCase(allTag.XmlTagmedicalaid_rules) )
         { smartPatient.medicalaid_rules=new String(ch, start, length);previousTag=currentTag; currentTag="";}
        else if(currentTag.equalsIgnoreCase(allTag.XmlTagmedicalaid_showpools) )
         { smartPatient.medicalaid_showpools=new String(ch, start, length);previousTag=currentTag; currentTag="";}
        else if(currentTag.equalsIgnoreCase(allTag.XmlTagmedicalaid_number) )
         { smartPatient.medicalaid_number=new String(ch, start, length);previousTag=currentTag; currentTag="";}
        else if(currentTag.equalsIgnoreCase(allTag.XmlTagmedicalaid_plan) )
         { smartPatient.medicalaid_plan=new String(ch, start, length);previousTag=currentTag; currentTag="";}
        else if(currentTag.equalsIgnoreCase(allTag.XmlTagmedicalaid_limit) )
         { smartPatient.medicalaid_limit=new String(ch, start, length); currentTag="";}
        else if(currentTag.equalsIgnoreCase(allTag.XmlTagglobal_id) )
         { smartPatient.global_id=new String(ch, start, length);previousTag=currentTag; currentTag="";}
        else if(currentTag.equalsIgnoreCase(allTag.XmlTagmedicalaid_code) )
         { smartPatient.medicalaid_code=new String(ch, start, length);previousTag=currentTag; currentTag="";}
        else if(currentTag.equalsIgnoreCase(allTag.XmlTagmedicalaid_scheme) )
         { smartPatient.medicalaid_scheme=new String(ch, start, length);previousTag=currentTag; currentTag="";}
        else if(currentTag.equalsIgnoreCase(allTag.XmlTagNr) && benefit)
         { smartPatient.pool_number=new String(ch, start, length); previousTag=currentTag;currentTag="";
         //System.out.println(previousTag+":NAHABONYE:"+SmartPatient.pool_number);
         benefit=false;
         }         
        else if(currentTag.equalsIgnoreCase(allTag.XmlTagpatient_hospitalnumber) )
         { smartPatient.patient_hospitalnumber=new String(ch, start, length);previousTag=currentTag; currentTag="";}
        else if(currentTag.equalsIgnoreCase(allTag.XmlTagamount) )
         { smartPatient.amount=new String(ch, start, length);previousTag=currentTag; currentTag="";}
        else if(currentTag.equalsIgnoreCase(allTag.XmlTagPreAuthNeeded) )
         { smartPatient.PreAuthNeeded=new String(ch, start, length); previousTag=currentTag;currentTag="";}
        else if(currentTag.equalsIgnoreCase(allTag.XmlTagClaimable) )
         { smartPatient.Claimable=new String(ch, start, length); previousTag=currentTag;currentTag="";} 
        else
         { previousTag=currentTag;currentTag="";}
         
    }
    
    public SmartPatient getPatient()
    {
        return smartPatient;
    }
    
    
    public static void main(String[] args) {
        
        new SmartXML("forwardedcarddata.xml",true);
        
        
        
    }
    
}
