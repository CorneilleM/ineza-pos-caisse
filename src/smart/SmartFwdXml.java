/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package smart;

/**
 *
 * @author HP
 */
public class SmartFwdXml {
    
    static public String smartDateFormat(String theDate)
{
    theDate=theDate.trim();
    System.out.println("before:"+(theDate.length()));
    String yy=theDate.substring(theDate.length()-4,theDate.length());
    System.out.println("yy:"+(yy));
    String mm=theDate.substring(theDate.length()-7,theDate.length()-5);
    System.out.println("mm:"+(mm));
    String dd=theDate.substring(theDate.length()-10,theDate.length()-8);
    System.out.println("hapa:"+(dd));  
    
    return yy+"-"+mm+"-"+dd;
}

    static public String doXmlClaim(String tin,int INV_NUMBER, String Claim_Date, String Claim_Time,
            int Pool_Number, String doXmlService, 
            String Scheme_Code, String Scheme_Plan, String First_Name, String Surname,
            String Date_Of_Birth,String company_name,double cash,double credit,
            SmartPatient smartPatient,
            int Total_Services, double Gross_Amount) {
       
        String ln = "\n";
        String Claim
                = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>   " + ln + //"" +ln+  //
                "<Claim> " + "" + ln +
                "<Claim_Header> " + "" + ln + //
                "<Invoice_Number>" + INV_NUMBER + "</Invoice_Number>"+ ln
                + "<Claim_Date>" + smartDateFormat(Claim_Date) + "</Claim_Date>"+ ln
                + "<Claim_Time>" + Claim_Time + "</Claim_Time>"+ln
                + "<Pool_Number>" + Pool_Number + "</Pool_Number>"+ ln
                + "<Total_Services>" + Total_Services + "</Total_Services>" + ln
                + "<Gross_Amount>" + Gross_Amount + "</Gross_Amount>"+ ln
                + "<Provider>" + ln
                + "<Role>SP</Role>"+ ln
                + "<Country_Code>RW</Country_Code>" + ln
                + "<Group_Practice_Number>"+tin+"</Group_Practice_Number>" + ln
                + "<Group_Practice_Name>"+company_name+"</Group_Practice_Name>" + ln
                + " </Provider>" + ln
                + "<Authorization>" + ln
                + "<Pre_Authorization_Number>12</Pre_Authorization_Number>" + ln
                + "<Pre_Authorization_Amount>0</Pre_Authorization_Amount>"+ ln
                + "</Authorization>" + ln
                + "<Payment_Modifiers>" + ln
                + "<Payment_Modifier>"  + ln
                + "<Type>"+smartPatient.Type2+"</Type>" + ln
                + "<Amount>"+cash+"</Amount>"+ ln
                + "<Receipt>"+INV_NUMBER+"</Receipt>" + ln
                + "</Payment_Modifier>" + ln
                + "<Payment_Modifier>" + ln
                + "<Type>5</Type>"+ ln
                + "<NHIF_Member_Nr></NHIF_Member_Nr>" + ln
                + "<NHIF_Contributor_Nr></NHIF_Contributor_Nr>" + ln
                + "<NHIF_Employer_Code></NHIF_Employer_Code>" + ln
                + "<NHIF_Site_Nr></NHIF_Site_Nr>" + ln
                + "<NHIF_Patient_Relation></NHIF_Patient_Relation>" + ln
                + "<Diagnosis_Code></Diagnosis_Code>"+ ln
                + "<Admit_Date>1900-01-01</Admit_Date>" + ln
                + "<Discharge_Date>1900-01-01</Discharge_Date>" + ln
                + "<Days_Used>0</Days_Used>"+ ln
                + "<Amount>0</Amount>" + ln
                + "</Payment_Modifier>" + ln
                + "</Payment_Modifiers>" + ln
                + "</Claim_Header>" + ln + // 
                "<Member>" + ln
                + "<Membership_Number>"+smartPatient.medicalaid_number+"</Membership_Number>" + ln + //			
                "<Scheme_Code>" + Scheme_Code + "</Scheme_Code>" + ln + //			
                "<Scheme_Plan>" + Scheme_Plan + "</Scheme_Plan>" + ln + //			
                "</Member>" + ln + //		
                "<Patient>" + ln + //	
                "<Dependant>Y</Dependant>"+ ln +
                "<First_Name>" + First_Name + "</First_Name>"+ ln + //		
                "<Middle_Name></Middle_Name>" + ln +  	
                "<Surname>" + Surname + "</Surname>" + ln + //		 
                "<Date_Of_Birth>" + smartPatient.patient_dob + "</Date_Of_Birth>" + ln + //   
                "<Gender>"+smartPatient.patient_gender+"</Gender>" + ln +
                "</Patient>" + ln + 
                "<Claim_Data>" + ln + 
                "<Discharge_Notes>Text Data as Discharge</Discharge_Notes>" + ln + //  
                doXmlService + ln + //
                "</Claim_Data>" + ln
                + //
                "</Claim>";
        return Claim;

    }

    static public String doXmlService(String tin, int Number, String Invoice_Number, 
            String Start_Date, String Start_Time,
            String Role, String Reason, String doXmlDignostic,int number) {
        String ln = "\n";
        String service = "  <Service>" + ln + //
                "<Number>" + number + "</Number>" + ln + //
                "<Invoice_Number>" + Invoice_Number + "</Invoice_Number>" + ln + //
                "<Global_Invoice_Nr>"+Invoice_Number+"</Global_Invoice_Nr>"+ln+
                "<Start_Date>" + smartDateFormat(Start_Date) + "</Start_Date>" + ln + //
                "<Start_Time>" + Start_Time + "</Start_Time>" + ln + //
                "<Provider>" + ln + //
                "<Role>" + Role + "</Role>"+ ln + //
                "<Practice_Number>"+tin+"</Practice_Number>" + ln + //
                "</Provider>" + ln + //
                doXmlDignostic + ln + //
                "<Reason>" + Reason + "</Reason>" + ln + //
                " </Service>";

        return service;

    }

    static public String doXmlDignostic(String Stage, String Code_Type, String Code, String Encounter_Type, String Code_Type1, String Code1, String Code_Description,
            int Quantity,
            double Total_Amount) {
        String ln = "\n";
        String diagnostic = "<Diagnosis> " + ln + //
                "<Stage>" + Stage + "</Stage> " + ln + //
               "<Code_Type>UNK</Code_Type>" + ln +
                "<Code>UNK</Code>" + ln + //"<Code>" + Code.substring(1, Code.length()) + "</Code>" + ln +
                "</Diagnosis>" + ln + //
                "<Encounter_Type>MEDICATION</Encounter_Type>" + ln + //
                "<Code_Type>MCODE</Code_Type>" + ln + 
                "<Code>" + Code1.substring(1,Code1.length()) + "</Code>" + ln + //
                "<Code_Description>" + Code_Description + "</Code_Description>" + ln + //
                "<Quantity>" + Quantity + "</Quantity>" + ln + //
                "<Total_Amount>" + Total_Amount + "</Total_Amount>" + ln;

        return diagnostic;

    }
    
        static public String doXmlPrescription(String Stage, String Code_Type, String Code, 
                String Encounter_Type,
            String Dosage, String Code_Type1, String Code1, String Code_Description,
            int Quantity) {
        String ln = "\n";
        String diagnostic = "<Prescription> " + ln + //
                "<Stage>" + Stage + "</Stage> " + ln + //
                "<Code_Type>" + Code_Type + "</Code_Type>" + ln +
                "<Code>" + Code.substring(1, Code.length()) + "</Code>" + ln + //
                "</Prescription>" + ln + //
                "<Encounter_Type>PRESCRIPTION</Encounter_Type>" + ln + //
                "<Code_Type>MCODE</Code_Type>" + ln + 
                "<Code>" + Code1.substring(1,Code1.length()) + "</Code>" + ln + //
                "<Code_Description>" + Code_Description + "</Code_Description>" + ln + //
                "<Quantity>" + Quantity + "</Quantity>" + ln + //
                "<Dosage>" + Dosage + "</Dosage>" + ln;

        return diagnostic;

    }

}
