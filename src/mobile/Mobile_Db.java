/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mobile;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Date;
import java.util.LinkedList;
import javax.swing.JOptionPane;
import rra.RRA_PACK;
import rra.RRA_PRINT2;
import pos.Product;

/**
 *
 * @author Kimenyi
 */
public class Mobile_Db {
    
     
String connectionURL ;  
ResultSet outResult ;
Connection conn = null;
Statement s; 
PreparedStatement psInsert; 
String server,  dataBase;
double tvaRateA, tvaRateB, tvaRateC, tvaRateD; 
String datez;
 Mobile_Db(String server,String dataBase)
{   
this.server=server;
this.dataBase=dataBase;
connectionURL = "jdbc:derby://"+server+":1527/" + dataBase  ;
read();  
}
 private  void read()
{ Date d= new Date();

        String day ;
        String mm ;
        String year ;


         if(d.getDate()<10) {
        day="0"+d.getDate();
    }
        else {
        day=""+d.getDate();
    }
        if((d.getMonth()+1)<10) {
        mm="0"+(d.getMonth()+1);
    }
        else {
        mm=""+(d.getMonth()+1);
    }
        if((d.getYear()-100)<10) {
        year="0"+(d.getYear()-100);
    }
        else {
        year=""+(d.getYear()-100);
    } 
                 datez =day+mm+year;
 try {
    conn = DriverManager.getConnection(connectionURL); 
    s = conn.createStatement();
    System.out.println( "connection etablie TO  "+connectionURL);  
    }
    catch (Throwable e){
 System.err.println(e+"connection FAILED TO "+connectionURL);
}    
    
}
static void andika(String kwandika, String file) {

      File f = new File(file);
      try {
          PrintWriter sorti = new PrintWriter(new FileWriter(f));
          sorti.println(kwandika);
          sorti.close();
      } catch (Throwable e) {
          JOptionPane.showConfirmDialog(null, "ECRIRE DANS " + file + " IMPOSSIBLE \n" + e);
}
}
 
 public int getIDProforma (String key )
{ 
try
{  
outResult =  s.executeQuery("select ID_PROFORMA from APP.BON_PROFORMA where   EXT_KEY='"+key+"'"); 
while ( outResult.next())
{
return outResult.getInt("ID_PROFORMA") ;  
}
 outResult.close(); 
}  catch (Throwable e)  {
/*       Catch all exceptions and pass them to
**       the exception reporting method             */
System.out.println(" . .invexception thrown:");
 insertError(e+""," V inv ");
}
return -1;
}
  void insertError(String desi,String ERR)
    { 
        try {
            psInsert = conn.prepareStatement("insert into APP.ERROR(DESI,USERNAME)  values (?,?)");

            if(desi.length()>160) {
                desi=desi.substring(0, 155);
            }
             if(ERR.length()>160) {
                ERR=ERR.substring(0, 155);
            }  
            
            psInsert.setString(1,desi );
            psInsert.setString(2, ERR);  
            psInsert.executeUpdate();
            psInsert.close();
                      
        } catch (Throwable ex) {
            System.out.println(ex);
              }
            
                    
          }
}
