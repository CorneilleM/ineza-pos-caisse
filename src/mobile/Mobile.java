/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package mobile;

/**
 *
 * @author Kimenyi
 */ 
      
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Date;
import java.util.LinkedList;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.SwingWorker;
import pos.Display_Client;
import pos.Employe;
import pos.Main;
import pos.Product;
import pos.Proforma;
import pos.StartServer;

/**
 *
 * @author Kimenyi
 */
public class Mobile  extends JFrame {

final static int port = 4444;
JButton connectButton,resetVarsButton,startIshyigaButton,clearButton; 
 
 static public int connNBR=0;
static public JTextArea textArea;  
String user=""; 
static public int Tosleep=1000;
static public int EjTosleep=0;
static public String version="ISHYIGA"; 
String []sp2=null;
Mobile_Db db;
Mobile_Monitor gm;
public static Main main;
 Display_Client dc;
boolean sendDisplay=false;
Employe umukozi;

public Mobile(String server,String dbName,pos.Cashier c,int id ) 
    {
         this.umukozi = c.db.getName(id); 
       c.db.insertLogin2(umukozi.NOM_EMPLOYE,"in",0);   
      this.main= new Main(umukozi,c , server, dbName, "");  
                                 
    this.user=user; 
    this.db=new Mobile_Db( server, dbName ); 
    
         this.dc= new Display_Client(server,umukozi.NOM_EMPLOYE ); 
      String reba  = dc.sendRequest("NDATANGIYE#NITWA#5#"+umukozi.NOM_EMPLOYE+"#1444445:55#0;1;1;nyitegure aha!!");
      if(reba!=null && reba.equals("OK"))
      {sendDisplay=true; 
     } 
      
    draw(); 
    clear(); 
    rebaNet();
    umvaAndoidPS();
    }

 public void draw()
{
    setTitle("Ishyiga MOBILE CONTROLER"); 
    this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE); 
    
    //on fixe  les composant del'interface
    Container content = getContentPane(); 
     westPane(content);

     setSize(500,800);
     setVisible(true);

}
   
public void westPane(Container content )
{ 

JPanel panel = new JPanel();
panel.setLayout(new GridLayout(4,1 ));//
panel.setPreferredSize(new Dimension(250,350)); //400=largeur350=longeur

connectButton=new JButton("CONNECT");
connectButton.setBackground(new Color(102, 255, 153));
resetVarsButton=new JButton("RESET RRA VARS");
resetVarsButton.setBackground(new Color(255, 255, 153));
startIshyigaButton =new JButton("SYNC OFFLINE");
startIshyigaButton.setBackground(new Color(51, 204, 255));
clearButton=new JButton("CLEAR");
clearButton.setBackground(new Color(255, 204, 102));
textArea=new JTextArea(20, 5);
 panel.add(connectButton); 
 panel.add(resetVarsButton );  
 panel.add(startIshyigaButton ); 
panel.add(clearButton ); 
JPanel MAIN = new JPanel();
//MAIN.setLayout(new GridLayout(1,2));
MAIN.setPreferredSize(new Dimension(400,350));
JScrollPane jt=new JScrollPane(textArea);
jt.setPreferredSize(new Dimension(350,350));
MAIN.add(jt);  MAIN.add(panel);
 
content.add(MAIN); 
 
}    
 
private void clear()
{ 
clearButton.addActionListener(new ActionListener() {
public void actionPerformed(ActionEvent ae)
{
    if(ae.getSource()==clearButton  )
    { 
 String text=textArea.getText();
  String dateD=""+new Date(); 
    dateD=dateD.replaceAll(":", "-");  
    String  file="TRACK/TRACK_"+dateD+".txt";
    db.andika(text,file);
    textArea.setText(""); 
    
    }
 }}); 
}  

public void actionPerformed(ActionEvent e) { 
  
textArea.append(e.getActionCommand());
System.out.println(e.getActionCommand());   
}
    private void umvaAndoidPS()
{ 
textArea.append("\n Lancement Ishyiga MOBILE :" ); 
/** Create a SwingWorker instance to run a compute-intens ive task */
final SwingWorker<String , String> worker = new SwingWorker<String, String>() 
{
/** Schedule a compute-intensive task in a background thread */
         @Override
protected String doInBackground() throws Exception {   
try { 
Thread t = new Thread();
while (true) 
{
if(Mobile_Monitor.queu2.size() >(Mobile_Monitor.currentQueuId))
{
    
textArea.append("\n SCD QUEU  SIZE :"+Mobile_Monitor.queu2.size()+
        " \n Processing :"+Mobile_Monitor.currentQueuId );
Mobile_Cmd rCmd=Mobile_Monitor.queu2.get(Mobile_Monitor.currentQueuId); 
if(rCmd.CMD.equals("PROFORMA"))
{
  Mobile_Cmd_JOB job   = 
 (Mobile_Cmd_JOB) Mobile_Monitor.queu2.get(Mobile_Monitor.currentQueuId); 
  
for(int j=0;j<job.listCmd.size();j++)
{    
rCmd=   job.listCmd.get(j);
 
double tot=0;double tva=0;
main.cashier.basketStart(rCmd.SENDER); 
String ibyabuze="";
String st="" ;
String list="";
    
st="CMD#"+umukozi.NOM_EMPLOYE+"#5#"+umukozi.NOM_EMPLOYE+"#"+new Date()+"#" ;

for(int i=0;i< rCmd.list.size(); i++)
{
  Mobile_Cmd.List_Cmd l=rCmd.list.get(i);
  Product prod=main.cashier.db.getProduct2(l.id_article,l.qte,l.price);
  int qte=main.cashier.db.getQuantiteV7(l.id_article);
  if(l.qte>qte)
  { 
      ibyabuze+="#  "+prod.productCode+" hari "+qte+"  waste "+l.qte; 
  }  
  tot+=prod.currentPrice*prod.qty; 
   tot+=prod.currentPrice*prod.qty; 
  double h = ((prod.currentPrice*prod.qty) / (1 + prod.tva));
  tva = tva + (h * prod.tva);
  main.cashier.current.add(prod); 
  System.out.println(prod.productName+"---------qtyDouble----------" +prod.qtyDouble);
     list+=""+prod.productCode+";"+prod.qty+";"+prod.currentPrice+";"+prod.productName+"::";
} 

Proforma profo = main.cashier.db.getProformaExtKey(rCmd.EXT_KEY);
System.out.println(rCmd.EXT_KEY);
int id= 0;
if(profo==null)
{  id= main.EndProforma(rCmd.NAME_CLIENT,(int)tot, rCmd.PLACE, (int)tva,"PROFORMAMOBILE",rCmd.EXT_KEY) ;
 if(sendDisplay) { dc.sendRequest(st+list)  ;   }
}
else
{
   id=profo.id_Proforma; 
}
    job.response+="#"+rCmd.EXT_KEY+"::"+id+"::OK::PS";  
}
job.replyClient(job.response);
}
else
    if(rCmd.CMD.equals("INVOICE"))
{
  Mobile_Cmd_JOB job   = 
 (Mobile_Cmd_JOB) Mobile_Monitor.queu2.get(Mobile_Monitor.currentQueuId); 
  
for(int j=0;j<job.listCmd.size();j++)
{    
rCmd=   job.listCmd.get(j);
 
double tot=0;
double tva=0;
main.cashier.basketStart(rCmd.SENDER); 
String ibyabuze="";

for(int i=0;i< rCmd.list.size(); i++)
{
  Mobile_Cmd.List_Cmd l=rCmd.list.get(i);
  Product prod=main.cashier.db.getProduct2(l.id_article,l.qte,l.price);
  int qte=main.cashier.db.getQuantiteV7(l.id_article);
  if(l.qte>qte)
  { 
      ibyabuze+="#  "+prod.productCode+" hari "+qte+"  waste "+l.qte; 
  }  
  tot+=prod.currentPrice*prod.qty; 
  main.cashier.current.add(prod); 
  main.setData();
  
  System.out.println(prod.productName+"---------qtyDouble  invoice  ----------" +prod.qtyDouble);
} 

String answer=main.PrintSilencieux (rCmd.NAME_CLIENT, main.TOTALPRICE2, main.TOTALPRICE2,0 ,main.TVAPRICE2,
  rCmd.PLACE,rCmd.PLACE+" "+rCmd.NAME_CLIENT+"  "+rCmd.SENDER); 

  job.response+="#"+rCmd.EXT_KEY+"::"+answer ; 
  
}

job.replyClient(job.response);
}
else if(rCmd.CMD.equals("PING"))
{    
rCmd.replyClient("CONNECTED");
}
else if(rCmd.CMD.equals("ERRORR"))
{    
System.out.println(rCmd.DATA);  

String [] sp=rCmd.DATA.split("::");
 String [][]s1=new String [sp.length][6]; 
 JTable jTable=new JTable();
 
//::1;java.lang.ArrayIndexOutOfBoundsException: length=1; index=4;2015-02-07 13:51:54; addTransaction 
//::2;java.lang.ArrayIndexOutOfBoundsException: length=1; index=4;2015-02-07 15:10:00; addTransaction 
//::3;getErrors;2015-02-09 05:29:07;java.lang.NullPointerException
//::4;getErrors;2015-02-09 05:31:51;java.lang.NullPointerException
//::5;getErrors;2015-02-09 05:34:36;java.net.SocketTimeoutException: failed to connect to /192.168.43.136 (port 4444) after 90000ms
//::6;none  ;2015-02-09 05:43:39;uri mubiki  
//::7;none  ;2015-02-09 05:44:57;uri mubiki  ::8;none  ;2015-02-09 05:46:43;uri mubiki  
 
 
for(int i=0;i< sp.length; i++)
{ 
   String [] spp = sp[i].split(";");
   
   for(int j=0;j< spp.length; j++)
{ 
  s1[i][j]=spp[j];  
} 
} 
    jTable.setModel(new javax.swing.table.DefaultTableModel(s1,new String []
            {"ID ERROR",  "Désignation", "TIME","SOURCE"}   ) );

jTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS); 
jTable.getColumnModel().getColumn(1).setPreferredWidth(300);//.setWidth(200);
jTable.doLayout(); 
jTable.validate();
JOptionPane.showMessageDialog(rootPane, jTable);
rCmd.replyClient("RECEIVED");
} 

else if(rCmd.CMD.contains("GETSTATUS"))
{ 
//"GETSTATUS#"+status+"#"+toGetStatus
 String [] sp=rCmd.CMD.split("<<"); 
 String response=main.cashier.db.getStatus((sp[1]),(sp[2])); 
System.out.println(response);
rCmd.replyClient(response);
}
else if(rCmd.CMD.contains("ACCESS"))
{ 
//"ACCESS;"+id+";"+pwd+"\nEND"
 String [] sp=rCmd.CMD.split(";"); 
 String response=main.cashier.db.getNameEmploye(Integer.parseInt(sp[1]), Integer.parseInt(sp[2])); 
System.out.println(response);
rCmd.replyClient(response);
}
else if(rCmd.CMD.equals("ITEMS"))
{   
String items=main.cashier.db.doworkOtherMobile();
System.out.println(items);
rCmd.replyClient(items);
}
else if(rCmd.CMD.equals("DPT"))
{   
System.out.println(" uno ");
String items=main.cashier.db.getTxtFamille("DPT"); 
System.out.println(" dos "+items);
rCmd.replyClient(items);
System.out.println(" tres ");
}
else if(rCmd.CMD.equals("TIERS"))
{   
System.out.println(" uno ");
String items=main.cashier.db.getTxtFamille("TIERS"); 
System.out.println(" dos "+items);
rCmd.replyClient(items);
System.out.println(" tres ");
}
else if(rCmd.CMD.equals("LOGIN"))
{
rCmd.replyClient(main.cashier.db.getEmploye(rCmd.NAME_CLIENT,rCmd.PLACE)) ;
}
Mobile_Monitor.currentQueuId++;
}// iyo hari ikintu gitegereje
else // IYO NTAKINTU GITEGEREGE RYAMA T0SLEEP MILLISECOND
{
  t.sleep(Tosleep);
}
}// END WHILE
}// END TRY

catch (Exception e) 
{
 textArea.append("\n Exception  : "+e); 
    e.printStackTrace(); 
} 
return  " "; 
}
@Override protected void done() { } 
@Override protected void process(java.util.List<String> chunks) { }
};
worker.execute();  // start the worker thread 
worker.addPropertyChangeListener(new PropertyChangeListener() { 
@Override public void propertyChange(PropertyChangeEvent evt) {
if (evt.getPropertyName().equals("progress")) { }}});  
}

  void umvaAndoidNS()
{ 
textArea.append("\n Lancement Ishyiga MOBILE :" ); 
/** Create a SwingWorker instance to run a compute-intens ive task */
final SwingWorker<String , String> worker = new SwingWorker<String, String>() 
{
/** Schedule a compute-intensive task in a background thread */
         @Override
protected String doInBackground() throws Exception {   
try { 
Thread t = new Thread();
while (true) 
{
if(Mobile_Monitor.queu2.size() >(Mobile_Monitor.currentQueuId))
{
textArea.append("\n SCD QUEU  SIZE :"+Mobile_Monitor.queu2.size()
        +" Processing :"+Mobile_Monitor.currentQueuId );
Mobile_Cmd rCmd=Mobile_Monitor.queu2.get(Mobile_Monitor.currentQueuId); 
 
    Mobile_Monitor.currentQueuId++;
}// iyo hari ikintu gitegereje
else // IYO NTAKINTU GITEGEREGE RYAMA T0SLEEP MILLISECOND
{
  t.sleep(Tosleep);
}
}// END WHILE
}// END TRY

catch (Exception e) 
{
 textArea.append("\n Exception  : "+e); 
    e.printStackTrace(); 
} 
return  " "; 
}
@Override protected void done() { } 
@Override protected void process(java.util.List<String> chunks) { }
};
worker.execute();  // start the worker thread 
worker.addPropertyChangeListener(new PropertyChangeListener() { 
@Override public void propertyChange(PropertyChangeEvent evt) {
if (evt.getPropertyName().equals("progress")) { }}}); 

}
  
public static int getQuantity(String s, String s1, int i) {
        int entier = -1;

        String in = JOptionPane.showInputDialog(null, s + s1);
        boolean done = true;

        while (in != null && i < 3 && done == true) {
            try {
                if (in.equals("")) {
                    i++;
                    in = JOptionPane.showInputDialog(null, s + " Enter a number ", "1");
                } else {
                    Integer in_int = new Integer(in);
                    entier = in_int.intValue();
                    done = false;
                    if (entier < 0 || entier == 0) {
                        entier = -1;
                    }
                } 
            } catch (NumberFormatException e) {
                i++;
                in = JOptionPane.showInputDialog(null, s + " = " + in + "= Not a number; Enter a number ");
                done = true;
            }
        }
        if (i == 3) {
            JOptionPane.showMessageDialog(null, " Too many try ", " Sorry we have to Exit ", JOptionPane.PLAIN_MESSAGE);
        }

        return entier;
    }
 private void rebaNet()
 {
      textArea.append("Lancement SCD Controler  " );
 
/** Create a SwingWorker instance to run a compute-intensive task */
final SwingWorker<String , String> worker = new SwingWorker<String, String>() 
{
/** Schedule a compute-intensive task in a background thread */
         @Override
protected String doInBackground() throws Exception {
   
  gm =   new Mobile_Monitor( ); 
try {
    
 ServerSocket socketServeur = new ServerSocket(port);
 System.out.println("Lancement du serveur");
 textArea.append("\n Lancement SCD Controler");
 textArea.append("\n Server :"+db.server );
 textArea.append("\n DB : "+db.dataBase); 
 textArea.append("\n PORT ISHYIGA : "+port);
 
 while (true) {
 Socket socketClient = socketServeur.accept();
 Mobile_Server t = new Mobile_Server(socketClient ); 
 t.start();
 }
 } catch (Exception e) { 
  textArea.append("SERVER SOCKET "+e);   
 } 
 return  " "; 
}
  /** Run in event-dispatching thread after doInBackground() completes */
@Override
protected void done() {
 
 textArea.append("  \n Result is...");                 
  
} 
 @Override
         protected void process(java.util.List<String> chunks) {
            // Get the latest result from the list
            String latestResult = chunks.get(chunks.size() - 1); 
            textArea.append("  \n Result is..."+latestResult);
         }
      };
             worker.execute();  // start the worker thread 
            textArea.append("  Running...");
      /** Event handler for the PropertyChangeEvent of property "progress" */
      
worker.addPropertyChangeListener(new PropertyChangeListener() {
@Override
public void propertyChange(PropertyChangeEvent evt) {
if (evt.getPropertyName().equals("progress")) {  // check the property name
//pbWorker.setValue((Integer)evt.getNewValue());  // update progress bar
}
}
}); 
}

 @Override
    protected void processWindowEvent(WindowEvent evt) {
        if (WindowEvent.WINDOW_CLOSING == evt.getID()) {
            //default icon, custom title
            int n = JOptionPane.showConfirmDialog(null, "EXIT ISHYIGA GAZ CONTROLER"
                    , "ATTENTION", JOptionPane.YES_NO_OPTION);
            if (n == 0) {
                 
    String text=textArea.getText();
    String dateD=""+new Date(); 
    dateD=dateD.replaceAll(":", "-");  
    String  file="TRACK/TRACK_"+dateD+".txt";
    db.andika(text,file);
    textArea.setText("");
     System.exit(0);
            }
        }
    }
  
    public static void main(String[] args) { 
           
       StartServer d;
        try {
            d = new StartServer();
          int id = Main.getQuantity("Nomero yumukozi "," : umubare",0);

           if(id!=-1)
          {
                      BufferedReader entree = null;
                      try {
                          JPanel pw=new JPanel();
                          JPasswordField passwordField = new JPasswordField(10);
                          JLabel label = new JLabel("Umubare w'ibanga: ");
                          pw.add(label);
                          
                          pw.add(passwordField);
                          passwordField.setText("");
                        //Integer   in_int.intValue();
                          JOptionPane.showConfirmDialog(null, pw);
                           Integer in_int=new Integer(passwordField.getText());
                          int carte = in_int.intValue();
                          LinkedList<String> give= new LinkedList();
                          File productFile= new File("log.txt");
                          String  ligne =null;
                          entree = new BufferedReader(new FileReader(productFile));
                          try{ ligne=entree.readLine(); }
                          catch (IOException e){ System.out.println(e);}
                          int i=0;
                          while(ligne!=null)
                          {
                         System.out.println(i+ligne);
                          give.add(ligne);
                          i++;

                          try{ ligne =entree.readLine();}
                          catch (IOException e){ JOptionPane.showMessageDialog(null,"Ikibazo"," reba log ",JOptionPane.PLAIN_MESSAGE);}
                          }
                          entree.close();
                          int frw =0;// getIn("Amafaranga mutangiranye "," : INT",0);
                          // String dataBase  = (JOptionPane.showInputDialog(frame, " dataBase ","score"));
                          //String serv="localhost";
//                          bac=give.get(0);
                          String dataBase=give.get(2);
                          String server=give.get(1); 
                                   String smartdb="",smartserver="";
         
                    smartdb="NOT AVAILABLE";
                    smartserver="NOT AVAILABLE"; 
                
//                          JOptionPane.showMessageDialog(null," 1 cashier" ," in ",JOptionPane.PLAIN_MESSAGE);
                          pos.Cashier c  =new  pos.Cashier(id,server,dataBase,"RWF",smartserver,smartdb);
//                          JOptionPane.showMessageDialog(null," 2 cashier"+c.db," in ",JOptionPane.PLAIN_MESSAGE);
                          if(c!=null && c.db.check_db(id,carte))
                          {      
                          new Mobile(server,dataBase,c ,id) ;
                          
                          } else {
                              JOptionPane.showMessageDialog(null,"  UMUBARE WIBANGA SIWO "," IKIBAZO ",JOptionPane.PLAIN_MESSAGE);
                          }
                      } catch (IOException ex) {
                      JOptionPane.showMessageDialog(null," ex "+ex," IKIBAZO ",JOptionPane.PLAIN_MESSAGE);
    
                      }  finally {
                          try {
                              entree.close();
                          } catch (IOException ex) {
                    JOptionPane.showMessageDialog(null," ex "+ex," IKIBAZO ",JOptionPane.PLAIN_MESSAGE);
               
                          }
                      }
          }
        } catch (Throwable ex) {
         JOptionPane.showMessageDialog(null," ex "+ex," IKIBAZO ",JOptionPane.PLAIN_MESSAGE);
      
        }
         
    } 
}
