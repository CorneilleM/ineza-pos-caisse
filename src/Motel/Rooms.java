/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
*/
package Motel;

/**
*
* @author Yves
*/
public class Rooms {

int room_number;
String libelle;
int level;
int tv, ID, VIEW, ID_PRODUCT_ROOM;
int phone;
int baignoir;
int douche;
int wifi;
String statut;
String lits;
double price ;
int frigo;
String categorie;
String view;
double PRIX_DOUBLE,PRIX_SINGLE;
double PRIX_GROUPE;
double CURRENTPRICE;
UneReservation une;
public Rooms(int room_number) {

    this.room_number = room_number;
}

public Rooms(int room_number, String nbLits, String statut, double price, int tv, int phone,
        int douche, int baignoir, int fredge, String cat, String view, int ID, int VIEW, int ID_PRODUCT_ROOM,
        double PRIX_DOUBLE, double PRIX_GROUPE) {

    this.ID = ID;
    this.room_number = room_number;
    this.VIEW = VIEW;
//  this.level = level;
    this.tv = tv;
    this.phone = phone;
    this.baignoir = baignoir;
    this.douche = douche;
    this.ID_PRODUCT_ROOM = ID_PRODUCT_ROOM;
    this.statut = statut;
    this.lits = nbLits;
    this.price = price ;
    this.PRIX_SINGLE= price ; 
    this.frigo = fredge;
    this.categorie = cat;
    this.view = view;
    this.PRIX_DOUBLE = PRIX_DOUBLE;
    this.PRIX_GROUPE = PRIX_GROUPE;
}

public boolean isReserved() {

    if (this.statut.matches("RESERVED")) {
        return true;
    } else {
        return false;
    }
}

public boolean isOccuped() {

    if (this.statut.matches("OCCUPED")) {
        return true;
    } else {
        return false;
    }
}

public String getStatut() {

    return this.statut;
}

public String getCategorie() {

    return this.categorie;
}

public double getPrice() {

    return this.price;
}

public String img(String path, String status) {
    String img = "<table   cellpadding='2' cellspacing='2' >"
            + "  <tbody>"
            + "   <tr>"
            + "   <td><img  width=40 height=40"
            + " src='" + path + "/" + status + ".jpg'></td> "
            + "      <td> " + setVirgule((int) price) + " RWF <br> " + categorie + " </td>"
            + "    </tr>"
            + "   <tr>"
            + "   <td> <img width=40 height=40 "
            + " src='" + path + "/" + view + ".jpg'></td>"
            + " <td>  " + lits + " <br> " + tv + " N° " + room_number + "   </td> "
            + " </tr> "
            + " </tbody> </table>";

    return img;
}

String setVirgule(int frw) {

    String setString = "" + frw;
    int l = setString.length();
    if (l < 2) {
        setString = "    " + frw;
    } else if (l < 3) {
        setString = "  " + frw;
    } else if (l < 4) {
        setString = "  " + frw;
    }

    String vir = " ";


    if (l > 3 && l <= 6) {
        setString = setString.substring(0, l - 3) + vir + setString.substring(l - 3);
    }
    if (l > 6) {

        String s1 = setString.substring(0, l - 3);
        int sl = s1.length();
        setString = s1.substring(0, sl - 3) + vir + s1.substring(sl - 3) + vir + setString.substring(l - 3);

    }

    return setString;
}

public int getRoomNumber() {
    return this.room_number;
}
}
