/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */ 
package Motel;
 
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import java.util.LinkedList;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel; 

/**
 *
 * @author Kimenyi
 */
public class HotelRooms {
    
static int currentView;
static String dateIn,dateOut,path;
static Client c; 
static Rooms [] lesRooms;
static String IBYEMEWE;
public static JPanel centerPane(int currentView,String dateIn, 
        String dateOut,String path,String numClient,String IBYEMEWE ){
         
JPanel CommonCenterPane = new JPanel();
CommonCenterPane.setLayout(new GridLayout(6,6));
JPanel centeUpPane = new JPanel();
centeUpPane.setPreferredSize(new Dimension (600,400));
HotelRooms.currentView=currentView;
HotelRooms.dateIn= dateIn;
HotelRooms.dateOut= dateOut;
HotelRooms.c=AutoOut.getClient(numClient,AutoOut.connStatic ); 
HotelRooms.path=path;
HotelRooms.lesRooms = AutoOut.getRoomsSTATIC(1,AutoOut.statementStatic,AutoOut.connStatic) ; 
HotelRooms.IBYEMEWE=IBYEMEWE;
// String checkin = HotelRooms.dateIn.getText();
// String checkout = HotelRooms.dateOut.getText();

UneReservation [] statusArray= AutoOut.getReservationsTime
        (HotelRooms.dateIn, HotelRooms.dateOut,currentView,AutoOut.statementStatic,AutoOut.connStatic);

JButton [] arrayButton=new JButton [36];
//System.err.println(statusArray[22]);
for(int i=0;i<36;i++)
{
JButton b=new JButton(""); 
b.setName(""+lesRooms[i].ID);
b.addActionListener(
new ActionListener() { 
public void actionPerformed(ActionEvent ae) 
{    
int room_number = Integer.parseInt(((JButton)ae.getSource()).getName()); 
String choix = "";  
boolean creeUneNouvelle=false;
//System.out.println("HAPA : "+"UP");

LinkedList <UneReservation>  liste =  AutoOut.getReservations(lesRooms[room_number].room_number,
        HotelRooms.dateIn, HotelRooms.dateOut,AutoOut.statementStatic,AutoOut.connStatic );  
System.out.println("UP KOO "+liste.size());

if(liste.size() > 0)
{ 
 UneReservation reservationexistante = (UneReservation) JOptionPane.showInputDialog(null, "Fait votre choix",
"les reservations",JOptionPane.QUESTION_MESSAGE, null, liste.toArray(), ""); 
if(reservationexistante!=null)
{
if(  reservationexistante.statut.matches("RESERVED"))
choix = (String) JOptionPane.showInputDialog(null, "RES "+reservationexistante.toString2(), " ",
JOptionPane.QUESTION_MESSAGE, null, new String[]{ "OCCUPIED", "NON DISPO", "FREE","SET UP"," CONSOMMATION"}, "OCCUPIED");
if(  reservationexistante.statut.matches("OCCUPIED"))
choix = (String) JOptionPane.showInputDialog(null, "RES "+reservationexistante.toString2(), " ",
JOptionPane.QUESTION_MESSAGE, null, new String[]{   "CHECK OUT"}, "CHECK OUT");
if(  reservationexistante.statut.matches("FREE"))
choix = (String) JOptionPane.showInputDialog(null, "RES "+reservationexistante.toString2(), " ",
JOptionPane.QUESTION_MESSAGE, null, new String[]{ "RESERVED" ,"NON DISPO", "OCCUPIED","SET UP"," CONSOMMATION"}, "FREE");
if(  reservationexistante.statut.matches("NON DISPO"))
 choix = (String) JOptionPane.showInputDialog(null, "RES "+reservationexistante.toString2(), " ",
 JOptionPane.QUESTION_MESSAGE, null, new String[]{ "RESERVED" ,"FREE", "OCCUPIED","SET UP"," CONSOMMATION"}, "FREE");

System.out.println("UPI "+choix);

if(  choix.equals("FREE"))
{ 
creeUneNouvelle=AutoOut.updateReservation("ETAT='PASSIF'", 
reservationexistante.num_reservation, "",AutoOut.statementStatic,AutoOut.connStatic); 
} 
}
/////////////////////////////////////
}
if(creeUneNouvelle==false)
{
choix = (String) JOptionPane.showInputDialog(null, "RESERVATION ", " ",
JOptionPane.QUESTION_MESSAGE, null, new String[]{"RESERVED", "OCCUPIED", "NON DISPO","SET UP"," CONSOMMATION"}, "RESERVED");

 
{
    System.out.println(" nshya ");
if(HotelRooms.c!=null && choix!=null)
{
//    System.out.println(" room "+db.lesRooms[room_number]);
//    System.out.println(" client "+c.NUM_AFFILIATION);
//     System.out.println(" price "+db.lesRooms[room_number].getPrice());
    UneReservation reservationNouveau= new UneReservation( 0,lesRooms[room_number],HotelRooms.c
,choix,""+new Date(), HotelRooms.dateIn , HotelRooms.dateOut,
         lesRooms[room_number].getPrice(),0) ;   
  // System.out.println(" res "+reservationNouveau);
AutoOut.addReservation(reservationNouveau,AutoOut.connStatic);
JOptionPane.showMessageDialog(null,  lesRooms[room_number].room_number+" IS "+ choix+" BY  "+HotelRooms.c.NOM_CLIENT, " SAVED ", JOptionPane.PLAIN_MESSAGE);
}
else
{
if(HotelRooms.c==null)
JOptionPane.showMessageDialog(null,   " OPERATION ABORTED \n SELECT A CLIENT ", " ", JOptionPane.PLAIN_MESSAGE);

if(choix==null)
JOptionPane.showMessageDialog(null,   " OPERATION ABORTED \n MAKE A CHOICE ", " ", JOptionPane.PLAIN_MESSAGE);
}
} 
}}  });  

if(!lesRooms[i].categorie.equals("UNAVAILABLE") && IBYEMEWE.contains(lesRooms[i].categorie))
if(statusArray[i]!=null)
b.setText("<html>  "+ lesRooms[i].img(HotelRooms.path,statusArray[i].statut)+"   </html>");
else
b.setText("<html>  "+  lesRooms[i].img(HotelRooms.path,"FREE")+"   </html>"); 

arrayButton[i]=b;
}    
CommonCenterPane.setBorder(BorderFactory.createLineBorder(Color.black));
for(int i=0;i<36;i++)
CommonCenterPane.add(arrayButton[i]); 

return CommonCenterPane  ;
}
public static void main(String[] args) {
    // TODO code application logic here
}
}
