/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package Motel;
   
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.util.LinkedList;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableCellRenderer;
/**
 *
 * @author Kimenyi
 */

public class RoomsView extends JFrame {

JMenuBar menu_bar1 = new JMenuBar();
JMenu menu1 = new JMenu("Fichier");
JTable table;   
Color [][] s1Color;
//String mode;
public int found=1;
int id_calcul3; 
String umukozi;
 
public RoomsView(Rooms [] reservation   ) 
{   
setData( reservation)  ; 
menu_bar1.add(menu1);
this.setJMenuBar(menu_bar1);
Container content = getContentPane();  
westPane(content); 
this.setTitle("ROOM STATUS   ");
this.setSize(1100,800);
this.setVisible(true); 

}     
private void westPane(  Container content )
{
    JPanel  productPane  = new JPanel();
    productPane.setPreferredSize(new Dimension (1100,600));
    
 
  JScrollPane jTableList =new JScrollPane(table);
    jTableList.setPreferredSize(new Dimension(1090,590));

    productPane.add(jTableList); 
    content.add(productPane);
    
    
}    
 

String setVirgule(double doub)
{
    
int frw=(int)doub;
String setString = ""+frw;
int l =setString.length();
if(l>3 && l<=6)
setString= setString.substring(0,l-3)+" "+setString.substring(l-3);
if(l>6)
{
String s1 = setString.substring(0,l-3);
int sl=s1.length();
setString=s1.substring(0,sl-3)+" "+s1.substring(sl-3)+" "+setString.substring(l-3)  ;
}
return setString;
}    
 
private void setData(Rooms []  rooms) { 
     
String [] title= new String[]{"ROOM#", "STATUS", "VIEW", " COST ", "BED "
,   "CLIENT NAME", "DATE IN", "DATE OUT"} ; 

int linge = rooms.length;
String[][] s1 = new String[linge][9];
s1Color=new Color[linge][9];
for (int i = 0; i < linge; i++) {
    
Rooms ro=    rooms[i]; 

System.out.println(ro.ID);

if(ro.ID>1)
{
s1[i][0] =""+ro.ID;
s1[i][1] = "FREE" ;
s1[i][2] = "" +ro.VIEW;
s1[i][3] = "" + ro.CURRENTPRICE;
s1[i][4] = "" + ro.lits;

UneReservation o = ro.une;

if(o!=null)
{ 
s1[i][1] = o.statut;
s1[i][5] = "" + o.client.NOM_CLIENT;
s1[i][6] = "" + o.date_arrive.substring(0, 11);
s1[i][7] = "" + o.date_depart.substring(0, 11); 
 
        if(o.statut.equals("RESERVED")) {
        s1Color[i][1]=  Color.YELLOW;
    }
        else if(o.statut.equals("OCCUPIED")) {
        s1Color[i][1]=  Color.ORANGE;
    }
        else if(o.statut.equals("FREE")) {
        s1Color[i][1]=  Color.GREEN;     
    }     
        else if(o.statut.equals("NON DISPO")) {
        s1Color[i][1]=  Color.RED;
    }    
        
        s1Color[i][ 2]=  Color.white;         
}
else
{
s1[i][5] = ""  ;
s1[i][6] = ""  ;
s1[i][7] = "";
    
}
}
else
{
s1[i][0] ="" ;
s1[i][1] = "" ;
s1[i][2] = "" ;
s1[i][3] = "" ;
s1[i][4] = "" ;
s1[i][5] = "" ;
s1[i][6] = "" ;
s1[i][7] = "" ;
 
     
}




        }
setTable(title,s1);
        

} 

 void setTable(String titles[],String [][] arraytoshow)
{  
                    MyRenderer myRenderer = new MyRenderer();
                    table=new javax.swing.JTable(); 
            table.setModel(new javax.swing.table.DefaultTableModel(arraytoshow, 
           titles
                    ));
            table.setDefaultRenderer(Object.class, myRenderer);
            table.setAutoResizeMode(table.AUTO_RESIZE_ALL_COLUMNS);
//            table.getColumnModel().getColumn(1).setPreferredWidth(350);//.setWidth(200);
            table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            table.doLayout();
            table.validate();   
}
  
public class MyRenderer extends DefaultTableCellRenderer  
{ 
    public Component getTableCellRendererComponent(JTable table, Object value, boolean   isSelected, boolean hasFocus, int row, int column) 
{ 
    Component c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column); 

     if(s1Color[row][column]!=null)
      c.setBackground(s1Color[row][column]);   
      
    return c;
    
}

}
}
