/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */ 

package Motel;
import com.barcodelib.barcode.DataMatrix;
import java.awt.*;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
/**
 *
 * @author Kimenyi
 */
public class Print extends JFrame {

LinkedList <Lot> lotList;
int pageNum = 0;
private Properties properties = new Properties();
int margin=0;
String numero; 
Client client;
String date,reference,mode,modality;
 
AutoOut db;
String devise=" RWF";
boolean byempurimwe=false;
String codeBar2D=" ";
double taux=1;
double [] totaux;
public Print(LinkedList <Lot> lotList,AutoOut db,String mode,
          String reference,String date,
        Client client,String numero,double [] totaux)  
{ 
this.codeBar2D= numero; 
this.taux=1;//i.DEVISE_INVOICE; 
this.devise=" FRW";//i.MONNAIE_INVOICE; 
this.reference =reference;
this.mode=mode;
this.date=date;
this.client= client;
//this.footer=i.footer;
this.numero=numero;   
this.modality=" RELEVE RESERVATION : ";//i.MODE; 
this.lotList=lotList;
this.db=db;
this.margin=getValue("margin");
this.totaux=totaux;
this.byempurimwe=PrintCommand(); 

} 
private boolean PrintCommand()
{
    PrintJob pjob = getToolkit().getPrintJob(this, "Ishyiga ", properties);
    if (pjob != null) {
    //    System.out.println("helloooooooooooo"+pjob);
    Graphics pg = pjob.getGraphics();
    //System.out.println("helloooooooooooo"+pg);
    if (pg != null) {
//System.out.println("helloooooooooooo");
    printCommande(pjob, pg);
    pg.dispose();
    }
    pjob.end();
    return true ;
    }
    else
        return false;
}

private void printCommande (PrintJob pjob, Graphics pg) {
   if (!(pg instanceof PrintGraphics)) {
      throw new IllegalArgumentException ("Graphics context not PrintGraphics");
    }

     int pageW = pjob.getPageDimension().width - margin;

  if (pg != null)
  {
    BufferedInputStream bis = null;
    try {
        
         System.err.println(pjob.getPageDimension().height);
    Font helv = new Font("Helvetica", Font.BOLD, getValue("dateT"));
    pg.setFont(helv);
    Image img=null;
    bis = new BufferedInputStream(new FileInputStream(getString("logoV")));
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    try {
    int ch;
    while ((ch = bis.read()) != -1) {
    baos.write(ch);
    System.err.println(ch);
    }
    System.err.println(img);
    img = Toolkit.getDefaultToolkit().createImage(baos.toByteArray());
     System.err.println(img);
    System.err.println(img.getWidth(rootPane));
    } catch (IOException exception) {
    System.err.println("Error loading: ");
    }
    pg.drawString(getString("ville")+", le " + date.substring(0, 2) + "/" + date.substring(2, 4) + "/20" + date.substring(4, 6), getValue("dateX"), getValue("dateY"));
    helv = new Font("Helvetica", Font.BOLD, 25);
    pg.setFont(helv);

    Thread t = new Thread();
    t.start();
    t.sleep(5000);
   // pg.drawImage(img, getValue("logoX")+margin, getValue("logoY"), pageW-30, 50, rootPane);
    int largeurLogo=200;
    int longeurLogo=45;
    
    if(getValue("largeurLogo")!=0 && getValue("longeurLogo")!=0)
    {
    largeurLogo=getValue("largeurLogo");
    longeurLogo=getValue("longeurLogo");
    } 
    pg.drawImage(img, getValue("logoX")+margin, getValue("logoY"), largeurLogo, longeurLogo, rootPane);

    helv = new Font("Helvetica", Font.PLAIN, getValue("adresseT"));
    //have to set the font to get any output
    pg.setFont(helv);
    FontMetrics fm = pg.getFontMetrics(helv);
    int fontHeight = fm.getHeight();
    int fontDescent = fm.getDescent();

    pg.drawString(getString("adresseV"), getValue("adresseX")+margin,  getValue("adresseY"));
    pg.drawString(getString("rueV"), getValue("rueX")+margin,  getValue("rueY"));
    pg.drawString(getString("localiteV"), getValue("localiteX")+margin,  getValue("localiteY"));
    pg.drawString(getString("telV"), getValue("telX")+margin, getValue("telY"));
    pg.drawString(getString("faxV"), getValue("faxX")+margin, getValue("faxY"));
    pg.drawString(getString("emailV"), getValue("emailX")+margin, getValue("emailY"));
    pg.drawString(getString("tvaV"), getValue("tvaX")+margin,  getValue("tvaY"));
    
    DataMatrix barcode = new DataMatrix();
                try {
                     barcode.setData(codeBar2D);
                     int dim=65;
                    pg.drawImage(barcode.renderBarcode(),getValue("codebarX"), getValue("codebarY"),dim,dim,  rootPane);
                } catch (Exception ex) {
                    Logger.getLogger(Print.class.getName()).log(Level.SEVERE, null, ex);
                }
    
    
    pg.drawRect(getValue("clientX"), getValue("clientY"), getValue("clientLongeur"), getValue("clientLargeur"));

    
    //pg.setColor(Color.BLACK);
//pg.fillRect(x, yy, getValue("clientLongeur")+cadre, getValue("clientLargeur")+cadre);
//pg.setColor(Color.WHITE);
//pg.fillRect(getValue("clientX"), getValue("clientY"), getValue("clientLongeur"), getValue("clientLargeur"));
//pg.setColor(Color.BLACK);


//        drawRect(getValue("clientX"), getValue("clientY"), getValue("clientLongeur"), getValue("clientLargeur"));
//pg.fillRect(tva, tva, pageW, fontHeight) 
     Font helv2 = new Font("Helvetica", Font.BOLD, getValue("clientT")); 
    pg.setFont(helv2);
    //System.out.println(client);
    
    pg.drawString(client.NOM_CLIENT, getValue("clientX") + 10,  getValue("clientY") + 10);
    
    helv2 = new Font("Helvetica", Font.ITALIC, getValue("clientT"));
    pg.setFont(helv2);
    pg.drawString(client.EMPLOYEUR, getValue("clientX" ) + 10, getValue("clientY" ) + 25);
    pg.drawString(client.SECTEUR, getValue("clientX" ) + 10,  getValue("clientY" ) + 40);

    Font helv1 = new Font("Helvetica", Font.BOLD, getValue("factureT"));
    pg.setFont(helv1);
   //System.out.println("FACTURE "+getString("factureV") + (numero)+getValue("factureX")+margin+ getValue("factureY"));
    String racine=getString("factureV");
      
    pg.drawString(modality+" "+numero, getValue("factureX")+margin, getValue("factureY"));
 // pg.drawString("FACTURE "+getString("factureV") + (numero), getValue("factureX")+margin, getValue("factureY"));
    helv2 = new Font("Helvetica", Font.PLAIN, getValue("referenceT"));
    pg.setFont(helv2); 
    if(reference!=null)
    pg.drawString(getString("referenceV") + reference, getValue("referenceX")+margin, getValue("referenceY"));
   
    helv1 = new Font("Helvetica", Font.BOLD, 10);
    pg.setFont(helv1);
    pageNum++;
    
// "BK 040-0015420-30 RWF ","BK 040-6015420-95","BK 040-6715420-46 EURO"
/* pg.drawString( "BK 040-0015420-30 RWF", 20, pageH - 15);
pg.drawString( "BK 040-6015420-95 USD", (pageW / 2)-30, pageH - 15);
pg.drawString( "BK 040-6715420-46 EURO", pageW -110, pageH - 15);*/ 
// pg.drawString("Page  " + pageNum, pageW / 2, pageH - 9

    pg.drawString("Page  " + pageNum, pageW / 2, getValue("pageNum") );

    helv2 = new Font("Helvetica", Font.BOLD, 10);
    pg.setFont(helv2);


            int g = getValue("length");
            String[] entete = new String [g];
            int[] longeur = new int[g+1];
            String[] variable= new String [g];
            int [] dimension=new int[g];
            longeur[0]=0;

            int page=1;


            int y=  getValue("entete"+page+"X");

            for(int k=0;k<g;k++)
            {

            entete[k]=getString("entete_"+(k+1)+"Valeur");
            longeur[k+1]=getValue("entete_"+(k+1)+"Largeur");

            variable[k]=getString("entete_"+(k+1)+"Variable");
            dimension[k]=getValue("entete_"+(k+1)+"Dimension");
 // System.out.println(" d   "   +getValue("entete_"+(k+1)+"Dimension"));
            }

            int w = margin;

            int curHeightLigne = getValue("ligne"+page+"X");

            Font helvLot = new Font("Helvetica", Font.PLAIN, 10);
            pg.setFont(helvLot);
            FontMetrics fmLot = pg.getFontMetrics(helvLot);
            fontHeight = fmLot.getHeight();
            boolean done = true;
System.out.println(curHeightLigne+"     "+fontHeight);
for (int j = 0; j < lotList.size(); j++) 
{
            helvLot = new Font("Helvetica", Font.PLAIN, getValue("tailleLigne"));
            pg.setFont(helvLot);
            Lot ci = lotList.get(j);
            w = margin;
            if (done) {
            curHeightLigne += fontHeight;
            } else {
            curHeightLigne += 2 * fontHeight;
            }
            done = true;
            for (int i = 0; i < g; i++) {
            w += (longeur[i]);
            String valeur=ci.vars[i];
            String variab=variable[i];
            int dim      =dimension[i];
    if(variab.contains("int")|| variab.equals("double")) {
            printInt(pg,setVirguleD(valeur), w + (longeur[i + 1]) - 5, curHeightLigne);
        }
            else if (valeur.length() < dim)
            {                
            pg.drawString(valeur, w + 5, curHeightLigne);
            }
            else
            {
            done = false;
            int index=kataNeza(valeur,dim);
            pg.drawString(valeur.substring(0,index), w + 5, curHeightLigne);
            pg.drawString(valeur.substring(index, valeur.length()), w + 5, fontHeight + curHeightLigne);
            }
            } 
           int marginFotter =getValue("marginfooter");
          if ((curHeightLigne+20) >  marginFotter-20) {

            helv1 = new Font("Helvetica", Font.BOLD, getValue("enteteTaille"));
            pg.setFont  (helv1);
            w = margin;
            for (int i = 0; i < g; i++) {
            w += (longeur[i]);
            pg.drawLine(w,y, w, marginFotter             );//ligne zihagaze
            pg.drawString(entete[i], w + 2, y +15   );// amazina
            }
            pg.drawLine(margin, marginFotter, w, marginFotter );//umurongo wo hasi
            pg.drawRect(margin, y , w - margin,25   ) ; // cadre ya entete
            /////////////////////////////////////////////////////////////////////////////////////////////
            pg.dispose();
            pg = pjob.getGraphics();
            if (pg != null)
            { 
            pg.setFont(helv);
            }
            helv1 = new Font("Helvetica", Font.PLAIN, getValue("factureT"));
            pg.setFont(helv1);
            pg.drawString(modality+getString("factureV") + (numero), margin, margin + 25); 
            helv1 = new Font("Helvetica", Font.BOLD, 10);
            pg.setFont(helv1);
            pageNum++;page=2;
            pg.drawString("Page  " + pageNum, pageW / 2,getValue("pageNum"));
              curHeightLigne=getValue("entete"+page+"X")+25;
              y=getValue("entete"+page+"X");
            done = true;
            }
 }
            if((curHeightLigne+20) > (  getValue("longeurLigne")) )
            {
            int marginfooter =   getValue("marginfooter");
            helv1 = new Font("Helvetica", Font.BOLD, getValue("enteteTaille"));
            pg.setFont(helv1);
            w = margin;

            for (int i = 0; i < g; i++) {
            w += (longeur[i]);
            pg.drawLine(w,y, w, marginfooter);//ligne zihagaze
            pg.drawString(entete[i], w + 2, y +15);// amazina
            }
            pg.drawLine(margin, marginfooter, w, marginfooter);//umurongo wo hasi
            pg.drawRect(margin, y , w - margin,25 ); // cadre ya entete

            /////////////////////////////////////////////////////////////////////////////////////////////
            pg.dispose();
            pg = pjob.getGraphics();
            if (pg != null)
            {
            pg.setFont(helv);
            }
            helv1 = new Font("Helvetica", Font.PLAIN, getValue("factureT"));
            pg.setFont(helv1);
            pg.drawString(getString("factureV") + numero, margin, margin + 25);
            helv1 = new Font("Helvetica", Font.BOLD, 10);
            pg.setFont(helv1);
            pageNum++;page=2;
            pg.drawString("Page  " + pageNum, pageW / 2,getValue("pageNum"));
              curHeightLigne=getValue("entete"+page+"X")+25;
            done = true;
            
            int longeurLigne = getValue("longeurLigne");
            
            helv1 = new Font("Helvetica", Font.BOLD, getValue("enteteTaille"));
            pg.setFont(helv1);
            w = margin;
            if(pageNum>1)
                page=2;
            y=getValue("entete"+page+"X");
            for (int i = 0; i < g; i++) {
            w += (longeur[i]);
            pg.drawLine(w,y, w, longeurLigne);//ligne zihagaze
            pg.drawString(entete[i], w + 2, y + 15);// amazina
            }
            System.out.println(longeurLigne);
            pg.drawLine(margin, longeurLigne, w, longeurLigne);//umurongo wo hasi
            pg.drawRect(margin, y , w - margin, 25); // cadre ya entete 
            }
            else
            { 
                
            int longeurLigne =getValue("longeurLigne");
            System.err.println("lllllllllllllllllllllll     "+longeurLigne);
            helv1 = new Font("Helvetica", Font.BOLD, getValue("enteteTaille"));
            pg.setFont(helv1);
            w = margin;
            if(pageNum>1)
                page=2;
            y=getValue("entete"+page+"X");
            for (int i = 0; i < g; i++) {
            w += (longeur[i]);
            pg.drawLine(w,y, w, longeurLigne);//ligne zihagaze
            pg.drawString(entete[i], w + 2, y + 15);// amazina
            }
            pg.drawLine(margin, longeurLigne, w, longeurLigne);//umurongo wo hasi
            pg.drawRect(margin, y , w - margin, 25); // cadre ya entete
            }
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        w = margin;
                        helv2 = new Font("Helvetica", Font.BOLD, getValue("prixTaille"));
                        pg.setFont(helv2);
                        int lp = getValue("lengthPrix");
                        String[] enteteP = new String [lp+1];
                        int[] longeurP = new int[lp+1];
                        String[] variableP= new String [lp];
                        longeurP[0]=0;
                         
                        for(int k=0;k<lp;k++)
                        {
                        enteteP[k]=getString("prix_"+(k+1)+"Valeur");
                        if(enteteP[k].length()>2) {
                                enteteP[k]=enteteP[k]+devise;
                            }
                        longeurP[k+1]=getValue("prix_"+(k+1)+"Largeur");
                        variableP[k]=getString("prix_"+(k+1)+"Variable");
                        System.out.println(enteteP[k+1]);
                        }
                        int Xprix = getValue("longeurY");
                        int marginP=getValue("longeurX");
                        w =marginP ;
                     
                        for (int i = 0; i < lp; i++) {
                        w += (longeurP[i]); 
                       int wid=w + (longeurP[i + 1]) - 20; 
                       System.out.println("  xxxxxx      "+totaux[i]);
                       printInt(pg,setVirguleD( ""+totaux[i]),  wid, Xprix+15); 
                       
                       pg.drawLine(w,Xprix-20, w, Xprix+20);//ligne zihagaze
                       pg.drawString(enteteP[i], w + 2, Xprix - 5);// amazina 
                      //+client.devise
                        }
                         if(lp!=0)
                        {
                        pg.drawLine(marginP, Xprix, w, Xprix);//umurongo wo hasi
                        pg.drawRect(marginP, Xprix-20 , w-marginP , 40); // cadre ya entete
                         }
helv2 = new Font("Helvetica", Font.BOLD, getValue("footer_1Taille" ) );
pg.setFont(helv2);
                               
                           

            } catch (InterruptedException ex) {
                System.out.println(ex);
            } catch (FileNotFoundException ex) {
                System.out.println(ex);
            }
           finally {
                try {
                    bis.close();
                } catch (IOException ex) {
                     System.out.println(ex);
                }
            } 
  } 
} 
private int getValue(String s)
{

int ret=0;
Variable var= db.getParam("PRINT",mode,s+mode);
 if(var!=null)
 {
Double val=new Double (var.value);
ret = val.intValue();
}

return ret;
}
String getString(String s)
{
String ret=" ";
 Variable var= db.getParam("PRINT",mode,s+mode);
 if(var!=null)
     ret=var.value;
     return ret;
}
public void printInt(Graphics pg,int nbre,int w,int h)
  {
  String s=setVirgule(nbre);

  int back=0;

  for(int i=s.length()-1;i>=0;i--)
  {
      if(s.charAt(i)==' ')
      back+=2;
      else
       back+=5;

      pg.drawString(""+s.charAt(i),w-back,h);

  }
}
public void printInt(Graphics pg,String s,int w,int h)
{
int back=0;
for(int i=s.length()-1;i>=0;i--)
  {
      if(s.charAt(i)==' ')
      back+=2;
      else
       back+=5;
pg.drawString(""+s.charAt(i),w-back,h);
}
}
String setVirgule(int frw)
{
String setString = ""+frw;
int l =setString.length();
if(l>3 && l<=6)
setString= setString.substring(0,l-3)+" "+setString.substring(l-3);
if(l>6)
{
String s1 = setString.substring(0,l-3);
int sl=s1.length();
setString=s1.substring(0,sl-3)+" "+s1.substring(sl-3)+" "+setString.substring(l-3)  ;
}
return setString;
}
String setVirgule(String setString, String uburyo)
{
    String espace=" ";
    if(uburyo.equals("int"))
            espace=" ";
    else     if(uburyo.equals("akadomoInt"))
              espace=" .";
    else  if(uburyo.equals("virguleInt"))
                espace=" ,";
    
int l =setString.length();
if(l>3 && l<=6)
setString= setString.substring(0,l-3)+espace+setString.substring(l-3) ;
if(l>6)
{
String s1 = setString.substring(0,l-3);
int sl=s1.length();
setString=s1.substring(0,sl-3)+espace+s1.substring(sl-3)+espace+setString.substring(l-3)  ;
}
return setString;
} 

String setVirguleD(String frw)
{
 //   System.out.println("**********before********"+frw);
    String setString = frw;
    if(frw.contains("."))
    {
    try
    {
     double d=Double.parseDouble(frw)  ; 
     
        d =((int)(d*1000))/1000.0;
        frw=""+frw;
        // System.out.println("********after**********"+frw);
StringTokenizer st1=new StringTokenizer (frw);
String entier =st1.nextToken(".");
//System.out.println(frw);
String decimal=st1.nextToken("").replace(".", "");
if(decimal.length()==1 && decimal.equals("0") )
decimal=".00";
else if (decimal.length()==1 && !decimal.equals("0") )
decimal="."+decimal+"0";
else
decimal="."+decimal.substring(0, 2);
  setString = entier+decimal;
    }
    catch(Throwable e)
    {System.out.println(frw+e);}
    

int l =setString.length();
if(l<2)
setString = "    "+frw;
else if(l<3)
setString = "  "+frw;
else if(l<4)
setString = "  "+frw;
int up=6;
int ju=up+3;
if(l>up && l<=ju)
{
setString= setString.substring(0,l-up)+","+setString.substring(l-up) ;
}
else if(l>ju)
{
setString=setString.substring(0,l-9)+","+setString.substring(l-9,l-6)+","+setString.substring(l-6,l)  ;
}
}
else
        return setVirgule(frw);
    
return setString;
}
String setVirgule(String setString)
{
int l =setString.length();
if(l>3 && l<=6)
setString= setString.substring(0,l-3)+" "+setString.substring(l-3) ;
if(l>6)
{
String s1 = setString.substring(0,l-3);
int sl=s1.length();
setString=s1.substring(0,sl-3)+" "+s1.substring(sl-3)+" "+setString.substring(l-3)  ;
}
return setString;
} 

public String getFacture(int id,String Racine)
{
String fact=""+id; 
for(int i=Racine.length();i<8-fact.length();i++)
Racine=Racine+"0"; 
fact=Racine+fact; 
return fact ;  
}

public static int kataNeza(String m,int l)
{
    String [] espaces=m.split(" ");
    int s=espaces.length;
    int index=0;
    for(int i=0;i<s-1;i++)
    {
        espaces[i]=espaces[i]+" ";
    }
    
    System.out.println(espaces.length);
    for(int i=0;i<s;i++)
    { 
        System.out.println("izina"+espaces[i]+"**length:"+espaces[i].length());
        index+=espaces[i].length();
        if(index>l)
        {
            index-=espaces[i].length();
            break;
        }
    }
    
    System.out.println("index:"+index);
    System.out.println(m.substring(0, index));
    return index; 
}


public static void main(String[] arghs)
{
 }

}
