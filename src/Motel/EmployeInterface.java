/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package Motel;
import java.awt.*;
import java.awt.event.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import javax.swing.JFrame;
/**
 *
 * @author aimable
 */
public class EmployeInterface extends JFrame{


private  JTextField
        ID_EMPLOYE ,
	NOM_EMPLOYE  ,
	PRENOM_EMPLOYE  ,
	BP_EMPLOYE ,
	CATRE_INDETITE ,
	LEVEL_EMPLOYE  ,
	TITRE_EMPLOYE  ,
	PWD_EMPLOYE  ,
	TEL_EMPLOYE  ;


private JLabel
        LID_EMPLOYE ,
	LNOM_EMPLOYE  ,
	LPRENOM_EMPLOYE  ,
	LBP_EMPLOYE ,
	LCATRE_INDETITE ,
	LLEVEL_EMPLOYE  ,
	LTITRE_EMPLOYE  ,
	LPWD_EMPLOYE  ,
	LTEL_EMPLOYE  ;

private JButton send;
private AutoOut db;
private Employe c ;


String  what;

EmployeInterface( AutoOut db )
{
this.db=db;
what="";
this.c=new Employe(0,0,0,0,"","","","","");
 makeFrame();
    send();
    draw(); 

}
EmployeInterface(AutoOut db,Employe c)
{
    what="UPDATE";
    this.db=db;
    this.c=c;
 makeFrame();
    send();
    draw();


}

public void makeFrame()
{

        this.ID_EMPLOYE = new JTextField();
	this.NOM_EMPLOYE  = new JTextField();
	this.PRENOM_EMPLOYE  = new JTextField();
	this.BP_EMPLOYE = new JTextField();
	this.CATRE_INDETITE = new JTextField();
	this.LEVEL_EMPLOYE  = new JTextField();
	this.TITRE_EMPLOYE  = new JTextField();
	this.PWD_EMPLOYE  = new JTextField();
	this.TEL_EMPLOYE  = new JTextField();

         ID_EMPLOYE.setText(""+c.ID_EMPLOYE);
	NOM_EMPLOYE.setText(c.NOM_EMPLOYE);
	PRENOM_EMPLOYE.setText(c.PRENOM_EMPLOYE);
	BP_EMPLOYE.setText(c.BP_EMPLOYE);
	CATRE_INDETITE.setText(""+c.CATRE_INDETITE);
	LEVEL_EMPLOYE.setText(""+c.LEVEL_EMPLOYE);
	TITRE_EMPLOYE.setText(c.TITRE_EMPLOYE);
	PWD_EMPLOYE.setText(c.PWD_EMPLOYE);
	TEL_EMPLOYE.setText(""+c.TEL_EMPLOYE);

        this.LID_EMPLOYE=new JLabel("ID_EMPLOYE ");
	this.LNOM_EMPLOYE =new JLabel("NOM_EMPLOYE ");
	this.LPRENOM_EMPLOYE =new JLabel("PRENOM_EMPLOYE ");
	this.LBP_EMPLOYE=new JLabel("BP_EMPLOYE ");
	this.LCATRE_INDETITE=new JLabel("CATRE_INDETITE ");
	this.LLEVEL_EMPLOYE =new JLabel("LEVEL_EMPLOYE ");
	this.LTITRE_EMPLOYE =new JLabel("TITRE_EMPLOYE ");
	this.LPWD_EMPLOYE =new JLabel("PWD_EMPLOYE ");
	this.LTEL_EMPLOYE=new JLabel("TEL_EMPLOYE ");
         this.send=new JButton("ENVOYER");

}

public void draw()
{
    setTitle("Register a new Employee for Ishyiga");

    this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);


    //on fixe  les composant del'interface
    Container content = getContentPane();


    if(what.equals("UPDATE"))
     send.setText("UPDATE");

     westPane(content);

     setSize(500,600);
     setVisible(true);

}
public static int getIn (String in)
{
                      int productCode=-1;
    try
    {
        if(in.equals(""))
        {

JOptionPane.showMessageDialog(null,"Ubusa  Ntabwo ari  umubare ",in,JOptionPane.PLAIN_MESSAGE);
        productCode=0;
        }
        else
        {
        Integer in_int=new Integer(in);
        productCode = in_int.intValue();
        if(productCode<0  )
             {

JOptionPane.showMessageDialog(null,"Ikibazo mumubare ",""+productCode,JOptionPane.PLAIN_MESSAGE);
        productCode=0;
        }


        }
         }
    catch (NumberFormatException e)
    {
     JOptionPane.showMessageDialog(null,in," Ntabwo ari  umubare ",JOptionPane.PLAIN_MESSAGE);

    productCode=0;
    }
    return productCode;
}



/**************************************************************************
affiche du stock
**************************************************************************/

public void westPane(Container content )
{
JPanel Pane  = new JPanel();

Pane.setPreferredSize(new Dimension (600,700));

JPanel panel = new JPanel();
panel.setLayout(new GridLayout(9,2));
panel.setPreferredSize(new Dimension (300,500));

panel.add(LID_EMPLOYE);panel.add(ID_EMPLOYE);
panel.add(LNOM_EMPLOYE);panel.add(NOM_EMPLOYE);
panel.add(LPRENOM_EMPLOYE);panel.add(PRENOM_EMPLOYE);
panel.add(LBP_EMPLOYE);panel.add(BP_EMPLOYE);
panel.add(LCATRE_INDETITE);panel.add(CATRE_INDETITE);
panel.add(LLEVEL_EMPLOYE);panel.add(LEVEL_EMPLOYE);
panel.add(LTITRE_EMPLOYE);panel.add(TITRE_EMPLOYE);
panel.add(LPWD_EMPLOYE);panel.add(PWD_EMPLOYE);
panel.add(LTEL_EMPLOYE);panel.add(TEL_EMPLOYE);
Pane.add(panel,BorderLayout.CENTER);
Pane.add(send,BorderLayout.SOUTH);

content.add(Pane, BorderLayout.WEST);

}

/**********************************************************************************************
AJOUT Mannuel
**********************************************************************************************/
public void send()
{

	send.addActionListener(new ActionListener() {
	public void actionPerformed(ActionEvent ae)
	{
    if(ae.getSource()==send  )
    {

	int vID_EMPLOYE =getIn(ID_EMPLOYE.getText());
	String vNOM_EMPLOYE= NOM_EMPLOYE.getText();
	String vPRENOM_EMPLOYE = PRENOM_EMPLOYE.getText();
	String vBP_EMPLOYE = BP_EMPLOYE.getText();
	int vCATRE_INDETITE = getIn(CATRE_INDETITE.getText());
	int vLEVEL_EMPLOYE =getIn(LEVEL_EMPLOYE.getText());
	String vTITRE_EMPLOYE = TITRE_EMPLOYE.getText();
	String vPWD_EMPLOYE = PWD_EMPLOYE.getText();
	int vTEL_EMPLOYE =getIn(TEL_EMPLOYE.getText());

Employe p=new Employe(vID_EMPLOYE, vCATRE_INDETITE,vLEVEL_EMPLOYE,vTEL_EMPLOYE, vNOM_EMPLOYE ,vPRENOM_EMPLOYE ,vBP_EMPLOYE ,vTITRE_EMPLOYE,vPWD_EMPLOYE );
if(what.equals("UPDATE"))
db.upDateEmp(p,vID_EMPLOYE);
else
db.insertEmp(p);





                        }
			}});

}
 @Override
protected void processWindowEvent(WindowEvent evt)
{
   if(WindowEvent.WINDOW_CLOSING == evt.getID())
   {
	   this.dispose();
			//default icon, custom title
//JOptionPane.showMessageDialog(this,"RIRAFUNGIRA RIMWE N'ISHYIGA"," IKIBAZO",JOptionPane.PLAIN_MESSAGE);
        			//this.setDefaultCloseOperation(HIDE_ON_CLOSE);


	}

}


public static void main(String[] arghs)
{
        try {
            StartServer d = new StartServer();
            AutoOut db = new AutoOut("localhost","ishyiga");
            db.read();

            //new PersonInterface(db, "", "RAMA");

        } catch (Throwable ex) {
            Logger.getLogger(Client_Interface.class.getName()).log(Level.SEVERE, null, ex);
        }

}
}
