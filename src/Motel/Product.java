package Motel;

/* Product for Ishyiga copyright 2007 Kimenyi Aimable
*/


import java.io.*;
import java.util.*;
import javax.swing.JOptionPane;


class Product implements  Comparable
{

	int productCode,qty;
	String productName,code;
        Lot courrant;
        String codeBar,observation,codeSoc;
        double tva,currentPrice,prix_revient;
        private int quantity ;
        int choixLot=1;
        AutoOut db;
        int qtyLive;
       //         courrant.qtyLive;
Product(int productCode,String productName,String code,int qty ,double currentPrice,double tva,String codeBar,String observation,String codeSoc,AutoOut db)
{
    this.productCode=productCode;
    this.productName=productName;
    if(productName.length()>55)
    this.productName=productName.substring(0, 50);
    this.code=code;
    this.qty =qty;
    this.currentPrice=currentPrice;
    this.db=db;
    this.tva=tva;
    this.codeBar=codeBar;
    this.observation=observation;
    this.codeSoc=codeSoc;
    }
Product(int productCode,String productName,String code,int qty ,double currentPrice,double tva,String codeBar,String observation,String codeSoc,AutoOut db,double pr)
{
		this.productCode=productCode;
        this.prix_revient=pr;
		this.productName=productName;
                        if(productName.length()>55)
                   this.productName=productName.substring(0, 50);
                this.code=code;
                this.qty =qty;
                this.currentPrice=currentPrice;
                this.db=db;
                this.tva=tva;
                this.codeBar=codeBar;
                this.observation=observation;
                this.codeSoc=codeSoc;
}
public int qty ()
{
		return qty;
}
/* gestion de vente */

public String toPrint()
{
    String nom="";
    
  if(productName.length()>20)
  nom=productName.substring(0,20)+".";
  else 
     nom=productName;
    
    int i = nom.length();
          
  while(i<=20)
  {
  nom=nom+" ";
  i++;
  }

return nom+" * "+qty+" = "+currentPrice*qty+" rwf";
}


// Qte Live
public int  getQuantite()
{
    int g =0;
    //System.out.println(codeSoc);
    
  //   JOptionPane.showMessageDialog(null,observation,db.observa,JOptionPane.PLAIN_MESSAGE);
  if(!observation.equals("NULL")&& (db.observa.contains("_RAMA")||db.observa.contains("_MMI")))
   // if(!observation.equals("NULL"))
    {
        g=JOptionPane.showConfirmDialog(null,db.observa+" dit : "+this.observation,"Observation de "+this.productName,JOptionPane.INFORMATION_MESSAGE);
    // System.out.println(g);
    }
    if(g!=0)
        g=0;
    else
        g=db.getQuantite(productCode);
    
    return g;
}

/* gestion du stock de ce produit voir la qty */
public boolean check(int i)
{
    
    qtyLive =getQuantite();
	boolean done=false;
	 if( qtyLive >= i)
	 		{
	 			done=true;
	 		}
         else//////////////////////////////////////////////////////
             db.insertMissed(productName,i,currentPrice,qtyLive );
        
return done;
}
public double salePrice()
{
   	return currentPrice*qty;
}
public int productCode()
{
	return productCode;
}

    @Override
public String toString()
{
   
return productName+"****"+setVirgule((int)currentPrice)+" RWF";
}
    
public String toSell()
{
  String g ="****"+ codeSoc;
return productCode+"****"+qty+"****"+(int)(currentPrice*qty)+" RWF"+"****"+productName+g;
}
public String toStock()
{
   // getQuantite();
return productCode+"***"+productName;//+"****"+courrant.id_LotS+"****"+courrant.qtyLive;
}
public String toSee()
{
    return productName+"****"+setVirgule((int)currentPrice)+" RWF";
}

public Product clone(int q)
{
    
Product p= new Product(productCode,productName, code, qty,currentPrice,tva,codeBar,observation,codeSoc,this.db,prix_revient);
p.qty=q;

return p;

}
String setVirgule(int frw)
        {

String setString = ""+frw;
int l =setString.length();

if(l>3 && l<=6)
setString= setString.substring(0,l-3)+"."+setString.substring(l-3) ;
if(l>6)
{
    
String s1 = setString.substring(0,l-3);
int sl=s1.length();
setString=s1.substring(0,sl-3)+"."+s1.substring(sl-3)+"."+setString.substring(l-3)  ;   
        
}
       
return setString;
}
public Product cloneRet(int q)
{
Product p= new Product(productCode,productName, code, qty,currentPrice,tva,codeBar,observation,codeSoc,this.db);
p.qty=-q;

return p;

}
public int compareTo( Object o )
    {
        if( o instanceof Product ){
           Product r = (Product)o;


           return this.productName.compareTo( r.productName );
        }
        return -1;
    }

}