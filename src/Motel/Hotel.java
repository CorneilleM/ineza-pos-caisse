/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
*/
package Motel;

/**
*
* @author Yves
*/  

import com.toedter.calendar.JCalendar;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import pos.Main;

public class Hotel extends JFrame {
 
private javax.swing.JLabel LESRESERVATIONS;
private javax.swing.JLabel LESCLIENTS;
private LinkedList<UneReservation> lesReservations; 
//private JList les_reservations = new JList();
private JTable les_reservations = new JTable();
private JList lesClients = new JList();
private JPanel roomsPane = new JPanel();
private JPanel vipRoomsPane = new JPanel();
private JPanel meetingsRoomsPane = new JPanel();
private JPanel timeCheckPanel = new JPanel();
private JCalendar dateIn;//= new JTextField("2012-02-01 18:34:50.943");
private JCalendar dateOut;
static Font police;
private JScrollPane scrollPaneReservationList, scrollPaneClientList; 
private JButton newClient = new JButton("CLIENT"); 
private JButton Rapport = new JButton("RAPPORT");
private JButton checkAvailable = new JButton("CHECK");
private JButton t1, t2, t3, t4;
JButton prev = new JButton("PREV.");
JButton next = new JButton("NEXT");
JButton print =new JButton("PRINT");
UneReservation resConsommation2 =null;
int minView = 1;
int currentView = 1;
int maxView = 5;
JButton[] arrayButton;
//private static JFrame frame;
private String currentprice="PRICE";
private JTextField searchClient=new JTextField();
Employe umukozi = null;
String date = "", empTitle;
AutoOut db;
String nom;
static String bac = "";
static String dbIkaze = "";
static String servIkaze = "";
//    String path="file:///C:/Users/Kimenyi/Desktop/2014/RRA/ISHYIGA/RRA SOFT/PharmaRRA HOTEL 4.0.5/";
//    String path="file:///C:/Users/toshiba/Desktop/ISHYIGA PROJECT/PharmaRRA HOTEL 4.0.5/";
String path = "";
Main main = null;
Cashier cashier;
JTable jTableRapport = new JTable();
private LinkedList<UneReservation> listReservation;
private LinkedList<Lot> listConsommation2;
 MatriceEtat mt;  
 Hotel(String server, String dbName, Employe umukozi, String datesT, 
        String path2, String empTitle,Cashier cashier) 
{
     String smartdb="",smartserver="";
         
                    smartdb="NOT AVAILABLE";
                    smartserver="NOT AVAILABLE";  
    pos.Cashier c=new pos.Cashier(umukozi.ID_EMPLOYE, server, dbName,"RWF",smartserver,smartdb);
    
        this.main= new Main(new pos.Employe
(umukozi.ID_EMPLOYE, umukozi.CATRE_INDETITE,umukozi.LEVEL_EMPLOYE,umukozi.TEL_EMPLOYE, 
umukozi.NOM_EMPLOYE ,umukozi.PRENOM_EMPLOYE ,umukozi.BP_EMPLOYE ,umukozi.TITRE_EMPLOYE,
umukozi.PWD_EMPLOYE,"FRA","",""),c, server, dbName, "");
        this.mt= new MatriceEtat( );
        setTitle("ISHYIGA HOTEL " + umukozi.NOM_EMPLOYE + " SESSION");
    
       this.path=path2;
       
      System.out.println("  +++++++++++++++++++++++++++++++++     " +path);
      
    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//    frame = this;
    this.empTitle = empTitle;
    this.umukozi = umukozi;
    nom = umukozi.NOM_EMPLOYE;
    dateIn = new JCalendar();
    dateIn.putClientProperty("JCalendar.footerStyle", "Today");
    dateOut = new JCalendar();
    db = new AutoOut(server, dbName);
    this.cashier=cashier;  
    db.read();
    db.getRooms2(currentView,currentprice); 
    System.out.println(this.path );
    
    removeReser();
    manageViews();
    draw();
    showReservations(); 
    client();
    rapport();
    lesclientsEcoute();
    mngConsommation();
//    actionSurListe();
    actualize();
    superSearchAff();
    t1();
    t2();
    t3();
    NEXT();
    PREV();
    print();
//    }
//    else
//    {
//        String pat=JOptionPane.showInputDialog(null, "PATH FOR PICTURE IS NOT DEFINED","",JOptionPane.WARNING_MESSAGE);
//    
//        db.insertInsurance(
// new  Insurance("pathMAIN",pat,"RUN",pat));
//    }
    
}

private void draw() {

    Container content = getContentPane();
//        westPane(content);
    centerPane2(content);
    eastPane(content);
    enableEvents(WindowEvent.WINDOW_CLOSING);
    //on fixe  les composant del'interface
    setSize(1300, 770);
    if (!empTitle.equals("SERVEUR")) {
        setVisible(true);
    }
}

private void manageViews() {
    minView = db.getMinView();
    maxView = db.getMaxView();
}

private void client() {
    newClient.addActionListener(
            new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent ae) {
                    if (ae.getSource() == newClient) {
                        if (lesClients.getSelectedValue() != null) {
                            Client cc = (Client) lesClients.getSelectedValue();
                            new Client_Interface(db, cc);
                        } else 
                        {
                            new Client_Interface(db);
                        }
                    }
                }
            });
}

private void NEXT() {
    next.addActionListener(
            new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent ae) {
                    if (ae.getSource() == next) {
                        if (currentView + 1 <= maxView) {
                            currentView++;
                            actualizeInterface(currentprice);
                        } else {
                            JOptionPane.showMessageDialog(null, "TU DEPASSE LES BORNES! " + maxView);
                        }
                    }
                }
            });
}

private void PREV() {
    prev.addActionListener(
            new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent ae) {
                    if (ae.getSource() == prev) {
                        if (currentView - 1 >= minView) {
                            currentView--;
                            actualizeInterface(currentprice);
                        } else {
                            JOptionPane.showMessageDialog(null, "TU DEPASSES LES BORNES! " + minView);
                        }
                    }
                }
            });
}

private void actualize() {

checkAvailable.addActionListener(
        new ActionListener() { 
            @Override
            public void actionPerformed(ActionEvent ae) {
                if (ae.getSource() == checkAvailable) {
                    //JOptionPane.showMessageDialog(null, " CHECK AVAILABILY=TY ", " ERROR ", JOptionPane.PLAIN_MESSAGE); 

                    actualizeInterface(currentprice);
                }

            }
        });
}

private void t1() {
t1.addActionListener(
        new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                if (ae.getSource() == t1) {
                    currentprice = "PRICE";
                    actualizeInterface(currentprice);
                }
            }
        });
}

private void t2() {
t2.addActionListener(
        new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                if (ae.getSource() == t2) {
                    currentprice = "PRIX_DOUBLE";
                    actualizeInterface(currentprice);
                }
            }
        });
}

private void t3() {
t3.addActionListener(
        new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                if (ae.getSource() == t3) {
                    currentprice = "PRIX_GROUPE";
                    actualizeInterface(currentprice);
                }
            }
        });
}

void actualizeInterface(String price) {
    if (dateIn.getDate().after(dateOut.getDate())) {
        JOptionPane.showMessageDialog(null, " DATE IN IS AFTER DATE OUT ", " ERROR ", JOptionPane.PLAIN_MESSAGE);
    } else {
        db.getRooms2(currentView,price);

        cashier.allClients=db.allClients();
        UneReservation[] statusArray = AutoOut.getReservationsTime(akiraDateSql(dateIn.getDate(), "00:00:00"),
                akiraDateSql(dateOut.getDate(), "23:59:00"), currentView, db.s, db.conn);
                setData(db.allReservations()); 
        lesClients.setListData(cashier.allClients.toArray());
        scrollPaneClientList.updateUI();
//JOptionPane.showMessageDialog(null, "HELLLOO ", " ERROR ", JOptionPane.PLAIN_MESSAGE); 

        for (int i = 0; i < 36; i++) {
            arrayButton[i].setName("" + i);
            arrayButton[i].setText("<html>   </html>");
            //System.out.println(statusArray[i]);
            if (!db.lesRooms[i].categorie.equals("UNAVAILABLE")) {
                
                if (statusArray[i] != null && statusArray[i].chambre.room_number==db.lesRooms[i].room_number) {
                         String txt="<html>  " + db.lesRooms[i].img(path, statusArray[i].statut) + "   </html>";
                         System.out.println(txt);
                         arrayButton[i].setText(txt);
                    } else { 
                        String txt="<html>  " + db.lesRooms[i].img(path, "FREE") + "   </html>";
                        System.out.println(txt);
                        arrayButton[i].setText(txt);
                     
                }
            }
        }
        //   arrayButton[5*currentView].setText("<html> HMMM ACT   </html>");
        Client c = (Client) lesClients.getSelectedValue();
        if (c != null) { 
            setData(AutoOut.getReservationDunClient(akiraDateSql(dateIn.getDate(), "00:00:00"),
            akiraDateSql(dateOut.getDate(), "23:59:00"),c.NUM_AFFILIATION, db.s, db.conn));
            lesClients.clearSelection();
            lesClients.setListData(cashier.allClients.toArray());
            scrollPaneClientList.updateUI();
        }
    }

}

private void showReservations() {

//           lesRooms = cashier.allRooms;  
    lesClients.setListData(cashier.allClients.toArray());
    setData(db.allReservations()); 
}

 
public void eastPane(Container content) 
{
    
    JPanel CommonWestPane = new JPanel();
    JPanel ReservationPane = new JPanel();
    JPanel clientPane = new JPanel();
    CommonWestPane.setPreferredSize(new Dimension(350, 600));
    CommonWestPane.setLayout(new GridLayout(3, 1));
    LESRESERVATIONS = new JLabel();
    LESCLIENTS = new JLabel();
    
    police = new Font("Arial",Font.BOLD,11);

    t1 = new JButton("SIMPLE");
    t1.setBackground(new Color(255, 220, 100));
    t1.setFont(police);
    t2 = new JButton("DOUBLE");
    t2.setBackground(new Color(192, 192, 192));
    t2.setFont(police);
    t3 = new JButton("GROUPE");
    t3.setBackground(new Color(0, 250,100));
    t3.setFont(police);
    t4 = new JButton("");
    t4.setBackground(new Color(250, 175, 175));
    t4.setFont(police);

    police = new Font("GungsuhChe", Font.BOLD, 26);
    LESRESERVATIONS.setFont(police); // NOI18N
    LESRESERVATIONS.setText(" LISTE DES RESERVATIONS");

    police = new Font("GungsuhChe", Font.BOLD, 26);
    LESCLIENTS.setFont(police); // NOI18N
    LESCLIENTS.setText(" LISTE DES CLIENTS");

    scrollPaneClientList = new JScrollPane(lesClients);
    scrollPaneClientList.setPreferredSize(new Dimension(350, 170));
    lesClients.setBackground(Color.PINK);

    scrollPaneReservationList = new JScrollPane(les_reservations);
    scrollPaneReservationList.setPreferredSize(new Dimension(350, 200));
    les_reservations.setBackground(new Color(255, 255, 180));

    ReservationPane.add(LESRESERVATIONS, BorderLayout.NORTH);
    ReservationPane.add(scrollPaneReservationList, BorderLayout.CENTER);
    clientPane.add(LESCLIENTS, BorderLayout.NORTH);
    clientPane.add(scrollPaneClientList, BorderLayout.CENTER);
    
    JPanel paneC = new JPanel();
//     paneC.setPreferredSize(new Dimension(20, 5)); 
     paneC.add(new JLabel("SEARCH CLIENT : "), BorderLayout.WEST);
     paneC.add(searchClient, BorderLayout.EAST);  
     searchClient.setPreferredSize(new Dimension(160,20));
     clientPane.add(paneC,BorderLayout.SOUTH);  
//     clientPane.add(new JLabel());

    JPanel panelSouth = new JPanel();
    // panelSouth.setLayout(new GridLayout(2,1));
    JPanel paneNew = new JPanel();

    paneNew.setPreferredSize(new Dimension(300, 30));
    paneNew.setLayout(new GridLayout(1, 2));
    paneNew.add(newClient);
    paneNew.add(Rapport);
    // downPane.add(paneNew,BorderLayout.SOUTH); 
    timeCheckPanel.setPreferredSize(new Dimension(350, 150));
    timeCheckPanel.setLayout(new GridLayout(1, 2));
    //timeCheckPanel.add(dateCheckin);
    //timeCheckPanel.add(dateCheckOUT); 
    timeCheckPanel.add(dateIn);
    timeCheckPanel.add(dateOut);
    panelSouth.add(timeCheckPanel, BorderLayout.NORTH);

    JPanel paneNewM = new JPanel();
    paneNewM.setPreferredSize(new Dimension(350, 50));
    paneNewM.setLayout(new GridLayout(1, 2));

    JPanel nextPrev = new JPanel();
    nextPrev.setLayout(new GridLayout(2, 2)); 
checkAvailable.setBackground(new Color(0, 200, 240));
    nextPrev.add(checkAvailable);
    nextPrev.add(RadioButton());
     
    nextPrev.add(prev);
    nextPrev.add(next);
    
 
    
    
    JPanel paneChoixM = new JPanel();
//    paneChoixM.setLayout(new GridLayout(1, 2));
    paneChoixM.setLayout(new GridLayout(2, 2));
    paneChoixM.add(t1);
    paneChoixM.add(t2);
    paneChoixM.add(t3);
    paneChoixM.add(t4); 
    


    paneNewM.add(nextPrev);
    paneNewM.add(paneChoixM);

    panelSouth.add(paneNewM, BorderLayout.CENTER);
    panelSouth.add(paneNew, BorderLayout.SOUTH);

    CommonWestPane.add(ReservationPane);
    CommonWestPane.add(clientPane);
    CommonWestPane.add(panelSouth);
    
    content.add(CommonWestPane, BorderLayout.WEST);
}

String akiraDateSql(Date d, String heure) {
    String day , mm,year ;

    if (d.getDate() < 10) {
        day = "0" + d.getDate();
    } else {
        day = "" + d.getDate();
    }
    if ((d.getMonth() + 1) < 10) {
        mm = "0" + (d.getMonth() + 1);
    } else {
        mm = "" + (d.getMonth() + 1);
    }
    if ((d.getYear() - 100) < 10) {
        year = "0" + (d.getYear() - 100);
    } else {
        year = "" + (d.getYear() - 100);
    }
    String checkin = "20" + year + "-" + mm + "-" + day + " " + heure;
    //System.out.println(checkin); 
    return checkin;
}

public JPanel RadioButton() {

//Create the radio buttons.
    JRadioButton SINGLE = new JRadioButton("S");
    SINGLE.setMnemonic(KeyEvent.VK_P);
    SINGLE.setActionCommand("S"); 
    JRadioButton DOUBLE = new JRadioButton("D");
    DOUBLE.setMnemonic(KeyEvent.VK_L);
    DOUBLE.setActionCommand("DOUBLE");
    JRadioButton VIP = new JRadioButton("V");
    VIP.setMnemonic(KeyEvent.VK_F);
    VIP.setActionCommand("VIP");
    VIP.setSelected(true);
    ButtonGroup group = new ButtonGroup();

    group.add(SINGLE);
    group.add(DOUBLE);
    group.add(VIP); 

//Register a listener for the radio buttons.
    SINGLE.setName("SINGLE");
    DOUBLE.setName("DOUBLE");
    VIP.setName("VIP"); 
    
    SINGLE.addActionListener(fataAction());
    VIP.addActionListener(fataAction());
    DOUBLE.addActionListener(fataAction()); 
//Put the radio buttons in a column in a panel.
    JPanel radioPanel = new JPanel(new GridLayout(2, 2));
    radioPanel.add(SINGLE);
     radioPanel.add(DOUBLE);
    radioPanel.add(VIP); 

    return radioPanel;
}

ActionListener fataAction() {
    return new ActionListener() {

        @Override
        public void actionPerformed(ActionEvent ae) {
            UneReservation[] statusArray = AutoOut.getReservationsTime(akiraDateSql(dateIn.getDate(), "00:00:00"),
                    akiraDateSql(dateOut.getDate(), "23:59:00"), currentView, db.s, db.conn);
//  les_reservations.setListData(db.allReservations.toArray());  

            for (int i = 0; i < 36; i++) {
                arrayButton[i].setText("<html>   --  </html>");
                String categorie = ((JRadioButton) ae.getSource()).getName();
                // System.err.println(categorie);
                if (db.lesRooms[i].getCategorie().equals(categorie)) {
                    if (statusArray[i] != null) {
                        String txt="<html>  " + db.lesRooms[i].img(path, statusArray[i].statut) + "   </html>";
                        System.out.println(txt);
                        arrayButton[i].setText(txt);
                    } else {
                        
                        String txt="<html>  " + db.lesRooms[i].img(path, "FREE") + "   </html>";
                        System.out.println(txt);
                        arrayButton[i].setText(txt);
                    }
                }
            }
        }
    };
}

void setData(LinkedList<UneReservation> reservation) {
 this.listReservation=reservation;
int linge = reservation.size();
String[][] s1 = new String[linge][7];
for (int i = 0; i < linge; i++) {
UneReservation o = reservation.get(i);
s1[i][0] =""+ o.chambre.room_number;
s1[i][1] = o.statut;
s1[i][2] = "" + o.client.NOM_CLIENT;
s1[i][3] = "" + o.date_arrive.substring(0, 11);
s1[i][4] = "" + o.date_depart.substring(0, 11); 
}
les_reservations.setModel(new javax.swing.table.DefaultTableModel(s1, 
        new String[]{"ROOM#", "STATUS", "CLIENT NAME", "DATE IN", "DATE OUT"}));

les_reservations.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
//les_reservations.getColumnModel().getColumn(1).setPreferredWidth(250);//.setWidth(200);
les_reservations.doLayout();
les_reservations.validate(); 
}
private void removeReser(){
les_reservations.addMouseListener(new java.awt.event.MouseAdapter() {

    @Override
    public void mouseClicked(java.awt.event.MouseEvent e){
    
        int row=les_reservations.rowAtPoint(e.getPoint());
        
        
        UneReservation reserv=(UneReservation)listReservation.get(row);
        int i=JOptionPane.showConfirmDialog(null, "VOULEZ VOUS CHANGER CETTE RESERVATION \n"+reserv);
        
        
        
        if (i==0){
        
String checkin = JOptionPane.showInputDialog(null, " CHECK IN \n"+reserv,""+reserv.date_arrive);
 String checkout= JOptionPane.showInputDialog(null, " CHECK OUT \n"+reserv,""+reserv.date_depart);
String statut  = (String) JOptionPane.showInputDialog(null, " STATUS " , " ",
                        JOptionPane.QUESTION_MESSAGE, null, new String[]
                        {"RESERVED","OCCUPIED", "DELETED", "FREE" }, reserv.statut);
                              
            db.upDateStatus3(reserv.num_reservation,   statut,  checkin,  checkout);
//            lesReservations.remove(row);
            System.out.println(reserv.chambre.room_number+" ,"+reserv.client.NOM_CLIENT+" ,"+" CANCELED "+" ,"+reserv.date_arrive+" ,"+reserv.date_depart);
            
             setData(db.allReservations()); 
//            setData(lesReservations);
        }
       System.err.println("The choice is: "+i+"the row selected is: "+row+" data: "+reserv);
    }
});}
private void mngConsommation(){
jTableRapport.addMouseListener(new java.awt.event.MouseAdapter() {

    @Override
    public void mouseClicked(java.awt.event.MouseEvent e){
    
        int row=jTableRapport.rowAtPoint(e.getPoint());
        
        
        Lot l=(Lot)listConsommation2.get(row);
        
        int i=JOptionPane.showConfirmDialog(null, "MOVE ROOM CONSUMMATION"
                + " \n"+l.u1);  
        if (i==0){  
            
            
            
           String newRes = (String) JOptionPane.showInputDialog(null, " MOVE ROOM " , " ",
 JOptionPane.QUESTION_MESSAGE, null, new String[]{"ATTENTE","PERTE","MOVE"}, "ATTENTE"); 
            if(newRes!= null && newRes.equals("MOVE"))
            {
              
               newRes = ""+cashier.db.getRes(l.u11);  
                                               
            }   
                
              if(newRes!= null && !newRes.equals(""))
            {   
                if(l.u1!=null && (l.u1.contains("PRO") || l.u1.contains("INV")  ))
            {
              if( (l.u1.contains("PRO")  ))
            {
                db.upDateProformaAttente(l.u1.replaceAll("PRO", ""),newRes); 
            }  
              else if( l.u1.contains("INV"))
            {
              db.upDateInvoiceAttente(l.u1.replaceAll("INV", ""),newRes);   
            }   
                
            }
            else
            {
                JOptionPane.showMessageDialog(null, "CHANGEMENT NE PAS POSSIBLE "," CHANGEMENT",JOptionPane.PLAIN_MESSAGE);

            }
            }
           else
              {
JOptionPane.showMessageDialog(null, "CHANGEMENT ECHOUE "," CHANGEMENT",JOptionPane.PLAIN_MESSAGE);

              }
              }
       System.err.println("The choice is: "+i+"the row selected is: "+row+" data: ");
    }
});}

public boolean manageButtonReservationList(LinkedList<UneReservation> liste,int room_number)
{
    String choix="";
    boolean updateSuccessfully=false;
         if (liste.size() > 0) {
                            
UneReservation reservationexistante = (UneReservation) JOptionPane.showInputDialog(null, "Fait votre choix",
        "les reservations", JOptionPane.QUESTION_MESSAGE, null, liste.toArray(), "");
                            
if (reservationexistante != null) {

    if (reservationexistante.statut.matches("RESERVED")) {
       choix = mt.doRun(1, reservationexistante.toString2());  
    } 
    else if (reservationexistante.statut.matches("OCCUPIED")) {
        choix = mt.doRun(2, reservationexistante.toString2()); 
    }
    else if (reservationexistante.statut.matches("FREE")) {
        choix = mt.doRun(0, reservationexistante.toString2());  
    }
    else if (reservationexistante.statut.matches("NON DISPO")) {
        choix = mt.doRun(5, reservationexistante.toString2());  
    }

                                System.out.println(" CHOIX YAKOZWE " + choix);

                                if (choix.matches("SET UP")) {
                                    new RoomInterface(db, db.lesRooms[room_number]);
                                } 
                                else if (choix.matches("OCCUPIED")) {
                                    String pay = JOptionPane.showInputDialog("DEPOSIT   ", "0");
                                    try {
                                        double DEPOSIT = Double.parseDouble(pay);
                                        updateSuccessfully = AutoOut.updateReservation("", reservationexistante.num_reservation,
                                                "STATUT='" + choix + "', deposit =" + DEPOSIT, db.s, db.conn);
                                        if (updateSuccessfully) {
                                            annuleAutreReservations(reservationexistante.num_reservation, room_number, akiraDateSql(dateIn.getDate(), "00:00:00"),
                                                    akiraDateSql(dateOut.getDate(), "23:59:00"));
                                            JOptionPane.showMessageDialog(null, " OPERATION SUCCESSFUL ", " THANK YOU", JOptionPane.PLAIN_MESSAGE);
                                        }
                                    } catch (NumberFormatException e) {
                                        JOptionPane.showMessageDialog(null, " OPERATION UNSUCCESSFUL \n WRITE WELL THE DEPOSIT ", " SORRY", JOptionPane.PLAIN_MESSAGE);
                                    }
                                } 
                                else if (choix.equals("ANNULE")) {
//System.err.println("QAZXkkkk");
                                    updateSuccessfully = AutoOut.updateReservation("ETAT='PASSIF'",
                                            reservationexistante.num_reservation, "", db.s, db.conn);
                                     if (updateSuccessfully) {
                                        annuleAutreReservations(reservationexistante.num_reservation, room_number, akiraDateSql(dateIn.getDate(), "00:00:00"),
                                                akiraDateSql(dateOut.getDate(), "23:59:00"));
                                        JOptionPane.showMessageDialog(null, " OPERATION SUCCESSFUL ", " THANK YOU", JOptionPane.PLAIN_MESSAGE);
                                    } else {
                                        JOptionPane.showMessageDialog(null, " OPERATION UNSUCCESSFUL \n SELECT A CLIENT ", " SORRY", JOptionPane.PLAIN_MESSAGE);
                                    }
                                } 
                                else if (choix.equals("NON DISPO")) 
                                {
//System.err.println("QAZX");
                                    updateSuccessfully = AutoOut.updateReservation("",
                                            reservationexistante.num_reservation, "STATUT='" + choix + "'", db.s, db.conn);
                                    if (updateSuccessfully) {
                                        annuleAutreReservations(reservationexistante.num_reservation, room_number, akiraDateSql(dateIn.getDate(), "00:00:00"),
                                                akiraDateSql(dateOut.getDate(), "23:59:00"));
                                        JOptionPane.showMessageDialog(null, " OPERATION SUCCESSFUL ", " THANK YOU", JOptionPane.PLAIN_MESSAGE);
                                    } else {
                                        JOptionPane.showMessageDialog(null, " OPERATION UNSUCCESSFUL \n SELECT A CLIENT ", " SORRY", JOptionPane.PLAIN_MESSAGE);
                                    }
                                } 
                                else if (choix.matches("CHECK OUT")) {
                                    readRoom(reservationexistante);
                                    JOptionPane.showMessageDialog(null, " CHECK OUT ", " ", JOptionPane.PLAIN_MESSAGE);
                                    updateSuccessfully = true;
                                }
                            }
/////////////////////////////////////
 }    
         return updateSuccessfully;
}

void showConsommation(int room_number)
{
            LinkedList<UneReservation> liste2 = AutoOut.getReservations(db.lesRooms[room_number].room_number,
                    akiraDateSql(dateIn.getDate(), "00:00:00"),
                    akiraDateSql(dateOut.getDate(), "23:59:00"),
                    db.s, db.conn); 
            String num_client ;
            double deposit ;
            double total=0; 
            
            if (liste2.size() > 0) {
                
              resConsommation2 = (UneReservation) JOptionPane.showInputDialog(null, "Fait votre choix",
            "les reservations", JOptionPane.QUESTION_MESSAGE, null, liste2.toArray(), "");
            num_client=resConsommation2.client.NUM_AFFILIATION; 
            deposit=resConsommation2.deposit;
            
           if(resConsommation2!=null)
{ 
           
                                    
  listConsommation2 = db.getRaport(akiraDateSql(dateIn.getDate(), "00:00:00"),
        akiraDateSql(dateOut.getDate(), "23:59:00"), resConsommation2.num_reservation
        ,num_client);

    int nbr=AutoOut.nb_joursBetweenToDate(resConsommation2.date_arrive,resConsommation2.date_depart);
  listConsommation2.addFirst(new Lot( "RES"+resConsommation2.num_reservation ,resConsommation2.date_arrive
        ,"  ","DEPOSIT  "+resConsommation2.deposit, "", 
          ""+nbr, ""+resConsommation2.montant, ""+resConsommation2.montant*nbr, resConsommation2.statut,
        "",
       num_client

        )

        );    
                                    String s1[][] = new String[listConsommation2.size()][9];
                                    for (int j = 0; j < listConsommation2.size(); j++) {

                                        Lot li = listConsommation2.get(j);
                                        s1[j][0] = li.u1;
                                        s1[j][1] = li.u2;
                                        s1[j][2] = li.u3;
                                        s1[j][3] = li.u4;
                                        s1[j][4] = li.u6;
                                        s1[j][5] = li.u8;
                                        total+=(Integer.parseInt(li.u6)*Double.parseDouble(li.u8));
                                        
                                        s1[j][6] = li.u9;
                                        s1[j][7] = li.u0;
                                        s1[j][8] = li.u11;


                                    }
                                    
                                    jTableRapport.setModel(new javax.swing.table.DefaultTableModel(s1, new String[]{"ID", "DATE", "CODE", " DES", "QTE", "PU", "EMPL", "HEURE", "SOCIETE"}));
                                    jTableRapport.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
                                    //jTable.getColumnModel().getColumn(0).setPreferredWidth(300);
                                    jTableRapport.getColumnModel().getColumn(3).setPreferredWidth(300);//.setWidth(200);
                                    jTableRapport.doLayout();
                                    jTableRapport.setRowHeight(20);
                                    jTableRapport.validate();
                                    JPanel jp= new JPanel();
                                    JScrollPane js = new JScrollPane(jTableRapport);
                                    js.setPreferredSize(new Dimension(540, 330));
jp.add(js);
jp.add(print);
 JOptionPane.showMessageDialog(null, jp, "Room " + room_number+" TOTAL  "
         +rra.RRA_PRINT2.setVirgule(total, 1 , ",")
         +" SOLDE  "+rra.RRA_PRINT2.setVirgule((total-deposit), 1 , ","),
         JOptionPane.INFORMATION_MESSAGE);
 }
 else
            {
                JOptionPane.showMessageDialog(null, " PLEASE SELECT A  RESERVATION IN "
            + "\n ROOM ID "+room_number, " ", JOptionPane.PLAIN_MESSAGE);
            }  
           
 }
            else
            {
                JOptionPane.showMessageDialog(null, " THERE IS NO DATA IN "
            + "\n ROOM ID "+room_number, " ", JOptionPane.PLAIN_MESSAGE);
            }
            
            
}


public void centerPane2(Container content) {

    JPanel CommonCenterPane = new JPanel();
    CommonCenterPane.setLayout(new GridLayout(6, 6));
    JPanel centeUpPane = new JPanel();
    centeUpPane.setPreferredSize(new Dimension(550, 400));

// String checkin = dateIn.getText();
// String checkout = dateOut.getText();

    UneReservation[] statusArray = AutoOut.getReservationsTime(akiraDateSql(dateIn.getDate(), "00:00:00"),
            akiraDateSql(dateOut.getDate(), "23:59:00"), currentView, db.s, db.conn);

    arrayButton = new JButton[36];
//System.err.println(statusArray[22]);
    for (int i = 0; i < 36; i++) {
        JButton b = new JButton("");
        b.setName("" + db.lesRooms[i].ID);
        b.addActionListener(
                new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent ae) { 
                        
                        Client c2 = (Client) lesClients.getSelectedValue();
                        int room_number = Integer.parseInt(((JButton) ae.getSource()).getName());
                         
                        LinkedList<UneReservation> liste = AutoOut.getReservations(db.lesRooms[room_number].room_number, akiraDateSql(dateIn.getDate(), "00:00:00"),
                                akiraDateSql(dateOut.getDate(), "23:59:00"), db.s, db.conn);
                        
                        System.out.println("UP KOO " + liste.size());
 boolean updateSuccessFull =  manageButtonReservationList(  liste,  room_number);
  
   if (c2 == null) { 
    JOptionPane.showMessageDialog(null, " OPERATION ABORTED "
            + "\n SELECT A CLIENT ", " ", JOptionPane.PLAIN_MESSAGE);
    updateSuccessFull=true;
   } 
  
   if (updateSuccessFull == false ) { 
       
                 String   choix = mt.doRun(0, c2.NOM_CLIENT+"  "+room_number);        
            
                 
        if (choix != null)
        {
                               
            if (choix.matches("CONSOMMATION")) 
            {   
                showConsommation(  room_number);
            }
            else if (choix.matches("SET UP")) 
            {
                new RoomInterface(db, db.lesRooms[room_number]);
            } 
            else 
            {
                                    System.out.println(" nshya ");
                                   
                UneReservation reservationNouveau = new UneReservation(0, db.lesRooms[room_number],
                        c2, choix, "" + new Date(),
                        akiraDateSql(dateIn.getDate(), "00:00:00"),
                        akiraDateSql(dateOut.getDate(), "23:59:00"),
                        db.lesRooms[room_number].getPrice(), 0);
                
                if (dateIn.getDate().after(dateOut.getDate())) {
                    JOptionPane.showMessageDialog(null, " DATE IN IS AFTER DATE OUT ", " ERROR ", JOptionPane.PLAIN_MESSAGE);
                } else {
                    AutoOut.addReservation(reservationNouveau, db.conn);
                    JOptionPane.showMessageDialog(null, reservationNouveau.toString2(), 
                            " SAVED ", JOptionPane.PLAIN_MESSAGE);

                } 
             }
                            } 
                    else {
                        JOptionPane.showMessageDialog(null, " OPERATION ABORTED \n MAKE A CHOICE ", " ", JOptionPane.PLAIN_MESSAGE);
                    }
                            
                            lesClients.clearSelection();
                        }
                   
                   
                    }
                });

        if (!db.lesRooms[i].categorie.equals("UNAVAILABLE")) {
            if (statusArray[i] != null) {
                b.setText("<html>  " + db.lesRooms[i].img(path, statusArray[i].statut) + "   </html>");
            } else {
                b.setText("<html>  " + db.lesRooms[i].img(path, "FREE") + "   </html>");
            }
        }

        arrayButton[i] = b;
    }
    CommonCenterPane.setBorder(BorderFactory.createLineBorder(Color.black));
    for (int i = 0; i < 36; i++) {
        CommonCenterPane.add(arrayButton[i]);
    }

    content.add(CommonCenterPane, BorderLayout.EAST);
}

private void print() {

    print.addActionListener(
            new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent ae) {
                    if (ae.getSource() == print) { 
                         db.printRetardReleve2(resConsommation2 );
                    }
                }
            });
}


 
 
public boolean reservationExiste(int numchambre, String numclient, String checkin, String checkout) {
    boolean ok = false;

    LinkedList<UneReservation> list = AutoOut.getReservations(db.s, db.conn);
    while (!list.isEmpty()) {
        UneReservation re = list.remove();

        if (re.chambre.room_number == numchambre && re.client.NUM_AFFILIATION.matches(numclient)
                && re.date_arrive.matches(checkin) && re.date_depart.matches(checkout)) {
            return true;
        }
    }
    return false;
}

public void annuleAutreReservations(int ntukoreho, int room_number, String checkin, String checkout) {

    AutoOut.getReservationsAnuller(ntukoreho, room_number, checkin, checkout, db.s, db.conn);
}

 
private void superSearchAff() { 
    searchClient.addKeyListener(
            new KeyListener()     {
                public void actionPerformed(ActionEvent ae) {
                }
                @Override
                public void keyTyped(KeyEvent e) { 

                    String cc=""+e.getKeyChar();
                     String find=(searchClient.getText()+ cc).toUpperCase();
                    if(cc.hashCode()==8 && find.length()>0 ) {
                        find=(find.substring(0, find.length()-1));
                    } 
                              superSearchPatient(find);
                        } 
                @Override
                public void keyPressed(KeyEvent e) { 
                } 
                @Override
                public void keyReleased(KeyEvent e) {
                }
            }); 
}

void superSearchPatient ( String find) {

LinkedList<Client> res=  new LinkedList(); 
find=find.replaceAll(" ",""); 
for (int i = 0; i < cashier.allClients.size(); i++) 
{
Client p = cashier.allClients.get(i);

if (p.NOM_CLIENT.toUpperCase().contains(find)||p.NUM_AFFILIATION.toUpperCase().contains(find)) { 
res.add(p);
}}
int c = res.size();
Client[] control = new Client[c];
for (int i = 0; i < c; i++) {
Client p = res.get(i);
control[i] = p;
}
lesClients.setListData(control);

}
private void lesclientsEcoute() {
        lesClients.addMouseListener(new MouseListener() {

            public void actionPerformed(ActionEvent ae) {
            }

            @Override
            public void mouseExited(MouseEvent ae) {
            }

            @Override
            public void mouseClicked(MouseEvent e) {
     Client c = (Client) lesClients.getSelectedValue();
        if (c != null) { 
            setData(AutoOut.getReservationDunClient(akiraDateSql(dateIn.getDate(), "00:00:00"),
            akiraDateSql(dateOut.getDate(), "23:59:00"),c.NUM_AFFILIATION, db.s, db.conn));
              
        }
            }

            ;  
            @Override
    public void mousePressed(MouseEvent e) {
            } 
            @Override
            public void mouseReleased(MouseEvent e) {
            } 
            @Override
            public void mouseEntered(MouseEvent e) {
            }
        });
    }


public void readRoom(UneReservation r) {
    if (docheckout(r)) {
        JOptionPane.showMessageDialog(null, " CHECKOUT DONE " + r.chambre.room_number, " IKIBAZO ", JOptionPane.PLAIN_MESSAGE);
    } else {
        JOptionPane.showMessageDialog(null, " CHECKOUT NTIYARANGIYE", " IKIBAZO ", JOptionPane.PLAIN_MESSAGE);
    }
}

private void rapport() {
    Rapport.addActionListener(
            new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent ae) {
                    if (ae.getSource() == Rapport) {
                        
String   choix = (String) JOptionPane.showInputDialog(null, "VIEW REPPORT ", " ",
JOptionPane.QUESTION_MESSAGE, null, new String[]{"ROOMS", "INVOICE" }, "ROOMS");

              if(choix!=null && choix.equals("INVOICE"))
                        {
                            
                        JTable jTable = new JTable();  
                        int RES = getIn("SELECTED RESERVATION", "", 0);
                        LinkedList<Lot> getRapport = db.getRaportSerena2(akiraDateSql(dateIn.getDate(), "00:00:00"),
                                akiraDateSql(dateOut.getDate(), "23:59:00"), RES);
//getRapport.addAll(db.getRaport(akiraDateSql(dateIn.getDate(), "00:00:00"), 
//akiraDateSql(dateOut.getDate(), "23:59:00"),room_number));

                        String s1[][] = new String[getRapport.size()][9];
                        for (int j = 0; j < getRapport.size(); j++) {
                            Lot li = getRapport.get(j);
                            s1[j][0] = li.u1;
                            s1[j][1] = li.u2;
                            s1[j][2] = li.u3;
                            s1[j][3] = li.u4;
                            s1[j][4] = li.u5;
                            s1[j][5] = li.u6;
                            s1[j][6] = li.u7;
                        }
                        jTable.setModel(new javax.swing.table.DefaultTableModel(s1, new String[]{"ID", "ITEM", "DATE", " TOTAL", "TVA", "CASH", "CREDIT"}));
                        jTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
                        //jTable.getColumnModel().getColumn(0).setPreferredWidth(300);
                        jTable.getColumnModel().getColumn(3).setPreferredWidth(300);
                        JOptionPane.showMessageDialog(null, jTable);
                        int n = JOptionPane.showConfirmDialog(null, " URASHAKA KWEMPURIMA",
                                " PRINT ", JOptionPane.YES_NO_OPTION);

                        if (n == 0) {
LinkedList<UneReservation> res = AutoOut.getReservations2(RES, akiraDateSql(dateIn.getDate(), "00:00:00"),
        akiraDateSql(dateOut.getDate(), "23:59:00"), db.s, db.conn);

                            UneReservation resa = (UneReservation) JOptionPane.showInputDialog(null, "RESERVATION ", " ",
                                    JOptionPane.QUESTION_MESSAGE, null, res.toArray(), "FREE");
                            if (resa != null) {
                                db.printRetardReleve2(resa );
                            }
 
                        } 
                    }
                 
                    else { 
                  String   cat = (String) JOptionPane.showInputDialog(null, "VIEW REPPORT ", " ",
JOptionPane.QUESTION_MESSAGE, null, new String[]{"SINGLE", "DOUBLE", "VIP" }, "SINGLE"); 
         new RoomsView(  db.chambreBYCAT(  cat,currentprice)   );
                    }
                }
                }
            });


}

public boolean docheckout(UneReservation r) {
    try { 

        if (Main.cashier.db.conn.isValid(1000)) {

            double montantZikaze = 0;
            double montantZibyumba = db.getMontantRoomSejour(r, nom, main);
            double montant = montantZibyumba + montantZikaze;
            if (montant == -1) {
                return false;
            }
            return true;
        } else {
            JOptionPane.showMessageDialog(null, "CHECK BAR CONNEXION");
        }
    } catch (SQLException ex) {
        JOptionPane.showMessageDialog(null, "NET WORK PROBLEM");
        return false;
    }
    return false;
}

 
 
public static int getIn(String s, String s1, int i) {
    int entier = -1;

    String in = JOptionPane.showInputDialog(null, s + s1);
    boolean done = true;

    while (in != null && i < 3 && done == true) {
        try {
            if (in.equals("")) {
                i++;
                in = JOptionPane.showInputDialog(null, s + " Enter a number ", "1");
            } else {
                Integer in_int = new Integer(in);
                entier = in_int.intValue();
                done = false;
                if (entier < 0) {
                    entier = -1;
                }
            }

        } catch (NumberFormatException e) {
            i++;
            in = JOptionPane.showInputDialog(null, s + " = " + in + "= Not a number; Enter a number ");
            done = true;
        }
    }
    if (i == 3) {
        JOptionPane.showMessageDialog(null, " Too many try ", " Sorry we have to Exit ", JOptionPane.PLAIN_MESSAGE);

    }

    return entier;
}

LinkedList<String> getFichier(File productFile) throws FileNotFoundException, IOException {
    LinkedList<String> give = new LinkedList();

    // = new File(fichier);
    String ligne = null;
    BufferedReader entree = new BufferedReader(new FileReader(productFile));
    try {
        ligne = entree.readLine();
    } catch (IOException e) {
        System.out.println(e);
    }
    int i = 0;
    while (ligne != null) {
        // System.out.println(i+ligne);
        give.add(ligne);
        i++;
        try {
            ligne = entree.readLine();
        } catch (IOException e) {
            System.out.println(e);
        }
    }
    entree.close();
    return give;
}

 @Override
    protected void processWindowEvent(WindowEvent evt) {
        if (WindowEvent.WINDOW_CLOSING == evt.getID()) {
            //default icon, custom title
            int n = JOptionPane.showConfirmDialog(null,"DO YOU WANT TO EXIT ? ",
                    "ATTENTION", JOptionPane.YES_NO_OPTION);
            if (n == 0) { 
                try { 
                    cashier.db.insertLogin(nom, "OUT", 0); 
                    cashier.db.closeConnection();

                } catch (Throwable e) {
                    System.out.println("ER:" + e);
                    cashier.db.insertError(e + "", "processWindowEvent");
                }
                // JOptionPane.showMessageDialog(frame,Cashier.yose+" RWF","UFITE MURI CAISSE ",JOptionPane.PLAIN_MESSAGE);

                System.exit(0);
            }
        }
    }

public static void main(String[] args) {

    try {
        //StartServer    d  =new StartServer ();

        int id = getIn("NUMERO D'EMPLOYE ", " : CHIFFRE", 0);
//        int id=1;

        if (id != -1) {
            BufferedReader entree = null;
            try {
                JPanel pw = new JPanel();
                JPasswordField passwordField = new JPasswordField(10);
                JLabel label = new JLabel("MOT DE PASSE: ");
                pw.add(label);
                pw.add(passwordField);
                passwordField.setText("");
                JOptionPane.showConfirmDialog(null, pw);
                Integer in_int = new Integer(passwordField.getText());
                int carte = in_int.intValue();
//                                int carte = 2;

                LinkedList<String> give = new LinkedList();

                File productFile = new File("log.txt");
                String ligne = null;
                entree = new BufferedReader(new FileReader(productFile));
                try {
                    ligne = entree.readLine();
                } catch (IOException e) {
                    System.out.println(e);
                }
                int i = 0;
                while (ligne != null) {
                    System.out.println(i + ligne);
                    give.add(ligne);
                    i++;

                    try {
                        ligne = entree.readLine();
                    } catch (IOException e) {
                        JOptionPane.showMessageDialog(null, "Ikibazo", " reba log ", JOptionPane.PLAIN_MESSAGE);
                    }
                }
                entree.close();
                int frw = 0;// getIn("Amafaranga mutangiranye "," : INT",0);
                // String dataBase  = (JOptionPane.showInputDialog(frame, " dataBase ","score"));
                //String serv="localhost";
                bac = give.get(0);
                String dataBase = give.get(2);
                String server = give.get(1);
                dbIkaze = give.get(5);
                servIkaze = give.get(4);
//                          JOptionPane.showMessageDialog(null," 1 cashier" ," in ",JOptionPane.PLAIN_MESSAGE);
                Cashier c = new Cashier(id, server, dataBase);

//                          JOptionPane.showMessageDialog(null," 2 cashier"+c.db," in ",JOptionPane.PLAIN_MESSAGE);
                if (c != null && c.db.check_db(id, carte)) {
                    Employe umukozi = c.db.getName(id);
                    Cashier.yose = frw;
                    c.db.insertLogin(umukozi.NOM_EMPLOYE, "in", frw);
                    //.showMessageDialog(null,"3  Main"," in ",JOptionPane.PLAIN_MESSAGE);
                    new Hotel(server, dataBase, umukozi, "121212", 
                            give.get(0), umukozi.TITRE_EMPLOYE,c);
                    

                } else {
                    JOptionPane.showMessageDialog(null, "  UMUBARE WIBANGA SIWO ", " IKIBAZO ", JOptionPane.PLAIN_MESSAGE);
                }
            } catch (IOException ex) {
                Logger.getLogger(Hotel.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                try {
                    entree.close();
                } catch (IOException ex) {
                    Logger.getLogger(Hotel.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    } catch (Throwable ex) {
        Logger.getLogger(Hotel.class.getName()).log(Level.SEVERE, null, ex);
    }
}
}
