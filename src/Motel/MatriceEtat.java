/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Motel;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import javax.swing.AbstractButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;

/**
 *
 * @author HP
 */
public class MatriceEtat {
    
   public static void main(String[] args) {
      
       MatriceEtat f=new MatriceEtat( );
            
     
   }   
   
   String [] options;
   boolean[] enable;
   Color [] color; 
   
    public MatriceEtat() {
    
  this.options = new String []  {"RESERVED","OCCUPIED","CHECK OUT", "ANNULE", "NON DISPO", "CONSOMMATION","SET UP"};   
  this.color= new Color []  { Color.yellow ,Color.green ,new Color(240, 100, 00) ,Color.orange 
          ,Color.pink ,new Color(0, 200, 240) ,Color.white };
  this.enable= new boolean [7]; 
  
 } 
   
   
public  String doRun(int index,String Chambre) { 
    
     for(int i=0;i<7;i++)
  {
      
      if(i<index )
      {
          enable[i]=false;
      }
      else
          {
          enable[i]=true;
      }
          
  }
   
    
      JOptionPane myOptionPane = new JOptionPane(options[index]+" >>>  CHOISIE LE NOUVEAU STATUS",
            JOptionPane.QUESTION_MESSAGE, JOptionPane.YES_NO_OPTION, 
            null, options, options[1]);
      
      JDialog myDialog = myOptionPane.createDialog(null, "RESERVATION CHAMBRE "+Chambre);
      myDialog.setModal(true); 
     
      for(int i=0;i<7;i++)
  {
      inactivateOption(myDialog, options[i],enable[i],color[i]);
  }
       
      myDialog.setVisible(true);
      Object result = myOptionPane.getValue();
      // Note: result might be null if the option is cancelled
      System.out.println("result: " + result); 
       myDialog.dispose(); // to stop Swing event thread
      
      return ""+result;
   }    
 private  void inactivateOption(Container container, String text,boolean enable,Color bckgd) {
      Component[] comps = container.getComponents();
      for (Component comp : comps) {
         if (comp instanceof AbstractButton) {
            AbstractButton btn = (AbstractButton) comp;
            if (btn.getActionCommand().equals(text)) {
                btn.setEnabled(enable);
                btn.setBackground(bckgd); 
               return;
            }
         } else if (comp instanceof Container) {
            inactivateOption((Container) comp, text,enable,bckgd);
         }
      }

   }

}
