
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package Motel; 
import java.awt.*;
import java.awt.event.*;
import javax.swing.*; 
import javax.swing.JFrame;
/**
 *
 * @author aimable
 */
public class RoomInterface extends JFrame{
    
private JTextField  room_number,nbLits, price1,price2,price3,  tv,  phone, douche,  baignoir,  fredge,  
        cat,  view, ID,VIEW,ID_PRODUCT_ROOM;
private JLabel room_numberL,nbLitsL, priceL1,priceL2,priceL3,  tvL,  phoneL, doucheL,  baignoirL,  fredgeL, 
        catL,  viewL, IDL,VIEWL,ID_PRODUCT_ROOML;
private JButton send;
private AutoOut db;
Rooms rm;     
RoomInterface(AutoOut db,Rooms rm)
{
    this.rm = rm;
    this.db=db;
    makeFrame();
    if(rm!=null)
    setRoom();
    send();
    draw(); 
}
public void makeFrame()
{
    this.room_number = new JTextField();this.room_numberL=new JLabel("ROOM NUMBER");
    this.nbLits = new JTextField("STANDARD");this.nbLitsL=new JLabel("BED ");
    this.price1= new JTextField();this.priceL1=new JLabel("PRICE SINGLE");
    this.price2= new JTextField();this.priceL2=new JLabel("PRICE DOUBLE");
    this.price3= new JTextField();this.priceL3=new JLabel("PRICE GROUPE");
    this.tv= new JTextField("1");this.tvL=new JLabel("TV");
    this.phone= new JTextField("1");this.phoneL=new JLabel("PHONE");
    this.douche= new JTextField("1"); this.doucheL=new JLabel("DOUCHE");
    this.baignoir= new JTextField("1"); this.baignoirL=new JLabel("BAIGNOIR");
    this.fredge= new JTextField("0"); this.fredgeL=new JLabel("FRIGO");
    this.cat= new JTextField(); this.catL=new JLabel("CAT");
    this.view= new JTextField(); this.viewL=new JLabel("VUE"); 
    this.ID = new JTextField();this.IDL=new JLabel("ROOM ID");
    this.ID_PRODUCT_ROOM = new JTextField();this.ID_PRODUCT_ROOML=new JLabel("ID PRODUCT ROOM");    
    this.VIEW = new JTextField();this.VIEWL=new JLabel("VIEW");
    this.send=new JButton("ADD");
}
public void setRoom()
{
   room_number.setText(""+rm.getRoomNumber())  ;
    this.nbLits.setText(""+rm.lits)  ;
    this.price1.setText(""+rm.PRIX_SINGLE)  ;
     this.price2.setText(""+rm.PRIX_DOUBLE)  ;
      this.price3.setText(""+rm.PRIX_GROUPE)  ;
    this.tv.setText(""+rm.tv)  ;
    this.phone.setText(""+rm.phone)  ;
    this.douche.setText(""+rm.douche)  ;
    this.baignoir.setText(""+rm.baignoir)  ;
    this.fredge.setText(""+rm.frigo)  ;
    this.cat.setText(rm.categorie)  ;
    this.view.setText(rm.view)  ; 
    this.ID.setText(""+rm.ID)  ;
    this.VIEW.setText(""+rm.VIEW)  ; 
    this.ID_PRODUCT_ROOM.setText(""+rm.ID_PRODUCT_ROOM)  ;
    send.setText("UPDATE");
}
public void draw() 
{
    setTitle("SET ROOM INFO");
    
    this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE); 
    //on fixe  les composant del'interface
    Container content = getContentPane();
    westPane(content);
    

     setSize(500,600);
     setVisible(true);

}
public static double getIn (String in)
{
                      double productCode=-1;
    try
    { 
    if(in.equals(""))
    {
    productCode=-3;
    }
    else 
    {
    Double in_int=new Double(in);
    productCode = in_int.doubleValue();
    }
    }
    catch (NumberFormatException e)
    {
    productCode=-2;
    }
    return productCode;
    }



/**************************************************************************
affiche du stock
**************************************************************************/

public void westPane(Container content )
        {
    JPanel Pane  = new JPanel();

    Pane.setPreferredSize(new Dimension (600,700));

    JPanel panel = new JPanel();
    panel.setLayout(new GridLayout(16,2));
    panel.setPreferredSize(new Dimension (300,500));         

    panel.add(room_numberL);panel.add(room_number);
    panel.add(nbLitsL);panel.add(nbLits);
    panel.add(priceL1);panel.add(price1);
    panel.add(priceL2);panel.add(price2);
    panel.add(priceL3);panel.add(price3);
    panel.add(tvL);panel.add(tv);
    panel.add(phoneL);panel.add(phone); 
    panel.add(doucheL);panel.add(douche);
    panel.add(baignoirL);panel.add(baignoir);
    panel.add(fredgeL);panel.add(fredge);
    panel.add(catL);panel.add(cat);
    panel.add(viewL);panel.add(view);
    panel.add(IDL);panel.add(ID);
    panel.add(ID_PRODUCT_ROOML);panel.add(ID_PRODUCT_ROOM);
     panel.add(VIEWL);panel.add(VIEW);   
        Pane.add(panel,BorderLayout.CENTER);
   Pane.add(send,BorderLayout.SOUTH);

    content.add(Pane, BorderLayout.WEST);

} 
/**********************************************************************************************
AJOUT Mannuel
**********************************************************************************************/
public void send()
{
send.addActionListener(new ActionListener() {
public void actionPerformed(ActionEvent ae)
{
if(ae.getSource()==send )
{
if(rm==null)
{ rm= new Rooms(
(int) getIn ( room_number.getText()),
nbLits.getText(), "FREE",getIn (price1.getText()), 
(int) getIn ( tv.getText()),  (int) getIn ( phone.getText()),
(int) getIn ( douche.getText()),  (int) getIn (  baignoir.getText()),
(int) getIn ( fredge.getText()),
cat.getText(),  view.getText(), (int) getIn ( ID.getText()), (int) getIn ( VIEW.getText())
        ,(int) getIn ( ID_PRODUCT_ROOM.getText()),getIn (price2.getText()),getIn (price3.getText())
        
        );
db.addRoom(rm); }
else
{
rm= new Rooms(
rm.room_number,
nbLits.getText(), "FREE",getIn (price1.getText()), 
(int) getIn ( tv.getText()),  (int) getIn ( phone.getText()),
(int) getIn ( douche.getText()),  (int) getIn (  baignoir.getText()),
(int) getIn ( fredge.getText()),
cat.getText(),  view.getText(), (int) getIn ( ID.getText()), (int) getIn ( VIEW.getText())
        ,(int) getIn ( ID_PRODUCT_ROOM.getText()),getIn (price2.getText()),getIn (price3.getText())
        );
if(db.updateRoom(rm)) {
        send.setBackground(Color.green);
    }
else {
        send.setBackground(Color.red);
    }  
} 
}
}});
}
 @Override
protected void processWindowEvent(WindowEvent evt)
{
   if(WindowEvent.WINDOW_CLOSING == evt.getID())
   {
  this.dispose();	 
 }

}
public static void main(String[] arghs)
{
  
}
}
