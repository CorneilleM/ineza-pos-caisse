/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Motel;

/**
 *
 * @author Yves
 * 
 * 
 */
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.JFrame;

public class Clinique extends JFrame {
    
    
    private JPanel westPane;
    private JPanel centerPane;
    private JPanel eastPane;
    private javax.swing.JLabel PATIENT;
    private javax.swing.JLabel LESPATIENTS;
    private javax.swing.JLabel ACTESMEDICAUX;
    private javax.swing.JLabel REMEDES;
    private javax.swing.JLabel FACTURATION;
    private javax.swing.JLabel IMITI;
    private javax.swing.JButton creerPatient;
    private javax.swing.JButton modifierPatient;
    private javax.swing.JButton listePatients;
    private javax.swing.JButton ajoutActeMedical;
    private javax.swing.JButton suprimerActeMedical;
    private javax.swing.JButton creerRemede;
    private javax.swing.JButton modifierRemede;
    private javax.swing.JButton afficherRemede;
    private javax.swing.JButton creerFacture;
    private javax.swing.JButton modifierFacture;
    private javax.swing.JButton chercher_facture;
    private javax.swing.JButton prescrireMedicament,suprimerMedicament;
    private JLabel L_NOM_PATIENT,L_PRENOM_PATIENT,L_DATE_NAISSANCE,L_ADRESSE,L_CI_NUMERO;
    private JTextField NOM_PATIENT,PRENOM_PATIENT,DATE_NAISSANCE,ADRESSE,CI_NUMERO;
    private JList allProductList = new JList();
    private JList allPatients = new JList();
    private JList allActes = new JList();
    private JList allInvoice = new JList();
    private JList lesExamens = new JList();
    private JList lesMedicamentDUnPatient = new JList();
    private  JTextField search = new JTextField();
    private JTextField serarchOnActes = new JTextField();
    private JTextField searchonPatients;
    private JTextArea listeFacture;
    private  JButton  searchButton,searchPatient;
    private  JScrollPane  scrollPaneFactureList,scrollPanePatientActesList,scrollPanePatientMedicaments;
    private JScrollPane scrollPaneActesList,scrollPanePatientInvoice,scrollPanePatientsList,scrollPaneMedicaments;
    
    
    
    private static JFrame frame;
    private Cashier cashier;
    private String stock;
     Employe umukozi=null;
     String date="";
      AutoOut db;
    
    public Clinique(String server,String dbName,Employe umukozi,String date){
        
        setTitle("    CLINIQUE ");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame = this;
       // cashier= c;
        cashier = new Cashier(1,"localHost","cliniquebeta");
        this.umukozi = umukozi;
        this.date = date;
        db=new AutoOut(server,dbName);
        db.read();
        draw();
        showProduct();
        showPatient();
        addProduct();
        
    }
   
  public void draw(){
      
      Container content = getContentPane();
        westPane(content);
        centerPane(content);
        eastPane(content);
        enableEvents(WindowEvent.WINDOW_CLOSING);
    //on fixe  les composant del'interface
         setSize(900,700);
         setVisible(true);
      
      
      
  }  
 /** fenetre des patients : creation et gestions */   
public void westPane(Container content){
JPanel commonWestPane = new JPanel();
commonWestPane.setPreferredSize(new Dimension (400,600));
LESPATIENTS = new javax.swing.JLabel();
ACTESMEDICAUX = new javax.swing.JLabel();
LESPATIENTS.setFont(new java.awt.Font("Papyrus", 1, 12)); // NOI18N
LESPATIENTS.setText("       LISTE DES PATIENTS"); 
         
      
       //setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
      JPanel patientPane = new JPanel();
      JPanel actes = new JPanel();
      patientPane.setPreferredSize(new Dimension (400,300));
      patientPane.setLayout(new GridLayout(3,1));
      patientPane.add(LESPATIENTS);
      
      scrollPanePatientsList = new JScrollPane(allPatients);
      searchonPatients = new JTextField();
      searchPatient = new JButton( " SEARCH : ");
      
      
      creerPatient = new JButton("CREATE");
      creerPatient.setBackground(Color.red);
      
       modifierPatient = new JButton("UPADATE");
       creerPatient.setBackground(Color.orange);
       JPanel sonPatient = new JPanel();
       sonPatient.setPreferredSize(new Dimension (400,20));
       searchonPatients.setPreferredSize(new Dimension(140,30));
      searchPatient.setBackground(Color.orange);
      sonPatient.add(searchonPatients, BorderLayout.WEST);
      sonPatient.add(searchPatient , BorderLayout.EAST);
       
       patientPane.add(scrollPanePatientsList);
       patientPane.add(sonPatient);
       
       /**
       listePatients = new JButton(" LIST ");
       listePatients.setBackground(Color.green);
       JPanel patientButtons = new JPanel();
       patientButtons.setPreferredSize(new Dimension (200,100));
       patientButtons.setLayout(new GridLayout(3,1));
       patientButtons.add(creerPatient);
       patientButtons.add(modifierPatient);
       patientButtons.add(listePatients);
       patientPane.add(patientButtons);
       */
       
       ACTESMEDICAUX.setFont(new java.awt.Font("Papyrus", 1, 12)); // NOI18N
       ACTESMEDICAUX.setText("    ACTES MEDICAUX "); 
      actes.setPreferredSize(new Dimension (400,300));
      actes.setLayout(new GridLayout(3,1));
      
      scrollPaneActesList = new JScrollPane(allActes);
      JPanel paneC = new JPanel();
      paneC.setPreferredSize(new Dimension (400,20));
      searchButton= new JButton("SEARCH ");
      serarchOnActes.setPreferredSize(new Dimension(140,30));
      searchButton.setBackground(Color.orange);
      paneC.add(serarchOnActes, BorderLayout.WEST);
      paneC.add(searchButton , BorderLayout.EAST);
     
      actes.add(ACTESMEDICAUX);
      actes.add(scrollPaneActesList);
      actes.add(paneC);
      
      
       commonWestPane.add(patientPane,BorderLayout.NORTH);
        commonWestPane.add(actes,BorderLayout.SOUTH);
        content.add(commonWestPane,BorderLayout.WEST);
     
 } 
 
 public void centerPane(Container content){
     
     JPanel commonCentePane = new JPanel();
     JPanel fichePane = new JPanel();
     JPanel actesPane = new JPanel();
     JPanel medicPane = new JPanel();
     //JPanel facturePane = new JPanel();
     commonCentePane.setPreferredSize(new Dimension (400,600));
     commonCentePane.setLayout(new GridLayout(4,1));
     commonCentePane.setBorder(BorderFactory.createLineBorder(Color.black));
     PATIENT = new javax.swing.JLabel();
         
      PATIENT.setText("           FICHE DU PATIENT"); 
       PATIENT.setFont(new java.awt.Font("Papyrus", 1, 12));
     
     
      
       fichePane.setPreferredSize(new Dimension (300,200));
      // actesPane.setPreferredSize(new Dimension (400,200));
      // medicPane.setPreferredSize(new Dimension (400,200));
      // facturePane.setPreferredSize(new Dimension (400,200));
      
      fichePane.setLayout(new GridLayout(2,1));
      JPanel fiche = new JPanel();
      fiche.setLayout(new GridLayout(5,2));
      L_NOM_PATIENT = new JLabel("     NOM ");
      L_PRENOM_PATIENT = new JLabel("     PRENOM ");
      L_DATE_NAISSANCE = new JLabel("     DATE DE NAISSANCE ");
      L_ADRESSE = new JLabel("     ADRESSE ");
      L_CI_NUMERO = new JLabel("     NUMERO DE CI");
      
      NOM_PATIENT= new JTextField();
      PRENOM_PATIENT =new JTextField();
      DATE_NAISSANCE = new JTextField();
      ADRESSE =  new JTextField();
      CI_NUMERO= new JTextField();
      fiche.add(L_NOM_PATIENT);fiche.add(NOM_PATIENT);
      fiche.add(L_PRENOM_PATIENT);fiche.add(PRENOM_PATIENT);
      fiche.add(L_DATE_NAISSANCE);fiche.add(DATE_NAISSANCE);
      fiche.add(L_ADRESSE);fiche.add(ADRESSE);
      fiche.add(L_CI_NUMERO);fiche.add(CI_NUMERO);
      //fichePane.setBorder(BorderFactory.createLineBorder(Color.black));
      fichePane.add(PATIENT);
      fichePane.add(fiche);
        
     
     scrollPanePatientActesList = new JScrollPane(lesExamens);
     scrollPanePatientActesList.setBorder(BorderFactory.createTitledBorder("LES EXAMENS"));
     actesPane.add(scrollPanePatientActesList);
     //actesPane.setBorder(new Border("ACTES") {});
     
     scrollPanePatientMedicaments = new JScrollPane(lesMedicamentDUnPatient);
     scrollPanePatientMedicaments.setBorder(BorderFactory.createTitledBorder("LES MEDICAMENTS"));
     medicPane.add(scrollPanePatientMedicaments);
     
     scrollPanePatientInvoice = new JScrollPane();
    scrollPanePatientInvoice.setBorder(BorderFactory.createTitledBorder("FACTURATION"));
       
     JPanel factureGlobal = new JPanel();
     factureGlobal.setPreferredSize(new Dimension (400,300));
     factureGlobal.setBorder(BorderFactory.createTitledBorder("FACTURATION"));
     factureGlobal.setLayout(new GridLayout(2,1));
    
     
      JPanel paneC = new JPanel();
     paneC.setPreferredSize(new Dimension (400,20));

        searchButton= new JButton("SEARCH");
        search.setPreferredSize(new Dimension(140,30));
            searchButton.setBackground(Color.orange);

            paneC.add(search, BorderLayout.WEST);
            paneC.add(searchButton , BorderLayout.EAST);


     
    
     modifierFacture = new JButton("MODIFIER UNE FACTURE");
     scrollPaneFactureList = new JScrollPane(allInvoice);
     scrollPaneFactureList.setBackground(Color.pink);
   
     
   
    // factureGlobal.add(FACTURATION);
     factureGlobal.add(scrollPaneFactureList);
     factureGlobal.add(paneC);
     commonCentePane.add(fichePane);
     commonCentePane.add(scrollPanePatientActesList);
     commonCentePane.add(scrollPanePatientMedicaments);
     commonCentePane.add(factureGlobal);
     //commonCentePane.add(medicPane);
    // commonCentePane.add(facturePane);
     
     content.add(commonCentePane,BorderLayout.CENTER);
     
     
     
 }
 public void eastPane(Container content){
     
     JPanel eastCommonPane = new JPanel();
    
     eastCommonPane.setPreferredSize(new Dimension (400,600));
     
    eastCommonPane.setLayout(new GridLayout(2,1));
     JPanel EastcenterPane = new JPanel();
      
     EastcenterPane.setLayout(new GridLayout(2,1));
     REMEDES = new JLabel("GERE555R LES MEDICAMENTS");
     REMEDES.setFont(new java.awt.Font("Papyrus", 1, 12));
     FACTURATION = new JLabel("FACTURATION ");
     FACTURATION.setFont(new java.awt.Font("Papyrus", 1, 12));
    
     
     prescrireMedicament = new JButton("PRESC555RIRE UN MEDICAMENT");
     suprimerMedicament = new JButton("SUPPRIMER UN MEDICAMENT");
     prescrireMedicament.setBackground(Color.yellow);
     
    suprimerMedicament.setBackground(Color.green);
     afficherRemede = new JButton("AFFICHER LES MEDICAMENTS");
     
     scrollPaneMedicaments = new JScrollPane(allProductList);
     scrollPaneMedicaments.setBorder(BorderFactory.createTitledBorder(" PHARMACIE "));
     
     
     JPanel eastSouth = new JPanel(); 
     eastSouth.setLayout(new GridLayout(3,1));
     JPanel remedeButton = new JPanel();
     remedeButton.setPreferredSize(new Dimension (200,100));
    // remedeButton.setBorder(BorderFactory.createLineBorder(Color.black));
     remedeButton.setLayout(new GridLayout (1,2));
     remedeButton.add(prescrireMedicament);
     remedeButton.add(suprimerMedicament);
    eastSouth.add(remedeButton);
  
     
     
    
     eastCommonPane.add(scrollPaneMedicaments);
    eastCommonPane.add(eastSouth);
     content.add(eastCommonPane,BorderLayout.EAST);
     
     
 }

 
 public void showProduct() {
        int c = cashier.allProduct.size();
        Product[] control = new Product[c];
        for (int i = 0; i < c; i++) {
            Product p = cashier.allProduct.get(i);
            control[i] = p;
        }
        allProductList.setListData(control);
    }
 public void showPatient(){
     
     
 }
 public void addProduct() {
        prescrireMedicament.addActionListener(
                new ActionListener()
     {
                    public void actionPerformed(ActionEvent ae) {
                        if (ae.getSource() == prescrireMedicament && allProductList.getSelectedValue() != null) {

                            Product p = (Product) allProductList.getSelectedValue(); 
                            if (p != null) {
                                int qte = getIn(p.productName + "  ZINGAHE KUMUNSI ??", "  ", 0);
                                int qte2 = getIn(" IMINSI INGAHE ??", "  ", 0);
                                addLesMedoc(p,qte,qte2);
                               // addProductLot(p, qte);
                                System.out.println("test1 :"+qte);
                                System.out.println(p);
                            } else {
                                JOptionPane.showMessageDialog(frame, p.productCode + "  NABWO IRI MURI STOCK COURRANT.", "IKIBAZO", JOptionPane.WARNING_MESSAGE);
                            }
                        }
                    }
                }); 
    }
  public static int getIn(String s, String s1, int i) {
        int entier = -1;

        String in = JOptionPane.showInputDialog(null, s + s1);
        boolean done = true;

        while (in != null && i < 3 && done == true) {
            try {
                if (in.equals("")) {
                    i++;
                    in = JOptionPane.showInputDialog(null, s + " Enter a number ", "1");
                } else {
                    Integer in_int = new Integer(in);
                    entier = in_int.intValue();
                    done = false;
                    if (entier < 0) {
                        entier = -1;
                    }
                }

            } catch (NumberFormatException e) {
                i++;
                in = JOptionPane.showInputDialog(null, s + " = " + in + "= Not a number; Enter a number ");
                done = true;
            }
        }
        if (i == 3) {
            JOptionPane.showMessageDialog(frame, " Too many try ", " Sorry we have to Exit ", JOptionPane.PLAIN_MESSAGE);
        }

        return entier;
    }
  
 public void addLesMedoc(Product p,int nbfois,int nbjour){
     String nom = p.productName;
    
     // lesMedicamentDUnPatient;
     System.out.println(nom + "   "+nbfois + "Par Jour pendant "+ nbjour+" Jours");
     
     
 } public static void main (String[]args)   {
     
     
      Employe em= new Employe(1,1,1,1,"","","","","");
     
     new Clinique("localhost","felsen",em,"121212");
 }
    
}
