/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package Motel;   

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.text.ParseException;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.text.MaskFormatter;

/**
 *
 * @author aimable
 */
public class Client_Interface extends JFrame{

JTextField REDUCTION, NUM_AFFILIATION,NOM_CLIENT ,PRENOM_CLIENT,EMPLOYEUR,SECTEUR ,CODE, DATE_EXP 
,PROFESSION,MOTIF_VOYAGE,NATIONALITE,DATE_LIVRAISON_ID,LIEU_NAISSANCE,DATE_NAISSANCE, TEL
        ,PREFERENCE,ETA,EMAIL,CREDIT_CARD; 
JButton ASSURANCEL;
JLabel REDUCTIONL,NUM_AFFILIATIONL,NOM_CLIENTL,PRENOM_CLIENTL,EMPLOYEURL,SECTEURL,CODEL,STATUSL,DATE_EXPL,SEXEL 
,LANGUEL,PROFESSIONL,MOTIF_VOYAGEL,NATIONALITEL,DATE_LIVRAISON_IDL,LIEU_NAISSANCEL,DATE_NAISSANCEL,TELL 
        ,PREFERENCEL,TITREL,ETAL,EMAILL,CREDIT_CARDL; 

JList sexeList=new JList(new String[]{"MALE","FEMALE"});
JList status=new JList(new String[]{"OK","SOMMEIL"});
JList titre=new JList(new String[]{"MR.","MDE","MLLE","SIR","HON.","EXC.","SEIN."});
JList  societeJList=new JList();
JList  LANGUES=new JList(new String  [] {"KINYARWANDA","ENGLISH ","FRANCAIS ","SWAHILI"});
  
private JButton send;
private AutoOut db;
Client c; 
String soc,what;

Font police = new Font("Arial", Font.BOLD,10);

Client_Interface( AutoOut db )
{
this.db=db;
what="ADD"; 
makeFrame(""); 
send();
draw(); 
}
Client_Interface(AutoOut db,Client c)
{
what="UPDATE";
this.db=db;
this.c=c;
int index=makeFrame(c.ASSURANCE);
addmakeFrameClient(c,  index);
clientGroupe();
send();
draw(); 
}
private void addmakeFrameClient(Client c,int index )
{
REDUCTION.setText(""+c.REDUCTION);NUM_AFFILIATION.setText(c.NUM_AFFILIATION);NOM_CLIENT.setText(c.NOM_CLIENT);
PRENOM_CLIENT.setText(c.PRENOM_CLIENT);
titre.setSelectedValue(c.TITRE, true);
sexeList.setSelectedValue(c.SEXE, true);
societeJList.setSelectedIndex(index);
EMPLOYEUR.setText(c.EMPLOYEUR);
SECTEUR .setText(c.SECTEUR);CODE.setText(c.CODE); DATE_EXP.setText(""+c.DATE_EXP);
sexeList.setSelectedValue(c.SEXE, true);status.setSelectedValue(c.STATUS, true);
LANGUES.setSelectedValue(c.LANGUE, true);PROFESSION.setText(c.PROFESSION);
MOTIF_VOYAGE.setText(c.MOTIF_VOYAGE);NATIONALITE.setText(c.NATIONALITE);DATE_LIVRAISON_ID.setText(c.DATE_LIVRAISON_ID);
LIEU_NAISSANCE.setText(c.LIEU_NAISSANCE);DATE_NAISSANCE.setText(c.DATE_NAISSANCE);TEL.setText(c.TEL)  ; 
 PREFERENCE.setText(c.PREFERENCE);  EMAIL.setText(c.EMAIL);ETA.setText(c.ETA);
  CREDIT_CARD.setText(c.CREDIT_CARD);
 
}
private int makeFrame(String groupe) 
{
    String codeun = "";
    String name = "";
    if (what.equals("ADD")) {
        name = JOptionPane.showInputDialog(null, "CLIENT NAME ", "");
        name = name.replaceAll("'", "");
//        st=st.replaceAll("&"," ET ");

        codeun = methode((name.substring(0, 3)).toUpperCase());
    }

    REDUCTION = new JTextField();
    NUM_AFFILIATION = new JTextField(codeun);
    NOM_CLIENT = new JTextField(name);
    PRENOM_CLIENT = new JTextField();
    EMPLOYEUR = new JTextField();
    SECTEUR = new JTextField();
    CODE = new JTextField();
    DATE_EXP = new JTextField();
    PROFESSION = new JTextField();
    MOTIF_VOYAGE = new JTextField();
    NATIONALITE = new JTextField();
//    DATE_LIVRAISON_ID = new JTextField();
    LIEU_NAISSANCE = new JTextField();
//    DATE_NAISSANCE = new JTextField();
    TEL = new JTextField();
    sexeList.setBackground(Color.PINK);
    sexeList.setFont(police);
    status.setBackground(Color.ORANGE);
    status.setFont(police);
    LANGUES.setFont(police);
    LANGUES.setBackground(Color.GRAY);
     PREFERENCE = new JTextField(); 
//       ETA = new JTextField();
    EMAIL = new JTextField();
     LinkedList<Client_Groupe> group=db.getGroupe();
     int index=0;
     for(int i=0;i<group.size();i++)
     {Client_Groupe g=group.get(i);
     
     if(g.INTITULE_CLIENT_GROUPE.equals(groupe))
     {
         index=i;
     }
     }
    societeJList.setListData(group.toArray());
    societeJList.setFont(police);
    
        
    try {
        MaskFormatter date = new MaskFormatter("####-##-##");
        DATE_NAISSANCE = new JFormattedTextField(date);
        Font policeZ = new Font("Consolas", Font.BOLD, 14);
        DATE_NAISSANCE.setFont(policeZ);
        DATE_NAISSANCE.setForeground(Color.BLUE);
        DATE_NAISSANCE.setBackground(new Color(175, 215, 255));
        
        
         MaskFormatter exp =  new MaskFormatter("####-##-##");
        DATE_LIVRAISON_ID = new JFormattedTextField(exp); 
        DATE_LIVRAISON_ID.setFont(policeZ);
        DATE_LIVRAISON_ID.setForeground(Color.RED);
        DATE_LIVRAISON_ID.setBackground(new Color(175, 215, 255));
        
       MaskFormatter time =  new MaskFormatter("####-##-## ##:##:##");
        ETA = new JFormattedTextField(time); 
        ETA.setFont(policeZ);
        ETA.setForeground(Color.BLACK);
        ETA.setBackground(new Color(175, 215, 255));
        
         MaskFormatter cd =  new MaskFormatter("####-####-####-####");
        CREDIT_CARD = new JFormattedTextField(cd); 
        CREDIT_CARD.setFont(policeZ);
        CREDIT_CARD.setForeground(Color.red);
        CREDIT_CARD.setBackground(new Color(175, 215, 255));
        
        
        
        
    } catch (ParseException e) { 
        System.out.println(e);
    }
    
    if(what.equals("UPDATE"))
    {
        NUM_AFFILIATION.setEnabled(false);
    }
    REDUCTIONL = new JLabel("REDUCTION EN % ");
    NUM_AFFILIATIONL = new JLabel("REF#");
    NOM_CLIENTL = new JLabel("NOM");
    PRENOM_CLIENTL = new JLabel("PRENOM");
    ASSURANCEL = new JButton("GROUPE");
    EMPLOYEURL = new JLabel("EMPLOYEUR");
    SECTEURL = new JLabel("SECTEUR");
    CODEL = new JLabel("DOC ID");
    STATUSL = new JLabel("STATUS");
    DATE_EXPL = new JLabel("EXP");
    SEXEL = new JLabel("SEXE");
    LANGUEL = new JLabel("LANGUE");
    PROFESSIONL = new JLabel("PROFESSION");
    MOTIF_VOYAGEL = new JLabel("MOTIF");
    NATIONALITEL = new JLabel("NATIONALITE");
    DATE_LIVRAISON_IDL = new JLabel("DATE EXP ID");
    LIEU_NAISSANCEL = new JLabel("LIEUX NAISSANCE");
    DATE_NAISSANCEL = new JLabel("DATE NAISSANCE");
    TELL = new JLabel("TEL");
    PREFERENCEL = new JLabel("PREFERENCE");
    TITREL = new JLabel("TITRE");
    ETAL = new JLabel("ETA");
    EMAILL = new JLabel("EMAIL");
    CREDIT_CARDL= new JLabel("CREDIT CARD");
    send = new JButton(what);
    return index;
}

private void draw()
{
    setTitle("Register a new Client for Ishyiga");  
    Image im = Toolkit.getDefaultToolkit().getImage("logo.png");
    this.setIconImage(im); 
    this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE); 
    //on fixe  les composant del'interface
    Container content = getContentPane(); 
     westPane(content);

     setSize(600,600);
     setVisible(true);

}
public String methode(String s)
    {
     LinkedList <String> lk =new LinkedList();   
 try
        {
            db.outResult=db.s.executeQuery("select NUM_AFFILIATION from app.CLIENT where NUM_AFFILIATION like '"+s+"%'"); 
            while(db.outResult.next())
            {
                String n=db.outResult.getString(1); 
                lk.add(n); 
            }
            db.outResult.close();
        }
        catch(Exception e)
        {
           System.out.println("ikibazo"+e);       
        } 
        
        int max=0;
        
        for(int i=0;i<lk.size();i++)
        {
            String current = lk.get(i);
            
            String currentString="";
            String currentInt="";
            for(int j=0;j<current.length();j++)
            {
                char cc=current.charAt(j);
//                 System.out.println(c+"  ikibazo "+(c>='0' && c<='9')); 
                if(cc>='0' && cc<='9')
                {
                    currentInt=currentInt+c;                    
                }
                else
                {
                    currentString=currentString+c;
                }
            }  
                if(Integer.parseInt(currentInt)>=max && currentString.equals(s))
                {
                    max=Integer.parseInt(currentInt);
                } 
        }
            String res=s+(max+1);
            if((max+1)<10) {
            res=s+"0"+(max+1);
        }
            return res;
 }

public static int getIn (String in)
{
                      int productCode;
    try
    {
        if(in.equals(""))
        {

JOptionPane.showMessageDialog(null,"Ubusa  Ntabwo ari  Pourcentage ",in,JOptionPane.PLAIN_MESSAGE);
        productCode=-1;
        }
        else
        {
        Integer in_int=new Integer(in);
        productCode = in_int.intValue();
        if(productCode<0 || productCode>100)
             {

JOptionPane.showMessageDialog(null,"Ikibazo kuri  Pourcentage ",""+productCode,JOptionPane.PLAIN_MESSAGE);
        productCode=-1;
        }


        }
         }
    catch (NumberFormatException e)
    {
     JOptionPane.showMessageDialog(null,in," Ntabwo ari  Pourcentage ",JOptionPane.PLAIN_MESSAGE);

    productCode=-1;
    }
    return productCode;
}



/**************************************************************************
affiche du stock
**************************************************************************/

public void westPane(Container content )
{
JPanel Pane  = new JPanel();

Pane.setPreferredSize(new Dimension (600,600));

JPanel panel = new JPanel();
panel.setLayout(new GridLayout(22,2));
panel.setPreferredSize(new Dimension (400,560)); 
JScrollPane S =new JScrollPane(LANGUES);
  S.setPreferredSize( new Dimension (40,40) );

panel.add(NUM_AFFILIATIONL);panel.add(NUM_AFFILIATION);
panel.add(TITREL);panel.add(titre); 
panel.add(NOM_CLIENTL);panel.add(NOM_CLIENT);
panel.add(PRENOM_CLIENTL);panel.add(PRENOM_CLIENT);
panel.add(ASSURANCEL);

JScrollPane    scrollPaneProductList =new JScrollPane(societeJList);
scrollPaneProductList.setPreferredSize(new Dimension (40,40));
societeJList.setBackground(Color.pink);
panel.add(scrollPaneProductList);

panel.add(EMPLOYEURL);panel.add(EMPLOYEUR);
panel.add(SECTEURL);panel.add(SECTEUR);
panel.add(STATUSL);panel.add(status);
panel.add(SEXEL);panel.add(sexeList);
panel.add(DATE_NAISSANCEL);panel.add(DATE_NAISSANCE);
panel.add(LIEU_NAISSANCEL);panel.add(LIEU_NAISSANCE);
panel.add(LANGUEL);panel.add(S);
panel.add(REDUCTIONL);panel.add(REDUCTION);
panel.add(MOTIF_VOYAGEL);panel.add(MOTIF_VOYAGE);
panel.add(NATIONALITEL);panel.add(NATIONALITE);
panel.add(CODEL);panel.add(CODE);
panel.add(DATE_LIVRAISON_IDL);panel.add(DATE_LIVRAISON_ID);
panel.add(TELL);panel.add(TEL);
panel.add(EMAILL);panel.add(EMAIL);
panel.add(PREFERENCEL);panel.add(PREFERENCE);
panel.add(ETAL);panel.add(ETA);
panel.add(CREDIT_CARDL);panel.add(CREDIT_CARD);
    
Pane.add(panel,BorderLayout.WEST);
Pane.add(send,BorderLayout.SOUTH); 
content.add(Pane, BorderLayout.WEST); 
}
 
/**********************************************************************************************
AJOUT Mannuel
**********************************************************************************************/

Client getWritenClient()
{ 
 return new Client(   Integer.parseInt(REDUCTION.getText()) ,NUM_AFFILIATION.getText(),NOM_CLIENT.getText(),
PRENOM_CLIENT.getText(),""+societeJList.getSelectedValue(),EMPLOYEUR.getText(),
SECTEUR .getText(),CODE.getText(),""+status.getSelectedValue(),311225,
"" + sexeList.getSelectedValue(),"" + LANGUES.getSelectedValue(),PROFESSION.getText(),MOTIF_VOYAGE.getText(),
NATIONALITE.getText(),DATE_LIVRAISON_ID.getText(),LIEU_NAISSANCE.getText(),DATE_NAISSANCE.getText()
   ,TEL.getText(),PREFERENCE.getText(),"" +titre.getSelectedValue(),ETA.getText(),EMAIL.getText(),CREDIT_CARD.getText()      
          )  ; 
}

private void send()
{ 
send.addActionListener(new ActionListener() {
    @Override
    public void actionPerformed(ActionEvent ae)
{
     
if(ae.getSource()==send && NUM_AFFILIATION!=null && NUM_AFFILIATION.getText().length()>2 )
{  
Client cliente= getWritenClient();
if(cliente.NUM_AFFILIATION!=null && !cliente.NUM_AFFILIATION.equals(""))
{
    if(cliente.NOM_CLIENT!=null && !cliente.NUM_AFFILIATION.equals("")) {
        if(cliente.PRENOM_CLIENT!=null && !cliente.NUM_AFFILIATION.equals("")) {
            if(cliente.ASSURANCE!=null && !cliente.ASSURANCE.equals("")) {
                if(cliente.EMPLOYEUR!=null && !cliente.EMPLOYEUR.equals("")) {
                    if(cliente.SECTEUR!=null && !cliente.SECTEUR.equals("")) {
                        if(cliente.CODE!=null && !cliente.CODE.equals("")) {
                            if(cliente.SEXE!=null && !cliente.SEXE.equals("")) {
                                if(cliente.DATE_NAISSANCE!=null && !cliente.DATE_NAISSANCE.equals("")) {
                                    if(cliente.NATIONALITE!=null && !cliente.NATIONALITE.equals("")) {
                                        if(cliente.TEL!=null && !cliente.TEL.equals("")) {
                                            if(what.equals("UPDATE")) {
                                                db.upDateClient(cliente); 
                                            } 
                                            else
                                            {
                                                db.insertClient( cliente);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
} 
}
else {
        JOptionPane.showMessageDialog(rootPane, "REF#");
    }
}}); 
}


private void clientGroupe() {
    ASSURANCEL.addActionListener(
            new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent ae) {
                    if (ae.getSource() == ASSURANCEL) {
                        if (societeJList.getSelectedValue() != null) {
                            Client_Groupe cc = (Client_Groupe) societeJList.getSelectedValue();
                            
                            if(cc!=null && !cc.ID_CLIENT_GROUPE.equals("NEW")) {
                                new Client_Groupe_Interface(db, cc);
                            }
                            else {
                                new Client_Groupe_Interface(db );
                            }    
                        }  
                    }
                }
            });
}

@Override
protected void processWindowEvent(WindowEvent evt)
{
   if(WindowEvent.WINDOW_CLOSING == evt.getID())
   {
	   this.dispose();
			//default icon, custom title
//JOptionPane.showMessageDialog(this,"RIRAFUNGIRA RIMWE N'ISHYIGA"," IKIBAZO",JOptionPane.PLAIN_MESSAGE);
        			//this.setDefaultCloseOperation(HIDE_ON_CLOSE);


	}

}


public static void main(String[] arghs)
{
        try {
            StartServer d = new StartServer();
            AutoOut db = new AutoOut("localhost","ishyiga");
            db.read();

         //   new PersonInterface(db, "", "felsen");

        } catch (Throwable ex) {
            Logger.getLogger(Client_Interface.class.getName()).log(Level.SEVERE, null, ex);
        }

}
}
