
package Motel; 

import java.awt.Color;
import java.io.*;

import java.sql.*;
import java.util.Date;
import java.util.LinkedList;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import pos.Credit;
import pos.Invoice;
import pos.Main;
import pos.Proforma;
public  class AutoOut
{
    //jdbc:derby://192.168.0.113:1527/ishyiga;create=true
//   ## DEFINE VARIABLES SECTION ##
// define the driver to use 
    
String driver = "org.apache.derby.jdbc.NetWorkDriver";
// define the Derby connection URL to use  192.168.0.113 99.240.205.21
String connectionURL ; 
ResultSet outResult, outResult1, outResult2;
Connection conn = null;
Statement s,s1,s2;
LinkedList <Product>allProduct ;
LinkedList allEmploye ;
LinkedList<Lot>lot = new LinkedList();
PreparedStatement psInsert;
PreparedStatement psInsert1;
String  datez,mois,observa,ip,dbName;
LinkedList <String> controlInvoice ;
LinkedList<Invoice> lesFactures;
Rooms [] lesRooms = new Rooms [37] ;  
//LinkedList <UneReservation> allReservations;
int ID_INVOICE=0;
static Connection connStatic=null;
static Statement statementStatic=null;

AutoOut(String server,String dataBase)
{
this.dbName=dataBase;
connectionURL = "jdbc:derby://"+server+":1527/" + dataBase + ";create=true";
}
 public  void read()
{
  Date d= new Date();

        String day="";
        String mm="";
        String year="";


         if(d.getDate()<10)
        day="0"+d.getDate();
        else
        day=""+d.getDate();
        if((d.getMonth()+1)<10)
        mm="0"+(d.getMonth()+1);
        else
        mm=""+(d.getMonth()+1);
        if((d.getYear()-100)<10)
        year="0"+(d.getYear()-100);
        else
        year=""+(d.getYear()-100);



                 datez =day+mm+year;

                 mois= mm;

    try {
    // Create (if needed) and connect to the database
        System.out.println(connectionURL+" trying etablie");
    conn = DriverManager.getConnection(connectionURL);
System.out.println(connectionURL+"connection etablie");
    s = conn.createStatement();
    s1= conn.createStatement();
    s2= conn.createStatement();

 
    }
    catch (Throwable e){
System.err.println(e+" connection fail");
      insertError(e+"","Read");
    }
}
 public static void setStatCon(Connection conn,Statement st)
 {
     AutoOut.connStatic=conn;
     AutoOut.statementStatic=st;
 }
 
 public static void backUpDatabase(Connection conn,String file)throws SQLException
    {
    // Get today's date as a string:
    java.text.SimpleDateFormat todaysDate =new java.text.SimpleDateFormat("yyyy-MM-dd");
int f= (int)(5*Math.random());
String backupdirectory=file+f;


    CallableStatement cs = conn.prepareCall("CALL SYSCS_UTIL.SYSCS_BACKUP_DATABASE(?)");

    cs.setString(1, backupdirectory);
    cs.execute();
    cs.close();
    }

public int  getQuantite(int pro)
{

    int id=0;
    try
{
outResult = s.executeQuery("select  sum(qty_live) from APP.lot where id_product= "+pro);
while (outResult.next())
{
id =outResult.getInt(1);

}
  }  catch (Throwable e)  {
   insertError(e+"","getQuantite") ;
    }

 return id;
}
    LinkedList<Client_Groupe> getGroupe()
{ 
    LinkedList<Client_Groupe> clien  =new LinkedList();
    try
    { 
    outResult = s.executeQuery("select * from APP.Client_Groupe   order by INTITULE_CLIENT_GROUPE ");
 
    while (outResult.next())
    { 
 clien.add( new Client_Groupe(
    outResult.getString("ID_CLIENT_GROUPE"), outResult.getString("INTITULE_CLIENT_GROUPE")   ,      
   outResult.getString("TARIF_CLIENT_GROUPE"), outResult.getString("ADRESSE_CLIENT_GROUPE")   ,  
  outResult.getString("TEL_CLIENT_GROUPE"), outResult.getString("EMAIL_CLIENT_GROUPE")   ,
 outResult.getDouble("REDUCTION"), outResult.getInt("NOMBRE_CLIENT_GROUPE"))  
   );
    } 
    //  Close the resultSet
outResult.close();

      }  catch (Throwable e)  {

   insertError(e+""," V getlot");
    }
  
return clien;
}

public int  CashierYose(String nom,String Debut,String FIN)
{
    int sum=0;
    int id=0;
    try
{
    System.out.println("11");
outResult = s.executeQuery("select  total from APP.INVOICE where num_client='CASHNORM' and invoice.HEURE >'"+Debut+"' AND invoice.HEURE <'"+FIN+"' AND EMPLOYE='"+nom+"'");
 while (outResult.next())
{   
sum =sum+outResult.getInt(1);
 }
LinkedList<List>inv=new LinkedList();
LinkedList<List>cre=new LinkedList();
 inv.add(  new List (0,0,0,"",""));
 System.out.println("22");
 outResult = s.executeQuery("select  id_invoice,total,num_client from APP.INVOICE  where num_client!='CASHNORM' and invoice.HEURE >'"+Debut+"' AND invoice.HEURE <'"+FIN+"' AND EMPLOYE='"+nom+"' order by id_invoice");
while (outResult.next())
{
id = outResult.getInt(1); 
 inv.add(  new List (id,0,0,"",""));
} 
//System.out.println();
System.out.println("33");
outResult = s.executeQuery("select  id_invoice,payed from APP.CREDIT where  id_invoice>"+(inv.getFirst().id_invoice-1)+" and  id_invoice<"+(inv.getLast().id_invoice+1));
while (outResult.next())
{
id = outResult.getInt(1);
cre.add(  new List (id,outResult.getDouble(2),0,"",""));
 }
for(int i=0;i<inv.size();i++)
{
    System.out.println("44");
    for(int j=i;j<cre.size();j++)
if(inv.get(i).id_invoice==cre.get(j).id_invoice)
 sum=sum+  (int) cre.get(j).price;
 }
  }  catch (Throwable e)  {   
   insertError(e+"","CashierYose") ;
    }
    
 return sum;
}



public String  CashierBon(String nom,String Debut,String FIN)
{
String bons="";
    try
    {
  outResult = s.executeQuery("select  id_invoice,total,num_client from APP.INVOICE  where num_client!='CASHNORM' and invoice.HEURE >'"+Debut+"' AND invoice.HEURE <'"+FIN+"' AND EMPLOYE='"+nom+"' order by id_invoice");

  
  while (outResult.next())
{ 
int id = outResult.getInt(1);
int tot=outResult.getInt(2);
String client=outResult.getString(3);
//System.out.println(client);
bons=bons+" \n "+id+"  "+client ; 

}
  }  catch (Throwable e)  {   
   insertError(e+"","BonZose") ;
    }
  // System.out.println(bons); 
 return bons;
}

public LinkedList <Invoice> lesFacturesDUnChambre(int numChambre,String checkin,String checkout){
    
    LinkedList <Invoice> lesInvoices = new LinkedList<Invoice>();
//    Invoice inv;
//  
//    try
//    {System.err.println(" select * from APP.invoice where INVOICE.REFERENCE ="+numChambre+" AND INVOICE.HEURE > '"+checkin+"'AND INVOICE.HEURE < '"+checkout+"'");
//    outResult = s.executeQuery(" select * from APP.invoice where INVOICE.REFERENCE ="+numChambre+" AND INVOICE.HEURE > '"+checkin+"'AND INVOICE.HEURE < '"+checkout+"'");
//    while (outResult.next())
//    { 
//        
////       inv = new  Invoice (outResult.getInt(1),outResult.getString("EMPLOYE"),outResult.getString("DATE"), outResult.getString("NUM_CLIENT"),  outResult.getInt("TOTAL"),
//   //    outResult.getString("HEURE") ,outResult.getInt("TVA"),outResult.getInt("REDUCTION") );
//       System.out.println("test g");
//       
//     lesInvoices.add(inv);
//    }
//    }  catch (Throwable e)  {
//    insertError(e+""," openPTr");
//    }
  
    this.lesFactures = lesInvoices;
    return lesInvoices;
} 

public double getMontantMwikaze(int numChambre,String checkin,String checkout){
    double montant =0;;
    
    try
    {System.err.println(" select * from APP.invoice where INVOICE.REFERENCE ="+numChambre+" AND INVOICE.HEURE > '"+checkin+"'AND INVOICE.HEURE < '"+checkout+"'");
    outResult = s.executeQuery(" select * from APP.invoice where INVOICE.REFERENCE ="+numChambre+" AND INVOICE.HEURE > '"+checkin+"'AND INVOICE.HEURE < '"+checkout+"'");
    while (outResult.next())
    { 
        
       montant += montant + outResult.getDouble("TOTAL");
       
     //lesInvoices.add(inv);
    }
    }  catch (Throwable e)  {
    insertError(e+""," openPTr1");
    }
    
    
    return montant;
    
}

public void finaliseLesFacturesZikaze(int numChambre,String checkin,String checkout){
    
    
    
    
}



public String  CashierTot(String nom,String Debut,String FIN)
{
String bons="";
    try
    {
  outResult = s.executeQuery("select  id_invoice,total,num_client from APP.INVOICE  where num_client!='CASHNORM' and invoice.HEURE >'"+Debut+"' AND invoice.HEURE <'"+FIN+"' AND EMPLOYE='"+nom+"' order by id_invoice");
 while (outResult.next())
{ 
int id = outResult.getInt(1);
int tot=outResult.getInt(2);
String client=outResult.getString(3);
//System.out.println(client);
bons=bons+" \n "+ "  "+tot; 

}
  }  catch (Throwable e)  {   
   insertError(e+"","BonZose") ;
    }
   //System.out.println(bons); 
 return bons;
}

 void changePrice  (int c,double p,double tva,String db,String name,String  productName)
 {

   try
    {

       if(db.equals("CASHNORM"))
          s.execute(" update APP.PRODUCT set PRIX ="+p+", TVA = "+tva+" where ID_PRODUCT="+c+"");
       else
        s.execute(" update APP.PRODUCT set "+db+" ="+p+ "where ID_PRODUCT="+c+"");

       if(c!=5)
       insertChangePrice(name,productName,p);
        
     }  catch (Throwable e)  {
         insertError(e+"","changePrice") ;
     }

 }
  void changePriceRama  (int c,double p,String soc,String db,String name,String  productName,String obs)
 {

   try
    {
        s.execute(" update APP.PRODUCT set PRIX_RAMA ="+p+ ",OBSERVATION='"+obs+"' ,CODE_SOC_RAMA = '"+soc+ "' where ID_PRODUCT="+c+"");

       if(c!=5)
       insertChangePrice(name,productName,p);

          JOptionPane.showMessageDialog(null,db," INPUT ",JOptionPane.PLAIN_MESSAGE);

     }  catch (Throwable e)  {
         insertError(e+"","changePriceRama") ;
     }

 }
 void insertMissed(String desi,int qty,double price,int qty_dispo)
    {
        try {
            psInsert = conn.prepareStatement("insert into APP.MISSED(DESI,QUANTITE,PRICE,qty_dispo)  values (?,?,?,?)");

            psInsert.setString(1, desi);
            psInsert.setInt(2, qty);
            psInsert.setDouble(3, price);
            psInsert.setInt(4, qty_dispo); 
            psInsert.executeUpdate();
            psInsert.close();
        } catch (SQLException ex) {
             insertError(ex+"","insertMissed") ;
        }


          }

 void insertError(String desi,String ERR)
    {
        try {
            psInsert = conn.prepareStatement("insert into APP.ERROR(DESI,USERNAME)  values (?,?)");

            if(desi.length()>160)
                desi=desi.substring(0, 155);
             if(ERR.length()>160)
                ERR=ERR.substring(0, 155);

            psInsert.setString(1,desi );
            psInsert.setString(2, ERR);



            psInsert.executeUpdate();
            psInsert.close();
            JOptionPane.showMessageDialog(null,"  "+desi," ERROR ",JOptionPane.PLAIN_MESSAGE);



        } catch (Throwable ex) {
         JOptionPane.showMessageDialog(null,"  "+ex," ERROR ",JOptionPane.PLAIN_MESSAGE);   ;
        }


          } 
  public void upDateStatus3(int NUM_RESERVATION, String statut,String checkin,String checkout){
 
try {
    s = conn.createStatement();
    
    String createString1 = " update APP.RESERVATION set STATUT ='"+statut+"'"
            + ",RESERVATION.CHECKIN = '"+checkin +"', RESERVATION.CHECKOUT = '"+checkout+"'  "
            + "  where NUM_RESERVATION = "+NUM_RESERVATION+" ";
 System.out.println(createString1);
    s.execute(createString1);

    }
    catch (Throwable e){
          JOptionPane.showMessageDialog(null,e,"LE STATUT N'EST PAS MODIFIE ",JOptionPane.PLAIN_MESSAGE);

    }
 
     }
  public void upDateProformaAttente(String id_prof,String Chambre){
 
try {
    s = conn.createStatement(); 
    String createString1 = " update APP.BON_PROFORMA set REFERENCE ='"+Chambre+"'" 
            + "  where ID_PROFORMA = "+id_prof+" ";
 System.out.println(createString1);
    s.execute(createString1);
 JOptionPane.showMessageDialog(null, "PROFORMA N  "+id_prof +"  EST MIS EN "+Chambre+" "," CHANGEMENT",JOptionPane.PLAIN_MESSAGE);

    }
    catch (Throwable e){
          JOptionPane.showMessageDialog(null,e,"LE STATUT N'EST PAS MODIFIE ",JOptionPane.PLAIN_MESSAGE);
 } 
     }
  
    public void upDateInvoiceAttente(String id_inv,String Chambre){
 
try {
    s = conn.createStatement(); 
    String createString1 = " update APP.CREDIT set NUMERO_QUITANCE ='"+Chambre+"'" 
            + "  where ID_INVOICE = "+id_inv+" ";
 System.out.println(createString1);
    s.execute(createString1);
 JOptionPane.showMessageDialog(null,"INVOICE N  "+id_inv +"  EST MIS EN "+Chambre+" "," CHANGEMENT",JOptionPane.PLAIN_MESSAGE);

    }
    catch (Throwable e){
          JOptionPane.showMessageDialog(null,e,"LE STATUT N'EST PAS MODIFIE ",JOptionPane.PLAIN_MESSAGE);
 } 
     }
 static void insertError(String desi,String ERR, Connection con)
    {
        try {
          PreparedStatement  psInsert = con.prepareStatement("insert into APP.ERROR(DESI,USERNAME)  values (?,?)");

            if(desi.length()>160)
                desi=desi.substring(0, 155);
             if(ERR.length()>160)
                ERR=ERR.substring(0, 155);

            psInsert.setString(1,desi );
            psInsert.setString(2, ERR);



            psInsert.executeUpdate();
            psInsert.close();
            JOptionPane.showMessageDialog(null,"  "+desi," ERROR ",JOptionPane.PLAIN_MESSAGE);



        } catch (Throwable ex) {
         JOptionPane.showMessageDialog(null,"  "+ex," ERROR ",JOptionPane.PLAIN_MESSAGE);   ;
        }


          }
 void insertChangePrice(String USERNAME ,String PRODUCTNAME,double PRICE)
    {
        try {
            psInsert = conn.prepareStatement("insert into APP.CHANGEMENT(PRICE,USERNAME,PRODUCTNAME)  values (?,?,?)");


            psInsert.setDouble(1, PRICE);
            psInsert.setString(2, USERNAME );
            psInsert.setString(3, PRODUCTNAME );
            psInsert.executeUpdate();
            psInsert.close();
        } catch (Throwable ex) {
        insertError(ex+"","insertChangePrice");
        }


          }



public void addOneLot(Lot l)
{
     try
    {

     psInsert = conn.prepareStatement("insert into APP.LOT (ID_PRODUCT,ID_LOT,ID_LOTS,DATE_EXP,QTY_START,QTY_LIVE,BON_LIVRAISON)  values (?,?,?,?,?,?,?)");

    psInsert.setInt(1,l.productCode);
    psInsert.setInt(2,l.id_lot);
    psInsert.setString(3,l.id_LotS);
    psInsert.setString(4,l.dateExp);
    psInsert.setInt(5,l.qtyStart);
    psInsert.setInt(6,l.qtyLive);
    psInsert.setString(7,l.bon_livraison);
    psInsert.executeUpdate();
        psInsert.close();

          }  catch (Throwable e)  {   insertError(e+"","addOneLot"); }

}
void upDatePr(Product p)
{
 Statement sU;
        try {


            sU = conn.createStatement();
             String createString1 = " update APP.list set prix_revient=" +p.prix_revient+ " where id_invoice> 35555 and code_uni='"+p.code+"'";
 sU.execute(createString1);
        } catch (SQLException ex) {
            Logger.getLogger(AutoOut.class.getName()).log(Level.SEVERE, null, ex);
        }




}

public LinkedList doworkOther(String s1)
{
    try
    {
         observa=s1;
        System.out.println(s1);
        
    if(s1.contains("RAMA"))
        {
       // System.out.println(s1);
       outResult = s.executeQuery("select id_product,name_product,code,"+s1+", tva,code_bar,observation,code_soc_RAMA,prix_revient from PRODUCT order by NAME_PRODUCT");
       System.out.println("select id_product,name_product,code,"+s1+", tva,code_bar,observation,code_soc_RAMA,prix_revient from PRODUCT order by NAME_PRODUCT");

    allProduct= new LinkedList();
    //  Loop through the ResultSet and print the data
    while (outResult.next())
    {
        int productCode=outResult.getInt(1);
            String productName   =  outResult.getString(2);
            String code  =  outResult.getString(3);
            double currentPrice=outResult.getDouble(4);
            double tva=outResult.getDouble(5);
            String codeBar  =  outResult.getString(6);
            String observation  =  outResult.getString(7);
            String codeSoc  =  outResult.getString(8);
            double pr=outResult.getDouble(9);

    Product p= new Product(productCode,productName, code, 1,currentPrice,tva,codeBar,observation,codeSoc,this,pr);
     if(p.currentPrice>0)
            allProduct.add(p);
     }
    //  Close the resultSet
    outResult.close();
    }
    else     {
       outResult = s.executeQuery("select id_product,name_product,code,"+s1+", tva,code_bar,observation,prix_revient from PRODUCT order by NAME_PRODUCT");
      System.out.println("select id_product,name_product,code,"+s1+", tva,code_bar,observation,prix_revient from PRODUCT order by NAME_PRODUCT");

    allProduct= new LinkedList();
    //  Loop through the ResultSet and print the data
    while (outResult.next())
    {
    int productCode=outResult.getInt(1);
    String productName   =  outResult.getString(2);
    String code  =  outResult.getString(3);
    double currentPrice=outResult.getDouble(4);
    double tva=outResult.getDouble(5);
    String codeBar  =  outResult.getString(6);
    String observation  =  outResult.getString(7);
    // String codeSoc  =  outResult.getString(8);
    double pr=outResult.getDouble(8);

    Product p= new Product(productCode,productName, code, 1,currentPrice,tva,codeBar,observation,code,this,pr);
//System.out.println("S1:"+s1+"***"+p.currentPrice);

    if(p.currentPrice>0 && !s1.contains("_VAN"))
        allProduct.add(p);
    else if(s1.contains("_VAN"))
       allProduct.add(p);
     }
    //  Close the resultSet
    outResult.close();
    }


    //  Beginning of the primary catch block: uses errorPrint method
    }  catch (Throwable e)  {
    insertError(e+"","doworkOther");
    }

    return allProduct;
}
public LinkedList < Invoice> findInvoice(String numclient){
 LinkedList <Invoice >    facture = new LinkedList();   
 try{ 
  outResult = s.executeQuery("select * from APP.INVOICE where  INVOICE.NUM_CLIENT = "+numclient+" ");
       while (outResult.next())
    {
        int id_invoice=  outResult.getInt(1); 
        String clientw =outResult.getString(2);
        String z=outResult.getString(3);
        int total=outResult.getInt(4);
        String id_employe=  outResult.getString(5);
        String time =""+outResult.getTimestamp(6);
        int tva=outResult.getInt(7); 
        double reduction=outResult.getDouble(9);
         
    }
 outResult.close(); 
    } 
catch (Throwable e)  {
/*  Catch all exceptions and pass them to
**  the exception reporting method             */
System.out.println(" . . inv . exception thrown:");
insertError(e+""," V reimpression facture");
}
Invoice [] factureArray=new Invoice [facture.size()];

for(int i=0;i<factureArray.length;i++)
    factureArray[i]=facture.get(i);

  return facture  ;      
}










public  Invoice []   printByChoice(String choix)
{
    LinkedList <Invoice >    facture = new LinkedList();
try
    {
if (choix.equals("PAR GROUPE"))                                      
    { 
    String client = (String) JOptionPane.showInputDialog(null, "Fait votre choix", " ",
    JOptionPane.QUESTION_MESSAGE, null, getGroupe().toArray(), "");
    
    outResult = s.executeQuery("select * from APP.INVOICE where invoice.HEURE >'2011-01-01 00:00:00' AND  invoice.num_client='"+client+"' ORDER BY HEURE ");
     while (outResult.next())
    {
        int id_invoice=  outResult.getInt(1); 
        String clientw =outResult.getString(2);
        String z=outResult.getString(3);
        int total=outResult.getInt(4);
        String id_employe=  outResult.getString(5);
        String time =""+outResult.getTimestamp(6);
        int tva=outResult.getInt(7); 
        double reduction=outResult.getDouble(9);
          }
    }
    if (choix.equals("PAR HEURE"))                                      
    {
    String  Debut = JOptionPane.showInputDialog("ENTREZ LE TEMPS du debut",getString("debut"));
    String FIN = JOptionPane.showInputDialog   ("ENTREZ LE TEMPS de la fin ",getString("fin")); 
    
    outResult = s.executeQuery("select * from APP.INVOICE where  invoice.HEURE >'"+Debut+"' AND invoice.HEURE <'"+FIN+" ' ORDER BY HEURE ");
       while (outResult.next())
    {
        int id_invoice=  outResult.getInt(1); 
        String clientw =outResult.getString(2);
        String z=outResult.getString(3);
        int total=outResult.getInt(4);
        String id_employe=  outResult.getString(5);
        String time =""+outResult.getTimestamp(6);
        int tva=outResult.getInt(7); 
        double reduction=outResult.getDouble(9);
          }
    }
 outResult.close(); 
    } 
catch (Throwable e)  {
/*  Catch all exceptions and pass them to
**  the exception reporting method             */
System.out.println(" . . inv . exception thrown:");
insertError(e+""," V reimpression facture");
}

Invoice [] factureArray=new Invoice [facture.size()];

for(int i=0;i<factureArray.length;i++)
    factureArray[i]=facture.get(i);

  return factureArray  ;                                 
}
public LinkedList <List > listTva(String d,String f)
{
    LinkedList <List >   li= new LinkedList();
    try
    {
        outResult = s.executeQuery("SELECT APP.INVOICE.ID_INVOICE,CODE_UNI,QUANTITE,(APP.LIST.QUANTITE*APP.LIST.PRICE*APP.PRODUCT.TVA) FROM APP.INVOICE,APP.LIST,APP.PRODUCT WHERE  invoice.HEURE >'"+d+"' AND invoice.HEURE <'"+f+"' and APP.INVOICE.ID_INVOICE=APP.LIST.ID_INVOICE AND APP.LIST.CODE_UNI=APP.PRODUCT.CODE and product.TVA > 0");

        //  Loop through the ResultSet and print the data
        while (outResult.next())
        {
        int id=outResult.getInt(1);
        String code   =  outResult.getString(2);
        int qte=outResult.getInt(3);
        double currentPrice=outResult.getDouble(4);
         li.add(new List (id,  currentPrice,qte,"", code));
     }
    //  Close the resultSet
    outResult.close();

    //  Beginning of the primary catch block: uses errorPrint method
    }  catch (Throwable e)  {
    insertError(e+"  list tva ","tva ");
    }

    return li;
}
public void listPrixErrone(String d,String f,String cli)
{

    LinkedList <List >   li= new LinkedList();
    try
    {String j="_"+cli;
     if(cli.equals(""))
        j=cli;
        outResult = s.executeQuery("SELECT APP.INVOICE.ID_INVOICE,CODE_UNI,QUANTITE,prix"+j+",price FROM APP.INVOICE,APP.LIST,product  where invoice.num_client='"+cli+"'  and invoice.id_invoice > " + d + " and invoice.id_invoice < " + f  +" and invoice.id_invoice=list.id_invoice AND APP.LIST.CODE_UNI=APP.PRODUCT.CODE" );

        //  Loop through the ResultSet and print the data
        while (outResult.next())
        {
        int id=outResult.getInt(1);
        String code   =  outResult.getString(2);
        int qte=outResult.getInt(3);
        double currentPrice=outResult.getDouble(4);
         li.add(new List (id,  currentPrice,qte,"", code));
     }
    //  Close the resultSet
    outResult.close();

    for(int i=0;i<li.size();i++)
    {
        List p=li.get(i);

           System.out.println("update list set price=" + p.price + " where invoice="+p.id_invoice+" and code_uni='" + p.code + "'");
                s.execute(" update list set price=" + p.price + " where id_invoice="+p.id_invoice+" and code_uni='" + p.code + "'");
    }
    } catch (SQLException ex) {
                Logger.getLogger(AutoOut.class.getName()).log(Level.SEVERE, null, ex);
            }
    }


public void tva(String d,String f)
{
    LinkedList <List > li=listTva(d,f);
    try
    {
         s.execute(" update APP.invoice set tva =0  WHERE  invoice.HEURE >'"+d+"' AND invoice.HEURE <'"+f+"'");
        for(int i=0;i<li.size();i++)
         s.execute(" update APP.invoice set tva =tva+"+li.get(i).price+" where id_invoice="+li.get(i).id_invoice);
     }  catch (Throwable e)  {  insertError(e+"tva","fixtva");}
}

public boolean existe(String s1)
{
    boolean done = false;
    try
    {
       outResult = s.executeQuery("select * from PRODUCT where code='"+s1+"'");

    while (outResult.next())
    {
            done=true;
         JOptionPane.showMessageDialog(null," existe"," THANK YOU ",JOptionPane.PLAIN_MESSAGE);

     }
    outResult.close();

    }

catch (Throwable e)  {
    insertError(e+"","existe");
    }

    return done;
}
 void insertInsurance(Insurance izina) {//hano turinjizamo dukoresheje objet
        try {
            psInsert = conn.prepareStatement("insert into APP.VARIABLES(NOM_VARIABLE,VALUE_VARIABLE,FAMILLE_VARIABLE,VALUE2_VARIABLE)values(?,?,?,?)");//injizamo
//aha ama attribut ya objet yo agomba kwandikwa uko asanzwe ameze
            //nom_assurance,valeur_assurance,famille_assurance,valeur2_assurance;
             //nomAssurance,valeurAssurance,familleAssurance,valeur2Assurance
            psInsert.setString(1, izina.nomAssurance);
            psInsert.setString(2, izina.valeurAssurance);
            psInsert.setString(3, izina.familleAssurance);
            psInsert.setString(4, izina.valeur2Assurance);
            psInsert.executeUpdate();
            psInsert.close();
        } catch (SQLException ex) {
            System.out.println(" " + ex);
        }
        System.out.println("IKIBAZO CYAKIRIWE!!!!!!!!!!!!!");

    }
public void UpdateDesignation()
{
    try
    {

   /*    outResult = s.executeQuery("select id_product,name_product from PRODUCT order by NAME_PRODUCT");
    //
    allProduct= new LinkedList();
    //  Loop through the ResultSet and print the data
    while (outResult.next())
    {

   int code=outResult.getInt(1);
    String productName   =  outResult.getString(2);
     String sl=productName.replace('°', 'E');

     sl=productName.replace('°', 'e');
  Product p= new Product(code,sl, "", 1,0,0,"","","",this);
  System.out.println(sl);
    allProduct.add(p); 
        }
    //  Close the resultSet
    outResult.close();*/

/* for(int i=0;i<allProduct.size();i++)
 { System.out.println(allProduct.get(i));

   String productName=allProduct.get(i).productName;
   if(!productName.contains("'"))*/
    s.execute(" update APP.product set name_product ='Perdolan Bebe 12 Supp 1mg' where id_product="+1374);
  // } 
    //  Beginning of the primary catch block: uses errorPrint method
    }  catch (Throwable e)  {
    insertError(e+"","doworkOther");
    } 
}

 LinkedList<Product> doworkOtherData(String s1)
{  try
    { 
    if(s1.contains("RAMA"))
        {
        System.out.println(s1);
       outResult = s.executeQuery("select id_product,name_product,code,"+s1+", tva,code_bar,observation,code_soc_RAMA from PRODUCT order by NAME_PRODUCT");
 allProduct= new LinkedList();
    //  Loop through the ResultSet and print the data
    while (outResult.next())
    {
        int productCode=outResult.getInt(1);
            String productName   =  outResult.getString(2);
            String code  =  outResult.getString(3);
            double currentPrice=outResult.getDouble(4);
            double tva=outResult.getDouble(5);
            String codeBar  =  outResult.getString(6);
            String observation  =  outResult.getString(7);
            String codeSoc  =  outResult.getString(8);
            Product p= new Product(productCode,productName, code, 1,currentPrice,tva,codeBar,observation,codeSoc,this);
           // if(p.currentPrice>0)
            allProduct.add(p);

           // System.out.println(p);
     }
    //  Close the resultSet
    outResult.close();
    }
    else 
    {
    outResult = s.executeQuery("select id_product,name_product,code,"+s1+", tva,code_bar,observation from PRODUCT order by NAME_PRODUCT");
    //  System.out.println(s1);
    allProduct= new LinkedList();
    //  Loop through the ResultSet and print the data
    while (outResult.next())
    {
    int productCode=outResult.getInt(1);
    String productName   =  outResult.getString(2);
    String code  =  outResult.getString(3);
    double currentPrice=outResult.getDouble(4);
    double tva=outResult.getDouble(5);
    String codeBar  =  outResult.getString(6);
    String observation  =  outResult.getString(7);
    // String codeSoc  =  outResult.getString(8);
    Product p= new Product(productCode,productName, code, 1,currentPrice,tva,codeBar,observation,code,this);
    //  if(p.currentPrice>0)
    allProduct.add(p);
     }
    //  Close the resultSet
    outResult.close();
    }

    //  Beginning of the primary catch block: uses errorPrint method
    }  catch (Throwable e)  {
 insertError(e+"","doworkOther"); }

    return allProduct;
}

Product makeProduct (String ligne,int i)
{
        StringTokenizer st1=new StringTokenizer (ligne);


        //recuperer le nom
        String productName =st1.nextToken(";");
        //recupere code
        String code=st1.nextToken(";");
       // String d=st1.nextToken(";");

      StringTokenizer st2=new StringTokenizer (code);
     String d =st2.nextToken("/");


       // System.out.println(code);

Double in_int=new Double(d);
//int prix = in_int.intValue();
// Double in_int=new Double(d);
double price = in_int.doubleValue();
        //recupere quantite

        return  new Product(i,productName, code, 1,price,0,code,"","",null);


        }

 Product getProduct (int pr)
{

   Product p=null;

    try
    {
    outResult = s.executeQuery("select id_product,name_product,code,prix, tva,code_bar,observation from PRODUCT WHERE ID_PRODUCT = "+pr);


    //  Loop through the ResultSet and print the data
    while (outResult.next())
    {

             int productCode=outResult.getInt(1);
            String productName   =  outResult.getString(2);
            String code  =  outResult.getString(3);
            double currentPrice=outResult.getDouble(4);
            double tva=outResult.getDouble(5);
            String codeBar  =  outResult.getString(6);
            String observation  =  outResult.getString(7);
             p= new Product(productCode,productName, code, 1,currentPrice,tva,codeBar,observation,code,this);

     }
    //  Close the resultSet
    outResult.close();


    //  Beginning of the primary catch block: uses errorPrint method
    }  catch (Throwable e)  {
    insertError(e+"","getProduct");
    }

    return p;
}
LinkedList <Product> doworkVedette(LinkedList<Vedette>v,String s1)
{
    LinkedList <Product> allProductVedette = new LinkedList();

    try
    {

        for(int i =0;i<v.size();i++)
        {
            Vedette ve=v.get(i);


        if(s1.contains("RAMA"))
        {
       outResult = s.executeQuery("select id_product,name_product,code,"+s1+", tva,code_bar,observation,code_soc_rama,prix_revient from PRODUCT WHERE ID_PRODUCT = "+ve.ID_PRODUCT+"order by NAME_PRODUCT");

    allProduct= new LinkedList();
    //  Loop through the ResultSet and print the data
    while (outResult.next())
    {
        int productCode=outResult.getInt(1);
            String productName   =  outResult.getString(2);
        
            String code  =  outResult.getString(3);
            double currentPrice=outResult.getDouble(4);
            double tva=outResult.getDouble(5);
            String codeBar  =  outResult.getString(6);
            String observation  =  outResult.getString(7);
            String codeSoc  =  outResult.getString(8);
            double pr =outResult.getDouble(9);

            Product p= new Product(productCode,productName, code, 1,currentPrice,tva,codeBar,observation,codeSoc,this,pr);
            p.qty=ve.qte;
                   allProductVedette.add(p);
     }
    //  Close the resultSet
    outResult.close();
    }
        else
        {
       outResult = s.executeQuery("select id_product,name_product,code,"+s1+", tva,code_bar,observation,prix_revient from PRODUCT WHERE ID_PRODUCT = "+ve.ID_PRODUCT+" order by NAME_PRODUCT");

    allProduct= new LinkedList();
    //  Loop through the ResultSet and print the data
    while (outResult.next())
    {
        int productCode=outResult.getInt(1);
            String productName   =  outResult.getString(2); 
            String code  =  outResult.getString(3);
            double currentPrice=outResult.getDouble(4);
            double tva=outResult.getDouble(5);
            String codeBar  =  outResult.getString(6);
            String observation  =  outResult.getString(7);
           // String codeSoc  =  outResult.getString(8);
                        double pr=outResult.getDouble(8);

            Product p= new Product(productCode,productName, code, 1,currentPrice,tva,codeBar,observation,code,this,pr);
           p.qty=ve.qte;
            // if(p.currentPrice>0)
            allProductVedette.add(p);
     }
    //  Close the resultSet
    outResult.close();
    }

 
        }}
      catch (Throwable e)  {
 insertError(e+"","doworkVedette");
    }

    return allProductVedette;
}
public void insertVedette()
{

    try
    {
       psInsert = conn.prepareStatement("insert into APP.VEDETTE (ID_VEDETTE,TYPE_VEDETTE ,CAT_VEDETTE,NUM_VEDETTE)  values (?,?,?,?)");
       int j=0;
         for(int i=0;i<12;i++)
       {
         psInsert.setInt(1,j);
         psInsert.setString(2,"VEDETTE");
         psInsert.setInt(3,1);
         psInsert.setInt(4,i);
         psInsert.executeUpdate();
         j++;
        }
         for(int i=0;i<12;i++)
       {     psInsert.setInt(1,j);
             psInsert.setString(2,"BIOTIQUE");
         psInsert.setInt(3,2);
         psInsert.setInt(4,i);
         psInsert.executeUpdate();
           j++;
        }
         for(int i=0;i<12;i++)
       {   psInsert.setInt(1,j);
             psInsert.setString(2,"HYPERTENSEUR.");
         psInsert.setInt(3,3);
         psInsert.setInt(4,i);
         psInsert.executeUpdate(); j++;
        }
         for(int i=0;i<12;i++)
       {
         psInsert.setInt(1,j);j++;
         psInsert.setString(2,"PROTOZOAIRE");
         psInsert.setInt(3,4);
         psInsert.setInt(4,i);
         psInsert.executeUpdate();
        } for(int i=0;i<12;i++)
       {
         psInsert.setInt(1,j);j++;
         psInsert.setString(2,"DIABETIQUE");
         psInsert.setInt(3,5);
         psInsert.setInt(4,i);
         psInsert.executeUpdate();
        }
        for(int i=0;i<12;i++)
       {
         psInsert.setInt(1,j);j++;
         psInsert.setString(2,"DOULEUR");
         psInsert.setInt(3,6);
         psInsert.setInt(4,i);
         psInsert.executeUpdate();
        }
        for(int i=0;i<12;i++)
       {
         psInsert.setInt(1,j);j++;
         psInsert.setString(2,"VERMIFUGE");
         psInsert.setInt(3,7);
         psInsert.setInt(4,i);
         psInsert.executeUpdate();
        }
        for(int i=0;i<12;i++)
       {
         psInsert.setInt(1,j);j++;
         psInsert.setString(2,"HISTAMINIQUE");
         psInsert.setInt(3,8);
         psInsert.setInt(4,i);
         psInsert.executeUpdate();
        }








      // Release the resources (clean up )
    psInsert.close();


     }  catch (Throwable e)  {  insertError(e+"","insertVedette");}

}
public LinkedList doVedette()
{
       LinkedList  vedette = new LinkedList();
    try
    {
    outResult = s.executeQuery("select * from APP.VEDETTE ");

    //  Loop through the ResultSet and print the data
    while (outResult.next())
    {
         int id =outResult.getInt(1);
         String type =outResult.getString(2);
         int cat =outResult.getInt(3);
        int num =outResult.getInt(4);
        int pro =outResult.getInt(5);
         int qte =outResult.getInt(6);
        // System.out.println(type);
        vedette.add(new Vedette(id, num,cat,type,pro,qte));
     }
    //  Close the resultSet
    outResult.close();




    //  Beginning of the primary catch block: uses errorPrint method
    }  catch (Throwable e)  {
    insertError(e+"","doVedette");
    }
   return vedette;

}

public void fixVedette(Vedette v)
{
    try
    {
        System.out.println(" update APP.VEDETTE set ID_PRODUCT ="+v.ID_PRODUCT+", qte="+v.qte+" where ID_VEDETTE="+v.id+"");
          s.execute(" update APP.VEDETTE set ID_PRODUCT ="+v.ID_PRODUCT+", qte="+v.qte+" where ID_VEDETTE="+v.id+"");

     }  catch (Throwable e)  {  insertError(e+"","fixVedette");}

}

void emp()
{
    try
  {


    outResult = s.executeQuery("select * from APP.EMPLOYE ");
    allEmploye= new LinkedList();
    //  Loop through the ResultSet and print the data
   while (outResult.next())
    {
    allEmploye.add(new Employe(outResult.getInt(1),outResult.getInt(5),outResult.getInt(6),outResult.getInt(9),outResult.getString(2),outResult.getString(3),outResult.getString(4),outResult.getString(7),outResult.getString(8)));
    }

    //  Close the resultSet
    outResult.close();



    //  Beginning of the primary catch block: uses errorPrint method
    }  catch (Throwable e)  {
   insertError(e+"","employe");
    }



}
public boolean check_db(int id, int carte)
{
int c =allEmploye.size();
		boolean done=false;

		for(int i=0;i<c;i++)
		{
			Employe e= ((Employe)(allEmploye.get(i)));
                        if(e.check(id, carte))
                            done=true;

}
return done;
}

public Employe getName(int id)
{

Employe s1 =new Employe(0,0,0,0,"","","","","");
      try
  {


    outResult = s.executeQuery("select * from APP.EMPLOYE WHERE ID_EMPLOYE="+id);
    allEmploye= new LinkedList();
    //  Loop through the ResultSet and print the data
   while (outResult.next())
    {
    s1=new Employe(outResult.getInt(1),outResult.getInt(5),outResult.getInt(6),outResult.getInt(9),outResult.getString(2),outResult.getString(3),outResult.getString(4),outResult.getString(7),outResult.getString(8));
   allEmploye.add(s1);
   }

    //  Close the resultSet
    outResult.close();
   }  catch (Throwable e)  {

   JOptionPane.showMessageDialog(null," EMPLOYEE  "," ERROR ",JOptionPane.ERROR_MESSAGE);


          }
return s1;
}
public Employe getEmp(String  name)
{

Employe s1 =new Employe(0,0,0,0,"","","","","");
      try
  {


    outResult = s.executeQuery("select * from APP.EMPLOYE WHERE NOM_EMPLOYE='"+name+"'");
    allEmploye= new LinkedList();
    //  Loop through the ResultSet and print the data
   while (outResult.next())
    {
    s1=new Employe(outResult.getInt(1),outResult.getInt(5),outResult.getInt(6),outResult.getInt(9),outResult.getString(2),outResult.getString(3),outResult.getString(4),outResult.getString(7),outResult.getString(8));
   allEmploye.add(s1);
   }

    //  Close the resultSet
    outResult.close();
   }  catch (Throwable e)  {

   JOptionPane.showMessageDialog(null," EMPLOYEE  "," ERROR ",JOptionPane.ERROR_MESSAGE);


          }
return s1;
}

public void insertEmp(Employe c)
{
     try
    {

psInsert = conn.prepareStatement("insert into APP.employe  (ID_EMPLOYE,NOM_EMPLOYE,PRENOM_EMPLOYE ,BP_EMPLOYE " +
        ",CATRE_INDETITE , LEVEL_EMPLOYE,TITRE_EMPLOYE,PWD_EMPLOYE,TEL_EMPLOYE  ) values (?,?,?,?,?,?,?,?,? )");


psInsert.setInt(1,c.ID_EMPLOYE);
psInsert.setString(2,c.NOM_EMPLOYE);
psInsert.setString(3,c.PRENOM_EMPLOYE);
psInsert.setString(4,c.BP_EMPLOYE);
psInsert.setInt(5,c.CATRE_INDETITE);
psInsert.setInt(6,c.LEVEL_EMPLOYE);
psInsert.setString(7,c.TITRE_EMPLOYE);
psInsert.setString(8,c.PWD_EMPLOYE);
psInsert.setInt(9,c.TEL_EMPLOYE);
psInsert.executeUpdate();
    psInsert.close();
    JOptionPane.showMessageDialog(null," UMUKIRIYA ARINJIYE NTAKIBAZO "," MURAKOZE ",JOptionPane.PLAIN_MESSAGE);


          }  catch (Throwable e)  {

   JOptionPane.showMessageDialog(null," Umukiriya ashobora kuba arimo  "," DUPLICAGE ",JOptionPane.ERROR_MESSAGE);


          }
 }
public void upDateEmp(Employe c,int code )
 {
        try {

            Statement sU = conn.createStatement();
            String createString1 = " update Employe set NOM_EMPLOYE='" + c.NOM_EMPLOYE + "',PRENOM_EMPLOYE='" + c.PRENOM_EMPLOYE + "' ," +
                    "CATRE_INDETITE=" + c.CATRE_INDETITE + " ,LEVEL_EMPLOYE=" + c.LEVEL_EMPLOYE + " , BP_EMPLOYE='" + c.BP_EMPLOYE + "'," +
                    "TITRE_EMPLOYE='" + c.TITRE_EMPLOYE + "',PWD_EMPLOYE='" + c.PWD_EMPLOYE + "' ,TEL_EMPLOYE=" + c.TEL_EMPLOYE + " where ID_EMPLOYE=" + code;
            sU.execute(createString1);
            JOptionPane.showMessageDialog(null," UMUKIRIYA ARINJIYE NTAKIBAZO "," MURAKOZE ",JOptionPane.PLAIN_MESSAGE);

        } catch (SQLException ex) {

            JOptionPane.showMessageDialog(null,"  "+ex," UpDate Client Fail " ,JOptionPane.ERROR_MESSAGE);
        insertError(ex+"","update");     } 
}
public void insertClient(Client c)
{
    System.out.println(c.REDUCTION+
"** \n"+c.NUM_AFFILIATION+
"** \n"+c.NOM_CLIENT+
"** \n"+c.PRENOM_CLIENT+
"** \n"+c.ASSURANCE+
"** \n"+c.EMPLOYEUR+
"** \n"+c.SECTEUR +
"** \n"+c.CODE+
"** \n"+c.STATUS+
"** \n"+311220+
"** \n"+c.SEXE+
"** \n"+c. LANGUE+
"** \n"+c.PROFESSION+
"** \n"+c.MOTIF_VOYAGE+
"** \n"+c.NATIONALITE+
"** \n"+c.DATE_LIVRAISON_ID+
"** \n"+c.LIEU_NAISSANCE+
"** \n"+c.DATE_NAISSANCE+
"** \n"+c.TEL);
try
{
       
    
    
psInsert = conn.prepareStatement("insert into APP.CLIENT (REDUCTION,NUM_AFFILIATION,NOM_CLIENT ,"
        + "PRENOM_CLIENT,ASSURANCE,EMPLOYEUR,SECTEUR ,CODE,STATUS, DATE_EXP,SEXE, LANGUE,PROFESSION,"
        + "MOTIF_VOYAGE,NATIONALITE,DATE_LIVRAISON_ID,LIEU_NAISSANCE,DATE_NAISSANCE, TEL"
        + ",PREFERENCE,TITRE,ETA,EMAIL,CREDIT_CARD ) values "
        + " (?,?,?,?,?,"
        + "  ?,?,?,?,?,"
        + "  ?,?,?,?,?,"
        + "  ?,?,?,?,?,?,?,?,? )");

psInsert.setInt   (1,c.REDUCTION);
psInsert.setString(2,c.NUM_AFFILIATION);
psInsert.setString(3,c.NOM_CLIENT);
psInsert.setString(4,c.PRENOM_CLIENT);
psInsert.setString(5,c.ASSURANCE);
psInsert.setString(6,c.EMPLOYEUR);
psInsert.setString(7,c.SECTEUR );
psInsert.setString(8,c.CODE);
psInsert.setString(9,c.STATUS);
psInsert.setInt(10,311220);
psInsert.setString(11,c.SEXE);
psInsert.setString(12,c. LANGUE);
psInsert.setString(13,c.PROFESSION);
psInsert.setString(14,c.MOTIF_VOYAGE);
psInsert.setString(15,c.NATIONALITE);
psInsert.setString(16,c.DATE_LIVRAISON_ID);
psInsert.setString(17,c.LIEU_NAISSANCE);
psInsert.setString(18,c.DATE_NAISSANCE);
psInsert.setString(19,c.TEL);
 psInsert.setString(20,c.PREFERENCE);
psInsert.setString(21,c.TITRE);
psInsert.setString(22,c.ETA);
psInsert.setString(23,c.EMAIL);
psInsert.setString(24,c.CREDIT_CARD);

psInsert.executeUpdate();
psInsert.close();

JOptionPane.showMessageDialog(null," UMUKIRIYA ARINJIYE NTAKIBAZO "," MURAKOZE ",JOptionPane.PLAIN_MESSAGE);
Statement sU = conn.createStatement();

String createString1 = "UPDATE APP.VARIABLES SET VALUE_VARIABLE = '"+c.CODE+"',VALUE2_VARIABLE = '"+c.ASSURANCE+"' WHERE NOM_VARIABLE = 'compteurClient'  AND FAMILLE_VARIABLE = 'RUN' " ;

sU.execute(createString1);

          }  catch (Throwable e)  {

   JOptionPane.showMessageDialog(null," Umukiriya ashobora kuba arimo  "+e," DUPLICAGE ",JOptionPane.ERROR_MESSAGE);
}
}

public void insertClientGroupe(Client_Groupe c)
{ 
    
try
{
        
psInsert = conn.prepareStatement("insert into APP.CLIENT_GROUPE (REDUCTION,ID_CLIENT_GROUPE,INTITULE_CLIENT_GROUPE ,"
        + "TARIF_CLIENT_GROUPE,ADRESSE_CLIENT_GROUPE,TEL_CLIENT_GROUPE,EMAIL_CLIENT_GROUPE ,NOMBRE_CLIENT_GROUPE ) values "
        + " (?,?,?,? ,"
        + "  ?,?,?,?  )");

psInsert.setInt   (1,(int)c.REDUCTION);
psInsert.setString(2,c.ID_CLIENT_GROUPE);
psInsert.setString(3,c.INTITULE_CLIENT_GROUPE);
psInsert.setString(4,c.TARIF_CLIENT_GROUPE);
psInsert.setString(5,c.ADRESSE_CLIENT_GROUPE);
psInsert.setString(6,c.TEL_CLIENT_GROUPE);
psInsert.setString(7,c.EMAIL_CLIENT_GROUPE );
psInsert.setInt(8,c.NOMBRE_CLIENT_GROUPE); 
 
psInsert.executeUpdate();
psInsert.close();

JOptionPane.showMessageDialog(null," SUCCESSFUL ENTERED "," THANKS ",JOptionPane.PLAIN_MESSAGE);
 
          }  catch (Throwable e)  {

   JOptionPane.showMessageDialog(null," Umukiriya ashobora kuba arimo  "+e," DUPLICAGE ",JOptionPane.ERROR_MESSAGE);
}
}
public void upDateClient(Client c)
 {
try {
Statement sU = conn.createStatement();

  
String createString1 = "update APP.CLIENT set "
        + " REDUCTION=" + c.REDUCTION + ","
        + " NOM_CLIENT='" + c.NOM_CLIENT + "',"
        + " PRENOM_CLIENT='"+ c.PRENOM_CLIENT + "',"
       // + " DATE_EXP=" + c.DATE_EXP+"," 
        + " ASSURANCE='" + c.ASSURANCE +"',"
        + " EMPLOYEUR='" + c.EMPLOYEUR + "',"
        + " SECTEUR='" + c.SECTEUR+ "', "
        + " LANGUE='" + c.LANGUE + "', "
        + " CODE='" + c.CODE+ "', "
        + " DATE_NAISSANCE='" + c.DATE_NAISSANCE+ "', "
        + " PROFESSION='" + c.PROFESSION+ "', "
        + " MOTIF_VOYAGE='" + c.MOTIF_VOYAGE+ "', "
        + " NATIONALITE='" + c.NATIONALITE+ "', "
        + " LIEU_NAISSANCE='" + c.LIEU_NAISSANCE+ "', "     
        + " DATE_LIVRAISON_ID='" + c.DATE_LIVRAISON_ID+ "', " 
         + " PREFERENCE='" + c.PREFERENCE+ "', "
        + " TITRE='" + c.TITRE+ "', "
        + " ETA='" + c.ETA+ "', "     
        + " EMAIL='" + c.EMAIL+ "', "  + " CREDIT_CARD='" + c.CREDIT_CARD+ "', "  
        + " TEL='" + c.TEL+ "', "  
        + " SEXE='" + c.SEXE + "'     where NUM_AFFILIATION='" + c.NUM_AFFILIATION + "'";
System.out.println(createString1);
sU.execute(createString1);
JOptionPane.showMessageDialog(null," UMUKIRIYA ARINJIYE NTAKIBAZO "," MURAKOZE ",JOptionPane.PLAIN_MESSAGE);
} catch (SQLException ex) {
JOptionPane.showMessageDialog(null,"  "+ex," UpDate Client Fail " ,JOptionPane.ERROR_MESSAGE);
insertError(ex+"","update");     } 
} 

public void upDateClientGroupe(Client_Groupe c)
 {
try {
Statement sU = conn.createStatement();

  
String createString1 = "update APP.CLIENT_GROUPE set "
        + " REDUCTION=" + c.REDUCTION + ","
        + " INTITULE_CLIENT_GROUPE='" + c.INTITULE_CLIENT_GROUPE + "',"
        + " ADRESSE_CLIENT_GROUPE='"+ c.ADRESSE_CLIENT_GROUPE + "'," 
        + " EMAIL_CLIENT_GROUPE='" + c.EMAIL_CLIENT_GROUPE +"',"
        + " TARIF_CLIENT_GROUPE='" + c.TARIF_CLIENT_GROUPE + "',"
        + " TEL_CLIENT_GROUPE='" + c.TEL_CLIENT_GROUPE+ "'  " 
        + "      where ID_CLIENT_GROUPE='" + c.ID_CLIENT_GROUPE + "'";
System.out.println(createString1);
sU.execute(createString1);
JOptionPane.showMessageDialog(null," SUCCESSFUL UPDATED "," THANKS ",JOptionPane.PLAIN_MESSAGE);
} catch (SQLException ex) {
JOptionPane.showMessageDialog(null,"  "+ex," UpDate GROUPE Fail " ,JOptionPane.ERROR_MESSAGE);
insertError(ex+"","update");     } 
} 
public int getRes(String num){ 
   
    LinkedList < String > des= new  LinkedList();
try
{ 
String sql=    "  select * from APP.RESERVATION WHERE  STATUT ='OCCUPIED' and ETAT = 'ACTIF'"
        + " and num_client='"+num+"' "
        + " order by num_room";
System.out.println(sql);
ResultSet outResult2 = s.executeQuery(sql);  
 
while (outResult2.next())
{   
des.add("RESER: "+outResult2.getInt("NUM_RESERVATION")+" ROOM"+outResult2.getInt("NUM_ROOM")); 
 }
outResult2.close();
}
catch (Throwable e)  {
insertError(e+" "," getRoom" );
} 

String CHOIX = (String) JOptionPane.showInputDialog(null, " Faites votre choix ", "RESERVATION ",
JOptionPane.QUESTION_MESSAGE, null, des.toArray(), "");

String [] sp= CHOIX.split(" ");

return Integer.parseInt(sp[1]);

}
public void getList()
{
try
    {
        outResult1 = s.executeQuery("select ID_INVOICE,CODE_UNI,QUANTITE,ID_PRODUCT from APP.LIST,APP.PRODUCT  WHERE PRODUCT.CODE=LIST.CODE_UNI");

        int k = 0 ;

        LinkedList <Integer> inve= new LinkedList();
          LinkedList <String> codel=new LinkedList();
          LinkedList <Integer> qt=new LinkedList();
          LinkedList <Integer> i=new LinkedList();

        while (outResult1.next() )
        {

         int INV  =outResult1.getInt(1);
        String code   =  outResult1.getString(2);
        int quantity  =  outResult1.getInt(3);
        int ID  =  outResult1.getInt(4);
        boolean done=true;

        inve.add(INV);
        codel.add(code);
        qt.add(quantity);
        i.add(ID);


        }
        outResult1.close();


for(int y = 40;y<inve.size();y++)
{
    boolean done=true;
         outResult = s.executeQuery("select * from APP.LOT where  ID_PRODUCT="+i.get(y)+"order by date_exp,id_lot ");
              int quantity=qt.get(y);
              String code= codel.get(y);
              int INV = inve.get(y);
         while (outResult.next()&& quantity>0 )
                {
                            int productCode =outResult.getInt(1);
                            int id =outResult.getInt(2);
                            String  id_LotS=  outResult.getString(3);
                            String date1=outResult.getString(4);
                            int qtyStart=outResult.getInt(5);
                            int qtyLive=outResult.getInt(6);
                            String bon_livraison=  outResult.getString(7);
 System.out.println(codel.get(y)+"   "+qtyLive+"   "+id_LotS);
 done=false;


                                    if(qtyLive>quantity)
                                    {

                                    qtyLive=qtyLive-quantity;


                Statement  sL= conn.createStatement();
                String createString2 = " update APP.LIST set num_lot='"+id_LotS+"' where code_uni='"+code+"'and id_invoice="+INV;

                sL.execute(createString2);



                                    Statement  sU = conn.createStatement();
                                    String createString1 = " update APP.LOT set QTY_LIVE="+qtyLive+" where BON_LIVRAISON='"+bon_livraison+"'AND ID_LOT="+id+" and ID_LOTS='"+id_LotS+"' and ID_PRODUCT="+productCode+"";

                                    sU.execute(createString1);
                                    quantity=0;

                                    //   System.out.println(id_LotS+" if "+qtyLive+" qtty "+quantity);
                                    done=false;
                                    }
                 else if(qtyLive<quantity)
                                {
                                quantity=quantity-qtyLive;

Statement  sL= conn.createStatement();
                String createString2 = " update APP.LIST set num_lot='"+id_LotS+"' where code_uni='"+code+"'and id_invoice="+INV;

                sL.execute(createString2);
                                PreparedStatement  psInsert2 = conn.prepareStatement("insert into APP.USED_LOT(ID_PRODUCT,ID_LOT,ID_LOTS,DATE_EXP,QTY_START,QTY_LIVE,BON_LIVRAISON)  values (?,?,?,?,?,?,?)");

                                psInsert2.setInt(1,productCode);
                                psInsert2.setInt(2,id);
                                psInsert2.setString(3,id_LotS);
                                psInsert2.setString(4,date1);
                                psInsert2.setInt(5,qtyStart);
                                psInsert2.setInt(6,qtyLive);
                                psInsert2.setString(7,bon_livraison);
                                psInsert2.executeUpdate();
                                psInsert2.close();

                                Statement sD = conn.createStatement();
                                String createString1 = " delete from lot where ID_LOT="+id+" and BON_LIVRAISON='"+bon_livraison+"' and date_exp= '"+date1+"' and id_LotS= '"+id_LotS+"' and ID_PRODUCT= "+productCode;

                                sD.execute(createString1);

                                }
                    else if(qtyLive==quantity)
                            {

                            if(quantity!=0)
                            {Statement  sL= conn.createStatement();
                String createString2 = " update APP.LIST set num_lot='"+id_LotS+"' where code_uni='"+code+"'and id_invoice="+INV;

                sL.execute(createString2);
                            }

                            quantity=quantity-qtyLive; done=false;
                            PreparedStatement  psInsert2 = conn.prepareStatement("insert into APP.USED_LOT(ID_PRODUCT,ID_LOT,ID_LOTS,DATE_EXP,QTY_START,QTY_LIVE,BON_LIVRAISON)  values (?,?,?,?,?,?,?)");

                            psInsert2.setInt(1,productCode);
                            psInsert2.setInt(2,id);
                            psInsert2.setString(3,id_LotS);
                            psInsert2.setString(4,date1);
                            psInsert2.setInt(5,qtyStart);
                            psInsert2.setInt(6,qtyLive);
                            psInsert2.setString(7,bon_livraison);
                            psInsert2.executeUpdate();
                            psInsert2.close();

                            Statement sD = conn.createStatement();
                            String createString1 = " delete from lot where ID_LOT="+id+" and BON_LIVRAISON='"+bon_livraison+"' and id_LotS= '"+id_LotS+"' and ID_PRODUCT= "+productCode;
                            sD.execute(createString1);

                            }
                     }
         if(done)
          System.out.println(codel.get(y));


        //  Close the resultSet
        outResult.close();
          }

}

       catch (Throwable e)  {
   insertError(e+"","getList");
    }

}
 
public LinkedList <Client> allClients()
{
Client c =null;
LinkedList <Client> client = new LinkedList();
try
{ 
outResult = s.executeQuery("select * from APP.CLIENT order by nom_client "); 
while (outResult.next())
{ 
c=new Client(outResult.getInt("REDUCTION"),outResult.getString("NUM_AFFILIATION"),outResult.getString("NOM_CLIENT"),outResult.getString("PRENOM_CLIENT"),
    outResult.getString("ASSURANCE"),outResult.getString("EMPLOYEUR"),outResult.getString("SECTEUR"),outResult.getString("CODE"),outResult.getString("STATUS"),
    outResult.getInt("DATE_EXP"),outResult.getString("SEXE"),outResult.getString("LANGUE"),outResult.getString("PROFESSION"),outResult.getString("MOTIF_VOYAGE"),
    outResult.getString("NATIONALITE"),outResult.getString("DATE_LIVRAISON_ID"),outResult.getString("LIEU_NAISSANCE"),outResult.getString("DATE_NAISSANCE"),
    outResult.getString("TEL"),
        outResult.getString("PREFERENCE"),outResult.getString("TITRE"),
        outResult.getString("ETA"),outResult.getString("EMAIL"),outResult.getString("CREDIT_CARD"));  
 
client.add(c); 
}
        //  Close the resultSet
    outResult.close();
      }  catch (Throwable e)  {
    insertError(e+"","getOldClient");
    } 
return  client; 
}
public LinkedList <UneReservation> allReservations()
{
UneReservation r =null;
LinkedList <UneReservation> reserv = new LinkedList();
try
{ 
outResult = s.executeQuery
("select RESERVATION.NUM_RESERVATION,RESERVATION.STATUT,RESERVATION.DATE_RESERVATION,RESERVATION.CHECKIN,"
        + "RESERVATION.CHECKOUT,RESERVATION.AMOUNT,RESERVATION.DEPOSIT, ROOM.NUMBER_ROMM,ROOM.BEDS,ROOM.STATUT,"
        + "ROOM.PRICE,ROOM.TV,ROOM.PHONE,ROOM.DOUCHE,ROOM.BAIGNOIR,ROOM.FREDGE,ROOM.CATEGORIE,ROOM.ROOM_VIEW,"
        + "ROOM.ID, ROOM.VIEW_ROOM,ROOM.ID_PRODUCT_ROOM,CLIENT.REDUCTION,CLIENT.NUM_AFFILIATION,CLIENT.NOM_CLIENT,"
        + "CLIENT.PRENOM_CLIENT,CLIENT.ASSURANCE,CLIENT.EMPLOYEUR, CLIENT.SECTEUR,CLIENT.CODE,CLIENT.STATUS,"
        + "CLIENT.DATE_EXP,CLIENT.SEXE,CLIENT.LANGUE,CLIENT.PROFESSION,CLIENT.MOTIF_VOYAGE,CLIENT.NATIONALITE,"
        + "CLIENT.DATE_LIVRAISON_ID,CLIENT.LIEU_NAISSANCE,CLIENT.DATE_NAISSANCE,CLIENT.TEL,"
        + "PREFERENCE,TITRE,ETA,EMAIL "
        + "from APP.RESERVATION,APP.CLIENT,APP.ROOM  "
        + "where ROOM.NUMBER_ROMM=RESERVATION.NUM_ROOM AND RESERVATION.STATUT!='CHECKOUT' "
        + "AND RESERVATION.NUM_CLIENT=CLIENT.NUM_AFFILIATION AND RESERVATION.STATUT!='FREE'"
        + " AND RESERVATION.STATUT!='' AND RESERVATION.ETAT='ACTIF' AND RESERVATION.STATUT is not null order by NUM_RESERVATION "); 

System.out.println("select RESERVATION.NUM_RESERVATION,RESERVATION.STATUT,RESERVATION.DATE_RESERVATION,RESERVATION.CHECKIN,"
        + "RESERVATION.CHECKOUT,RESERVATION.AMOUNT,RESERVATION.DEPOSIT, ROOM.NUMBER_ROMM,ROOM.BEDS,ROOM.STATUT,"
        + "ROOM.PRICE,ROOM.TV,ROOM.PHONE,ROOM.DOUCHE,ROOM.BAIGNOIR,ROOM.FREDGE,ROOM.CATEGORIE,ROOM.ROOM_VIEW,"
        + "ROOM.ID, ROOM.VIEW_ROOM,ROOM.ID_PRODUCT_ROOM,CLIENT.REDUCTION,CLIENT.NUM_AFFILIATION,CLIENT.NOM_CLIENT,"
        + "CLIENT.PRENOM_CLIENT,CLIENT.ASSURANCE,CLIENT.EMPLOYEUR, CLIENT.SECTEUR,CLIENT.CODE,CLIENT.STATUS,"
        + "CLIENT.DATE_EXP,CLIENT.SEXE,CLIENT.LANGUE,CLIENT.PROFESSION,CLIENT.MOTIF_VOYAGE,CLIENT.NATIONALITE,"
        + "CLIENT.DATE_LIVRAISON_ID,CLIENT.LIEU_NAISSANCE,CLIENT.DATE_NAISSANCE,CLIENT.TEL "
        + "from APP.RESERVATION,APP.CLIENT,APP.ROOM  "
        + "where ROOM.NUMBER_ROMM=RESERVATION.NUM_ROOM AND RESERVATION.STATUT!='CHECKOUT' "
        + "AND RESERVATION.NUM_CLIENT=CLIENT.NUM_AFFILIATION AND RESERVATION.STATUT!='FREE'"
        + " AND RESERVATION.STATUT!='' AND RESERVATION.STATUT is not null order by NUM_RESERVATION "); 
while (outResult.next())
    
{ 
r=new UneReservation(outResult.getInt("NUM_RESERVATION"),new Rooms(outResult.getInt(8),outResult.getString(9),outResult.getString(10),outResult.getDouble(11),outResult.getInt(12),outResult.getInt(13),
            outResult.getInt(14), outResult.getInt(15),outResult.getInt(16),outResult.getString(17),outResult.getString(18),outResult.getInt(19),outResult.getInt(20),outResult.getInt(21),0,0),
            new Client(outResult.getInt(22),outResult.getString(23),outResult.getString(24),outResult.getString(25),
                outResult.getString(26),outResult.getString(27),outResult.getString(28) ,outResult.getString(29),outResult.getString(30),
                outResult.getInt("DATE_EXP"),outResult.getString(32),outResult.getString(33),outResult.getString(34),
                outResult.getString(35),outResult.getString(36),outResult.getString(37),outResult.getString(38),
                outResult.getString(39),outResult.getString(40),
        outResult.getString("PREFERENCE"),outResult.getString("TITRE"),outResult.getString("ETA"),outResult.getString("EMAIL")
        ,""),outResult.getString("STATUT"),
    outResult.getString("DATE_RESERVATION"),outResult.getString("CHECKIN"),outResult.getString("CHECKOUT"),outResult.getDouble("AMOUNT"),outResult.getDouble("DEPOSIT"));  
reserv.add(r); 
}
        //  Close the resultSet
    outResult.close();
      }  catch (Throwable e)  {
    insertError(e+"","getOldReserv");
    } 
return  reserv; 
}
public Rooms []  chambreBYCAT(String cat,String price)
{
    
   Rooms [] chambre= getRoomsCat(  cat,  price) ;
    
    
UneReservation r =null;
LinkedList <UneReservation> reserv = new LinkedList();
try
{ 
outResult = s.executeQuery
("select RESERVATION.NUM_RESERVATION,RESERVATION.STATUT,RESERVATION.DATE_RESERVATION,RESERVATION.CHECKIN,"
        + "RESERVATION.CHECKOUT,RESERVATION.AMOUNT,RESERVATION.DEPOSIT, ROOM.NUMBER_ROMM,ROOM.BEDS,ROOM.STATUT,"
        + "ROOM.PRICE,ROOM.TV,ROOM.PHONE,ROOM.DOUCHE,ROOM.BAIGNOIR,ROOM.FREDGE,ROOM.CATEGORIE,ROOM.ROOM_VIEW,"
        + "ROOM.ID, ROOM.VIEW_ROOM,ROOM.ID_PRODUCT_ROOM,CLIENT.REDUCTION,CLIENT.NUM_AFFILIATION,CLIENT.NOM_CLIENT,"
        + "CLIENT.PRENOM_CLIENT,CLIENT.ASSURANCE,CLIENT.EMPLOYEUR, CLIENT.SECTEUR,CLIENT.CODE,CLIENT.STATUS,"
        + "CLIENT.DATE_EXP,CLIENT.SEXE,CLIENT.LANGUE,CLIENT.PROFESSION,CLIENT.MOTIF_VOYAGE,CLIENT.NATIONALITE,"
        + "CLIENT.DATE_LIVRAISON_ID,CLIENT.LIEU_NAISSANCE,CLIENT.DATE_NAISSANCE,CLIENT.TEL,"
        + "PREFERENCE,TITRE,ETA,EMAIL "
        + "from APP.RESERVATION,APP.CLIENT,APP.ROOM  "
        + "where ROOM.NUMBER_ROMM=RESERVATION.NUM_ROOM AND RESERVATION.STATUT!='CHECKOUT' "
        + "AND RESERVATION.NUM_CLIENT=CLIENT.NUM_AFFILIATION AND RESERVATION.STATUT!='FREE'"
        + " AND RESERVATION.STATUT!='' AND RESERVATION.ETAT='ACTIF' AND RESERVATION.STATUT is not null order by NUM_RESERVATION "); 

System.out.println("select RESERVATION.NUM_RESERVATION,RESERVATION.STATUT,RESERVATION.DATE_RESERVATION,RESERVATION.CHECKIN,"
        + "RESERVATION.CHECKOUT,RESERVATION.AMOUNT,RESERVATION.DEPOSIT, ROOM.NUMBER_ROMM,ROOM.BEDS,ROOM.STATUT,"
        + "ROOM.PRICE,ROOM.TV,ROOM.PHONE,ROOM.DOUCHE,ROOM.BAIGNOIR,ROOM.FREDGE,ROOM.CATEGORIE,ROOM.ROOM_VIEW,"
        + "ROOM.ID, ROOM.VIEW_ROOM,ROOM.ID_PRODUCT_ROOM,CLIENT.REDUCTION,CLIENT.NUM_AFFILIATION,CLIENT.NOM_CLIENT,"
        + "CLIENT.PRENOM_CLIENT,CLIENT.ASSURANCE,CLIENT.EMPLOYEUR, CLIENT.SECTEUR,CLIENT.CODE,CLIENT.STATUS,"
        + "CLIENT.DATE_EXP,CLIENT.SEXE,CLIENT.LANGUE,CLIENT.PROFESSION,CLIENT.MOTIF_VOYAGE,CLIENT.NATIONALITE,"
        + "CLIENT.DATE_LIVRAISON_ID,CLIENT.LIEU_NAISSANCE,CLIENT.DATE_NAISSANCE,CLIENT.TEL "
        + "from APP.RESERVATION,APP.CLIENT,APP.ROOM  "
        + "where ROOM.NUMBER_ROMM=RESERVATION.NUM_ROOM AND RESERVATION.STATUT!='CHECKOUT' "
        + "AND RESERVATION.NUM_CLIENT=CLIENT.NUM_AFFILIATION AND RESERVATION.STATUT!='FREE'"
        + " AND RESERVATION.STATUT!='' AND RESERVATION.STATUT is not null order by NUM_RESERVATION "); 
while (outResult.next())
    
{ 
chambre[outResult.getInt(8)].une=new UneReservation(outResult.getInt("NUM_RESERVATION"),new Rooms(outResult.getInt(8),outResult.getString(9),outResult.getString(10),outResult.getDouble(11),outResult.getInt(12),outResult.getInt(13),
            outResult.getInt(14), outResult.getInt(15),outResult.getInt(16),outResult.getString(17),outResult.getString(18),outResult.getInt(19),outResult.getInt(20),outResult.getInt(21),0,0),
            new Client(outResult.getInt(22),outResult.getString(23),outResult.getString(24),outResult.getString(25),
                outResult.getString(26),outResult.getString(27),outResult.getString(28) ,outResult.getString(29),outResult.getString(30),
                outResult.getInt("DATE_EXP"),outResult.getString(32),outResult.getString(33),outResult.getString(34),
                outResult.getString(35),outResult.getString(36),outResult.getString(37),outResult.getString(38),
                outResult.getString(39),outResult.getString(40),
        outResult.getString("PREFERENCE"),outResult.getString("TITRE"),outResult.getString("ETA"),outResult.getString("EMAIL")
        ,""),outResult.getString("STATUT"),
    outResult.getString("DATE_RESERVATION"),outResult.getString("CHECKIN"),outResult.getString("CHECKOUT"),outResult.getDouble("AMOUNT"),outResult.getDouble("DEPOSIT"));  
reserv.add(r); 
}
        //  Close the resultSet
    outResult.close();
      }  catch (Throwable e)  {
    insertError(e+"","getOldReserv");
    } 
return  chambre; 
}
public static Client getClient(String num,Connection con)
{
Client c =null;
 try
{ 
    Statement st=  con.createStatement();
ResultSet outResult2 = st.executeQuery("select * from APP.CLIENT where NUM_AFFILIATION='"+num+"'"); 
while (outResult2.next())
{ 
return new Client(outResult2.getInt("REDUCTION"),outResult2.getString("NUM_AFFILIATION"),outResult2.getString("NOM_CLIENT"),outResult2.getString("PRENOM_CLIENT"),
    outResult2.getString("ASSURANCE"),outResult2.getString("EMPLOYEUR"),outResult2.getString("SECTEUR"),outResult2.getString("CODE"),outResult2.getString("STATUS"),
    outResult2.getInt("DATE_EXP"),outResult2.getString("SEXE"),outResult2.getString("LANGUE"),outResult2.getString("PROFESSION"),outResult2.getString("MOTIF_VOYAGE"),
    outResult2.getString("NATIONALITE"),outResult2.getString("DATE_LIVRAISON_ID"),outResult2.getString("LIEU_NAISSANCE"),outResult2.getString("DATE_NAISSANCE"),
    outResult2.getString("TEL"),outResult2.getString("PREFERENCE"),outResult2.getString("TITRE"),outResult2.getString("ETA"),outResult2.getString("EMAIL"),outResult2.getString("CREDIT_CARD"));   
}
        //  Close the resultSet
    outResult2.close();
      }  catch (Throwable e)  {
    insertError(e+"","getOldClient",con);
    } 
return  c; 
}
 public void insertLogin(String id,String in_out,int frw)
{
    try
    {
       psInsert = conn.prepareStatement("insert into APP.LOGIN (ID_EMPLOYE,IN_OUT,ARGENT )  values (?,?,?)");
        psInsert.setString(1,id);
         psInsert.setString(2,in_out);
         psInsert.setInt(3,frw);
         psInsert.executeUpdate();
          psInsert.close();
          }  catch (Throwable e)  {
  insertError(e+"","insertLogin");
    }}
 void addOneNew(Product p)
{
    try
    {

    psInsert = conn.prepareStatement("insert into PRODUCT (id_product,name_product,code,prix,prix_SOCIETE, tva,code_bar,observation )  values (?,?,?,?,?,?,?,?)");
    psInsert.setInt(1,p.productCode);
    psInsert.setString(2,p.productName);
    psInsert.setString (3,p.code );
    psInsert.setDouble(4,p.currentPrice);
    psInsert.setDouble(5,(p.currentPrice)*0.85);
    psInsert.setDouble(6,p.tva);
     psInsert.setString (7,p.code );
      psInsert.setString (8,p.observation );

    psInsert.executeUpdate();

    // Release the resources (clean up )
    psInsert.close();
    }  catch (Throwable e)  { insertError(e+"","addonenew");}


}
 void addRoom(Rooms rm)
{
    try
    {

    psInsert = conn.prepareStatement("insert into ROOM "
            + "(NUMBER_ROMM,BEDS,STATUT,PRICE,TV,PHONE,DOUCHE,BAIGNOIR,FREDGE,CATEGORIE,ROOM_VIEW"
            + ",NAME,ID,VIEW_ROOM )  values ("
            + "?,?,?,?,?,"
            + "?,?,?,?,?,"
            + "?,?,?,?)");
    psInsert.setInt(1,rm.room_number);
    psInsert.setString(2,rm.lits); 
    psInsert.setString(3,rm.statut);
    psInsert.setDouble(4,rm.price); 
    psInsert.setInt(5,rm.tv);
    psInsert.setInt(6,rm.phone);
    psInsert.setInt(7,rm.douche);
    psInsert.setInt(8,rm.baignoir);
    psInsert.setInt(9,rm.frigo);
    psInsert.setString(10,rm.categorie);
    psInsert.setString(11,rm.view);
    psInsert.setString(12,"");
    psInsert.setInt(13,rm.ID);
    psInsert.setInt(14,rm.VIEW);
    psInsert.executeUpdate();

    // Release the resources (clean up )
    psInsert.close();
    }  catch (Throwable e)  { insertError(e+"","addonenew room");}


}
boolean updateRoom(Rooms rm)
{
    try
    {
s = conn.createStatement();
String createString1 = " UPDATE ROOM SET "
+ "BEDS='"+rm.lits+"' ,STATUT='"+rm.statut+"' "
+ ",PRICE="+rm.price+",PRIX_DOUBLE="+rm.PRIX_DOUBLE+",PRIX_GROUPE="+rm.PRIX_GROUPE+" ,TV="+rm.tv+" ,PHONE="+rm.phone+" ,DOUCHE="+rm.douche+" ,"
+ "BAIGNOIR="+rm.baignoir+" ,FREDGE="+rm.frigo+" ,CATEGORIE='"+rm.categorie+"' ,"
+ "ROOM_VIEW='"+rm.view+"' ,ID="+rm.ID+",ID_PRODUCT_ROOM="+rm.ID_PRODUCT_ROOM+","
        + "VIEW_ROOM="+rm.VIEW+" WHERE NUMBER_ROMM="+rm.room_number;
//System.out.println(createString1);
s.execute(createString1);
return true;
    }  catch (Throwable e)  { insertError(e+"","update room");}

return false;
}

   LinkedList <String>getFichier(String fichier)throws FileNotFoundException, IOException
{
    LinkedList<String> give= new LinkedList();

        File productFile= new File(fichier);
        String  ligne =null;
        BufferedReader entree=new BufferedReader(new FileReader(productFile));
        try{ ligne=entree.readLine(); }
        catch (IOException e){ System.out.println(e);}
        int i=0;
        while(ligne!=null)
        {
        // System.out.println(i+ligne);
        give.add(ligne);
        i++;

        try{ ligne =entree.readLine();}
        catch (IOException e){  insertError(e+"","getFichier");}
        }
        entree.close();
return give;
}

   public int getMaxCodeMachine()
{
int id =0;
try
{
outResult = s.executeQuery("select max(ID_PRODUCT) from APP.PRODUCT ");
while (outResult.next())
{
id =outResult.getInt(1);
}
 outResult.close();
      }  catch (Throwable e)  {
    insertError(e+"","getmaxcodemachine");
    }
return id;

}
   public int getMaxInvoice()
{
int id =0;
try
{
outResult = s.executeQuery("select max(ID_INVOICE) from APP.Invoice ");
while (outResult.next())
{
id =outResult.getInt(1);
}
 outResult.close();
      }  catch (Throwable e)  {
    insertError(e+"","max invoice");
    }
return id;

}
//public int getIdFacture(String id_client,String date,int  tot, String id_employe,int tva)
//{
//int id =0;
//String id_clientdb="";
//String datedb="";
//int totdb=0;
//String id_employedb="";
//int tvadb=0;
//String heure="";
//boolean done=true;int compteur=0;
//try
//{
//outResult = s.executeQuery("select  max(ID_INVOICE) from APP.INVOICE ");
//while (outResult.next())
//{
//id =outResult.getInt(1);
//}
//
//while(done ||compteur>5)
//{
//
//compteur++;
//
//outResult = s.executeQuery("select  * from APP.INVOICE where ID_INVOICE= "+id);
//while (outResult.next())
//{
//
//    id_clientdb=outResult.getString(2);
//    datedb=outResult.getString(3);
//    totdb=outResult.getInt(4);
//    id_employedb=outResult.getString(5);
//    heure=""+outResult.getTimestamp(6);
//    tvadb=outResult.getInt(7);
//
//}
//if(id_clientdb.equals(id_client)&& datedb.equals(date)&& totdb==tot && id_employedb.equals(id_employe)&&tvadb==tva)
//{
//    done=false;
//
////JOptionPane.showMessageDialog(null,compteur,"ok "+id,JOptionPane.PLAIN_MESSAGE);
//
//
//}
//else
//id=id-1;
//
//}
// outResult.close();
//      }  catch (Throwable e)  {
//    insertError(e+"","getIdfacture");
//    }
// this.controlInvoice = new LinkedList();
//       controlInvoice.add("  FA"+id);
//        controlInvoice.add(heure);
// if(compteur==5)
//  insertError(id+" "+tot+""+date+""+id_client," manque idfacture");
//return id;
//
//}
   
  public int  getIdFacture(String keyInvoice)
  {
      
      try
      {
          outResult = s.executeQuery("select  ID_INVOICE from APP.INVOICE WHERE KEY_INVOICE='"+keyInvoice+"'");
          while (outResult.next())
          {
            return(outResult.getInt(1));
          }
          
          outResult.close();
      }
      catch (Throwable e)       
      {
        insertError(e+"","getIdfacture");
      }
     return -1;
      
  }
  
   String getString(String s)
{
    String ret=" ";
    Variable var=  getParam("RUN","MAIN",s+"MAIN");
    if(var!=null)
    ret=var.value2;
    return ret;
} 
   
    public LinkedList<String>  printRetard(int id_invoice,String printStringUp)
{

    LinkedList<String> printR =new LinkedList<String>();
//String printStringUp="    Tel: 573489 - BP: 263 - KIGALI \n    N ° TVA : 10000111 ";
int totale=0;
LinkedList lotList=new LinkedList();
boolean mut=false;
boolean done=false;
String printStringE="";
String printStringW="";

        String client="";
        String   num_affiliation="";
        String  nom_client="";
        String  prenom_client="";
        int  percentage=0;
        String secteur="";
        String employeur="";
        String date1="";
        String code=  "";
        String lot1 ="";
        String heure ="";
        String employe ="";
        int quantite =0;
        double price =0;
        String desi ="";
        String   numero_quitance  ="";
        String   code_soc  ="";
        String tva="";
        int aff=0;
        String NAISSANCE ="";String SEXE="";String LIEN=""; String BENEFICIAIRE="";

try
    {
// Clients Cash
outResult = s.executeQuery("" +
"select app.invoice.id_invoice,num_client,date,code_uni,num_lot,quantite" +
",list.price,name_product,heure,employe,invoice.tva  from APP.PRODUCT,APP.INVOICE ,APP.LIST where " +
"APP.INVOICE.NUM_CLIENT='CASHNORM' " +
"and APP.INVOICE.id_invoice = "+id_invoice+"  " +
"and APP.INVOICE.id_invoice = APP.LIST.id_invoice " +
"and  APP.PRODUCT.CODE=APP.LIST.CODE_UNI order" +
" by  app.invoice.id_invoice");

System.out.println("" +
"select app.invoice.id_invoice,num_client,date,code_uni,num_lot,quantite" +
",list.price,name_product,heure,employe,invoice.tva  from APP.PRODUCT,APP.INVOICE ,APP.LIST where " +
"APP.INVOICE.NUM_CLIENT='CASHNORM' " +
"and APP.INVOICE.id_invoice = "+id_invoice+"  " +
"and APP.INVOICE.id_invoice = APP.LIST.id_invoice " +
"and  APP.PRODUCT.CODE=APP.LIST.CODE_UNI order" +
" by  app.invoice.id_invoice");

while (outResult.next())
{

client=outResult.getString(2);
date1=outResult.getString(3);
code=  outResult.getString(4);
lot1 =outResult.getString(5);
quantite =outResult.getInt(6);
price =outResult.getDouble(7);
desi =outResult.getString(8);
printStringE=printStringE+ "\n  "+quantite+ "  "+desi;
printStringW=printStringW+ "\n "+(int)(quantite*price);
totale=totale+(int)(quantite*price);
done=true;
heure=""+outResult.getTimestamp(9);
        employe=outResult.getString(10);
        tva=""+outResult.getInt(11);
}
outResult.close();

        /// Clients Cash  Unipharma
        outResult = s.executeQuery("" +
        "select app.invoice.id_invoice,num_client,date,code_uni,num_lot," +
        "quantite,list.price,name_product,num_affiliation,nom_client," +
        "prenom_client,percentage,numero_quitance,heure,employe,invoice.tva  " +
        "from APP.PRODUCT,APP.INVOICE ,APP.LIST,APP.CREDIT,APP.CLIENT_UNIPHARMA " +
        "where APP.INVOICE.NUM_CLIENT ='CASHREMISE'  " +
        "and APP.credit.numero_affilie =  APP.client_UNIPHARMA.num_affiliation " +
        "and APP.INVOICE.id_invoice = "+id_invoice+" " +
        "and APP.INVOICE.id_invoice = APP.credit.id_invoice " +
        "and APP.INVOICE.id_invoice = APP.LIST.id_invoice " +
        "AND APP.PRODUCT.CODE=APP.LIST.CODE_UNI " +
        "order by app.invoice.id_invoice");
        
        System.out.println("" +
        "select app.invoice.id_invoice,num_client,date,code_uni,num_lot," +
        "quantite,list.price,name_product,num_affiliation,nom_client," +
        "prenom_client,percentage,numero_quitance,heure,employe,invoice.tva  " +
        "from APP.PRODUCT,APP.INVOICE ,APP.LIST,APP.CREDIT,APP.CLIENT_UNIPHARMA " +
        "where APP.INVOICE.NUM_CLIENT ='CASHREMISE'  " +
        "and APP.credit.numero_affilie =  APP.client_UNIPHARMA.num_affiliation " +
        "and APP.INVOICE.id_invoice = "+id_invoice+" " +
        "and APP.INVOICE.id_invoice = APP.credit.id_invoice " +
        "and APP.INVOICE.id_invoice = APP.LIST.id_invoice " +
        "AND APP.PRODUCT.CODE=APP.LIST.CODE_UNI " +
        "order by app.invoice.id_invoice");

        while (outResult.next())
        {
        client=outResult.getString(2);
        date1=outResult.getString(3);
        code=  outResult.getString(4);
        lot1 =outResult.getString(5);
        quantite =outResult.getInt(6);
        price =outResult.getDouble(7);
        desi =outResult.getString(8);
        num_affiliation=outResult.getString(9);
        nom_client=outResult.getString(10);
        prenom_client=outResult.getString(11);
        percentage=outResult.getInt(12);
        numero_quitance  =outResult.getString(13);
        printStringE=printStringE+ "\n  "+quantite+ "  "+desi;
        printStringW=printStringW+ "\n "+(int)(quantite*price);
        totale=totale+(int)(quantite*price);
        heure=""+outResult.getTimestamp(14);
        employe=outResult.getString(15);
        tva=""+outResult.getInt(16);
        }
        //  Close the resultSet  and date ='"+d+"'
        outResult.close();

       //// Clients Rama

            outResult = s.executeQuery("select secteur,employe ,date,code_uni,num_lot,quantite,list.price, name_product,num_affiliation," +
                    "nom_client,prenom_client,percentage,numero_quitance,code_soc_rama,employeur,invoice.tva,AGE,SEXE,LIEN,BENEFICIAIRE from APP.PRODUCT,APP.INVOICE ,APP.LIST,APP.CREDIT,APP.CLIENT_RAMA where APP.INVOICE.NUM_CLIENT ='RAMA' and APP.INVOICE.id_invoice =  "+id_invoice+"   and APP.INVOICE.id_invoice = APP.credit.id_invoice and APP.client_RAMA.num_affiliation = APP.credit.numero_affilie  and APP.INVOICE.id_invoice = APP.LIST.id_invoice  and APP.PRODUCT.CODE=APP.LIST.CODE_UNI  order by app.invoice.id_invoice");
            
            System.out.println("select secteur,employe ,date,code_uni,num_lot,quantite,list.price, name_product,num_affiliation," +
                    "nom_client,prenom_client,percentage,numero_quitance,code_soc_rama,employeur,invoice.tva,AGE,SEXE,LIEN,BENEFICIAIRE from APP.PRODUCT,APP.INVOICE ,APP.LIST,APP.CREDIT,APP.CLIENT_RAMA where APP.INVOICE.NUM_CLIENT ='RAMA' and APP.INVOICE.id_invoice =  "+id_invoice+"   and APP.INVOICE.id_invoice = APP.credit.id_invoice and APP.client_RAMA.num_affiliation = APP.credit.numero_affilie  and APP.INVOICE.id_invoice = APP.LIST.id_invoice  and APP.PRODUCT.CODE=APP.LIST.CODE_UNI  order by app.invoice.id_invoice");
            
            int i=0;
            while (outResult.next())
            {
            secteur=outResult.getString(1);

            client =outResult.getString(2);
            date1=outResult.getString(3);
            code=  outResult.getString(4);
            lot1 =outResult.getString(5);
            quantite =outResult.getInt(6);
            price =outResult.getDouble(7);
            desi =outResult.getString(8);
            num_affiliation=outResult.getString(9);
            nom_client=outResult.getString(10);
            prenom_client=outResult.getString(11);
            percentage=outResult.getInt(12);
            numero_quitance  =outResult.getString(13);
            code_soc  =outResult.getString(14);
             employeur =outResult.getString(15);
           //  tva =outResult.getDouble(16);
            NAISSANCE  =outResult.getString(17);
            SEXE  =outResult.getString(18);
            LIEN  =outResult.getString(19);
            BENEFICIAIRE=outResult.getString(20); 
            i++; 
//            System.out.println(numero_quitance); 
            int y=(int)((((int)((quantite*price))*(percentage/100.0))));   
            mut=true;
            
               int affilie = (int) (((((quantite*price)) *   (percentage/100.0)) )); 
               int ramaP   = (int) (((((quantite*price)) *  (1-percentage/100.0)) ));  
                
            aff=aff+y;
            totale=totale+(int)(quantite*price);
            Lot l = new Lot(code_soc,(i)+". "+desi,""+quantite,""+setVirgule((int)price), ""+setVirgule((int)(quantite*price)),""+setVirgule(affilie) ,""+setVirgule(ramaP),"","","","");
            lotList.add(l);
            }
            aff=(int)((totale*(percentage/100.0))+0.5);

                    ///// Clients Societe
                   if(!mut)
                   {
                    outResult = s.executeQuery("" +
                    "select secteur,num_client,date,code_uni,num_lot,quantite,list.price,name_product" +
                    ",num_affiliation,nom_client,prenom_client,percentage,numero_quitance,heure,employe,invoice.tva " +
                    "from APP.PRODUCT,APP.INVOICE ,APP.LIST,APP.CREDIT,APP.CLIENT " +
                    "where  APP.INVOICE.NUM_CLIENT !='CASHNORM'" +
                    "and APP.INVOICE.NUM_CLIENT !='CASHREMISE'" +
                    "and APP.INVOICE.NUM_CLIENT !='RAMA'" +
                    "and APP.client.num_affiliation = APP.credit.numero_affilie " +
                    "and APP.INVOICE.id_invoice = "+id_invoice+
                    " and APP.INVOICE.id_invoice = APP.credit.id_invoice " +
                    "and APP.INVOICE.id_invoice = APP.LIST.id_invoice  " +
                    "AND APP.PRODUCT.CODE=APP.LIST.CODE_UNI" +
                    " order by app.invoice.id_invoice");
                    
                    System.out.println("" +
                    "select secteur,num_client,date,code_uni,num_lot,quantite,list.price,name_product" +
                    ",num_affiliation,nom_client,prenom_client,percentage,numero_quitance,heure,employe,invoice.tva " +
                    "from APP.PRODUCT,APP.INVOICE ,APP.LIST,APP.CREDIT,APP.CLIENT " +
                    "where  APP.INVOICE.NUM_CLIENT !='CASHNORM'" +
                    "and APP.INVOICE.NUM_CLIENT !='CASHREMISE'" +
                    "and APP.INVOICE.NUM_CLIENT !='RAMA'" +
                    "and APP.client.num_affiliation = APP.credit.numero_affilie " +
                    "and APP.INVOICE.id_invoice = "+id_invoice+
                    " and APP.INVOICE.id_invoice = APP.credit.id_invoice " +
                    "and APP.INVOICE.id_invoice = APP.LIST.id_invoice  " +
                    "AND APP.PRODUCT.CODE=APP.LIST.CODE_UNI" +
                    " order by app.invoice.id_invoice");
                    i=0;
                    while (outResult.next())
                    {

                    client=outResult.getString(2);
                    date1=outResult.getString(3);
                    code=  outResult.getString(4);
                    lot1 =outResult.getString(5);
                    quantite =outResult.getInt(6);
                    price =outResult.getDouble(7);
                    desi =outResult.getString(8);
                    num_affiliation=outResult.getString(9);
                    nom_client=outResult.getString(10);
                    prenom_client=outResult.getString(11);
                    percentage=outResult.getInt(12);
                    numero_quitance  =outResult.getString(13);
                    printStringE=printStringE+ "\n      "+quantite+ "  "+desi;
                    printStringW=printStringW+ "\n   "+(int)(quantite*price);
                    totale=totale+(int)(quantite*price);
                    heure=""+outResult.getTimestamp(14);
        employe=outResult.getString(15);
        tva=""+outResult.getInt(16);
        i++;
             
               int affilie = (totale- (int)(totale*percentage*0.01));
               int ramaP   = (int)(totale*percentage*0.01);  
                
            Lot l = new Lot(code_soc,(i)+". "+desi,""+quantite,""+setVirgule((int)price), ""+setVirgule((int)(quantite*price)),""+setVirgule(affilie) ,""+setVirgule(ramaP),"","","","");
            lotList.add(l);
                    }
                    outResult.close();
                    }
if(done)// SI CASH
{
    System.out.println("nahageze1"+id_invoice+"**"+client);
    
printStringUp=printStringUp+ "\n          FACTURE N°    : " +id_invoice;
printStringUp = printStringUp + "\n         " + date1 + "   " + heure.substring(10) + "  #: " + employe;
              
printStringE="    "+printStringE+ "\n  ";
printStringW=printStringW+ "\n   ";
printStringE=printStringE+ "\n       TOTAL en RWF: ";
printStringW=printStringW+ "\n"+(int)totale;
printStringE=printStringE+ "\n       Tva : ";
printStringW=printStringW+ "\n  "+tva;
printStringE=printStringE+ "\n             Merci de votre visite."  ;
printStringW=printStringW+ "\n "; 
}
else if(mut)
 {
     System.out.println("nahageze2");
 //PrintCommandM(printStringUp,printStringE,printStringW,printStringW1,down,merci,visa,reception);String EMPLOYEUR,String SECTEUR,String VISA,String CODE
// new Client(NUM_AFFILIATIONa,NOM_CLIENTa ,PRENOM_CLIENTa ,PERCENTAGEa,DATE_EXPa,ASSURANCEa ,EMPLOYEURa,SECTEURa,"",CODE.getText(),NAISSANCE.getText(),SEXE.getText(),link))
//new Print(lotList,this,"FACTURE",date1,new Client( num_affiliation,nom_client ,prenom_client ,0,0,"RAMA" ,employeur,secteur,"","",NAISSANCE,SEXE,LIEN,BENEFICIAIRE ),id_invoice,(int)aff,totale,new String [] {"","","","","","","","","",""}, numero_quitance,client,"RAMA");
 }
                else
                {
                    String ch="";
                    if(client.equals("MMI"))
                    {
                        ch = (String) JOptionPane.showInputDialog(null, "Fait votre choix", "Impression ",
                                JOptionPane.QUESTION_MESSAGE, null, new String[]{"EPSON", "A5",}, "");
                        if(ch.equals("A5"))   
                        {
                            printR=new LinkedList<String>();
                            //new Print(lotList,this,"FACTUREMMI",date1,new Client( num_affiliation,nom_client ,prenom_client ,0,0,"MMI" ,employeur,secteur,"","",NAISSANCE,SEXE,LIEN,BENEFICIAIRE ),id_invoice,(int)(totale*percentage*0.01),totale,new String [] {"","","","","","","","","",""}, numero_quitance,client,"MMI");                       
                        }
                    }
                    if((client.equals("MMI")&& ch.equals("EPSON")) || !client.equals("MMI"))
                    {
                        System.out.println("nahageze3");
                        printStringUp=printStringUp+ "\n    FACTURE N°    : " +id_invoice;
                        printStringUp=printStringUp+ "\n    " +date1 ;
                        printStringUp=printStringUp+"\n    -------------------------------------------   ";
                        printStringUp=printStringUp+"\n    Assureur            : "+client;
                        printStringUp=printStringUp+ "\n    N° Affiliation      : "+num_affiliation;
                        printStringUp=printStringUp+ "\n    Nom et Prenom : "+nom_client+ " "+prenom_client;
                        //   printStringUp=printStringUp+ "\n    Departement     : "+c.EMPLOYEUR;
                        printStringUp=printStringUp+ "\n    N° Quittance     : "+numero_quitance;
                        System.out.println("nahageze4"+heure);
                        printStringUp = printStringUp + "\n    " + date1 + "   " + heure.substring(10) + "  #: " + employe;
                        // printStringUp = printStringUp + "\n    " + date1 + "   " + heure.substring(10) + "  #: " + employe;
                        
                        System.out.println("nahag");
                        printStringUp=printStringUp+"\n    -------------------------------------------   ";
                        printStringE=printStringE+ "\n  ";
                        printStringW=printStringW+ "\n   ";
                        printStringE=printStringE+ "\n  TOTAL en RWF: ";
                        printStringW=printStringW+ "\n"+totale;
                        printStringE=printStringE+ "\n  Tva : ";
                        printStringW=printStringW+ "\n  "+tva;
                        printStringE=printStringE+ "\n  "+client+" "+(100-percentage)+" % : ";
                        printStringW=printStringW+ "\n"+(totale- (int)(totale*percentage*0.01));
                        printStringE=printStringE+ "\n  ADHERENT  "+percentage+" % : ";
                        printStringW=printStringW+ "\n"+(int)(totale*percentage*0.01);
                        printStringE=printStringE+ "\n           Merci de votre visite."  ;
                        printStringW=printStringW+ "\n ";
                        System.out.println("nahageze5");
                    }}

//  Close the resultSet  and date ='"+d+"'

printR.add(printStringUp);
printR.add(printStringE);
printR.add(printStringW);
System.out.println("nahageze");
                    
//JOptionPane.showMessageDialog(null,"BL VENTES SANS DOWNLOADED PROBLEMES","MURAKOZE",JOptionPane.PLAIN_MESSAGE);

      }  catch (Throwable e)  {
   insertError(e+"","printRetard");
    }
        if(mut)
            printR=new LinkedList<String>();
  return printR;
}

String setVirgule(int frw)
{
    String setString = ""+frw;
    int l =setString.length();
    if(l<2)
        setString = "    "+frw;
    else if(l<3)
        setString = "  "+frw;
    else if(l<4)
        setString = "  "+frw;
String vir=".";
if(l>3 && l<=6)
    setString= setString.substring(0,l-3)+vir+setString.substring(l-3) ;
if(l>6)
{

String s1 = setString.substring(0,l-3);
int sl=s1.length();
setString=s1.substring(0,sl-3)+vir+s1.substring(sl-3)+vir+setString.substring(l-3)  ;

}

return setString;
}
public void  annule(int id_invoice,String nom,String RAIS)
{
    LinkedList <List> todelete=new LinkedList();

String printStringUp="    REBA NEZA KO IYI FACTURE ARI IYO GUSIBA ";
int totale=0;
boolean done=false;
boolean done1=false;
String printStringE="";
String printStringW="";

        String client="";
        String date1="";
        String code=  "";
        String lot1 ="";
        int quantite =0;
        double price =0;
        String desi ="";
        String   num_affiliation="";
        String  nom_client="";
        String  prenom_client="";
        int  percentage=0;
        String   numero_quitance  ="";

try
    {
            // Clients Cash
    outResult = s.executeQuery("" +
            "select app.invoice.id_invoice,num_client,date,code_uni,num_lot,quantite" +
            ",list.price,name_product from APP.PRODUCT,APP.INVOICE ,APP.LIST where " +
            "APP.INVOICE.NUM_CLIENT='CASHNORM' " +
            "and APP.INVOICE.id_invoice = "+id_invoice+"  " +
            "and APP.INVOICE.id_invoice = APP.LIST.id_invoice " +
            "and  APP.PRODUCT.CODE=APP.LIST.CODE_UNI order" +
            " by  app.invoice.id_invoice");
    
    System.out.println("" +
            "select app.invoice.id_invoice,num_client,date,code_uni,num_lot,quantite" +
            ",list.price,name_product from APP.PRODUCT,APP.INVOICE ,APP.LIST where " +
            "APP.INVOICE.NUM_CLIENT='CASHNORM' " +
            "and APP.INVOICE.id_invoice = "+id_invoice+"  " +
            "and APP.INVOICE.id_invoice = APP.LIST.id_invoice " +
            "and  APP.PRODUCT.CODE=APP.LIST.CODE_UNI order" +
            " by  app.invoice.id_invoice");

    while (outResult.next())
    {

         client=outResult.getString(2);
         date1=outResult.getString(3);
         code=  outResult.getString(4);
         lot1 =outResult.getString(5);
         quantite =outResult.getInt(6);
         price =outResult.getDouble(7);
         desi =outResult.getString(8);
 printStringE=printStringE+ "\n  "+quantite+ "  "+desi+ "    "+(int)(quantite*price);
printStringW=printStringW+"("+code+"-"+quantite+")";
todelete.add(new List(id_invoice,price,quantite,lot1,code));

      totale=totale+(int)(quantite*price);
             done=true;
     }
      outResult.close();
       // Clients Cash  Unipharma
     outResult = s.executeQuery("" +
             "select app.invoice.id_invoice,num_client,date,code_uni,num_lot," +
             "quantite,list.price,name_product,num_affiliation,nom_client," +
             "prenom_client,percentage,numero_quitance " +
             "from APP.PRODUCT,APP.INVOICE ,APP.LIST,APP.CREDIT,APP.CLIENT_UNIPHARMA " +
             "where APP.INVOICE.NUM_CLIENT ='CASHREMISE'  " +
             "and APP.credit.numero_affilie =  APP.client_UNIPHARMA.num_affiliation " +
             "and APP.INVOICE.id_invoice = "+id_invoice+" " +
             "and APP.INVOICE.id_invoice = APP.credit.id_invoice " +
             "and APP.INVOICE.id_invoice = APP.LIST.id_invoice " +
             "AND APP.PRODUCT.CODE=APP.LIST.CODE_UNI " +
             "order by app.invoice.id_invoice");

    while (outResult.next())
    {
         client=outResult.getString(2);
         date1=outResult.getString(3);
         code=  outResult.getString(4);
         lot1 =outResult.getString(5);
         quantite =outResult.getInt(6);
        price =outResult.getDouble(7);
        desi =outResult.getString(8);
         num_affiliation=outResult.getString(9);
          nom_client=outResult.getString(10);
          prenom_client=outResult.getString(11);
          percentage=outResult.getInt(12);
           numero_quitance  =outResult.getString(13);
printStringE=printStringE+ "\n  "+quantite+ "  "+desi+ "    "+(int)(quantite*price);
printStringW=printStringW+"("+code+"-"+quantite+")";
todelete.add(new List(id_invoice,price,quantite,lot1,code));
      totale=totale+(int)(quantite*price);
         done1=true;

     }

    //  Close the resultSet  and date ='"+d+"'
    outResult.close();

      // Clients Rama

     outResult = s.executeQuery("" +
             "select app.invoice.id_invoice,num_client,date,code_uni,num_lot,quantite,list.price," +
             "name_product,num_affiliation,nom_client,prenom_client,percentage,numero_quitance " +
             "from APP.PRODUCT,APP.INVOICE ,APP.LIST,APP.CREDIT,APP.CLIENT_RAMA " +
             "where APP.INVOICE.NUM_CLIENT ='RAMA'" +
             "and APP.client_RAMA.num_affiliation = APP.credit.numero_affilie " +
             "and APP.INVOICE.id_invoice = "+id_invoice+" " +
             "and APP.INVOICE.id_invoice = APP.credit.id_invoice " +
             "and APP.INVOICE.id_invoice = APP.LIST.id_invoice  " +
             "and APP.PRODUCT.CODE=APP.LIST.CODE_UNI " +
             "order by app.invoice.id_invoice");

    while (outResult.next())
    {
         client=outResult.getString(2);
         date1=outResult.getString(3);
         code=  outResult.getString(4);
         lot1 =outResult.getString(5);
         quantite =outResult.getInt(6);
          price =outResult.getDouble(7);
         desi =outResult.getString(8);
           num_affiliation=outResult.getString(9);
          nom_client=outResult.getString(10);
          prenom_client=outResult.getString(11);
          percentage=outResult.getInt(12);
           numero_quitance  =outResult.getString(13);
printStringE=printStringE+ "\n  "+quantite+ "  "+desi+ "    "+(int)(quantite*price);
printStringW=printStringW+"("+code+"-"+quantite+")";
todelete.add(new List(id_invoice,price,quantite,lot1,code));
       totale=totale+(int)(quantite*price);
              done1=true;
     }

    //  Close the resultSet  and date ='"+d+"'
    outResult.close();


     // Clients Societe

     outResult = s.executeQuery("" +
             "select app.invoice.id_invoice,num_client,date,code_uni,num_lot,quantite,list.price,name_product" +
             ",num_affiliation,nom_client,prenom_client,percentage,numero_quitance " +
             "from APP.PRODUCT,APP.INVOICE ,APP.LIST,APP.CREDIT,APP.CLIENT " +
             "where  APP.INVOICE.NUM_CLIENT !='CASHNORM'" +
             "and APP.INVOICE.NUM_CLIENT !='CASHREMISE'" +
             "and APP.INVOICE.NUM_CLIENT !='RAMA'" +
             "and APP.client.num_affiliation = APP.credit.numero_affilie " +
             "and APP.INVOICE.id_invoice = "+id_invoice+
             " and APP.INVOICE.id_invoice = APP.credit.id_invoice " +
             "and APP.INVOICE.id_invoice = APP.LIST.id_invoice  " +
             "AND APP.PRODUCT.CODE=APP.LIST.CODE_UNI" +
             " order by app.invoice.id_invoice");

    while (outResult.next())
    {

         client=outResult.getString(2);
         date1=outResult.getString(3);
         code=  outResult.getString(4);
         lot1 =outResult.getString(5);
         quantite =outResult.getInt(6);
         price =outResult.getDouble(7);
         desi =outResult.getString(8);
           num_affiliation=outResult.getString(9);
        nom_client=outResult.getString(10);
        prenom_client=outResult.getString(11);
          percentage=outResult.getInt(12);
         numero_quitance  =outResult.getString(13);
printStringE=printStringE+ "\n  "+quantite+ "  "+desi+ "    "+(int)(quantite*price);
printStringW=printStringW+"("+code+"-"+quantite+")";
todelete.add(new List(id_invoice,price,quantite,lot1,code));
    totale=totale+(int)(quantite*price);
        done1=true;


     }
     if(done)
   {
          printStringUp=printStringUp+ "\n    FACTURE N°    : " +id_invoice;
     printStringE=printStringE+ "\n  ";


printStringE=printStringE+ "\n  TOTAL en RWF: "+(int)totale;


     }

     else
         {


   printStringUp=printStringUp+ "\n    FACTURE N°    : " +id_invoice;
        printStringUp=printStringUp+ "\n    " +date1 ;
        printStringUp=printStringUp+"\n    -------------------------------------------   ";
        printStringUp=printStringUp+"\n    Assureur            : "+client;
        printStringUp=printStringUp+ "\n    N° Affiliation      : "+num_affiliation;
        printStringUp=printStringUp+ "\n    Nom et Prenom : "+nom_client+ " "+prenom_client;
     //   printStringUp=printStringUp+ "\n    Departement     : "+c.EMPLOYEUR;
        printStringUp=printStringUp+ "\n    N° Quittance     : "+numero_quitance;
        printStringUp=printStringUp+"\n    -------------------------------------------   ";





         printStringE=printStringE+ "\n  ";


printStringE=printStringE+ "\n  TOTAL en RWF: "+totale;


 printStringE=printStringE+ "\n  "+client+" "+(100-percentage)+" % : "+(totale- (int)(totale*percentage*0.01));

 printStringE=printStringE+ "\n  ADHERENT  "+percentage+" % : "+(int)(totale*percentage*0.01);



     }
    //  Close the resultSet  and date ='"+d+"'
    outResult.close();
    if(done ||done1)
    {


           int n = JOptionPane.showConfirmDialog(null,
                        printStringUp+
                        "\n"+printStringE,
                      "GUSIBA FACTURE ????? ",JOptionPane.YES_NO_OPTION);
                if(n==0)
                {

           Annule deleted=new Annule(id_invoice,num_affiliation,nom,client,printStringW,date1,RAIS);

      if(retourMse(todelete))
      {
            String rais  = (JOptionPane.showInputDialog(null, " Impamvu"));
          insertDeleted(deleted,rais);
          deleteInvoice(id_invoice);
         JOptionPane.showMessageDialog(null,"FACTURE N° "+id_invoice+" ANNULEE","",JOptionPane.PLAIN_MESSAGE);
       }
      else
       JOptionPane.showMessageDialog(null,"kwanira FACTURE N° "+id_invoice+"  biranze ","ERROR",JOptionPane.PLAIN_MESSAGE);

    }


                }else
    JOptionPane.showMessageDialog(null,"FACTURE N° "+id_invoice+" NTIBAHO","",JOptionPane.PLAIN_MESSAGE);


   //JOptionPane.showMessageDialog(null,"BL VENTES SANS DOWNLOADED PROBLEMES","MURAKOZE",JOptionPane.PLAIN_MESSAGE);

      }  catch (Throwable e)  {
   insertError(e+"","ANNULE");
    }

}
public int  retour(int id_invoice,int productcode,String nom)
{
    LinkedList <List> todelete=new LinkedList();

String printStringUp="    REBA NEZA KO IYI FACTURE ARIYO ya RETOUR  ";
int totale=0;
boolean done=false;
boolean done1=false;
String printStringE="";
String printStringW="";

        String client="";
        String date1="";
        String code=  "";
        String lot1 ="";
        int quantite =0;
        double price =0;
        String desi ="";
        String   num_affiliation="";
        String  nom_client="";
        String  prenom_client="";
        int  percentage=0;
        String   numero_quitance  ="";

try
    {
            // Clients Cash
    outResult = s.executeQuery("" +
            "select app.invoice.id_invoice,num_client,date,code_uni,num_lot,quantite" +
            ",list.price,name_product from APP.PRODUCT,APP.INVOICE ,APP.LIST where " +
            "APP.INVOICE.NUM_CLIENT='CASHNORM' " +
            "and APP.INVOICE.id_invoice = "+id_invoice+"  " +
            "and APP.INVOICE.id_invoice = APP.LIST.id_invoice " +
            "and  APP.PRODUCT.CODE=APP.LIST.CODE_UNI " +
            "AND APP.PRODUCT.ID_PRODUCT ="+productcode+"" +
            " order by  app.invoice.id_invoice");

    while (outResult.next())
    {

         client=outResult.getString(2);
         date1=outResult.getString(3);
         code=  outResult.getString(4);
         lot1 =outResult.getString(5);
         quantite =outResult.getInt(6);
         price =outResult.getDouble(7);
         desi =outResult.getString(8);
 printStringE=printStringE+ "\n  "+quantite+ "  "+desi+ "    "+(int)(quantite*price);
printStringW=printStringW+"("+code+"-"+quantite+")";
todelete.add(new List(id_invoice,price,quantite,lot1,code));

      totale=totale+(int)(quantite*price);
             done=true;
     }
      outResult.close();

       // Clients Cash  Unipharma
     outResult = s.executeQuery("" +
             "select app.invoice.id_invoice,num_client,date,code_uni,num_lot," +
             "quantite,list.price,name_product,num_affiliation,nom_client," +
             "prenom_client,percentage,numero_quitance " +
             "from APP.PRODUCT,APP.INVOICE ,APP.LIST,APP.CREDIT,APP.CLIENT_UNIPHARMA " +
             "where APP.INVOICE.NUM_CLIENT ='CASHREMISE'  " +
             "and APP.credit.numero_affilie =  APP.client_UNIPHARMA.num_affiliation " +
             "and APP.INVOICE.id_invoice = "+id_invoice+" " +
             "and APP.INVOICE.id_invoice = APP.credit.id_invoice " +
             "and APP.INVOICE.id_invoice = APP.LIST.id_invoice " +
             "AND APP.PRODUCT.CODE=APP.LIST.CODE_UNI " +
              "AND APP.PRODUCT.ID_PRODUCT="+productcode+"" +
             "order by app.invoice.id_invoice");

    while (outResult.next())
    {
         client=outResult.getString(2);
         date1=outResult.getString(3);
         code=  outResult.getString(4);
         lot1 =outResult.getString(5);
         quantite =outResult.getInt(6);
        price =outResult.getDouble(7);
        desi =outResult.getString(8);
         num_affiliation=outResult.getString(9);
          nom_client=outResult.getString(10);
          prenom_client=outResult.getString(11);
          percentage=outResult.getInt(12);
           numero_quitance  =outResult.getString(13);
printStringE=printStringE+ "\n  "+quantite+ "  "+desi+ "    "+(int)(quantite*price);
printStringW=printStringW+"("+code+"-"+quantite+")";
todelete.add(new List(id_invoice,price,quantite,lot1,code));
      totale=totale+(int)(quantite*price);
         done1=true;

     }

    //  Close the resultSet  and date ='"+d+"'
    outResult.close();
      // Clients Rama

     outResult = s.executeQuery("" +
             "select app.invoice.id_invoice,num_client,date,code_uni,num_lot,quantite,list.price," +
             "name_product,num_affiliation,nom_client,prenom_client,percentage,numero_quitance " +
             "from APP.PRODUCT,APP.INVOICE ,APP.LIST,APP.CREDIT,APP.CLIENT_RAMA " +
             "where APP.INVOICE.NUM_CLIENT ='RAMA'" +
             "and APP.client_RAMA.num_affiliation = APP.credit.numero_affilie " +
             "and APP.INVOICE.id_invoice = "+id_invoice+" " +
             "and APP.INVOICE.id_invoice = APP.credit.id_invoice " +
             "and APP.INVOICE.id_invoice = APP.LIST.id_invoice  " +
             "and APP.PRODUCT.CODE=APP.LIST.CODE_UNI " +
              "AND APP.PRODUCT.ID_PRODUCT="+productcode+"" +
             "order by app.invoice.id_invoice");

    while (outResult.next())
    {
         client=outResult.getString(2);
         date1=outResult.getString(3);
         code=  outResult.getString(4);
         lot1 =outResult.getString(5);
         quantite =outResult.getInt(6);
          price =outResult.getDouble(7);
         desi =outResult.getString(8);
           num_affiliation=outResult.getString(9);
          nom_client=outResult.getString(10);
          prenom_client=outResult.getString(11);
          percentage=outResult.getInt(12);
           numero_quitance  =outResult.getString(13);
printStringE=printStringE+ "\n  "+quantite+ "  "+desi+ "    "+(int)(quantite*price);
printStringW=printStringW+"("+code+"-"+quantite+")";
todelete.add(new List(id_invoice,price,quantite,lot1,code));
       totale=totale+(int)(quantite*price);
              done1=true;
     }

    //  Close the resultSet  and date ='"+d+"'
    outResult.close();


     // Clients Societe
      outResult = s.executeQuery("" +
             "select app.invoice.id_invoice,num_client,date,code_uni,num_lot,quantite,list.price,name_product" +
             ",num_affiliation,nom_client,prenom_client,percentage,numero_quitance " +
             "from APP.PRODUCT,APP.INVOICE ,APP.LIST,APP.CREDIT,APP.CLIENT " +
             "where  APP.INVOICE.NUM_CLIENT !='CASHNORM' " +
             "and APP.INVOICE.NUM_CLIENT !='CASHREMISE' " +
             "and APP.INVOICE.NUM_CLIENT !='RAMA'"  +
             "and APP.client.num_affiliation = APP.credit.numero_affilie " +
             "and APP.INVOICE.id_invoice = "+id_invoice+
             " and APP.INVOICE.id_invoice = APP.credit.id_invoice " +
             " and APP.INVOICE.id_invoice = APP.LIST.id_invoice  " +
             " AND APP.PRODUCT.CODE=APP.LIST.CODE_UNI " +
             " AND APP.PRODUCT.ID_PRODUCT="+productcode+" " +
             " order by app.invoice.id_invoice");

    while (outResult.next())
    {

         client=outResult.getString(2);
         date1=outResult.getString(3);
         code=  outResult.getString(4);
         lot1 =outResult.getString(5);
         quantite =outResult.getInt(6);
         price =outResult.getDouble(7);
         desi =outResult.getString(8);
           num_affiliation=outResult.getString(9);
        nom_client=outResult.getString(10);
        prenom_client=outResult.getString(11);
          percentage=outResult.getInt(12);
         numero_quitance  =outResult.getString(13);
printStringE=printStringE+ "\n  "+quantite+ "  "+desi+ "    "+(int)(quantite*price);
printStringW=printStringW+"("+code+"-"+quantite+")";
todelete.add(new List(id_invoice,price,quantite,lot1,code));
    totale=totale+(int)(quantite*price);
        done1=true;


     }
     if(done)
   {
          printStringUp=printStringUp+ "\n    FACTURE N°    : " +id_invoice;
     printStringE=printStringE+ "\n  ";


printStringE=printStringE+ "\n  TOTAL en RWF: "+(int)totale;


     }

     else
         {


   printStringUp=printStringUp+ "\n    FACTURE N°    : " +id_invoice;
        printStringUp=printStringUp+ "\n    " +date1 ;
        printStringUp=printStringUp+"\n    -------------------------------------------   ";
        printStringUp=printStringUp+"\n    Assureur            : "+client;
        printStringUp=printStringUp+ "\n    N° Affiliation      : "+num_affiliation;
        printStringUp=printStringUp+ "\n    Nom et Prenom : "+nom_client+ " "+prenom_client;
     //   printStringUp=printStringUp+ "\n    Departement     : "+c.EMPLOYEUR;
        printStringUp=printStringUp+ "\n    N° Quittance     : "+numero_quitance;
        printStringUp=printStringUp+"\n    -------------------------------------------   ";





         printStringE=printStringE+ "\n  ";


printStringE=printStringE+ "\n  TOTAL en RWF: "+totale;


 printStringE=printStringE+ "\n  "+client+" "+(100-percentage)+" % : "+(totale- (int)(totale*percentage*0.01));

 printStringE=printStringE+ "\n  ADHERENT  "+percentage+" % : "+(int)(totale*percentage*0.01);
totale=(totale);


     }
    //  Close the resultSet  and date ='"+d+"'
    outResult.close();

     if(done ||done1)
    {


           int n = JOptionPane.showConfirmDialog(null,
                        printStringUp+
                        "\n "+printStringE+
                        "\n MONTANT RETOUR "+totale,
                      "RETOUR ????? ",JOptionPane.YES_NO_OPTION);
         //  JOptionPane.showMessageDialog(null,"FACTURE N° "+n+" ANNULEE","",JOptionPane.PLAIN_MESSAGE);
if(n==0)
{

    Annule deleted=new Annule(id_invoice,num_affiliation,nom+" R",client,printStringW,date1,"RETOUR");

    if(retourMse(todelete))
    {
    String rais  = (JOptionPane.showInputDialog(null, " Impamvu"));
          insertDeleted(deleted,rais);
   // JOptionPane.showMessageDialog(null,"FACTURE N° "+id_invoice+" ANNULEE","",JOptionPane.PLAIN_MESSAGE);
    }
}
else
    totale=0;


                }else
    JOptionPane.showMessageDialog(null,"FACTURE N° "+id_invoice+" NTIBAHO","",JOptionPane.PLAIN_MESSAGE);

    }  catch (Throwable e)  {
   insertError(e+"","retour ");
    }
return totale;
}
public boolean retourMse(LinkedList <List> todelete)
{
    boolean done=true;
    for(int i=0;i< todelete.size();i++)
    {
            try {
                List l = todelete.get(i);

                if(getLot(l))
                {
                    Statement sD = conn.createStatement();
                     String createString1 = " delete from list where id_invoice=" + l.id_invoice + " and code_uni='" + l.code + "'";
                      sD.execute(createString1);

                }
                else{
                  done=false;
                        JOptionPane.showMessageDialog(null,"kwanira FACTURE N° "+l.id_invoice+" umuti wa "+l.code+" biranze ","Ikibazo",JOptionPane.PLAIN_MESSAGE);


             break;
                }






            }  catch (Throwable e)  {
   insertError(e+"","retourMse");
    }

    }
   return done;
}
public boolean  getLot(List l)
{
    boolean done=false;
          try
    {

         Lot lotToUpdate=null;
    outResult = s.executeQuery("" +
            "select * from APP.LOT,APP.PRODUCT" +
            " where code='"+l.code+"' and " +
            "APP.LOT.ID_PRODUCT=APP.PRODUCT.ID_PRODUCT " +
            "and id_LotS ='"+l.lot+"' ");


     while (outResult.next())
    {

    lotToUpdate=new Lot( outResult.getInt(1),"",outResult.getString(10),outResult.getString(3),"",outResult.getInt(2),0,0, outResult.getString(7));
          done=true;    break;

     }
    outResult.close();
    if(lotToUpdate!=null)
    {
s = conn.createStatement();
String createString1 = "" +
        "update APP.LOT set QTY_LIVE=QTY_LIVE+" + l.qty + "" +
        "where id_product="+lotToUpdate.productCode+" " +
        "and ID_LOTS= '" + lotToUpdate.id_LotS + "'" +
        "and BON_LIVRAISON= '" + lotToUpdate.bon_livraison + "'" +
        "and ID_LOT=" + lotToUpdate.id_lot;
s.execute(createString1);

    }
    else
     {
    outResult = s.executeQuery("" +
            "select * from APP.USED_LOT,APP.PRODUCT" +
            " where code='"+l.code+"' and " +
            "APP.USED_LOT.ID_PRODUCT=APP.PRODUCT.ID_PRODUCT " +
            "and id_LotS ='"+l.lot+"' ");
       while (outResult.next())
    {

   lotToUpdate=new Lot( outResult.getInt(1),"",outResult.getString(10),outResult.getString(3),outResult.getString(4),outResult.getInt(2),outResult.getInt(5),outResult.getInt(6), outResult.getString(7));
            done=true;   break;

     }
    outResult.close();

     if(lotToUpdate!=null)
    {
          psInsert = conn.prepareStatement("insert into APP.LOT(ID_PRODUCT,ID_LOT,ID_LOTS,DATE_EXP,QTY_START,QTY_LIVE,BON_LIVRAISON)  values (?,?,?,?,?,?,?)");

        psInsert.setInt(1,lotToUpdate.productCode);
        psInsert.setInt(2,lotToUpdate.id_lot);
        psInsert.setString(3,lotToUpdate.id_LotS);
        psInsert.setString(4,lotToUpdate.dateExp);
        psInsert.setInt(5,lotToUpdate.qtyStart);
        psInsert.setInt(6,l.qty);
        psInsert.setString(7,lotToUpdate.bon_livraison);
        psInsert.executeUpdate();
        psInsert.close();

s = conn.createStatement();
 String createString1 = " " +
         "delete from USED_LOT where ID_LOT="+lotToUpdate.id_lot+" " +
         "and BON_LIVRAISON='"+lotToUpdate.bon_livraison +"' " +
         "and id_LotS= '"+lotToUpdate.id_LotS+"' " +
          "and DATE_EXP= '"+lotToUpdate.dateExp+"' " +
         "and ID_PRODUCT= "+lotToUpdate.productCode;

s.execute(createString1);

    }


    }

      }  catch (Throwable e)  {
         insertError(e+""," get lot");
    }
    return done;
}


  public void insertDeleted(Annule d,String raison)
  {
 try
    {
psInsert = conn.prepareStatement("insert into APP.ANNULE(ID_INVOICE,DATE,NUM_CLIENT ,NUMERO_AFFILIE ,EMPLOYE ,CODE_QTE,RAISON )  values (?,?,?,?,?,?,?)");

    psInsert.setInt(1,d.ID_INVOICE);
    psInsert.setString(2,d.DATE);
    psInsert.setString(3,d.NUM_CLIENT);
    psInsert.setString(4,d.NUMERO_AFFILIE);
    psInsert.setString(5,d.EMPLOYE);
    psInsert.setString(6,d.CODE_QTE);
    psInsert.setString(7,raison);
        psInsert.executeUpdate();
        psInsert.close();
   }  catch (Throwable e)  { insertError(e+"","insertDeleted"); }
  }

public void  deleteInvoice(int id_invoice)
{
        try {
            Statement sD = conn.createStatement();
            String createString1 = " delete from invoice where id_invoice=" + id_invoice;
            sD.execute(createString1);
            createString1 = " delete from credit where id_invoice=" + id_invoice;
            sD.execute(createString1);

        } catch (Throwable e)  { insertError(e+"","deleteInvoice"); }
}


public void  bcpTxt()throws FileNotFoundException, IOException
{




                   String  file="BL_VENTES/"+(new Date().getMonth()+1)+"/"+datez+".txt";
         //   JOptionPane.showMessageDialog(null,file,"Downloading Sales",JOptionPane.PLAIN_MESSAGE);


          File f = new File(file);

        PrintWriter sorti=new PrintWriter(new FileWriter(f));

    try
    {
            // Clients Cash
    outResult = s.executeQuery("" +
"select app.invoice.id_invoice,num_client,date,code_uni,num_lot,quantite" +
            ",list.price,name_product from APP.PRODUCT,APP.INVOICE ,APP.LIST where " +
            "APP.INVOICE.id_invoice = APP.LIST.id_invoice " +
             "and APP.INVOICE.date = '"+datez+"' " +
            "AND APP.PRODUCT.CODE=APP.LIST.CODE_UNI order" +
            " by app.invoice.id_invoice");

    while (outResult.next())
    {

        int  id_invoice=  outResult.getInt(1);
        String client=outResult.getString(2);
        String date1=outResult.getString(3);
        String code=  outResult.getString(4);
        String lot1 =outResult.getString(5);
        int quantite =outResult.getInt(6);
        double price =outResult.getDouble(7);
        String desi =outResult.getString(8);


        String factureSage="13;BL"+id_invoice+";"+client+";"+ date1 +";"+code+";"+desi+";"+lot1+";"+quantite+";"+price;
        sorti.println(factureSage);
       //  JOptionPane.showMessageDialog(null,factureSage,"Downloaded",JOptionPane.PLAIN_MESSAGE);

     }
      outResult.close();

      }  catch (Throwable e)  {
   insertError(e+"","bcupText");
    }
    sorti.close();
}

public void closeConnection()
{
try
{
//  JOptionPane.showMessageDialog(null,ip," ip1 ",JOptionPane.PLAIN_MESSAGE);

if(ip.equals("elephant/192.168.0.113"))
      bcpTxt();

    conn.close();

    if (driver.equals("org.apache.derby.jdbc.EmbeddedDriver")) {
    boolean gotSQLExc = false;
    try {
    DriverManager.getConnection("jdbc:derby:;shutdown=true");
    } catch (SQLException se)  {
    if ( se.getSQLState().equals("XJ015") ) {
    gotSQLExc = true;
    }
    }
    if (!gotSQLExc) {
    System.out.println("Database did not shut down normally");
    }  else  {
    System.out.println("Database shut down normally");
    }
   }
}
 catch (Throwable e)  {}
}

public void upDateStatus(int room_number,String c,String statut,String checkin,String checkout){
  //System.out.println(" room" +room_number + " num client"+c.NUM_AFFILIATION) ; 
  String num_client = c;
    
try {
    s = conn.createStatement();
    String createString1 = " update APP.RESERVATION set STATUT ='"+statut+"' where RESERVATION.NUM_CLIENT = '"+num_client+"' AND RESERVATION.NUM_ROOM = "+room_number+" ";
    s.execute(createString1);

    }
    catch (Throwable e){
          JOptionPane.showMessageDialog(null,e,"LE STATUT N ESP MODIFIE ",JOptionPane.PLAIN_MESSAGE);

    }
     if ( statut.matches("OCCUPIED") ){
         try{
        Statement sD = conn.createStatement();
            String createString1 = "update APP.RESERVATION  set ETAT = 'PASSIF'  WHERE RESERVATION.NUM_ROOM ="+room_number+" AND ETAT = 'ACTIF'"
            + "((RESERVATION.CHECKIN <= '"+checkin +"'AND RESERVATION.CHECKOUT >= '"+checkout+"') OR "
            + " (RESERVATION.CHECKIN >= '"+checkin +"'AND RESERVATION.CHECKOUT <= '"+checkout+"') OR "   
            + " (RESERVATION.CHECKIN >= '"+checkin +"' AND RESERVATION.CHECKIN <= '"+checkout+"' AND RESERVATION.CHECKOUT >= '"+checkout+"')"
            + "OR (RESERVATION.CHECKOUT <= '"+checkout+"' AND RESERVATION.CHECKOUT >= '"+checkin+"' AND RESERVATION.CHECKIN <='"+checkin+"'))";

          sD.execute(createString1);
        
     }catch (Throwable e){
         JOptionPane.showMessageDialog(null,e,"LE STATUT N ESP MODIFIE ",JOptionPane.PLAIN_MESSAGE);
     }
     }
     if ( statut.matches("FREE")|| statut.matches("NON DISPO") ){
         try{
        Statement sD = conn.createStatement();
            String createString1 = "update APP.RESERVATION  set ETAT = 'PASSIF' WHERE RESERVATION.NUM_ROOM ="+room_number+" AND ETAT = 'ACTIF'"
           
            
            + "AND ((RESERVATION.CHECKIN >= '"+checkin +"'AND RESERVATION.CHECKOUT <= '"+checkout+"')"
            + "OR (RESERVATION.CHECKIN >= '"+checkin +"' AND RESERVATION.CHECKIN <= '"+checkout+"' AND RESERVATION.CHECKOUT >= '"+checkout+"')"
            + "OR (RESERVATION.CHECKOUT <= '"+checkout+"' AND RESERVATION.CHECKIN >= '"+checkin+"' AND RESERVATION.CHECKIN <='"+checkout+"'))";
            sD.execute(createString1);
        
     }catch (Throwable e){
         JOptionPane.showMessageDialog(null,e,"LE STATUT N ESP MODIFIE ",JOptionPane.PLAIN_MESSAGE);
     }
     }
    
}
public void upDateCodeBar(String codebar,int code)
{
    try {
    s = conn.createStatement();
    String createString1 = " update APP.PRODUCT set CODE_BAR='"+codebar+"' where ID_PRODUCT="+code+"";
    s.execute(createString1);

    }
    catch (Throwable e){
          JOptionPane.showMessageDialog(null,e,"code bar existant ",JOptionPane.PLAIN_MESSAGE);

    }

}
 void addOneLot(int p,Lot neW)
{


    try
    {
psInsert = conn.prepareStatement("insert into APP.LOT(ID_PRODUCT,ID_LOT,ID_LOTS,DATE_EXP,QTY_START,QTY_LIVE,BON_LIVRAISON)  values (?,?,?,?,?,?,?)");

        psInsert.setInt(1,p);
        psInsert.setInt(2,neW.id_lot);
        psInsert.setString(3,neW.id_LotS);
        psInsert.setString(4,neW.dateExp);
        psInsert.setInt(5,neW.qtyStart);
        psInsert.setInt(6,neW.qtyLive);
        psInsert.setString(7,neW.bon_livraison);
        psInsert.executeUpdate();
        psInsert.close();
   }  catch (Throwable e)  { insertError(e+"","addonecodeBar"); }


}

 void addOneCode()
{

   doworkOther("");
    try
    {
        for(int i=0;i<allProduct.size();i++)
        {
    psInsert = conn.prepareStatement("insert into CODEBAR(CODE_MACHINE,CODE_BAR)  values (?,?)");
    psInsert.setInt(1,i);
    psInsert.setString(2,"c"+i);

    psInsert.executeUpdate();
        }
    // Release the resources (clean up )
    psInsert.close();
    }  catch (Throwable e)  { insertError(e+"","addonecodeBar"); }


}

Variable getParama(String famille , String nom)
{
  /// System.out.println("select  * from APP.VARIABLES where APP.variables.famille_variable= '"+nom+"' and APP.variables.value2_variable= and APP.variables.famille_variable= '"+famille+"' ");

   Variable par = null;
try
{
    System.out.println("NOM VAR:"+nom+" FAMILLE:"+famille);
outResult = s.executeQuery("select  * from APP.VARIABLES where APP.variables.nom_variable= '"+nom+"'  and APP.variables.famille_variable= '"+famille+"' ");

while (outResult.next())
{
  //  System.out.println(outResult.getString(2));
        String num =outResult.getString(1);
        String value =outResult.getString(2);
        String fam =outResult.getString(3);
        String value2 =outResult.getString(4);
        par=(new Variable(num,value,fam,value2));
}}
catch (Throwable e){
      insertError(e+"","getparm autoout");
 }
//System.out.println("PAR:"+par.value);
return     par;
}
void updateParameter1(LinkedList<Variable> par)
{
    for(int i=0;i<par.size();i++)
  {

   Variable v= par.get(i);
  //System.out.println(" update APP.VARIABLES set value_variable ='" + v.value + "',value2_variable ='" + v.value2 + "' where nom_variable='" + v.nom + "' AND famille_variable='" + v.famille + "'");
            try {

                s.execute("update APP.VARIABLES set value_variable ='" + v.value + "'  where nom_variable='" + v.nom+v.value2 + "' AND famille_variable='" + v.famille + "'");
            } catch (SQLException ex) {
                insertError(ex+"","updateparam"+v.nom);
            }    }
}
void updateParameter2(LinkedList<Variable> par,String soc)
{
    for(int i=0;i<par.size();i++)
  {

   Variable v= par.get(i);
 // System.out.println(" update APP.VARIABLES set value_variable ='" + v.value + "',value2_variable ='" + v.value2 + "' where nom_variable='" + v.nom+soc + "' AND famille_variable='" + v.famille + "'");
            try {

                s.execute(  " update APP.VARIABLES set value_variable ='" + v.value + "'  where nom_variable='" + v.nom+soc + "' AND famille_variable='" + v.famille + "'");
            } catch (SQLException ex) {
                insertError(ex+"","updateparam"+v.nom);
            }    }
}
 Variable getParam(String famille,String mode, String nom)
{
  // System.out.println("select  * from APP.VARIABLES where APP.variables.nom_variable= '"+nom+"'   and APP.variables.famille_variable= '"+famille+"' ");
    Variable par = null;
try
{
    
                System.out.println("nom variable:"+nom+"famille"+famille);
outResult = s.executeQuery("select  * from APP.VARIABLES where APP.variables.nom_variable= '"+nom+"'  and APP.variables.famille_variable= '"+famille+"' ");

while (outResult.next())
{
  //  System.out.println(outResult.getString(2));
        String num =outResult.getString(1);
        String value =outResult.getString(2);
        String fam =outResult.getString(3);
        String value2 =outResult.getString(4);
        par=(new Variable(num,value,fam,value2));
}}
catch (Throwable e){
      insertError(e+"","getparm autoout");
 }
return     par;
}

 LinkedList<Variable>getParam(String famille)
{
    LinkedList<Variable> par = new LinkedList ();
try
{
outResult = s.executeQuery("select  * from APP.VARIABLES where APP.variables.famille_variable= '"+famille+"'");

while (outResult.next())
{
//    System.out.println(outResult.getString(2));
        String num =outResult.getString(1);
        String value =outResult.getString(2);
        String fam =outResult.getString(3);
        par.add(new Variable(num,value,fam));
}}
catch (Throwable e){
      insertError(e+"","getparm autoout");
 }
return     par;
}

Tarif [] getTarif()
{
 Tarif [] tar=new Tarif[10];
try
{
outResult = s.executeQuery("select  * from APP.TARIF ");
int i=0;
while (outResult.next() && i<10)
{

String ID_TARIF=outResult.getString("ID_TARIF");   
String DES_TARIF=outResult.getString("DES_TARIF"); 
String FONT_TARIF =outResult.getString("FONT_TARIF");
String VAR_TARIF =outResult.getString("VAR_TARIF");
double COEF_TARIF =outResult.getDouble("COEF_TARIF"); 
int b=outResult.getInt("B_TARIF");
int g=outResult.getInt("G_TARIF");
int r=outResult.getInt("R_TARIF");  

tar[i]=new  Tarif( ID_TARIF, VAR_TARIF, DES_TARIF, COEF_TARIF, FONT_TARIF, r, g,  b);
 i++;
}}
catch (Throwable e){
      insertError(e+"","getparm autoout");
 }
return     tar;
}
void updateParameter(LinkedList<Variable> par)
{

    for(int i=0;i<par.size();i++)
  {

   Variable v= par.get(i);
   //System.out.println(" update APP.VARIABLES set value_variable ='" + v.value + "',value2_variable ='" + v.value2 + "' where nom_variable='" + v.nom + "' AND famille_variable='" + v.famille + "'");
            try {

                s.execute(  " update APP.VARIABLES set value_variable ='" + v.value + "'  where nom_variable='" + v.nom+v.value2 + "' AND famille_variable='" + v.famille + "'");
            } catch (SQLException ex) {
                insertError(ex+"","updateparam"+v.nom);
            }    }
}

void initializeRooms()
{
 lesRooms= new   Rooms[37];
for(int i=0;i<37;i++)
{
 lesRooms[i] = new  Rooms ( 1, "BEDS" , "STATUT" ,0 ,1,1,1,
        1,1, "CATEGORIE" ,"ROOM_VIEW" ,1,1,1,0,0);
   
}
}
Rooms []  initializeRooms2()
{
Rooms [] lesRoomsz= new   Rooms[37];
for(int i=0;i<37;i++)
{
 lesRoomsz[i] = new  Rooms ( -1, "BEDS" , "STATUT" ,0 ,1,1,1,
        1,1, "CATEGORIE" ,"ROOM_VIEW" ,1,1,1,0,0);
   
}
return lesRoomsz;
}
////////////////////////////////////////////////////////////////////////reservations/////////////
public  Rooms [] getRooms2(int view,String price){
initializeRooms();
int i = 0; 
try
{
System.out.println("select * from room where view_room="+view);
outResult = s.executeQuery("select * from room where view_room="+view);
System.out.println("DOWN select * from room where view_room="+view);
while (outResult.next())
{  
lesRooms[i] = new  Rooms ( outResult.getInt("NUMBER_ROMM"),outResult.getString("BEDS"),outResult.getString("STATUT")
        ,outResult.getDouble(""+price),outResult.getInt("TV"),outResult.getInt("PHONE"),outResult.getInt("DOUCHE"),
        outResult.getInt("BAIGNOIR"),outResult.getInt("FREDGE"),outResult.getString("CATEGORIE"),
        outResult.getString("ROOM_VIEW"),outResult.getInt("ID"),outResult.getInt("VIEW_ROOM"),
        outResult.getInt("ID_PRODUCT_ROOM"),outResult.getDouble("PRIX_DOUBLE"),outResult.getDouble("PRIX_GROUPE"));
i++;
//System.out.println(outResult.getInt("NUMBER_ROMM")+"select  ="+i);
}
}catch (Throwable e)  {
insertError(e+""," openPTr2");
}

System.out.println( "s      size elect  ="+lesRooms.length);

return lesRooms; 
} 
public  Rooms [] getRoomsCat(String cat,String price){
Rooms [] lesRooms2= initializeRooms2();
int i = 0; 
try
{
System.out.println("select * from room where CATEGORIE='"+cat+"'" );
outResult = s.executeQuery("select * from room where CATEGORIE='"+cat+"'" ); 

while (outResult.next())
{  
lesRooms2[outResult.getInt("NUMBER_ROMM")-1] = new  Rooms ( outResult.getInt("NUMBER_ROMM"),outResult.getString("BEDS"),outResult.getString("STATUT")
        ,outResult.getDouble("PRICE"),outResult.getInt("TV"),outResult.getInt("PHONE"),outResult.getInt("DOUCHE"),
        outResult.getInt("BAIGNOIR"),outResult.getInt("FREDGE"),outResult.getString("CATEGORIE"),
        outResult.getString("ROOM_VIEW"),outResult.getInt("ID"),outResult.getInt("VIEW_ROOM"),
        outResult.getInt("ID_PRODUCT_ROOM"),outResult.getDouble("PRIX_DOUBLE"),outResult.getDouble("PRIX_GROUPE"));
i++;
//System.out.println(outResult.getInt("NUMBER_ROMM")+"select  ="+i);
}
}catch (Throwable e)  {
insertError(e+""," openPTr2");
}

System.out.println( "s      size elect  ="+lesRooms.length);

return lesRooms2; 
} 
public static Rooms [] getRoomsSTATIC(int view,Statement st,Connection conn){
Rooms [] lesRooms= new   Rooms[37];
int i = 0; 
try
{
System.out.println("select * from room where view_room="+view);
ResultSet outResult = st.executeQuery("select * from room where view_room="+view);
while (outResult.next())
{  
lesRooms[i] = new  Rooms ( outResult.getInt("NUMBER_ROMM"),outResult.getString("BEDS"),outResult.getString("STATUT"),
        outResult.getDouble("PRICE"),outResult.getInt("TV"),outResult.getInt("PHONE"),outResult.getInt("DOUCHE"),
        outResult.getInt("BAIGNOIR"),outResult.getInt("FREDGE"),outResult.getString("CATEGORIE"),
        outResult.getString("ROOM_VIEW"),outResult.getInt("ID"),outResult.getInt("VIEW_ROOM"),
        outResult.getInt("ID_PRODUCT_ROOM"),outResult.getDouble("PRIX_DOUBLE"),outResult.getDouble("PRIX_GROUPE"));
i++;

}
}catch (Throwable e)  {
insertError(e+""," CHAMBRE INNACCESSIBLE",conn);
} 
return lesRooms; 
} 
 public  int getMaxView(){ 
try
{
//System.out.println("select * from ROOM   ");
outResult = s.executeQuery("select max(view_room)  from room  ");
while (outResult.next())
{  
return outResult.getInt(1);
}
}catch (Throwable e)  {
insertError(e+""," min");
} 
return 1; 
} 
  public  int getMinView(){ 
try
{
//System.out.println("select * from ROOM   ");
outResult = s.executeQuery("select min(view_room)  from room  ");
while (outResult.next())
{  
return outResult.getInt(1);
}
}catch (Throwable e)  {
insertError(e+""," min");
} 
return 1; 
} 
public static Rooms  getRoom(int num_room,Connection con){
try
    {
      Statement st=  con.createStatement();
    //System.err.println("select * from ROOM where number_romm="+num_room);
   ResultSet outResult1 = st.executeQuery("select * from room  where number_romm="+num_room);
    while (outResult1.next())
    {  
 return new  Rooms (outResult1.getInt("NUMBER_ROMM"),outResult1.getString("BEDS"),outResult1.getString("STATUT"),
         outResult1.getDouble("PRICE"),outResult1.getInt("TV"),outResult1.getInt("PHONE"),outResult1.getInt("DOUCHE"),
         outResult1.getInt("BAIGNOIR"),outResult1.getInt("FREDGE"), outResult1.getString("CATEGORIE"),
         outResult1.getString("ROOM_VIEW"),outResult1.getInt("ID"),outResult1.getInt("VIEW_ROOM"),
         outResult1.getInt("ID_PRODUCT_ROOM"),outResult1.getDouble("PRIX_DOUBLE"),outResult1.getDouble("PRIX_GROUPE"));
 
    }
    }  catch (Throwable e)  {
    insertError(e+""," room",con);
    } 
    return null; 
} 
  
public static LinkedList <UneReservation> getReservationDunClient
        ( String checkin, String checkout,String numClient,Statement st,Connection con)

{
    LinkedList <UneReservation>reservations = new LinkedList<UneReservation>();
    UneReservation reserv;

    try
    { 
 ResultSet outResult = st.executeQuery("  select * from APP.RESERVATION WHERE  "
         + " RESERVATION.NUM_CLIENT ='"+numClient+"' and "
            + "((RESERVATION.CHECKIN <= '"+checkin +"'AND RESERVATION.CHECKOUT >= '"+checkout+"') OR "
            + " (RESERVATION.CHECKIN >= '"+checkin +"'AND RESERVATION.CHECKOUT <= '"+checkout+"') OR "   
            + " (RESERVATION.CHECKIN >= '"+checkin +"' AND RESERVATION.CHECKIN <= '"+checkout+"' AND RESERVATION.CHECKOUT >= '"+checkout+"')"
            + "OR (RESERVATION.CHECKOUT <= '"+checkout+"' AND RESERVATION.CHECKOUT >= '"+checkin+"' AND RESERVATION.CHECKIN <='"+checkin+"')) AND ETAT = 'ACTIF' AND STATUT != 'FREE' order by num_room");

// System.out.println("  select * from APP.RESERVATION WHERE "
//         + "RESERVATION.NUM_CLIENT ='"+numClient+"'");
    while (outResult.next())
    {   
   reserv = new UneReservation (outResult.getInt(1),getRoom(outResult.getInt("NUM_ROOM"),con),
getClient(outResult.getString("NUM_CLIENT"),con),outResult.getString("STATUT"),
outResult.getString("DATE_RESERVATION"), outResult.getString("CHECKIN"),  
outResult.getString("CHECKOUT"), outResult.getDouble("AMOUNT"), outResult.getDouble("DEPOSIT")); 
     reservations.add(reserv);
    }
    }  catch (Throwable e)  {
    insertError(e+""," openPTr3",con);
    }  
    return reservations;
}

public static LinkedList<UneReservation> getReservations(Statement st,Connection con){
    LinkedList <UneReservation>reservations = new LinkedList<UneReservation>();
    UneReservation reserv;

    try
    {//System.out.println("AAAA"+"select * from RESERVATION");
  ResultSet  outResult = st.executeQuery("  select * from APP.RESERVATION");
    while (outResult.next())
    {   
 reserv = new UneReservation (outResult.getInt(1),getRoom(outResult.getInt("NUM_ROOM"),con),
getClient(outResult.getString("NUM_CLIENT"),con),outResult.getString("STATUT"),
outResult.getString("DATE_RESERVATION"), outResult.getString("CHECKIN"),  
outResult.getString("CHECKOUT"), outResult.getDouble("AMOUNT"), outResult.getDouble("DEPOSIT"));      
     reservations.add(reserv);
    }
    }  catch (Throwable e)  {
    insertError(e+""," openPTr4",con);
    } 
    return reservations;
} 

public static UneReservation [] getReservationsTime
        (String checkin, String checkout,int view,Statement st,Connection con)
{ 
UneReservation [] status=new UneReservation [36];
     try
    {      
//        System.out.println("  select * from APP.RESERVATION WHERE   "
//            + "((RESERVATION.CHECKIN <= '"+checkin +"'AND RESERVATION.CHECKOUT >= '"+checkout+"') OR "
//            + " (RESERVATION.CHECKIN >= '"+checkin +"'AND RESERVATION.CHECKOUT <= '"+checkout+"') OR "   
//            + " (RESERVATION.CHECKIN >= '"+checkin +"' AND RESERVATION.CHECKIN <= '"+checkout+"' AND RESERVATION.CHECKOUT >= '"+checkout+"')"
//            + "OR (RESERVATION.CHECKOUT <= '"+checkout+"' AND RESERVATION.CHECKOUT >= '"+checkin+"' AND RESERVATION.CHECKIN <='"+checkin+"')) AND ETAT = 'ACTIF' AND STATUT != 'FREE' order by num_room");

ResultSet outResult = st.executeQuery("  select * from APP.RESERVATION WHERE   "
            + "((RESERVATION.CHECKIN <= '"+checkin +"'AND RESERVATION.CHECKOUT >= '"+checkout+"') OR "
            + " (RESERVATION.CHECKIN >= '"+checkin +"'AND RESERVATION.CHECKOUT <= '"+checkout+"') OR "   
            + " (RESERVATION.CHECKIN >= '"+checkin +"' AND RESERVATION.CHECKIN <= '"+checkout+"' AND RESERVATION.CHECKOUT >= '"+checkout+"')"
            + "OR (RESERVATION.CHECKOUT <= '"+checkout+"' AND RESERVATION.CHECKOUT >= '"+checkin+"' AND RESERVATION.CHECKIN <='"+checkin+"')) AND ETAT = 'ACTIF' AND STATUT != 'FREE' order by num_room");

while (outResult.next())
    {   
      UneReservation reserv =  new UneReservation (outResult.getInt(1),
              getRoom(outResult.getInt("NUM_ROOM"),con),
getClient(outResult.getString("NUM_CLIENT"),con),outResult.getString("STATUT"),
outResult.getString("DATE_RESERVATION"), outResult.getString("CHECKIN"),  
outResult.getString("CHECKOUT"), outResult.getDouble("AMOUNT"), outResult.getDouble("DEPOSIT")); 
      
           
//       if( status[reserv.num_chambre]!=null )
//           status[reserv.num_chambre].statut=status[reserv.num_chambre].statut+" "+reserv.statut;
//       else
           // if(reserv.chambre.VIEW==view)
      System.out.println(reserv.chambre.ID);
           status[reserv.chambre.ID]=reserv;   
    }
    }  catch (Throwable e)  {
    insertError(e+""," openPTr5",con);
    }   
    return status;
}
public static void addReservation(UneReservation res, Connection con)
{ 
    try
    {
         
PreparedStatement psInsert = con.prepareStatement("insert into APP.RESERVATION( NUM_CLIENT,NUM_ROOM,CHECKIN,CHECKOUT,DATE_RESERVATION,AMOUNT,statut)  values (?,?,?,?,?,?,?)");
 
        psInsert.setString(1,res.client.NUM_AFFILIATION);
        psInsert.setInt(2,res.chambre.room_number);
        psInsert.setString(3,res.date_arrive);
        psInsert.setString(4,res.date_depart);
        psInsert.setString(5,res.date_reservation);
        psInsert.setDouble(6,res.montant);
        psInsert.setString(7,res.statut);
       
        psInsert.executeUpdate();
        psInsert.close();
   }  catch (Throwable e)  { insertError(e+""," addReservation",con); }

    
} 
static void getReservationsAnuller(int ntukoreho,int room_number, String checkin, String checkout,Statement st,Connection con) {
LinkedList <UneReservation>reservations = new LinkedList<UneReservation>();
    UneReservation reserv;
String toDelete="RESERVATION WILL BE DELETED";
    try
    {

ResultSet outResult = st.executeQuery("  select * from APP.RESERVATION WHERE NUM_ROOM= "+room_number+" and num_reservation!="+ntukoreho+" "
            + "and ((RESERVATION.CHECKIN <= '"+checkin +"'AND RESERVATION.CHECKOUT >= '"+checkout+"') OR "
            + " (RESERVATION.CHECKIN >= '"+checkin +"'AND RESERVATION.CHECKOUT <= '"+checkout+"') OR "   
            + " (RESERVATION.CHECKIN >= '"+checkin +"' AND RESERVATION.CHECKIN <= '"+checkout+"' AND RESERVATION.CHECKOUT >= '"+checkout+"')"
            + "OR (RESERVATION.CHECKOUT <= '"+checkout+"' AND RESERVATION.CHECKOUT >= '"+checkin+"' AND RESERVATION.CHECKIN <='"+checkin+"')) AND ETAT = 'ACTIF' AND STATUT != 'FREE' order by num_room");
  

while (outResult.next())
    {   
       reserv = reserv = new UneReservation (outResult.getInt(1),getRoom(outResult.getInt("NUM_ROOM"),con),
getClient(outResult.getString("NUM_CLIENT"),con),outResult.getString("STATUT"),
outResult.getString("DATE_RESERVATION"), outResult.getString("CHECKIN"),  
outResult.getString("CHECKOUT"), outResult.getDouble("AMOUNT"), outResult.getDouble("DEPOSIT"));
       
       if(reserv.num_reservation!=ntukoreho)
      { toDelete=toDelete+"\n--------------------------\n"+reserv.toString2();
       reservations.add(reserv);}
    }
    
 if(reservations.size()>0)
 {int n = JOptionPane.showConfirmDialog(null,toDelete,
" ACTION "  , JOptionPane.YES_NO_OPTION);
if (n == 0) { 
for(int i=0;i<reservations.size();i++)
{  
  UneReservation re=reservations.get(i);  
 updateReservation("ETAT='PASSIF'", re.num_reservation, "",st,con);               
}
}}
    }
    catch (Throwable e)  {
    insertError(e+""," openPTr6",con);
    }  
}   
static boolean  updateReservation(String etat,int num_res,String Status,Statement st,Connection con)     
{
try
{
Statement sU = con.createStatement();
String sqlCombined=etat+" "+Status;

if(etat!=null && etat.length()>4 && Status!=null && Status.length()>4 )
sqlCombined=etat+" , "+Status;

String createString1 = "update RESERVATION set  "+sqlCombined+"  where NUM_RESERVATION=" + num_res;
 System.out.println(createString1 );
sU.execute(createString1);  
}
catch (Throwable e)  {
insertError(e+""," openPTr7",con);
return false;
}
return true;
}

public static void main(String[] arghs)
{
  //  AutoOut --db =new AutoOut("localhost","felsen");
    //db.read();
   // db.listPrixErrone("0","3572","MMI");
} 
public  double getMontantRoomSejour(UneReservation r,String employe,
       Main main ) {
       double price =0;
       int deposit=0;
       int nbJours = 0; 
try 
    { 
   outResult = s.executeQuery("SELECT AMOUNT,DEPOSIT FROM APP.RESERVATION  WHERE NUM_RESERVATION ="+r.num_reservation+""
                + "and num_room="+r.chambre.room_number+"  ");
        System.out.println        ("SELECT AMOUNT,DEPOSIT FROM APP.RESERVATION  WHERE NUM_RESERVATION ="+r.num_reservation+""
                + "and num_room="+r.chambre.room_number+"  ");
        while (outResult.next()){ 
             price = outResult.getDouble(1); 
             deposit=outResult.getInt(2); 
        } 
          
         System.out.println        ( price+" price "+r.getDateArrivee()+" price "+r.getDateDepart());
       nbJours =  nb_joursBetweenToDate(r.getDateArrivee(),r.getDateDepart());
       if(nbJours==0)
           nbJours=1;
      
     System.out.println( nbJours+" nbJours ");
       System.out.println  ("SELECT sum(total) FROM APP.invoice  WHERE reference ='"+r.chambre.room_number+"'"
                + " and num_client='CHAMBRE' and heure > '"+r.date_arrive+"'");
//  outResult =  s.executeQuery(" SELECT sum(total) FROM APP.invoice  WHERE reference ='"
//          +r.chambre.room_number+"' "
//                + " and num_client='CHAMBRE' and heure > '"+r.date_arrive+"'");
//        System.out.println (outResult+"SELECT sum(total) FROM APP.invoice  WHERE reference ='"+r.chambre.room_number+"'"
//                + "and num_client='CHAMBRE' and heure > '"+r.date_arrive+"'");
      int totalSanck  =0;
//while (outResult.next()){ 
//   totalSanck=outResult.getInt(1); 
//}  
     double  tot = price * nbJours;
     int htva=(int)( (tot) / (1 + 0.18));
      int tva= (int)(tot- htva); 
      double totalYose=  tot+totalSanck; 
      int reduction=(int)(totalYose*(r.client.REDUCTION/100.0));
      int toPay=(int)(totalYose-deposit-reduction);
//       System.out.println(r.client.REDUCTION+"  somme "+deposit);
       String ref = JOptionPane.showInputDialog("REFERENCE","BC ");
       String  pay = JOptionPane.showInputDialog
       ("HOTEL : "+tot+
      "\n Sejours " + nbJours+" "+
              // + "CONSOMATION : "+totalSanck+
      "\n DEPOSIT : "+deposit+
      "\n REDUCTION : "+reduction+
      "\n A PAYER : "+toPay+" ",
      ""+(toPay));
    
       double payed=  Double.parseDouble(pay)+deposit+reduction ; 

boolean updated=updateReservation("", r.num_reservation, "STATUT='CHECKOUT', ETAT='PASSIF', PAYEMENT="+payed,s, conn);

if(updated)
{
 Invoice invoice =new Invoice (0,   employe,datez,r.client.ASSURANCE,tot,"",0,reduction);
 Credit cr=new Credit(0, r.client.NUM_AFFILIATION, ref, tot-payed, payed, "M"); 
//   System.out.println(r.chambre+"---------1----------" +nbJours);
//     System.out.println(main.cashier.db+"---------1----------" +nbJours);
 invoice.add(main.cashier.db.getProduct2(""+r.chambre.ID_PRODUCT_ROOM,nbJours,price)); 
//   System.out.println(main.cashier.db+"---------1----------" +nbJours);
//    System.out.println(r.chambre+"---------r.chambre.room_number----------" +r.chambre.room_number);
 
 invoice.id_invoice=main.cashier.db.doFinishVenteV5(invoice,cr,main,r.chambre.room_number,payed,0,""); 
 printRetardReleve2( r  );
    }

}  catch (Throwable e)  { insertError(e+"","getMontantRoomSejour"); } 

return price;
    } 
public void printRetardReleve2(UneReservation r  )
{ 
 LinkedList <Lot> getRapport =  getRaportSerena2(r.date_arrive, 
r.date_depart,r.num_reservation ) ;
double [] totaux= getRaportSerenaSums2(r.date_arrive, 
r.date_depart,r.num_reservation) ;
//getRapport.addAll(getRapportRoomSejour(r.date_arrive, 
//r.date_depart,r.chambre.room_number)); 
           
new Print(  getRapport,this,"FACTURERELEVE"   
         ,""+r.chambre.room_number,datez , r.client,
        ""+r.num_reservation ,totaux); 
} 

public LinkedList <Lot>  getRaport(String  Debut,String FIN,int room_number,String NUMERO_AFFILIE)
{
    LinkedList  <Lot> listlot = new LinkedList();
 try        {
 
   String sql="SELECT APP.LIST_PROFORMA.ID_PROFORMA,CODE_UNI,QUANTITE,"
        + "APP.LIST_PROFORMA.PRICE,APP.PRODUCT.NAME_PRODUCT,PRODUCT.ID_PRODUCT,PRODUCT.TVA,date,"
           + "employe,nom_client "
        + "FROM APP.BON_PROFORMA,APP.LIST_PROFORMA,APP.PRODUCT WHERE "
        + "REFERENCE LIKE'%"+room_number +"%' and"
        + " APP.BON_PROFORMA.ID_PROFORMA=APP.LIST_PROFORMA.ID_PROFORMA AND "
        + "APP.LIST_PROFORMA.CODE_UNI=APP.PRODUCT.CODE   ";
   System.out.println(sql);
     outResult = s.executeQuery(sql);

        //  Loop through the ResultSet and print the data
        while (outResult.next())
        {
        int id=outResult.getInt(1);
        String code   =  outResult.getString(2);
        int qte=outResult.getInt(3);
        double currentPrice=outResult.getDouble(4);
        String desi=outResult.getString(5);
        int id_pro=outResult.getInt(6);
        double tva=outResult.getDouble(7); 
        String date=outResult.getString(8);
        String employe=outResult.getString(9);
        String num_client=outResult.getString(10); 
        
    listlot.add(new Lot(
        "PRO"+id,
        date,
        code,
        desi,
        "", 
        ""+qte,
        ""+currentPrice,
        ""+currentPrice,
        employe,
        "",
       num_client

        )

        );
 
        }
        
        sql="SELECT APP.LIST.ID_INVOICE,CODE_UNI,QUANTITE,"
        + "APP.LIST.PRICE,APP.PRODUCT.NAME_PRODUCT,PRODUCT.ID_PRODUCT,PRODUCT.TVA,date,"
           + "employe,NUMERO_AFFILIE "
        + "FROM APP.INVOICE,APP.LIST,APP.PRODUCT,APP.CREDIT WHERE "
        + "( NUMERO_QUITANCE LIKE'%"+room_number +"%'  ) and  "
        + " APP.invoice.ID_INVOICE=APP.LIST.ID_INVOICE AND APP.invoice.ID_INVOICE=APP.credit.ID_INVOICE AND "
        + "APP.LIST.CODE_UNI=APP.PRODUCT.CODE   ";
   System.out.println(sql);
     outResult = s.executeQuery(sql);

        //  Loop through the ResultSet and print the data
        while (outResult.next())
        {
        int id=outResult.getInt(1);
        String code   =  outResult.getString(2);
        int qte=outResult.getInt(3);
        double currentPrice=outResult.getDouble(4);
        String desi=outResult.getString(5);
        int id_pro=outResult.getInt(6);
        double tva=outResult.getDouble(7); 
        String date=outResult.getString(8);
        String employe=outResult.getString(9);
        String num_client=outResult.getString(10); 
        
    listlot.add(new Lot(
        "INV"+id,
        date,
        code,
        desi,
        "", 
        ""+qte,
        ""+currentPrice,
        ""+currentPrice,
        employe,
        "",
       num_client

        )

        );
 
        }
        
        
        }
        catch(Throwable e)
        {insertError(e+""," V consommation");}


return listlot;
}
public LinkedList <Lot>  getRaportSerena2(String  Debut,String FIN,int res )
{
    LinkedList  <Lot> listlot = new LinkedList();
 try        { 
     
 int  n = JOptionPane.showConfirmDialog(null," VOULEZ VOUS IMPRIMER FACTURE DETAILLEE ?",
        " PRINT " , JOptionPane.YES_NO_OPTION); 
 
  String sql =       "select date,APP.CREDIT.id_INVOICE,total,tva,CASH,CREDIT,'',"
        + "NUMERO_QUITANCE   from  APP.INVOICE,APP.CREDIT   where  "
        + "APP.INVOICE.id_INVOICE=APP.CREDIT.id_INVOICE AND "
        + " NUMERO_QUITANCE='"+res+"' and heure< '"+FIN+"' and heure > '"+Debut+"' " ;
  
  System.out.println(sql);
  
outResult = s.executeQuery(sql);   

 while (outResult.next())
 {
 
     listlot.add(new Lot(""+ outResult.getInt(2),outResult.getString(8),
  outResult.getString(1),  
        ""+outResult.getDouble(3),
        ""+outResult.getDouble(4),
        ""+outResult.getDouble(5),
        ""+outResult.getDouble(6),
               "","","",""
        ));
   if(n==0)
     {
         listlot.addAll(getListInvoice(outResult.getInt(2)));
     }
  
}
}
        catch(Throwable e)
        {insertError(e+""," V getRaportSerena");}
return listlot;
}
public double[] getRaportSerenaSums2(String  Debut,String FIN,int res)
{
    double[] sums = new  double [7];
 try        {  
 
outResult = s.executeQuery(" select sum(total),sum(tva),sum(CASH),sum(CREDIT)"
        + " FROM APP.INVOICE,APP.CREDIT   where  "
        + " APP.INVOICE.id_INVOICE=APP.CREDIT.id_INVOICE AND "
        + " (NUMERO_QUITANCE='"+res+"'   ) "
        + " and  heure< '"+FIN+"' and heure > '"+Debut+"'");   
System.out.println("select sum(total),sum(tva),sum(CASH),sum(CREDIT)"
        + "from  APP.INVOICE,APP.CREDIT   where REFERENCE='"+res+"' and  heure< '"+FIN+"' and heure > '"+Debut+"'");   


int i=0;
 while (outResult.next())
 {
 sums[i]=outResult.getDouble(i+1);i++;
 sums[i]=outResult.getDouble(i+1);i++;
 sums[i]=outResult.getDouble(i+1);i++;
 sums[i]=outResult.getDouble(i+1);i++;
}
 
outResult = s.executeQuery(" select sum(total),sum(tva),sum(CASH),sum(CREDIT)"
        + " FROM APP.INVOICE    where  " 
        + " (  reference='"+res+"' ) "
        + " and  heure< '"+FIN+"' and heure > '"+Debut+"'");   
System.out.println(" select sum(total),sum(tva),sum(CASH),sum(CREDIT)"
        + " FROM APP.INVOICE    where  " 
        + " (  reference='"+res+"' ) "
        + " and  heure< '"+FIN+"' and heure > '"+Debut+"'");   

 i=0;
 while (outResult.next())
 {
 sums[i]+=outResult.getDouble(i+1);i++;
 sums[i]+=outResult.getDouble(i+1);i++;
 sums[i]+=outResult.getDouble(i+1);i++;
 sums[i]+=outResult.getDouble(i+1);i++;
} 
 
 
}
        catch(Throwable e)
        {insertError(e+""," V getRaportSerena");}
return sums;
}
public LinkedList <Lot>  getListInvoice( int id)
{
    LinkedList  <Lot> listlot = new LinkedList();
 try        { 
ResultSet outResult3 = s2.executeQuery("select name_product,quantite,price,LIST.tva "
        + "  from  APP.list,APP.product   where  "
        + " APP.LIST.id_INVOICE ="+id+" AND  app.list.code_uni=code");   

 while (outResult3.next())
 {
 double priceTotal=(outResult3.getInt(2)*outResult3.getDouble(3));
 double tva=priceTotal-(priceTotal /(1+outResult3.getDouble(4)));
  
     listlot.add(new Lot("" ,outResult3.getString(1)+" x "+ outResult3.getInt(2) ,
         "",  
        ""+priceTotal ,
        ""+ tva ,
        "" ,
        "" ,
               "","","",""
        ));
   
}
}
        catch(Throwable e)
        {insertError(e+""," V getListInvoice");}
return listlot;
}
public  LinkedList <Lot> getRapportRoomSejour(String  Debut,String FIN,int room_number) {
      LinkedList  <Lot> listlot = new LinkedList(); 
    double price =0 ;
    double depot=0;
    double paid=0;
    
    String checkin="";
       int nbJours = 0; 
try 
    { 
        outResult = s.executeQuery("SELECT * FROM APP.RESERVATION  WHERE " 
                + " num_room="+room_number+" and CHECKOUT< '"+FIN+"' and  CHECKIN> '"+Debut+"'");
   while (outResult.next()){ 
             price = outResult.getDouble("AMOUNT"); 
             depot = outResult.getDouble("DEPOSIT"); 
             paid = outResult.getDouble("PAYEMENT");
             checkin= ""+outResult.getTimestamp("CHECKIN");
             ID_INVOICE=outResult.getInt("ID_INVOICE_OUT");
        } 
         }  catch (Throwable e)  { insertError(e+"","getMontantRoomSejour"); }  
        
       nbJours =  nb_joursBetweenToDate(Debut,FIN);
      
     double  tot = price * nbJours;
     listlot.add(new Lot(
                        checkin,
                        "CASH ",
                        "DEPOSIT ", "", ""+depot,
                        "","","","","","","","",
                        "",
                        ""));
    listlot.add(new Lot(checkin,
                        "DAYS "+Debut.substring(0, 10)+" - "+FIN.substring(0, 10),
                        "ACCOM. #: "+room_number, ""+tot, "",
                        "","","","","","","","",
                        "",""));
     listlot.add(new Lot(
                        checkin,
                        "CASH ",
                        " ", "", ""+paid,
                        "","","","","","","","",
                        "",
                        "")); 
return listlot;
    } 

public static LinkedList <UneReservation> getReservations( int roomNumber,String checkin, String checkout
        ,Statement st,Connection con){
    LinkedList <UneReservation>reservations = new LinkedList<UneReservation>();
    UneReservation reserv;

try
{ 
String sql=    "  select * from APP.RESERVATION WHERE RESERVATION.NUM_ROOM ="+roomNumber+" and  "
+ "((RESERVATION.CHECKIN <= '"+checkin +"'AND RESERVATION.CHECKOUT >= '"+checkout+"') OR "
+ " (RESERVATION.CHECKIN >= '"+checkin +"'AND RESERVATION.CHECKOUT <= '"+checkout+"') OR "   
+ " (RESERVATION.CHECKIN >= '"+checkin +"' AND RESERVATION.CHECKIN <= '"+checkout+"' AND RESERVATION.CHECKOUT >= '"+checkout+"')"
+ "OR (RESERVATION.CHECKOUT <= '"+checkout+"' AND RESERVATION.CHECKOUT >= '"+checkin+"' AND RESERVATION.CHECKIN <='"+checkin+"')) AND ETAT = 'ACTIF'  order by num_room";
System.out.println(sql);
ResultSet outResult = st.executeQuery(sql);

while (outResult.next())
{
    System.out.println("UP KOO "+outResult.getInt(1));
    Rooms rm=getRoom(outResult.getInt("NUM_ROOM"),con);
    System.out.println("UP KOO "+rm);
    Client client=getClient(outResult.getString("NUM_CLIENT"),con);
    System.out.println("UP KOO "+client);
reserv = new UneReservation (outResult.getInt(1),rm,
client,outResult.getString("STATUT"),
outResult.getString("DATE_RESERVATION"), outResult.getString("CHECKIN"),  
outResult.getString("CHECKOUT"), outResult.getDouble("AMOUNT"), outResult.getDouble("DEPOSIT"));
reservations.add(reserv);
}
outResult.close();
}
catch (Throwable e)  {
insertError(e+""," openPTr8",con);
} 
    return reservations;
}
 public static LinkedList <UneReservation> getReservations2( int roomNumber,String checkin, String checkout
        ,Statement st,Connection con){
    LinkedList <UneReservation>reservations = new LinkedList<UneReservation>();
    UneReservation reserv;

try
{ 
String sql=    "  select * from APP.RESERVATION WHERE RESERVATION.NUM_ROOM ="+roomNumber+" and  "
+ " RESERVATION.CHECKIN <= '"+checkin +"'AND RESERVATION.CHECKOUT >= '"+checkout+"'  ";
System.out.println(sql);
ResultSet outResult = st.executeQuery(sql);

while (outResult.next())
{
    System.out.println("UP KOO "+outResult.getInt(1));
    Rooms rm=getRoom(outResult.getInt("NUM_ROOM"),con);
    System.out.println("UP KOO "+rm);
    Client client=getClient(outResult.getString("NUM_CLIENT"),con);
    System.out.println("UP KOO "+client);
reserv = new UneReservation (outResult.getInt(1),rm,
client,outResult.getString("STATUT"),
outResult.getString("DATE_RESERVATION"), outResult.getString("CHECKIN"),  
outResult.getString("CHECKOUT"), outResult.getDouble("AMOUNT"), outResult.getDouble("DEPOSIT"));
reservations.add(reserv);
}
outResult.close();
}
catch (Throwable e)  {
insertError(e+""," openPTr9",con);
} 
    return reservations;
} 
public static int nb_joursBetweenToDate( String debut,String fin){ // 
       int dayIn,dayOut, nbJours;
       
        Integer kkdeb=new Integer (debut.substring(8,10));
        int dateDocdeb = kkdeb.intValue();
        Integer kkfin=new Integer (fin.substring(8,10));
        int dateDocfin = kkfin.intValue();
    
       
        dayIn = days(debut.substring(5,7),0)+dateDocdeb+4;
        dayOut = days(fin.substring(5,7),0)+dateDocfin+4;
         
        nbJours = dayOut-dayIn; 
       return nbJours;
       
       
       
   }
   
  static int days(String m,int mult)
{
    int dec=31;
    
    if(mult<1)
        dec=0;
    int days=0;
    if(m.equals("01"))
    days=(mult*334+dec)+0 ;
    if(m.equals("02"))
    days=(mult*334+dec)+31 ;
    if(m.equals("03"))
    days=(mult*334+dec)+59 ;
    if(m.equals("04"))
    days=(mult*334+dec)+90 ;
    if(m.equals("05"))
    days=(mult*334+dec)+120 ;
    if(m.equals("06"))
    days=(mult*334+dec)+151 ;
    if(m.equals("07"))
    days=(mult*334+dec)+181 ;
    if(m.equals("08"))
    days=(mult*334+dec)+212 ;
    if(m.equals("09"))
    days=(mult*334+dec)+243 ;
    if(m.equals("10"))
    days=(mult*334+dec)+ 273 ;
    if(m.equals("11"))
    days=(mult*334+dec)+ 304;
    if(m.equals("12"))
    days=(mult*334+dec)+ 334;
return days;
}

   


 }
