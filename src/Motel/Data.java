package Motel;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author aimable
 */

/*
Interface Stock copyright 2007 Kimenyi Aimable Gestion des ventes et du stock
*/
 
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import javax.swing.JFrame;

public  class Data extends JFrame
{
    private  String nom;                                // le nom du Caissier
    static int counterInvoice;
    final int LENTH=700;
    final int WIDT =800;
    private  JButton priceButton;                       // le boutton qui envoie le selection
    private  JButton qtyButton;                     // le boutton qui enleve l'produit
    private  JButton addButton,saveButton,vedetteButton;                        // le botton qui ajoute les produits
    private  JList  allProductList=new JList();        // un JList qui affiche tous tout les produits
    private  JScrollPane  scrollPaneProductList;     	// si il ya trop des produits qui depasse le Jpanel
    private  JPanel productPane ;
    private Cashier cashier; // Caisse courant
    private String stock;
     private String var;

/********************************************************************************
le constructeur qui cree un frame
********************************************************************************/
 Data(Cashier cashier,String stock,String var) 
{ 
this.cashier= cashier; 
this.var=var;
this.stock=stock;
counterInvoice=1; 
nom=cashier.cashierName; 
showProduct();
draw(); 
} 
// on dessine les composants
public void draw() 
{
    setTitle(nom+"'s Stock Session");
    enableEvents(WindowEvent.WINDOW_CLOSING);

    //on fixe  les composant del'interface
    Container content = getContentPane();
    westPane(content);
    eastPane(content);

            priceProduct();  // Ajout manuel
            addProduct();   // Ajout preselectionne
            qtyProduct();
            ved();

     setSize(WIDT,LENTH);
     setVisible(true);

}

// Methode qui Montre tout le stock
void showProduct()
{
 
    LinkedList <Product> allPro = cashier.db.doworkOtherData(var);
    
     int c=allPro.size();
                
		Product [] control= new Product [c];
		for(int i=0;i<c;i++)
		{
			//Product p= cashier.allProduct.get(i);
			control[i]=allPro.get(i);
			

                }

				allProductList.setListData(control);
}

/**************************************************************************
affiche du stock
**************************************************************************/

public void westPane(Container content )
{
    productPane  = new JPanel();
  
    productPane.setPreferredSize(new Dimension (600,700));
    
   JLabel out= new JLabel(stock+"*******NAME*******&&******PRIX");

		
    scrollPaneProductList =new JScrollPane(allProductList);
    scrollPaneProductList.setPreferredSize(new Dimension(550,600));

    allProductList.setBackground(Color.pink);


    productPane.add(out,BorderLayout.NORTH);
    productPane.add(scrollPaneProductList,BorderLayout.SOUTH);
  

    content.add(productPane, BorderLayout.WEST);


}
/***********************************************************************
fenetre des achats
***********************************************************************/
public void eastPane(  Container content )
{
    JPanel panel = new JPanel();
    panel.setLayout(new GridLayout(3,1));
    qtyButton=new JButton("Code Bar");
    qtyButton.setBackground(Color.yellow);
    qtyButton.setFont(new Font("Helvetica", Font.PLAIN, 20));
    priceButton= new JButton(" Guhindura ");
    priceButton.setBackground(Color.red);
    priceButton.setFont(new Font("Helvetica", Font.PLAIN, 20));
    addButton= new JButton("IBISHYA");
 
    addButton.setFont(new Font("Helvetica", Font.PLAIN, 20));
    saveButton=new JButton("Save");
    saveButton.setBackground(Color.orange);
    saveButton.setFont(new Font("Helvetica", Font.PLAIN, 20));
    
    vedetteButton=new JButton("Vedette");
    vedetteButton.setBackground(Color.green);
    vedetteButton.setFont(new Font("Helvetica", Font.PLAIN, 20));
    
   
    panel.setPreferredSize(new Dimension (180,300));
    
    panel.add( qtyButton);
    panel.add( priceButton);
   // panel.add( addButton);
   // panel.add( saveButton);
    panel.add( vedetteButton);

    content.add(panel, BorderLayout.EAST);


}
/**********************************************************************************************
Ajout Mannuel  verifier les entrees
**********************************************************************************************/
public void ved()
{
    vedetteButton.addActionListener(new ActionListener() {
    public void actionPerformed(ActionEvent ae)
    {

        if(ae.getSource()==vedetteButton && allProductList.getSelectedValue()!=null   )
        {
        Product p = (Product )allProductList.getSelectedValue();
        int c = p.productCode;
       
    
    int ino = getIn( " Categorie ushaka 1-8"," INT",0);
    if(ino!=-1 && 0<ino && ino<10)
    {
    int out = getIn( " UMWANYA WA KANGAHE 0-11 "," INT",0);
    if(out!=-1 && 0<=out && out<12)
    {
        Vedette v;
       
        for(int i=0;i<cashier.vedette.size();i++)
        {
            v=cashier.vedette.get(i);
        if(v.cat==ino && v.num==out)
        {
            int qte = getIn( " Qte Multiple "," INT",0);
            v.ID_PRODUCT=c;
        v.qte=qte;
        cashier.db.fixVedette(v);
Product inter=cashier.db.getProduct(c);
inter.qty=qte;
        cashier.allProductVedette.add(inter);
        
        
        }
        }
       
    }
    }}}});
}
/**********************************************************************************************
Ajout Mannuel  verifier les entrees
**********************************************************************************************/
public void priceProduct()
{
    priceButton.addActionListener(new ActionListener() {
    public void actionPerformed(ActionEvent ae)
    {

        if(ae.getSource()==priceButton && allProductList.getSelectedValue()!=null   )
        { 
      Product p = (Product )allProductList.getSelectedValue();
        int c = p.productCode; 
     try
     {
         
     String money = JOptionPane.showInputDialog(null, "Kizagurishwa");
     
     
     Double money3=new Double(money);
     double money4= money3.doubleValue();
     if(money4>=0)
     {
     double tvaa=0.0;
     if(stock.equals("CASHNORM"))
     {
    String tva = JOptionPane.showInputDialog(null, "TVA : 0.18");
    if(tva.equals("0")||tva.equals("0.18"))
    {Double in_d=new Double(tva);
    tvaa= in_d.doubleValue(); 
    cashier.db.changePrice(c,money4,tvaa,stock,nom,p.productName+" "+stock);}
    else
    JOptionPane.showMessageDialog(null," Ikibazo Muri TVA "," Mwandiste tva = "+ tva ,JOptionPane.PLAIN_MESSAGE);

    }

     else if(stock.contains("RAMA"))
     {
       
     String tva = JOptionPane.showInputDialog(null, "CODE RAMA ");
     
     String observation = JOptionPane.showInputDialog(null, "OBSERVATION RAMA ",p.observation);
    
    cashier.db.changePriceRama(c,money4,tva,stock,nom,p.productName+" "+stock,observation);
     }
   else
             cashier.db.changePrice(c,money4,tvaa,var,nom,p.productName+" "+stock);
        
         JOptionPane.showMessageDialog(null,stock," INPUT ",JOptionPane.PLAIN_MESSAGE);

     } }
    
    catch(Throwable ex)
    {
    JOptionPane.showMessageDialog(null," Ikibazo Mu nyandiko "," Itariki ",JOptionPane.PLAIN_MESSAGE);
    }
   
        
        
        }}});
}





/***************************************************************************************************
Ajoute un produit dans le panier
**************************************************************************************************/
public void addProduct()
{  addButton.addActionListener(
new ActionListener()
{  public void actionPerformed(ActionEvent ae)
{  if(ae.getSource()==addButton  )
{

    String n = JOptionPane.showInputDialog(null, " IZINA: s");
    if(n!=null && n.length()!=0)
    {
    int cat = getIn( "  cat"," INT",0);
    if(cat!=-1)
    {
    String t = JOptionPane.showInputDialog(null, " type: s");
    if(t!=null && t.length()!=0)
    {
    int ino = getIn( " Cyaranguwe "," INT",0);
    if(ino!=-1)
    {
    int out = getIn( " Kizagurishwa "," INT",0);
    while(ino>=out && out!=-1 )
          out = getIn( " Kizagurishwa in >=out "," INT",0);
    if(out!=-1)
    {
     
        
    int quantite =getIn( " Umubare "," INT",0);
    if(quantite!=-1)
    {

    int id = cashier.allProduct.size()+2;

    
    }}}}}}
    
    }}});

}
public void qtyProduct()
{  qtyButton.addActionListener(
new ActionListener()
{  public void actionPerformed(ActionEvent ae)
{
if(ae.getSource()==qtyButton && allProductList.getSelectedValue()!=null )
{
  Product p = (Product )allProductList.getSelectedValue();
        int c = p.productCode;
String in = JOptionPane.showInputDialog(p.productName+" Scannez Maintenant "," Scann");

if(!in.equals(""))
{
cashier.db.upDateCodeBar(in, c);

}
}}});
}
public  int getIn (String s,String s1,int i)
{
    int entier=-1;

    String in = JOptionPane.showInputDialog(null, s+s1);
   boolean done =true;
    
    while(in!=null && i<3&& done==true)
    try
    {   if(in.equals(""))
        {
        i++;
       in = JOptionPane.showInputDialog(null, s+" Enter a number ");
        }
        else
        {
        Integer in_int=new Integer(in);
        entier = in_int.intValue();
        done=false;
        }
        
     }
    catch (NumberFormatException e)
    {
        i++;
        in = JOptionPane.showInputDialog(null, s+" ="+in+"= Not a number; Enter a number ");
        done=true;
    }
     if(i==3)
        {
        JOptionPane.showMessageDialog(this,"Too many try"," Sorry we have to Exit ",JOptionPane.PLAIN_MESSAGE);
        
        }

    return entier;
}




    @Override
protected void processWindowEvent(WindowEvent evt)
{
   if(WindowEvent.WINDOW_CLOSING == evt.getID())
   {
	 
         this.dispose();
			//default icon, custom title
//JOptionPane.showMessageDialog(this,"RIRAFUNGIRA RIMWE N'ISHYIGA"," IKIBAZO",JOptionPane.PLAIN_MESSAGE);
        			//this.setDefaultCloseOperation(HIDE_ON_CLOSE);

			
	}

}}