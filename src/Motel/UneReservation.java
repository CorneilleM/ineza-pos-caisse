/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Motel;

/**
 *
 * @author Yves
 */
public class UneReservation {
    
   int num_reservation;  
    Rooms chambre;
    Client client;
    String statut;
    String date_reservation;
    String date_arrive;
    String date_depart;
    double montant;
    double deposit;
    
  public UneReservation(int num_reservation,Rooms chambre,Client client,String statut,
            String date_reservation,String date_arrive,String date_depart,double montant,double deposit){
        this.num_reservation = num_reservation;
        this.chambre = chambre;
        this.client = client;
        this.date_reservation = date_reservation;
        this.date_arrive = date_arrive;
        this.date_depart = date_depart;
        this.statut = statut;
        this.montant =  montant;
        this.deposit=deposit;
         
    }
     
    @Override
     public String toString(){
       return  chambre.room_number+" : "+statut+" : "+client.NOM_CLIENT+" IN : "+date_arrive.substring(0, 11) +" : "+date_depart.substring(0, 11);
  
//         return   " : "+statut+" :   IN : "+date_arrive.substring(0, 11) +" : "+date_depart.substring(0, 11);
    }
      public String toString2(){
        return ""+" ROOM : "+ chambre.room_number+"    \n STATUT : "+statut+"    \n  CLIENT : "+
                client.NOM_CLIENT+"   \nCHECK IN : "+date_arrive
                +"   \n COST: "+montant+"   \n DEPOSIT: "+deposit;
    }
  
    public String getStatut(){
        return this.statut;
    }
    public String getDateArrivee(){
        return this.date_arrive;
    }
    public String getDateDepart(){
        return this.date_depart;
    }
    public Client getClient(){
        return this.client;
    }
    public Rooms getChambre(){
        return this.chambre;
    }
    public void setStatut(String statut){
        this.statut = statut;
    }
    public String getName(){
        return this.client.NOM_CLIENT;
    }
}
